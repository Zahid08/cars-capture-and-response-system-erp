<?php
/*
Template Name: Header HTML
*/
get_header(); ?>

    <div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <div class="kc_clfw"></div>
        <section class="kc-elm kc-css-741346 kc_row">
            <div class="kc-row-container  kc-container">
                <div class="kc-wrap-columns">
                    <div class="kc-elm kc-css-830155 kc_col-sm-12 kc_column kc_col-sm-12">
                        <div class="kc-col-container">
                            <div class="kc-elm kc-css-637975 kc_text_block post-content-wrapper">
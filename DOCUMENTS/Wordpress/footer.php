<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
</div>


<div class="clearfix"></div>

<footer id="colophon" class="site-footer" role="contentinfo">
    <div class="kc_row">
        <div class="kc-row-container  kc-container">
                <div class="footer-top">
                    <div class="kc-wrap-columns">
                        <div class="about-gso">
                            <?php
                            // Calling the header sidebar if it exists.
                            if ( !dynamic_sidebar( 'gso_footer_text' ) ):
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
                <div class="footer-bottom">
                    <div class="kc-wrap-columns">
                        <div class="kc_column kc_col-sm-12">
                            <div class="copyright">
                                <p class="copy-text">© 2017 GSO. All Rights Reserved. </p>

                                <div class="footer-right">
                                    <p class="develop">Developed by</p>
                                    <a class="powered" href="http://www.unlocklive.com/">
                                        <?php
                                        // Calling the header sidebar if it exists.
                                        if ( !dynamic_sidebar( 'gso_footer_logo' ) ):
                                        endif;
                                        ?>
                                    </a>
                                </div>

                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>



</footer><!-- .site-footer -->

<?php wp_footer(); ?>

</body>
</html>

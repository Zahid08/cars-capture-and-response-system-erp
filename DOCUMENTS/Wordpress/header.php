<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <!--[if lt IE 9]>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="header">
    <div class="kc_row">
        <div class="kc-row-container  kc-container">
            <div class="kc-wrap-columns">
                <div class="header-wrapper">
                    <div class="kc_column kc_col-sm-9">
                        <div class="logo-left">
                            <?php
                            $custom_logo_id = get_theme_mod( 'custom_logo' );
                            $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                            if ( has_custom_logo() ) {
                                echo '<a href="'.site_url().'"><img src="'. esc_url( $logo[0] ) .'"></a>';
                            } else {
                                echo '<h1>'. esc_attr( get_bloginfo( 'name' ) ) .'</h1>';
                            }
                            ?>
                        </div>
                        <div class="banner-title">
                            <?php
                            // Calling the header sidebar if it exists.
                            if ( !dynamic_sidebar( 'gso_header_text' ) ):
                            endif;
                            ?>
                        </div>
                    </div>
                    <div class="kc_column kc_col-sm-3">
                        <div class="logo-right">
                            <?php
                            // Calling the header sidebar if it exists.
                            if ( !dynamic_sidebar( 'gso_inner_top' ) ):
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ( is_page( 81 ) ): ?>
    <div class="slider-content">
        <div class="kc_row">
            <div class="kc-row-container  kc-container">
                <div class="kc-wrap-columns">
                    <div class="kc_column kc_col-sm-12">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">

                                <?php
                                $args = array(
                                    'post_type' => 'slider', //Post type
                                    'posts_per_page' => 10
                                );
                                $the_query = new WP_Query( $args );
                                $i = 0;
                                if ( $the_query->have_posts() ) {

                                    while ( $the_query->have_posts() ) {
                                        $the_query->the_post();

                                        $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'full' );
                                        $i++;
                                        ?>
                                        <div class="item <?php if($i == '1'){echo "active";}?>">
                                            <img src="<?=$url?>" class="img-responsive" alt="<?php the_title();?>" />
                                        </div>
                                    <?php } } ?>

                            </div>

                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>



<div class="menu-section">
    <div class="kc_row">
        <div class="kc-row-container  kc-container">
            <div class="kc-wrap-columns">
                <div class="kc_column kc_col-sm-12">
                    <div class="main-menu">
                        <div class="navbar navbar-inverse navbar">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <?php if ( has_nav_menu( 'primary' ) ) : ?>
                                <div class="collapse navbar-collapse" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'gso' ); ?>">
                                    <?php
                                    wp_nav_menu( array(
                                        'theme_location' => 'primary',
                                        'menu_class'     => 'primary-menu',
                                        'menu_class'     => 'nav navbar-nav',
                                    ) );
                                    ?>
                                </div><!-- .main-navigation -->
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="kc_row">
    <div class="kc-row-container  kc-container">
        <div class="kc-wrap-columns">
            <div class="kc_column kc_col-sm-12">
                <?php if ( is_page( 81 ) ): ?>
                    <div class="notice-section">
                        <?php
                        // Calling the header sidebar if it exists.
                        if ( !dynamic_sidebar( 'gso_notice_box' ) ):
                        endif;
                        ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>




        <div class="clearfix"></div>
    <div id="content" class="site-content">


# GIT Add Single File
=====================

git add <file_name>

git commit -m "comment"

git push

# GIT Add All File
==================

git add --all

git commit -m "comment"

git push --all

# Undo File Changes
====================
git checkout -- path/to/the/file.txt

1. Single File :

git checkout <path-to-file>

2. All Files:

git checkout --

# GIT Merge: (Please, commit your changes or stash them before you can merge.)
==============================================================================

git stash

git pull

git stash apply

# GIT reset "local" changes
===========================

git fetch --all

git reset --hard origin/master

# Git Reset Previous Commit
===========================

STEP 1:
git reflog show

/*
Result:
edbfd0d HEAD@{0}: commit: Import code from development environment.
da39602 HEAD@{1}: checkout: moving from master to development
*/

STEP 2:
git reset --hard da39602


# Remove credentials from Git
Windows:Control Panel → User Accounts → Credential Manager → Manage Windows Credentials.

# Git Export tar.gz  Archive
============================

tar czf changed-files.tar.gz $(git log ^a7b0207bd713acec6bf1a03fb4aa9725e5f68a93 1b059afbfa3aac32cb5ddff2e3115a875f8b286b --name-only --pretty=format: | sort | uniq)

============================

# Show Git Username
git config user.name
git config --list

============================

# Set Git Username
git config --global user.name "Masud Hasan"
git config --global user.email "hasanmasudnet@gmail.com"

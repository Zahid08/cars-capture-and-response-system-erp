<?php

$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'tUOwoOn6Lm-oIXpGwmKYr6DUGGQTY_YM',
        ],
    ],
];

if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'generators' => [
            'crud' => [
                'class' => 'unlock\modules\core\gii\generators\crud\Generator',
                'templates' => [
                    'unlock' => '@unlock/modules/core/gii/generators/crud/default',
                ]
            ],
            'model' => [
                'class' => 'yii\gii\generators\model\Generator',
                'templates' => [
                    'unlock' => '@unlock/modules/core/gii/generators/model/default',
                ]
            ],
            'module' => [
                'class' => 'unlock\modules\core\gii\generators\module\Generator',
                'templates' => [
                    'unlock' => '@unlock/modules/core/gii/generators/module/default',
                ]
            ],
        ],
    ];
}

return $config;
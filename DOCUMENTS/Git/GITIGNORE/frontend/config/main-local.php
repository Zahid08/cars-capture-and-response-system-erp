<?php
return [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'w1Ykqn2AWMq89cDb9FmqlL_2niL3FKJm',
        ],
    ],
];

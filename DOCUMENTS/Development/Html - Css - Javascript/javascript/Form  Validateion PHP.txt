# PHP
======
Solution 1:

 - Field Validation
    $validator = \yii\validators\Validator::createValidator('required', $model, ['submit_authority']);
    $model->validators->append($validator);

 - Field
    echo $form->field($model, 'submit_authority');

Solution 2:

<?php
    $this->registerJs('
            jQuery("#w0").yiiActiveForm("add",{
                "id": "customer-name",
                "name": "name",
                "container": ".field-customer-name",
                "input": "#customer-name",
                "error": ".help-block.help-block-error",
                "validate": function(attribute, value, messages, deferred, $form) {

                    yii.validation.required(value, messages, {
                        "message": "Name be blank bug."
                    });

                    yii.validation.string(value, messages, {
                        "message": "Name must be a string.",
                        "max": 255,
                        "tooLong": "Name should contain at most 255 characters.",
                        "skipOnEmpty": 1
                    });
                }
        });
    ');
 ?>

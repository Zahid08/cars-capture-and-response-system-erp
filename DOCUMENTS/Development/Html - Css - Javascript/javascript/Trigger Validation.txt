# Trigger validation for the whole form

$('#contact-form').yiiActiveForm('validate');

# Triggering validation for individual form fields

$('#formName').yiiActiveForm('validateAttribute', 'fieldId');
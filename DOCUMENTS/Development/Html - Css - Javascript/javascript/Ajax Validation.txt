# Yii2 Ajax Validation Return Sample

1. {"password":["not allow empty"],"confirm":[""not allow empty"],"email":["not valid email"],"salesId":["not allow empty"],"username":["user name exists"]}

2. return ['success'=>'true'];

#  Validate Yii2 forms before submitting with AJAX

$(document).ready(
    $('#login-form').on('beforeSubmit', function(event, jqXHR, settings) {
        var form = $(this);
        if(form.find('.has-error').length) {
            return false;
        }

        $.ajax({
            url: form.attr('action'),
            type: 'post',
            data: form.serialize(),
            success: function(data) {
                // do something ...
            }
        });

        return false;
    }),
);
<div class="row" id="sampleItems">
    <div class="dynamic-options-panel">
        <div class="dynamic-panel-head">
            <h4>Sample Details</h4>
            <div class="right">
                <a href="javascript:void(0)" class="btn btn-add-more add-new-row" id="add-new-row">
                    <i class="fa fa-plus"></i> Add Sample Item
                </a>
            </div>
        </div>
        <div class="dynamic-panel-container">
            <table class="table dynamic-panel-table-form" id="sampleTable">
                <thead>
                <tr>
                    <th width="80">Serial No</th>
                    <th>Title</th>
                    <th width="15%">Amount</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($modelSampleItems as $index => $item){ ?>
                    <tr>
                        <td>
                            <span class="serial-number"><?= ($index + 1) ?></span>
                            <?= $form->field($item, "[$index]id", ['template' => '{input}{error}', 'options' => ['tag' => null]])->hiddenInput(['class' => 'field-id']); ?>
                        </td>
                        <td>
                            <?= $form->field($item, "[$index]title", ['template' => '{input}{error}'])->textInput(['class' => 'form-control field-title']); ?>
                        </td>
                        <td>
                            <?= $form->field($item, "[$index]amount", ['template' => '{input}{error}'])->textInput(['class' => 'form-control field-amount']); ?>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                <tr class="footer-total footer-total-sample">
                    <td>&nbsp</td>
                    <td class="total">Total:</td>
                    <td id="total-sample-amount"></td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

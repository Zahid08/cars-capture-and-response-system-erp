<?php

class DynamicItem
{

    public function actionCreate()
    {
        $model = new Compilation();

        $model->compilation_type = CommonHelper::COMPILATION_PART_ONE;

        $modelCompilationItem = [];

        // Budget Code List
        $compilationBudgetCode = BudgetCode::getBudgetCodeForPartOneAndTwo();

        foreach ($compilationBudgetCode as $key => $item) {
            $modelCompilationItem[$key] = new CompilationItem();
            $modelCompilationItem[$key]['budget_code_id'] = $item['budget_code_id'];
            $modelCompilationItem[$key]['directorate_id'] = $item['directorate_id'];
        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && Model::loadMultiple($modelCompilationItem, Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return array_merge(\yii\widgets\ActiveForm::validate($model), \yii\widgets\ActiveForm::validateMultiple($modelCompilationItem));
        }

        if ($model->load(Yii::$app->request->post()) && Model::loadMultiple($modelCompilationItem, Yii::$app->request->post()))
        {
            $this->saveData($model, $modelCompilationItem);
        }

        $model->month = CommonHelper::getCurrentMonth();
        $model->financial_year = CommonHelper::getSelectedFinancialYear();

        return $this->render('_form', [
            'model' => $model,
            'modelCompilationItem' => $modelCompilationItem,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $modelCompilationItem = $model->compilationItems;

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && Model::loadMultiple($modelCompilationItem, Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return array_merge(\yii\widgets\ActiveForm::validate($model), \yii\widgets\ActiveForm::validateMultiple($modelCompilationItem));
        }

        if ($model->load(Yii::$app->request->post()) && Model::loadMultiple($modelCompilationItem, Yii::$app->request->post()))
        {
            $this->saveData($model, $modelCompilationItem);
        }

        return $this->render('_form', [
            'model' => $model,
            'modelCompilationItem' => $modelCompilationItem,
        ]);
    }


    private function saveData($model, $modelCompilationItem){
        if (isset($model) && isset($modelCompilationItem)) {

            $transaction = Yii::$app->db->beginTransaction();

            try {
                $post = Yii::$app->request->post();

                if (!$model->save()){
                    throw new Exception(Yii::t('app', Html::errorSummary($model)));
                }

                foreach ($modelCompilationItem as $key => $item) {
                    $item->compilation_id = $model->id;
                    if(empty($item->amount)) $item->amount = 0;
                    if (!$item->save()) {
                        throw new Exception(Yii::t('app', Html::errorSummary($model)));
                    }
                }

                $transaction->commit();
                Yii::$app->session->setFlash('success', Yii::t('app', 'Record successfully saved.'));
                $this->_redirect($model);
            }
            catch (Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
    }

}
<?php

class DynamicItem
{
    public function actionCreate()
    {
        $model = new Sample();

        $hasSampleItem = true;
        $modelSampleItems = [new SampleItem()];

        $sampleItemArray = Yii::$app->request->post('SampleItem');
        if($sampleItemArray){
            $modelSampleItems = [];
            foreach ($sampleItemArray as $key => $item) {
                $modelSampleItems[$key] = new SampleItem();
            }
        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && Model::loadMultiple($modelSampleItems, Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return array_merge(\yii\widgets\ActiveForm::validate($model), \yii\widgets\ActiveForm::validateMultiple($modelSampleItems));
        }

        if ($model->load(Yii::$app->request->post()) && Model::loadMultiple($modelSampleItems, Yii::$app->request->post()))
        {
            $this->saveData($model, $modelSampleItems);
        }

        return $this->render('_form', [
            'model' => $model,
            'modelSampleItems' => $modelSampleItems,
            'hasSampleItem' => $hasSampleItem,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $hasSampleItem = true;
        $modelSampleItems = $model->sampleItems;
        if (empty($modelSampleItems)) {
            $hasSampleItem = false;
            $modelSampleItems = [new SampleItem()];
        }

        $sampleItemArray = Yii::$app->request->post('SampleItem');
        if($sampleItemArray){
            $modelSampleItems = [];
            foreach ($sampleItemArray as $key => $item) {
                $modelSampleItems[$key] = $this->findSampleItemModel($item);
            }
        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && Model::loadMultiple($modelSampleItems, Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return array_merge(\yii\widgets\ActiveForm::validate($model), \yii\widgets\ActiveForm::validateMultiple($modelSampleItems));
        }

        if ($model->load(Yii::$app->request->post()) && Model::loadMultiple($modelSampleItems, Yii::$app->request->post()))
        {
            $this->saveData($model, $modelSampleItems);
        }

        return $this->render('_form', [
            'model' => $model,
            'modelSampleItems' => $modelSampleItems,
            'hasSampleItem' => $hasSampleItem,
        ]);
    }

    private function saveData($model, $modelSampleItems){
        if (isset($model) && isset($modelSampleItems)) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $post = Yii::$app->request->post();

                $model->ltr_date = date('Y-m-d',strtotime($model->ltr_date));
                if (!$model->save()){
                    throw new Exception(Yii::t('app', Html::errorSummary($model)));
                }


                $ids = [];
                foreach ($modelSampleItems as $key => $item) {
                    // Process Image and File
                    if(!empty($_FILES['SampleItem']['tmp_name'][$key]['attachment_file'])){
                        $file = [];
                        $file['name'] = $_FILES['SampleItem']['name'][$key]['attachment_file'];
                        $file['size'] = $_FILES['SampleItem']['size'][$key]['attachment_file'];
                        $file['tmp_name']  = $_FILES['SampleItem']['tmp_name'][$key]['attachment_file'];
                        $file['type'] = $_FILES['SampleItem']['type'][$key]['attachment_file'];

                        if($fileName = $this->uploadFile($model, $item, $file)){
                            $item->attachment_file = $fileName;
                        }
                    }
                    else{
                        $item->attachment_file = isset($item->oldAttributes['attachment_file']) ? $item->oldAttributes['attachment_file'] : '';
                    }

                    $item->sample_id = $model->id;
                    if (!$item->save()) {
                        throw new Exception(Yii::t('app', Html::errorSummary($item)));
                    }

                    array_push($ids, $item->id);
                }

                // Remove Sample Item
                if($ids){
                    SampleItem::deleteAll(
                        [
                            'and', 'sample_id = :sample_id',
                            ['not in', 'id', $ids]
                        ],
                        [
                            ':sample_id' => $model->id
                        ]
                    );
                }

                $transaction->commit();
                Yii::$app->session->setFlash('success', Yii::t('app', 'Record successfully saved.'));
                $this->_redirect($model);
            }
            catch (Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
    }

    public function uploadFile($model, $item, $file){

        $prevFile = $item->attachment_file;

        $filePath = FileHelper::getUniqueFilenameWithPath($file['name'], 'sample/' . $model->id);
        $fileName = FileHelper::getFilename($filePath);
        $fileName = $model->id . '/' . $fileName;

        //Upload File
        if(move_uploaded_file($file['tmp_name'], $filePath)){
            if($fileName != $prevFile){
                FileHelper::removeFile($prevFile, 'sample');
            }
            return $fileName;
        }
        else{
            return false;
        }
    }

    protected function findSampleItemModel($item)
    {
        if(array_key_exists('id', $item)){
            if (($model = SampleItem::findOne($item['id'])) !== null) {
                return $model;
            }
            else {
                return new SampleItem();
            }
        }
        else{
            return new SampleItem();
        }
    }


}
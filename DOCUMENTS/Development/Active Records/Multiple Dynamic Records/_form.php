<div class="row" id="sampleItems">
    <div class="dynamic-options-panel">
        <div class="dynamic-panel-head">
            <h4>Sample Details</h4>
            <div class="right">
                <a href="javascript:void(0)" class="btn btn-add-more add-new-row" id="add-new-row">
                    <i class="fa fa-plus"></i> Add Sample Item
                </a>
            </div>
        </div>
        <div class="dynamic-panel-container">
            <table class="table dynamic-panel-table-form" id="sampleTable">
                <thead>
                    <tr>
                        <th width="80">Serial No</th>
                        <th>Title</th>
                        <th>Amount</th>
                        <th width="80">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($modelSampleItems as $index => $item){ ?>
                    <?php if($hasSampleItem) { ?>
                        <tr>
                            <td>
                                <span class="serial-number"><?= ($index + 1) ?></span>
                                <?= $form->field($item, "[$index]id", ['template' => '{input}{error}', 'options' => ['tag' => null]])->hiddenInput(['class' => 'field-id']); ?>
                            </td>
                            <td>
                                <?= $form->field($item, "[$index]title", ['template' => '{input}{error}'])->textInput(['class' => 'form-control field-title']); ?>
                            </td>
                            <td>
                                <?= $form->field($item, "[$index]amount", ['template' => '{input}{error}'])->textInput(['class' => 'form-control field-amount']); ?>
                            </td>
                            <td>
                                <a href="javascript:void(0)" class="btn btn-danger remove-row" style="display: none"><i class="fa fa-times"></i> Delete</a>&nbsp;
                            </td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                </tbody>
                <tfoot>
                    <tr class="footer-total footer-total-sample">
                        <td>&nbsp</td>
                        <td class="total">Total:</td>
                        <td id="total-sample-amount"></td>
                        <td>&nbsp</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<div class="hidden-field-for-javascript" style="display: none">
    <?= $form->field($item, "ship_id", ['template' => '{input}'])->dropDownList(\unlock\modules\ship\models\Ship::shipDropDownList(),['prompt'=>'Select ' . $item->getAttributeLabel('ship_id') , 'id' => 'hidden-dropdown-field-ship-id']) ?>
</div>

<script type="text/javascript">
    $(document).ready(function ()
    {
        // Add New Row
        var index = $('.dynamic-panel-container tbody tr').length;

        $(document).on('blur', '.dynamic-field-validation', function(){
            $("#formName").yiiActiveForm("validateAttribute", $(this).attr('id'));
        });

        // Remove Dynamic Row
        $(document).on('click', '.remove-row', function(){
            $(this).closest("tr").remove();
            setSerialNumber();
            showHideDeleteBtn();
        })

        // Add Dynamic Row
        $('.add-new-row').on('click', function ()
        {
            addDynamicValidation(index, 'user_info', 'Org Name / P / O Number cannot be blank.');
            addDynamicValidation(index, 'amount', 'Amount cannot be blank.');

            var rowHtml = prepareRow(index);
            $('.dynamic-panel-container tbody').append(rowHtml);
            index++;

            setSerialNumber();
            showHideDeleteBtn();

            // Clone Options Ship Name
            $('#hidden-dropdown-field-ship-id option').clone().appendTo('#sampleitem-'+(index-1)+'-ship_id');
        })

    })

    function prepareRow(index) {
        var html = '';

        html += '<tr>';

        html += '<td><span class="serial-number">'+(index + 1)+'</span></td>';

        html += '<td>' +
            '<input id="sampleitem-'+index+'-user_id" class="field-user-id" id="auto-complete-user-id" name="sampleitem['+index+'][user_id]" type="hidden">' +
            '<div class="form-group field-sampleitem-'+index+'-user_info required">' +
            '<input id="sampleitem-'+index+'-user_info" placeholder="Search" class="form-control dynamic-field-validation field-user-info field-search-auto-complete" name="sampleitem['+index+'][user_info]" type="text">' +
            '<i class="fa fa-search"></i>' +
            '<div class="help-block help-block-error "></div>' +
            '</div>' +
            '</td>';

        html += '<td>' +
            '<div class="form-group field-sampleitem-'+index+'-sample_amount required">' +
            '<input id="sampleitem-'+index+'-sample_amount" class="form-control dynamic-field-validation field-sample-amount" name="sampleitem['+index+'][sample_amount]" type="text">' +
            '<div class="help-block help-block-error "></div>' +
            '</div>' +
            '</td>';

        html += '<td'+advanceFieldStyle+'>' +
            '<div class="form-group field-sampleitem-'+index+'-advance_receive required">' +
            '<input id="sampleitem-'+index+'-advance_receive" class="form-control dynamic-field-validation field-advance-amount" name="sampleitem['+index+'][advance_receive]" type="text">' +
            '<div class="help-block help-block-error "></div>' +
            '</div>' +
            '</td>';

        html += '<td'+advanceFieldStyle+'>' +
            '<div class="form-group field-sampleitem-'+index+'-balance required">' +
            '<input name="balance" class="form-control field-balance-amount" readonly="" type="text">' +
            '<div class="help-block help-block-error "></div>' +
            '</div>' +
            '</td>';

        html += '<td>' +
            '<div class="form-group field-sampleitem-'+index+'-remarks">' +
            '<input id="sampleitem-'+index+'-remarks" class="form-control field-remarks" name="sampleitem['+index+'][remarks]" type="text">' +
            '<div class="help-block help-block-error "></div>' +
            '</div>' +
            '</td>';

        html += '<td class="field-file-upload">' +
            '<div class="form-group field-sampleitem-'+index+'-attachment_file">' +
            '<input name="sampleitem['+index+'][attachment_file]" value="" type="hidden">' +
            '<input id="sampleitem-'+index+'-attachment_file" class="field-attachment-file" name="sampleitem['+index+'][attachment_file]" type="file">' +
            '<div class="help-block help-block-error "></div>' +
            '</div>' +
            '<a href="javascript:void(0)" class="file-clear-btn">Clear File</a>' +
            '</td>';

        html += '<td>' +
            '<a href="javascript:void(0)" class="remove-row btn btn-danger"><i class="fa fa-times"></i> Delete</a>' +
            '</td>';

        html += '</tr>';

        return html;
    }

    function addDynamicValidation(index, fieldName, errorMessage){
        $('#formName').yiiActiveForm('add', {
            id: 'sampleitem-'+index+'-'+fieldName,
            name: '['+index+']'+fieldName,
            container: '.field-sampleitem-'+index+'-'+fieldName,
            input: '#sampleitem-'+index+'-'+fieldName,
            error: '.help-block.help-block-error',
            validate:  function (attribute, value, messages, deferred, $form) {
                yii.validation.required(value, messages, {message: errorMessage});
            }
        });
    }

    function setSerialNumber() {
        var count = 1;
        $(".serial-number").each(function( index, element ) {
            $(this).text(count);
            count++;
        });
    }

    function showHideDeleteBtn() {
        if($(".serial-number").length == 1){
            $('.remove-row').hide();
        }
        else{
            $('.remove-row').show();
        }

    }
</script>
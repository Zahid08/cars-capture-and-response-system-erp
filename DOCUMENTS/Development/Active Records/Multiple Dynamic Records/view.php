<div class="row" id="sampleItems">
    <div class="dynamic-options-panel">
        <div class="dynamic-panel-head">
            <h4>Sample Details</h4>
        </div>
        <div class="dynamic-panel-container">
            <table class="table dynamic-panel-table-view" id="sampleTable">
                <thead>
                    <tr>
                        <th>Serial No</th>
                        <th>Budget Code</th>
                        <th>Budget Amount (Tk)</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $items = $model->compilationItems;
                foreach ($items as $k => $value) :
                    ?>
                    <tr>
                        <td><?= ($k + 1)?></td>
                        <td><?= $value->budget_code_id; ?></td>
                        <td><?= $value->amount; ?></td>
                    </tr>
                <?php endforeach;?>
                </tbody>
                <tfoot>
                    <tr class="footer-total footer-total-sample">
                        <td>&nbsp;</td>
                        <td class="total">Total:</td>
                        <td id="total-sample-amount">100</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
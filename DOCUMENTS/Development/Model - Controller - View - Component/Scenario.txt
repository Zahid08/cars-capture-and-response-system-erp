# Constant

const SCENARIO_LOGIN = 'login';
const SCENARIO_REGISTER = 'register';


# Rules

public function rules()
{
    return [
        // username, email and password are all required in "register" scenario
        [['username', 'email', 'password'], 'required', 'on' => self::SCENARIO_REGISTER],

        // username and password are required in "login" scenario
        [['username', 'password'], 'required', 'on' => self::SCENARIO_LOGIN],
    ];
}

# CallBack Function

return ArrayHelper::map($listData, 'id', function($model, $defaultValue) {
            $title = '';
            if ($model['title']){
                $title = ' (' . $model['title'] . ')';
            }
            return $model['id'] . $title;
        });

#

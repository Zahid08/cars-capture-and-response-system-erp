<div class="admin-grid-toolbar">
   <?= NewButton::widget(['url' => Url::toRoute(['create'])]) ?>

   <?= EditButton::widget(['url' => Url::toRoute(['update', 'id' => ''])]) ?>

   <?= DeleteButton::widget(['url' => Url::toRoute(['mass-delete'])]); ?>

   <?= VerticalDivider::widget(); ?>

   <?= ActiveButton::widget(['url' => Url::toRoute(['mass-active'])]) ?>

   <?= InactiveButton::widget(['url' => Url::toRoute(['mass-inactive'])]) ?>

   <?= VerticalDivider::widget(); ?>

   <?= ExportCsvButton::widget(['url' => Url::toRoute(['export-csv'])]) ?>

   <?= ExportPdfButton::widget(['url' => Url::toRoute(['export-pdf'])]) ?>

   <?= VerticalDivider::widget(); ?>

   <?= ResetFilterButton::widget(['url' => Url::toRoute(['index'])]) ?>

   <?= AdvancedSearchButton::widget() ?>
</div>

# Format

Path: vendor/yiisoft/yii2/i18n/Formatter.php

Example 1:

'url:url' call method = asUrl

Example 2:

'details:ntext' Call Methode: asNtext

Example 3:

3.
[
   'attribute' => 'date_end',
   'format' => ['date', 'php:d/m/Y']
 ],

Example 4:

4.
[
    'attribute' => 'appointment_date',
    'format'=>['DateTime', 'php:d-m-Y H:i:s']
],

Example 5:
Yii::$app->formatter->format->asDate('2014-01-01', 'long');

Example 6:
Yii::$app->formatter->format('2014-01-01', 'date');

Example 7:
Yii::$app->formatter->format(0.125, ['percent', 2]);







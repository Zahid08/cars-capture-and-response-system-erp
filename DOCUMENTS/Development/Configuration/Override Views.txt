﻿1.


'components' => [
   'view' => [
     'theme' => [
       'pathMap' => [
         '@vendor/modules/user/views' => '@app/themes/user/views'
       ],
     ],
   ],
 ],




2.


<?php return [
   ...
   'components' => [
       'view' => [
           'theme' => [
               'pathMap' => [
                   '@dektrium/user/views' => '@app/views/user'
               ],
           ],
       ],
   ],
...
];


3.


Inside config, add your view:
'components' => [
   ...
   'view'=> [
       'theme' => [
           'pathMap' => [
               '@common/extensions/my_widget/views' => [
                   '@myapplication/views/widgets/my_widget/views', // Override
                   '@common/extensions/my_widget/views', // Default
               ],
           ],
       ],
   ]
],
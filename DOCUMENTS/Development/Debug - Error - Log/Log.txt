﻿# Reference
 http://www.yiiframework.com/doc-2.0/guide-runtime-logging.html






# LOG
$message - The log message to be recorded
$category - The category of the log message

# String
 Yii::log($message, $level, $category);
 Example:
 Yii::log($what, 'info', 'application');

# Array
 Yii::trace(VarDumper::dumpAsString($array));

# Yii::error() - this is for fatal error messages.  
 Yii::error('error log message');
 Yii::log('error', CLogger::LEVEL_ERROR, 'example');

# Yii::warning() - this is for warning messages.  
 Yii::warning('warning log message');
 Yii::log('warning', CLogger::LEVEL_WARNING, 'example');

# Yii::info() - this is for logging general information.
 Yii::info('info log message');
 Yii::log('info', CLogger::LEVEL_INFO, 'example');

# Yii::trace() - Records a message to trace how a piece of code runs.  
 Yii::trace('trace log message');
 Yii::log('trace', CLogger::LEVEL_TRACE, 'example');
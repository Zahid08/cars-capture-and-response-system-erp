# Checkbox Button
<?= $form->field($model, 'population')->checkbox(); ?>

# Checkbox Button Without Label
<?= $form->field($model, 'population')->checkbox(array('label'=>'')); ?>

# Checkbox Button Custom Label
<?= $form->field($model, 'population')	->checkbox(array('label'=>''))->label('Gender'); ?>

# Checkbox Button Without Label Options
<?= $form->field($model, 'population')->checkbox(array(
								'label'=>'',
								'labelOptions'=>array('style'=>'padding:5px;'),
								'disabled'=>true
								))
								->label('Gender'); ?>

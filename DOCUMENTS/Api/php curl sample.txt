<?php
function httpRequest($method, $url, $accessToken, $vars = array()){

    if (!function_exists('curl_init')){
        die('Sorry cURL is not installed!');
    }

    if (is_array($vars)) $vars = http_build_query($vars, '', '&');

    if($method != 'POST'){
        $url = $url . '?' . $vars;
    }

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

    if($method == 'POST'){
        curl_setopt($ch, CURLOPT_POST, true);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
    }

    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . $accessToken));

    curl_setopt($ch, CURLOPT_VERBOSE, true);

    curl_setopt($ch, CURLOPT_HEADER, true);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);

    $response = curl_exec($ch);

    curl_close($ch);

    return $response;
}

function get_headers_from_curl_response($response)
{
    $headers = [];

    if (strpos($response, "\r\n\r\n") > 0){
        list($header, $body) = explode("\r\n\r\n", $response);

        $headerTextArray = explode("\r\n", $header);

        foreach ($headerTextArray as $i => $line) {
            if ($i === 0) {
                $headers['http_code'] = $line;
            }
            else {
                list ($key, $value) = explode(': ', $line);
                $headers[$key] = $value;
            }
        }

        return $headers;
    }
    else {
        return false;
    }

}

function get_body_from_curl_response($response)
{
    if (strpos($response, "\r\n\r\n") > 0){
        list($header, $body) = explode("\r\n\r\n", $response);
    }
    else {
        $body = $response;
    }

    return json_decode($body);
}

$method = 'GET';
$url = 'http://localhost/gso/api/web/v1/service-cdc/search';
$access_token = '7Cg9EAJh7RalxFzOc5IrC4py5H9aZ0oV';
$vars = [
    'name' => 'QA',
];

$response = httpRequest($method, $url, $access_token, $vars);

$header = get_headers_from_curl_response($response);
$body = get_body_from_curl_response($response);

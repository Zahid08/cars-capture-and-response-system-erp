# Override checkAccess in rest ActiveController

public function checkAccess($action, $model = null, $params = [])
{
    if ($action === 'view' or $action === 'update' or $action === 'delete')
    {
        if ( Yii::$app->user->can('supplier') === false
             or Yii::$app->user->identity->supplierID === null
             or $model->supplierID !== \Yii::$app->user->identity->supplierID )
        {
             throw new \yii\web\ForbiddenHttpException('You can\'t '.$action.' this product.');
        }

    }
}

# Performing Access Check

When exposing resources through RESTful APIs, you often need to check if the current user has the permission to access
and manipulate the requested resource(s). With yii\rest\ActiveController, this can be done by overriding the checkAccess()
 method like the following,

public function checkAccess($action, $model = null, $params = [])
{
    // check if the user can access $action and $model
    // throw ForbiddenHttpException if access should be denied
    if ($action === 'update' || $action === 'delete') {
        if ($model->author_id !== \Yii::$app->user->id)
            throw new \yii\web\ForbiddenHttpException(sprintf('You can only %s articles that you\'ve created.', $action));
    }
}

Tip: You may implement checkAccess() by using the Role-Based Access Control (RBAC) component.



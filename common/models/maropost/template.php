<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12/31/2018
 * Time: 5:11 PM
 */

use common\models\maropost\maropost;



class template extends maropost
{
    public  function create_template(array $template){
        return $this->request( "POST", 'contents', array("content" =>$template["content"]) );
    }
    public  function get_template_content(){
        return $this->request( "GET", "contents"."/all_contents", null );
    }
    function  put_template_content( array $template ) {
        return $this->request( "PUT", 'contents/'.$template['id'], array("content" =>$template["content"]));
    }
}
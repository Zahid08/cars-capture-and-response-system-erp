<?php
namespace common\models;

class Autoload{
  public function __construct()
  {
      require_once(__DIR__.'/PHPImageWorkshop/Exception/ImageWorkshopBaseException.php');
      require_once(__DIR__.'/PHPImageWorkshop/Exception/ImageWorkshopException.php');
      require_once(__DIR__.'/PHPImageWorkshop/Exif/ExifOrientations.php');
      require_once(__DIR__.'/PHPImageWorkshop/Core/Exception/ImageWorkshopLayerException.php');
      require_once(__DIR__.'/PHPImageWorkshop/Core/ImageWorkshopLib.php');
      require_once(__DIR__.'/PHPImageWorkshop/Core/ImageWorkshopLayer.php');
      require_once(__DIR__.'/PHPImageWorkshop/ImageWorkshop.php');

  }
}



<?php

namespace common\models\PHPImageWorkshop\Core\Exception;

use common\models\PHPImageWorkshop\Exception\ImageWorkshopBaseException as ImageWorkshopBaseException;

/**
 * ImageWorkshopLayerException
 *
 * Manage ImageWorkshopLayer exceptions
 *
 * @link http://phpimageworkshop.com
 * @author Sybio (Clément Guillemain  / @Sybio01)
 * @license http://en.wikipedia.org/wiki/MIT_License
 * @copyright Clément Guillemain
 */
class ImageWorkshopLayerException extends ImageWorkshopBaseException
{
}

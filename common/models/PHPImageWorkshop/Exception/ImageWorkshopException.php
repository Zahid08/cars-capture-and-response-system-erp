<?php

namespace common\models\PHPImageWorkshop\Exception;

use common\models\PHPImageWorkshop\Exception\ImageWorkshopBaseException as ImageWorkshopBaseException;

/**
 * ImageWorkshopException
 *
 * Manage ImageWorkshop exceptions
 *
 * @link http://phpimageworkshop.com
 * @author Sybio (Clément Guillemain  / @Sybio01)
 * @license http://en.wikipedia.org/wiki/MIT_License
 * @copyright Clément Guillemain
 */
class ImageWorkshopException extends ImageWorkshopBaseException
{
}

<?php

namespace unlock\modules\ajax\controllers;

use unlock\modules\contacts\Contacts;
use unlock\modules\developers\models\Developers;
use unlock\modules\developers\models\DevelopersContacts;
use Yii;
use unlock\modules\usermanager\role\models\Role;
use unlock\modules\core\helpers\CommonHelper;

class AjaxController extends \yii\web\Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    // url: ajax/ajax/load-role-by-user-group
    public function actionLoadRoleByUserGroup()
    {
        $post = Yii::$app->request->post();

        $userGroup = $post['depdrop_all_params']['dep_drop_user_group'];

        $list = Role::rolesDropDownList($userGroup);

        $data = [];
        foreach ($list as $key => $value) {
            $data[] = ['id' => $key, 'name' => $value];
        }

        echo json_encode(['output' => $data, 'selected' => '']);
    }

    public function actionContactNameList() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $search = Yii::$app->request->post('search');
        $data = [];
        if (!is_null($search)) {
            $listData = \unlock\modules\contacts\models\Contacts::find()
                ->where(['like', 'LOWER(first_name)', strtolower($search)])
                ->orWhere(['like', 'LOWER(last_name)', strtolower($search)])
                ->limit(50)
                ->all();

            $count = 0;
            foreach ($listData as $key => $item) {
                $data[$count]['id'] = $item->id;
                $data[$count]['value'] =$item['first_name'].' '.$item['last_name'].' (ID-'.$item->id.')';
                $count++;
            }
        }
        return $data;
        // return $out;
    }

    public function actionDeveloperNameList() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $search = Yii::$app->request->post('search');
        $data = [];
        if (!is_null($search)) {
            $listData = Developers::find()
                ->where(['like', 'LOWER(developer_name)', strtolower($search)])
                ->limit(50)
                ->all();

            $count = 0;
            foreach ($listData as $key => $item) {
                $data[$count]['id'] = $item->id;
                $data[$count]['value'] =$item['developer_name'].' (ID-'.$item->id.')';
                $count++;
            }
        }
        return $data;
        // return $out;
    }

}




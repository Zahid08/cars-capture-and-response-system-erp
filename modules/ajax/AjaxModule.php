<?php

namespace unlock\modules\ajax;

/**
 * Class AjaxModule
 */
class AjaxModule extends \yii\base\Module
{
    /**
     * @var string the namespace that controller classes are in
     */
    public $controllerNamespace = 'unlock\modules\ajax\controllers';

    /**
    * @var string the namespace that view file are in
    */
    public $viewNamespace = '@unlock/modules/ajax/views/';
}

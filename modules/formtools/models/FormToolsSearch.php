<?php

namespace unlock\modules\formtools\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use unlock\modules\formtools\models\Formtools;

/**
 * FormtoolsSearch represents the model behind the search form of `unlock\modules\formtools\models\Formtools`.
 */
class FormtoolsSearch extends Formtools
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'form_type_id', 'media_source_id', 'development_id', 'tag_id', 'redirect_status', 'created_by', 'updated_by'], 'integer'],
            [['subject', 'success_message', 'fail_message', 'redirect_link', 'privacy_link', 'publish_date', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Formtools::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'form_type_id' => $this->form_type_id,
            'media_source_id' => $this->media_source_id,
            'development_id' => $this->development_id,
            'tag_id' => $this->tag_id,
            'redirect_status' => $this->redirect_status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'subject', $this->subject])
            ->andFilterWhere(['like', 'success_message', $this->success_message])
            ->andFilterWhere(['like', 'fail_message', $this->fail_message])
            ->andFilterWhere(['like', 'redirect_link', $this->redirect_link])
            ->andFilterWhere(['like', 'privacy_link', $this->privacy_link])
            ->andFilterWhere(['like', 'publish_date', $this->publish_date])
            ->orderBy(['created_at' => SORT_DESC]);

        return $dataProvider;
    }
}

<?php

namespace unlock\modules\formtools\models;

use unlock\modules\core\helpers\CommonHelper;
use Yii;

/**
 * This is the model class for table "{{%form_tools_event_realtion}}".
 *
 * @property int $id
 * @property string $event_start_time
 * @property string $event_end_time
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class FormtoolsEventRealtion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%form_tools_event_realtion}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at','event_id'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['event_start_time', 'event_end_time', 'time_slote_start','time_slote_end'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'event_id' => Yii::t('app', 'Event  ID'),
            'event_start_time' => Yii::t('app', 'Event Start Time'),
            'event_end_time' => Yii::t('app', 'Event End Time'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'time_slote_start' => Yii::t('app', 'Time Slote Start'),
            'time_slote_end' => Yii::t('app', 'Time Slote Start'),
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }
}

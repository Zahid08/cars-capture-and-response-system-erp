<?php

namespace unlock\modules\formtools\models;

use unlock\modules\core\helpers\CommonHelper;
use Yii;

/**
 * This is the model class for table "{{%form_tools_style}}".
 *
 * @property int $id
 * @property int $form_id
 * @property string $back_color
 * @property string $back_height
 * @property string $back_width
 * @property string $btn_color
 * @property string $btn_height
 * @property string $btn_width
 * @property string $label_color
 * @property string $label_font_size
 * @property string $checkbox_color
 * @property string $checkbox_font_size
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class FormtoolsStyle extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%form_tools_style}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['form_id', 'created_by', 'updated_by'], 'safe'],
            [['created_at', 'updated_at'], 'safe'],
            [['updated_at'], 'required'],
            [['back_color','btn_color', 'label_color', 'label_font_size','checkbox_color', 'checkbox_font_size'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'form_id' => Yii::t('app', 'Form ID'),
            'back_color' => Yii::t('app', 'Background Color'),
            'btn_color' => Yii::t('app', 'Button Color'),
            'label_color' => Yii::t('app', 'Label Text Color'),
            'label_font_size' => Yii::t('app', 'Label Text Font Size'),
            'checkbox_color' => Yii::t('app', 'Checkbox Text Color'),
            'checkbox_font_size' => Yii::t('app', 'Checkbox Font Size'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }

}

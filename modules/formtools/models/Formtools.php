<?php

namespace unlock\modules\formtools\models;

use unlock\modules\core\helpers\CommonHelper;
use Yii;

/**
 * This is the model class for table "{{%form_tools}}".
 *
 * @property int $id
 * @property string $subject
 * @property int $form_type_id
 * @property int $media_source_id
 * @property int $development_id
 * @property int $tag_id
 * @property string $success_message
 * @property string $fail_message
 * @property string $redirect_link
 * @property int $redirect_status
 * @property string $privacy_link
 * @property string $publish_date
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class FormTools extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%form_tools}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['form_type_id', 'media_source_id', 'development_id', 'tag_id', 'redirect_status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at','status','brand_id','comments','validate_phone_status'], 'safe'],
            [['subject'], 'string', 'max' => 200],
            [['success_message', 'fail_message'], 'string', 'max' => 70],
            [['redirect_link', 'privacy_link', 'publish_date'], 'string'],

            [['form_type_id'], 'required'],
            [['brand_id'], 'required'],
           /* [['development_id'], 'required'],*/
            [['media_source_id'], 'required'],
            /*[['tag_id'], 'required'],*/
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'subject' => Yii::t('app', 'Form Title'),
            'form_type_id' => Yii::t('app', 'Form Type'),
            'media_source_id' => Yii::t('app', 'Default Media Source'),
            'comments' => Yii::t('app', 'Comments'),
            'development_id' => Yii::t('app', 'Development Name'),
            'brand_id' => Yii::t('app', 'Brand Name'),
            'tag_id' => Yii::t('app', 'Tag Name'),
            'success_message' => Yii::t('app', 'Success Message'),
            'fail_message' => Yii::t('app', 'Fail Message'),
            'redirect_link' => Yii::t('app', 'Redirect Link'),
            'redirect_status' => Yii::t('app', 'Redirect Status'),
            'privacy_link' => Yii::t('app', 'Privacy Link'),
            'publish_date' => Yii::t('app', 'Publish Date'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'status' => Yii::t('app', 'Status'),
            'validate_phone_status' => Yii::t('app', 'Validate Phone'),
            'action' => Yii::t('app', 'Action'),
        ];
    }

    public function  getFormtoolsEvent(){
        return $this->hasMany(FormtoolsEvent::className(), ['form_id' => 'id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }
}

<?php

namespace unlock\modules\formtools;

/**
 * formtools module definition class
 */
class Formtools extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'unlock\modules\formtools\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}

<?php

namespace unlock\modules\formtools\controllers;

use api\common\maropost\tags;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\formtools\models\FormtoolsEvent;
use unlock\modules\formtools\models\FormtoolsEventRealtion;
use unlock\modules\formtools\models\FormtoolsStyle;
use Yii;
use unlock\modules\formtools\models\Formtools;
use unlock\modules\formtools\models\FormtoolsSearch;
use yii\base\Model;
use yii\db\Exception;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FormtoolsController implements the CRUD actions for Formtools model.
 */
class FormtoolsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => Yii::$app->DynamicAccessRules->customAccess('formtools/formtools'),
        ];
    }

    /**
     * Lists all Formtools models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FormtoolsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Formtools model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $formToolsStyle = FormtoolsStyle::find()->where(['form_id' => $id])->one();

        $html = $this->renderAjax('view', [
            'model' => $model,
            'formToolsStyle' => $formToolsStyle,
        ]);
        $filename = "Portable Form"." ".$model->publish_date.".html";
        header('Content-disposition: attachment; filename=' . $filename);
        header('Content-type: text/html');
        return $html;
    }

    public function actionPreView($id)
    {
        $this->layout = false;
        $model = $this->findModel($id);
        $formToolsStyle = FormtoolsStyle::find()->where(['form_id' => $id])->one();
        return $this->render('view', [
            'model' => $model,
            'formToolsStyle' => $formToolsStyle,
        ]);

    }

    /**
     * Creates a new Formtools model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Formtools();
        $formToolsStyle = new FormtoolsStyle();
        $formToolsEventSection = [new FormtoolsEvent()];
        $formToolsEventRealation = [new FormtoolsEventRealtion()];

        //Get event date
        $hasFormtoolsEvent = true;
        $formToolsItemArray = Yii::$app->request->post('FormtoolsEvent');
        if($formToolsItemArray){
            $formToolsEventSection = [];
            foreach ($formToolsItemArray as $key => $item) {
                $formToolsEventSection[$key] = new FormtoolsEvent();
            }
        }
        Model::loadMultiple($formToolsEventSection, Yii::$app->request->post());

        //Get event time
        $formToolsEventRealationItemArray = Yii::$app->request->post('FormtoolsEventRealtion');

        if ($model->load(Yii::$app->request->post()) && $formToolsStyle->load(Yii::$app->request->post()) && Model::loadMultiple($formToolsEventSection, Yii::$app->request->post())) {
            $this->saveData($model, $formToolsStyle, $formToolsEventSection, $formToolsEventRealationItemArray);
        }
        return $this->render('_form', [
            'model' => $model,
            'formToolsStyle' => $formToolsStyle,
            'formToolsEventSection' => $formToolsEventSection,
            'formToolsEventRealation' => $formToolsEventRealation,
        ]);
    }

    /**
     * Updates an existing Formtools model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $formToolsEventRealation=[];
        $model = $this->findModel($id);
        $formToolsStyle = FormtoolsStyle::find()->where(['form_id' => $id])->one();

        // Foorm Tools Event Items Update
        $formToolsEventSection = $model->formtoolsEvent;
        if (empty($formToolsEventSection)) {
            $formToolsEventSection = [new FormtoolsEvent()];
        }
        $formToolsEventSectionArray = Yii::$app->request->post('FormtoolsEvent');

        if($formToolsEventSectionArray){
            $formToolsEventSection = [];
            foreach ($formToolsEventSectionArray as $key => $item) {
                $formToolsEventSection[$key] = $this->findFormToolsEventModel($item);
            }
        }

        $formToolsEventRealationItemArray = Yii::$app->request->post('FormtoolsEventRealtion');
        $eventModel=FormtoolsEvent::find()->where(['form_id' => $id])->all();
        foreach ($eventModel as $eventdata) {
            $formToolsEventRealation[] = FormtoolsEventRealtion::find()->where(['event_id' => $eventdata->id])->all();
        }

        if ($model->load(Yii::$app->request->post()) && $formToolsStyle->load(Yii::$app->request->post()) && Model::loadMultiple($formToolsEventSection, Yii::$app->request->post()) ) {
            $this->saveData($model, $formToolsStyle, $formToolsEventSection, $formToolsEventRealationItemArray);
        }
        return $this->render('_formupdate', [
            'model' => $model,
            'formToolsStyle' => $formToolsStyle,
            'formToolsEventSection' => $formToolsEventSection,
            'formToolsEventRealation' => $formToolsEventRealation,
        ]);
    }

    function saveData($model, $formToolsStyle, $formToolsEventSection, $formToolsEventRealation){
        if (isset($model) && isset($formToolsStyle) && isset($formToolsEventSection) && isset($formToolsEventRealation) ){
            try {
                if ($model->save()){
                    foreach ($formToolsEventSection as $index_key=> $formToolsEventSectionItem){

                        if(array_key_exists('id',Yii::$app->request->post('FormtoolsEvent')[$index_key])){
                            $formToolsEventSectionItem = FormtoolsEvent::findOne(Yii::$app->request->post('FormtoolsEvent')[$index_key]['id']);
                            if($formToolsEventSectionItem){
                                //CommonHelper::changeDateFormat($model->created_at, 'd M Y');
                                $event_date=CommonHelper::changeDateFormat(Yii::$app->request->post('FormtoolsEvent')[$index_key]['event_date'],'Y-m-d H:i:s');
                                $formToolsEventSectionItem->event_date =$event_date;
                            }
                        }else{
                            $formToolsEventSectionItem->form_id = $model->id;
                            $event_date=CommonHelper::changeDateFormat(Yii::$app->request->post('FormtoolsEvent')[$index_key]['event_date'],'Y-m-d H:i:s');
                            $formToolsEventSectionItem->event_date =$event_date;
                        }

                        if ($formToolsEventSectionItem->save()){
                            if(array_key_exists($index_key,$formToolsEventRealation)){
                                foreach (  $formToolsEventRealation[$index_key] as $evKey=>$evdata){
                                    if(empty($evdata['event_id'])){
                                        $eventRelation=new FormtoolsEventRealtion();
                                        $eventRelation->event_id = $formToolsEventSectionItem->id;
                                    }
                                    else
                                        $eventRelation= FormtoolsEventRealtion::findOne($evdata['event_id']);
                                        $eventRelation->event_start_time = $evdata['event_start_time'];
                                        $eventRelation->time_slote_end = $evdata['time_slote_end'];
                                        $eventRelation->time_slote_start = $evdata['time_slote_start'];
                                        $eventRelation->event_end_time = $evdata['event_end_time'];
                                    if (!$eventRelation->save()) {
                                        throw new Exception(Yii::t('app', Html::errorSummary($eventRelation)));
                                    }
                                   /* echo "<pre>";print_r($eventRelation);exit();*/
                                }
                            }
                        }
                    }

                    $formToolsStyle->form_id = $model->id;
                    $formToolsStyle->updated_at = date('Y-m-d H:i:s');
                    if (!$formToolsStyle->save()) {
                        throw new Exception(Yii::t('app', Html::errorSummary($formToolsStyle)));
                    }
                }
                return $this->redirect(['update', 'id' => $model->id]);
            }
            catch (Exception $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
    }

    function findFormToolsEventRelationModel($item){
        if(array_key_exists('footerid', $item)){
            if (($model = FormtoolsEventRealtion::findOne($item['footerid'])) !== null) {
                return $model;
            }
            else {
                return new FormtoolsEventRealtion();
            }
        }
        else{
            return new FormtoolsEventRealtion();
        }
    }

    function findFormToolsEventModel($item){
        if(array_key_exists('footerid', $item)){
            if (($model = FormtoolsEvent::findOne($item['footerid'])) !== null) {
                return $model;
            }
            else {
                return new FormtoolsEvent();
            }
        }
        else{
            return new FormtoolsEvent();
        }
    }

    /**
     * Deletes an existing Formtools model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDeleteEventItem($id){
        $model = FormtoolsEventRealtion::findOne($id);
        try {
            if (!$model->delete()) {
                throw new Exception(Yii::t('app', 'The record cannot be deleted.'));
            }
            Yii::$app->session->setFlash('success', Yii::t('app', 'The record have been successfully deleted.'));
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            if($e->getName() == CommonHelper::INTEGRITY_CONSTRAINT_VIOLATION){
                $message = "This record has relational data. You must delete the following data first";
                $message .= "<ul>";
                $message .= "<li>Example => Example List</li>";
                $message .= "</ul>";
            }
            Yii::$app->session->setFlash('error', $message);
        }
        return $this->redirect(['formtools/update?id='.$_REQUEST['form_tools_id']]);
    }

    public function actionSubmitMaropostTags(){
        $tags=new tags();
        //tag created if tag not exist in maropost
        if (isset($_REQUEST['tags_array'])) {
            $tags_array = $_REQUEST['tags_array'];
            if (!empty($tags_array)) {
                foreach ($tags_array as $dataitem) {
                    $array = array(
                        "tags" => array(
                            "name" =>$dataitem
                        )
                    );
                    $response=$tags->post_tags($array);
                }
            }
        }
       // print_r($_REQUEST['tags_array']);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Formtools model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Formtools the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Formtools::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}

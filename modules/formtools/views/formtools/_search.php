<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model unlock\modules\formtools\models\FormtoolsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="formtools-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'subject') ?>

    <?= $form->field($model, 'form_type_id') ?>

    <?= $form->field($model, 'media_source_id') ?>

    <?= $form->field($model, 'development_id') ?>

    <?php // echo $form->field($model, 'tag_id') ?>

    <?php // echo $form->field($model, 'success_message') ?>

    <?php // echo $form->field($model, 'fail_message') ?>

    <?php // echo $form->field($model, 'redirect_link') ?>

    <?php // echo $form->field($model, 'redirect_status') ?>

    <?php // echo $form->field($model, 'privacy_link') ?>

    <?php // echo $form->field($model, 'publish_date') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

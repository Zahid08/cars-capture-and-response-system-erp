<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $model unlock\modules\formtools\models\Formtools */

$form_type=CommonHelper::formTypeDropDownList($model->form_type_id);
$utm_source=CommonHelper::leadSourceByBrandId($model->media_source_id);
$development_id=$model->development_id;
$development=CommonHelper::developmentListById($development_id);
$tagModel=CommonHelper::contactIdByTagsNew($model->tag_id);
$getEventModel=CommonHelper::eventDateByFormId($model->id);
if (!empty($getEventModel)) {
    $event_js_array = json_encode($getEventModel);
}else{
    $event_js_array="{}";
}

$brand_id=$model->brand_id;
$development_name   =   !empty($development->development_name) ? $development->development_name : '';
$affiliate_tag      =   !empty($tagModel->tag_name) ? $tagModel->tag_name : '';

$success_message    =str_replace('"', ' ', $model->success_message);
$fail_message       =str_replace('"', ' ', $model->fail_message);
$redirect_url       =str_replace('"', ' ', $model->redirect_link);
$privacy_link       =str_replace('"', ' ', $model->privacy_link);
$comments           =str_replace('"', ' ', $model->comments);
$validation_status  =$model->validate_phone_status;
$redirect_status    =$model->redirect_status;
//style
$background_color   =$formToolsStyle->back_color;

$button_backcolor   =$formToolsStyle->btn_color;
$checkbox_color     =$formToolsStyle->checkbox_color;
$checkbox_font_size =$formToolsStyle->checkbox_font_size;

$label_text_color   =$formToolsStyle->label_color;
$label_font_size    =$formToolsStyle->label_font_size;

$version=time();
$baseUrl = Yii::getAlias('@baseUrl');


?>

<script>
    var form_type            	="<?php echo $form_type;?>"; //Required - Subscribe/Contact/Registration/RSVP/Appointment/Reservation/Survey
    var brand_id                ="<?php echo $brand_id;?>";//Must be a positive integer
    var utm_source              ="<?php echo $utm_source;?>"; //Required
    var development_id          ="<?php echo $development_id;?>";//Must be a positive integer
    var development_name        ="<?php echo $development_name;?>";
    var affiliate_tag           ="<?php echo $affiliate_tag;?>";
    var success_message         ="<?php echo $success_message;?>";
    var fail_message            ="<?php echo $fail_message;?>";
    var error_message           ="Your submission has some errors";
    var fail_notice_email       ="Your submission was not successful, error code";
    var redirect_url            ="<?php echo $redirect_url;?>";
    var link_to_privacy_policy  ="<?php echo $privacy_link;?>";
    var redirect				=<?php echo $redirect_status;?>; //0 or 1 //1 for redirect
    var comments                ="<?php echo $comments;?>";
    var eventsinfo              =<?php echo $event_js_array;?>;
    var validatephone           =<?php echo $validation_status;?>;  //0 for not validate 1 for validate
</script>

<div id="portable-form"></div>
<script src="<?php echo $baseUrl;?>/backend/web/themes/connecterp/js/cars-portableform-embed.js?tp=<?php echo $version;?>" type="text/javascript"></script>

<style>
    .portable-hmtl-row{
        background-color:<?php echo $background_color; ?>!important;
    }
    .portable-hmtl-btn{
        background-color:<?php echo $button_backcolor; ?> !important;
        border: solid 1px <?php echo $button_backcolor; ?> !important;
    }
    .portable-hmtl-label{
        color:<?php echo $label_text_color?>!important;
        font-size:<?php echo $label_font_size.'px';?> !important;
    }
    .check-box-label{
        color:<?php echo $checkbox_color?>!important;
        font-size:<?php echo $checkbox_font_size.'px';?> !important;
    }
</style>
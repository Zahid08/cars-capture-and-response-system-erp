<?php
use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\grid\GridView;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\buttons\NewButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $searchModel unlock\modules\formtools\models\FormtoolsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Formtools');
?>

<div class="main-container" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-grid-filter">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <div class="admin-grid-content">
        <div class="admin-grid-toolbar">
            <div class="admin-grid-toolbar-left">
                <?= NewButton::widget(['url' => Url::toRoute(['create'])]) ?>
            </div>
            <div class="admin-grid-toolbar-right">
                <?= Summary::widget(['dataProvider' => $dataProvider]) ?>
            </div>
        </div>

        <div class="admin-grid-view">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
                'pager' => ['class' => LinkPager::className()],
                'columns' => [
                    [
                        'attribute' => 'id',
                        'format'    => 'html',
                        'headerOptions' => ['style' => 'color:#337ab7;'],
                        'value' => function($model) {
                            $subject = '<a href="'.Url::toRoute('formtools/update?id='.$model->id).'">'.$model->id.'</a>';
                            return $subject;
                        },
                    ],

                    [
                        'attribute' => 'subject',
                        'format'    => 'html',
                        'headerOptions' => ['style' => 'color:#337ab7;'],
                        'value' => function($model) {
                            $subject = '<a href="'.Url::toRoute('formtools/update?id='.$model->id).'">'.$model->subject.'</a>';
                            return $subject;
                        },
                    ],
                    [
                        'attribute' => 'form_type_id',
                        'format'    => 'html',
                        'headerOptions' => ['style' => 'color:#337ab7;'],
                        'value' => function($model) {
                            $subject =CommonHelper::formTypeDropDownList($model->form_type_id);
                            if ($subject=="Appointment" || $subject=="RSVP"){
                                $getEventModel=CommonHelper::getEventDateByFormID($model->id);
                                $count=count($getEventModel);
                                $event_date=$getEventModel[$count-1];
                                $current_date=date("m/d/Y");
                                if ($event_date<$current_date){
                                    return $subject."<span id='expired' class=\"blink_me\" style='color: red;font-size: 9px;'>Expired</span>";
                                }
                                else{
                                    return $subject;
                                }
                            }
                            else{
                                return $subject;
                            }
                        },
                    ],
                    [
                        'attribute' => 'development_id',
                        'format'    => 'html',
                        'headerOptions' => ['style' => 'color:#337ab7;'],
                        'value' => function($model) {
                            $development=CommonHelper::developmentListById($model->development_id);
                            if (!empty($development)){
                                $development_name = '<a href="'.Url::toRoute('/developments/developments/view?id='.$development->id).'">'.$development->development_name.'</a>';
                                return $development_name;
                            }
                        },
                    ],
                    'publish_date',
                    [
                        'attribute' => 'status',
                        'headerOptions' => ['style' => 'width:180px'],
                        'value' => function($model){
                            $status = CommonHelper::newsletterStatusTypeById($model->status);
                            return $status->status_name;
                        }
                    ],
                    [
                        'attribute' => 'action',
                        'format'    => 'raw',
                        'headerOptions' => ['style' => 'color:#337ab7;'],
                        'value' => function($model) {
                            $url = '<a title="View"  href="'.Url::toRoute('formtools/pre-view?id='.$model->id).'" target="_blank">'.'<i class=\'fa fa-eye\'></i>'.'</a><span><a title="View" href="'.Url::toRoute('formtools/view?id='.$model->id).'">'.'<i class=\'fa fa-download\'></i>'.'</a></span>';
                            return $url;
                        },
                    ],
                ],
            ]); ?>
        </div>
    </div>

</div>

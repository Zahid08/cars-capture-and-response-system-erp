<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtons;
use unlock\modules\core\buttons\BackButton;
use kartik\date\DatePicker;
use unlock\modules\developments\models\Developments;
use yii\helpers\ArrayHelper;
use kartik\color\ColorInput;

/* @var $this yii\web\View */
/* @var $model unlock\modules\formtools\models\Formtools */
/* @var $form yii\widgets\ActiveForm */
$this->title= ($model->isNewRecord) ? Yii::t('app', 'Create Formtools') : Yii::t('app', 'Update FormTools');
?>
<div class="main-container main-container-form" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a(Yii::t('app', 'Home'), Yii::$app->homeUrl) ?></li>
                <li><?= Html::a(Yii::t('app', 'Formtools'), Url::to('index')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>


    <div class="admin-form-container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <div class="pull-left">
                        <h3 class="panel-title"><i class="fas fa-pencil-alt"></i> <?= Html::encode($this->title) ?></h3>
                    </div>
                    <div class="pull-right">
                        <p style="margin:0px;font-size: 14px">Version 1.0</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-heading-button">
                    <?= BackButton::widget() ?>
                </div>
            </div>
            <div class="panel-body newsletter-wrapper">
                <?php $form = ActiveForm::begin([
                    'options' => ['enctype' => 'multipart/form-data'],
                ]); ?>
                <div class="row">
                    <div class="col-md-11">
                        <?= $form->field($model, 'subject')->textInput() ?>
                    </div>
                    <div class="col-md-1">
                        <?= Html::button('Copy',['id' => 'CopySubject', 'class' => 'btn btn-success']) ?>
                    </div>
                    <div class="clearfix"></div>
                    <?php if ($model->isNewRecord){ ?>
                    <div class="col-md-6">
                        <?php }else{ ?>
                        <div class="col-md-3">
                            <?php } ?>
                            <?= $form->field($model, 'publish_date')->widget(
                                DatePicker::className(), [
                                'name' => 'publish_date',
                                'value' => '12-31-2010',
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'yyyy-mm-dd'
                                ]
                            ]);?>
                        </div>
                        <?php if (!$model->isNewRecord){ ?>
                            <div class="col-md-3">
                                <?= $form->field($model, 'created_at')->textInput(['maxlength' => true,'readonly'=>true]) ?>
                            </div>
                        <?php }?>

                        <div class="col-md-6">
                            <?= $form->field($model, 'status')->dropDownList(CommonHelper::newsletterStatusTypeDropDownList()) ?>
                        </div>
                    </div>
                    <div class="divider"></div>
                    <div class="formtools-form-title">
                        <h3>COMMON FEATURE</h3>
                    </div>
                    <div class="row">
                        <div class="clearfix"></div>
                        <div class="col-md-4">
                            <?=
                            $form->field($model, 'form_type_id')->widget(Select2::classname(), [
                                'data' => CommonHelper::formTypeDropDownList(),
                                'language' => 'de',
                                'options' => ['placeholder' => 'Select a form type ...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);
                            ?>
                        </div>
                        <div class="col-md-4">
                            <?=
                            $form->field($model, 'development_id')->widget(Select2::classname(), [
                                'data' => CommonHelper::developmentDropdownList(),
                                'language' => 'de',
                                'options' => ['placeholder' => 'Select a Development ...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);
                            ?>
                        </div>
                        <div class="col-md-4">
                            <?=
                            $form->field($model, 'media_source_id')->widget(Select2::classname(), [
                                'data' => CommonHelper::leadSourceDropDownList(),
                                'language' => 'de',
                                'options' => ['placeholder' => 'Select a Development ...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);
                            ?>
                        </div>
                        <div class="col-md-4">
                            <?=
                            $form->field($model, 'tag_id')->widget(Select2::classname(), [
                                'data' => CommonHelper::tagNameDropDownList(),
                                'language' => 'de',
                                'options' => ['placeholder' => 'Select a Development ...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);
                            ?>
                        </div>
                        <div class="col-md-4">
                            <?=
                            $form->field($model, 'brand_id')->widget(Select2::classname(), [
                                'data' => CommonHelper::brandNameDropDownList(),
                                'language' => 'de',
                              /*  'options' => ['placeholder' => 'Select a brand name ...'],*/
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);
                            ?>
                        </div>
                        <div class="col-md-2">
                            <?= $form->field($model, 'redirect_status')->dropDownList(CommonHelper::redirectStatusDropDownList()) ?>
                        </div>
                        <div class="col-md-2">
                            <?= $form->field($model, 'validate_phone_status')->dropDownList(CommonHelper::redirectStatusDropDownList()) ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'redirect_link')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'privacy_link')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'success_message')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'fail_message')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'comments')->textarea(['rows' => 3]) ?>
                        </div>
                    </div>
                    <div class="divider"></div>
                    <div class="event_section" style="display: none">
                        <div class="formtools-form-title">
                            <h3>Events</h3>
                            <h2 style="font-size: 18px;line-height: 30px;" class="event-subhedder"><span>Events are used only with RSVP and Appointment form types. You will need to set at minimum 1 event date and time that is used to trigger the rsvp or appointment form expiry. This is the point in time at which it is no longer possible to attend the event and the form will revert itself to a Registration form automatically</span></h2>
                        </div>
                    <div class="row">
                        <div class="addevent-wrapper">
                            <div class="addevent-list">
                                <?php foreach ($formToolsEventSection as $indexevent => $itemEvent) { ?>
                                <div class="col-md-3">
                                    <div class="col-md-12">
                                        <?= $form->field($itemEvent, '['.$indexevent.']event_date')->textInput(['data-provide'=>"datepicker","placeholder"=>"10/12/2019",'class'=>'datepicker form-control']);?>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                <?php foreach ($formToolsEventRealation as $index => $item) { ?>
                                <div class="footer-wrapper<?php echo $index;?>">
                                        <div class="footer-list<?php echo $index;?>">
                                            <div class="col-md-3">
                                                <div class=" col-md-3 input-group clockpicker-<?php echo $indexevent;?>-<?php echo $index;?>" data-placement="bottom" data-align="bottom" data-autoclose="true" data-twelvehour="true">
                                                    <?= $form->field($item, '['.$indexevent.']['.$index.']event_start_time')->textInput(['data-provide'=>'timepicker','maxlength' => true,'placeholder'=>'10:00',"class"=>'timpicker form-control'])->label(true); ?>

                                                </div>
                                            </div>
                                            <div class="col-md-3" style="width: 15%;margin-top: 20px;">
                                                <?= $form->field($item, '['.$indexevent.']['.$index.']time_slote_start')->dropDownList(CommonHelper::timeSloteDropDownList())->label(false) ?>
                                            </div>
                                            <div class="col-md-3">
                                                <div class=" col-md-3 input-group clockpicker-<?php echo $indexevent;?>-<?php echo $index;?>" data-placement="bottom" data-align="bottom" data-autoclose="true" data-twelvehour="true">
                                                    <?= $form->field($item, '['.$indexevent.']['.$index.']event_end_time')->textInput(['data-provide'=>'timepicker','maxlength' => true,'placeholder'=>'10:00',"class"=>'timpicker form-control'])->label(true); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-3" style="width: 15%;margin-top: 20px;">
                                                <?= $form->field($item, '['.$indexevent.']['.$index.']time_slote_end')->dropDownList(CommonHelper::timeSloteDropDownList())->label(false) ?>
                                            </div>
                                            <?php if(!$model->isNewRecord){?>

                                            <?php } ?>
                                        </div>

                                    <div style="margin-right: 10px;margin-top: 20px;margin-left: 13px;" class="pull-left">
                                        <?= Html::button('+', ['class' => 'btn btn-success', 'id' => 'FooterEvent'.$index.'']) ?>
                                    </div>

                                </div>
                                <?php } ?>
                            </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div style="margin-right: 5px;margin-bottom: 10px;margin-top: 36px;" class="pull-right">
                        <?= Html::button('Add New Event', ['class' => 'btn btn-success', 'id' => 'AddNewEvent']) ?>
                    </div>
                    <div class="divider"></div>
                    </div>
                    <div class="formtools-form-title">
                        <h3>Style Portable Form</h3>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <?= $form->field($formToolsStyle, 'back_color')->widget(ColorInput::classname(), [
                                'options' => ['placeholder' => 'Select background color ...','readonly'=>true,'value'=>'#000000'],
                            ]); ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($formToolsStyle, 'label_color')->widget(ColorInput::classname(), [
                                'options' => ['placeholder' => 'Select label text color ...','readonly'=>true,'value'=>'#fff'],
                            ]); ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($formToolsStyle, 'checkbox_color')->widget(ColorInput::classname(), [
                                'options' => ['placeholder' => 'Select checkbox text color ...','readonly'=>true,'value'=>'#F5BA3D'],
                            ]); ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($formToolsStyle, 'btn_color')->widget(ColorInput::classname(), [
                                'options' => ['placeholder' => 'Select button color ...','readonly'=>true,'value'=>'#F5BA3D'],
                            ]); ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($formToolsStyle, 'label_font_size')->textInput(['maxlength' => true,'name'=>'FormtoolsStyle[label_font_size]', 'placeholder' => "Label text font size "]);?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($formToolsStyle, 'checkbox_font_size')->textInput(['maxlength' => true,'name'=>'FormtoolsStyle[checkbox_font_size]', 'placeholder' => "Label text font size "]);?>
                        </div>
                    </div>

                    <div class="divider"></div>
                        <div class="formtools-form-title" id="possbile">
                            <h3>Possible Tags</h3>
                            <h2 style="font-size: 18px;line-height: 30px;" class="event-subhedder"><span>You can click on any tag below to copy it to the clipboard</span></h2>
                        </div>
                        <div class="row">
                            <div class="col-md-12" id="possible_tags_embeded">
                            </div>
                        </div>
                    <br><br>

                    <div class="footer-button">
                        <div class="row">
                            <div class="col-md-4 text-left" id="success-message">
                                <button type="button" class="btn btn-success btn-push-to-maropost btn-show-after-save" name="action" value="save"><!-- <i class="fa fa-check"></i> --> Tags (Send To Maropost)</button>
                            </div>
                            <div class="col-md-4 text-center">
                            </div>
                            <div class="col-md-4 text-right">
                                <div class="admin-form-button">
                                    <?= FormButtons::widget() ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>


<?php
$baseUrl = Yii::getAlias('@baseUrl');
$formToolsJs=<<<JS
 
 function toggleOn() {
    $('#toggle-trigger').bootstrapToggle('on')
  }
  function toggleOff() {
    $('#toggle-trigger').bootstrapToggle('off')  
  }
  
    // Add New Event  Add New Row
    var addeventrowCount = $('.addevent-wrapper .addevent-list').length;
     var addevent=2;
     var newaddcount=addeventrowCount-1;
    $("#AddNewEvent").on("click", function () {
        var newRow = $("<div class='addevent-list'>");
        var cols = "";
        var count = addeventrowCount;
        var newcountrow=0;
        cols +='<div class="col-md-3">' +
                     '<div class="col-md-12">' +
                         '<div class="form-group field-formtoolsevent-'+ count +'-event_date">' +
                            '<label for="formtoolsevent-'+ count +'-event_date" class="control-label">Event Day '+ count +'</label> ' +
                            '<input type="text" data-provide="datepicker" placeholder="10/12/2019" id="formtoolsevent-'+ count +'-event_date" name="FormtoolsEvent['+ count +'][event_date]" class="form-control datepicker ">' +
                            '<p class="help-block help-block-error"></p>' +
                         '</div>' +
                      '</div>' +
                '</div>';
        
        cols +='<div class="col-md-8">' +
                  '<div class="footer-wrapper'+ count +'">' +
                        '<div class="footer-list'+ count +'">' +
                            '<div class="col-md-3">' +
                                '<div data-placement="bottom" data-align="bottom" data-autoclose="true" data-twelvehour="true" class=" col-md-3 input-group clockpicker-'+ count +'-'+ newcountrow +'">' +
                                    '<div class="form-group field-formtoolseventrealtion-0-0-event_start_time">' +
                                           '<label for="formtoolseventrealtion-'+ count +'-'+ count +'-event_start_time" class="control-label">Event Start Time</label> ' +
                                            '<input type="text" id="formtoolseventrealtion-'+ count +'-'+ newcountrow +'-event_start_time" name="FormtoolsEventRealtion['+ count +']['+ newcountrow +'][event_start_time]" maxlength="20" data-provide="timepicker" placeholder="10:00" class="timpicker form-control" style="text-align: center; width: 150px;"> ' +
                                            '<p class="help-block help-block-error"></p>' +
                                    '</div>' +
                                '</div> ' +
                                    '<div class="form-group field-formtoolseventrealtion-0-0-event_id">' +
                                            '<input type="hidden" id="formtoolseventrealtion-0-0-event_id" name="FormtoolsEventRealtion['+ count +']['+ newcountrow +'][event_id]" class="form-control"> ' +
                                            '<p class="help-block help-block-error"></p>' +
                                    '</div>' +
                              '</div>' +
                               '<div class="col-md-3" style="width: 15%; margin-top: 20px;">' +
                                            '<div class="form-group field-formtoolseventrealtion-0-0-time_slote_start">' +
                                            '<select id="formtoolseventrealtion-0-0-time_slote_start" name="FormtoolsEventRealtion['+ count +']['+ newcountrow +'][time_slote_start]" class="form-control">' +
                                            '<option value="AM">AM</option> <option value="PM">PM</option></select> <p class="help-block help-block-error"></p>' +
                                '</div>' +
                            '</div> ' +
                            '<div class="col-md-3">' +
                                '<div data-placement="bottom" data-align="bottom" data-autoclose="true" data-twelvehour="true" class="input-group clockpicker-'+ count +'-'+ newcountrow +'">' +
                                    '<div class="form-group field-formtoolseventrealtion-0-0-event_end_time">' +
                                    '<label for="formtoolseventrealtion-0-0-event_end_time" class="control-label">Event End Time</label>' +
                                    '<input type="text" id="formtoolseventrealtion-0-0-event_end_time" name="FormtoolsEventRealtion['+ count +']['+ newcountrow +'][event_end_time]" maxlength="20" data-provide="timepicker" placeholder="10:00" class="timpicker form-control" style="text-align: center; width: 150px;">' +
                                    '<p class="help-block help-block-error"></p>' +
                             '</div>' +
                            '</div>' +
                            '</div> ' +
                            '<div class="col-md-3" style="width: 15%; margin-top: 20px;">' +
                                '<div class="form-group field-formtoolseventrealtion-0-0-time_slote_end">' +
                                '<select id="formtoolseventrealtion-0-0-time_slote_end" name="FormtoolsEventRealtion['+ count +']['+ newcountrow +'][time_slote_end]" class="form-control">' +
                                '<option value="AM">AM</option>' +
                                 ' <option value="PM">PM</option>' +
                                '</select> <p class="help-block help-block-error"></p>' +
                           '</div>' +
                            '</div>' +
                            '</div>' +
                           '<div class="pull-left" style="margin-right: 10px; margin-top: 20px; margin-left: 13px;">' +
                            '<button type="button" id="FooterEvent'+count+'" class="btn btn-success">+</button>' +
                         '</div>' +
                      '</div>' +
                 '</div>';
        
         cols += '<div class="col-md-2 pull-left" style="margin-top: 20px;">';
            cols += '<input type="button" class="ibtnDel btn btn-md btn-danger "  value="x">';
        cols += '</div>';
        
        newRow.append(cols);
        $(".addevent-wrapper").append(newRow);
        
        $(".addevent-wrapper").on("click", ".ibtnDel", function (event) {
            $(this).closest(".addevent-list").remove();
        });
        
        eventcall(addeventrowCount);
        timeslect(addeventrowCount,newcountrow);
        addeventrowCount++;
    });
    
    eventcall(0);
    timeslect(0,0);
    
    // Add Event Time Section
    function eventcall(addeventrowCount) {
    var footerrowCount = $('.footer-wrapper'+ addeventrowCount +' .footer-list').length;
    $('#FooterEvent'+addeventrowCount+'').on("click", function () {
        var newRow = $("<div class='footer-list"+addeventrowCount+"'>");
        var cols = "";
        var count = addeventrowCount;
        var newcountrow=footerrowCount+1;
		var newcount=count -1;
        cols += '<div class="col-md-3">' +
                                '<div data-placement="bottom" data-align="bottom" data-autoclose="true" data-twelvehour="true" class=" col-md-3 input-group clockpicker-'+ count +'-'+ newcountrow +'">' +
                                    '<div class="form-group field-formtoolseventrealtion-0-0-event_start_time">' +
                                           '<label for="formtoolseventrealtion-'+ count +'-'+ count +'-event_start_time" class="control-label">Event Start Time</label> ' +
                                            '<input type="text" id="formtoolseventrealtion-'+ count +'-'+ newcountrow +'-event_start_time" name="FormtoolsEventRealtion['+ count +']['+ newcountrow +'][event_start_time]" maxlength="20" data-provide="timepicker" placeholder="10:00" class="timpicker form-control" style="text-align: center; width: 150px;"> ' +
                                            '<p class="help-block help-block-error"></p>' +
                                    '</div>' +
                                '</div> </div> ';
          cols +=  '<div class="col-md-3" style="width: 15%; margin-top: 20px;">' +
                                            '<div class="form-group field-formtoolseventrealtion-0-0-time_slote_start">' +
                                            '<select id="formtoolseventrealtion-0-0-time_slote_start" name="FormtoolsEventRealtion['+ count +']['+ newcountrow +'][time_slote_start]" class="form-control">' +
                                            '<option value="AM">AM</option> <option value="PM">PM</option></select> <p class="help-block help-block-error"></p>' +
                                '</div>' +
                            '</div>';
         cols += '<div class="col-md-3">' +
                                '<div data-placement="bottom" data-align="bottom" data-autoclose="true" data-twelvehour="true" class="input-group clockpicker-'+ count +'-'+ newcountrow +'">' +
                                    '<div class="form-group field-formtoolseventrealtion-0-0-event_end_time">' +
                                    '<label for="formtoolseventrealtion-0-0-event_end_time" class="control-label">Event End Time</label>' +
                                    '<input type="text" id="formtoolseventrealtion-0-0-event_end_time" name="FormtoolsEventRealtion['+ count +']['+ newcountrow +'][event_end_time]" maxlength="20" data-provide="timepicker" placeholder="10:00" class="timpicker form-control" style="text-align: center; width: 150px;">' +
                                    '<p class="help-block help-block-error"></p>' +
                             '</div>' +
                            '</div></div>';
         cols +='<div class="col-md-3" style="width: 15%; margin-top: 20px;">' +
                                '<div class="form-group field-formtoolseventrealtion-0-0-time_slote_end">' +
                                '<select id="formtoolseventrealtion-0-0-time_slote_end" name="FormtoolsEventRealtion['+ count +']['+ newcountrow +'][time_slote_end]" class="form-control">' +
                                '<option value="AM">AM</option>' +
                                 ' <option value="PM">PM</option>' +
                                '</select> <p class="help-block help-block-error"></p>' +
                           '</div>' +
                            '</div>';
        
         cols += '<div style="text-align:center" class="col-md-1">';
            cols += '<input style="margin-top:20px" type="button" class="ibtnDel'+addeventrowCount+' ibtnDel btn btn-md btn-danger "  value="x">';
        cols += '</div>';
        
        cols += '</div>';
        
        newRow.append(cols);
        $(".footer-wrapper"+addeventrowCount+"").append(newRow);
        
        $(".footer-wrapper"+addeventrowCount+"").on("click", ".ibtnDel"+addeventrowCount+"", function (event) {
            $(this).closest(".footer-list"+addeventrowCount+"").remove();       
            footerrowCount -= 1
        });
        footerrowCount++;
          timeslect(count,newcountrow);
        $('.footer-list'+addeventrowCount+'').css("clear","both");
    });
    }
    
    //Form type on change event section show or not
     $('#formtools-form_type_id').on('change', function() {
            var event_select_id=$(this).val();
            if (event_select_id==4 || event_select_id==5){
                $(".event_section").css("display","block");
            }
            else {
                 $(".event_section").css("display","none");
            }
     });
    
    function timeslect(first_index,second_index){
            $('.clockpicker-'+first_index+'-'+second_index+'').clockpicker()
	        .find('input').change(function(){
		    console.log(this.value);
	});
            $('.timpicker').keydown(function() {
            return false;
    });
          $('.datepicker').keydown(function() {
              return false;
    });
    }
    
    //timepicker css change
   $('.timpicker').css("text-align","center");
   $('.timpicker').css("width","150px");
   
   
   //Possible Tags Make Function
    $('#formtools-form_type_id,#formtools-development_id').on('change', function() {
        
        //enable possible tag section
          $('.btn-push-to-maropost').css("display","block");
          $('#possbile').css("display","block");
          
          //form type and developemnt data pass to function
          
         var form_type = $("#formtools-form_type_id option:selected").text();
         var form_type_value=$("#formtools-form_type_id option:selected").val();
         if (form_type_value==''){
             form_type='';
         } 
         var development_name = $("#formtools-development_id option:selected").text();
         var development_name_value_check= $("#formtools-development_id option:selected").val();
          if (development_name_value_check==''){
             development_name='';
          }
          console.log(form_type)
          console.log(development_name)
          
          var get_tags_list=maketags(form_type,development_name);
          //console.log(get_tags_list);    
          var data='';
          jQuery.each(get_tags_list, function(index, item) {
              data +="<span class='possible-tags-span' id='id"+index+"'>"+item+"</span><label  class='tooltiptext' data-toggle='tooltip'>Click to copy</label>";    
            });
          
          $('#possible_tags_embeded').html(data);
          
          //copy tags from label
           copytags(); 
     });
    
    //Function return all possible tags
   function maketags(form_type_option_value,development_option_value) {
        
        var form_type=form_type_option_value.toLowerCase();
        var development_name='';
        var possible_tags=[];
         
       if (development_option_value!=''){ 
             development_name='-'+development_option_value.toLowerCase();
       }
       //make
       if (form_type=='registration'){
           possible_tags=[
               'registration-prospect'+development_name+'',
               'registration-agent'+development_name+'',
           ];
       } else if (form_type=='subscribe') {
           possible_tags=[
               'subscribe-prospect'+development_name+'',
           ];
       }
        else if (form_type=='contact') {
            possible_tags=[
               'contact-prospect'+development_name+'',
           ];
       }
        else if (form_type=='rsvp') {
           possible_tags=[
               'rsvp-yes-prospect'+development_name+'',
               'rsvp-yes-agent'+development_name+'',
               'rsvp-no-prospect'+development_name+'',
               'rsvp-no-agent'+development_name+'',
           ];
       }
        else if (form_type=='appointment') {
           possible_tags=[
               'appointment-yes-prospect'+development_name+'',
               'appointment-yes-agent'+development_name+'',
               'appointment-no-prospect'+development_name+'',
               'appointment-no-agent'+development_name+'',
           ];
       }
        else if (form_type=='reservation') {
           possible_tags=[
               'reservation-prospect'+development_name+'',
               'reservation-yes-agent'+development_name+'',
               'reservation-no-prospect'+development_name+'',
               'reservation-no-agent'+development_name+'',
           ];
       } 
        else if (form_type=='survey') {
           possible_tags=[
               'survey-prospect'+development_name+'',
           ];
       }
        return possible_tags;
   }
   
    $('.btn-push-to-maropost').css("display","none");
    $('#possbile').css("display","none");
    
   //Send tags data to maropost
   $('.btn-push-to-maropost').click(function() {
        var tags_array = [];
        $("#possible_tags_embeded").find("span").each(function(){ tags_array.push($(this).text()); });
        $('#success-message').html('<button type="button" name="action" value="save" class="btn btn-success btn-show-after-save" style="display: block;">Sending....</button>');
         var url='$baseUrl/backend/web/formtools/formtools/submit-maropost-tags';
        $.ajax({
           url: url,
          /* type: 'POST',*/
           data: {tags_array},
           success: function(data) {
               setTimeout(function(){
                $('#success-message').html("<label id='possible-tags-success-message'>Successfully Sent To Maropost</label>");
            },5000);
           }
        });
   });
   
   //label content copy tags
   function copytags(){
    $('#possible_tags_embeded span').each(function() {
        var id=this.id;
        var text='';
        if ($('#'+id+'').click(function() {
              copy(this);
              
              //visiblity on off
              $('.tooltiptext').css("visibility","visible");
              $('.tooltiptext').html("Copied!");
              $('.tooltiptext').delay(500).fadeIn('normal', function() {
                    $(this).delay(2500).fadeOut();
              });
              
        }));
     })
   }
   
   function copy(that){
        var inp =document.createElement('input');
        document.body.appendChild(inp)
        inp.value =that.textContent
        inp.select();
        document.execCommand('copy',false);
        inp.remove();
    }
    
JS;
$this->registerJs($formToolsJs);

?>
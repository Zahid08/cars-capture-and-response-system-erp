<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model unlock\modules\formtools\models\Formtools */
?>
<div class="formtools-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'formToolsStyle' => $formToolsStyle,
        'formToolsEventSection' => $formToolsEventSection,
        'formToolsEventRealation' => $formToolsEventRealation,
    ]) ?>

</div>

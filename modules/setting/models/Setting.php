<?php

namespace  unlock\modules\setting\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use yii\base\DynamicModel;
use unlock\modules\core\helpers\FileHelper;
/**
 * This is the model class for table "setting".
 *
 * @property int $id
 * @property string $key
 * @property string $value
 * @property int $autoload

 */
class Setting extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%settings}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key', 'value', 'autoload'], 'safe'],
            [['key'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'value' => 'Value',
            'autoload' => 'Autoload',
        ];
    }

    /**
     * Return array of setting
     *
     * @return array
     */
    public static function getSetting()
    {
        $result = [];
        $setting = Setting::find()->select(['key', 'value'])->asArray()->all();

        foreach ($setting as $setting) {
            $key = $setting['key'];
            $value = $setting['value'];
            $result[$key] = $value;
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();

        Yii::$app->setting->invalidateCache();
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        Yii::$app->setting->invalidateCache();
    }

    public static function getDescription(){
        return Yii::$app->setting->get('description');
    }

    public static function saveSetting($setting){
        // Upload All Files
        self::uploadAllFiles($setting);

        if($setting){
            foreach ($setting as $key => $value) {
                $value = trim($value);
                self::saveSettingItem($key, $value);
            }
        }
    }

    public static function saveSettingItem($key, $value){
        $model = self::find()->where(['key' => $key])->one();
        if (empty($model)) {
            $model = new Setting();
        }
        $model->key = $key;
        $model->value = strval($value);

        $model->save();
    }

    public static function uploadAllFiles(&$setting){
        $model = new Setting();
        $post = Yii::$app->request->post();
        $dynamicModel = new DynamicModel();
        $uploadBasePath =  Yii::getAlias('@basePath') . '/media/setting/';

        if (isset($_FILES['DynamicModel']['name']) && is_array($_FILES['DynamicModel']['name'])) {
            foreach ($_FILES['DynamicModel']['name'] as $key => $value) {
                if($value){
                    // File Process
                    $file = UploadedFile::getInstance($dynamicModel, $key);
                    if($file)
                    {
                        $filePath = FileHelper::getUniqueFilenameWithPath($file->name, 'setting');
                        $file->saveAs($filePath);
                        $setting[$key] = FileHelper::getFilename($filePath);

                        self::deleteFile($key);
                    }
                }
                else if(isset($post['FileDelete'][$key])){
                    // Delete Previous  File
                    self::deleteFile($key);

                    // Save Setting
                    $setting[$key] = '';
                }
                else {
                    $setting[$key] = Yii::$app->setting->get($key);
                }
            }
        }
    }

    public static function deleteFile($key){
        $model = self::find()->where(['key' => $key])->one();
        if (!empty($model->value)) {
            $uploadBasePath =  Yii::getAlias('@basePath') . '/media/setting/';
            if(file_exists($uploadBasePath . $model->value) ){
                unlink($uploadBasePath . $model->value);
                return true;
            }
        }
        return false;
    }
}
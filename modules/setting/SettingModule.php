<?php

namespace unlock\modules\setting;

/**
 * Class Module
 *
 * @package yii2mod\setting
 */
class SettingModule extends \yii\base\Module
{
    /**
     * @var string the namespace that controller classes are in
     */
    public $controllerNamespace = 'unlock\modules\setting\controllers';

    /**
     * @var string the namespace that view file are in
     */
    public $viewNamespace = '@unlock/modules/setting/views/';
}

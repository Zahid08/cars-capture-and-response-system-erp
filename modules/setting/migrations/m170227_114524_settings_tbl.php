<?php

use yii\db\Migration;

/**
 * Class m170227_114524_settings_tbl
 */
class m170227_114524_settings_tbl extends Migration
{
    /**
     * This method contains the logic to be executed when applying this migration.
     */
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%settings}}', [
            'id' => $this->primaryKey(),
            'key' => $this->string()->notNull()->unique(),
            'value' => $this->text()->notNull(),
            'autoload' => $this->smallInteger(2)->notNull()->defaultValue(1),
        ], $tableOptions);

    }

    /**
     * This method contains the logic to be executed when removing this migration.
     */
    public function down()
    {
        $this->dropTable('{{%settings}}');
    }
}

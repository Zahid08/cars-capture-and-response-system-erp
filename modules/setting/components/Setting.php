<?php

namespace unlock\modules\setting\components;

use Yii;
use yii\base\Component;
use yii\caching\Cache;
use yii\di\Instance;
/**
 * Class Setting
 *
 * @package yii2mod\setting\components
 */
class Setting extends Component
{
    /**
     * @var string setting model class name
     */
    public $modelClass = 'unlock\modules\setting\models\Setting';

    /**
     * @var Cache|array|string the cache used to improve RBAC performance. This can be one of the followings:
     *
     * - an application component ID (e.g. `cache`)
     * - a configuration array
     * - a [[yii\caching\Cache]] object
     *
     * When this is not set, it means caching is not enabled
     */
    public $cache = 'cache';

    /**
     * @var string the key used to store setting data in cache
     */
    public $cacheKey = 'unlock-setting';

    /**
     * @var \unlock\modules\setting\models\SettingModel setting model
     */
    protected $model;

    /**
     * @var array list of setting
     */
    protected $items;

    /**
     * @var mixed setting value
     */
    protected $setting;

    /**
     * Initialize the component
     */
    public function init()
    {
        parent::init();

        if ($this->cache !== null) {
            $this->cache = Instance::ensure($this->cache, Cache::class);
        }

        $this->model = Yii::createObject($this->modelClass);
    }

    /**
     * Get's the value for the given section and key.
     *
     * @param string $section
     * @param string $key
     * @param null $default
     *
     * @return mixed
     */
    public function get($key, $default = null)
    {
        $items = $this->getSettingConfig();

         if (isset($items[$key])) {
            $this->setting = $items[$key];
        } else {
            $this->setting = $default;
        }

        return $this->setting;
    }



    /**
     * Checking existence of setting
     *
     * @param string $section
     * @param string $key
     *
     * @return bool
     */
    public function has($key)
    {
        $setting = $this->get($key);

        return !empty($setting);
    }



    /**
     * Returns the setting config
     *
     * @return array
     */
    protected function getSettingConfig()
    {
        if (!$this->cache instanceof Cache) {
            $this->items = $this->model->getSetting();
        }
        else {
            $cacheItems = $this->cache->get($this->cacheKey);
            if (!empty($cacheItems)) {
                $this->items = $cacheItems;
            } else {
                $this->items = $this->model->getSetting();
                $this->cache->set($this->cacheKey, $this->items);
            }
        }

        return $this->items;
    }

    /**
     * Invalidate the cache
     *
     * @return bool
     */
    public function invalidateCache()
    {
        if ($this->cache !== null) {
            $this->cache->delete($this->cacheKey);
            Yii::$app->cacheFrontend->delete($this->cacheKey);
            $this->items = null;
        }

        return true;
    }

    /**
     * Set type for setting
     *
     * @param $type
     */
    protected function convertSettingType($type)
    {
        if ($type === SettingType::BOOLEAN_TYPE) {
            $this->setting = filter_var($this->setting, FILTER_VALIDATE_BOOLEAN);
        } else {
            settype($this->setting, $type);
        }
    }

    public function __get($property) {
        return empty($this->get($property)) ? null : $this->get($property);
    }


}

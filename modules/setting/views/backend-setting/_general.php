<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\DatePicker;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\components\FileInput;
use unlock\modules\core\buttons\FormButtons;

/* @var $this yii\web\View */
/* @var $model unlock\modules\setting\models\Setting */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Update Configuration');
?>
<div class="main-container" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?=  Html::a(Yii::t('app', 'Home'), Yii::$app->homeUrl) ?></li>
                <li><?= Html::a(Yii::t('app', 'Settings'), Yii::$app->homeUrl.Url::to('/site/settings')) ?></li>
                <li class="active"><span><?= Yii::t('app', 'Configuration') ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-form-container">

        <?php if($isShowGeneralSetting) { ?>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fas fa-pencil-alt"></i> <?= Yii::t('app', 'General Settings')?></h3>
                </div>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin([
                    'action' => Url::to(['general']),
                    'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
                    'validateOnBlur' => false,
                    'enableAjaxValidation' => false,
                    'errorCssClass'=>'has-error',
                    'options' => ['enctype' => 'multipart/form-data'],
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
                        'options' => ['class' => 'form-group'],
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-2',
                            'offset' => '',
                            'wrapper' => '',
                            'hint' => '',
                        ],
                    ],
                ]); ?>

                <?= $form->field($modelGeneral, 'site_name') ?>

                <?= $form->field($modelGeneral, 'favicon_icon')->widget(FileInput::classname(),['dirName' => 'setting']); ?>

                <?= $form->field($modelGeneral, 'company_logo')->widget(FileInput::classname(),['dirName' => 'setting']); ?>

                <?= $form->field($modelGeneral, 'company_logo_text') ?>

                <?= $form->field($modelGeneral, 'contact_address')->textarea(['rows' => '4']) ?>

                <?= $form->field($modelGeneral, 'copyright')->textarea(['rows' => '4'])->label('Copyright') ?>

                <div class="admin-form-button">
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <?= FormButtons::widget(['item' => ['save']]) ?>
                        </div>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <?php } ?>

        <?php if($isShowEmailSetting) { ?>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fas fa-pencil-alt"></i> <?= Yii::t('app', 'Email Settings')?></h3>
                </div>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin([
                    'action' => Url::to(['general']),
                    'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
                    'validateOnBlur' => false,
                    'enableAjaxValidation' => false,
                    'errorCssClass'=>'has-error',
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
                        'options' => ['class' => 'form-group'],
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-2',
                            'offset' => '',
                            'wrapper' => '',
                            'hint' => '',
                        ],
                    ],
                ]); ?>

                <?= $form->field($modelEmail, 'admin_sender_name') ?>
                <?= $form->field($modelEmail, 'admin_sender_email') ?>

                <?= $form->field($modelEmail, 'support_sender_name') ?>
                <?= $form->field($modelEmail, 'support_sender_email') ?>

                <div class="admin-form-button">
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <?= FormButtons::widget(['item' => ['save']]) ?>
                        </div>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <?php } ?>

    </div>
</div>

<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\DatePicker;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\components\FileInput;

/* @var $this yii\web\View */
/* @var $model unlock\modules\sample\models\Sample */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Clear Cache');
?>
<div class="main-container" role="main">
    <div class="page-header">
        <div class="page-header-title">
            <h1 class="page-title"><?= Yii::t('app', 'Clear Cache') ?></h1>
        </div>
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?=  Html::a(Yii::t('app', 'Home'), Yii::$app->homeUrl) ?></li>
                <li class="active"><span><?= Yii::t('app', 'Clear Cache') ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-form-container">
        <p>
            <a href="<?= Url::to(['/setting/backend-setting/flush-cache']); ?>" class="btn btn-default"><i class="glyphicon glyphicon-flash"></i> Flush cache</a>
        </p>

        <p>
            <a href="<?= Url::to(['/setting/backend-setting/clear-assets']); ?>" class="btn btn-default"><i class="glyphicon glyphicon-trash"></i> Clear assets</a>
        </p>
    </div>
</div>


<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model unlock\modules\setting\models\SuiteType */
?>
<div class="suite-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

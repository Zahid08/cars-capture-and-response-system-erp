<?php

use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\grid\GridView;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\buttons\NewButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $searchModel unlock\modules\setting\models\LeadSourceSearch */
/* @var $dataProvider unlock\modules\core\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Lead Source');
?>
<div class="main-container" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li><?= Html::a(Yii::t('app', 'Marketing'), Yii::$app->homeUrl.Url::to('/site/marketing')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-grid-filter">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <div class="admin-grid-content">
        <div class="admin-grid-toolbar">
            <div class="admin-grid-toolbar-left">
                <?= NewButton::widget(['url' => Url::toRoute(['create'])]) ?>
            </div>
            <div class="admin-grid-toolbar-right">
                <?= Summary::widget(['dataProvider' => $dataProvider]) ?>
            </div>
        </div>

        <div class="admin-grid-view">
                            <?php 
                echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
                'pager' => ['class' => LinkPager::className()],
                'columns' => [
                //['class' => 'unlock\modules\core\grid\SerialColumn'],
                    [
                        'attribute' => 'id',
                        'format'    => 'html',
                        'value' => function($model){
                            $name = Html::a($model->id, ['update', 'id' => $model->id]);
                            return $name;
                        }
                    ],

                    [
                        'attribute' => 'lead_source_name',
                        'format'    => 'html',
                        'value' => function($model){
                            $name = Html::a($model->lead_source_name, ['update', 'id' => $model->id]);
                            return $name;
                        }
                    ],
                    [
                        'attribute'  => 'media_source_type',
                        'format'    => 'raw',
                        'headerOptions' => ['style' => ''],
                        'value' => function($model){
                        $urlInterac = yii\helpers\Url::to(['lead-source/change-media-source-type?id='.$model->id]);
                        return Html::dropDownList('media-source-dropdown', $model->media_source_type, CommonHelper::mediaSourceTypeDropdownList(), [
                            'class' => 'media-source-dropdown',
                            'onchange' => "     
                                        var source_type_id = $(this).val();
                                        console.log(source_type_id);
                                        $.ajax('$urlInterac&status='+source_type_id+'', {
                                            type: 'POST'
                                        }).done(function(data) {
                                            //$.pjax.reload({container: '#pjax-container'});
                                        });     
                                    ",
                        ]);
                        ?>
                     <?php
                        }
                    ],
                    'created_at:datetime',
                    'created_by:user',
                    'updated_at:datetime',
                    'updated_by:user',
                    // 'status',
                    [
                    'attribute' => 'status',
                    'format' => 'status',
                    'filter' => Html::activeDropDownList($searchModel, 'status', CommonHelper::statusDropDownList(), ['prompt' => CommonHelper::GRID_FILTER_PROMPT]),
                    ],
                    //['class' => 'unlock\modules\core\grid\ActionColumn'],
                ],
                ]);   ?>
                    </div>
    </div>

</div>

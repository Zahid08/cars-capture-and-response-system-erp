<?php

use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\grid\GridView;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\buttons\NewButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $searchModel unlock\modules\setting\models\RegistrationStatusSearch */
/* @var $dataProvider unlock\modules\core\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Registration Status');
?>
<div class="main-container" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li><?= Html::a(Yii::t('app', 'Settings'), Yii::$app->homeUrl.Url::to('/site/settings')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-grid-filter">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <div class="admin-grid-content">
        <div class="admin-grid-toolbar">
            <div class="admin-grid-toolbar-left">
                <?= NewButton::widget(['url' => Url::toRoute(['create'])]) ?>
            </div>
            <div class="admin-grid-toolbar-right">
                <?= Summary::widget(['dataProvider' => $dataProvider]) ?>
            </div>
        </div>

        <div class="admin-grid-view">
                            <?php 
                echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
                'pager' => ['class' => LinkPager::className()],
                'columns' => [
                	'id',
                    [
                        'attribute' => 'registration_status_name',
                        'format'    => 'html',
                        'value' => function($model){
                            $name = Html::a($model->registration_status_name, ['update', 'id' => $model->id]);
                            return $name;
                        }
                    ],
                    [
                        'class' => 'yii\grid\CheckboxColumn',
                        'header' => Html::checkBox('fdf', false, [
                            'class' => 'select-on-check-all',
                            'label' => 'Closed State',
                        ]),
                        'checkboxOptions' => function($model) {
                           if ($model->journey_status==0) {
                               $journey_status = ["value" => $model->id, "class" => 'journey-status', "label" => '', "checked" => false];
                           }
                           else{
                               $journey_status = ["value" => $model->id, "class" => 'journey-status', "label" => '', "checked" => true];
                           }
                             return $journey_status;
                        },
                    ],
                    'created_at:datetime',
                    'created_by:user',
                    'updated_at:datetime',
                    // 'updated_by',
                    // 'status',
                [
                'attribute' => 'status',
                'format' => 'status',
                'filter' => Html::activeDropDownList($searchModel, 'status', CommonHelper::statusDropDownList(), ['prompt' => CommonHelper::GRID_FILTER_PROMPT]),
                ],
                //['class' => 'unlock\modules\core\grid\ActionColumn'],
                ],
                ]);   ?>
                    </div>
    </div>

</div>
<?php
$baseUrl = Yii::getAlias('@baseUrl');
$js = <<< JS
         $('.journey-status').click(function() {
              var registration_status_id=$(this).val();
              var  journey_status='';
              if ($(this).is(':checked')) {
                    journey_status=1;      
                } else {
                   journey_status=0;             
                }
            var url='$baseUrl/backend/web/setting/registration-status/change-registration-status?id='+registration_status_id+'&journey_status='+journey_status+'';
                $.post(url, function(data){
                    alert('Successfully Updated Status');
                });
                    
         });
JS;
$this->registerJs($js);
?>

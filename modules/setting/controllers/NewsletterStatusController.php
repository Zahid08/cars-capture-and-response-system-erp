<?php

namespace unlock\modules\setting\controllers;

use Yii;
use yii\helpers\Html;
use yii\base\Exception;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use unlock\modules\setting\models\NewsletterStatus;
use unlock\modules\setting\models\NewsletterStatusSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use unlock\modules\core\helpers\Collection;
use unlock\modules\core\helpers\FileHelper;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\actions\ExportCsvAction;
use unlock\modules\core\actions\ExportPdfAction;

/**
 * NewsletterStatusController implements the CRUD actions for NewsletterStatus model.
 */
class NewsletterStatusController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => Yii::$app->DynamicAccessRules->customAccess('setting/newsletterstatus'),
        ];
    }
    
    /**
     * Lists all NewsletterStatus models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsletterStatusSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        Collection::setData($dataProvider);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single NewsletterStatus model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new NewsletterStatus model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NewsletterStatus();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $this->saveData($model);
        }

        return $this->render('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing NewsletterStatus model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $this->saveData($model);
        }

        return $this->render('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing NewsletterStatus model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        try {
            if (!$model->delete()) {
                throw new Exception(Yii::t('app', 'The record cannot be deleted.'));
            }

            // Delete Image:
            // FileHelper::removeFile($model->image, 'sanction');
            Yii::$app->session->setFlash('success', Yii::t('app', 'The record have been successfully deleted.'));
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            if($e->getName() == CommonHelper::INTEGRITY_CONSTRAINT_VIOLATION){
                $message = "This record has relational data. You must delete the following data first";
                $message .= "<ul>";
                $message .= "<li>Example => Example List</li>";
                $message .= "</ul>";
            }
            Yii::$app->session->setFlash('error', $message);
        }

        return $this->redirect(['index']);
    }

    /**
    * Save Data.
    * @param object $model
    * @return mixed
    */
    private function saveData($model){
        if (isset($model)) {
            //$transaction = Yii::$app->db->beginTransaction();
            try {
                $post = Yii::$app->request->post();
                
                if (!$model->save()) {
                    throw new Exception(Yii::t('app', Html::errorSummary($model)));
                }

                //$transaction->commit();
                Yii::$app->session->setFlash('success', Yii::t('app', 'Record successfully saved.'));
                $this->_redirect($model);
            }
            catch (Exception $e) {
                //$transaction->rollBack();
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
    }
    
    private function _redirect($model){
        $action = Yii::$app->request->post('action', 'save');
        switch ($action) {
            case 'save':
                return $this->redirect(['index']);
                break;
            case 'apply':
                return $this->redirect(['update', 'id' => $model->id]);
                break;
            case 'save2new':
                return $this->redirect(['create']);
                break;
            default:
                return $this->redirect(['index']);
        }
    }

    /**
     * Finds the NewsletterStatus model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NewsletterStatus the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NewsletterStatus::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}

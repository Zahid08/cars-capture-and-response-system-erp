<?php

namespace unlock\modules\setting\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use unlock\modules\setting\models\Setting;
use yii\web\UploadedFile;
use yii\base\DynamicModel;

/**
 * Class BackendSettingController
 *
 * @package yii2mod\setting\controllers
 */
class BackendSettingController extends Controller
{

    public $defaultAction = 'general';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => Yii::$app->DynamicAccessRules->customAccess('setting/backendsetting'),
        ];
    }

    /**
     * Updates an existing Setting.
     *
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function actionGeneral()
    {
        // General
        $isShowGeneralSetting = true;
        $modelGeneral = new DynamicModel(['site_name', 'favicon_icon', 'company_logo', 'company_logo_text', 'copyright', 'contact_address']);
        $modelGeneral->addRule(['site_name', 'favicon_icon', 'company_logo', 'company_logo_text', 'copyright', 'contact_address'], 'safe');

        $modelGeneral->copyright = 'Copyright © 2017-2018 Govt. Shipping Office, Chittagong. All Rights Reserved.';

        // Email Settings
        $isShowEmailSetting = true;
        $modelEmail = new DynamicModel(['admin_sender_name', 'admin_sender_email', 'support_sender_name', 'support_sender_email']);
        $modelEmail->addRule(['admin_sender_name', 'admin_sender_email', 'support_sender_name', 'support_sender_email'], 'safe');

        $modelEmail->admin_sender_name = 'Admin';
        $modelEmail->admin_sender_email = 'admin@example.com';
        $modelEmail->support_sender_name = 'Customer Support';
        $modelEmail->support_sender_email = 'support@example.com';

        if(Yii::$app->request->post()){
            $setting = Yii::$app->request->post('DynamicModel');
            Setting::saveSetting($setting);
            Yii::$app->session->setFlash('success', Yii::t('app', 'Setting have been successfully updated.'));
        }

        // Load Default Value
        if($defaultValues = Setting::getSetting()){
            $modelGeneral->setAttributes($defaultValues, false);
            $modelEmail->setAttributes($defaultValues, false);
        }

        return $this->render('_general', [
            'modelGeneral' => $modelGeneral,
            'modelEmail' => $modelEmail,
            'isShowGeneralSetting' => $isShowGeneralSetting,
            'isShowEmailSetting' => $isShowEmailSetting,
        ]);

    }

    public function actionCache()
    {
        return $this->render('_cache');
    }

    //setting/backend-setting/flush-cache
    public function actionFlushCache()
    {
        Yii::$app->cache->flush();
        Yii::$app->session->setFlash('success', Yii::t('app', 'Cache flushed'));
        return $this->redirect(['cache']);
    }

    //clear-assets
    public function actionClearAssets()
    {
        // Frontend
        foreach(glob(str_replace('\\backend\\', '\\frontend\\', Yii::$app->assetManager->basePath) . DIRECTORY_SEPARATOR . '*') as $asset){
            if(is_link($asset)){
                unlink($asset);
            } elseif(is_dir($asset)){
                $this->deleteDir($asset);
            } else {
                unlink($asset);
            }
        }

        // Backend
        foreach(glob(Yii::$app->assetManager->basePath . DIRECTORY_SEPARATOR . '*') as $asset){
            if(is_link($asset)){
                unlink($asset);
            } elseif(is_dir($asset)){
                $this->deleteDir($asset);
            } else {
                unlink($asset);
            }
        }
        Yii::$app->session->setFlash('success', Yii::t('app', 'Assets cleared'));
        return $this->redirect(['cache']);
    }

    private function deleteDir($directory)
    {
        $iterator = new \RecursiveDirectoryIterator($directory, \RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new \RecursiveIteratorIterator($iterator, \RecursiveIteratorIterator::CHILD_FIRST);
        foreach($files as $file) {
            if ($file->isDir()){
                rmdir($file->getRealPath());
            } else {
                unlink($file->getRealPath());
            }
        }
        return rmdir($directory);
    }

}

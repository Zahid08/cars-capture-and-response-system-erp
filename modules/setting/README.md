Setting
=======================================================================================================================

Setting Manager for every Site

Migration
-----------------------------------------------------------------------------------------------------------------------

**Create Database**

php yii migrate/up --migrationPath=@unlock/modules/setting/migrations

**Delete Database**

php yii migrate/down --migrationPath=@unlock/modules/setting/migrations

Configuration
-----------------------------------------------------------------------------------------------------------------------

**Module Setup**

'modules' => [
    'setting' =>  [
        'class' => 'unlock\modules\setting\SettingModule',
    ]
],

**Component Setup**

'components' => [
    'setting'=>[
        'class'=>'unlock\modules\setting\components\Setting'
    ],
],

Left Menu
-----------------------------------------------------------------------------------------------------------------------
**Left Admin Menu**
// Settings
[
    'label' => '<i class="fa fa-cog"></i> <span>Settings</span>',
    'url' => ['#'],
    'visible'=> Yii::$app->user->checkAccess('setting/backendsetting', 'general') ||
                Yii::$app->user->checkAccess('dynamicdropdown/dynamicdropdown', 'index'),
    'template' => '<a class="dropdown-toggle" href="{url}" >{label}</a>',
    'items' => [
        [
            'label' => '<span>Configuration</span>',
            'url' => ['/setting/backend-setting/general'],
            'visible'=> Yii::$app->user->checkAccess('setting/backendsetting', 'general'),
            'active' => LeftMenu::isActive(['setting/backend-setting/general']),
        ],
        [
            'label' => '<span>Manage Drop Down</span>',
            'url' => ['/dynamic-drop-down/dynamic-drop-down/index'],
            'visible'=> Yii::$app->user->checkAccess('dynamicdropdown/dynamicdropdown', 'index'),
            'active' => LeftMenu::isActive(['dynamic-drop-down/dynamic-drop-down/']),
        ],
    ],
],

Usage
-----------------------------------------------------------------------------------------------------------------------
echo Yii::$app->setting->siteName;
echo Yii::$app->setting->get('siteName');

URLs
-----------------------------------------------------------------------------------------------------------------------
Backend:
/setting/general

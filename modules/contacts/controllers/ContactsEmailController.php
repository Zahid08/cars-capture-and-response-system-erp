<?php

namespace unlock\modules\contacts\controllers;

use unlock\modules\contacts\models\ContactsRegistration;
use unlock\modules\contacts\models\ContactsSysteminfo;
use unlock\modules\usermanager\user\models\User;
use Yii;
use unlock\modules\contacts\models\ContactsEmailLog;
use unlock\modules\contacts\models\ContactsEmailLogSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use unlock\modules\contacts\models\ContactsRegistrationSearch;
use unlock\modules\core\helpers\CommonHelper;
use api\common\maropost\maropost;

/**
 * ContactsEmailLogController implements the CRUD actions for ContactsEmailLog model.
 */
class ContactsEmailController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    protected function findModel($id)
    {
        if (($model = ContactsEmailLog::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }


    /*Auto Email Secnd Action Before 7 days Registration data*/
    public function actionAutoEmail()
    {
        $basAuth            =Yii::getAlias('@baseAuthhor');
        $html_wrapper       =$this->email_template('6','Weekly');
      /*  echo "<pre>";print_r($html_wrapper);exit();*/
        $subject            =$this->email_subject('6','Weekly');
        $maropost=new maropost();
        $arrayNew = array(
            "content" => array(
                "name" =>'Admin System Global Mail Content [Weekly]',
                "html_part" => $html_wrapper,
                "full_email" => "false",
                "footer_type" => "default_footer",
                "footer_id" => ""
            ),
            "id" =>195
        );
        $maropost->put_template_content($arrayNew);
        $assign_user=User::find()->where(['id'=>$basAuth])->one(); //User assigned id 57 st /48 Lv
        $response_register=$this->register_assigned_user_maropost($assign_user,$subject);
        if ($response_register['http_status']==201){
            echo "<h1 style='text-align: center;color: green'>Weekly Email Sent Successfully !</h1>";
        }
    }

    /*Auto Email Secnd Action Before 1 days Registration data*/
    public function actionAutoEmailDaily()
    {
        $basAuth            =Yii::getAlias('@baseAuthhor');
        $html_wrapper       =$this->email_template('1','Daily');
      /*  echo "<pre>";print_r($html_wrapper);exit();*/
        $subject            =$this->email_subject('1','Daily');
        $maropost=new maropost();
        $arrayNew = array(
            "content" => array(
                "name" =>'Admin System Global Mail Content [Weekly]',
                "html_part" => $html_wrapper,
                "full_email" => "false",
                "footer_type" => "default_footer",
                "footer_id" => ""
            ),
            "id" =>195
        );
        $maropost->put_template_content($arrayNew);
        $assign_user=User::find()->where(['id'=>$basAuth])->one(); //User assigned id 48 /57 st
        $response_register=$this->register_assigned_user_maropost($assign_user,$subject);
        if ($response_register['http_status']==201){
            echo "<h1 style='text-align: center;color: green'>Daily System Log  Email Sent Successfully !</h1>";
        }
    }

    /*For Mail subject return*/
    private function email_subject($day,$status){
        $previous_date      ='-'.$day.' day';
        $end_date           =date('Y-m-d H:i:s');
        $start_date         = date('Y-m-d H:i:s', strtotime($end_date .$previous_date));
        $to                 =CommonHelper::changeDateFormat($end_date,'d M Y');
        $from               =CommonHelper::changeDateFormat($start_date,'d M Y');
        $subject            ='System Lead '.$status.' Report From ('.$from.' - '.$to.')';
        return $subject;
    }

    /*Email Template make for daily and weekly */
    private function email_template($day,$status){
        $previous_date      ='-'.$day.' day';
        $basAuth            =Yii::getAlias('@baseAuthhor');
        $basUrl             =Yii::getAlias('@baseUrl');
        $end_date           =date('Y-m-d H:i:s');
        $start_date         = date('Y-m-d H:i:s', strtotime($end_date .$previous_date));
        $to                 =CommonHelper::changeDateFormat($end_date,'d M Y');
        $from               =CommonHelper::changeDateFormat($start_date,'d M Y');
        $phone=$created_at=$contact_type=$form_type=$assigned_username='';

        $registration_data = ContactsRegistration ::find()
            ->where(['between', 'created_at', $start_date, $end_date ])
            ->orderBy(  ['id' => SORT_DESC])
            ->groupBy('development_id')
            ->all();

        $html_wrapper='<h3 style="font-family: \'Open Sans\', Arial, sans-serif; font-size:15px; line-height:20px; color:#4777bb; font-weight:bold;">'.$status.' Lead Report - Leads for all form types from ('.$from.' to '.$to.')</h3>';
        $c=0;
        foreach ($registration_data as $total_development_registrationItem){
            $development_id         =!empty($total_development_registrationItem->development_id)?$total_development_registrationItem->development_id:'';
            $developmentName        = CommonHelper::developmentListById($total_development_registrationItem->development_id);
            $registration_record    =$this->sql_query($development_id,$start_date,$end_date);
            if ($c==0){
                $height='0';
            }else{
                $height='25';
            }
            $html_wrapper .='<table >
                                <thead>
                                <tr>
                                  <th height="'.$height.'"></th>
                                </tr>
                                <tr>
                                    <th height="" align="left"><a href="'.$basUrl.'/backend/web/developments/developments/view?id='.$developmentName->id.'">'.$developmentName->development_name.'</a></th>
                                </tr>
                                </thead>
                            </table>';
            $html_wrapper .="<table style='border-collapse: collapse;'><thead><tr>
                                <th height=\"45\" width='180'style='border: 1px solid black;font-size: 16px;'>ID</th> 
                                <th height=\"45\" width='190' style='border: 1px solid black;font-size: 16px;'>Media Source</th>  
                                <th height=\"45\" width='230' style='border: 1px solid black;font-size: 16px;'>Name</th>     
                                <th height=\"45\" width='230' style='border: 1px solid black;font-size: 16px;'>Phone number </th>     
                                <th height=\"45\" width='230' style='border: 1px solid black;font-size: 16px;'>Email</th>
                                <th height=\"45\" width='200' style='border: 1px solid black;font-size: 16px;'>Contact Type</th>
                                <th height=\"45\" width='200' style='border: 1px solid black;font-size: 16px;'>Form Type</th> 
                                <th height=\"45\" width='200' style='border: 1px solid black;font-size: 16px;'>User</th>
                                <th height=\"45\" width='200' style='border: 1px solid black;font-size: 16px;'>Status </th></tr></thead><tbody>";

           /* echo "<pre>";print_r($registration_record);*/
            foreach ($registration_record as $registration){

                $registration_status    =CommonHelper::getRegistrationStatusById($registration['status']);
                $Id                     ='<a href="'.Yii::getAlias('@baseUrl').'/backend/web/contacts/contacts-registration/update?id='.$registration['id'].'">'.$registration['id'].'</a>';
                $developmentName        =!empty($registration['development_name'])?$registration['development_name']:'';
                $first_name             =!empty($registration['first_name'])?$registration['first_name']:'';
                $lastname               =!empty($registration['last_name'])?$registration['last_name']:'';
                $phone                  =!empty($registration['phone'])?$registration['phone']:'';
                $email                  =!empty($registration['email'])?$registration['email']:'';
                $contact_type           =!empty($registration['contact_type'])?$registration['contact_type']:'';
                $media_lead             =!empty($registration['lead_source_name'])?$registration['lead_source_name']:'';
                $full_name              ='<a href="'.Yii::getAlias('@baseUrl').'/backend/web/contacts/contacts/view?id='.$registration['contact_id'].'">'.$first_name.' '.$lastname.'</a>';
                $form_type              =!empty($registration['form_type'])?$registration['form_type']:'';
                if ($registration['assignment_id']==0){
                    $assigned_username='';
                }else{
                    $username           =CommonHelper::getLoggedInUserNameByUserId($registration['assignment_id']);
                    $assigned_username=$username;
                }
                $html_wrapper .="
                               <tr>
                               <td height=\"45\" width='150' style='border: 1px solid black;text-align: center;font-size: 14px;'>$Id</td>
                               <td height=\"45\" style='border: 1px solid black;text-align: center;font-size: 14px;'>$media_lead</td>
                               <td height=\"45\" style='border: 1px solid black;text-align: center;font-size: 14px;'>$full_name</td>
                               <td height=\"45\" style='border: 1px solid black;text-align: center;font-size: 14px;'>$phone</td>
                               <td height=\"45\" style='border: 1px solid black;text-align: center;font-size: 14px;'>$email</td>
                               <td height=\"45\" style='border: 1px solid black;text-align: center;font-size: 14px;'>$contact_type</td>
                               <td height=\"45\" style='border: 1px solid black;text-align: center;font-size: 14px;'>$form_type</td> 
                               <td height=\"45\" style='border: 1px solid black;text-align: center;font-size: 14px;'>$assigned_username</td>
                               <td height=\"45\" style='border: 1px solid black;text-align: center;font-size: 14px;'>$registration_status</td>
                               </tr>
                             ";
            }

            $html_wrapper .=" </tbody></table>";
            $c++;
        }
        //System Information Rap
        $registration_system_record=$this->systemlog_query($start_date,$end_date);
        $html_wrapper .='<table>
                                <thead>
                                <tr>
                                  <th height="25"></th>
                                </tr>
                                <tr>
                                    <th height="" align="left"><a href="#">No Development</a></th>
                                </tr>
                                </thead>
                            </table>';
        $html_wrapper .="<table style='border-collapse: collapse;'><thead><tr>
                                <th height=\"45\" width='180'style='border: 1px solid black;font-size: 16px;'>ID</th>  
                                <th height=\"45\" width='230' style='border: 1px solid black;font-size: 16px;'>Media Source</th>
                                <th height=\"45\" width='230' style='border: 1px solid black;font-size: 16px;'>Name</th>     
                                <th height=\"45\" width='230' style='border: 1px solid black;font-size: 16px;'>Phone number </th>     
                                <th height=\"45\" width='230' style='border: 1px solid black;font-size: 16px;'>Email</th>
                                <th height=\"45\" width='200' style='border: 1px solid black;font-size: 16px;'>Contact Type</th>
                                <th height=\"45\" width='190' style='border: 1px solid black;font-size: 16px;'>Form Type</th> 
                                <th height=\"45\" width='190' style='border: 1px solid black;font-size: 16px;'>User</th>
                                <th height=\"45\" width='190' style='border: 1px solid black;font-size: 16px;'>Status </th></tr></thead><tbody>";

        foreach ($registration_system_record as $sys_item){
            $registration_status    ='New';
            $Id                     ='<a href="'.Yii::getAlias('@baseUrl').'/backend/web/contacts/contacts-systeminfo/view?id='.$sys_item['id'].'">'.$sys_item['id'].'</a>';
            $first_name             =!empty($sys_item['first_name'])?$sys_item['first_name']:'';
            $lastname               =!empty($sys_item['last_name'])?$sys_item['last_name']:'';
            $phone                  =!empty($sys_item['phone'])?$sys_item['phone']:'';
            $email                  =!empty($sys_item['email'])?$sys_item['email']:'';
            $contact_type           =!empty($sys_item['contact_type'])?$sys_item['contact_type']:'';
            $media_lead             =!empty($sys_item['media_source'])?$sys_item['media_source']:'';
            $full_name              ='<a href="'.Yii::getAlias('@baseUrl').'/backend/web/contacts/contacts/view?id='.$sys_item['contact_id'].'">'.$first_name.' '.$lastname.'</a>';
            $form_type              =!empty($sys_item['form_type'])?$sys_item['form_type']:'';
            $developmentName        = CommonHelper::developmentListById($sys_item['development_id']);
            if(!empty($developmentName)){
                $username           =CommonHelper::getLoggedInUserNameByUserId($developmentName->assignment_id);
                $assigned_username =$username;
            }
            $html_wrapper .="
                           <tr>
                           <td height=\"45\" width='150' style='border: 1px solid black;text-align: center;font-size: 14px;'>$Id</td>
                           <td height=\"45\" style='border: 1px solid black;text-align: center;font-size: 14px;'>$media_lead</td>
                           <td height=\"45\" style='border: 1px solid black;text-align: center;font-size: 14px;'>$full_name</td>
                           <td height=\"45\" style='border: 1px solid black;text-align: center;font-size: 14px;'>$phone</td>
                           <td height=\"45\" style='border: 1px solid black;text-align: center;font-size: 14px;'>$email</td>
                           <td height=\"45\" style='border: 1px solid black;text-align: center;font-size: 14px;'>$contact_type</td>
                           <td height=\"45\" style='border: 1px solid black;text-align: center;font-size: 14px;'>$form_type</td> 
                           <td height=\"45\" style='border: 1px solid black;text-align: center;font-size: 14px;'>$assigned_username</td>
                           <td height=\"45\" style='border: 1px solid black;text-align: center;font-size: 14px;'>$registration_status</td>
                           </tr>
                             ";

        }

        $html_wrapper .=" </tbody></table>";

        return $html_wrapper;
    }
    /* Registration to maropost*/
    private function register_assigned_user_maropost($assign_user,$subject){
        $first_name =   !empty($assign_user->first_name) ? $assign_user->first_name : '';
        $last_name =   !empty($assign_user->last_name) ? $assign_user->last_name : '';
        $mobile =   !empty($assign_user->mobile) ? $assign_user->mobile : '';
        $email =   !empty($assign_user->email) ? $assign_user->email : '';
        $list_id=50;
        $contactsNew=new maropost();
        $contactsNew = $contactsNew->contacts();
        $arrayNew = array(
            'list_id' => $list_id,
            'contact' =>
                array(
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'email' => $email,
                    'subscribe' => 'true',
                    'custom_field' =>
                        array(
                            'phone_1' =>$mobile,
                            'company' => 'CONNECT asset management',
                            'realtor' => 'False',
                            'prospect' => 'true',
                            'system_weekly_email_subject' => $subject,
                            'system_weekly_email_status' => time()
                        )
                ));
        $response=$contactsNew->post_new_contact_into_list($arrayNew);
        return $response;
    }

    private function sql_query($development_id,$from,$to){
        $connection = Yii::$app->getDb();
        $query="select  reg.id,reg.contact_id,reg.development_id,reg.reg_datetime,reg.status,cont.first_name,cont.phone,cont.last_name,cont.email,sys.contact_type,sys.form_type,leads.lead_source_name,dev.assignment_id,dev.development_name
                from `conn_contacts_systeminfo` sys 
                JOIN `conn_contacts_registration` reg ON reg.system_log_id=sys.id
                JOIN `conn_contacts` cont ON cont.id=reg.contact_id 
                JOIN `conn_setting_lead_source` leads ON leads.id=reg.reg_source 
                JOIN `conn_developments` dev ON dev.development_id=reg.development_id 
                WHERE reg.development_id=".$development_id."
                AND sys.visit_date between '".$from."'
                AND '".$to."'
                GROUP by reg.id
                ORDER by sys.contact_type DESC";
        $command = $connection->createCommand($query);
        $records_total =$command->queryAll();
        return $records_total;
    }

    private function systemlog_query($from,$to){
        $connection = Yii::$app->getDb();
        $query="select *
                from `conn_contacts_systeminfo` sys
                WHERE sys.form_type in ('contact','subscribe')
                AND sys.visit_date between '".$from."'
                AND '".$to."'
                ORDER by sys.contact_type DESC";
        $command = $connection->createCommand($query);
        $records_total =$command->queryAll();
        return $records_total;
    }

}

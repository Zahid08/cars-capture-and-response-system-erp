<?php

namespace unlock\modules\contacts\controllers;


use api\common\maropost\tags;
use api\modules\v1\models\ContactsBrands;
use api\modules\v1\models\MediaLeads;
use unlock\modules\contacts\models\ContactsEmailLog;
use unlock\modules\contacts\models\ContactsInteractions;
use unlock\modules\contacts\models\ContactsPurchases;
use unlock\modules\contacts\models\ContactsRegistrationSearch;
use unlock\modules\contacts\models\ContactsReservations;
use unlock\modules\contacts\models\ContactsRsvp;
use unlock\modules\contacts\models\ContactsSysteminfo;
use unlock\modules\contacts\models\ContactsTags;
use unlock\modules\contacts\models\ContactsTagsApplied;
use unlock\modules\contacts\models\ContactsTagsNew;
use unlock\modules\contacts\models\ContactsTagsRelation;
use unlock\modules\generalsettings\models\City;
use unlock\modules\generalsettings\models\Province;
use unlock\modules\setting\models\MediaType;
use unlock\modules\usermanager\user\models\User;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\base\Exception;
use yii\web\ServerErrorHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use unlock\modules\contacts\models\Contacts;
use unlock\modules\contacts\models\ContactsSearch;
use unlock\modules\contacts\models\ContactsRegistration;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use unlock\modules\core\helpers\Collection;
use unlock\modules\core\helpers\FileHelper;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\actions\ExportCsvAction;
use unlock\modules\core\actions\ExportPdfAction;
use api\common\maropost\maropost;

/**
 * ContactsController implements the CRUD actions for Contacts model.
 */
class ContactsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => Yii::$app->DynamicAccessRules->customAccess('contacts/Contacts'),
        ];
    }
    
    /**
     * Lists all Contacts models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new ContactsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        Collection::setData($dataProvider);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Displays a single Contacts model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $registrationsModel = ContactsRegistration::find()->where(['contact_id' => $id])->all();

        return $this->render('view', [
            'model' => $this->findModel($id),
            'registrationsModel' => $registrationsModel,
        ]);
    }



    // Registration Contact
    public function actionRegistrations(){

        $searchModel = new ContactsRegistrationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        //$dataProvider->query->andWhere(['contact_type'=>'1']);
        Collection::setData($dataProvider);

        return $this->render('registrations', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);


    }


    /**
     * Creates a new Contacts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Contacts();


        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $this->saveData($model);
        }

        return $this->render('_form', [
            'model' => $model,
        ]);
    }
/*
 * ##Change Contact type action like: Prospects or agent from dropdown
 */
    public function actionChangeContactType($id,$status){
        $model = $this->findModel($id);
        $model->contact_type = $status;
        if (!$model->save()) {
            throw new Exception(Yii::t('app', Html::errorSummary($model)));
        }

        return $this->redirect(['view', 'id' => $model->id]);
    }

    /**
     * Updates an existing Contacts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            $this->saveData($model);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Contacts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

       /* $modelBrand =  ContactsBrands::findAll(['contact_id' => $id]);
        $modelInteractions =  ContactsInteractions::findAll(['contact_id' => $id]);
        $modelPurchases = ContactsPurchases::findAll(['contact_id' => $id]);
        $modelRegistration = ContactsRegistration::findAll(['contact_id' => $id]);
        $modelReservations = ContactsReservations::findAll(['contact_id' => $id]);
        $modelRsvp = ContactsRsvp::findAll(['contact_id' => $id]);
        $modelSysteminfo = ContactsSysteminfo::findAll(['contact_id' => $id]);
        $modelTags = ContactsTags::findAll(['contact_id' => $id]);*/

        try {
            ContactsBrands::deleteAll(['contact_id' => $id]);
            ContactsInteractions::deleteAll(['contact_id' => $id]);
            ContactsPurchases::deleteAll(['contact_id' => $id]);
            ContactsRegistration::deleteAll(['contact_id' => $id]);
            ContactsReservations::deleteAll(['contact_id' => $id]);
            ContactsRsvp::deleteAll(['contact_id' => $id]);
            ContactsSysteminfo::deleteAll(['contact_id' => $id]);
            ContactsTags::deleteAll(['contact_id' => $id]);
            if (!$model->delete()) {
                throw new Exception(Yii::t('app', 'The record cannot be deleted.'));
            }

            // Delete Image:
            // FileHelper::removeFile($model->image, 'sanction');
            Yii::$app->session->setFlash('success', Yii::t('app', 'The record have been successfully deleted.'));
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            if($e->getName() == CommonHelper::INTEGRITY_CONSTRAINT_VIOLATION){
                $message = "This record has relational data. You must delete the following data first";
                $message .= "<ul>";
                $message .= "<li>Example => Example List</li>";
                $message .= "</ul>";
            }
            Yii::$app->session->setFlash('error', $message);
        }

        return $this->redirect(['index']);
    }
    /**
    * Save Data.
    * @param object $model
    * @return mixed
    */
    private function saveData($model){
        if (isset($model)) {
            //$transaction = Yii::$app->db->beginTransaction();
            try {

                $post = Yii::$app->request->post();

                if (!$model->save()) {
                    throw new Exception(Yii::t('app', Html::errorSummary($model)));
                }
                // Maropost API Integration
                $media_source_id=$post['Contacts']['lead_source'];
                $this->maropostCallback($model,$media_source_id,$tag_name='');

                if($model->save()){
                    $modelContactBrands = new ContactsBrands();

                    $modelContactBrands->contact_id     = $model->id;
                    $modelContactBrands->brand_id       = $model->brand_name;
                    $modelContactBrands->applied_date   = date('Y-m-d');
                    $modelContactBrands->created_at     = date('Y-m-d H:i:s');
                    $modelContactBrands->save();
                }

                Yii::$app->session->setFlash('success', Yii::t('app', 'Record successfully saved.'));
                $this->_redirect($model);
            }
            catch (Exception $e) {
                //$transaction->rollBack();
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
    }

    /*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Import Function Architecture Rules Maintain @@@@@@@@@@@@@@@@*/
    /*
     * Import Tool Functionality Architecture Maintain Rules Ans FHTML form rule
     */
    public function actionImportContacts(){
        $contact_mode=new Contacts();
        $data           =   array();
        $data['title']  =   'Contact Import Tool';

        return $this->render('_import', [
            'data' => $data,
            'model' => $contact_mode,
        ]);
    }

    /*
     * Import Tags Tool
     */

    public function actionImportContactsTags(){
        $data_array    =[];
        $mimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
        if(in_array($_FILES['file_select_tags']['type'],$mimes)){
            if(!empty($_FILES["file_select_tags"]["name"])) {
               $file_data = fopen($_FILES["file_select_tags"]["tmp_name"], 'r');
               fgetcsv($file_data);
               $exist_row=true;
               $totalimport=0;
               while ($row = fgetcsv($file_data)) {
                   $data_array['email']        = !empty($row[0]) ? $row[0] : '';
                   $data_array['tag_name']     = !empty($row[1])? $row[1] : '';
                   $data_array['applied_date']  = !empty($row[2])? $row[2] : ''; //Month/Date/Year
                   $exist_row=false;
                   $this->saveContactTags($data_array);
                   $totalimport++;
               }
               if ($exist_row==false){
                  echo ' <label class="btn btn-info" style="margin-top: 15px;color: green;background-color: #ddd;">'.$totalimport.' Tags Successfully Import.</label>';
               }
            }
        }
        else {
            echo '0';
            exit();
        }
    }
    public function saveContactTags($data_array){
        $checkEmail = Contacts::find()->where(['email' =>$data_array['email']])->one();
        $contactModel=new Contacts();
        $contactId='';
        if (!empty($checkEmail)) {
            $contactId=$checkEmail->id;
        }
        else{
            $contactModel->email =$data_array['email'];
            $contactModel->first_name ='';
            $contactModel->last_name ='';
            $contactModel->phone ='';
            $contactModel->agent = 'connect';
            if($contactModel->save()){
                $contactId=$contactModel->id;
            }
        }
        $this->saveImportTags($contactId,$data_array);
    }
    public function saveImportTags($contact_id,$data_array){
        $applied_date=CommonHelper::changeDateFormat($data_array['applied_date'], 'Y-m-d H:i:s');
        $contactsTag = new ContactsTagsNew();
        $contactsTag->tag_name =$data_array['tag_name'];
        $contactsTag->tag_category_id='1';
        $checkTags = ContactsTagsNew::find()->where(['tag_name' => $contactsTag->tag_name])->one();
        if (empty($checkTags)){
            if ($contactsTag->save(false)) {
                //save data relation table
                $tagRelation=new ContactsTagsRelation();
                $tagRelation->contact_id=$contact_id;
                $tagRelation->tag_id=$contactsTag->id;
                $tagRelation->date_applied=$applied_date;
                if (!$tagRelation->save(false)) {
                    throw new Exception(json_encode($tagRelation->errors));
                }
                //save data to applied tag
                $tagApplied=new ContactsTagsApplied();
                $tagApplied->tag_id=$contactsTag->id;
                if (!$tagApplied->save(false)) {
                    throw new Exception(json_encode($tagApplied->errors));
                }

            }
        }
        else{
            $tagRelation=new ContactsTagsRelation();
            $tagRelation->contact_id=$contact_id;
            $tagRelation->tag_id=$checkTags->id;
            $tagRelation->date_applied=$applied_date;
            if (!$tagRelation->save(false)) {
                throw new Exception(json_encode($tagRelation->errors));
            }
            $tagApplied=new ContactsTagsApplied();
            $tagApplied->tag_id=$checkTags->id;
            if (!$tagApplied->save(false)) {
                throw new Exception(json_encode($tagApplied->errors));
            }
        }
    }
    /*
    * End  Import Tags Tool
    */
    /*
    * Import Contacts Tool
    */
    public function actionImportContactsAjax($brand,$media_source){
        $mimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
        if(in_array($_FILES['file_select']['type'],$mimes)){
            $data_array    =[];
            if(!empty($_FILES["file_select"]["name"])) {
                $file_data = fopen($_FILES["file_select"]["tmp_name"], 'r');
                fgetcsv($file_data);
                $prospect=0;$agent=0;$total_count=0;
                $result_back=[];
                $exist_row=true;
                $total_data=[];
                while ($row = fgetcsv($file_data)) {
                    $data_array['first_name']   = !empty($row[0]) ? $row[0] : '';
                    $data_array['last_name']    = !empty($row[1]) ? $row[1] : '';
                    $data_array['phone']        = !empty($row[2]) ? $row[2] : '';
                    $data_array['email']        = !empty($row[3]) ? $row[3] : '';
                    $data_array['address']      = !empty($row[4])? $row[4] : '';
                    $data_array['city']         = !empty($row[5])? $row[5] : '';
                    $data_array['province']     = !empty($row[6])? $row[6] : '';
                    $data_array['country']      = !empty($row[7])? $row[7] : '';
                    $data_array['postal']       = !empty($row[8])? $row[8] : '';
                    $data_array['media_source'] = $media_source;
                    $data_array['brand_id']     = $brand;
                    $data_array['contact_type'] = !empty($row[10])? $row[10] : '';

                    if ($data_array['contact_type']=='Agent'){
                        $agent++;
                    }else{
                        $prospect++;
                    }
                    $total_count++;
                    $exist_row=false;
                    $total_data[]=$data_array;
                    $contactsSystemInfo=$this->saveSystemLog($data_array);
                    if (!empty($contactsSystemInfo)){
                        $this->saveImportData($contactsSystemInfo,$data_array);
                    }
                }
                if ($exist_row==false){
                    echo ' <label class="btn btn-info" style="margin-top: 15px;color: green;background-color: #ddd;">'.$total_count.' Contacts Successfully Import </label>';
                }
            }
        }
        else {
            echo '0';
            exit();
        }
    }
    //Save Import Function
    private function saveImportData($contactsSystemInfo,$data_array){
        try {
            if(!empty($contactsSystemInfo)) {
                $contact_model = $this->saveProspectInfo($contactsSystemInfo,$data_array);
                if (!empty($contact_model)){
                    $this->saveBrandInfo($data_array, $contact_model->id);
                    $this->saveTagsInfo($contactsSystemInfo, $contact_model->id);
                    $this->updateSystemInfo($contact_model->id, $contactsSystemInfo->id);
                   // $this->saveMaropost($contact_model,$data_array,$contactsSystemInfo);
                }
            }
        }catch (Exception $e) {
            throw new ServerErrorHttpException($e->getMessage());
        }
    }

    /*Update System Information*/
    private function updateSystemInfo($contact_id,$system_log_id){
        // System information
        $checkSystemInfo = ContactsSysteminfo::find()->where(['id' =>$system_log_id])->one();
        if (!empty($checkSystemInfo)) {
            $checkSystemInfo->contact_id = $contact_id;
            if (!$checkSystemInfo->save(false)) {
                throw new Exception(json_encode($checkSystemInfo->errors));
            }
        }
    }

    /*Tags Information Save To Cars*/
    private function saveTagsInfo($model,$contact_id){
        $tag_name=$develomentList=$development_name='';
        //Realtor Check
        if (!empty($model->realtor)) {
            $contact_type='realtor';
        }
        else{
            $contact_type='prospect';
        }
        $tag_name=$model->form_type.'-'.$contact_type;
        //Save contact tag
        $contactsTag = new ContactsTagsNew();
        $contactsTag->tag_name =$tag_name;
        $contactsTag->tag_category_id=1;
        $checkTags = ContactsTagsNew::find()->where(['tag_name' => $contactsTag->tag_name])->one();
        if (empty($checkTags)){
            if ($contactsTag->save(false)) {
                //save data relation table
                $tagRelation=new ContactsTagsRelation();
                $tagRelation->contact_id=$contact_id;
                $tagRelation->tag_id=$contactsTag->id;
                $tagRelation->date_applied= date('Y-m-d H:i:s');
                if (!$tagRelation->save(false)) {
                    throw new Exception(json_encode($tagRelation->errors));
                }
                //save data to applied tag
                $tagApplied=new ContactsTagsApplied();
                $tagApplied->tag_id=$contactsTag->id;
                if (!$tagApplied->save(false)) {
                    throw new Exception(json_encode($tagApplied->errors));
                }

            }
        }
        else{
            $tagRelation=new ContactsTagsRelation();
            $tagRelation->contact_id=$contact_id;
            $tagRelation->tag_id=$checkTags->id;
            $tagRelation->date_applied= date('Y-m-d H:i:s');
            if (!$tagRelation->save(false)) {
                throw new Exception(json_encode($tagRelation->errors));
            }
            $tagApplied=new ContactsTagsApplied();
            $tagApplied->tag_id=$checkTags->id;
            if (!$tagApplied->save(false)) {
                throw new Exception(json_encode($tagApplied->errors));
            }
        }
    }

    /*Brand Information Save To Cars*/
    private function saveBrandInfo($model,$contact_id){
        $brandName = ContactsBrands::find()->where(['contact_id' => $contact_id])->andWhere(['brand_id' => $model['brand_id']])->one();
        if (empty($brandName)) {
            $brandInfo = new ContactsBrands();
            $brandInfo->contact_id = $contact_id;
            $brandInfo->brand_id = $model['brand_id'];
            $brandInfo->applied_date = date('Y-m-d H:i:s');
            if (!$brandInfo->save(false)) {
                throw new Exception(json_encode($brandInfo->errors));
            }
        }
    }

    /*Prospect Information Save To Cars (Event Prospect)*/
    private function saveProspectInfo($contact_system_model,$data_array){
        $cityInfo = City::find()->where(['city_name' => $data_array['city']])->one();
        if (!empty($cityInfo)) {$cityId = $cityInfo->id;} else {$cityId = '';}
        $proviInfo = Province::find()->where(['province_name' => $data_array['province']])->one();
        if (!empty($proviInfo)) {
            $provId = $proviInfo->id;
        } else {
            $provId = '';
        }
        $postal=$data_array['postal'];
        $address=$data_array['address'];

        $contact_type=$update_system_contact_type='';
        if (!empty($contact_system_model->realtor)):
            $contact_type=2;
            $update_system_contact_type='Agent';
        else:
            $contact_type=1;
            $update_system_contact_type='Prospect';
        endif;

        /*Save Contacts Information*/
        $contactModel=new Contacts();
        $checkEmail = Contacts::find()->where(['email' =>$contact_system_model->email])->one();
        if (!empty($checkEmail)) {
            $contactModel = $this->findModel($checkEmail->id);
            if (empty($checkEmail->first_name)) {
                $contactModel->first_name = $contact_system_model->first_name;
            }
            if (empty($checkEmail->last_name)) {
                $contactModel->last_name =$contact_system_model->last_name;
            }
            if (empty($checkEmail->phone)) {
                $contactModel->phone = $contact_system_model->phone;
            }

            if ($checkEmail->contact_type !='2'){
                $contactModel->contact_type =$contact_type;
            }
            $contactModel->agent = 'connect';
            $contactModel->address =$address;
            $contactModel->city =(string)$cityId;
            $contactModel->province =(string)$provId;
            $contactModel->postal =$postal;
            $contactModel->maropost_import_status =0;
            if ($contactModel->save()) {
                $contactId=$contactModel->id;
            }
        }
        else{
            $mediaLeadSourceID=$this->saveMediaInfo($contact_system_model);
            $contactModel->first_name =$contact_system_model->first_name;
            $contactModel->last_name =$contact_system_model->last_name;
            $contactModel->email = $contact_system_model->email;
            $contactModel->phone = $contact_system_model->phone;
            $contactModel->lead_source = $mediaLeadSourceID;
            $contactModel->contact_type = $contact_type;
            $contactModel->agent = 'connect';
            $contactModel->address =$address;
            $contactModel->city =(string)$cityId;
            $contactModel->province =(string)$provId;
            $contactModel->postal =$postal;
            $contactModel->maropost_import_status =0;
            if($contactModel->save()){
                $contactId=$contactModel->id;
            }
        }
        $this->updateSystemContactType($update_system_contact_type,$contact_system_model->id);
        return $contactModel;
    }

    //Update Contact Type
    private function updateSystemContactType($contact_type,$system_log_id){
        $checkSystemInfo = ContactsSysteminfo::find()->where(['id' =>$system_log_id])->one();
        if (!empty($checkSystemInfo)) {
            $checkSystemInfo->contact_type =$contact_type;
            if (!$checkSystemInfo->save(false)) {
                throw new Exception(json_encode($checkSystemInfo->errors));
            }
        }
    }

    /*System Information Save To Cars*/
    public function saveSystemLog($dataList){
        $mediaLeads_name =CommonHelper::leadSourceByBrandId($dataList['media_source']);

        if ($dataList['contact_type']=='Agent'){
            $realtor='1';
        }else{
            $realtor='';
        }
        $browser=$this->getBrowserInfo(); //Browser Informaiton Get Dynamically
        $contactsSystemInfo = new ContactsSysteminfo(); //Contact System Information Model Get
        $contactsSystemInfo->first_name        = $dataList['first_name'];
        $contactsSystemInfo->last_name         = $dataList['last_name'];
        $contactsSystemInfo->email             = $dataList['email'];
        $contactsSystemInfo->phone             = $dataList['phone'];
        $contactsSystemInfo->media_source      = $mediaLeads_name;
        $contactsSystemInfo->brand_id          =$dataList['brand_id'];
        $contactsSystemInfo->form_type         ='Import';
        $contactsSystemInfo->realtor           =$realtor;
        $contactsSystemInfo->ip_address        = $browser['ip_address'];
        $contactsSystemInfo->browser           = $browser['name'];
        $contactsSystemInfo->operating         = $browser['platform'];
        $contactsSystemInfo->device            = $browser['device'];
        $contactsSystemInfo->visit_date        = date('Y-m-d H:i:s');
        $contactsSystemInfo->log_create_status =0;
        $contactsSystemInfo->log_data          = date('Y-m-d H:i:s');

        if (!$contactsSystemInfo->save(false)) {
            throw new Exception(json_encode($contactsSystemInfo->errors));
        }
        return $contactsSystemInfo;
    }

    /*Media Information Save To Cars*/
    private function saveMediaInfo($contact_system_model){
        $mediaLeadSource=new MediaLeads();
        $checkLeadName=MediaLeads::find()->where(['lead_source_name' => $contact_system_model->media_source])->one();
        if (empty($checkLeadName)) {
            $mediaLeadSource->lead_source_name = $contact_system_model->media_source;
            $mediaLeadSource->created_at = date('Y-m-d H:i:s');
            $mediaLeadSource->status = 1;
            if (!$mediaLeadSource->save(false)) {
                throw new Exception(json_encode($mediaLeadSource->errors));
            }
            $media_id = $mediaLeadSource->id;
        } else {
            $media_id  = $checkLeadName->id;
        }
        return $media_id ;
    }
    /**/
    private function saveMaropost($contact_model,$data_array,$contactsSystemInfo){
        $media_source_id=$data_array['media_source'];
        $tag_name='';
        if (!empty($contactsSystemInfo->realtor)) {
            $contact_type='realtor';
        }
        else{
            $contact_type='prospect';
        }
        $tag_name=$contactsSystemInfo->form_type.'-'.$contact_type;
        $this->maropostCallback($contact_model,$media_source_id,$tag_name);
    }

    /*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    END AGRICULTURE RULES @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/

    /*
     * #Change Rsv Status Action.
     */
    public function actionChangeRsvpStatus($id,$status){
        if (($model = ContactsRsvp::findOne($id)) !== null) {
            $model->rsvp_status = $status;
            if (!$model->save()) {
                throw new Exception(Yii::t('app', Html::errorSummary($model)));
            }
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * Finds the Contacts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Contacts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Contacts::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /* Province List*/
    public function actionProvince($provinceid){

        $sectionCount = Province::find()->where(['id' => $provinceid])->count();
        $sectionAll = Province::find()->where(['id' => $provinceid])->all();

        if($sectionCount > 0){
            foreach ($sectionAll as $value){
                echo '<option value="'.$value->id.'">'.$value->province_name.'</option>';
            }
        }else{
            echo '<option>Select Province Name...</option>';
        }

    }

    /* City List*/
    public function actionCity($id){

        $sectionCount = City::find()->where(['province_id' => $id])->count();
        $sectionAll = City::find()->where(['province_id' => $id])->all();

        if($sectionCount > 0){
            echo '<option>Select a City...</option>';
            foreach ($sectionAll as $value){
                echo '<option value="'.$value->id.'">'.$value->city_name.'</option>';
            }
        }else{
            echo '<option>Select a City...</option>';
        }

    }

    /* Tags List*/
    public function actionTag($id){

        $sectionCount = ContactsTagsNew::find()->where(['tag_category_id' => $id])->count();
        $sectionAll = ContactsTagsNew::find()->where(['tag_category_id' => $id])->orderBy(['tag_name' => SORT_ASC])->all();

        if($sectionCount > 0){
            foreach ($sectionAll as $value){
              /*  echo "<pre>";print_r($value);*/
                if (!empty($value->tag_name)) {
                    echo '<option value="' . $value->id . '">' . $value->tag_name . '</option>';
                }
            }
        }else{
            echo '<option>Select Tag...</option>';
        }

    }

    private function _redirect($model){
        $action = Yii::$app->request->post('action', 'save');
        switch ($action) {
            case 'save':
                return $this->redirect(['index']);
                break;
            case 'apply':
                return $this->redirect(['update', 'id' => $model->id]);
                break;
            case 'save2new':
                return $this->redirect(['create']);
                break;
            default:
                return $this->redirect(['index']);
        }
    }

    /*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    MAROPOST CALLBACK @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/

    private function maropostCallback($model,$media_source_id,$tag_name){
        $mediaLeads_name =CommonHelper::leadSourceByBrandId($media_source_id);

        $city_model=CommonHelper::cityListByCityId($model->city);

        if (empty($city_model)){
            $cityname="NULL";
        }
        else{
            $cityname=ArrayHelper::getValue($city_model,'city_name');
        }

        $provinec_model=CommonHelper::provinceListByProvinceId($model->province);

        if (empty($provinec_model)){
            $province_name="NULL";
        }
        else{
            $province_name=ArrayHelper::getValue($provinec_model,'province_name');
        }

        $country_model=CommonHelper::countryListByCountryId($model->country);

        if (empty($country_model)){
            $country_name="NULL";
        }
        else{
            $country_name=ArrayHelper::getValue($country_model,'country_name');
        }

        $registraion_data['media_source']='Original Media Source -'.$mediaLeads_name;

        $tags=new tags();
        $array1 = array(
            "tags" => array(
                "name" =>$registraion_data['media_source']
            )
        );
        $tags->post_tags($array1);
        $maropost_tag_name='';
        if (!empty($tag_name)){
            $array2 = array(
                "tags" => array(
                    "name" =>$tag_name
                )
            );
            $tags->post_tags($array2);
            $maropost_tag_name=$tag_name;
        }

        $maropost = new maropost();
        $contactsNew 	= $maropost->contacts();

        $realtor_ = 'False';
        $prospect = 'true';
        $realtor_tag = 'Prospect';
        $list_id = 1;
        if (!empty($mediaLeads_name)) {
            $registraion_data['media_source_tag'] = 'Original Media Source -' . $mediaLeads_name;
        }
        else{
            $registraion_data['media_source_tag']=$mediaLeads_name;
        }

        $assigned_user = CommonHelper::contactIdByRegistrationListDevelopmentId($model->id);
        $assign_user_name=ucfirst($assigned_user);

        $arrayNew = array(
            'list_id' => $list_id,
            'contact' =>
                array(
                    'first_name' 	=> $model->first_name,
                    'last_name' 	=> $model->last_name,
                    'email' 		=> $model->email,
                    'subscribe'  	=> 'true',
                    'custom_field' =>
                        array(
                            'phone_1' => $model->phone,
                            'company' => 'CONNECT asset management',
                            'realtor' => $realtor_,
                            'prospect' => $prospect,
                            'full_name' 	=> $model->first_name.' '.$model->last_name,
                            'address' 	    => $model->address,
                            'city' 	        => $cityname,
                            'province' 	    => $province_name,
                            'assigned_to' 	=> $assign_user_name,
                            'country' 	    => $country_name,
                            'postal'=>$model->postal,
                            'birthday'=>$model->date_of_birth
                        ),
                    'add_tags' =>
                        array(
                            $registraion_data['media_source_tag'],
                            $maropost_tag_name,
                            $realtor_tag,
                        )
                ));


        $arrayNewsletter = $arrayNew;
        if($arrayNewsletter['list_id'] == 1){
            $arrayNewsletter['list_id'] = 3;
            $contactsNew->post_new_contact_into_list($arrayNewsletter);
        }

        $contactsNew->post_new_contact_into_list($arrayNew);
    }

    /*Browser Information get for architecture*/
    public function getBrowserInfo() {
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version= "";
        $deveice='';
        if (stristr($_SERVER['HTTP_USER_AGENT'],'mobi')!==FALSE) {
            $deveice='Mobile';
        }
        else{
            $deveice='Desktop';
        }

        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'linux';
        } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'mac';
        } elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'windows';
        }
        if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) {
            $bname = 'Internet Explorer';
            $ub = "MSIE";
        } elseif(preg_match('/Firefox/i',$u_agent)) {
            $bname = 'Mozilla Firefox';
            $ub = "Firefox";
        } elseif(preg_match('/Chrome/i',$u_agent)) {
            $bname = 'Google Chrome';
            $ub = "Chrome";
        } elseif(preg_match('/Safari/i',$u_agent)) {
            $bname = 'Apple Safari';
            $ub = "Safari";
        } elseif(preg_match('/Opera/i',$u_agent)) {
            $bname = 'Opera';
            $ub = "Opera";
        } elseif(preg_match('/Netscape/i',$u_agent)) {
            $bname = 'Netscape';
            $ub = "Netscape";
        }
        $known = array('Version', $ub, 'other');
        $pattern = '#(?<browser>' . join('|', $known) . ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
        }
        $i = count($matches['browser']);
        if ($i != 1) {
            if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
                $version= $matches['version'][0];
            } else {
                $version= $matches['version'][1];
            }
        } else {
            $version= $matches['version'][0];
        }
        if ($version==null || $version=="") {$version="?";}

        if (!empty($_SERVER['HTTP_CLIENT_IP']))
        {
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            $ip=$_SERVER['REMOTE_ADDR'];
        }

        return array(
            'userAgent' => $u_agent,
            'name'      => $bname,
            'version'   => $version,
            'platform'  => $platform,
            'pattern'    => $pattern,
            'ip_address'=>$ip,
            'device'=>$deveice,
        );
    }
}

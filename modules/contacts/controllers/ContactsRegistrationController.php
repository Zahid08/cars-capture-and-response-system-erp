<?php

namespace unlock\modules\contacts\controllers;

use api\modules\v1\models\ContactsSysteminfo;
use api\modules\v1\models\MediaLeads;
use unlock\modules\contacts\models\ContactsInteractions;
use unlock\modules\contacts\models\ContactsTagsApplied;
use unlock\modules\contacts\models\ContactsTagsNew;
use unlock\modules\contacts\models\ContactsTagsRelation;
use unlock\modules\core\helpers\MaropostHelper;
use unlock\modules\developments\models\Developments;
use Yii;
use yii\helpers\Html;
use yii\base\Exception;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use unlock\modules\contacts\models\ContactsRegistration;
use unlock\modules\contacts\models\ContactsRegistrationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use unlock\modules\core\helpers\Collection;
use unlock\modules\core\helpers\FileHelper;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\actions\ExportCsvAction;
use unlock\modules\core\actions\ExportPdfAction;
use unlock\modules\contacts\models\Contacts;
use yii\helpers\Json;
/**
 * ContactsRegistrationController implements the CRUD actions for ContactsRegistration model.
 */
class ContactsRegistrationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => Yii::$app->DynamicAccessRules->customAccess('contacts/contactsregistration'),
        ];
    }
    
    /**
     * Lists all ContactsRegistration models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContactsRegistrationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        Collection::setData($dataProvider);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Displays all Reports.
     * @param integer $id
     * @return mixed
     */
    public function actionRegistrationReport(){
        $lastdate=date('Y-m-d H:i:s');
        $prev_date = date('Y-m-d H:i:s', strtotime($lastdate .' -7 day'));
        $searchModel = new ContactsRegistrationSearch();
        $dataProvider = $searchModel->search_registration_report(Yii::$app->request->queryParams,$prev_date,$lastdate);
        return $this->render('_quicreport.php', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ContactsRegistration model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ContactsRegistration model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ContactsRegistration();


        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            $this->saveData($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $this->saveData($model);
        }

        return $this->render('_form', [
            'model' => $model,
        ]);
    }


    /**
     * Updates an existing ContactsRegistration model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);


        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            $this->saveData($model);
        }

        return $this->render('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ContactsRegistration model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        try {
            if (!$model->delete()) {
                throw new Exception(Yii::t('app', 'The record cannot be deleted.'));
            }

            // Delete Image:
            // FileHelper::removeFile($model->image, 'sanction');
            Yii::$app->session->setFlash('success', Yii::t('app', 'The record have been successfully deleted.'));
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            if($e->getName() == CommonHelper::INTEGRITY_CONSTRAINT_VIOLATION){
                $message = "This record has relational data. You must delete the following data first";
                $message .= "<ul>";
                $message .= "<li>Example => Example List</li>";
                $message .= "</ul>";
            }
            Yii::$app->session->setFlash('error', $message);
        }

        return $this->redirect(['index']);
    }

    public function actionChangeStatus($id, $status){
        $model = $this->findModel($id);
        $model->status = $status;
        $model->status_date = date("Y-m-d H:i:s");

        if (!$model->save()) {
            throw new Exception(Yii::t('app', Html::errorSummary($model)));
        }
        return $this->redirect(['index']);
    }

    public function actionChangeLeadSource($id, $status){
        $model = $this->findModel($id);

        $model->reg_source = $status;
        $model->status_date = date("Y-m-d H:i:s");


        if (!$model->save()) {
            throw new Exception(Yii::t('app', Html::errorSummary($model)));
        }
        return $this->redirect(['index']);
    }

    /**
    * Save Data.
    * @param object $model
    * @return mixed
    */
    private function saveData($model){
        if (isset($model)) {
            //$transaction = Yii::$app->db->beginTransaction();
            try {
                $post = Yii::$app->request->post();
                
                if (!$model->save()) {
                    throw new Exception(Yii::t('app', Html::errorSummary($model)));
                }

                $this->saveOthersInformation($model);
                //$transaction->commit();
                Yii::$app->session->setFlash('success', Yii::t('app', 'Record successfully saved.'));
                $this->_redirect($model);
            }
            catch (Exception $e) {
                //$transaction->rollBack();
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
    }
    
    private function _redirect($model){
        $action = Yii::$app->request->post('action', 'save');
        switch ($action) {
            case 'save':
                return $this->redirect(['index']);
                break;
            case 'apply':
                return $this->redirect(['update', 'id' => $model->id]);
                break;
            case 'save2new':
                return $this->redirect(['create']);
                break;
            default:
                return $this->redirect(['index']);
        }
    }

    public function actionContactInfo($contactid){
        $contactInfo = Contacts::findOne($contactid);
        echo Json::encode($contactInfo);
    }
    /**
     * Finds the ContactsRegistration model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ContactsRegistration the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ContactsRegistration::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    private function saveOthersInformation($model){
        $contact_system_model=$this->saveSystemLog($model);
        $this->saveTagsInfo($contact_system_model,$model->contact_id);
        $baseUrl = Yii::getAlias('@baseUrl');
        $cars_contact_url=$baseUrl . '/backend/web/contacts/contacts-registration/update?id=' . $model->id;
        MaropostHelper::sendNewLeadEmail($contact_system_model,$cars_contact_url);
    }

    public function saveSystemLog($model){
        $realtor='';
       if ($model->reg_type==2){
           $realtor=1;
       }
       elseif ($model->reg_type==1){
           $realtor='';
       }
        $browser=$this->getBrowserInfo();
        $contactInfo=Contacts::find()->where(['id'=>$model->contact_id])->one();
        $mediaLeads = MediaLeads::find()->where(['id' => $model->reg_source])->one();
        $develoment = Developments::find()->where(['development_id' =>$model->development_id])->one();
        $page_url = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";

        $contactsSystemInfo = new ContactsSysteminfo(); //Contact System Information Model Get
        $contactsSystemInfo->first_name        = $contactInfo->first_name;
        $contactsSystemInfo->contact_id        = $model->contact_id;
        $contactsSystemInfo->last_name         = $contactInfo->last_name;
        $contactsSystemInfo->email             = $contactInfo->email;
        $contactsSystemInfo->phone             = $contactInfo->phone;
        $contactsSystemInfo->media_source      = $mediaLeads->lead_source_name;
        $contactsSystemInfo->development_id    = $model->development_id;
        $contactsSystemInfo->development_name  = $develoment->development_name;
        $contactsSystemInfo->page_name         = $develoment->development_name;
        $contactsSystemInfo->page_url          =$page_url;
        $contactsSystemInfo->brand_id          =1;
        $contactsSystemInfo->form_type         ='registration';
        $contactsSystemInfo->affiliate_tag     =strtolower($develoment->development_name).' - '.'Registration Journey';
        $contactsSystemInfo->ip_address        = $browser['ip_address'];
        $contactsSystemInfo->browser           = $browser['name'];
        $contactsSystemInfo->operating         = $browser['platform'];
        $contactsSystemInfo->device            = $browser['device'];
        $contactsSystemInfo->visit_date        = date('Y-m-d H:i:s');
        $contactsSystemInfo->realtor           =$realtor;
        $contactsSystemInfo->log_create_status =0;
        if (!$contactsSystemInfo->save(false)) {
            throw new Exception(json_encode($contactsSystemInfo->errors));
        }
        return $contactsSystemInfo;
    }
    //@@Browser Information get
    public function getBrowserInfo() {
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version= "";
        $deveice='';
        if (stristr($_SERVER['HTTP_USER_AGENT'],'mobi')!==FALSE) {
            $deveice='Mobile';
        }
        else{
            $deveice='Desktop';
        }

        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'linux';
        } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'mac';
        } elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'windows';
        }
        if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) {
            $bname = 'Internet Explorer';
            $ub = "MSIE";
        } elseif(preg_match('/Firefox/i',$u_agent)) {
            $bname = 'Mozilla Firefox';
            $ub = "Firefox";
        } elseif(preg_match('/Chrome/i',$u_agent)) {
            $bname = 'Google Chrome';
            $ub = "Chrome";
        } elseif(preg_match('/Safari/i',$u_agent)) {
            $bname = 'Apple Safari';
            $ub = "Safari";
        } elseif(preg_match('/Opera/i',$u_agent)) {
            $bname = 'Opera';
            $ub = "Opera";
        } elseif(preg_match('/Netscape/i',$u_agent)) {
            $bname = 'Netscape';
            $ub = "Netscape";
        }
        $known = array('Version', $ub, 'other');
        $pattern = '#(?<browser>' . join('|', $known) . ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
        }
        $i = count($matches['browser']);
        if ($i != 1) {
            if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
                $version= $matches['version'][0];
            } else {
                $version= $matches['version'][1];
            }
        } else {
            $version= $matches['version'][0];
        }
        if ($version==null || $version=="") {$version="?";}

        if (!empty($_SERVER['HTTP_CLIENT_IP']))
        {
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            $ip=$_SERVER['REMOTE_ADDR'];
        }

        return array(
            'userAgent' => $u_agent,
            'name'      => $bname,
            'version'   => $version,
            'platform'  => $platform,
            'pattern'    => $pattern,
            'ip_address'=>$ip,
            'device'=>$deveice,
        );
    }

    private function saveTagsInfo($contact_system_model,$contact_id){
        $contactsTag = new ContactsTagsNew();
        $contactsTag->tag_name =strtolower($contact_system_model->affiliate_tag);
        $contactsTag->tag_category_id=1;
        $checkTags = ContactsTagsNew::find()->where(['tag_name' => $contactsTag->tag_name])->one();
        if (empty($checkTags)){
            if ($contactsTag->save(false)) {
                //save data relation table
                $tagRelation=new ContactsTagsRelation();
                $tagRelation->contact_id=$contact_id;
                $tagRelation->tag_id=$contactsTag->id;
                $tagRelation->date_applied= date('Y-m-d H:i:s');
                if (!$tagRelation->save(false)) {
                    throw new Exception(json_encode($tagRelation->errors));
                }
                //save data to applied tag
                $tagApplied=new ContactsTagsApplied();
                $tagApplied->tag_id=$contactsTag->id;
                if (!$tagApplied->save(false)) {
                    throw new Exception(json_encode($tagApplied->errors));
                }

            }
        }
        else{
            $tagRelation=new ContactsTagsRelation();
            $tagRelation->contact_id=$contact_id;
            $tagRelation->tag_id=$checkTags->id;
            $tagRelation->date_applied= date('Y-m-d H:i:s');
            if (!$tagRelation->save(false)) {
                throw new Exception(json_encode($tagRelation->errors));
            }
            $tagApplied=new ContactsTagsApplied();
            $tagApplied->tag_id=$checkTags->id;
            if (!$tagApplied->save(false)) {
                throw new Exception(json_encode($tagApplied->errors));
            }
        }
    }

}

<?php

namespace unlock\modules\contacts\controllers;

use api\common\maropost\tags;
use unlock\modules\contacts\models\Contacts;
use unlock\modules\contacts\models\ContactsImportLog;
use Yii;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use unlock\modules\core\helpers\CommonHelper;
use api\common\maropost\maropost;

/**
 * ContactsEmailLogController implements the CRUD actions for ContactsEmailLog model.
 */
class ContactsImportController extends Controller
{
    /**
     * {@inheritdoc}
     */
    protected $maropost_connection;
    protected $tags_connection;
    protected $contact_connection;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    //Maropost Import Data Triger On
    public function actionMaropostImport()
    {
        $this->maropost_connection  =new maropost();
        $this->tags_connection      =new tags();
        $this->contact_connection   = $this->maropost_connection->contacts();
        $contactsList               =Contacts::find()->where(['maropost_import_status'=>'0'])->orderBy(['id' => SORT_ASC])->all();

        $total_import=$total_new_email=$total_existing_email=$total_subscribe_email=$total_unsubscribe_email=0;
        $exist_row=true;
        set_time_limit(0);
        $execution_time_count = microtime(TRUE);

        if (!empty($contactsList)) {
            foreach ($contactsList as $contactsListItem) {
                //Save Maropost
                $response = self::maropostCallback($contactsListItem);
                //Get Back Maropost
                $get_contact_by_email = $this->contact_connection->get_contact_by_contact_email(!empty($contactsListItem->email) ? $contactsListItem->email : '');
                $user_existing_status = 1;
                $subscriptions_status = 1;
                if ($get_contact_by_email['http_status'] == 404) {
                    $user_existing_status = 0;
                } else {
                    $list_subscriptions = $get_contact_by_email['list_subscriptions'];
                    foreach ($list_subscriptions as $subscriptions) {
                    }
                    if ($subscriptions['status'] == 'Unsubscribed') {
                        $subscriptions_status = 0;
                    }
                }
                //Email count exists or not
                if ($user_existing_status == 1) {
                    $total_existing_email++;
                } else {
                    $total_new_email++;
                }
                //Subscribe count
                if ($subscriptions_status == 1) {
                    $total_subscribe_email++;
                } else {
                    $total_unsubscribe_email++;
                }
                $total_import++;
                //update status
                $updatecontact = Contacts::findOne($contactsListItem->id);
                $updatecontact->maropost_import_status = 1;
                $updatecontact->save();

                $exist_row = false;
            }
            $execution_time_count1 = microtime(TRUE);
            $time_diffrence = $execution_time_count1 - $execution_time_count;

            //If finished then save to import log
            if ($exist_row == false) {
                $maxWaitTime = gmdate("H:i:s.u", $time_diffrence); //(HOURS:MINUTES:SECONDS:MILLISECONDS)
                $import_log = new ContactsImportLog();
                $import_log->total_import = $total_import;
                $import_log->total_new_email = $total_new_email;
                $import_log->total_exist_email = $total_existing_email;
                $import_log->execution_time = $maxWaitTime;
                $import_log->total_subscribe_email = $total_subscribe_email;
                $import_log->total_unsubscribe_email = $total_unsubscribe_email;
                $import_log->status = 1;
                if ($import_log->save()) {
                    echo "<h1 style='text-align: center;color: green'>Successfully Import To Maropost ! Total Contact :" . $total_import . "</h1>";
                }
            }
        }else{
            echo "<h1 style='text-align: center;color: green'>No data Prepare</h1>";
        }
    }

    //Maropost callback
    private function maropostCallback($model){
        $media_source_id=!empty($model->lead_source)?$model->lead_source:'';
        $tag_name=$maropost_tag_name=$list_name='';
        if (!empty($model->contact_type) && $model->contact_type==2) {
            $contact_type='realtor';
            $realtor_ = 'true';
            $prospect = 'False';
            $realtor = 1;
            $realtor_tag = 'Realtor';
            $list_id = 17;
            $list_name='CONNECT Realtor List (Dedicated IP)';
        }
        else{
            $contact_type='prospect';
            $realtor_ = 'False';
            $prospect = 'true';
            $realtor_tag = 'Prospect';
            $realtor = 0;
            $list_id = 1;
            $list_name='Investor Alerts (Dedicated IP),Newsletter (Dedicated IP)';
        }
        $tag_name='import'.'-'.$contact_type;

        $mediaLeads_name =CommonHelper::leadSourceByBrandId($media_source_id);

        $city_model=CommonHelper::cityListByCityId($model->city);

        if (empty($city_model)){
            $cityname="NULL";
        }
        else{
            $cityname=ArrayHelper::getValue($city_model,'city_name');
        }

        $provinec_model=CommonHelper::provinceListByProvinceId($model->province);

        if (empty($provinec_model)){
            $province_name="NULL";
        }
        else{
            $province_name=ArrayHelper::getValue($provinec_model,'province_name');
        }

        $country_model=CommonHelper::countryListByCountryId($model->country);

        if (empty($country_model)){
            $country_name="NULL";
        }
        else{
            $country_name=ArrayHelper::getValue($country_model,'country_name');
        }
        $registraion_data['media_source']='Original Media Source -'.$mediaLeads_name;

        $array1 = array(
            "tags" => array(
                "name" =>$registraion_data['media_source']
            )
        );
        $this->tags_connection->post_tags($array1);

        if (!empty($tag_name)){
            $array2 = array(
                "tags" => array(
                    "name" =>$tag_name
                )
            );
            $this->tags_connection->post_tags($array2);
            $maropost_tag_name=$tag_name;
        }
        if (!empty($mediaLeads_name)) {
            $registraion_data['media_source_tag'] = 'Original Media Source -' . $mediaLeads_name;
        }
        else{
            $registraion_data['media_source_tag']=$mediaLeads_name;
        }
        $assigned_user = CommonHelper::contactIdByRegistrationListDevelopmentId($model->id);
        $assign_user_name=ucfirst($assigned_user);

        $arrayNew = array(
            'list_id' => $list_id,
            'contact' =>
                array(
                    'first_name' 	=> $model->first_name,
                    'last_name' 	=> $model->last_name,
                    'email' 		=> $model->email,
                    'subscribe'  	=> 'true',
                    'custom_field' =>
                        array(
                            'phone_1' => $model->phone,
                            'company' => 'CONNECT asset management',
                            'realtor' => $realtor_,
                            'prospect' => $prospect,
                            'full_name' 	=> $model->first_name.' '.$model->last_name,
                            'address' 	    => $model->address,
                            'city' 	        => $cityname,
                            'province' 	    => $province_name,
                            'assigned_to' 	=> $assign_user_name,
                            'country' 	    => $country_name,
                            'postal'=>$model->postal,
                            'birthday'=>$model->date_of_birth
                        ),
                    'add_tags' =>
                        array(
                            $registraion_data['media_source_tag'],
                            $maropost_tag_name,
                            $realtor_tag,
                        )
                ));

        $arrayNewsletter = $arrayNew;
        if($arrayNewsletter['list_id'] == 1){
            $arrayNewsletter['list_id'] = 3;
            $this->contact_connection->post_new_contact_into_list($arrayNewsletter);
        }
        $response=$this->contact_connection->post_new_contact_into_list($arrayNew);

        return $response;
    }

}

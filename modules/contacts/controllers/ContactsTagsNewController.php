<?php

namespace unlock\modules\contacts\controllers;
use api\common\maropost\maropost;
use api\common\maropost\tags;
use unlock\modules\contacts\Contacts;
use unlock\modules\contacts\models\ContactsTagsRelation;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\base\Exception;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use unlock\modules\contacts\models\ContactsTagsNew;
use unlock\modules\contacts\models\ContactsTagsNewSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use unlock\modules\core\helpers\Collection;
use unlock\modules\core\helpers\FileHelper;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\actions\ExportCsvAction;
use unlock\modules\core\actions\ExportPdfAction;


/**
 * ContactsTagsNewController implements the CRUD actions for ContactsTagsNew model.
 */
class ContactsTagsNewController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => Yii::$app->DynamicAccessRules->customAccess('contacts/contactstagsnew'),
        ];
    }
    
    /**
     * Lists all ContactsTagsNew models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContactsTagsNewSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        Collection::setData($dataProvider);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ContactsTagsNew model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ContactsTagsNew model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ContactsTagsNew();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $this->saveData($model);
        }

        return $this->render('_form', [
            'model' => $model,
        ]);
    }

    public function actionQuickCreate()
    {
        $model = new ContactsTagsNew();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $contact_id=$_REQUEST['contactid'];
            $tag_id=$_REQUEST['ContactsTagsNew']['tag_name'];

           $contact_information=\unlock\modules\contacts\models\Contacts::find()->where(['id' => $contact_id])->one();

            if (!empty($contact_information->email)){
                $email=ArrayHelper::getValue($contact_information,'email');
            }
            if (!empty($contact_information->first_name)){
                $fname=ArrayHelper::getValue($contact_information,'first_name');
            }
            else{
                $fname='';
            }

            if (!empty($contact_information->last_name)){
                $lname=ArrayHelper::getValue($contact_information,'last_name');
            }
            else{
                $lname='NULL';
            }

            if (!empty($contact_information->phone)){
                $phone=ArrayHelper::getValue($contact_information,'phone');
            }
            else{
                $phone='NULL';
            }

            $tag_name='';
           if (!empty($tag_id)){
               $tag_information=ContactsTagsNew::find()->where(['id'=>$tag_id])->one();
               $tag_name=$tag_information->tag_name;
           }

            $tagRelation=new ContactsTagsRelation();
            $tagRelation->contact_id=$contact_id;
            $tagRelation->tag_id=$tag_id;
            $tagRelation->date_applied= date('Y-m-d H:i:s');
            if ($tagRelation->save()) {
                $this->saveToMaropost($fname,$lname,$email,$phone,$tag_name);
                die();
            }
        }
        return $this->renderAjax('_quickform', [
            'model' => $model,
        ]);
    }

    public  function  saveToMaropost($fname,$lname,$email,$phone,$tagname){

        $maropost = new maropost();

        $tags=new tags();
        $array = array(
            "tags" => array(
                "name" =>$tagname
            )
        );
        $tags->post_tags($array);

        $contactsNew 	= $maropost->contacts();
        $list_id=1;
        $registraion_data['infusionsoftTag']=$tagname;
        $arrayNew = array(
            'list_id' => $list_id,
            'contact' =>
                array(
                    'first_name' =>$fname,
                    'last_name' => $lname,
                    'email' => $email,
                    'subscribe' => 'true',
                    'custom_field' =>
                        array(
                            'phone_1' => $phone,
                            'company' => 'CONNECT asset management',
                            'realtor' =>'False',
                            'prospect' => 'true',
                        ),
                    'add_tags' =>
                        array(
                            $registraion_data['infusionsoftTag'],
                            $tagname
                        )
                ));

        $arrayNewsletter = $arrayNew;
        if($arrayNewsletter['list_id'] == 1){
            $arrayNewsletter['list_id'] = 3;
            $contactsNew->post_new_contact_into_list($arrayNewsletter);
        }
        $contactsNew->post_new_contact_into_list($arrayNew);
    }

    /**
     * Updates an existing ContactsTagsNew model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $this->saveData($model);
        }

        return $this->render('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ContactsTagsNew model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        try {
            if (!$model->delete()) {
                throw new Exception(Yii::t('app', 'The record cannot be deleted.'));
            }

            // Delete Image:
            // FileHelper::removeFile($model->image, 'sanction');
            Yii::$app->session->setFlash('success', Yii::t('app', 'The record have been successfully deleted.'));
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            if($e->getName() == CommonHelper::INTEGRITY_CONSTRAINT_VIOLATION){
                $message = "This record has relational data. You must delete the following data first";
                $message .= "<ul>";
                $message .= "<li>Example => Example List</li>";
                $message .= "</ul>";
            }
            Yii::$app->session->setFlash('error', $message);
        }

        return $this->redirect(['index']);
    }

    /**
    * Save Data.
    * @param object $model
    * @return mixed
    */
    private function saveData($model){
        if (isset($model)) {
            //$transaction = Yii::$app->db->beginTransaction();
            try {
                $post = Yii::$app->request->post();
                
                if (!$model->save()) {
                    throw new Exception(Yii::t('app', Html::errorSummary($model)));
                }

                //$transaction->commit();
                Yii::$app->session->setFlash('success', Yii::t('app', 'Record successfully saved.'));
                $this->_redirect($model);
            }
            catch (Exception $e) {
                //$transaction->rollBack();
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
    }
    
    private function _redirect($model){
        $action = Yii::$app->request->post('action', 'save');
        switch ($action) {
            case 'save':
                return $this->redirect(['index']);
                break;
            case 'apply':
                return $this->redirect(['update', 'id' => $model->id]);
                break;
            case 'save2new':
                return $this->redirect(['create']);
                break;
            default:
                return $this->redirect(['index']);
        }
    }

    /**
     * Finds the ContactsTagsNew model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ContactsTagsNew the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ContactsTagsNew::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}

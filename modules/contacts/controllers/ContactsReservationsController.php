<?php

namespace unlock\modules\contacts\controllers;

use unlock\modules\contacts\models\Contacts;
use Yii;
use yii\helpers\Html;
use yii\base\Exception;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use unlock\modules\contacts\models\ContactsReservations;
use unlock\modules\contacts\models\ContactsReservationsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use unlock\modules\core\helpers\Collection;
use unlock\modules\core\helpers\FileHelper;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\actions\ExportCsvAction;
use unlock\modules\core\actions\ExportPdfAction;
use yii\helpers\Json;
use unlock\modules\generalsettings\models\City;

/**
 * ContactsReservationsController implements the CRUD actions for ContactsReservations model.
 */
class ContactsReservationsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => Yii::$app->DynamicAccessRules->customAccess('contacts/contactsreservations'),
        ];
    }
    
    /**
     * Lists all ContactsReservations models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContactsReservationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        Collection::setData($dataProvider);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ContactsReservations model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ContactsReservations model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ContactsReservations();
        $contactsModel = new Contacts();


        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {

            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $contactsModel->load(Yii::$app->request->post())) {
            if(!empty($model->contact_id)){
                $contactsUpdate = Contacts::findOne($model->contact_id);
                $contactsUpdate->save(false);
            }else{
                $contactsModel->save();
                $model->contact_id = $contactsModel->id;
            }
            if(!empty($model->nature_of_purchase)){
                $model->nature_of_purchase = implode(', ', $model->nature_of_purchase);
            }

            $this->saveData($model,$contactsModel);

        }

        return $this->render('_form', [
            'model' => $model,
            'contactsModel' => $contactsModel,
        ]);
    }


    public function actionQuickCreate()
    {

        $model = new ContactsReservations();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            if(!empty($model->nature_of_purchase)){
                $model->nature_of_purchase = implode(', ', $model->nature_of_purchase);
            }
            if($model->save()){
                echo '1';
                return $this->redirect(['contacts/view', 'id' => $_REQUEST['contactid']]);
            }else{
                echo '0';
            }
            return \yii\widgets\ActiveForm::validate($model);
        }
        return $this->renderAjax('_quickform', [
            'model' => $model,
        ]);
    }
    /**
     * Updates an existing ContactsReservations model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $contactsModel = Contacts::findOne($model->contact_id);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $contactsModel->load(Yii::$app->request->post() )) {
            if(!empty($model->nature_of_purchase)){
                $model->nature_of_purchase = implode(', ', $model->nature_of_purchase);
            }
            $this->saveData($model, $contactsModel);
        }

        return $this->render('_form', [
            'model' => $model,
            'contactsModel' => $contactsModel,
        ]);
    }

    /**
     * Deletes an existing ContactsReservations model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        try {
            if (!$model->delete()) {
                throw new Exception(Yii::t('app', 'The record cannot be deleted.'));
            }

            // Delete Image:
            // FileHelper::removeFile($model->image, 'sanction');
            Yii::$app->session->setFlash('success', Yii::t('app', 'The record have been successfully deleted.'));
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            if($e->getName() == CommonHelper::INTEGRITY_CONSTRAINT_VIOLATION){
                $message = "This record has relational data. You must delete the following data first";
                $message .= "<ul>";
                $message .= "<li>Example => Example List</li>";
                $message .= "</ul>";
            }
            Yii::$app->session->setFlash('error', $message);
        }

        return $this->redirect(['index']);
    }

    /**
    * Save Data.
    * @param object $model
    * @return mixed
    */
    private function saveData($model, $contactsModel){
        if (isset($model)) {
            //$transaction = Yii::$app->db->beginTransaction();
            try {
                $post = Yii::$app->request->post();
                
                if (!$contactsModel->save()) {
                    throw new Exception(Yii::t('app', Html::errorSummary($contactsModel)));
                }

                if (!$model->save()) {
                    throw new Exception(Yii::t('app', Html::errorSummary($model)));
                }

                //$transaction->commit();
                Yii::$app->session->setFlash('success', Yii::t('app', 'Record successfully saved.'));
                $this->_redirect($model);
            }
            catch (Exception $e) {
                //$transaction->rollBack();
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
    }
    
    private function _redirect($model){
        $action = Yii::$app->request->post('action', 'save');
        switch ($action) {
            case 'save':
                return $this->redirect(['index']);
                break;
            case 'apply':
                return $this->redirect(['update', 'id' => $model->id]);
                break;
            case 'save2new':
                return $this->redirect(['create']);
                break;
            default:
                return $this->redirect(['index']);
        }
    }


    public function actionContactInfo($contactid){
        $contactInfo = Contacts::findOne($contactid);
        echo Json::encode($contactInfo);
    }
    /**
     * Finds the ContactsReservations model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ContactsReservations the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ContactsReservations::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    // City List
    public function actionCity($id){

        $sectionCount = City::find()->where(['province_id' => $id])->count();
        $sectionAll = City::find()->where(['province_id' => $id])->all();

        if($sectionCount > 0){
            echo '<option>Select a City...</option>';
            foreach ($sectionAll as $value){
                echo '<option value="'.$value->id.'">'.$value->city_name.'</option>';
            }
        }else{
            echo '<option>Select a City...</option>';
        }

    }

    public function actionChangeReservationStatus($id, $status){
        $model = $this->findModel($id);
        $model->status = $status;
        /*$model->status_date = date("Y-m-d H:i:s");*/
        if (!$model->save()) {
            throw new Exception(Yii::t('app', Html::errorSummary($model)));
        }
        return $this->redirect(['index']);
    }

}

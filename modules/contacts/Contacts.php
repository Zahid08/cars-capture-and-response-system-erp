<?php

namespace unlock\modules\contacts;

/**
 * Class Contacts
 */
class Contacts extends \yii\base\Module
{
    /**
     * @var string the namespace that controller classes are in
     */
    public $controllerNamespace = 'unlock\modules\contacts\controllers';

    /**
    * @var string the namespace that view file are in
    */
    public $viewNamespace = '@unlock/modules/contacts/views/';
}

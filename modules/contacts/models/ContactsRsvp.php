<?php

namespace unlock\modules\contacts\models;

use Yii;
use yii\helpers\ArrayHelper;
use unlock\modules\core\helpers\CommonHelper;

/**
 * This is the model class for table "{{%contacts_rsvp}}".
 *
 * @property integer $id
 * @property integer $contact_id
 * @property integer $event_id
 * @property integer $rsvp_source
 * @property string $rsvp_date
 * @property integer $rsvp_status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 */
class ContactsRsvp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public  $name;
    public static function tableName()
    {
        return '{{%contacts_rsvp}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contact_id','development_id', 'event_id', 'rsvp_source', 'rsvp_status', 'created_by', 'updated_by'], 'integer'],
            [['event_id', 'rsvp_source', 'rsvp_status', 'rsvp_date'], 'required'],
            [['rsvp_date', 'created_at', 'updated_at','rsvp_event'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'contact_id' => Yii::t('app', 'Contact Name'),
            'development_id' => Yii::t('app', 'Development Name'),
            'name' => Yii::t('app', 'Contact Name'),
            'event_id' => Yii::t('app', 'Event Name'),
            'rsvp_source' => Yii::t('app', 'Media Source'),
            'rsvp_date' => Yii::t('app', 'Submission Date'),
            'rsvp_status' => Yii::t('app', 'Rsvp Status'),
            'rsvp_event' => Yii::t('app', 'Rsvp Event'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /** Get All Active ContactsRsvp Drop Down List*/
    public static function contactsRsvpDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }
}

<?php

namespace unlock\modules\contacts\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use unlock\modules\contacts\models\ContactsEmailLog;

/**
 * ContactsEmailLogSearch represents the model behind the search form of `unlock\modules\contacts\models\ContactsEmailLog`.
 */
class ContactsEmailLogSearch extends ContactsEmailLog
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'contact_id', 'user_id', 'status'], 'integer'],
            [['subject', 'mail_header', 'mail_body', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ContactsEmailLog::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'contact_id' => $this->contact_id,
            'user_id' => $this->user_id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'subject', $this->subject])
            ->andFilterWhere(['like', 'mail_header', $this->mail_header])
            ->andFilterWhere(['like', 'mail_body', $this->mail_body])
            ->orderBy(  ['id' => SORT_DESC]);

        return $dataProvider;
    }
}

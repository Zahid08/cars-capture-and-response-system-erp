<?php

namespace unlock\modules\contacts\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use unlock\modules\contacts\models\ContactsImportLog;

/**
 * ContactsImportLogSearch represents the model behind the search form of `unlock\modules\contacts\models\ContactsImportLog`.
 */
class ContactsImportLogSearch extends ContactsImportLog
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['total_import', 'total_new_email','status','execution_time','total_exist_email','total_subscribe_email', 'total_unsubscribe_email', 'created_by', 'updated_by'], 'safe'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ContactsImportLog::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'total_import' => $this->total_import,
            'total_new_email' => $this->total_new_email,
            'total_subscribe_email' => $this->total_subscribe_email,
            'total_unsubscribe_email' => $this->total_unsubscribe_email,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'total_exist_email', $this->total_exist_email]);

        return $dataProvider;
    }
}

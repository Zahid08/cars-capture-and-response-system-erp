<?php

namespace unlock\modules\contacts\models;

use unlock\modules\core\helpers\CommonHelper;
use Yii;

/**
 * This is the model class for table "conn_contacts_import_log".
 *
 * @property int $id
 * @property int $total_import
 * @property string $maropost_contact_list
 * @property int $total_duplicated_email
 * @property int $total_subscribe_email
 * @property int $total_unsubscribe_email
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class ContactsImportLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'conn_contacts_import_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['total_import', 'total_new_email','status','execution_time','total_exist_email','total_subscribe_email', 'total_unsubscribe_email', 'created_by', 'updated_by'], 'safe'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'total_import' => 'Total Import',
            'total_new_email' => 'New Email',
            'total_exist_email' => 'Exist Email',
            'total_subscribe_email' => 'Subscribe',
            'total_unsubscribe_email' => 'Unsubscribe',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }

}

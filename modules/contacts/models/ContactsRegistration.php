<?php

namespace unlock\modules\contacts\models;

use Yii;
use yii\helpers\ArrayHelper;
use unlock\modules\core\helpers\CommonHelper;

/**
 * This is the model class for table "{{%contacts_registration}}".
 *
 * @property integer $id
 * @property integer $contact_id
 * @property integer $development_id
 * @property string $reg_source
 * @property string $reg_datetime
 * @property string $status
 * @property string $status_date
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 *
 * @property Contacts $contact
 */
class ContactsRegistration extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $name;
    public static function tableName()
    {
        return '{{%contacts_registration}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reg_source', 'reg_type', 'contact_id', 'development_id', 'created_by', 'updated_by'], 'integer'],
            [['reg_type', 'reg_datetime', 'status_date', 'created_at', 'updated_at','system_log_id'], 'safe'],
            [['contact_id', 'development_id', 'status'], 'required'],
            [['current_result'], 'string', 'max' => 100],
            [['contact_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contacts::className(), 'targetAttribute' => ['contact_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'contact_id' => Yii::t('app', 'Contact Name'),
            'name' => Yii::t('app', 'Contact Name'),
            'development_id' => Yii::t('app', 'Development Name'),
            'reg_type' => Yii::t('app', 'Type'),
            'reg_source' => Yii::t('app', 'Media Source'),
            'reg_datetime' => Yii::t('app', 'Date/Time'),
            'status' => Yii::t('app', 'Status'),
            'status_date' => Yii::t('app', 'Status Date'),
            'current_result' => Yii::t('app', 'Current Result'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(Contacts::className(), ['id' => 'contact_id']);
    }

    /** Get All Active ContactsRegistration Drop Down List*/
    public static function contactsRegistrationDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }
}

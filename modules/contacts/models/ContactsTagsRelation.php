<?php

namespace unlock\modules\contacts\models;

use unlock\modules\usermanager\user\models\User;
use Yii;
use yii\helpers\ArrayHelper;
use unlock\modules\core\helpers\CommonHelper;

/**
 * This is the model class for table "{{%contacts_tags_relation}}".
 *
 * @property string $id
 * @property string $packageId
 * @property string $orgIdPrefix
 * @property string $name
 * @property string $domain
 * @property string $email
 * @property string $favicon
 * @property string $logo
 * @property string $theme
 * @property string $skin
 * @property string $detailsinfo
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class ContactsTagsRelation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%contacts_tags_relation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contact_id','tag_id' ,'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'contact_id' => Yii::t('app', 'Contact ID'),
            'tag_id' => Yii::t('app', 'Tag Id'),
            'applied_date' => Yii::t('app', 'Applied Date'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }

    public function fields()
    {
        $fields = parent::fields();

        $additionalFields = [
            /*'userInfo' => function ($model) {
                return ArrayHelper::getValue($model, 'crBy0');
            },*/
        ];

        $result = ArrayHelper::merge($fields, $additionalFields);

        return $result;
    }

    public function extraFields()
    {
        return [
            /*'userInfo' => function ($model) {
                return ArrayHelper::getValue($model, 'crBy0');
            },*/
        ];
    }
}

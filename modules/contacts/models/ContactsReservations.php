<?php

namespace unlock\modules\contacts\models;

use Yii;
use yii\helpers\ArrayHelper;
use unlock\modules\core\helpers\CommonHelper;

/**
 * This is the model class for table "{{%contacts_reservations}}".
 *
 * @property integer $id
 * @property integer $contact_id
 * @property integer $development_id
 * @property string $floor_plans_1st_choice
 * @property string $floor_plans_2nd_choice
 * @property string $floor_plans_3rd_choice
 * @property string $floor_range
 * @property string $require_parking
 * @property string $require_locker
 * @property string $nature_of_purchase
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property integer $status
 */
class ContactsReservations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $name;
    public static function tableName()
    {
        return '{{%contacts_reservations}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contact_id', 'development_id', 'created_by', 'updated_by', 'status'], 'integer'],
            [['floor_range', 'require_parking', 'require_locker'], 'string'],
            [['floor_range', 'require_parking', 'require_locker'], 'required'],
            [['created_at', 'updated_at','nature_of_purchase'], 'safe'],
            [['floor_plans_1st_choice', 'floor_plans_2nd_choice', 'floor_plans_3rd_choice'], 'string', 'max' => 100],
            //[['nature_of_purchase'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'contact_id' => Yii::t('app', 'Contact Name'),
            'name' => Yii::t('app', 'Contact Name'),
            'development_id' => Yii::t('app', 'Development Name'),
            'floor_plans_1st_choice' => Yii::t('app', 'Floor Plans 1st Choice'),
            'floor_plans_2nd_choice' => Yii::t('app', 'Floor Plans 2nd Choice'),
            'floor_plans_3rd_choice' => Yii::t('app', 'Floor Plans 3rd Choice'),
            'floor_range' => Yii::t('app', 'Floor Range'),
            'require_parking' => Yii::t('app', 'Require Parking'),
            'require_locker' => Yii::t('app', 'Require Locker'),
            'nature_of_purchase' => Yii::t('app', 'Nature Of Purchase'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Status Date'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'status' => Yii::t('app', 'Reservation Status'),
        ];
    }

    /** Get All Active ContactsReservations Drop Down List*/
    public static function contactsReservationsDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }
}

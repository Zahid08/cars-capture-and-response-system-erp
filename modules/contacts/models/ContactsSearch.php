<?php

namespace unlock\modules\contacts\models;

use Yii;
use yii\base\Model;
use unlock\modules\core\data\ActiveDataProvider;
use unlock\modules\contacts\models\Contacts;
use unlock\modules\core\helpers\CommonHelper;

/**
 * ContactsSearch represents the model behind the search form about `unlock\modules\contacts\models\Contacts`.
 */
class ContactsSearch extends Contacts
{

    public $brands;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by'], 'integer'],
            [['suite', 'brands', 'allsearch', 'first_name', 'last_name', 'email', 'phone', 'address', 'city', 'province', 'postal', 'country', 'agent', 'social_insurance_number', 'drivers_license', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
       /* print_r("asd");exit(); SELECT * FROM `employees` LEFT JOIN states ON employees.id=states.fk_employee_id WHERE employees.status='active'
       Employees::find()
        ->leftJoin('states','employees.id=states.fk_employee_id')
        ->where(['employees.status'=>'active'])
        ->all();

       $query->leftJoin('post', 'post.user_id = user.id');
       */
        $query = Contacts::find();
        $query->leftJoin('conn_contacts_brands', 'conn_contacts_brands.contact_id = conn_contacts.id');

        // add conditions that should always apply here
        $pageSize = isset($params['per-page']) ? intval($params['per-page']) : CommonHelper::GRID_PER_PAGE;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created_at' => SORT_DESC]],
            'pagination' =>  ['pageSize' => $pageSize],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions


       /* $query->andFilterWhere([
            'id' => $this->id,

            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);*/


        if($this->brands) {
            $query->Where(['conn_contacts_brands.brand_id' => $this->brands]);
        }

        if($this->allsearch){

            $query->orFilterWhere(['like', 'email', $this->allsearch])
                ->orFilterWhere(['like', 'phone', $this->allsearch])
                ->orFilterWhere(['like', 'address', $this->allsearch])
                ->orFilterWhere(['like', 'suite', $this->allsearch])
                ->orFilterWhere(['like', 'city', $this->allsearch])
                ->orFilterWhere(['like', 'province', $this->allsearch])
                ->orFilterWhere(['like', 'postal', $this->allsearch])
                ->orFilterWhere(['like', 'country', $this->allsearch])
                ->orFilterWhere(['like', 'agent', $this->allsearch])
                ->orFilterWhere(['like', 'social_insurance_number', $this->allsearch])
                ->orFilterWhere(['like', 'drivers_license', $this->allsearch]);

            $query->orWhere('first_name LIKE "%' . $this->allsearch . '%" ' . //This will filter when only first name is searched.
                'OR last_name LIKE "%' . $this->allsearch . '%" '. //This will filter when only last name is searched.
                'OR CONCAT(first_name, " ", last_name) LIKE "%' . $this->allsearch . '%"' //This will filter when full name is searched.
            );

        }

        //echo '<pre>'; print_r($query); exit();

        return $dataProvider;
    }
}

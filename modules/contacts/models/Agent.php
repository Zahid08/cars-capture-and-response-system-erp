<?php

namespace unlock\modules\contacts\models;

use Yii;
use yii\helpers\ArrayHelper;
use unlock\modules\core\helpers\CommonHelper;

/**
 * This is the model class for table "{{%contacts_agent}}".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $mobile
 * @property string $office_phone
 * @property string $website
 * @property string $company_name
 * @property string $address
 * @property string $suite
 * @property integer $city
 * @property integer $province
 * @property string $postal_code
 * @property integer $country
 * @property integer $agent_type
 * @property integer $brand
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property integer $status
 */
class Agent extends \yii\db\ActiveRecord
{
    public $allsearch;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%contacts_agent}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city', 'province', 'country', 'agent_type', 'brand', 'created_by', 'updated_by', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['first_name', 'last_name', 'mobile', 'office_phone'], 'string', 'max' => 80],
            [['email', 'company_name', 'address'], 'string', 'max' => 100],
            [['website'], 'string', 'max' => 255],
            [['suite', 'postal_code'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'allsearch' => Yii::t('app', 'Search Anything'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'email' => Yii::t('app', 'Email'),
            'mobile' => Yii::t('app', 'Phone'),
            'office_phone' => Yii::t('app', 'Office Phone'),
            'website' => Yii::t('app', 'Website'),
            'company_name' => Yii::t('app', 'Company Name'),
            'address' => Yii::t('app', 'Address'),
            'suite' => Yii::t('app', 'Suite'),
            'city' => Yii::t('app', 'City'),
            'province' => Yii::t('app', 'Province'),
            'postal_code' => Yii::t('app', 'Postal Code'),
            'country' => Yii::t('app', 'Country'),
            'agent_type' => Yii::t('app', 'Type'),
            'brand' => Yii::t('app', 'Brand'),
            'created_at' => Yii::t('app', 'Create Date'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /** Get All Active Agent Drop Down List*/
    public static function agentDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }
}

<?php

namespace unlock\modules\contacts\models;

use Yii;
use yii\base\Model;
use unlock\modules\core\data\ActiveDataProvider;
use unlock\modules\contacts\models\Agent;
use unlock\modules\core\helpers\CommonHelper;

/**
 * AgentSearch represents the model behind the search form about `unlock\modules\contacts\models\Agent`.
 */
class AgentSearch extends Agent
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'city', 'province', 'country', 'agent_type', 'brand', 'created_by', 'updated_by', 'status'], 'integer'],
            [['first_name', 'allsearch', 'last_name', 'email', 'mobile', 'office_phone', 'website', 'company_name', 'address', 'suite', 'postal_code', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Agent::find();

        // add conditions that should always apply here
        $pageSize = isset($params['per-page']) ? intval($params['per-page']) : CommonHelper::GRID_PER_PAGE;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created_at' => SORT_DESC]],
            'pagination' =>  ['pageSize' => $pageSize],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
      /*  $query->andFilterWhere([
            'id' => $this->id,
            'city' => $this->city,
            'province' => $this->province,
            'country' => $this->country,
            'agent_type' => $this->agent_type,
            'brand' => $this->brand,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'status' => $this->status,
        ]);*/

        $query->orFilterWhere(['like', 'first_name', $this->allsearch])
            ->orFilterWhere(['like', 'last_name', $this->allsearch])
            ->orFilterWhere(['like', 'email', $this->allsearch])
            ->orFilterWhere(['like', 'mobile', $this->allsearch])
            ->orFilterWhere(['like', 'office_phone', $this->allsearch])
            ->orFilterWhere(['like', 'website', $this->allsearch])
            ->orFilterWhere(['like', 'company_name', $this->allsearch])
            ->orFilterWhere(['like', 'address', $this->allsearch])
            ->orFilterWhere(['like', 'suite', $this->allsearch])
            ->orFilterWhere(['like', 'postal_code', $this->allsearch])
            ->orFilterWhere(['like', 'brand', $this->allsearch]);

        return $dataProvider;
    }
}

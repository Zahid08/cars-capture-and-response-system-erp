<?php

namespace unlock\modules\contacts\models;

use Yii;
use yii\base\Model;
use unlock\modules\core\data\ActiveDataProvider;
use unlock\modules\contacts\models\ContactsPurchases;
use unlock\modules\core\helpers\CommonHelper;

/**
 * ContactsPurchasesSearch represents the model behind the search form about `unlock\modules\contacts\models\ContactsPurchases`.
 */
class ContactsPurchasesSearch extends ContactsPurchases
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'contact_id', 'development_id', 'deal_status', 'created_by', 'updated_by'], 'integer'],
            [['suite', 'purchase_date', 'occupancy', 'purchase_price', 'pm', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ContactsPurchases::find();

        // add conditions that should always apply here
        $pageSize = isset($params['per-page']) ? intval($params['per-page']) : CommonHelper::GRID_PER_PAGE;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created_at' => SORT_DESC]],
            'pagination' =>  ['pageSize' => $pageSize],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'contact_id' => $this->contact_id,
            'development_id' => $this->development_id,
            'purchase_date' => $this->purchase_date,
            'deal_status' => $this->deal_status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'suite', $this->suite])
            ->andFilterWhere(['like', 'occupancy', $this->occupancy])
            ->andFilterWhere(['like', 'purchase_price', $this->purchase_price])
            ->andFilterWhere(['like', 'pm', $this->pm]);

        return $dataProvider;
    }
}

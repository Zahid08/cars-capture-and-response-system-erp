<?php

namespace unlock\modules\contacts\models;

use api\modules\v1\models\ContactsBrands;
use Yii;
use yii\helpers\ArrayHelper;
use unlock\modules\core\helpers\CommonHelper;

/**
 * This is the model class for table "{{%clients_contacts}}".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 * @property string $address
 * @property string $city
 * @property string $province
 * @property string $postal
 * @property string $country
 * @property string $agent
 * @property string $social_insurance_number
 * @property string $drivers_license
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 *
 * @property ContactsRegistration[] $ContactsRegistrations
 */
class Contacts extends \yii\db\ActiveRecord
{

    public $allsearch;
    public $meadia_source;
    public $brand_name;
    public $original_media_source;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%contacts}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city', 'province', 'country', 'agent','suite','date_of_birth', 'SIN', 'created_at', 'updated_at','contact_type', 'brand_name'], 'safe'],
            [['lead_source', 'status', 'created_by', 'updated_by'], 'integer'],
            [['email'], 'required'],
            [['email'], 'unique'],
            [['first_name', 'last_name', 'phone'], 'string', 'max' => 80],
            [['lead_score', 'land_line', 'profession', 'email', 'social_insurance_number', 'drivers_license'], 'string', 'max' => 100],
            [['address'], 'string', 'max' => 255],

            [['postal'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'allsearch' => Yii::t('app', 'Search Anything'),
            'id' => Yii::t('app', 'ID'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'address' => Yii::t('app', 'Address'),
            'suite' => Yii::t('app', 'Suite'),
            'city' => Yii::t('app', 'City'),
            'province' => Yii::t('app', 'Province'),
            'postal' => Yii::t('app', 'Postal'),
            'country' => Yii::t('app', 'Country'),
            'agent' => Yii::t('app', 'Agent'),
            'social_insurance_number' => Yii::t('app', 'Social Insurance Number'),
            'drivers_license' => Yii::t('app', 'Drivers License'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'date_of_birth' => Yii::t('app', 'Date of Birth (yyyy-mm-dd) '),
            'SIN' => Yii::t('app', 'S.I.N'),
            'lead_source' => Yii::t('app', 'Original Media Source'),
            'lead_score' => Yii::t('app', 'Lead Score'),
            'meadia_source' => Yii::t('app', 'Original Media Source'),
            'status' => Yii::t('app', 'Status'),
            'land_line' => Yii::t('app', 'Land Line'),
            'profession' => Yii::t('app', 'Profession'),
            'contact_type' => Yii::t('app', 'Contact Type'),
            'brand_name' => Yii::t('app', 'Brand Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactsRegistrations()
    {
        return $this->hasMany(ContactsRegistration::className(), ['contact_id' => 'id']);
    }

    /** Get All Active Contacts Drop Down List*/
    public static function ContactsDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    public function getBrandswisecontacts(){
        return $this->hasMany(ContactsBrands::className(), ['contact_id' => 'id']);
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }
}

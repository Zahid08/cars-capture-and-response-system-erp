<?php

namespace unlock\modules\contacts\models;

use unlock\modules\core\helpers\CommonHelper;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use unlock\modules\contacts\models\ContactsSysteminfo;

/**
 * ContactsSysteminfoSearch represents the model behind the search form of `unlock\modules\contacts\models\ContactsSysteminfo`.
 */
class ContactsSysteminfoSearch extends ContactsSysteminfo
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'contact_id'], 'integer'],
            [['page_name', 'page_url', 'log_note', 'ip_address', 'browser', 'operating', 'visit_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ContactsSysteminfo::find();

        // add conditions that should always apply here
        $pageSize = isset($params['per-page']) ? intval($params['per-page']) : CommonHelper::GRID_PER_PAGE;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' =>  ['pageSize' => $pageSize],

        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'contact_id' => $this->contact_id,
            'visit_date' => $this->visit_date,
        ]);

        $query->andFilterWhere(['like', 'page_name', $this->page_name])
            ->andFilterWhere(['like', 'page_url', $this->page_url])
            ->andFilterWhere(['like', 'log_note', $this->log_note])
            ->andFilterWhere(['like', 'ip_address', $this->ip_address])
            ->andFilterWhere(['like', 'browser', $this->browser])
            ->andFilterWhere(['like', 'operating', $this->operating])
            ->orderBy(  ['id' => SORT_DESC]);

        return $dataProvider;
    }
}

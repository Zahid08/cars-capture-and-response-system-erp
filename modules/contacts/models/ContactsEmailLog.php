<?php

namespace unlock\modules\contacts\models;

use unlock\modules\core\helpers\CommonHelper;
use Yii;

/**
 * This is the model class for table "{{%contacts_email_log}}".
 *
 * @property int $id
 * @property int $contact_id
 * @property int $user_id
 * @property string $subject
 * @property string $mail_header
 * @property string $mail_body
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 */
class ContactsEmailLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%contacts_email_log}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['contact_id', 'user_id', 'subject', 'mail_header', 'mail_body', 'status', 'created_at', 'updated_at'], 'required'],
            [['contact_id', 'user_id', 'status'], 'integer'],
            [['created_at', 'updated_at'],['sent_date_time'], 'safe'],
            [['subject', 'mail_header'], 'string', 'max' => 500],
            [['mail_body'], 'string', 'max' => 1500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Log ID'),
            'contact_id' => Yii::t('app', 'Contact ID'),
            'user_id' => Yii::t('app', 'Recipient '),
            'subject' => Yii::t('app', 'Subject'),
            'mail_header' => Yii::t('app', 'Mail Header'),
            'mail_body' => Yii::t('app', 'Mail Body'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'sent_date_time' => Yii::t('app', 'Sent Date'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /*public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }*/

}

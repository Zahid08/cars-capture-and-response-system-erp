<?php

namespace unlock\modules\contacts\models;

use Yii;
use yii\helpers\ArrayHelper;
use unlock\modules\core\helpers\CommonHelper;

/**
 * This is the model class for table "{{%contacts_purchases}}".
 *
 * @property integer $id
 * @property integer $contact_id
 * @property integer $development_id
 * @property string $suite
 * @property string $purchase_date
 * @property string $occupancy
 * @property string $purchase_price
 * @property integer $deal_status
 * @property string $pm
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property integer $status
 */
class ContactsPurchases extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%contacts_purchases}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contact_id', 'development_id', 'deal_status', 'created_by', 'updated_by'], 'integer'],
            [['purchase_date', 'created_at', 'updated_at'], 'safe'],
            [['contact_id', 'development_id', 'deal_status','occupancy','purchase_price'], 'required'],
            [['pm'], 'string'],
            [['suite'], 'string', 'max' => 20],
            [['occupancy', 'purchase_price'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'contact_id' => Yii::t('app', 'Contact Name'),
            'development_id' => Yii::t('app', 'Development Name'),
            'suite' => Yii::t('app', 'Suite'),
            'purchase_date' => Yii::t('app', 'Purchase Date'),
            'occupancy' => Yii::t('app', 'Occupancy'),
            'purchase_price' => Yii::t('app', 'Purchase Price'),
            'deal_status' => Yii::t('app', 'Deal Status'),
            'pm' => Yii::t('app', 'PM'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /** Get All Active ContactsPurchases Drop Down List*/
    public static function contactsPurchasesDropDownList()
    {
        $model = self::find()
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }
}

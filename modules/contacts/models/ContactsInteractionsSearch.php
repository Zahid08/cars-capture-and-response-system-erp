<?php

namespace unlock\modules\contacts\models;

use Yii;
use yii\base\Model;
use unlock\modules\core\data\ActiveDataProvider;
use unlock\modules\contacts\models\ContactsInteractions;
use unlock\modules\core\helpers\CommonHelper;

/**
 * ContactsInteractionsSearch represents the model behind the search form about `unlock\modules\contacts\models\ContactsInteractions`.
 */
class ContactsInteractionsSearch extends ContactsInteractions
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lead_source', 'status', 'id', 'contact_id', 'interaction_type', 'created_by', 'updated_by'], 'integer'],
            [['land_line', 'profession', 'date_of_birth', 'SIN', 'interaction_note', 'interaction_datetime', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ContactsInteractions::find();

        // add conditions that should always apply here
        $pageSize = isset($params['per-page']) ? intval($params['per-page']) : CommonHelper::GRID_PER_PAGE;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created_at' => SORT_DESC]],
            'pagination' =>  ['pageSize' => $pageSize],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'contact_id' => $this->contact_id,
            'interaction_type' => $this->interaction_type,
            'interaction_datetime' => $this->interaction_datetime,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'interaction_note', $this->interaction_note]);

        return $dataProvider;
    }
}

<?php

namespace unlock\modules\contacts\models;

use Yii;
use yii\base\Model;
use unlock\modules\core\data\ActiveDataProvider;
use unlock\modules\contacts\models\ContactsRegistration;
use unlock\modules\core\helpers\CommonHelper;

/**
 * ContactsRegistrationSearch represents the model behind the search form about `unlock\modules\contacts\models\ContactsRegistration`.
 */
class ContactsRegistrationSearch extends ContactsRegistration
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'contact_id', 'reg_source', 'reg_type', 'development_id', 'created_by', 'updated_by'], 'integer'],
            [['reg_datetime', 'status', 'status_date', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ContactsRegistration::find();

        // add conditions that should always apply here
        $pageSize = isset($params['per-page']) ? intval($params['per-page']) : CommonHelper::GRID_PER_PAGE;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['reg_datetime' => SORT_DESC]],
            'pagination' =>  ['pageSize' => $pageSize],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'contact_id' => $this->contact_id,
            'development_id' => $this->development_id,
            'reg_datetime' => $this->reg_datetime,
            'status_date' => $this->status_date,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'reg_source', $this->reg_source])
            ->andFilterWhere(['like', 'status', $this->status]);
        return $dataProvider;
    }

    public function search_registration_report($params,$prev_date=null,$lastdate=null)
    {
        $query = ContactsRegistration::find();

        // add conditions that should always apply here
        $pageSize = isset($params['per-page']) ? intval($params['per-page']) : CommonHelper::GRID_PER_PAGE;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['reg_datetime' => SORT_DESC]],
            'pagination' =>  ['pageSize' => 20],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'contact_id' => $this->contact_id,
            'development_id' => $this->development_id,
            'reg_datetime' => $this->reg_datetime,
            'status_date' => $this->status_date,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'reg_source', $this->reg_source])
            ->andFilterWhere(['like', 'status', $this->status])
            ->where(['between', 'created_at', $prev_date, $lastdate ])
            ->orderBy(  ['id' => SORT_DESC])
            ->groupBy('contact_id,development_id');
        return $dataProvider;
    }

}

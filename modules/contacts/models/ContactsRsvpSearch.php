<?php

namespace unlock\modules\contacts\models;

use Yii;
use yii\base\Model;
use unlock\modules\core\data\ActiveDataProvider;
use unlock\modules\contacts\models\ContactsRsvp;
use unlock\modules\core\helpers\CommonHelper;

/**
 * ContactsRsvpSearch represents the model behind the search form about `unlock\modules\contacts\models\ContactsRsvp`.
 */
class ContactsRsvpSearch extends ContactsRsvp
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'contact_id', 'event_id', 'rsvp_source', 'rsvp_status', 'created_by', 'updated_by'], 'integer'],
            [['rsvp_date', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ContactsRsvp::find();

        // add conditions that should always apply here
        $pageSize = isset($params['per-page']) ? intval($params['per-page']) : CommonHelper::GRID_PER_PAGE;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' =>  ['pageSize' => $pageSize],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'contact_id' => $this->contact_id,
            'event_id' => $this->event_id,
            'rsvp_source' => $this->rsvp_source,
            'rsvp_date' => $this->rsvp_date,
            'rsvp_status' => $this->rsvp_status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);
        $query->orderBy(  ['id' => SORT_DESC]);

        return $dataProvider;
    }
}

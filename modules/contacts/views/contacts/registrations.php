<?php

use yii\helpers\Url;
use yii\helpers\Html;
/*use yii\jui\DatePicker;*/
use unlock\modules\core\grid\GridView;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\buttons\NewButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $searchModel unlock\modules\contacts\models\ContactsSearch */
/* @var $dataProvider unlock\modules\core\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Registration Contacts');
?>
<div class="main-container" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-grid-filter">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <div class="admin-grid-content">
        <div class="admin-grid-toolbar">
            <div class="admin-grid-toolbar-left">
                <?= Summary::widget(['dataProvider' => $dataProvider]) ?>
            </div>
        </div>
        <div class="admin-grid-view">
            <?php
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
                'pager' => ['class' => LinkPager::className()],
                'columns' => [
                    ['class' => 'unlock\modules\core\grid\SerialColumn'],

                    [
                        'attribute' => 'contact_id',
                        'format' => 'raw',
                        'value' => function($model) {
                            $contactInfo = CommonHelper::contactIdByContacts($model->contact_id);

                            if($contactInfo){
                                $name = '<a href="'.Url::to('view?id='.$contactInfo->id).'">'.$contactInfo->first_name.' '.$contactInfo->last_name.'</a>';
                                return $name;
                            }else{
                                return null;
                            }
                        },
                        /*'filter' => \kartik\select2\Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'contact_id',
                            'data' => CommonHelper::contactByArrayMap('1'),
                            // ... other params
                        ])*/
                        //'filter' => Html::activeDropDownList($searchModel, 'id', CommonHelper::contactIdByContacts(), ['class' => 'form-control', 'prompt' => CommonHelper::GRID_FILTER_PROMPT]),
                    ],
                    [
                        'attribute' => 'development_id',
                        'format' => 'raw',
                        'value' => function($model) {
                            //$contactInfo = CommonHelper::contactIdByContacts($model->contact_id);
                            if($model){
                                $name = $model->development_id;
                                return $name;
                            }else{
                                return null;
                            }
                        },
                        //'filter' => Html::activeDropDownList($searchModel, 'id', CommonHelper::contactIdByContacts(), ['class' => 'form-control', 'prompt' => CommonHelper::GRID_FILTER_PROMPT]),
                    ],
                    'reg_datetime:datetime',
                    'status'
                  /*  ['class' => 'yii\grid\ActionColumn',
                        'header' => 'Actions',
                        'headerOptions' => ['style' => 'color:#337ab7;width:8%'],
                        'template'=>'{details}',
                        'buttons'=>[
                            'details' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span> Details', $url, [
                                    'title' => Yii::t('app', 'Details'),
                                ]);
                            },
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            if ($action === 'details') {
                                $url =  Url::toRoute('/contacts/contacts/view/?id=').$model->id;
                                return $url;
                            }
                        }
                    ],*/
                ],
            ]); ?>
        </div>
    </div>

</div>

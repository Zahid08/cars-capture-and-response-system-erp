<?php

use yii\helpers\Url;
use yii\helpers\Html;
/*use yii\jui\DatePicker;*/
use unlock\modules\core\grid\GridView;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\buttons\NewButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $searchModel unlock\modules\contacts\models\ContactsSearch */
/* @var $dataProvider unlock\modules\core\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Contacts');
?>
<div class="main-container" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-grid-filter">
        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <div class="admin-grid-content">
        <div class="admin-grid-toolbar">
            <div class="admin-grid-toolbar-left">
                <?= NewButton::widget(['url' => Url::toRoute(['create'])]) ?>
            </div>
            <div class="admin-grid-toolbar-right">
                <?= Summary::widget(['dataProvider' => $dataProvider]) ?>
            </div>
        </div>

        <div class="admin-grid-view">
            <?php
                echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
                'pager' => ['class' => LinkPager::className()],
                'columns' => [
                   /* ['class' => 'unlock\modules\core\grid\SerialColumn'],*/
                    [
                        'attribute' => 'ID',
                        'format'    => 'html',
                        'headerOptions' => ['style' => 'color:#337ab7;'],
                        'value' => function($model) {
                            $url = '<a href="'.Url::toRoute('contacts/view?id='.$model->id).'">'.$model->id.'</a>';
                            return $url;
                        },
                    ],

                     [
                        'attribute' => 'Name',
                        'format'    => 'html',
                        'headerOptions' => ['style' => 'color:#337ab7;'],
                        'value' => function($model) {
                            $url = '<a href="'.Url::toRoute('contacts/view?id='.$model->id).'">'.$model->first_name.' '.$model->last_name.'</a>';
                            return $url;
                        },
                    ],

                    'email:email',
                    'phone',
                    [
                        'attribute' => 'created_at',
                        'value' => function($model){
                          return CommonHelper::changeDateFormat($model->created_at, 'd M Y');
                        }
                    ],
                    'lead_score',
                    [
                        'attribute' => 'lead_source',
                        'headerOptions' => ['style' => 'width:140px;'],
                        'value' => function($model){

                            if(!empty($model->lead_source)){
                                return CommonHelper::leadSourceByBrandId($model->lead_source);
                            }
                        }
                    ],

                    // 'address',
                    // 'city',
                    // 'province',
                    // 'postal',
                    // 'country',
                    // 'agent',
                    // 'social_insurance_number',
                    // 'drivers_license',
                    // 'created_at',
                    // 'created_by',
                    // 'updated_at',
                    // 'updated_by',
                    //'contact_type',
                 /*   [
                        'attribute' => 'contact_type',
                        'value' => function($model) {
                            return CommonHelper::contactTypeDropDownList($model->contact_type);
                        },
                        'filter' => Html::activeDropDownList($searchModel, 'contact_type', CommonHelper::contactTypeDropDownList(), ['prompt' => CommonHelper::GRID_FILTER_PROMPT]),
                    ],*/
                    /*['class' => 'yii\grid\ActionColumn',
                        'header' => 'Actions',
                        'headerOptions' => ['style' => 'color:#337ab7;width:8%'],
                        'template'=>'{details}',
                        'buttons'=>[
                            'details' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span> Details', $url, [
                                    'title' => Yii::t('app', 'View'),
                                ]);
                            },
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            if ($action === 'details') {
                                $url =  Url::toRoute('/contacts/contacts/view/?id=').$model->id;
                                return $url;
                            }
                        }
                    ],*/
                ],
            ]); ?>
        </div>
    </div>

</div>

<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtons;
use unlock\modules\core\buttons\BackButton;
use yii\helpers\ArrayHelper;
use unlock\modules\generalsettings\models\City;
/* @var $this yii\web\View */
/* @var $model unlock\modules\contacts\models\Contacts */
/* @var $form yii\bootstrap\ActiveForm */

$this->title= ($model->isNewRecord) ? Yii::t('app', 'Create Contacts') : Yii::t('app', 'Update Contacts');
?>

<div class="main-container main-container-form" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a(Yii::t('app', 'Home'), Yii::$app->homeUrl) ?></li>
                <li><?= Html::a(Yii::t('app', 'Contacts'), Url::to('index')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-form-container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fas fa-pencil-alt"></i> <?= Html::encode($this->title) ?></h3>
                </div>
                <div class="panel-heading-button">
                    <?= BackButton::widget() ?>
                </div>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
                    /*'options' => ['enctype' => 'multipart/form-data'],*/
                    'validateOnBlur' =>true,
                    'enableAjaxValidation'=>true,
                    'errorCssClass'=>'has-error',
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-sm-9\">\n{input}\n{hint}\n{error}\n</div>",
                        'options' => ['class' => 'form-group'],
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-3',
                            'offset' => '',
                            'wrapper' => '',
                            'hint' => '',
                        ],
                    ],
                ]); ?>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'land_line')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'brand_name')->dropDownList(CommonHelper::brandNameDropDownList(), ['prompt'=>'Select brand', 'options'=>['1'=>['Selected'=>true]] ]) ?>

                        <?= $form->field($model, 'lead_source')->dropDownList(CommonHelper::leadSourceDropDownList(), ['prompt'=>'Select lead source']) ?>

                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'suite')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'country')->dropDownList(CommonHelper::countryDropDownList(), ['prompt' => 'Select a Country']) ?>
                        <?= $form->field($model, 'province')->dropDownList(CommonHelper::provinceDropDownList(),
                            [
                                'prompt'=> 'Select a Province..',
                                'onChange' => '
                                    $.post("city?id='.'"+$(this).val(), function(data){
                                        $("select#contacts-city").html(data);
                                    });
                                ',
                                'class'=>'form-control select2'
                            ]
                        ); ?>
                        <?php
                        if(!empty($model->city)){
                            $city = ArrayHelper::map(City::find()->where(['province_id' => $model->province])->all(), 'id', 'city_name');
                            ?>
                            <?= $form->field($model, 'city')->dropDownList(
                                $city
                            //[''=> 'Select a City']
                            );?>
                            <?php
                        }else{
                            ?>
                            <?= $form->field($model, 'city')->dropDownList(['' => 'Select a City']) ?>
                            <?php
                        }
                        ?>


                        <?= $form->field($model, 'postal')->textInput(['maxlength' => true]) ?>



                    </div>
                </div>

                <?php /*
				<?= $form->field($model, 'agent')->textInput(['maxlength' => true]) ?>

				<?= $form->field($model, 'social_insurance_number')->textInput(['maxlength' => true]) ?>

				<?= $form->field($model, 'drivers_license')->textInput(['maxlength' => true]) ?>

                */ ?>


                <div class="admin-form-button">
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-5">
                            <?= FormButtons::widget() ?>
                        </div>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
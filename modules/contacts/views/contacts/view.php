<?php

use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\widgets\DetailView;
use unlock\modules\core\buttons\BackButton;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtons;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use unlock\modules\generalsettings\models\City;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model unlock\modules\contacts\models\Contacts */

$this->title = Yii::t('app', 'View Contacts');
$this->params['breadcrumbs'][] = ['label' => 'Contacts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-container main-container-view" role="main">
    <div class="panel panel-primary">

        <div class="panel-heading"><i class="fa fa-eye"></i> Prospect Details - <?php echo $model->id; ?></div>
        <div class="panel-body">
            <div class="developer-status-details">
                <div class="row">
                    <div class="col-md-4 col-12 contact-name">
                        <p style="float: left;"><span class="grade-value">Name: </span><?php echo $model->first_name.' '.$model->last_name; ?></p>
                    </div>
                    <div class="col-md-4 col-12 contact-number">
                        <p style="float: left;"><span class="grade-value">Lead Score: </span><?php echo $model->lead_score; ?></p>
                    </div>
                    <div class="col-md-4 col-12 contact-number" style="padding-left: 30px;">
                        <p style="float: left;"><span class="grade-value">Phone: </span><?php echo $model->phone; ?></p>
                    </div>
                    <?php
                    $assigned_user = CommonHelper::contactIdByRegistrationListDevelopmentId($model->id);
                    ?>
                    <div class="col-md-4 col-12 contact-email" style="margin-top: 21px;">
                            <p style="float: left;"><span class="grade-value">Email: </span><?php
                                if (strlen($model->email) >25){
                                    echo substr($model->email, 0, 25).'...';
                                }
                                else{
                                    echo  $model->email;
                                }
                            ?></p>
                    </div>
                    <div class="col-md-4 col-md-12 contact-name" style="margin-top: 21px;">
                        <p style="float: left;"><span class="grade-value">Assigned To: </span><?php /*echo ucfirst($assigned_user);*/?></p>
                    </div>
                    <?php $form = ActiveForm::begin(
                        [
                            'id' => 'update-form-contact-type',
                          /*  'action' => Url::to(['contacts/update', 'id' => $model->id]),*/
                        ]
                    ); ?>

                    <div class="col-md-4 col-12 contact-number" style="margin-top: 21px;">
                       <div class="col-md-5">
                           <p style="float: left;font-size: 19px;"><span class="grade-value">Contact Type: </span><?php /*echo ucfirst($assigned_user);*/?></p>
                       </div>
                        <div class="col-md-7">
                            <?php
                            //echo CommonHelper::contactStatusTypeById($model->status);
                            $urlInterac = yii\helpers\Url::to(['contacts/change-contact-type?id='.$model->id]);
                            echo Html::dropDownList('contact_type-dropdown', $model->contact_type, CommonHelper::contactRegistrationTypeDropDownList(), [
                                'class' => 'contact_type-dropdown form-control',
                                'onchange' => "
                                            var statusid = $(this).val();
                                            console.log(statusid);
                                            $.ajax('$urlInterac&status='+statusid+'', {
                                                type: 'POST'
                                            }).done(function(data) {
                                                //$.pjax.reload({container: '#pjax-container'});
                                            });                 
                                            ",
                                ]);
                            ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>

            <div class="divider"></div>

            <div id="app" class="tab_section">
                <tabs>

                    <?php
                      Modal::begin([
                          'header' => '<h4>Create a New Interaction</h4>',
                          'id'     => 'model-interaction',
                          'size'   => 'model-lg',
                      ]);

                      echo "<div id='add-contactInteraction-modelContent'></div>";

                      Modal::end();
                    ?>
                    <?php
                    Modal::begin([
                        'header' => '<h4>Update Interaction</h4>',
                        'id'     => 'model-update-interaction',
                        'size'   => 'model-lg',
                    ]);

                    echo "<div id='update-contactInteraction-modelContent'></div>";

                    Modal::end();
                    ?>

                    <?php
                      Modal::begin([
                          'header' => '<h4>Create a New Reservation</h4>',
                          'id'     => 'model-reservation',
                          'size'   => 'model-lg',
                      ]);

                      echo "<div id='add-reservation-modelContent'></div>";

                      Modal::end();
                    ?>
                    <?php
                      Modal::begin([
                          'header' => '<h4>Create a New Purchase</h4>',
                          'id'     => 'model-purchases',
                          'size'   => 'model-lg',
                      ]);

                      echo "<div id='add-purchases-modelContent'></div>";

                      Modal::end();
                    ?>

                    <?php
                    Modal::begin([
                        'header' => '<h4>Create a New RSVP</h4>',
                        'id'     => 'model-RSVP',
                        'size'   => 'model-lg',
                    ]);

                    echo "<div id='add-RSVP-modelContent'></div>";

                    Modal::end();
                    ?>

                    <?php
                    Modal::begin([
                        'header' => '<h4>Add To Prospects</h4>',
                        'id'     => 'model-tags',
                        'size'   => 'model-lg',
                    ]);

                    echo "<div id='add-tags-modelContent'></div>";

                    Modal::end();
                    ?>
                   
                  <?php
                    $developercontacts = CommonHelper::developerByDeveloperContact($model->id);
                  ?>

                    <tab name="Profile" :selected="true">
                        <?php $form = ActiveForm::begin(
                            [
                                'id' => 'update-form',
                                'action' => Url::to(['contacts/update', 'id' => $model->id]),
                            ]
                        ); ?>
                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

                                <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

                                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                                <?= $form->field($model, 'land_line')->textInput(['maxlength' => true]) ?>

                                <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

                                <?= $form->field($model, 'lead_score')->textInput(['maxlength' => true]) ?>
                                <?php

                                $lead_name=CommonHelper::leadSourceByBrandId($model->lead_source);
                                $user_permission=CommonHelper::getLoggedInUserRoleId();

                                if (!empty($model->lead_source)):?>

                                <?php if ($user_permission==1):?>
                                    <?= $form->field($model, 'lead_source')->dropDownList(CommonHelper::leadSourceDropDownList(), ['prompt'=>'Select lead source','calss'=>'meadia-source-c=']) ?>
                                <?php else:?>
                                    <?= $form->field($model, 'lead_source')->hiddenInput(['maxlength' => true])->label(false) ?>
                                    <?= $form->field($model, 'meadia_source')->textInput(['maxlength' => true,'readonly'=>true,'value'=>$lead_name]) ?>
                                <?php endif; ?>
                               <?php else:?>
                                    <?= $form->field($model, 'lead_source')->dropDownList(CommonHelper::leadSourceDropDownList(), ['prompt'=>'Select lead source','calss'=>'meadia-source-c=']) ?>
                                <?php endif; ?>

                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

                                <?= $form->field($model, 'suite')->textInput(['maxlength' => true]) ?>

                                <?= $form->field($model, 'country')->dropDownList(CommonHelper::countryDropDownList(), ['prompt' => 'Select a Country']) ?>

                                <?= $form->field($model, 'province')->dropDownList(CommonHelper::provinceDropDownList(),
                                    [
                                        'prompt'=> 'Select a Province..',
                                        'onChange' => '
                                    $.post("'.Url::toRoute('contacts/city').'?id='.'"+$(this).val(), function(data){
                                        $("select#contacts-city").html(data);
                                    });
                                ',
                                        'class'=>'form-control select2'
                                    ]
                                ); ?>

                                <?php
                                if(!empty($model->city)){

                                    $city = ArrayHelper::map(City::find()->where(['province_id' => $model->province])->all(), 'id', 'city_name');

                                    ?>
                                    <?= $form->field($model, 'city')->dropDownList(
                                        $city,
                                        ['prompt'=> 'Select a City']
                                    );?>
                                    <?php
                                }else{
                                    ?>
                                    <?= $form->field($model, 'city')->dropDownList(['prompt' => 'Select a City']) ?>
                                    <?php
                                }
                                ?>

                                <?= $form->field($model, 'postal')->textInput(['maxlength' => true]) ?>
                                <?= $form->field($model, 'date_of_birth')->widget(
                                    DatePicker::className(), [
                                    'name' => 'updated_at',
                                    'attribute' => 'updated_at',
                                    'value' => '12-31-2010',
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'format' => 'yyyy-mm-dd',
                                        'todayHighlight' => true
                                    ],
                                    'options'=>[   'placeholder'=>'yyyy-mm-dd',
                                     ]
                                ]);?>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </tab>
                    <tab name="Interactions">
                        <?php
                            $contactInteractions = CommonHelper::contactIdByInteraction($model->id);
                        ?>
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Note</th>
                                <th>Type</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>User</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach ($contactInteractions as $contactInteraction){
                                ?>
                                <tr>
                                    <td><?php echo $contactInteraction->id;?></td>
                                    <td><?php echo $contactInteraction->interaction_note;?></td>
                                    <td><?php echo CommonHelper::interactionTypeListById($contactInteraction->interaction_type);?></td>
                                    <td><?php echo CommonHelper::changeDateFormat($contactInteraction->interaction_datetime, 'd M Y'); ?></td>
                                    <td><?php echo CommonHelper::changeDateFormat($contactInteraction->interaction_datetime, 'g:i a'); ?></td>
                                    <td>
                                        <?php $user = CommonHelper::getUserById($contactInteraction->created_by)?>
                                        <?php
                                            if(!empty($user)){
                                                echo $user->first_name.' '.$user->last_name;
                                            }
                                        ?>
                                    </td>
                                    <td>
                                        <?= Html::button('<span class="glyphicon glyphicon-pencil"></span>', ['value' => \yii\helpers\Url::to(['contacts-interactions/quick-update?id='.$contactInteraction->id.'']), 'class' => 'update-contactInteraction-modelButton btn btn-success']) ?>
                                        <?php
                                            $urlInterac = yii\helpers\Url::to(['contacts-interactions/delete?id='.$contactInteraction->id.'&contactid='.$model->id]);
                                        ?>
                                        <?= Html::a(Yii::t('yii', '<span class="glyphicon glyphicon-trash"></span>'), '#', [
                                            'title' => Yii::t('yii', 'Delete'),
                                            'aria-label' => Yii::t('yii', 'Delete'),
                                            'onclick' => "
                                            if (confirm('Are you sure you want to delete this item?')) {
                                                $.ajax('$urlInterac', {
                                                    type: 'POST'
                                                }).done(function(data) {
                                                    $.pjax.reload({container: '#pjax-container'});
                                                });
                                            }
                                            return false;
                                        ",
                                        ]); ?>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </tab>
                    <tab name="Registration">
                        <?php
                            $registrationlist = CommonHelper::contactIdByRegistrationList($model->id);
                        ?>
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Development</th>
                                <th>Type</th>
                                <th>Media Source</th>
                                <th>Reg Date</th>
                                <th>Reg Status</th>
                                <th>Status Date</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                if(!empty($registrationlist)){
                                    foreach ($registrationlist as $value){
                                        ?>
                                <tr>
                                    <td> <a href="<?php echo Url::toRoute('/contacts/contacts-registration/update?id='.$value->id)?>"><?php echo $value->id;?></a></td>
                                    <td>
                                        <?php
                                            $development = CommonHelper::developmentListById($value->development_id);
                                            $devId = '';
                                            if(!empty($development->id)){
                                                $devId = $development->id;
                                            }
                                            $devname = '';
                                            if(!empty($development)){
                                                $devname = $development->development_name;
                                            }
                                        ?>
                                        <a href="<?php echo Url::toRoute('/developments/developments/view?id='.$devId)?>"><?php echo $devname;?></a>
                                    </td>
                                    <td>
                                        <?php
                                        $systemInfo=CommonHelper::contactIdBySystemInfoContactType($value->system_log_id);
                                       if (!empty($systemInfo->contact_type)){
                                          echo  $systemInfo->contact_type;
                                       }
                                       else{
                                           echo "Prospect";
                                       }
                                        ?>
                                    </td>

                                    <td><?php echo CommonHelper::leadSourceByBrandId($value->reg_source)?></td>
                                    <td><?php echo CommonHelper::changeDateFormat($value->reg_datetime, 'd M Y'); ?></td>
                                    <td><?php echo CommonHelper::contactStatusTypeById($value->status);?></td>
                                    <td><?php echo CommonHelper::changeDateFormat($value->status_date, 'd M Y H g:i a'); ?></td>
                                </tr>
                                <?php }} ?>
                            </tbody>
                        </table>
                    </tab>
                    <tab name="RSVP">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Development</th>
                                <th>Media Source</th>
                                <th>Submission Date</th>
                                <th>RSVP Status</th>
                                <th>RSVP Event</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $contactRsvp = CommonHelper::contactIdByRSVP($model->id);
                                    if(!empty($contactRsvp)){
                                    foreach ($contactRsvp as $rsvp){
                                        $event = CommonHelper::eventsListById($rsvp->event_id);
                                        $eventName =$rsvp_event= '';
                                        if(!empty($event->event_name)){
                                            $eventName = $event->event_name;
                                        }
                                        if (!empty($rsvp->rsvp_event)){
                                            $rsvp_event='True';
                                        }
                                        else{
                                            $rsvp_event='False';
                                        }
                                ?>
                                <tr>
                                    <td><a href="<?php echo Url::toRoute('/contacts/contacts-rsvp/update?id='.$rsvp->id)?>"><?php echo $rsvp->id;?></a></td>
                                    <td>
                                       <?php
                                            $development = CommonHelper::developmentListById($rsvp->development_id);
                                            $devId = '';
                                            if(!empty($development->id)){
                                                $devId = $development->id;
                                            }
                                            $devname = '';
                                            if(!empty($development)){
                                                $devname = $development->development_name;
                                            }
                                        ?>
                                        <a href="<?php echo Url::toRoute('/developments/developments/view?id='.$devId)?>"><?php echo $devname;?></a>
                                    </td>
                                    <td><?php echo CommonHelper::leadSourceByBrandId($rsvp->rsvp_source)?></td>
                                    <td><?php echo CommonHelper::changeDateFormat($rsvp->rsvp_date,'d M Y');?></td>
                                    <td>
                                        <?php
                                        $status='';
                                        $rsvp_source_status = CommonHelper::getRsvpStatus($rsvp->rsvp_status);
                                        $rsvp_source_dropdown = CommonHelper::rsvfDropdownList();
                                        if (!empty($rsvp_source_status->rsvp_status_name)){
                                            $status=$rsvp_source_status->rsvp_status_name;
                                        }
                                        $baseUrl=Yii::getAlias('@baseUrl');
                                        ?>
                                        <select name="status-dropdown" onchange="var statusid = $(this).val();console.log(statusid);
                                                $.ajax('<?php echo $baseUrl;?>/backend/web/contacts/contacts/change-rsvp-status?id=<?php echo $rsvp->id; ?>&amp;status='+statusid+'', {
                                                type: 'POST'
                                                }).done(function(data) {
                                                });" class="status-dropdown">
                                            <?php foreach ($rsvp_source_dropdown as $key=>$rsvp_source_dropdown_item){?>
                                                <option value="<?php echo $key ?>" <?php if ($rsvp->rsvp_status==$key){ echo "selected";}?>><?php echo $rsvp_source_dropdown_item?></option>
                                            <?php } ?>
                                        </select>
                                    </td>
                                    <td><?php echo $rsvp_event;?></td>

                                </tr>
                                <?php }} ?>
                            </tbody>
                        </table>
                    </tab>

                    <tab name="Reservations">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Development</th>
                                <th>Floor Plans</th>
                                <th style="padding: 0" align="left">Range</th>
                                <th style="padding: 0" align="left">Park</th>
                                <th style="padding: 0" align="left">Lock</th>
                                <th style="padding: 0" align="left">Res Date</th>
                                <th>Type</th>
                                <th width="100px">Res Status</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $contactReservations = CommonHelper::contactIdByReservation($model->id);
                                    if(!empty($contactReservations)){
                                    foreach ($contactReservations as $contactReservation){
                                        $developmentName = CommonHelper::developmentListById($contactReservation->development_id);

                                        $devId = '';
                                        if(!empty($development->id)){
                                            $devId = $development->id;
                                        }

                                        $devName = '';
                                        if(!empty($developmentName->development_name)){
                                            $devName = $developmentName->development_name;
                                        }
                                ?>
                                <tr>
                                    <td><a href="<?php echo Url::toRoute('/contacts/contacts-reservations/update?id='.$contactReservation->id)?>"><?php echo $contactReservation->id;?></a></td>
                                    <td><a href="<?php echo Url::toRoute('/developments/developments/view?id='.$devId)?>"><?php echo $devName;?></a></td>
                                    <td><?php echo $contactReservation->floor_plans_1st_choice.', '.$contactReservation->floor_plans_2nd_choice.', '.$contactReservation->floor_plans_3rd_choice;?></td>
                                    <td><?php echo $contactReservation->floor_range;?></td>
                                    <td><?php echo $contactReservation->require_parking;?></td>
                                    <td><?php echo $contactReservation->require_locker;?></td>
                                    <td><?php echo CommonHelper::changeDateFormat($contactReservation->created_at, 'd M Y');?></td>
                                    <td><?php echo 'Prospect';?></td>
                                    <td><?php echo CommonHelper::reservationStatusById($model->status);?></td>
                                </tr>
                                <?php }} ?>
                            </tbody>
                        </table>
                    </tab>

                    <tab name="Appointments">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>AppID</th>
                                <th>Development Name</th>
                                <th>On</th>
                                <th>At</th>
                                <th>Created At</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $contactsSystemInfo = CommonHelper::contactSystemInfoAppointment($model->id);
                            if(!empty($contactsSystemInfo)){
                                $count=count($contactsSystemInfo);
                                foreach ($contactsSystemInfo as $systemInfo){
                                    $developmentName = CommonHelper::developmentListById($systemInfo->development_id);
                                    $devId = '';
                                    if(!empty($developmentName->id)){
                                        $devId = $developmentName->id;
                                    }

                                    $devName = '';
                                    if(!empty($developmentName->development_name)){
                                        $devName = $developmentName->development_name;
                                    }
                                        ?>
                                        <tr>
                                            <td><?php echo $count;?></td>
                                            <td>
                                                <a href="<?php echo Url::toRoute('/developments/developments/view?id=' . $devId) ?>"><?php echo $devName ?></a>
                                            </td>
                                            <td><?php echo $systemInfo->event_on; ?></td>
                                            <td><?php echo $systemInfo->event_at; ?></td>
                                            <td><?php
                                                $date=$systemInfo->visit_date;
                                                $date_system =date('F',strtotime($date)).' '.date('d',strtotime($date)).'<sup>th</sup>'.' '.date('Y',strtotime($date)).' '.date('H g:i a',strtotime($date));
                                                echo $date_system;
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    $count--;}} ?>
                            </tbody>
                        </table>
                    </tab>

                    <tab name="Purchases">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Development</th>
                                <th>Suite</th>
                                <th>Purchase Date</th>
                                <th>Occupancy</th>
                                <th>Purchase Price</th>
                                <th>Deal Status</th>
                                <th>PM</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $contactPurchases = CommonHelper::contactIdByPurchases($model->id);
                                    if(!empty($contactPurchases)){
                                    foreach ($contactPurchases as $contactPurchase){
                                        $developmentName = CommonHelper::developmentListById($contactPurchase->development_id);
                                        $devId = '';
                                        if(!empty($development->id)){
                                            $devId = $development->id;
                                        }
                                        $devName = '';
                                        if(!empty($developmentName->development_name)){
                                            $devName = $developmentName->development_name;
                                        }
                                ?>
                                <tr>
                                    <td><a href="<?php echo Url::toRoute('/contacts/contacts-purchases/view?id='.$contactPurchase->id)?>"><?php echo $contactPurchase->id;?></a></td>
                                    <td><a href="<?php echo Url::toRoute('/developments/developments/view?id='.$devId)?>"><?php echo $devName;?></a></td>
                                    <td><?php echo $contactPurchase->suite; ?></td>
                                    <td><?php echo CommonHelper::changeDateFormat($contactPurchase->purchase_date, 'd M Y') ?></td>
                                    <td><?php echo $contactPurchase->occupancy; ?></td>
                                    <td>$<?php echo Yii::$app->formatter->format( $contactPurchase->purchase_price, 'money'); ?></td>
                                    <td><?php echo CommonHelper::contactDealStatusDropDownList($contactPurchase->deal_status) ?></td>
                                    <td><?php echo $contactPurchase->pm; ?></td>
                                </tr>
                                <?php }} ?>

                            </tbody>
                        </table>
                    </tab>
                    <tab name="TAGS">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Tag Name</th>
                                <th>Applied Date</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $contactsTag = CommonHelper::contactIdSearchByTagsNew($model->id);
                                    if(!empty($contactsTag)){
                                    foreach ($contactsTag as $tag){
                                        $contactsTags = CommonHelper::contactIdByTagsNew($tag->tag_id);
                                ?>
                                <tr>
                                    <td><?php echo ArrayHelper::getValue($tag,'tag_id'); ?></td>
                                    <td><?php echo ArrayHelper::getValue($contactsTags,'tag_name');?></td>
                                    <td><?php
                                        $date=ArrayHelper::getValue($tag,'date_applied');
                                        echo date('m',strtotime($date)).'/'.date('d',strtotime($date)).'/'.date('Y',strtotime($date)).' '.date('H g:i a',strtotime($date)) ;
                                        ?></td>
                                </tr>
                                <?php }} ?>
                            </tbody>
                        </table>
                    </tab>
                    <tab name="BRANDS">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Brand Name</th>
                                <th>Status Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $contactsBrand = CommonHelper::contactIdByBrand($model->id);

                            if(!empty($contactsBrand)){
                                foreach ($contactsBrand as $brand){
                                    $brandName = CommonHelper::brandsListByBrandId($brand->brand_id);
                                    if(!empty($brandName)){
                                    ?>
                                    <tr>
                                        <td><?php echo $brandName->id;?></td>
                                        <td><?php echo $brandName->brand_name;?></td>
                                        <td><?php echo CommonHelper::changeDateFormat($brand->created_at, 'd M Y H g:i a'); ?></td>
                                    </tr>
                                <?php }} } ?>
                            </tbody>
                        </table>
                    </tab>
                    <tab name="System">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Page Name</th>
                                <th>Page URL</th>
                                <th>IP Address</th>
                                <th>Browser</th>
                                <th>Operating</th>
                                <th style="width: 127px;">Visit Date</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $contactsSystemInfo = CommonHelper::contactIdBySystemInfo($model->id);
                                    if(!empty($contactsSystemInfo)){
                                    foreach ($contactsSystemInfo as $systemInfo){
                                ?>
                                <tr>
                                    <td><a href="<?php echo Url::toRoute('/contacts/contacts-systeminfo/view?id='.$systemInfo->id)?>"><?php echo $systemInfo->id;?></a></td>
                                    <td><?php echo $systemInfo->page_name;?></td>
                                    <td><?php echo $systemInfo->page_url;?></td>
                                    <td><?php echo $systemInfo->ip_address;?></td>
                                    <td><?php echo $systemInfo->browser;?></td>
                                    <td><?php echo $systemInfo->operating;?></td>
                                    <!--<td><?php /*echo CommonHelper::changeDateFormat($systemInfo->visit_date, 'F d Y H g:i a');*/?></td>-->
                                    <td><?php
                                        $date=$systemInfo->visit_date;
                                        $date_system =date('F',strtotime($date)).' '.date('d',strtotime($date)).'<sup>th</sup>'.' '.date('Y',strtotime($date)).' '.date('H g:i a',strtotime($date));
                                        echo $date_system;
                                        ?>
                                </tr>
                                <?php }} ?>
                            </tbody>
                        </table>
                    </tab>
                </tabs>
            </div>
            <div class="divider"></div>
            <div class="button_group">
                <div class="row">
                    <div class="pull-left form_action_btn_2">

                        <?= Html::button('Add Interaction', ['id' => 'add-contactInteraction-modelButton', 'value' => \yii\helpers\Url::to(['contacts-interactions/quick-create?contactid='.$model->id.'']), 'class' => 'btn btn-success']) ?>

                        <?= Html::button('Add Reservation', ['id' => 'add-reservation-modelButton', 'value' => \yii\helpers\Url::to(['contacts-reservations/quick-create?contactid='.$model->id.'']), 'class' => 'btn btn-success']) ?>

                        <?= Html::button('Add Purchase', ['id' => 'add-purchases-modelButton', 'value' => \yii\helpers\Url::to(['contacts-purchases/quick-create?contactid='.$model->id.'']), 'class' => 'btn btn-success']) ?>

                        <?= Html::button('Add RSVP', ['id' => 'add-RSVP-modelButton', 'value' => \yii\helpers\Url::to(['contacts-rsvp/quick-create?contactid='.$model->id.'']), 'class' => 'btn btn-success']) ?>

                        <?= Html::button('Add Tag', ['id' => 'add-tags-modelButton', 'value' => \yii\helpers\Url::to(['contacts-tags-new/quick-create?contactid='.$model->id.'']), 'class' => 'btn btn-success']) ?>
                        <input type="hidden" id="contact_id_get" value="<?php echo $model->id; ?>">
                    </div>
                    <div class="pull-right update_btn form_action_btn_2">
                        <?= FormButtons::widget(['item' => ['save', 'cancel']]) ?>
                        <?php
                        $contactUrl = yii\helpers\Url::to(['contacts/delete?id='.$model->id]);
                        ?>
                        <?= Html::a(Yii::t('yii', '<span class="glyphicon glyphicon-trash"></span> Delete'), '#', [
                            'class' => 'btn btn-danger',
                            'title' => Yii::t('yii', 'Delete'),
                            'aria-label' => Yii::t('yii', 'Delete'),
                            'onclick' => "
                                if (confirm('Are you sure you want to delete this item?')) {
                                    $.ajax('$contactUrl', {
                                        type: 'POST'
                                    }).done(function(data) {
                                        $.pjax.reload({container: '#pjax-container'});
                                    });
                                }
                                return false;
                            ",
                        ]); ?>
                    </div>
                </div>
                <div class="update_btn">
                    <div class="row">
                        <?php //FormButtons::widget(['item' => ['save', 'cancel']]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$js = <<< JS
    $(function(){
        // Contact Interactions Create and Update
        $('#add-contactInteraction-modelButton').click(function(){
            $('#model-interaction').modal('show')
                .find('#add-contactInteraction-modelContent')
                .load($(this).attr('value'));
        });
         $('.update-contactInteraction-modelButton').click(function(){
            $('#model-update-interaction').modal('show')
                .find('#update-contactInteraction-modelContent')
                .load($(this).attr('value'));
        });
        
        
        // Contact Reservation Create and Update
        $('#add-reservation-modelButton').click(function(){
            $('#model-reservation').modal('show')
                .find('#add-reservation-modelContent')
                .load($(this).attr('value'));
        });
        
        
         // Contact Purchases Create and Update
        $('#add-purchases-modelButton').click(function(){
            $('#model-purchases').modal('show')
                .find('#add-purchases-modelContent')
                .load($(this).attr('value'));
        });   
        
        
        // Contact RSVP Create and Update
        $('#add-RSVP-modelButton').click(function(){
            $('#model-RSVP').modal('show')
                .find('#add-RSVP-modelContent')
                .load($(this).attr('value'));
        });
        
         // Contact tags Create and Update
        $('#add-tags-modelButton').click(function(){
            $('#model-tags').modal('show')
                .find('#add-tags-modelContent')
                .load($(this).attr('value'));
        });
        
        
        
        $('.update_btn button').click(function(){
            var form = $("#update-form");
            var formData = form.serialize();
            //console.log(formData);
            $.ajax({
  
                url: form.attr("action"),
        
                type: form.attr("method"),
        
                data: formData,
            
                success: function (data) {
                    var obj = jQuery.parseJSON( JSON.stringify(data) );
                    console.log(obj);
                    $('button.btn.btn-primary').text("Save");
                },
                error: function () {
                    //$('button.btn.btn-primary').text("Save");
                    //alert("Something went wrong");
                    //$('button.btn.btn-primary').text("Save");
                },
                headers: {'Authorization': 'Basic bWFkaHNvbWUxMjM='},
                beforeSend: function (xhr) {
                    $('button.btn.btn-primary').text("Sending....");
                },
        
            });
        });
    });
    $(document).on("beforeSubmit", "#add-contact-form-1", function () {
        var form = $(this);
        var formData = form.serialize();
        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: formData,
            success: function (data) {
                var obj = jQuery.parseJSON( JSON.stringify(data) );
                console.log(obj);
                $('button.btn.btn-primary').text("Save");
            },
            error: function () {
                //$('button.btn.btn-primary').text("Save");
                //alert("Something went wrong");
                //$('button.btn.btn-primary').text("Save");
            },
            headers: {'Authorization': 'Basic bWFkaHNvbWUxMjM='},
            beforeSend: function (xhr) {
                $('button.btn.btn-primary').text("Sending....");
            },
        });
     }).on('submit', function(e){
        e.preventDefault();
     });
    
    
    $(document).on("beforeSubmit", "#add-contact-form-2", function () {
        var form = $(this);
        var formData = form.serialize();
        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: formData,
            success: function (data) {
                var obj = jQuery.parseJSON( JSON.stringify(data) );
                console.log(obj);
                $('button.btn.btn-primary').text("Save");
            },
            error: function () {
                //$('button.btn.btn-primary').text("Save");
                //alert("Something went wrong");
                //$('button.btn.btn-primary').text("Save");
            },
            headers: {'Authorization': 'Basic bWFkaHNvbWUxMjM='},
            beforeSend: function (xhr) {
                $('button.btn.btn-primary').text("Sending....");
            },
        });
     }).on('submit', function(e){
        e.preventDefault();
     });
    
    $(document).on("beforeSubmit", "#add-contact-form-3", function () {
        var form = $(this);
        var formData = form.serialize();
        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: formData,
            success: function (data) {
                var obj = jQuery.parseJSON( JSON.stringify(data) );
                console.log(obj);
                $('button.btn.btn-primary').text("Save");
            },
            error: function () {
                //$('button.btn.btn-primary').text("Save");
                //alert("Something went wrong");
                //$('button.btn.btn-primary').text("Save");
            },
            headers: {'Authorization': 'Basic bWFkaHNvbWUxMjM='},
            beforeSend: function (xhr) {
                $('button.btn.btn-primary').text("Sending....");
            },
        });
     }).on('submit', function(e){
        e.preventDefault();
     });
    
    $(document).on("beforeSubmit", "#add-contact-form-4", function () {
        var form = $(this);
        var formData = form.serialize();
        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: formData,
            success: function (data) {
                var obj = jQuery.parseJSON( JSON.stringify(data) );
                console.log(obj);
                $('button.btn.btn-primary').text("Save");
            },
            error: function () {
                //$('button.btn.btn-primary').text("Save");
                //alert("Something went wrong");
                //$('button.btn.btn-primary').text("Save");
            },
            headers: {'Authorization': 'Basic bWFkaHNvbWUxMjM='},
            beforeSend: function (xhr) {
                $('button.btn.btn-primary').text("Sending....");
            },
        });
     }).on('submit', function(e){
        e.preventDefault();
     });
    
    
     $(document).ready(function () {
        //called when key is pressed in textbox
      $("#contacts-date_of_birth").keypress(function (e) {
         //if the letter is not digit then display error and don't type anything
         if (e.which != 8 && e.which != 0 && String.fromCharCode(e.which) != '-' && (e.which < 48 || e.which > 57)) {
             return false;
        }
       });
    });
     
JS;
$this->registerJs($js);
?>

<?php

use yii\helpers\Url;
use yii\helpers\Html;
/*use yii\jui\DatePicker;*/
use unlock\modules\core\grid\GridView;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\buttons\NewButton;
use unlock\modules\core\helpers\CommonHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

$this->title = Yii::t('app', 'Import Tools');
?>
    <div class="main-container main-container-form" role="main">
        <div class="page-header">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb">
                    <li><?= Html::a(Yii::t('app', 'Home'), Yii::$app->homeUrl) ?></li>
                    <li><?= Html::a(Yii::t('app', 'Contacts'), Url::to('index')) ?></li>
                    <li class="active"><span><?= Html::encode($this->title) ?></span></li>
                </ul>
            </div>
        </div
        <div class="admin-form-container">
            <div class="panel panel-primary" style="position: relative;">
                <div class="panel-heading">
                    <div class="panel-heading-title">
                        <h3 class="panel-title"><i class="fas fa-pencil-alt"></i> <?= Html::encode($this->title) ?></h3>
                    </div>
                    <div class="panel-heading-button">
                        <?= \unlock\modules\core\buttons\BackButton::widget() ?>
                    </div>
                </div>

                <h3 class="contact-import-tools-title">Contacts Import Tool</h3>
                <div class="download-import-contact-csv">
                    <div class="col-md-12">
                        <a href="https://connectassetmgmt.com/import/contacts_import_example.csv" download class="btn btn-success btn-download-html btn-show-after-save"><i class="fa fa-download" style="margin-right: 7px;"></i>Download Example</a>
                    </div>
                </div>
                <?php $form = ActiveForm::begin(
                    [
                        'id' => 'upload_csv_contact',
                        'options' => ['enctype' => 'multipart/form-data'],
                    ]);
                ?>
                <div class="admin-grid-content" style="max-width: 600px; margin: 35px auto; background-color: #eee; padding: 30px;">
                    <div class="col-md-6">
                        <input type="file" name="file_select" style="margin-top:15px;" />
                        <label id="error_show" style="margin-top: 17px;color: red;"></label>
                    </div>
                    <div class="col-md-6" id="loading">
                        <p style="text-align: center;">
                            <input type="submit" name="upload" id="upload" value="Import Contacts" style="border-radius: 5%!important;margin-top:10px;" class="btn btn-primary" /><br>
                        </p>
                    </div>
                    <div style="clear:both ;padding-top: 13px;color: gray;">
                        <label>Note :* <span>1.File type should be CSV format.</span><br>
                            <span style="margin-left:55px;">2.System log save one time.If success then contact information save.</span>
                        </label>
                    </div>
                </div>
                <div class="admin-grid-content">
                        <div class="col-md-3 col-sm-offset-3">
                            <?=
                            $form->field($model, 'original_media_source')->widget(\kartik\select2\Select2::classname(), [
                                'data' => CommonHelper::leadSourceDropDownList(),
                                'language' => 'de',
                                'options' => ['placeholder' => 'Select media source ...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ])->label(false);
                            ?>
                        </div>
                        <div class="col-md-3">
                            <?=
                            $form->field($model, 'brand_name')->widget(Select2::classname(), [
                                'data' => CommonHelper::brandNameDropDownList(),
                                'language' => 'de',
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ])->label(false);
                            ?>
                        </div>
                </div>
                <br/><br/><br/>
                <?php ActiveForm::end(); ?>

                <div class="divider" style="margin: 15px -1px!important;"></div>
                <?php $form = ActiveForm::begin(
                    [
                        'id' => 'upload_csv_contact_tags',
                        'options' => ['enctype' => 'multipart/form-data'],
                    ]);
                ?>
                <h3 class="contact-import-tools-title">Tags Import Tool</h3>
                <div class="download-import-contact-csv">
                    <div class="col-md-12">
                        <a href="https://connectassetmgmt.com/import/tags_import_example.csv" download class="btn btn-info btn-download-html btn-show-after-save"><i class="fa fa-download" style="margin-right: 7px;"></i>Download Example</a>
                    </div>
                </div>
                <div class="admin-grid-content" style="max-width: 600px; margin: 35px auto; background-color: #eee; padding: 30px;">
                    <div class="col-md-6">
                        <input type="file" name="file_select_tags" style="margin-top:15px;" />
                        <label id="error_show_tags" style="margin-top: 17px;color: red;"></label>
                    </div>
                    <div class="col-md-6" id="loading_tags">
                        <p style="text-align: center;">
                            <input type="submit" name="upload" id="upload" value="Import Tags" style=" border-radius: 5%!important;margin-top:10px;" class="btn btn-primary" /><br>
                        </p>
                    </div>
                    <div style="clear:both ;padding-top: 13px;color: gray;">
                        <label>Note :* <span>1.File type should be CSV format.</span><br>
                        </label>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

<?php
$baseUrl = Yii::getAlias('@baseUrl');
$import_tool_jsscript = <<< JS
    $('#upload_csv_contact').off().on("submit", function(e){
           var file=$("input[name='file_select']").val();
           var media=$("#contacts-original_media_source").val();
           var brand=$("#contacts-brand_name").val();
           var siteUrl = '$baseUrl/backend/web/contacts/contacts/import-contacts-ajax?brand='+brand+'&media_source='+media+'';
           
           if (file=='' || media==''){
                $('#error_show').html("*Please Select Csv File");
                $('.field-contacts-original_media_source .help-block').html('Original Media Source cannot be blank.');
                $('.field-contacts-original_media_source .help-block').css('color','#a94442');
                $('.field-contacts-original_media_source .select2-selection').css('border-color','#a94442');
                if (file!=''){
                     $('#error_show').html('');
                }
                if (media !=''){
                     $('.field-contacts-original_media_source .help-block').html('');
                      $('.field-contacts-original_media_source .select2-selection').css('border-color','gray');
                }
                return false;
           }else {
               
            $('#error_show').html("");
            $('.field-contacts-original_media_source .help-block').html('');
            $('.field-contacts-original_media_source .select2-selection').css('border-color','gray');
            
            $('#loading').html('<div> <center><img style="height: 54px;" src="https://connectassetmgmt.connectcondos.com/backend/web/themes/connecterp/images/loader.gif"></center> </div>');
            e.preventDefault();
            $.ajax({
                url:siteUrl,
                method:"POST",
                data:new FormData(this),
                contentType:false,
                cache:false,
                processData:false,
                success: function(data){
                     if (data==0){
                      $('#error_show').html("*Please Select Csv File");
                       $('#loading').html('<p style="text-align: center;"><input type="submit" name="upload" id="upload" value="Import Contacts" class="btn btn-info" style="margin-top: 10px;"></p>');
                    }else {
                      $('#loading').html('<p style="text-align: center;"><input type="submit" name="upload" id="upload" value="Import Contacts" class="btn btn-info" style="margin-top: 10px;"></p>');
                      $('#loading').html(data);
                   }
                }
            })
        }
    });

$('#upload_csv_contact_tags').off().on("submit", function(e){
           var file=$("input[name='file_select_tags']").val();
           var siteUrl = '$baseUrl/backend/web/contacts/contacts/import-contacts-tags';
           if (file==''){
                $('#error_show_tags').html("*Please Select Csv File");
                return false;
           }else {
            $('#error_show_tags').html("");
            $('#loading_tags').html('<div> <center><img style="height: 54px;" src="https://connectassetmgmt.connectcondos.com/backend/web/themes/connecterp/images/loader.gif"></center> </div>');
            e.preventDefault();
            $.ajax({
                url:siteUrl,
                method:"POST",
                data:new FormData(this),
                contentType:false,
                cache:false,
                processData:false,
                success: function(data){
                    if (data==0){
                      $('#error_show_tags').html("*Please Select Csv File");
                       $('#loading_tags').html('<p style="text-align: center;"><input type="submit" name="upload" id="upload" value="Import Tags" class="btn btn-info" style="margin-top: 10px;"></p>');
                    }else{
                        $('#loading_tags').html('<input type="submit" name="upload" id="upload" value="Successfully Import Contacts" class="btn btn-info" style="margin-top: 15px;color: green;background-color: #ddd;">');
                        $('#loading_tags').html(data);
                    }
                }
            })
        }
    });

JS;

$this->registerJs($import_tool_jsscript);


?>
<?php

use yii\helpers\Html;
use yii\grid\GridView;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\buttons\NewButton;
use unlock\modules\core\helpers\CommonHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel unlock\modules\contacts\models\ContactsSysteminfoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'System Log API Documentation');
$request=[
        'access-token'=>'C6VlLzAnLJp-lM8O3v_7DBHH6I6mXql5', //
        'email'=>'sample@gmail.com',
        'first_name'=>'Sample',
        'last_name'=>'Test',
        'phone'=>'01545......',
        'event'=>'true',
        'url'=>'https://connectassetmgmt.connectcondos.com',
        'media'=>'Default Media',
        'development_id'=>'1545',
        'brand'=>'1',
        'realtor'=>'1',
        'development_name'=>'Default Development Name',
        'affiliate_tag'=>'Default Tag',
        'form_type'=>'Appointment',
        'on'=>'07/24/2019',
        'at'=>'12:58 PM-01:00 PM',
        'page_name'=>'Connect',
        'comments'=>'Sample Commnents',
    ]
?>
<style>
    .api-doc h3 {
        display: block;
        font-size: 22px;
        text-transform: uppercase;
        font-family: "abadi-regular";
    }
</style>
<div class="main-container" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li><?= Html::a(Yii::t('app', 'Settings'), Yii::$app->homeUrl.Url::to('/site/settings')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>
    <div class="admin-grid-filter">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <div class="admin-grid-content">
        <div class="admin-grid-toolbar" style="border-top: 3px solid #4677ba;">
            <div class="admin-grid-toolbar-left">
            <input type="hidden" id="copy-store">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 api-doc">
               <h3>Introduction</h3>
                <p>The Send System Log API allows you to easily and quickly send contact information through out highly reliable System log service.</p>
            </div>
            <div class="col-md-12 api-doc">
                <h3>API URL</h3>
                <div class="col-md-12 well" style="padding: 11px!important;" id="copy-content">https://connectassetmgmt.connectcondos.com/api/web/v1/portable-form/register-contact?access-token=C6VlLzAnLJp-lM8O3v_7DBHH6I6mXql5</div>
            </div>
            <div class="col-md-12 api-doc">
                <h3>Access Token</h3>
                <div class="col-md-12 well" style="padding: 11px!important;" id="copy-content">C6VlLzAnLJp-lM8O3v_7DBHH6I6mXql5</div>
            </div>
            <div class="col-md-6 api-doc">
                <h3>Request</h3>
                <div class="col-md-12 well" style="padding: 11px!important;" id="copy-content">
                    <code class="json">
                        [<br>
                        &nbsp;&nbsp;'email'=>'sample@gmail.com',<br>
                        &nbsp;&nbsp;'first_name'=>'Sample',<br>
                        &nbsp;&nbsp;'last_name'=>'Test',<br>
                        &nbsp;&nbsp;'phone'=>'01545......',<br>
                        &nbsp;&nbsp;'event'=>'true',<br>
                        &nbsp;&nbsp;'url'=>'https://connectassetmgmt.connectcondos.com',<br>
                        &nbsp;&nbsp;'media'=>'Default Media',<br>
                        &nbsp;&nbsp;'development_id'=>'1545',<br>
                        &nbsp;&nbsp;'brand'=>'1',<br>
                        &nbsp;&nbsp;'realtor'=>'1',<br>
                        &nbsp;&nbsp;'development_name'=>'Default Development Name',<br>
                        &nbsp;&nbsp;'affiliate_tag'=>'Default Tag',<br>
                        &nbsp;&nbsp;'form_type'=>'Appointment',<br>
                        &nbsp;&nbsp;'on'=>'07/24/2019',<br>
                        &nbsp;&nbsp;'at'=>'12:58 PM-01:00 PM',<br>
                        &nbsp;&nbsp;'page_name'=>'Connect',<br>
                        &nbsp;&nbsp;'comments'=>'Sample Commnents',<br>
                        ]
                    </code>
                </div>
            </div>
            <div class="col-md-6 api-doc">
                <h3>Specification</h3>
                <div class="col-md-12 well" style="padding: 11px!important;" id="copy-content">
                   <div class="col-md-12">
                       <div class="col-md-6">
                        <label>Mandatory:</label>
                        <p>Access Token</p>
                        <p>Email Address</p>
                        <p>Phone In Special Case</p>
                        <p>Development ID</p><br>
                        </div>
                        <div class="col-md-6">
                            <label></label>
                            <p>Form Type</p>
                            <p>First Name</p>
                            <p>Utm Source Or Media Source</p>
                        </div>
                   </div>
                    <div class="col-md-12" style="margin-top: 5px;">
                        <div class="col-md-6">

                            <label>Not Mandatory:</label>
                            <p>Last Name</p>
                            <p>Event</p>
                            <p>Page Url</p>
                            <p>Brand</p>
                            <p>Affiliate Tag</p>
                            <p>Realtor</p>
                        </div>
                        <div class="col-md-6">
                            <label></label>
                            <p>Comments</p>
                            <p>Page Name</p>
                            <p>Event On</p>
                            <p>Event At</p>
                            <p>Development Name</p>
                        </div>
                    </div>
                </div>
                </div>

            <div class="col-lg-12 api-doc" style="margin-bottom: 10px;">
                <h3>Field Value Specification</h3>
                <p>Some field value you can not never changed.When you put right information then API working goods as your expectation.Let's see which one filed to set specific....</p>
                <h4>Form Type : [Subscribe/Contact/Registration/RSVP/Appointment/Reservation/Survey]</h4>
                <h4>Brand ID : [Must be a positive integer value.]</h4>
                <h4>Development ID : [Must be a positive integer value.]</h4>
            </div>
            <div class="divider"></div>

            </div>
        </div>
    </div>
</div>
<?php
$script = <<< JS
$('#CopyBtn').on("click",function() {
      var value=$('#copy-content').text();
      $('#copy-store').val(value);
      $('#copy-store').select();
      document.execCommand("copy");
     /*  $('#CopyBtn').text("Copied!");*/
    });
JS;
$this->registerJs($script);
?>


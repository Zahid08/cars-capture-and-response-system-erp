<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model unlock\modules\contacts\models\ContactsSysteminfoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contacts-systeminfo-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'contact_id') ?>

    <?= $form->field($model, 'page_name') ?>

    <?= $form->field($model, 'page_url') ?>

    <?= $form->field($model, 'log_note') ?>

    <?php // echo $form->field($model, 'ip_address') ?>

    <?php // echo $form->field($model, 'browser') ?>

    <?php // echo $form->field($model, 'operating') ?>

    <?php // echo $form->field($model, 'visit_date') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

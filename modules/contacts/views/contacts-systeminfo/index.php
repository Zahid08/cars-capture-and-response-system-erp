<?php

use yii\helpers\Html;
use yii\grid\GridView;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\buttons\NewButton;
use unlock\modules\core\helpers\CommonHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel unlock\modules\contacts\models\ContactsSysteminfoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Contacts Systeminfos');
?>
<div class="main-container" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>
    <div class="admin-grid-filter">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <div class="admin-grid-content">
        <div class="admin-grid-toolbar" style="border-top: 3px solid #4677ba;">
            <div class="admin-grid-toolbar-left">
            </div>
            <div class="admin-grid-toolbar-right">
                <?= Summary::widget(['dataProvider' => $dataProvider]) ?>
            </div>
        </div>
        <div class="admin-grid-view">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
                'pager' => ['class' => LinkPager::className()],
                'columns' => [
                    [
                        'attribute' => 'id',
                        'format' => 'html',
                        'headerOptions' => ['style' => 'color:#337ab7;'],
                        'value' => function($model){
                            $id = Html::a($model->id, ['/contacts/contacts-systeminfo/view', 'id' => $model->id]);
                            return $id;
                        }
                    ],
                    [
                        'attribute' => 'form_type',
                        'format' => 'html',
                        'headerOptions' => ['style' => 'color:#337ab7;'],
                        'value' => function($model){
                            return $model->form_type;
                        }
                    ],
                    [
                        'attribute' => 'media_source',
                        'format' => 'html',
                        'headerOptions' => ['style' => 'color:#337ab7;'],
                        'value' => function($model){
                                return $model->media_source;
                        }
                    ],
                    [
                        'attribute' => 'email',
                        'format' => 'html',
                        'headerOptions' => ['style' => 'color:#337ab7;'],
                        'value' => function($model){
                            if (!empty($model->contact_id)) {
                                    $contact = CommonHelper::contactIdByContacts($model->contact_id);
                                    if (!empty($contact)) {
                                        $email = Html::a($contact->email, ['/contacts/contacts/view', 'id' => $contact->id]);
                                        return $email;
                                    }
                            }
                            else{
                                return $model->email;
                            }
                        }
                    ],
                    [
                        'attribute' => 'first_name',
                        'format' => 'html',
                        'headerOptions' => ['style' => 'color:#337ab7;'],
                        'value' => function($model){
                            if (!empty($model->contact_id)) {
                                    $contact = CommonHelper::contactIdByContacts($model->contact_id);
                                    if (!empty($contact)) {
                                        $name = Html::a($contact->first_name." ".$contact->last_name, ['/contacts/contacts/view', 'id' => $contact->id]);
                                        return $name;
                                    }
                            }
                            else{
                                return $model->first_name.' '.$model->last_name;
                            }
                        }
                    ],
                    [
                        'attribute' => 'visit_date',
                        'format' => 'html',
                        'headerOptions' => ['style' => 'color:#337ab7;width:250px;'],
                        'value' => function($model){
                            $date_system =date('F',strtotime($model->visit_date)).' '.date('d',strtotime($model->visit_date)).'<sup>th</sup>'.' '.date('Y',strtotime($model->visit_date)).' '.date('H g:i a',strtotime($model->visit_date));
                            return $date_system;
                        }
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>


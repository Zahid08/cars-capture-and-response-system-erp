<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use unlock\modules\core\helpers\CommonHelper;
/* @var $this yii\web\View */
/* @var $model unlock\modules\contacts\models\ContactsSysteminfo */

$this->title = Yii::t('app', 'View Contacts Systeminfo');
$this->params['breadcrumbs'][] = ['label' => 'Contacts Systeminfos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
//first name
$first_name=$last_name=$phone=$email=$sytem_log_contact_id='';
if (!empty($model->contact_id)) {
    $agent = CommonHelper::agentListByContactsId($model->contact_id);
    if (!empty($agent)) {
        $first_name = Html::a($agent->first_name, ['/contacts/agent/update', 'id' => $agent->id]);
        $last_name = Html::a($agent->last_name, ['/contacts/agent/update', 'id' => $agent->id]);
        $phone=$agent->mobile;
        $sytem_log_contact_id = Html::a($model->contact_id, ['/contacts/agent/update', 'id' => $agent->id]);
    }
    else{
        $contact = CommonHelper::contactIdByContacts($model->contact_id);
        if (!empty($contact)) {
            if (!empty($model->contact_id)) {
                $first_name = Html::a($contact->first_name, ['/contacts/contacts/view', 'id' => $contact->id]);
                $last_name = Html::a($contact->last_name, ['/contacts/contacts/view', 'id' => $contact->id]);
                $phone=$contact->phone;
                $sytem_log_contact_id = Html::a($model->contact_id, ['/contacts/contacts/view', 'id' => $contact->id]);
                //$media_source=CommonHelper::leadSourceByBrandId($contact->lead_source);
            }
        }
    }
}

//email
if (!empty($model->contact_id)) {
    $contact = CommonHelper::contactIdByContacts($model->contact_id);
    if (!empty($contact)) {
        $email = Html::a($contact->email, ['/contacts/contacts/view', 'id' => $contact->id]);
    }
}
else{
    $email=$model->email;
}

//brand name
$brandName = CommonHelper::brandsListByBrandId($model->brand_id);
if (!empty($brandName)) {
    $brand_1=$brandName->brand_name;
}
else{
    $contactsBrand = CommonHelper::contactIdByBrand($model->contact_id);
    if (!empty($contactsBrand)) {
        foreach ($contactsBrand as $brand) {
            $brand_c = CommonHelper::brandsListByBrandId($brand->brand_id);
        }
        $brand_1=$brand_c->brand_name;
    }
    else{
        $brand_1='';
    }
}


//event
if (!empty($model->event)){
    $event_sec='True';
}
else{
    $event_sec='False';
}
//Contact Tyoe
if (!empty($model->realtor)){
    $contact_type='Agent';
}
else{
    $contact_type='Prospect';
}
//realtor
if (!empty($model->realtor)){
    $realtor='True';
}
else{
    $realtor='False';
}
$operating=$model->operating;
$device=$model->device;
$comment=$model->comment;
$page_name=$model->page_name;
$page_url=Html::a($model->page_url, [$model->page_url]);
$ip_address=$model->ip_address;
$browser=$model->browser;
$browser=$model->browser;
$form_type=$model->form_type;
$affiliate_tag=$model->affiliate_tag;
$visit_date=$model->visit_date;
$event_on=$model->event_on;
$event_at=$model->event_at;
$media_source=$model->media_source;
$development_name=$model->development_name;
$development_id=$model->development_id;
$log_data = Html::a('Download', ['/contacts/contacts-systeminfo/download-log-data', 'id' => $model->id]);
$api_array_code=json_decode($model->log_data);
?>

<div class="main-container main-container-view" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">

        </div>
    </div>

    <div class="admin-view-container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fa fa-eye"></i> <?= Yii::t('app', 'View') ?> - <?= Html::encode($model->id) ?></h3>
                </div>
                <div class="panel-heading-button">
                    <?= \unlock\modules\core\buttons\BackButton::widget() ?>
                </div>
            </div>

            <div class="panel-body">
                <table  class="table table-bordered system-info-details-view">
                    <tr>
                        <th><strong>Contact ID</strong></th>
                        <td><div id="contact-id"><?php echo $sytem_log_contact_id?></div></td>
                        <td><div id="contact-id"><?php if (!empty($api_array_code->email->vcode)){echo $api_array_code->email->vcode;} ?></div></td>
                    </tr>
                    <tr>
                        <th><strong>First Name</strong></th>
                        <td><div id=""><?php echo !empty($first_name)?$first_name:$model->first_name;?></div></td>
                        <td><div id=""><?php if (!empty($api_array_code->first_name->vcode)){ echo $api_array_code->first_name->vcode; } ?></div></td>
                    </tr>
                    <tr>
                        <th><strong>Last Name</strong></th>
                        <td><div id=""><?php echo !empty($last_name)?$last_name:$model->last_name; ?></div></td>
                        <td><div id=""><?php if (!empty($api_array_code->last_name->vcode)){echo $api_array_code->last_name->vcode;}  ?></div></td>
                    </tr>
                    <tr>
                        <th><strong>Phone</strong></th>
                        <td><div id=""><?php echo !empty($phone)?$phone:$model->phone;?></div></td>
                        <td><div id=""><?php if (!empty($api_array_code->phone->vcode)){echo $api_array_code->phone->vcode; } ?></div></td>
                    </tr>
                    <tr>
                        <th><strong>Email</strong></th>
                        <td><div id=""><?php echo $email;?></div></td>
                        <td><div id=""><?php  if (!empty($api_array_code->email->vcode)){echo $api_array_code->email->vcode; }?></div></td>
                    </tr>
                    <tr>
                        <th><strong>Brand ID</strong></th>
                        <td><div id=""><?php echo $brand_1;?></div></td>
                        <td><div id=""><?php if (!empty($api_array_code->brand->vcode)){ echo $api_array_code->brand->vcode; } ?></div></td>
                    </tr>
                    <tr>
                        <th><strong>Development ID</strong></th>
                        <td><div id=""><?php echo $development_id;?></div></td>
                        <td><div id=""><?php if (!empty($api_array_code->development_id->vcode)){ echo $api_array_code->development_id->vcode;} ?></div></td>
                    </tr>
                    <tr>
                        <th><strong>Development Name</strong></th>
                        <td><div id=""><?php echo $development_name;?></div></td>
                        <td><div id=""><?php if (!empty($api_array_code->development_name->vcode)){ echo $api_array_code->development_name->vcode; }?></div></td>
                    </tr>
                    <tr>
                        <th><strong>Media Source</strong></th>
                        <td><div id=""><?php echo $media_source;?></div></td>
                        <td><div id=""><?php if (!empty($api_array_code->media->vcode)){  echo $api_array_code->media->vcode;} ?></div></td>
                    </tr>
                    <tr>
                        <th><strong>Event On</strong></th>
                        <td><div id=""><?php echo $event_on;?></div></td>
                        <td><div id=""><?php if (!empty($api_array_code->event_on->vcode)){  echo $api_array_code->event_on->vcode;} ?></div></td>
                    </tr>
                    <tr>
                        <th><strong>Event At</strong></th>
                        <td><div id=""><?php echo $event_at;?></div></td>
                        <td><div id=""><?php if (!empty($api_array_code->event_at->vcode)){   echo $api_array_code->event_at->vcode; }?></div></td>
                    </tr>
                    <tr>
                        <th><strong>Event</strong></th>
                        <td><div id=""><?php echo $event_sec;?></div></td>
                        <td><div id=""><?php if (!empty($api_array_code->event->vcode)){ echo $api_array_code->event->vcode;} ?></div></td>
                    </tr>
                    <tr>
                        <th><strong>Affiliate Tag</strong></th>
                        <td><div id=""><?php echo $affiliate_tag;?></div></td>
                        <td><div id=""><?php if (!empty($api_array_code->affiliate_tag->vcode)){echo $api_array_code->affiliate_tag->vcode; }?></div></td>
                    </tr>
                    <tr>
                        <th><strong>Form Type</strong></th>
                        <td><div id=""><?php echo $form_type;?></div></td>
                        <td><div id=""><?php if (!empty($api_array_code->form_type->vcode)){echo $api_array_code->form_type->vcode; }?></div></td>
                    </tr>
                    <tr>
                        <th><strong>Contact Type</strong></th>
                        <td><div id=""><?php echo $contact_type;?></div></td>
                        <td><div id=""><?php if (!empty($api_array_code->realtor->vcode)){echo $api_array_code->realtor->vcode; } ?></div></td>
                    </tr>
                    <tr>
                        <th><strong>Log Date</strong></th>
                        <td><div id=""><?php echo $visit_date;?></div></td>
                        <td><div id=""><?php ?></div></td>
                    </tr>
                    <tr>
                        <th><strong>Page Name</strong></th>
                        <td><div id=""><?php echo  $page_name;?></div></td>
                        <td><div id=""><?php ?></div></td>
                    </tr>
                    <tr>
                        <th><strong>Page Url</strong></th>
                        <td><div id=""><?php echo $page_url;?></div></td>
                        <td><div id=""><?php ?></div></td>
                    </tr>
                    <tr>
                        <th><strong>Ip Address</strong></th>
                        <td><div id=""><?php echo $ip_address;?></div></td>
                        <td><div id=""><?php ?></div></td>
                    </tr>
                    <tr>
                        <th><strong>Browser</strong></th>
                        <td><div id=""><?php echo $browser;?></div></td>
                        <td><div id=""><?php ?></div></td>
                    </tr>
                    <tr>
                        <th><strong>Realtor</strong></th>
                        <td><div id=""><?php echo $realtor;?></div></td>
                        <td><div id=""><?php ?></div></td>
                    </tr>
                    <tr>
                        <th><strong>Operating</strong></th>
                        <td><div id=""><?php  echo  $operating;?></div></td>
                        <td><div id=""><?php ?></div></td>
                    </tr>
                    <tr>
                        <th><strong>Device</strong></th>
                        <td><div id=""><?php echo $device; ?></div></td>
                        <td><div id=""><?php ?></div></td>
                    </tr>
                    <tr>
                        <th><strong>Comments</strong></th>
                        <td><div id=""><?php echo $comment;?></div></td>
                        <td><div id=""><?php ?></div></td>
                    </tr>
                    <tr>
                        <th><strong>Log Data</strong></th>
                        <td><div id=""><?php echo $log_data;?></div></td>
                        <td><div id=""><?php ?></div></td>
                    </tr>
                </table>
            </div>

        </div>
    </div>
</div>


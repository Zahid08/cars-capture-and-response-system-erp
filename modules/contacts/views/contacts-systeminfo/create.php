<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model unlock\modules\contacts\models\ContactsSysteminfo */

$this->title = Yii::t('app', 'Create Contacts Systeminfo');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Contacts Systeminfos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contacts-systeminfo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

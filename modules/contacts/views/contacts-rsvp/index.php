<?php

use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\grid\GridView;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\buttons\NewButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $searchModel unlock\modules\contacts\models\ContactsRsvpSearch */
/* @var $dataProvider unlock\modules\core\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Contacts Rsvp');
?>
<div class="main-container" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-grid-filter">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <div class="admin-grid-content">
        <div class="admin-grid-toolbar">
            <div class="admin-grid-toolbar-left">
                <?= NewButton::widget(['url' => Url::toRoute(['create'])]) ?>
            </div>
            <div class="admin-grid-toolbar-right">
                <?= Summary::widget(['dataProvider' => $dataProvider]) ?>
            </div>
        </div>

        <div class="admin-grid-view">
                            <?php 
                echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
                'pager' => ['class' => LinkPager::className()],
                'columns' => [
               /* ['class' => 'unlock\modules\core\grid\SerialColumn'],*/
                    [
                        'attribute' => 'id',
                        'format'    => 'html',
                        'headerOptions' => ['style' => 'color:#337ab7;'],
                        'value' => function($model) {
                            $url = '<a href="'.Url::toRoute('contacts-rsvp/view?id='.$model->id).'">'.$model->id.'</a>';
                            return $url;
                        },
                    ],

                    [
                        'attribute' => 'contact_id',
                        'format' => 'html',
                        'headerOptions' => ['style' => 'color:#337ab7;'],
                        'value' => function($model){
                            $contact = CommonHelper::contactIdByContacts($model->contact_id);
                            if (!empty($contact->first_name) || !empty($contact->last_name)):
                            $name = Html::a($contact->first_name." ".$contact->last_name, ['/contacts/contacts/view', 'id' => $contact->id]);
                            return $name;
                            else:
                                return '';
                            endif;
                        }
                    ],
                    [
                        'attribute' => 'development_id',
                        'format' => 'html',
                        'value' => function($model) {
                            $development = CommonHelper::developmentListById($model->development_id);
                            $devId = '';
                            if(!empty($development->id)){
                                $devId = $development->id;
                            }
                            $devname = '';
                            if(!empty($development)){
                                $devname = Html::a($development->development_name, ['/developments/developments/view', 'id' => $devId]);
                            }
                            return $devname;
                        },
                    ],
                    [
                        'attribute' => 'rsvp_source',
                        'value' => function($model) {
                            if(!empty($model->rsvp_source)){
                                return CommonHelper::leadSourceByBrandId($model->rsvp_source);
                            }
                        },
                    ],
                    [
                        'attribute' => 'rsvp_date',
                        'value' => function($model) {
                            return CommonHelper::changeDateFormat($model->rsvp_date, 'd M Y');
                        },
                    ],

                    [
                        'attribute'  => 'rsvp_status',
                        'format'    => 'raw',
                        'headerOptions' => ['style' => ''],
                        'value' => function($model){
                            $urlInterac = yii\helpers\Url::to(['contacts-rsvp/change-rsvp-status?id='.$model->id]);
                            return Html::dropDownList('rsvp_status-dropdown', $model->rsvp_status, CommonHelper::rsvfDropdownList(), [
                                'class' => 'rsvp_status-dropdown',
                                'onchange' => "     
                                            var statusid = $(this).val();
                                            console.log(statusid);
                                            $.ajax('$urlInterac&status='+statusid+'', {
                                                type: 'POST'
                                            }).done(function(data) {
                                                //$.pjax.reload({container: '#pjax-container'});
                                            });
                                          
                                        ",
                            ]);
                            ?>
                            <?php
                        }
                    ],
                ],
                ]);   ?>
        </div>
    </div>

</div>

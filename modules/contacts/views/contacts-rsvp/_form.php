<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtons;
use unlock\modules\core\buttons\BackButton;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use unlock\modules\contacts\models\Contacts;
use yii\jui\JuiAsset;
JuiAsset::register($this);

/* @var $this yii\web\View */
/* @var $model unlock\modules\contacts\models\ContactsRsvp */
/* @var $form yii\bootstrap\ActiveForm */

$this->title= ($model->isNewRecord) ? Yii::t('app', 'Create a New RSVP') : Yii::t('app', 'Update Rsvp');
?>

<div class="main-container main-container-form" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a(Yii::t('app', 'Home'), Yii::$app->homeUrl) ?></li>
                <li><?= Html::a(Yii::t('app', 'Contacts Rsvp'), Url::to('index')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-form-container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fas fa-pencil-alt"></i> <?= Html::encode($this->title) ?></h3>
                </div>
                <div class="panel-heading-button">
                    <?= BackButton::widget() ?>
                </div>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
                    /*'options' => ['enctype' => 'multipart/form-data'],*/
                    'validateOnBlur' =>true,
                    'enableAjaxValidation'=>true,
                    'errorCssClass'=>'has-error',
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
                        'options' => ['class' => 'form-group'],
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-2',
                            'offset' => '',
                            'wrapper' => '',
                            'hint' => '',
                        ],
                    ],
                ]); ?>
                <?php
                if ($model->isNewRecord){
                   echo $form->field($model, 'name', ['options' => ['class' => 'form-group form-group-search-auto-complete'],'inputTemplate' => '{input}<i class="fa fa-search"></i>'])->textInput(['class' => 'form-control field-contact-id field-search-auto-complete', 'placeholder' => 'Search','data-input-type' => 'select','data-hidden-field' => '#auto-complete-contact-id','data-ajax-url' => 'ajax/ajax/contact-name-list', 'maxlength' => true]);
                }
                else{
                     $contact_id=$model->contact_id;
                     $contact_name=CommonHelper::contactIdByName($contact_id);
                     echo $form->field($model, 'name', ['options' => ['class' => 'form-group form-group-search-auto-complete'],'inputTemplate' => '{input}<i class="fa fa-search"></i>'])->textInput([ 'value'=>$contact_name, 'class' => 'form-control field-contact-id field-search-auto-complete', 'placeholder' => 'Search','data-input-type' => 'select','data-hidden-field' => '#auto-complete-contact-id','data-ajax-url' => 'ajax/ajax/contact-name-list', 'maxlength' => true]);
                }
                ?>
                <input type="hidden" name="ContactsRsvp[contact_id]" id="auto-complete-contact-id">

                <?=
                $form->field($model, 'development_id')->widget(Select2::classname(), [
                    'data' => CommonHelper::developmentDropdownList(),
                    'language' => 'de',
                    'options' => ['placeholder' => 'Select a development ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

				<?= $form->field($model, 'rsvp_source')->dropDownList(CommonHelper::leadSourceDropDownList(), ['prompt' => 'Select a media source']) ?>

                <?= $form->field($model, 'rsvp_date')->widget(
                    DatePicker::className(), [
                    'name' => 'rsvp_date',
                    'value' => '12-31-2010',
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]);?>

				<?= $form->field($model, 'rsvp_status')->dropDownList(CommonHelper::rsvfDropdownList(), ['prompt' => 'Select a RSVP Status']) ?>

                <div class="admin-form-button">
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <?= FormButtons::widget() ?>
                            <?php if (!$model->isNewRecord){ ?>
                            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                            ]) ?>
                            <?php }?>
                        </div>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>


<?php

use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\widgets\DetailView;
use unlock\modules\core\buttons\BackButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $model unlock\modules\contacts\models\ContactsRsvp */

$this->title = Yii::t('app', 'View Contacts Rsvp');
?>
<div class="main-container main-container-view" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li><?= Html::a(Yii::t('app', 'Contacts Rsvp'), Url::to('index')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-view-container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fa fa-eye"></i> <?= Yii::t('app', 'View') ?> - <?= Html::encode($model->id) ?></h3>
                </div>
                <div class="panel-heading-button">
                    <?= BackButton::widget() ?>
                </div>
            </div>

            <div class="panel-body">
                <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                	'id',
                    [
                        'attribute' => 'contact_id',
                        'format' => 'html',
                        'value' => function($model) {
                            $contact = CommonHelper::contactIdByContacts($model->contact_id);
                            if (!empty($contact->first_name) || !empty($contact->last_name)):
                                $name = Html::a($contact->first_name." ".$contact->last_name, ['/contacts/contacts/view', 'id' => $contact->id]);
                                return $name;
                            else:
                                return '';
                            endif;
                        },
                    ],
                    [
                        'attribute' => 'development_id',
                        'format' => 'html',
                        'value' => function($model) {
                            $development = CommonHelper::developmentListById($model->development_id);
                            $devId = '';
                            if(!empty($development->id)){
                                $devId = $development->id;
                            }
                            $devname = '';
                            if(!empty($development)){
                                $devname = Html::a($development->development_name, ['/developments/developments/view', 'id' => $devId]);
                            }
                            return $devname;
                        },
                    ],
                    [
                        'attribute' => 'rsvp_source',
                        'value' => function($model) {
                            if(!empty($model->rsvp_source)){
                                return CommonHelper::leadSourceByBrandId($model->rsvp_source);
                            }
                        },
                    ],
                    [
                        'attribute' => 'rsvp_date',
                        'value' => function($model) {
                            return CommonHelper::changeDateFormat($model->rsvp_date, 'd M Y');
                        },
                    ],
                    [
                        'attribute' => 'rsvp_status',
                        'format' => 'html',
                        'headerOptions' => ['style' => 'color:#337ab7;'],
                        'value' => function($model){
                            $rsvp_source = CommonHelper::getRsvpStatus($model->rsvp_status);
                            if (!empty($rsvp_source->rsvp_status_name)){
                                return $rsvp_source->rsvp_status_name;
                            }
                        }
                    ],
					'created_at:datetime',
					'created_by:user',
					'updated_at:datetime',
					'updated_by:user',
                ],
                ]) ?>
                <div class="row">
                    <div class="col-md-4 text-left">
                        <a class="btn btn-primary" href="<?php echo Yii::$app->request->referrer; ?>">Back</a>
                    </div>
                    <div class="col-md-4 text-center">
                        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    </div>
                    <div class="col-md-4 text-right">
                        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtons;
use unlock\modules\core\buttons\BackButton;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use unlock\modules\contacts\models\Contacts;

/* @var $this yii\web\View */
/* @var $model unlock\modules\contacts\models\ContactsRsvp */
/* @var $form yii\bootstrap\ActiveForm */

$this->title= ($model->isNewRecord) ? Yii::t('app', 'Create a New RSVP') : Yii::t('app', 'Update Rsvp');
?>


<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
    /*'options' => ['enctype' => 'multipart/form-data'],*/
    'id' => 'add-contact-form-4',
    'validateOnBlur' =>true,
    'enableAjaxValidation'=>false,
    'errorCssClass'=>'has-error',
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
        'options' => ['class' => 'form-group'],
        'horizontalCssClasses' => [
            'label' => 'col-sm-2',
            'offset' => '',
            'wrapper' => '',
            'hint' => '',
        ],
    ],
]); ?>


<?=
$form->field($model, 'event_id')->widget(Select2::classname(), [
    'data' => CommonHelper::eventsTypeDropDownList(),
    'language' => 'de',
    'options' => ['placeholder' => 'Select a Event ...'],
    'pluginOptions' => [
        'allowClear' => true
    ],
]);
?>

<?= $form->field($model, 'rsvp_source')->dropDownList(CommonHelper::contactRSVPSourceDropDownList(), ['prompt' => 'Select a RSVP Source']) ?>

<div style="margin-bottom: 10px;" class="form-group field-contactsrsvp-rsvp_date required">
    <label class="control-label col-sm-2" for="contactsrsvp-rsvp_date">Rsvp Date</label>
    <div class="col-sm-10">
        <?php
        $todaydatetime = date("Y-m-d");
        echo DatePicker::widget([
            'name' => 'ContactsRsvp[rsvp_date]',
            'value' => $todaydatetime,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd',
            ]
        ]);
        ?>
    </div>
    <p class="help-block help-block-error "></p>
</div>

<?= $form->field($model, 'rsvp_status')->dropDownList(CommonHelper::contactRSVPStatusDropDownList(), ['prompt' => 'Select a RSVP Status']) ?>

<?= $form->field($model, 'contact_id')->hiddenInput(['value' => $_REQUEST['contactid']])->label('') ?>
<div class="admin-form-button">
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <?= FormButtons::widget() ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
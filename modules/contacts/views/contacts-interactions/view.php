<?php

use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\widgets\DetailView;
use unlock\modules\core\buttons\BackButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $model unlock\modules\contacts\models\ContactsInteractions */

$this->title = Yii::t('app', 'View Contacts Interactions');
?>
<div class="main-container main-container-view" role="main">
    <div class="page-header">
        <div class="page-header-title">
            <h1 class="page-title"><?= Yii::t('app', 'Contacts Interactions') ?></h1>
        </div>
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li><?= Html::a(Yii::t('app', 'Contacts Interactions'), Url::to('index')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-view-container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fa fa-eye"></i> <?= Yii::t('app', 'View') ?> - <?= Html::encode($model->id) ?></h3>
                </div>
                <div class="panel-heading-button">
                    <?= BackButton::widget() ?>
                </div>
            </div>

            <div class="panel-body">
                <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                	'id',
					'contact_id',
					'interaction_note:ntext',
					'interaction_type',
					'interaction_datetime',
					'created_at',
					'created_by',
					'updated_at',
					'updated_by',
                ],
                ]) ?>
            </div>

        </div>
    </div>
</div>

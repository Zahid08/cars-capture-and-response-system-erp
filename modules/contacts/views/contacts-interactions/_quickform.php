<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\select2\Select2;
use kartik\datetime\DateTimePicker;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtons;
use unlock\modules\core\buttons\BackButton;
use unlock\modules\contacts\models\Contacts;

/* @var $this yii\web\View */
/* @var $model unlock\modules\contacts\models\ContactsInteractions */
/* @var $form yii\bootstrap\ActiveForm */

$this->title= ($model->isNewRecord) ? Yii::t('app', 'Create Contacts Interactions') : Yii::t('app', 'Update Contacts Interactions');
?>

<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
    /*'options' => ['enctype' => 'multipart/form-data'],*/
    'id' => 'add-contact-form-1',
    'validateOnBlur' =>true,
    'enableAjaxValidation'=>false,
    'errorCssClass'=>'has-error',
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
        'options' => ['class' => 'form-group'],
        'horizontalCssClasses' => [
            'label' => 'col-sm-2',
            'offset' => '',
            'wrapper' => '',
            'hint' => '',
        ],
    ],
]); ?>


<?= $form->field($model, 'interaction_note')->textarea(['rows' => 6]) ?>

<?= $form->field($model, 'interaction_type')->dropDownList(CommonHelper::interactionTypeDropDownList()) ?>

<div style="margin-bottom: 10px;" class="form-group field-contactsinteractions-interaction_datetime required">
    <label class="control-label col-sm-2" for="contactsinteractions-interaction_datetime">Date</label>
    <div class="col-sm-10">
        <?php
        $todaydatetime = date("Y-m-d H:i:s");
        echo DateTimePicker::widget([
            'name' => 'ContactsInteractions[interaction_datetime]',
            'value' => $todaydatetime,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd hh:ii:ss',
            ]
        ]);
        ?>
    </div>
    <p class="help-block help-block-error "></p>
</div>

<?= $form->field($model, 'contact_id')->hiddenInput(['value' => $_REQUEST['contactid']])->label('') ?>

<div class="admin-form-button">
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <?= FormButtons::widget() ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>

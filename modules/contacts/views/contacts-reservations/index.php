<?php

use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\grid\GridView;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\buttons\NewButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $searchModel unlock\modules\contacts\models\ContactsReservationsSearch */
/* @var $dataProvider unlock\modules\core\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Contacts Reservations');
?>
<div class="main-container" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-grid-filter">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <div class="admin-grid-content">
        <div class="admin-grid-toolbar">
            <div class="admin-grid-toolbar-left">
                <?= NewButton::widget(['url' => Url::toRoute(['create'])]) ?>
            </div>
            <div class="admin-grid-toolbar-right">
                <?= Summary::widget(['dataProvider' => $dataProvider]) ?>
            </div>
        </div>

        <div class="admin-grid-view">
                            <?php 
                echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
                'pager' => ['class' => LinkPager::className()],
                'columns' => [
                    [
                        'attribute' => 'id',
                        'format'    => 'html',
                        'value' => function($model){
                            $name = Html::a($model->id, ['update', 'id' => $model->id]);
                            return $name;
                        }
                    ],
                    [
                        'attribute' => 'contact_id',
                        'format' => 'html',
                        'headerOptions' => ['style' => 'color:#337ab7;'],
                        'value' => function($model){
                            $contact = CommonHelper::contactIdByContacts($model->contact_id);
                            $name='';
                            if (!empty($contact->first_name) || !empty($contact->last_name)) {
                                $name = Html::a($contact->first_name . " " . $contact->last_name, ['/contacts/contacts/view', 'id' => $contact->id]);
                            }
                            return $name;
                        }
                    ],
                    [
                        'attribute'  => 'development_id',
                        'format' => 'html',
                        'value' => function($model){
                            $developmentName = CommonHelper::developmentListById($model->development_id);

                            $name = Html::a($developmentName['development_name'], ['/developments/developments/view', 'id' => $developmentName['id']]);
                            return $name;
                        }
                    ],
                   /* 'floor_plans_1st_choice',
                    'floor_plans_2nd_choice',*/
                    // 'floor_plans_3rd_choice',
                    'floor_range',
                    'require_parking',
                    'require_locker',
                    // 'nature_of_purchase',
                    [
                        'attribute' => 'Res Date',
                        'headerOptions' => ['style' => 'color:#337ab7;'],
                        'value' => function($model){
                            return CommonHelper::changeDateFormat($model->created_at, 'd M Y');
                        }
                    ],
                    // 'created_by',
                    // 'updated_at',
                    // 'updated_by',
                    // 'status',
                    /*[
                        'attribute' => 'status',
                        'headerOptions' => ['style' => 'width:160px;'],
                        'value' => function($model){
                            return CommonHelper::reservationStatusById($model->status);
                        }
                    ],*/

                    [
                        'attribute'  => 'status',
                        'format'    => 'raw',
                        'headerOptions' => ['style' => 'width:180px;'],
                        'value' => function($model){
                            $urlInterac = yii\helpers\Url::to(['contacts-reservations/change-reservation-status?id='.$model->id]);
                            return Html::dropDownList('status-dropdown', $model->status, CommonHelper::ReservationContactStatusTypeDropDownList(), [
                                'class' => 'status-dropdown',
                                'onchange' => "
                                            var statusid = $(this).val();
                                            console.log(statusid);
                                            $.ajax('$urlInterac&status='+statusid+'', {
                                                type: 'POST'
                                            }).done(function(data) {
                                                //$.pjax.reload({container: '#pjax-container'});
                                            });
                                          
                                        ",
                            ]);

                            ?>
                            <?php
                        }
                    ],

                    //['class' => 'unlock\modules\core\grid\ActionColumn'],
                ],
                ]);   ?>
                    </div>
    </div>

</div>

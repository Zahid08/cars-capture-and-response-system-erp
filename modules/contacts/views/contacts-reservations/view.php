<?php

use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\widgets\DetailView;
use unlock\modules\core\buttons\BackButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $model unlock\modules\contacts\models\ContactsReservations */

$this->title = Yii::t('app', 'View Contacts Reservations');
?>
<div class="main-container main-container-view" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li><?= Html::a(Yii::t('app', 'Contacts Reservations'), Url::to('index')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-view-container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fa fa-eye"></i> <?= Yii::t('app', 'View') ?> - <?= Html::encode($model->id) ?></h3>
                </div>
                <div class="panel-heading-button">
                    <?= BackButton::widget() ?>
                </div>
            </div>

            <div class="panel-body">
                <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                	'id',
                    [
                        'attribute' => 'contact_id',
                        'value' => function($model) {
                            $contactName = CommonHelper::contactIdByContacts($model->contact_id);
                            return $contactName->first_name.' '.$contactName->last_name;
                        },
                    ],
                    [
                        'attribute' => 'development_id',
                        'value' => function($model) {
                            $developmentName = CommonHelper::developmentListById($model->development_id);
                            return $developmentName['development_name'];
                        },
                    ],
					'floor_plans_1st_choice',
					'floor_plans_2nd_choice',
					'floor_plans_3rd_choice',
					'floor_range',
					'require_parking',
					'require_locker',
					'nature_of_purchase',
					'created_at:datetime',
					'created_by:user',
					'updated_at:datetime',
					'updated_by:user',
                    [
                        'attribute' => 'status',
                        'value' => function($model) {
                            return CommonHelper::reservationStatusById($model->status);
                        },
                    ],
                ],
                ]) ?>

                <div class="row">
                    <div class="col-md-4 text-left">
                        <a class="btn btn-primary" href="<?php echo Yii::$app->request->referrer; ?>">Back</a>
                    </div>
                    <div class="col-md-4 text-center">
                        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    </div>
                    <div class="col-md-4 text-right">
                        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtons;
use unlock\modules\core\buttons\BackButton;
use unlock\modules\contacts\models\Contacts;
use kartik\datetime\DateTimePicker;
use kartik\date\DatePicker;
use unlock\modules\generalsettings\models\City;
use yii\jui\JuiAsset;
JuiAsset::register($this);

/* @var $this yii\web\View */
/* @var $model unlock\modules\contacts\models\ContactsReservations */
/* @var $form yii\bootstrap\ActiveForm */

$this->title= ($model->isNewRecord) ? Yii::t('app', 'Create a new Reservation') : Yii::t('app', 'Update Reservation');
?>
<div class="main-container main-container-form contacts-reservations-form" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a(Yii::t('app', 'Home'), Yii::$app->homeUrl) ?></li>
                <li><?= Html::a(Yii::t('app', 'Reservation'), Url::to('index')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-form-container">
        <div class="panel panel-primary contacts-reservations-panel">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fas fa-pencil-alt"></i> <?= Html::encode($this->title) ?></h3>
                </div>
                <div class="panel-heading-button">
                    <?= BackButton::widget() ?>
                </div>
            </div>
            <div class="panel-body">

                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
                    /*'options' => ['enctype' => 'multipart/form-data'],*/
                    'validateOnBlur' =>true,
                    'enableAjaxValidation'=>false,
                    'errorCssClass'=>'has-error',
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-sm-8\">\n{input}\n{hint}\n{error}\n</div>",
                        'options' => ['class' => 'form-group'],
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-4',
                            'offset' => '',
                            'wrapper' => '',
                            'hint' => '',
                        ],
                    ],
                ]); ?>

                <div class="row">
                    <div class="col-md-6">
                        <h3 class="contact-reservation-title">Reservation Information</h3>
                        <?php
                        if ($model->isNewRecord){
                            echo $form->field($model, 'name', ['options' => ['class' => 'form-group form-group-search-auto-complete reservation'],'inputTemplate' => '{input}<i class="fa fa-search"></i>'])->textInput(['class' => 'form-control field-contact-id field-search-auto-complete', 'placeholder' => 'Search','data-input-type' => 'select','data-hidden-field' => '#auto-complete-contact-id','data-ajax-url' => 'ajax/ajax/contact-name-list', 'maxlength' => true]);
                            echo '<input type="hidden" name="ContactsReservations[contact_id]" id="auto-complete-contact-id">';
                        }
                        else{
                            $contact_id=$model->contact_id;
                            $contact_name=CommonHelper::contactIdByName($contact_id);
                            echo $form->field($model, 'name', ['options' => ['class' => 'form-group form-group-search-auto-complete'],'inputTemplate' => '{input}<i class="fa fa-search"></i>'])->textInput([ 'value'=>$contact_name, 'class' => 'form-control field-contact-id field-search-auto-complete', 'placeholder' => 'Search','data-input-type' => 'select','data-hidden-field' => '#auto-complete-contact-id','data-ajax-url' => 'ajax/ajax/contact-name-list', 'maxlength' => true]);
                            echo "<input type='hidden' name='ContactsReservations[contact_id]' id='auto-complete-contact-id' value='$contact_id'>";
                        }
                        ?>

                        <?=
                        $form->field($model, 'development_id')->widget(Select2::classname(), [
                            'data' => CommonHelper::developmentDropdownList(),
                            'language' => 'de',
                            'options' => ['placeholder' => 'Select a development ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>

                        <?= $form->field($model, 'floor_plans_1st_choice')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'floor_plans_2nd_choice')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'floor_plans_3rd_choice')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'floor_range')->radioList([ 'High' => 'High', 'Medium' => 'Medium', 'Low' => 'Low', ], ['prompt' => '']) ?>

                        <?= $form->field($model, 'require_parking')->radioList([ 'Yes' => 'Yes', 'No' => 'No', ], ['prompt' => '']) ?>

                        <?= $form->field($model, 'require_locker')->radioList([ 'Yes' => 'Yes', 'No' => 'No', ], ['prompt' => '']) ?>

                        <?php
                            if(!empty($model->nature_of_purchase)){
                                $model->nature_of_purchase = explode(', ',$model->nature_of_purchase);
                            }
                        ?>
                        <?= $form->field($model, 'nature_of_purchase')->checkboxList([ 'End User' => 'End User', 'Investor' => 'Investor', ]) ?>

                        <?= $form->field($model, 'status')->dropDownList(CommonHelper::reservationStatusDropDownList()) ?>
                        <?php if(!$model->isNewRecord && !empty($model->updated_at)){?>
                            <?php
                            $userName=CommonHelper::getLoggedInUserFullName();
                            $userID= CommonHelper::getLoggedInUserId();
                            $array= [$userID => $userName];
                            ?>
                            <?= $form->field($model, 'updated_at')->widget(
                                DatePicker::className(), [
                                'name' => 'updated_at',
                                'value' => '12-31-2010',
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'yyyy-mm-dd'
                                ]
                            ]);?>
                            <?php echo $form->field($model, 'updated_by')->dropDownList($array); ?>

                        <?php } ?>
                    </div>
                    <div class="col-md-6">
                        <h3 class="contact-reservation-title">Contact Information</h3>
                        <?= $form->field($contactsModel, 'first_name')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($contactsModel, 'last_name')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($contactsModel, 'phone')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($contactsModel, 'email')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($contactsModel, 'address')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($contactsModel, 'province')->dropDownList(CommonHelper::provinceDropDownList(),
                            [
                                'prompt'=> 'Select a Province..',
                                'onChange' => '
                                    $.post("city?id='.'"+$(this).val(), function(data){
                                        $("select#contacts-city").html(data);
                                    });
                                ',
                                'class'=>'form-control select2'
                            ]
                        ); ?>
                        <?php
                        if(!empty($contactsModel->city)){
                            $city = ArrayHelper::map(City::find()->where(['province_id' => $contactsModel->province])->all(), 'id', 'city_name');
                            ?>
                            <?= $form->field($contactsModel, 'city')->dropDownList(
                                $city
                            //[''=> 'Select a City']
                            );?>
                            <?php
                        }else{
                            ?>
                            <?= $form->field($contactsModel, 'city')->dropDownList(['' => 'Select a City']) ?>
                            <?php
                        }
                        ?>
                        <?= $form->field($contactsModel, 'postal')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($contactsModel, 'profession')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($contactsModel, 'drivers_license')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($contactsModel, 'date_of_birth')->widget(
                            DatePicker::className(), [
                            'name' => 'date_of_birth',
                            'value' => '12-31-2010',
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd'
                            ]
                        ]);?>
                        <?= $form->field($contactsModel, 'SIN')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>


                <div class="admin-form-button">
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <?= FormButtons::widget() ?>
                            <?php
                                $contactUrl = yii\helpers\Url::to(['contacts-reservations/delete?id='.$model->id]);
                            ?>
                            <?= Html::a(Yii::t('yii', '<span class="glyphicon glyphicon-trash"></span> Delete'), '#', [
                                'class' => 'btn btn-danger',
                                'title' => Yii::t('yii', 'Delete'),
                                'aria-label' => Yii::t('yii', 'Delete'),
                                'onclick' => "
                                if (confirm('Are you sure you want to delete this item?')) {
                                    $.ajax('$contactUrl', {
                                        type: 'POST'
                                    }).done(function(data) {
                                        $.pjax.reload({container: '#pjax-container'});
                                    });
                                }
                                return false;
                            ",
                            ]); ?>
                        </div>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<?php
$js = <<< JS
    $(function(){
         $('#contactsreservations-contact_id').change(function(){
            var contactid = $(this).val();
            $.get('contact-info', { contactid : contactid }, function(data) {
                var data = $.parseJSON(data);
                console.log(data);
                $('#contacts-first_name').val(data.first_name);
                $('#contacts-last_name').val(data.last_name);
                $('#contacts-phone').val(data.phone);
                $('#contacts-email').val(data.email);
                $('#contacts-address').val(data.address);
                $('#contacts-city').val(data.city);
                $('#contacts-province').val(data.province);
                $('#contacts-postal').val(data.postal);
                $('#contacts-profession').val(data.profession);
                $('#contacts-drivers_license').val(data.drivers_license);
                $('#contacts-date_of_birth').val(data.date_of_birth);
                $('#contacts-sin').val(data.SIN);
                
            });
         });
    });
             
JS;
$this->registerJs($js);
?>

<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtons;
use unlock\modules\core\buttons\BackButton;
use unlock\modules\contacts\models\Contacts;
use kartik\datetime\DateTimePicker;
use kartik\date\DatePicker;

$this->title= ($model->isNewRecord) ? Yii::t('app', 'Create a Reservation') : Yii::t('app', 'Update a Reservation');
?>


<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
    /*'options' => ['enctype' => 'multipart/form-data'],*/
    'id' => 'add-contact-form-3',
    'validateOnBlur' =>true,
    'enableAjaxValidation'=>false,
    'errorCssClass'=>'has-error',
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-sm-8\">\n{input}\n{hint}\n{error}\n</div>",
        'options' => ['class' => 'form-group'],
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => '',
            'wrapper' => '',
            'hint' => '',
        ],
    ],
]); ?>

<?=
$form->field($model, 'development_id')->widget(Select2::classname(), [
    'data' => CommonHelper::developmentDropdownList(),
    'language' => 'de',
    'options' => ['placeholder' => 'Select a development ...'],
    'pluginOptions' => [
        'allowClear' => true
    ],
]);
?>

<?= $form->field($model, 'floor_plans_1st_choice')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'floor_plans_2nd_choice')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'floor_plans_3rd_choice')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'floor_range')->radioList([ 'High' => 'High', 'Medium' => 'Medium', 'Low' => 'Low', ], ['prompt' => '']) ?>

<?= $form->field($model, 'require_parking')->radioList([ 'Yes' => 'Yes', 'No' => 'No', ], ['prompt' => '']) ?>

<?= $form->field($model, 'require_locker')->radioList([ 'Yes' => 'Yes', 'No' => 'No', ], ['prompt' => '']) ?>
<?php
    if(!empty($model->nature_of_purchase)){
        $model->nature_of_purchase = explode(', ',$model->nature_of_purchase);
    }
?>
<?= $form->field($model, 'nature_of_purchase')->checkboxList([ 'End User' => 'End User', 'Investor' => 'Investor', ]) ?>

<?= $form->field($model, 'status')->dropDownList(CommonHelper::reservationStatusDropDownList()) ?>

<?= $form->field($model, 'contact_id')->hiddenInput(['value' => $_REQUEST['contactid']])->label('') ?>

<div class="admin-form-button">
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-4">
            <?= FormButtons::widget() ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>

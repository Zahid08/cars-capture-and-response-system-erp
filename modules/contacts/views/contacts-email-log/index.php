<?php
use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\grid\GridView;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\buttons\NewButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $searchModel unlock\modules\contacts\models\ContactsEmailLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Contacts Email Logs');
?>

<div class="main-container" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-grid-content">
        <div class="admin-grid-toolbar" style="border-top: 3px solid #4677ba;">
            <div class="admin-grid-toolbar-left">
            </div>
            <div class="admin-grid-toolbar-right">
                <?= Summary::widget(['dataProvider' => $dataProvider]) ?>
            </div>
        </div>

        <div class="admin-grid-view">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
                'pager' => ['class' => LinkPager::className()],
                'columns' => [
                    [
                        'attribute' => 'id',
                        'format'    => 'html',
                        'value' => function($model){
                            $name = Html::a($model->id, ['view', 'id' => $model->id]);
                            return $name;
                        }
                    ],
                    [
                        'attribute' => 'subject',
                        'format'    => 'html',
                        'value' => function($model){
                            $name = Html::a($model->subject, ['view', 'id' => $model->id]);
                            return $name;
                        }
                    ],
                    [
                        'attribute' => 'user_id',
                        'format'    => 'html',
                        'value' => function($model){
                            $username=CommonHelper::getLoggedInUserNameByUserId($model->user_id);
                            return $username;
                        }
                    ],
                    [
                        'attribute' => 'sent_date_time',
                        'format'    => 'html',
                        'value' => function($model){
                            $sent_date =CommonHelper::changeDateFormat($model->sent_date_time, 'd M Y'); ;
                            return $sent_date;
                        }
                    ],
                    [
                        'attribute' => 'sent_date_time',
                        'label' => 'Sent Time',
                        'format'    => 'html',
                        'value' => function($model){
                            $sent_time = CommonHelper::changeDateFormat($model->sent_date_time, 'g:i a');
                            return $sent_time;
                        }
                    ],
                    [
                        'attribute' => 'status',
                        'format'    => 'html',
                        'value' => function($model){
                             $staus='';
                             if ($model->status==1){
                                 $staus='Pass';
                             }
                             else{
                                 $staus='Fail';
                             }
                            return $staus;
                        }
                    ],
                ],
            ]); ?>
        </div>
    </div>

</div>


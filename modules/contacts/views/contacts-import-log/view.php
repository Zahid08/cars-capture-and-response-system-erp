<?php

use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\widgets\DetailView;
use unlock\modules\core\buttons\BackButton;
use unlock\modules\core\helpers\CommonHelper;
/* @var $this yii\web\View */
/* @var $model unlock\modules\contacts\models\ContactsImportLog */
$this->title = Yii::t('app', 'View Contacts Maropost Import Logs');
?>

<div class="main-container main-container-view" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li><?= Html::a(Yii::t('app', 'Contacts Maropost Import Logs'), Url::to('index')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>
    <div class="admin-view-container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fa fa-eye"></i> <?= Yii::t('app', 'View') ?></h3>
                </div>
                <div class="panel-heading-button">
                    <?= BackButton::widget() ?>
                </div>
            </div>
            <div class="panel-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        [
                            'attribute' => 'total_import',
                            'format'    => 'html',
                            'value' => function($model){
                                $total_import ='<span id="total_rsvp" class="color-content" style="text-align: center;">'.$model->total_import.'</span>';
                                return $total_import;
                            }
                        ],
                        [
                            'attribute' => 'total_new_email',
                            'format'    => 'html',
                            'value' => function($model){
                                $total_new_email ='<span id="total_rsvp" class="color-content-new-email" style="text-align: center;">'.$model->total_new_email.'</span>';
                                return $total_new_email;
                            }
                        ],
                        [
                            'attribute' => 'total_exist_email',
                            'format'    => 'html',
                            'value' => function($model){
                                $total_exist_email ='<span id="total_rsvp" class="color-content-exist-email" style="text-align: center;">'.$model->total_exist_email.'</span>';
                                return $total_exist_email;
                            }
                        ],
                        [
                            'attribute' => 'total_subscribe_email',
                            'format'    => 'html',
                            'value' => function($model){
                                $total_subscribe_email ='<span id="total_rsvp" class="color-content-subs-email" style="text-align: center;">'.$model->total_subscribe_email.'</span>';
                                return $total_subscribe_email;
                            }
                        ],
                        [
                            'attribute' => 'total_unsubscribe_email',
                            'format'    => 'html',
                            'value' => function($model){
                                $total_unsubscribe_email ='<span id="total_rsvp" class="color-content-usubs-email" style="text-align: center;">'.$model->total_unsubscribe_email.'</span>';
                                return $total_unsubscribe_email;
                            }
                        ],
                        [
                            'attribute' => 'execution_time',
                            'format'    => 'html',
                            'value' => function($model){
                                $sent_time = CommonHelper::changeDateFormat($model->execution_time, 'H:i:s');
                                return $sent_time;
                            }
                        ],
                        [
                            'attribute' => 'created_at',
                            'format'    => 'html',
                            'value' => function($model){
                                $sent_time = CommonHelper::changeDateFormat($model->created_at, 'd M Y');
                                return $sent_time;
                            }
                        ],
                        [
                            'attribute' => 'status',
                            'format'    => 'html',
                            'value' => function($model){
                                $staus='';
                                if ($model->status==1){
                                    $staus='<span id="total_rsvp" class="color-content-status-pass-email" style="text-align: center;">Pass</span>';
                                }
                                else{
                                    $staus='<span id="total_rsvp" class="color-content-status-fail-email" style="text-align: center;">Fail</span>';
                                }
                                return $staus;
                            }
                        ],
                    ],
                ]) ?>
            </div>

        </div>
    </div>
</div>
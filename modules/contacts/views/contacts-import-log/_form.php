<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model unlock\modules\contacts\models\ContactsImportLog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contacts-import-log-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'total_import')->textInput() ?>

    <?= $form->field($model, 'maropost_contact_list')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total_duplicated_email')->textInput() ?>

    <?= $form->field($model, 'total_subscribe_email')->textInput() ?>

    <?= $form->field($model, 'total_unsubscribe_email')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model unlock\modules\contacts\models\ContactsImportLogSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contacts-import-log-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'total_import') ?>

    <?= $form->field($model, 'maropost_contact_list') ?>

    <?= $form->field($model, 'total_duplicated_email') ?>

    <?= $form->field($model, 'total_subscribe_email') ?>

    <?php // echo $form->field($model, 'total_unsubscribe_email') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

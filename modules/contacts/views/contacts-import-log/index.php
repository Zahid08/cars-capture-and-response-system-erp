<?php

use yii\helpers\Html;
use yii\grid\GridView;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $searchModel unlock\modules\contacts\models\ContactsImportLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('app', 'Contacts Maropost Import Logs');
?>
<div class="main-container" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-grid-content">
        <div class="admin-grid-toolbar" style="border-top: 3px solid #4677ba;">
            <div class="admin-grid-toolbar-left">
            </div>
            <div class="admin-grid-toolbar-right">
                <?= Summary::widget(['dataProvider' => $dataProvider]) ?>
            </div>
        </div>

        <div class="admin-grid-view" style="text-align: center">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
                'pager' => ['class' => LinkPager::className()],
                'columns' => [
                    [
                        'attribute' => 'id',
                        'format'    => 'html',
                        'value' => function($model){
                            $id = Html::a($model->id, ['view', 'id' => $model->id]);
                            return $id;
                        }
                    ],
                    [
                        'attribute' => 'total_import',
                        'format'    => 'html',
                        'value' => function($model){
                            $total_import ='<span id="total_rsvp" class="color-content" style="text-align: center;">'.$model->total_import.'</span>';
                            return $total_import;
                        }
                    ],
                    [
                        'attribute' => 'total_new_email',
                        'format'    => 'html',
                        'value' => function($model){
                            $total_new_email ='<span id="total_rsvp" class="color-content-new-email" style="text-align: center;">'.$model->total_new_email.'</span>';
                            return $total_new_email;
                        }
                    ],
                    [
                        'attribute' => 'total_exist_email',
                        'format'    => 'html',
                        'value' => function($model){
                            $total_exist_email ='<span id="total_rsvp" class="color-content-exist-email" style="text-align: center;">'.$model->total_exist_email.'</span>';
                            return $total_exist_email;
                        }
                    ],
                    [
                        'attribute' => 'total_subscribe_email',
                        'format'    => 'html',
                        'value' => function($model){
                            $total_subscribe_email ='<span id="total_rsvp" class="color-content-subs-email" style="text-align: center;">'.$model->total_subscribe_email.'</span>';
                            return $total_subscribe_email;
                        }
                    ],
                    [
                        'attribute' => 'total_unsubscribe_email',
                        'format'    => 'html',
                        'value' => function($model){
                            $total_unsubscribe_email ='<span id="total_rsvp" class="color-content-usubs-email" style="text-align: center;">'.$model->total_unsubscribe_email.'</span>';
                            return $total_unsubscribe_email;
                        }
                    ],
                    [
                        'attribute' => 'execution_time',
                        'format'    => 'html',
                        'value' => function($model){
                            $sent_time = CommonHelper::changeDateFormat($model->execution_time, 'H:i:s');
                            return $sent_time;
                        }
                    ],
                    [
                        'attribute' => 'created_at',
                        'format'    => 'html',
                        'value' => function($model){
                            $sent_time = CommonHelper::changeDateFormat($model->created_at, 'd M Y');
                            return $sent_time;
                        }
                    ],
                    [
                        'attribute' => 'status',
                        'format'    => 'html',
                        'value' => function($model){
                            $staus='';
                            if ($model->status==1){
                                $staus='<span id="total_rsvp" class="color-content-status-pass-email" style="text-align: center;">Pass</span>';
                            }
                            else{
                                $staus='<span id="total_rsvp" class="color-content-status-fail-email" style="text-align: center;">Fail</span>';
                            }
                            return $staus;
                        }
                    ],
                ],
            ]); ?>
        </div>
    </div>

</div>
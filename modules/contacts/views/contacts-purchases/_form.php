<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtons;
use unlock\modules\core\buttons\BackButton;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use unlock\modules\contacts\models\Contacts;

/* @var $this yii\web\View */
/* @var $model unlock\modules\contacts\models\ContactsPurchases */
/* @var $form yii\bootstrap\ActiveForm */

$this->title= ($model->isNewRecord) ? Yii::t('app', 'Create a new Purchases') : Yii::t('app', 'Update Purchases');
?>

<div class="main-container main-container-form" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a(Yii::t('app', 'Home'), Yii::$app->homeUrl) ?></li>
                <li><?= Html::a(Yii::t('app', 'Contacts Purchases'), Url::to('index')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-form-container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fas fa-pencil-alt"></i> <?= Html::encode($this->title) ?></h3>
                </div>
                <div class="panel-heading-button">
                    <?= BackButton::widget() ?>
                </div>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
                    /*'options' => ['enctype' => 'multipart/form-data'],*/
                    'validateOnBlur' =>true,
                    'enableAjaxValidation'=>true,
                    'errorCssClass'=>'has-error',
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
                        'options' => ['class' => 'form-group'],
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-2',
                            'offset' => '',
                            'wrapper' => '',
                            'hint' => '',
                        ],
                    ],
                ]); ?>

                <?=
                $form->field($model, 'contact_id')->widget(Select2::classname(), [
                    'data' => CommonHelper::contactNameList(),
                    'language' => 'de',
                    'options' => ['placeholder' => 'Select a contact ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
                <?=
                $form->field($model, 'development_id')->widget(Select2::classname(), [
                    'data' => CommonHelper::developmentDropdownList(),
                    'language' => 'de',
                    'options' => ['placeholder' => 'Select a development ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

				<?= $form->field($model, 'suite')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'purchase_date')->widget(
                    DatePicker::className(), [
                    'name' => 'purchase_date',
                    'value' => '12-31-2010',
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]);?>
                <?php
                    $current_year = date('Y');
                    $date = strtotime(''.$current_year.' -8 year');
                    $last_10_year = date('Y', $date); // echoes '2009-01-01'
                    $next_10_year = date('Y',strtotime($current_year . "+12 years"));
                    $year_array = array();
                    for($i = $last_10_year; $i <= $next_10_year; $i++){
                        $year_array[$i] = $i;
                    }
                ?>
				<?= $form->field($model, 'occupancy')->dropDownList($year_array, ['prompt' => 'Select a Occupancy']) ?>

				<?= $form->field($model, 'purchase_price')->textInput(['maxlength' => true]) ?>

				<?= $form->field($model, 'deal_status')->dropDownList(CommonHelper::contactDealStatusDropDownList()) ?>


				<?= $form->field($model, 'pm')->radioList([ 'Yes' => 'Yes', 'No' => 'No', ]) ?>


                <div class="admin-form-button">
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <?= FormButtons::widget() ?>
                        </div>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

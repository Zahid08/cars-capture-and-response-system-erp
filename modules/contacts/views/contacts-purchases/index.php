<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\jui\DatePicker;
use unlock\modules\core\grid\GridView;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\buttons\NewButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $searchModel unlock\modules\contacts\models\ContactsPurchasesSearch */
/* @var $dataProvider unlock\modules\core\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Contacts Purchases');
?>
<div class="main-container" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-grid-filter">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <div class="admin-grid-content">
        <div class="admin-grid-toolbar">
            <div class="admin-grid-toolbar-left">
                <?= NewButton::widget(['url' => Url::toRoute(['create'])]) ?>
            </div>
            <div class="admin-grid-toolbar-right">
                <?= Summary::widget(['dataProvider' => $dataProvider]) ?>
            </div>
        </div>

        <div class="admin-grid-view">
                            <?php 
                echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
                'pager' => ['class' => LinkPager::className()],
                'columns' => [
                    [
                        'attribute' => 'id',
                        'format'    => 'html',
                        'value' => function($model){
                            $name = Html::a($model->id, ['view', 'id' => $model->id]);
                            return $name;
                        }
                    ],
                    [
                        'attribute' => 'contact_id',
                        'format' => 'html',
                        'headerOptions' => ['style' => 'color:#337ab7;'],
                        'value' => function($model){
                            $contact = CommonHelper::contactIdByContacts($model->contact_id);
                            $name = Html::a($contact->first_name." ".$contact->last_name, ['/contacts/contacts/view', 'id' => $contact->id]);
                            return $name;
                        }
                    ],
                    [
                        'attribute'  => 'development_id',
                        'format' => 'html',
                        'value' => function($model){
                            $developmentName = CommonHelper::developmentListById($model->development_id);
                            return '<a href="#">'.$developmentName['development_name'].'</a>';
                        }
                    ],
                    'suite',
                    'purchase_date',
                    'occupancy',
                    [
                        'attribute'  => 'purchase_price',
                        'value' => function($model){
                            return '$'.Yii::$app->formatter->format( $model->purchase_price, 'money');
                        }
                    ],
                    [
                        'attribute'  => 'deal_status',
                        'value' => function($model){
                            return CommonHelper::contactDealStatusDropDownList($model->deal_status);
                        }
                    ],
                    'pm',
                    // 'created_at',
                    // 'created_by',
                    // 'updated_at',
                    // 'updated_by',
                    /*'status',*/
                    //['class' => 'unlock\modules\core\grid\ActionColumn'],
                ],
                ]);   ?>
                    </div>
    </div>

</div>

<?php

use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\widgets\DetailView;
use unlock\modules\core\buttons\BackButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $model unlock\modules\contacts\models\ContactsPurchases */

$this->title = Yii::t('app', 'View Contacts Purchases');
?>

<div class="main-container main-container-view" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li><?= Html::a(Yii::t('app', 'Contacts Purchases'), Url::to('index')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-view-container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fa fa-eye"></i> <?= Yii::t('app', 'View') ?> - <?= Html::encode($model->id) ?></h3>
                </div>
                <div class="panel-heading-button">
                    <?= BackButton::widget() ?>
                </div>
            </div>

            <div class="panel-body">
                <?php
                $contactName = CommonHelper::contactIdByContacts($model->contact_id);
                ?>
                <div class="developer-status-details">
                    <div class="row">
                        <div class="col-md-3 col-12 contact-name">
                            <p><span class="grade-value">Name: </span><?php echo $contactName->first_name.' '.$contactName->last_name; ?></p>
                        </div>
                        <div class="col-md-2 col-12 contact-number">
                            <p><span class="grade-value">Lead Score: </span><?php echo $contactName->lead_score; ?></p>
                        </div>
                        <div class="col-md-3 col-12 contact-number">
                            <p><span class="grade-value">Phone: </span><?php echo $contactName->phone; ?></p>
                        </div>
                        <div class="col-md-4 col-12 contact-email">
                            <p><span class="grade-value">Email: </span><?php echo $contactName->email; ?></p>
                        </div>
                    </div>
                </div>

                <div class="divider"></div>

                <div id="app" class="tab_section">

                    <tabs>
                        <tab name="Deal" :selected="true">
                            <?= DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    //'id',
                                    /*[
                                        'attribute' => 'contact_id',
                                        'value' => function($model) {
                                            $contactName = CommonHelper::contactIdByContacts($model->contact_id);
                                            return $contactName->first_name.' '.$contactName->last_name;
                                        },
                                    ],*/
                                    [
                                        'attribute' => 'development_id',
                                        'format'    => 'html',
                                        'value' => function($model) {
                                            $developmentName = CommonHelper::developmentListById($model->development_id);

                                            return '<a href="'.Url::toRoute('developments/developments/view?id='.$model->development_id).'">'.$developmentName['development_name'].'</a>';;
                                        },
                                    ],
                                    'suite',
                                    [
                                        'attribute' => 'purchase_date',
                                        'value' => function($model) {
                                            return CommonHelper::changeDateFormat($model->purchase_date, 'd M Y');
                                        },
                                    ],
                                    'occupancy',
                                    [
                                        'attribute' => 'purchase_price',
                                        'value'     => function($model){
                                            return '$'.Yii::$app->formatter->format( $model->purchase_price, 'money');
                                        }
                                    ],

                                    [
                                        'attribute' => 'deal_status',
                                        'format'    => 'raw',
                                        'value' => function($model) {
                                            $urlInterac = yii\helpers\Url::to(['contacts-purchases/change-status?id='.$model->id]);
                                            return Html::dropDownList('status-dropdown', $model->deal_status, CommonHelper::contactDealStatusDropDownList(), [
                                                'class' => 'status-dropdown',
                                                'onchange' => "
                                            var statusid = $(this).val();
                                            console.log(statusid);
                                            $.ajax('$urlInterac&status='+statusid+'', {
                                                type: 'POST'
                                            }).done(function(data) {
                                                //$.pjax.reload({container: '#pjax-container'});
                                            });
                                          
                                        ",
                                            ]);
                                            //return CommonHelper::contactDealStatusDropDownList($model->deal_status);
                                        },
                                    ],
                                    'pm',
                                    /*'created_at:datetime',
                                    'created_by:user',
                                    'updated_at:datetime',
                                    'updated_by:user',*/
                                ],
                            ]) ?>
                        </tab>
                        <tab name="Deposits">
                            <table class="depagg7" width="100%">
                                <tr>
                                    <td style="padding: 8px;">Agreement Date</td>
                                    <td style="padding: 8px;">12 Feb 2019</td>
                                </tr>
                                <tr>
                                    <td style="padding: 8px;">Deposit with Agreement</td>
                                    <td style="padding: 8px;"><input value="<?php echo $model->purchase_price; ?>" type="text"></td>
                                </tr>
                            </table>
                            <div class="divider"></div>
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Deposit No</th>
                                        <th>Deposit Date</th>
                                        <th>Amount</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>12 Feb 2019</td>
                                        <td>$27000</td>
                                        <td>Upcoming</td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>12 Feb 2019</td>
                                        <td>$27000</td>
                                        <td>Upcoming</td>
                                    </tr>
                                </tbody>
                            </table>
                            <button class="btn btn-default pull-right">Add Deposit</button>
                            <div class="clearfix"></div>
                            <div class="divider"></div>
                        </tab>
                        <tab name="Purchaser">
                            <table class="table table-striped table-bordered">
                                <tbody>
                                <tr>
                                    <td>Contact Name</td>
                                    <td><?php echo $contactName->first_name.' '.$contactName->last_name; ?></td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td><?php echo $contactName->email; ?></td>
                                </tr>
                                <tr>
                                    <td>Phone Number</td>
                                    <td><?php echo $contactName->phone; ?></td>
                                </tr>
                                <tr>
                                    <td>Country</td>
                                    <td>
                                        <?php
                                            $country = CommonHelper::countryListByCountryId($contactName->country);
                                            if(!empty($country)){
                                                echo $country->country_name;
                                                //echo CommonHelper::countryListByCountryId($contactName->country);
                                            }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Province/State</td>
                                    <td><?php //echo CommonHelper::provinceListByProvinceId($contactName->province); ?></td>
                                </tr>
                                <tr>
                                    <td>Address</td>
                                    <td><?php echo $contactName->address;?></td>
                                </tr>
                                <tr>
                                    <td>Postal Code</td>
                                    <td><?php echo $contactName->postal;?></td>
                                </tr>
                                </tbody>
                            </table>
                        </tab>
                        <tab name="Sales Notes">
                            Comming Soon............
                        </tab>
                        <tab name="System">
                            <table class="table table-striped table-bordered">
                                <tbody>
                                    <tr>
                                        <td>Purchase Record ID</td>
                                        <td>78</td>
                                    </tr>
                                    <tr>
                                        <td>Reservation Record ID</td>
                                        <td>54478</td>
                                    </tr>
                                    <tr>
                                        <td>Created Date</td>
                                        <td>	30 Jan 2019</td>
                                    </tr>
                                    <tr>
                                        <td>Created By</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Last Record Update</td>
                                        <td>30 Jan 2019</td>
                                    </tr>
                                    <tr>
                                        <td>Last Record Update By</td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </tab>
                    </tabs>
                </div>
                <div class="divider"></div>
                <div class="row">
                    <div class="col-md-4 text-left">
                        <a class="btn btn-primary" href="<?php echo Yii::$app->request->referrer; ?>">Back</a>
                    </div>
                    <div class="col-md-4 text-center">
                        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    </div>
                    <div class="col-md-4 text-right">
                        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtons;
use unlock\modules\core\buttons\BackButton;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use unlock\modules\contacts\models\Contacts;

/* @var $this yii\web\View */
/* @var $model unlock\modules\contacts\models\ContactsPurchases */
/* @var $form yii\bootstrap\ActiveForm */

$this->title= ($model->isNewRecord) ? Yii::t('app', 'Create a new Purchases') : Yii::t('app', 'Update Purchases');
?>

<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
    /*'options' => ['enctype' => 'multipart/form-data'],*/
    'id' => 'add-contact-form-2',
    'validateOnBlur' =>true,
    'enableAjaxValidation'=>false,
    'errorCssClass'=>'has-error',
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-sm-9\">\n{input}\n{hint}\n{error}\n</div>",
        'options' => ['class' => 'form-group'],
        'horizontalCssClasses' => [
            'label' => 'col-sm-3',
            'offset' => '',
            'wrapper' => '',
            'hint' => '',
        ],
    ],
]); ?>

<?=
$form->field($model, 'development_id')->widget(Select2::classname(), [
    'data' => CommonHelper::developmentDropdownList(),
    'language' => 'de',
    'options' => ['placeholder' => 'Select a development ...'],
    'pluginOptions' => [
        'allowClear' => true
    ],
]);
?>

<?= $form->field($model, 'suite')->textInput(['maxlength' => true]) ?>


<div style="margin-bottom: 10px;" class="form-group field-contactspurchases-purchase_date required">
    <label class="control-label col-sm-3" for="contactspurchases-purchase_date">Purchase Date</label>
    <div class="col-sm-9">
        <?php
        $todaydatetime = date("Y-m-d");
        echo DatePicker::widget([
            'name' => 'ContactsPurchases[purchase_date]',
            'value' => $todaydatetime,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd',
            ]
        ]);
        ?>
    </div>
    <p class="help-block help-block-error "></p>
</div>


<?= $form->field($model, 'occupancy')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'purchase_price')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'deal_status')->dropDownList(CommonHelper::contactDealStatusDropDownList()) ?>

<?= $form->field($model, 'pm')->radioList([ 'Yes' => 'Yes', 'No' => 'No', ]) ?>

<?= $form->field($model, 'contact_id')->hiddenInput(['value' => $_REQUEST['contactid']])->label('') ?>

<div class="admin-form-button">
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <?= FormButtons::widget() ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>

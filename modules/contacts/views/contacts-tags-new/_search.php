<?php

use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\buttons\ExportCsvButton;
use unlock\modules\core\buttons\ExportPdfButton;
use unlock\modules\core\buttons\ResetFilterButton;
use unlock\modules\core\buttons\SearchFilterButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $model unlock\modules\contacts\models\ContactsTagsNewSearch */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="admin-search-form-container contacts-tags-new-search">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
        'validateOnBlur' => false,
        'enableAjaxValidation' => false,
        'errorCssClass'=>'has-error',
        'fieldConfig' => [
            'template' => "{label}\n{input}\n",
            'options' => ['class' => 'col-sm-4'],
            'horizontalCssClasses' => [
                'label' => '',
                'offset' => '',
                'wrapper' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <div class="admin-search-form-fields">

        <div class="row">
            <div class="col-sm-12 col-md-8 col-lg-9">
			<?= $form->field($model, 'tag_name') ?>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-3">
                <div class="filter-custom-search-btn">
                    <?= SearchFilterButton::widget() ?>
                    <a class="btn btn-default" href="<?php echo Url::toRoute(['index']);?>"> Reset Filter</a>
                </div>
            </div>
        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>

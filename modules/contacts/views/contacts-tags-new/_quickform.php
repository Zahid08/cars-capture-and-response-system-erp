<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtonsCustomAction;
use unlock\modules\core\buttons\BackButton;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use unlock\modules\contacts\models\Contacts;
use api\modules\v1\models\ContactsTagsNew;

/* @var $this yii\web\View */
/* @var $model unlock\modules\contacts\models\ContactsRsvp */
/* @var $form yii\bootstrap\ActiveForm */

$this->title= ($model->isNewRecord) ? Yii::t('app', 'Add to Prospect') : Yii::t('app', 'Update Prospect Tag');
?>

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
        /*'options' => ['enctype' => 'multipart/form-data'],*/
        'id' => 'add-contact-form-4',
        'validateOnBlur' =>true,
        'enableAjaxValidation'=>false,
        'errorCssClass'=>'has-error',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-sm-9\">\n{input}\n{hint}\n{error}\n</div>",
            'options' => ['class' => 'form-group'],
            'horizontalCssClasses' => [
                'label' => 'col-sm-3',
                'offset' => '',
                'wrapper' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?php
    echo $form->field($model, 'tag_category_id')->widget(Select2::classname(), [
        'data' => CommonHelper::contactsCategoryTagsDropdownlist(),
        'language' => 'en',
        'options' => ['placeholder' => 'Select Category'],
        'pluginOptions' => [
            'allowClear' => true,
        ]
    ]);
    ?>

    <?=
    $form->field($model, 'tag_name')->widget(Select2::classname(), [
        'language' => 'en',
        'options' => ['placeholder' => 'Select a tag_name ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>
    <div class="col-sm-10 col-sm-offset-3" style="margin-bottom: 10px;">
        <span style="padding: 15px;font-size: 20px;" class="success-message"></span>
    </div>

    <div class="admin-form-button" style="margin-top: 20px;">
        <div class="form-group">
            <div class="col-sm-10 col-sm-offset-3">
                <?= FormButtonsCustomAction::widget() ?>
            </div>
        </div>
    </div>

<?php ActiveForm::end(); ?>

<?php
$baseUrl = Yii::getAlias('@baseUrl');
$js = <<< JS

        $('#contactstagsnew-tag_category_id').on('change', function() {
            var cat_id=$(this).val();
            var url='$baseUrl/backend/web/contacts/contacts/tag?id='+cat_id+'';
            $.post(url, function(data){
                                  $("select#contactstagsnew-tag_name").html(data);
            });
        });

        $('.save-btn-id').click(function(){
            var catname=$('#contactstagsnew-tag_category_id').val();
            if (catname !=''){
                $('.success-message').html("Tag has been successfully applied !");
                 $('.success-message').hide().delay(800).fadeIn(800).delay(4000).fadeOut(800);
            }
        });

       $('.done-btn-id').click(function(){
             var catname=$('#contactstagsnew-tag_category_id').val();
             var contact_id_get=$('#contact_id_get').val();
            if (catname !=''){
                  $('.save-btn-id').css('cursor','not-allowed');
                   $('button.btn.btn-check').text("Sending....");
                   $('button.btn.btn-check').css('cursor','not-allowed');
                 var timer = setTimeout(function() {
            window.location = '$baseUrl/backend/web/contacts/contacts/view?id=' +contact_id_get;
            }, 3000);}
         });
JS;
$this->registerJs($js);
?>

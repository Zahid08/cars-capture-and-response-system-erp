<?php

use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\grid\GridView;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\buttons\NewButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $searchModel unlock\modules\contacts\models\ContactsTagsNewSearch */
/* @var $dataProvider unlock\modules\core\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Contacts Tags');
?>
<div class="main-container" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-grid-filter">
        <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <div class="admin-grid-content">
        <div class="admin-grid-toolbar">
            <div class="admin-grid-toolbar-left">
                <?= NewButton::widget(['url' => Url::toRoute(['create'])]) ?>
            </div>
            <div class="admin-grid-toolbar-right">
                <?= Summary::widget(['dataProvider' => $dataProvider]) ?>
            </div>
        </div>

        <div class="admin-grid-view">
                            <?php 
                echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
                'pager' => ['class' => LinkPager::className()],
                'columns' => [
                	'id',
                    [
                        'attribute' => 'tag_name',
                        'format'    => 'html',
                        'value' => function($model){
                            $name = Html::a($model->tag_name, ['update', 'id' => $model->id]);
                            return $name;
                        }
                    ],
                    [
                        'attribute' => 'tag_category_id',
                        'value' => function($model){
                            $category = CommonHelper::TagCategoryById($model->tag_category_id);
                            $categoryName = '';
                            if(!empty($category->tag_category_name)){
                                $categoryName = $category->tag_category_name;
                            }
                            return $categoryName;
                        }
                    ],
                    [
                        'attribute' => 'tag_applied',
                        'headerOptions' => ['style' => 'width:30%'],
                        'value' => function($model){
                            $applied_date =\unlock\modules\contacts\models\ContactsTagsApplied::find()->where(['tag_id' => $model->id])->count();
                            return $applied_date;

                        }
                    ],

                    /* ['class' => 'unlock\modules\core\grid\ActionColumn'],*/

			    // 'status',
                ],
                ]);   ?>
                    </div>
    </div>

</div>

<?php

use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\buttons\ExportCsvButton;
use unlock\modules\core\buttons\ExportPdfButton;
use unlock\modules\core\buttons\ResetFilterButton;
use unlock\modules\core\buttons\SearchFilterButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $model unlock\modules\contacts\models\AgentSearch */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="admin-search-form-container agent-search">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
        'validateOnBlur' => false,
        'enableAjaxValidation' => false,
        'errorCssClass'=>'has-error',
        'fieldConfig' => [
            'template' => "{label}\n{input}\n",
            'options' => ['class' => 'col-sm-4'],
            'horizontalCssClasses' => [
                'label' => '',
                'offset' => '',
                'wrapper' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <div class="admin-search-form-fields">

        <div class="row">

            <div class="col-sm-12 col-md-8 col-lg-9">
                <?= $form->field($model, 'allsearch') ?>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-3">
                <div class="filter-custom-search-btn">
                    <?= SearchFilterButton::widget() ?>
                    <?php //ResetFilterButton::widget(['url' => Url::toRoute(['index'])]) ?>
                    <a class="btn btn-default" href="<?php echo Url::toRoute(['index']);?>"> Reset Filter</a>
                </div>
            </div>


            <?php // echo $form->field($model, 'email') ?>

			<?php // echo $form->field($model, 'mobile') ?>

			<?php // echo $form->field($model, 'office_phone') ?>

			<?php // echo $form->field($model, 'website') ?>

			<?php // echo $form->field($model, 'company_name') ?>

			<?php // echo $form->field($model, 'address') ?>

			<?php // echo $form->field($model, 'suite') ?>

			<?php // echo $form->field($model, 'city') ?>

			<?php // echo $form->field($model, 'province') ?>

			<?php // echo $form->field($model, 'postal_code') ?>

			<?php // echo $form->field($model, 'country') ?>

			<?php // echo $form->field($model, 'agent_type') ?>

			<?php // echo $form->field($model, 'brand') ?>

			<?php // echo $form->field($model, 'created_at') ?>

			<?php // echo $form->field($model, 'created_by') ?>

			<?php // echo $form->field($model, 'updated_at') ?>

			<?php // echo $form->field($model, 'updated_by') ?>

			<?php // echo $form->field($model, 'status') ?>

        </div>

       <!-- <div class="row">
            <div class="col-sm-6 filter-custom-search-btn">
                <?/*= SearchFilterButton::widget() */?>
                <?/*= ResetFilterButton::widget(['url' => Url::toRoute(['index'])]) */?>
            </div>
            <div class="col-sm-6 filter-custom-export-btn">
                <?/*= ExportCsvButton::widget(['url' => Url::toRoute(['export-csv'])]) */?>
                <?/*= ExportPdfButton::widget(['url' => Url::toRoute(['export-pdf'])]) */?>
            </div>
        </div>-->

    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\grid\GridView;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\buttons\NewButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $searchModel unlock\modules\contacts\models\AgentSearch */
/* @var $dataProvider unlock\modules\core\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Agent');
?>
<div class="main-container" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-grid-filter">
        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <div class="admin-grid-content">
        <div class="admin-grid-toolbar">
            <div class="admin-grid-toolbar-left">
                <?= NewButton::widget(['url' => Url::toRoute(['create'])]) ?>
            </div>
            <div class="admin-grid-toolbar-right">
                <?= Summary::widget(['dataProvider' => $dataProvider]) ?>
            </div>
        </div>

        <div class="admin-grid-view">
                            <?php 
                echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
                'pager' => ['class' => LinkPager::className()],
                'columns' => [
                	'id',
                    [
                        'attribute' => 'Name',
                        'format'    => 'html',
                        'headerOptions' => ['style' => 'color:#337ab7;'],
                        'value' => function($model) {
                            $name = Html::a($model->first_name.' '.$model->last_name, ['update', 'id' => $model->id]);
                            return $name;
                        },
                    ],

                    'email:email',
                    'mobile',
                  /* [
                        'attribute' => 'created_at',
                        'value'     => function($model){
                            return CommonHelper::changeDateFormat($model->created_at, 'd M Y');
                        }
                    ],*/
                    [
                        'attribute' => 'agent_type',
                        'value'     => function($model){
                            $agentType = CommonHelper::agentTypeListByAgentId($model->agent_type);
                            $agentTypeName = '';
                            if(!empty($agentType->agent_type_name)){
                                $agentTypeName = $agentType->agent_type_name;
                            }
                            return $agentTypeName;
                        }
                    ],
                    [
                        'attribute' => 'brand',
                        'headerOptions' => ['style' => 'width:260px;'],
                        'value'     => function($model){
                            $brands = CommonHelper::brandsListByBrandId($model->brand);
                            $brandName = '';
                            if(!empty($brands->brand_name)){
                                $brandName = $brands->brand_name;
                            }
                            return $brandName;
                        }
                    ],

			// 'office_phone',
			// 'website',
			// 'company_name',
			// 'address',
			// 'suite',
			// 'city',
			// 'province',
			// 'postal_code',
			// 'country',
			// 'agent_type',
			// 'brand',
			// 'created_at',
			// 'created_by',
			// 'updated_at',
			// 'updated_by',
			// 'status',

               /* [
                'attribute' => 'status',
                'format' => 'status',
                'filter' => Html::activeDropDownList($searchModel, 'status', CommonHelper::statusDropDownList(), ['prompt' => CommonHelper::GRID_FILTER_PROMPT]),
                ],*/
                    /*['class' => 'yii\grid\ActionColumn',
                        'header' => 'Actions',
                        'headerOptions' => ['style' => 'color:#337ab7;width:8%'],
                        'template'=>'{details}',
                        'buttons'=>[
                            'details' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span> Details', $url, [
                                    'title' => Yii::t('app', 'View'),
                                ]);
                            },
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            if ($action === 'details') {
                                $url =  Url::toRoute('/contacts/agent/view/?id=').$model->id;
                                return $url;
                            }
                        }
                    ],*/
                ],
                ]);   ?>
                    </div>
    </div>

</div>

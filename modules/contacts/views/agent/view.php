<?php

use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\widgets\DetailView;
use unlock\modules\core\buttons\BackButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $model unlock\modules\contacts\models\Agent */

$this->title = Yii::t('app', 'View Agent');
?>
<div class="main-container main-container-view" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li><?= Html::a(Yii::t('app', 'Agent'), Yii::$app->homeUrl.Url::to('contacts/agent/index')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-view-container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fa fa-eye"></i> <?= Yii::t('app', 'View') ?> - <?= Html::encode($model->id) ?></h3>
                </div>
                <div class="panel-heading-button">
                    <?= BackButton::widget() ?>
                </div>
            </div>

            <div class="panel-body">
                <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                	'id',
					'first_name',
					'last_name',
					'email:email',
					'mobile',
					'office_phone',
					'website',
					'company_name',
					'address',
					'suite',
                    [
                        'attribute' => 'city',
                        'value'     => function($model){
                            $city = CommonHelper::cityListByCityId($model->city);
                            $cityName = '';
                            if(!empty($city->city_name)){
                                $cityName = $city->city_name;
                            }
                            return $cityName;
                        }
                    ],
                    [
                        'attribute' => 'province',
                        'value'     => function($model){
                            $province = CommonHelper::provinceListByProvinceId($model->province);
                            $provinceName = '';
                            if(!empty($province->province_name)){
                                $provinceName = $province->province_name;
                            }
                            return $provinceName;
                        }
                    ],
					'postal_code',

                    [
                        'attribute' => 'country',
                        'value'     => function($model){
                            $country = CommonHelper::countryListByCountryId($model->country);
                            $countryName = '';
                            if(!empty($country->country_name)){
                                $countryName = $country->country_name;
                            }
                            return $countryName;
                        }
                    ],
                    [
                        'attribute' => 'agent_type',
                        'value'     => function($model){
                            $agentType = CommonHelper::agentTypeListByAgentId($model->agent_type);
                            $agentTypeName = '';
                            if(!empty($agentType->agent_type_name)){
                                $agentTypeName = $agentType->agent_type_name;
                            }
                            return $agentTypeName;
                        }
                    ],
                    [
                        'attribute' => 'brand',
                        'value'     => function($model){
                            $brands = CommonHelper::brandsListByBrandId($model->brand);
                            $brandName = '';
                            if(!empty($brands->brand_name)){
                                $brandName = $brands->brand_name;
                            }
                            return $brandName;
                        }
                    ],
					'created_at:datetime',
					'created_by:user',
					'updated_at:datetime',
					'updated_by:user',
					'status:status',
                ],
                ]) ?>
                <div class="row">
                    <div class="col-md-4 text-left">
                        <?= Html::a('Back to Agent List', ['agent/index'], ['class' => 'btn btn-primary']) ?>
                    </div>
                    <div class="col-md-4 text-center">
                        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    </div>
                    <div class="col-md-4 text-right">
                        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

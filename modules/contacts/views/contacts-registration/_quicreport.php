<?php

use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\grid\GridView;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\buttons\NewButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $searchModel unlock\modules\contacts\models\ContactsRegistrationSearch */
/* @var $dataProvider unlock\modules\core\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Contacts Registration');
?>

<div class="main-container" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-grid-filter">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <div class="admin-grid-content">
        <div class="admin-grid-toolbar " style="border-top: 3px solid #4677ba;">
            <div class="admin-grid-toolbar-left">
            </div>
            <div class="admin-grid-toolbar-right">
                <?= Summary::widget(['dataProvider' => $dataProvider]) ?>
            </div>
        </div>

        <div class="admin-grid-view">
            <?php
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
                'pager' => ['class' => LinkPager::className()],
                'columns' => [
                    //['class' => 'unlock\modules\core\grid\SerialColumn'],
                    [
                        'attribute' => 'id',
                        'format'    => 'html',
                        'value' => function($model){
                            $name = Html::a($model->id, ['update', 'id' => $model->id]);
                            return $name;
                        }
                    ],
                    [
                        'attribute'  => 'development_id',
                        'format'    => 'html',
                        'value' => function($model){
                            $developmentName = CommonHelper::developmentListById($model->development_id);
                            $devName = '';
                            if(!empty($developmentName['development_name'])){
                                $devName = Html::a($developmentName['development_name'], ['/developments/developments/view', 'id' => $developmentName->id]);
                            }
                            return $devName;
                        }
                    ],
                    [
                        'attribute' => 'contact_id',
                        'format' => 'html',
                        'label' => 'First Name',
                        'headerOptions' => ['style' => 'color:#337ab7;'],
                        'value' => function($model){
                            $contact = CommonHelper::contactIdByContacts($model->contact_id);
                            if(!empty($contact)){
                                //$name = Html::a($contact['first_name']." ".$contact['last_name'], ['/contacts/contacts/view', 'id' => $contact['id']]);
                                return $contact['first_name'];
                            }
                        }
                    ],
                    [
                        'attribute' => 'contact_id',
                        'format' => 'html',
                        'label' => 'Last Name',
                        'headerOptions' => ['style' => 'color:#337ab7;'],
                        'value' => function($model){
                            $contact = CommonHelper::contactIdByContacts($model->contact_id);
                            if(!empty($contact)){
                                //$name = Html::a($contact['first_name']." ".$contact['last_name'], ['/contacts/contacts/view', 'id' => $contact['id']]);
                                return $contact['last_name'];
                            }
                        }
                    ],
                    [
                        'attribute' => 'created_at',
                        'format' => 'html',
                        'label' => 'Date',
                        'headerOptions' => ['style' => 'color:#337ab7;'],
                        'value' => function($model){
                                return  CommonHelper::changeDateFormat($model->created_at,'d M Y H g:i a');
                        }
                    ],
                    [
                        'attribute'  => 'development_id',
                        'format'    => 'html',
                        'label' => 'User',
                        'headerOptions' => ['style' => 'width:160px;'],
                        'value' => function($model){
                            $developmentName = CommonHelper::developmentListById($model->development_id);
                            $devName = '';
                            if(!empty($developmentName['assignment_id'])){
                                $username=CommonHelper::getLoggedInUserNameByUserId($developmentName->assignment_id);
                                $devName =$username;
                            }
                            return $devName;
                        }
                    ],
                    [
                        'attribute'  => 'status',
                        'format'    => 'raw',
                        'headerOptions' => ['style' => 'width:120px;'],
                        'value' => function($model){
                            $urlInterac = yii\helpers\Url::to(['contacts-registration/change-status?id='.$model->id]);
                            return Html::dropDownList('status-dropdown', $model->status, CommonHelper::contactStatusTypeDropDownList(), [
                                'class' => 'status-dropdown',
                                'onchange' => "
                                            var statusid = $(this).val();
                                            console.log(statusid);
                                            $.ajax('$urlInterac&status='+statusid+'', {
                                                type: 'POST'
                                            }).done(function(data) {
                                                //$.pjax.reload({container: '#pjax-container'});
                                            });
                                          
                                        ",
                            ]);

                            ?>
                            <?php
                        }
                    ],
                ],
            ]);   ?>
        </div>
    </div>

</div>
<?php

?>

<?php

use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\grid\GridView;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\buttons\NewButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $searchModel unlock\modules\contacts\models\ContactsRegistrationSearch */
/* @var $dataProvider unlock\modules\core\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Contacts Registration');
?>

<div class="main-container" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-grid-filter">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <div class="admin-grid-content">
        <div class="admin-grid-toolbar">
            <div class="admin-grid-toolbar-left">
                <?= NewButton::widget(['url' => Url::toRoute(['create'])]) ?>
            </div>
            <div class="admin-grid-toolbar-right">
                <?= Summary::widget(['dataProvider' => $dataProvider]) ?>
            </div>
        </div>

        <div class="admin-grid-view">
                            <?php 
                echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
                'pager' => ['class' => LinkPager::className()],
                'columns' => [
                //['class' => 'unlock\modules\core\grid\SerialColumn'],
                    [
                        'attribute' => 'id',
                        'format'    => 'html',
                        'value' => function($model){
                            $name = Html::a($model->id, ['update', 'id' => $model->id]);
                            return $name;
                        }
                    ],
                    [
                        'attribute' => 'contact_id',
                        'format' => 'html',
                        'headerOptions' => ['style' => 'color:#337ab7;'],
                        'value' => function($model){
                            $contact = CommonHelper::contactIdByContacts($model->contact_id);
                            if(!empty($contact)){
                                $name = Html::a($contact['first_name']." ".$contact['last_name'], ['/contacts/contacts/view', 'id' => $contact['id']]);
                                return $name;
                            }
                        }
                    ],
                    [
                        'attribute'  => 'development_id',
                        'format'    => 'html',
                        'value' => function($model){
                            $developmentName = CommonHelper::developmentListById($model->development_id);
                            $devName = '';
                            if(!empty($developmentName['development_name'])){
                                $devName = Html::a($developmentName['development_name'], ['/developments/developments/view', 'id' => $developmentName->id]);
                            }
                            return $devName;
                        }
                    ],
                    /*[
                        'attribute'  => 'reg_type',
                        'value' => function($model){
                            return CommonHelper::contactRegistrationTypeDropDownList($model->reg_type);
                        }
                    ],*/
                    [
                        'attribute'  => 'reg_source',
                        'format'    => 'raw',
                        'headerOptions' => ['style' => 'width:100px;'],
                        'value' => function($model){
                            if(!empty($model->reg_source)){
                                $urlLead = yii\helpers\Url::to(['contacts-registration/change-lead-source?id='.$model->id]);
                                return Html::dropDownList('status-dropdown', $model->reg_source, CommonHelper::leadSourceDropDownList(), [
                                    'class' => 'status-dropdown',
                                    'onchange' => "
                                                var statusid = $(this).val();
                                                console.log(statusid);
                                                $.ajax('$urlLead&status='+statusid+'', {
                                                    type: 'POST'
                                                }).done(function(data) {
                                                    //$.pjax.reload({container: '#pjax-container'});
                                                });
                                              
                                            ",
                                ]);
                            }
                        }
                    ],
                    //'reg_datetime:datetime',
                    [
                        'attribute'  => 'status',
                        'format'    => 'raw',
                        'headerOptions' => ['style' => 'width:120px;'],
                        'value' => function($model){
                            $urlInterac = yii\helpers\Url::to(['contacts-registration/change-status?id='.$model->id]);
                            return Html::dropDownList('status-dropdown', $model->status, CommonHelper::contactStatusTypeDropDownList(), [
                                    'class' => 'status-dropdown',
                                    'onchange' => "
                                            var statusid = $(this).val();
                                            console.log(statusid);
                                            $.ajax('$urlInterac&status='+statusid+'', {
                                                type: 'POST'
                                            }).done(function(data) {
                                                //$.pjax.reload({container: '#pjax-container'});
                                            });
                                          
                                        ",
                            ]);

                            ?>
                            <?php
                        }
                    ],
                    [
                        'attribute' => 'status_date',
                        'format' => 'html',
                        'headerOptions' => ['style' => 'width:210px;'],
                        'value' => function($model){
                            if(!empty($model->status_date)){
                                return CommonHelper::changeDateFormat($model->status_date,'d M Y H g:i a');
                            }
                        }
                    ],
                    [
                        'attribute' => 'current_result',
                        'format' => 'html',
                        'headerOptions' => ['style' => 'width:150px;'],
                        'value' => 'current_result'
                    ],

                    //'status_date:datetime',
                    // 'status',
                    // 'status_date',
                    // 'created_at',
                    // 'created_by',
                    // 'updated_at',
                    // 'updated_by',
                      /*  [
                        'attribute' => 'created_at',
                        'format' => 'datetime',
                        'filter' => DatePicker::widget([
                        'name'  => 'SampleSearch[created_at]',
                        'dateFormat' => 'yyyy-MM-dd'
                        ]),
                        ],*/
                    /*[
                    'attribute' => 'status',
                    'format' => 'status',
                    'filter' => Html::activeDropDownList($searchModel, 'status', CommonHelper::statusDropDownList(), ['prompt' => CommonHelper::GRID_FILTER_PROMPT]),
                    ],*/
                    //['class' => 'unlock\modules\core\grid\ActionColumn'],
                ],
                ]);   ?>
                    </div>
    </div>

</div>
<?php

?>

<?php

use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\widgets\DetailView;
use unlock\modules\core\buttons\BackButton;
use unlock\modules\core\helpers\CommonHelper;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model unlock\modules\contacts\models\ContactsRegistration */

$this->title = Yii::t('app', 'View Contacts Registration');
?>
<div class="main-container main-container-view" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li><?= Html::a(Yii::t('app', 'Contacts Registration'), Url::to('index')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>
    <?php
    Modal::begin([
        'header' => '<h4>Create a New Interaction</h4>',
        'id'     => 'model-interaction',
        'size'   => 'model-lg',
    ]);

    echo "<div id='add-contactInteraction-modelContent'></div>";

    Modal::end();
    ?>
    <div class="admin-view-container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fa fa-eye"></i> <?= Yii::t('app', 'View') ?> - <?= Html::encode($model->id) ?></h3>
                </div>
                <div class="panel-heading-button">
                    <?= BackButton::widget() ?>
                </div>
            </div>

            <div class="panel-body">

                <div class="user-group">
                    <h4 style="text-align: left;text-transform: uppercase;margin-bottom: 5px; margin-top: 25px;">User Group</h4>
                    <?php
                        $contactName = CommonHelper::contactIdByContacts($model->contact_id);
                        $developmentName = CommonHelper::developmentListById($model->development_id);
                    ?>
                    <table class="table table-striped table-bordered">
                        <tbody>
                            <tr>
                                <td>Contact Name</td>
                                <td><?php echo $contactName->first_name.' '.$contactName->last_name;?></td>
                            </tr>
                            <tr>
                                <td>Development Name</td>
                                <td><?php echo $developmentName['development_name'];?></td>
                            </tr>
                            <tr>
                                <td>Registration Type</td>
                                <td><?php echo CommonHelper::contactRegistrationTypeDropDownList($model->reg_type);?></td>
                            </tr>
                            <tr>
                                <td>Media Source</td>
                                <td>
                                    <?php
                                        //echo CommonHelper::leadSourceByBrandId($model->reg_source);
                                    ?>
                                    <?php
                                    //echo CommonHelper::contactStatusTypeById($model->status);
                                    $urlLead = yii\helpers\Url::to(['contacts-registration/change-lead-source?id='.$model->id]);
                                    echo Html::dropDownList('status-dropdown', $model->reg_source, CommonHelper::leadSourceDropDownList(), [
                                        'class' => 'status-dropdown',
                                        'onchange' => "
                                                var statusid = $(this).val();
                                                console.log(statusid);
                                                $.ajax('$urlLead&status='+statusid+'', {
                                                    type: 'POST'
                                                }).done(function(data) {
                                                    //$.pjax.reload({container: '#pjax-container'});
                                                });
                                              
                                            ",
                                    ]);
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Registration Status</td>
                                <td>
                                     <?php
                                        //echo CommonHelper::contactStatusTypeById($model->status);
                                        $urlInterac = yii\helpers\Url::to(['contacts-registration/change-status?id='.$model->id]);
                                        echo Html::dropDownList('status-dropdown', $model->status, CommonHelper::contactStatusTypeDropDownList(), [
                                            'class' => 'status-dropdown',
                                            'onchange' => "
                                                var statusid = $(this).val();
                                                console.log(statusid);
                                                $.ajax('$urlInterac&status='+statusid+'', {
                                                    type: 'POST'
                                                }).done(function(data) {
                                                    //$.pjax.reload({container: '#pjax-container'});
                                                });
                                              
                                            ",
                                        ]);
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Current Result Notes</td>
                                <td><?php echo $model->current_result;?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="divider"></div>
                <div class="system-field">
                    <h4 style="text-align: left;text-transform: uppercase;margin-bottom: 5px; margin-top: 25px;">System Field</h4>
                    <table class="table table-striped table-bordered">
                        <tbody>
                            <tr>
                                <td>Registration Record ID</td>
                                <td><?php echo $contactName->first_name.' '.$contactName->last_name;?></td>
                            </tr>
                            <tr>
                                <td>Created Date</td>
                                <td>
                                    <?php
                                        echo CommonHelper::changeDateFormat($model->created_at, 'd M Y H g:i a');
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Created By</td>
                                <td>
                                    <?php
                                    $userList = CommonHelper::getUserById($model->created_by);
                                    if(!empty($model->created_by)){
                                        echo $userList->first_name.' '.$userList->last_name;
                                    }

                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Last Registration Update</td>
                                <td>
                                    <?php
                                        echo CommonHelper::changeDateFormat($model->updated_at, 'd M Y H g:i a');
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Last Registration Update By</td>
                                <td>
                                    <?php
                                    $userList = CommonHelper::getUserById($model->updated_by);
                                    if(!empty($model->updated_by)){
                                        echo $userList->first_name.' '.$userList->last_name;
                                    }
                                    ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <?php /*
                <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                	'id',
                    [
                        'attribute' => 'contact_id',
                        'value' => function($model) {
                            $contactName = CommonHelper::contactIdByContacts($model->contact_id);
                            return $contactName->first_name.' '.$contactName->last_name;
                        },
                    ],
                    [
                        'attribute' => 'development_id',
                        'value' => function($model) {
                            $developmentName = CommonHelper::developmentListById($model->development_id);
                            return $developmentName['development_name'];
                        },
                    ],
                    [
                        'attribute' => 'reg_type',
                        'value' => function($model) {
                            return CommonHelper::contactRegistrationTypeDropDownList($model->reg_type);
                        },
                    ],
                    [
                        'attribute' => 'reg_source',
                        'value' => function($model) {
                            return CommonHelper::leadSourceByBrandId($model->reg_source);
                        },
                    ],
					'reg_datetime:datetime',
					'status_date:datetime',
					'created_at:datetime',
					'created_by:user',
					'updated_at:datetime',
					'updated_by:user',
                    [
                        'attribute' => 'status',
                        'value' => function($model) {
                            return CommonHelper::contactStatusTypeById($model->status);
                        },
                    ],
                ],
                ]) ?>
                */ ?>

                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-left">
                        <?php if(!$model->isNewRecord){
                            ?>
                            <?= Html::button('Add Interaction', ['id' => 'add-contactInteraction-modelButton', 'value' => \yii\helpers\Url::to(['contacts-interactions/reg-quick-create?contactid='.$model->contact_id.'&regid='.$model->id.'']), 'class' => 'btn btn-success']) ?>
                            <?php
                        }?>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center">
                        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-right">
                        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php
$baseUrl = Yii::getAlias('@baseUrl');
$js = <<< JS
    $(function(){
        // Contact Interactions Create and Update
        $('#add-contactInteraction-modelButton').click(function(){
            $('#model-interaction').modal('show')
                .find('#add-contactInteraction-modelContent')
                .load($(this).attr('value'));
        });
         $('#contactsregistration-contact_id').change(function(){
            var contactid = $(this).val();
            $.get('contact-info', { contactid : contactid }, function(data) {
                var data = $.parseJSON(data);
                console.log(data);
                $('#contact-id').html(data.id);
                $('#contact-name').html(data.first_name+" "+data.last_name);
                $('#contact-phone').html(data.phone);
                $('#contact-email').html(data.email);
                
            });
    });
    });
   
    $(document).on("beforeSubmit", "#add-contact-form-1", function () {
        var form = $(this);
        var formData = form.serialize();
        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: formData,
            success: function (data) {
                var obj = jQuery.parseJSON( JSON.stringify(data) );
                console.log(obj);
                $('button.btn.btn-primary').text("Save");
            },
            error: function () {
                //$('button.btn.btn-primary').text("Save");
                //alert("Something went wrong");
                //$('button.btn.btn-primary').text("Save");
            },
            headers: {'Authorization': 'Basic bWFkaHNvbWUxMjM='},
            beforeSend: function (xhr) {
                $('button.btn.btn-primary').text("Sending....");
            },
        });
     }).on('submit', function(e){
        e.preventDefault();
     });
    
     
JS;
$this->registerJs($js);
?>

<?php

use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\buttons\ExportCsvButton;
use unlock\modules\core\buttons\ExportPdfButton;
use unlock\modules\core\buttons\ResetFilterButton;
use unlock\modules\core\buttons\SearchFilterButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $model unlock\modules\contacts\models\ContactsRegistrationSearch */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="admin-search-form-container contacts-registration-search">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
        'validateOnBlur' => false,
        'enableAjaxValidation' => false,
        'errorCssClass'=>'has-error',
        'fieldConfig' => [
            'template' => "{label}\n{input}\n",
            'options' => ['class' => 'col-sm-4'],
            'horizontalCssClasses' => [
                'label' => '',
                'offset' => '',
                'wrapper' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <div class="admin-search-form-fields">

        <div class="row">
        	<?= $form->field($model, 'id') ?>

			<?= $form->field($model, 'contact_id') ?>

			<?= $form->field($model, 'development_id') ?>

			<?php // echo $form->field($model, 'reg_source') ?>

			<?php // echo $form->field($model, 'reg_datetime') ?>

			<?php // echo $form->field($model, 'status') ?>

			<?php // echo $form->field($model, 'status_date') ?>

			<?php // echo $form->field($model, 'created_at') ?>

			<?php // echo $form->field($model, 'created_by') ?>

			<?php // echo $form->field($model, 'updated_at') ?>

			<?php // echo $form->field($model, 'updated_by') ?>

        </div>

        <div class="row">
            <div class="col-sm-6 filter-custom-search-btn">
                <?= SearchFilterButton::widget() ?>
                <?= ResetFilterButton::widget(['url' => Url::toRoute(['index'])]) ?>
            </div>
            <div class="col-sm-6 filter-custom-export-btn">
                <?= ExportCsvButton::widget(['url' => Url::toRoute(['export-csv'])]) ?>
                <?= ExportPdfButton::widget(['url' => Url::toRoute(['export-pdf'])]) ?>
            </div>
        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>

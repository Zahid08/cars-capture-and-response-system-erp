<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use unlock\modules\contacts\models\Contacts;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtons;
use unlock\modules\core\buttons\BackButton;
use kartik\datetime\DateTimePicker;
use yii\bootstrap\Modal;
use yii\jui\JuiAsset;
JuiAsset::register($this);
/* @var $this yii\web\View */
/* @var $model unlock\modules\contacts\models\ContactsRegistration */
/* @var $form yii\bootstrap\ActiveForm */

$this->title= ($model->isNewRecord) ? Yii::t('app', 'Create a New Registration') : Yii::t('app', 'Update Registration');
?>

<div class="main-container main-container-form" role="main">
    <div class="page-header">
     <!--   <div class="page-header-title">
            <h1 class="page-title"><?/*= Yii::t('app', 'Contacts Registration') */?></h1>
        </div>-->
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a(Yii::t('app', 'Home'), Yii::$app->homeUrl) ?></li>
                <li><?= Html::a(Yii::t('app', 'Contacts Registration'), Url::to('index')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>
    <?php
    Modal::begin([
        'header' => '<h4>Create a New Interaction</h4>',
        'id'     => 'model-interaction',
        'size'   => 'model-lg',
    ]);

    echo "<div id='add-contactInteraction-modelContent'></div>";

    Modal::end();
    ?>
    <div class="admin-form-container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fas fa-pencil-alt"></i> <?= Html::encode($this->title) ?></h3>
                </div>
                <div class="panel-heading-button">
                    <?= BackButton::widget() ?>
                </div>
            </div>
            <div class="panel-body">
                <?php if(!$model->isNewRecord){
                    $contactInfo = CommonHelper::contactIdByContacts($model->contact_id);
                    $fullName = $contactInfo->first_name.' '.$contactInfo->last_name;
                    ?>

                    <table style="width: 360px;margin: 0px auto 20px auto" class="table table-bordered">
                        <tr>
                            <td width="100px"><strong>Prospect ID:</strong></td>
                            <td><div id="contact-id"><?php echo $contactInfo->id; ?></div></td>
                        </tr>
                        <tr>
                            <td><strong>Full Name:</strong></td>
                            <td><div id="contact-name"><?php echo $fullName; ?></div></td>
                        </tr>
                        <tr>
                            <td><strong>Phone:</strong></td>
                            <td><div id="contact-email"><?php echo $contactInfo->phone;?></div></td>
                        </tr>
                        <tr>
                            <td><strong>Email:</strong></td>
                            <td><div id="contact-phone"><?php echo $contactInfo->email;?></div></td>
                        </tr>

                    </table>
                    <?php
                } ?>

                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
                    /*'options' => ['enctype' => 'multipart/form-data'],*/

                    'id' => 'add-contact-form-1',
                    'validateOnBlur' =>true,
                    'enableAjaxValidation'=>false,
                    'errorCssClass'=>'has-error',
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
                        'options' => ['class' => 'form-group'],
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-2',
                            'offset' => '',
                            'wrapper' => '',
                            'hint' => '',
                        ],
                    ],
                ]); ?>
                <?= $form->field($model, 'reg_type')->dropDownList(CommonHelper::contactRegistrationTypeDropDownList(), ['prompt'=>'Select type']) ?>
                <?php
                if ($model->isNewRecord){
                    echo $form->field($model, 'name', ['options' => ['class' => 'form-group form-group-search-auto-complete'],'inputTemplate' => '{input}<i class="fa fa-search"></i>'])->textInput(['class' => 'form-control field-contact-id field-search-auto-complete', 'placeholder' => 'Search','data-input-type' => 'select','data-hidden-field' => '#auto-complete-contact-id','data-ajax-url' => 'ajax/ajax/contact-name-list', 'maxlength' => true]);
                    echo '<input type="hidden" name="ContactsRegistration[contact_id]" id="auto-complete-contact-id">';
                }
                else{
                    $contact_id=$model->contact_id;
                    $contact_name=CommonHelper::contactIdByName($contact_id);
                    echo $form->field($model, 'name', ['options' => ['class' => 'form-group form-group-search-auto-complete'],'inputTemplate' => '{input}<i class="fa fa-search"></i>'])->textInput([ 'value'=>$contact_name, 'class' => 'form-control field-contact-id field-search-auto-complete', 'placeholder' => 'Search','data-input-type' => 'select','data-hidden-field' => '#auto-complete-contact-id','data-ajax-url' => 'ajax/ajax/contact-name-list', 'maxlength' => true]);
                    echo "<input type='hidden' name='ContactsRegistration[contact_id]' id='auto-complete-contact-id' value='$contact_id'>";
                }
                ?>
                <?=
                $form->field($model, 'development_id')->widget(Select2::classname(), [
                    'data' => CommonHelper::developmentDropdownList(),
                    'language' => 'de',
                    'options' => ['placeholder' => 'Select a Development ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

                <?= $form->field($model, 'reg_source')->dropDownList(CommonHelper::leadSourceDropDownList(), ['prompt'=>'Select source type']) ?>

                <?= $form->field($model, 'reg_datetime')->widget(
                    DateTimePicker::className(), [
                    'name' => 'reg_datetime',
                    'value' => '12-31-2010 05:10:20',
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd hh:ii:ss'
                    ]
                ]);?>

                <?= $form->field($model, 'status_date')->widget(
                    DateTimePicker::className(), [
                    'name' => 'status_date',
                    'value' => '12-31-2010 05:10:20',
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd hh:ii:ss'
                    ]
                ]);?>

                <?= $form->field($model, 'status')->dropDownList(CommonHelper::contactStatusTypeDropDownList()) ?>
                <?= $form->field($model, 'current_result')->textInput(['maxlength' => true]) ?>


                <div class="admin-form-button">
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <?= FormButtons::widget() ?>
                            <?php if(!$model->isNewRecord){
                                ?>
                                <?= Html::button('Add Interaction', ['id' => 'add-contactInteraction-modelButton', 'value' => \yii\helpers\Url::to(['contacts-interactions/reg-quick-create?contactid='.$model->contact_id.'&regid='.$model->id.'']), 'class' => 'btn btn-success','style'=>'margin-right: 5px!important;']) ?>
                                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                                    'class' => 'btn btn-danger',
                                    'data' => [
                                        'confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post',
                                    ],
                                ]) ?>
                                <?php
                            }?>
                        </div>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<?php
$baseUrl = Yii::getAlias('@baseUrl');
$js = <<< JS
    $(function(){
        // Contact Interactions Create and Update
        $('#add-contactInteraction-modelButton').click(function(){
            $('#model-interaction').modal('show')
                .find('#add-contactInteraction-modelContent')
                .load($(this).attr('value'));
        });
         $('#contactsregistration-contact_id').change(function(){
            var contactid = $(this).val();
            $.get('contact-info', { contactid : contactid }, function(data) {
                var data = $.parseJSON(data);
                console.log(data);
                $('#contact-id').html(data.id);
                $('#contact-name').html(data.first_name+" "+data.last_name);
                $('#contact-phone').html(data.phone);
                $('#contact-email').html(data.email);
                
            });
    });
    });
   
    $(document).on("beforeSubmit", "#add-contact-form-1", function () {
        var form = $(this);
        var formData = form.serialize();
        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: formData,
            success: function (data) {
                var obj = jQuery.parseJSON( JSON.stringify(data) );
                console.log(obj);
                $('button.btn.btn-primary').text("Save");
            },
            error: function () {
                //$('button.btn.btn-primary').text("Save");
                //alert("Something went wrong");
                //$('button.btn.btn-primary').text("Save");
            },
            headers: {'Authorization': 'Basic bWFkaHNvbWUxMjM='},
            beforeSend: function (xhr) {
                $('button.btn.btn-primary').text("Sending....");
            },
        });
     }).on('submit', function(e){
        e.preventDefault();
     });
    
     
JS;
$this->registerJs($js);
?>

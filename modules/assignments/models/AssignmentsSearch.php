<?php

namespace unlock\modules\assignments\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use unlock\modules\assignments\models\Assignments;

/**
 * AssignmentsSearch represents the model behind the search form of `unlock\modules\assignments\models\Assignments`.
 */
class AssignmentsSearch extends Assignments
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'development_id', 'listing_price', 'original_price', 'deposit_price', 'remaining_deposit', 'commission', 'service', 'psf', 'status', 'mail_status', 'created_by', 'updated_by'], 'safe'],
            [['remaining_due_date', 'occupancy', 'purchaser_name', 'expire_date', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Assignments::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'development_id' => $this->development_id,
            'listing_price' => $this->listing_price,
            'original_price' => $this->original_price,
            'deposit_price' => $this->deposit_price,
            'remaining_deposit' => $this->remaining_deposit,
            'remaining_due_date' => $this->remaining_due_date,
            'occupancy' => $this->occupancy,
            'commission' => $this->commission,
            'expire_date' => $this->expire_date,
            'service' => $this->service,
            'psf' => $this->psf,
            'status' => $this->status,
            'mail_status' => $this->mail_status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'purchaser_name', $this->purchaser_name])
           ->orderBy(['id'=>SORT_DESC]);
        ;

        return $dataProvider;
    }
}

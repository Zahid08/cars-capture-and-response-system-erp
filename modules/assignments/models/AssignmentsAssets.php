<?php

namespace unlock\modules\assignments\models;

use unlock\modules\core\helpers\CommonHelper;
use Yii;

/**
 * This is the model class for table "{{%assignments_assets}}".
 *
 * @property int $id
 * @property int $assignment_id
 * @property string $assignment_assets
 * @property string $upload_url
 * @property string $upload_file
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class AssignmentsAssets extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $option;
    public $sendemail;
    public $subject;
    public $message_body;

    public static function tableName()
    {
        return '{{%assignments_assets}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['assignment_id', 'created_by', 'updated_by'], 'safe'],
            [['option','assignment_assets'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['assignment_assets', 'upload_url', 'upload_file'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'assignment_id' => Yii::t('app', 'Assignment ID'),
            'assignment_assets' => Yii::t('app', 'Assets-1'),
            'upload_url' => Yii::t('app', 'Upload Url'),
            'upload_file' => Yii::t('app', 'Upload File'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'option' => Yii::t('app', 'Upload'),
            'sendemail' => Yii::t('app', 'Send Email'),
            'subject' => Yii::t('app', 'Subject'),
            'message_body' => Yii::t('app', 'Message Body'),
        ];
    }
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }
}

<?php

namespace unlock\modules\assignments\models;

use unlock\modules\core\helpers\CommonHelper;
use Yii;

/**
 * This is the model class for table "{{%assignments}}".
 *
 * @property int $id
 * @property int $development_id
 * @property int $unit_details_id
 * @property int $building_assets_id
 * @property int $assignment_assets_id
 * @property int $listing_price
 * @property int $original_price
 * @property int $deposit_price
 * @property int $remaining_deposit
 * @property string $remaining_due_date
 * @property string $occupancy
 * @property string $purchaser_name
 * @property int $commission
 * @property string $expire_date
 * @property int $service 1=MLS or 2=EXCLUSIVE
 * @property int $psf
 * @property int $status 1=New 2=Sold 3=Remove 4=Expired 5=Draft
 * @property int $mail_status 1=Checked 0=Non Checked
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class Assignments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $beds_id;
    public $address;
    public $sendemail;
    public $subject;
    public $message_body;
    public $suite_number;

    public static function tableName()
    {
        return '{{%assignments}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['development_id','listing_price', 'original_price', 'deposit_price', 'remaining_deposit', 'commission', 'service', 'psf', 'status', 'mail_status', 'created_by', 'updated_by'], 'safe'],
            [['remaining_due_date', 'occupancy', 'expire_date', 'created_at', 'updated_at'], 'safe'],
            [['purchaser_name'], 'string', 'max' => 100],
            [['development_id','listing_price','original_price','deposit_price','remaining_deposit','purchaser_name','commission'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'development_id' => Yii::t('app', 'Building Name'),
            'sendemail' => Yii::t('app', 'To : '),
            'subject' => Yii::t('app', 'Subject:'),
            'message_body' => Yii::t('app', 'Top Message:'),
            'address' => Yii::t('app', 'Address'),
            'beds_id' => Yii::t('app', 'Suite Type'),
            'suite_number' => Yii::t('app', 'Suite Number'),
            'assignment_assets_id' => Yii::t('app', 'Assignment Assets ID'),
            'listing_price' => Yii::t('app', 'Listing Price'),
            'original_price' => Yii::t('app', 'Original Price'),
            'deposit_price' => Yii::t('app', 'Deposit Price'),
            'remaining_deposit' => Yii::t('app', 'Remaining Deposit'),
            'remaining_due_date' => Yii::t('app', 'Remaining Due Date'),
            'occupancy' => Yii::t('app', 'Occupancy'),
            'purchaser_name' => Yii::t('app', 'Purchaser Name'),
            'commission' => Yii::t('app', 'Commission'),
            'expire_date' => Yii::t('app', 'Expire Date'),
            'service' => Yii::t('app', 'MLS/Exclusive'),
            'psf' => Yii::t('app', 'PSF'),
            'status' => Yii::t('app', 'Status'),
            'mail_status' => Yii::t('app', 'Mail Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
                $this->status =5;
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }
}

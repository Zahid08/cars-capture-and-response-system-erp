<?php

namespace unlock\modules\assignments\models;

use unlock\modules\core\helpers\CommonHelper;
use Yii;

/**
 * This is the model class for table "{{%assignments_units}}".
 *
 * @property int $id
 * @property int $assignment_id
 * @property int $suite_number
 * @property int $beds_id 1 BED / 1BED+DEN /2BED / 2BED + DEN/ 3BED/ 3BED + DEN/ STUDIO/ 4 BED/ 4BED + DEN
 * @property int $baths
 * @property int $balcony 1=Yes / 0=No
 * @property int $balcony_size
 * @property int $parking 1=Yes / 0=No
 * @property int $parking_total
 * @property int $locker 1=Yes / 0=No
 * @property string $sqft
 * @property string $exposure
 * @property string $notes
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class AssignmentsUnits extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%assignments_units}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['assignment_id', 'suite_number', 'beds_id', 'baths', 'balcony', 'balcony_size', 'parking', 'parking_total', 'locker', 'created_by', 'updated_by'], 'safe'],
            [['created_at', 'updated_at'], 'safe'],
            [['sqft', 'exposure'], 'safe'],
            [['notes'], 'safe'],
            [['beds_id','baths','sqft'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'assignment_id' => Yii::t('app', 'Assignment ID'),
            'suite_number' => Yii::t('app', 'Suite Number'),
            'beds_id' => Yii::t('app', 'Beds'),
            'baths' => Yii::t('app', 'Baths'),
            'balcony' => Yii::t('app', 'Balcony'),
            'balcony_size' => Yii::t('app', 'Balcony Size'),
            'parking' => Yii::t('app', 'Parking'),
            'parking_total' => Yii::t('app', 'Parking Total'),
            'locker' => Yii::t('app', 'Locker'),
            'sqft' => Yii::t('app', 'Sqft'),
            'exposure' => Yii::t('app', 'Exposure'),
            'notes' => Yii::t('app', 'Notes'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }
}

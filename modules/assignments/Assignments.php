<?php

namespace unlock\modules\assignments;

/**
 * assignments module definition class
 */
class Assignments extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'unlock\modules\assignments\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}

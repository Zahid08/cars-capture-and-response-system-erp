<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use unlock\modules\core\widgets\DetailView;
use unlock\modules\core\buttons\BackButton;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtons;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use unlock\modules\generalsettings\models\City;
use unlock\modules\usermanager\user\models\User;

$this->title = Yii::t('app', 'View Assignments');
/*$this->params['breadcrumbs'][] = ['label' => 'Assignments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/

?>

<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
    'options' => ['enctype' => 'multipart/form-data'],
    'id' => 'add-contact-form-3',
    'validateOnBlur' =>true,
    'enableAjaxValidation'=>false,
    'errorCssClass'=>'has-error',
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-sm-8\">\n{input}\n{hint}\n{error}\n</div>",
        'options' => ['class' => 'form-group'],
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => '',
            'wrapper' => '',
            'hint' => '',
        ],
    ],
]); ?>


<h3 class="assignments-create-title">Add New Units Assets</h3>

<?= $form->field($assignmentAssets, 'assignment_assets')->dropDownList(CommonHelper::assignmentAssetsDropDownList(), ['prompt' => 'Select assignment assets'])->label('Asset') ?>

<?= $form->field($assignmentAssets, 'assignment_id')->hiddenInput(['value' => $assignment_id])->label(false) ?>

<?php
if($assignmentAssets->upload_url){
    $assignmentAssets->option = 'Url';
}else{
    $assignmentAssets->option = 'File';
}
?>
<?= $form->field($assignmentAssets, 'option')->radioList([ 'Url' => 'Url', 'File' => 'File', ], ['prompt' => '']) ?>

<?php
echo $form->field($assignmentAssets, 'upload_url')->textInput(['maxlength' => true]);
echo $form->field($assignmentAssets, 'upload_file')->fileInput(['maxlength' => true]);
?>


<div class="admin-form-button">
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-4">
            <?= FormButtons::widget() ?>
        </div>
    </div>
</div>


<?php ActiveForm::end(); ?>

<?php
$js = <<< JS
    $(function(){
    
        $('input[name="AssignmentsAssets[option]"]:radio').change(function () {
            var radioValue = $("input[name='AssignmentsAssets[option]']:checked").val();
            if(radioValue){
                if(radioValue == 'Url'){
                    $('.field-assignmentsassets-upload_url').show();
                    $('.field-assignmentsassets-upload_file').hide();
                }else{
                    $('.field-assignmentsassets-upload_url').hide();
                    $('.field-assignmentsassets-upload_file').show();
                }
            }
        });
        
        $(document).ready(function(){
        
            var radioValue = $("input[name='AssignmentsAssets[option]']:checked").val();
            if(radioValue){
                if(radioValue == 'Url'){
                    $('.field-assignmentsassets-upload_url').show();
                    $('.field-assignmentsassets-upload_file').hide();
                }else{
                    $('.field-assignmentsassets-upload_url').hide();
                    $('.field-assignmentsassets-upload_file').show();
                }
            }
            
         });
            
    });
JS;
$this->registerJs($js);
?>




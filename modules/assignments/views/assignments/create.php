<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model unlock\modules\assignments\models\Assignments */
?>
<div class="assignments-create">

    <?= $this->render('_form', [
        'model' => $model,
        'assignmentUnits' => $assignmentUnits,
        'assignmentAssets' => $assignmentAssets,
        'assignmentBuildingAssets' => $assignmentBuildingAssets,
    ]) ?>

</div>

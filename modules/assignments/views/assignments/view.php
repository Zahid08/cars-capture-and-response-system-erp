<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use unlock\modules\core\widgets\DetailView;
use unlock\modules\core\buttons\BackButton;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtons;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use unlock\modules\generalsettings\models\City;
use unlock\modules\usermanager\user\models\User;

$this->title = Yii::t('app', 'View Assignments');
$this->params['breadcrumbs'][] = ['label' => 'Assignments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-container main-container-view" role="main">
    <div class="panel panel-primary">
        <div class="panel-heading"><i class="fa fa-eye"></i> Assignment Details - <?php echo $model->id; ?></div>
        <div class="panel-body">
            <div class="assignment-details">
                <div class="row">
                    <div class="col-md-5 col-12">
                        <h3 style="font-size: 21px;">
                            <?php
                            $developmentList = CommonHelper::developmentListById($model->development_id);
                            echo $developmentList->development_name;
                            ?>
                        </h3>
                    </div>
                    <div class="col-md-2 col-12">
                        <h3 style="text-align: center;font-size: 21px;"><span>ID: </span>
                            <?php
                           echo  $model->id;
                            ?>
                        </h3>
                    </div>
                    <div class="col-md-3 col-12 pull-right">
                        <button type="button" id="send-assignment-email-btn" value="/ERP/backend/web/assignments/assignments/" class="btn btn-success" style="float: right;margin-top: 15px;">Email</button>
                    </div>
                </div>
            </div>
            <div class="divider"></div>

            <div id="app" class="tab_section">
                <tabs>
                    <?php $form = ActiveForm::begin([
                        'options' => ['enctype' => 'multipart/form-data'],
                        'id' => 'update-form',
                        'action' => Url::to(['assignments/update', 'id' => $model->id]),
                        'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
                        'validateOnBlur' =>false,
                        'enableAjaxValidation'=>false,
                        'errorCssClass'=>'has-error',
                        'fieldConfig' => [
                            'template' => "{label}\n<div class=\"col-sm-8\">\n{input}\n{hint}\n{error}\n</div>",
                            'options' => ['class' => 'form-group'],
                            'horizontalCssClasses' => [
                                'label' => 'col-sm-4',
                                'offset' => '',
                                'wrapper' => '',
                                'hint' => '',
                            ],
                        ],
                    ]); ?>

                    <tab name="Profile" :selected="true">
                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'address')->textInput(['maxlength' => true,'readonly'=>true,'value'=>''.$development_address.'']) ?>

                                <?= $form->field($model, 'listing_price')->textInput(['maxlength' => true]) ?>

                                <?= $form->field($model, 'original_price')->textInput(['maxlength' => true]) ?>

                                <?= $form->field($model, 'deposit_price')->textInput(['maxlength' => true]) ?>

                                <?= $form->field($model, 'remaining_deposit')->textInput(['maxlength' => true]) ?>
                                <?= $form->field($model, 'remaining_due_date')->widget(
                                    DatePicker::className(), [
                                    'name' => 'remaining_due_date',
                                    'value' => '12-31-2010',
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'format' => 'yyyy-mm-dd'
                                    ]
                                ]);?>
                                <?= $form->field($model, 'occupancy')->widget(
                                    DatePicker::className(), [
                                    'name' => 'occupancy',
                                    'value' => '12-31-2010',
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'format' => 'yyyy-mm'
                                    ]
                                ]);?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'commission')->textInput(['maxlength' => true]) ?>
                                <?= $form->field($model, 'purchaser_name')->textInput(['maxlength' => true]) ?>
                                <?= $form->field($model, 'expire_date')->widget(
                                    DatePicker::className(), [
                                    'name' => 'expire_date',
                                    'value' => '12-31-2010',
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'format' => 'yyyy-mm-dd'
                                    ]
                                ]);?>
                                <?= $form->field($model, 'service')->dropDownList(CommonHelper::assingmentServiceDropDownList(), ['prompt' => 'Select']) ?>
                                <?= $form->field($model, 'psf')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                    </tab>

                    <tab name="Unit Details">
                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($assignmentUnits, 'suite_number')->textInput(['maxlength' => true]) ?>
                                <?php
                                echo $form->field($assignmentUnits, 'beds_id')->widget(Select2::classname(), [
                                    'data' => CommonHelper::getActiveSuiteType(),
                                    'language' => 'en',
                                    'options' => ['placeholder' => 'Select Type'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]);
                                ?>
                                <?= $form->field($assignmentUnits, 'baths')->dropDownList(CommonHelper::parkingTotalDropDownList()) ?>
                                <?= $form->field($assignmentUnits, 'balcony')->checkboxList([ '1' => 'do you have a balcony ?']) ?>
                                <?= $form->field($assignmentUnits, 'balcony_size')->textInput(['maxlength' => true,'readonly'=>true]) ?>
                                <?= $form->field($assignmentUnits, 'notes')->textarea(['rows' => '5']) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($assignmentUnits, 'parking')->checkboxList([ '1' => 'do you have a parking spot ?']) ?>
                                <?= $form->field($assignmentUnits, 'parking_total')->dropDownList(CommonHelper::parkingTotalDropDownList(), ['readonly'=>true]) ?>
                                <?= $form->field($assignmentUnits, 'locker')->checkboxList([ '1' => 'is there a locker?']) ?>
                                <?= $form->field($assignmentUnits, 'sqft')->textInput(['maxlength' => true]) ?>
                                <?= $form->field($assignmentUnits, 'exposure')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                    </tab>
                    <?php ActiveForm::end(); ?>
                    <tab name="Media Tools">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th style="width: 25%;">Media</th>
                                <th style="width: 25%;">Status</th>
                                <th style="width: 25%;">Date</th>
                                <th style="width: 25%;">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($mediaList as $mediaListItem) {?>
                                    <tr>
                                        <td style="text-align: center;"><?php echo $mediaListItem->lead_source_name;?></td>
                                        <td style="text-align: center;">
                                            <span class="label label-danger" style="background-color: red; color: #fff;font-size: 12px">NO</span>
                                          <!--  <span class="label label-success" style="color: #fff;font-size: 10px"> Yes</span>-->
                                        </td>
                                        <td style="text-align: center;" ></td>
                                        <td style="text-align: center;">
                                            <button type="submit" class="btn btn-success" style="margin-right: 10px;">Post</button>
                                            <button type="submit" class="btn btn-dark" style="">Retract</button>
                                        </td>
                                    </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </tab>
                    <tab name="Assets">
                        <div class="row">
                            <div class="col-md-12">

                                <?php
                                Modal::begin([
                                    'header' => '<h4>Edit Assets</h4>',
                                    'id'     => 'model-interaction',
                                    'size'   => 'model-lg',
                                ]);

                                echo "<div id='edit-assignmentsAssets-modelContent'></div>";

                                Modal::end();
                                ?>

                                <?php
                                Modal::begin([
                                    'header' => '<h4>Add More Assets</h4>',
                                    'id'     => 'addmore-assignmentsAssets-model-interaction',
                                    'size'   => 'model-lg',
                                ]);

                                echo "<div id='addmore-assignmentsAssets-modelContent'></div>";

                                Modal::end();
                                ?>

                                <h4>Units Assets</h4>
                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%;">ID</th>
                                            <th style="width: 20%;">Category</th>
                                            <th style="width: 20%;">Name</th>
                                            <th style="width: 35%;">File</th>
                                            <th style="width: 20%;"></th>
                                        </tr>
                                    </thead>
                                    <?php
                                    $assignments_assets    =   array();
                                    $assignments_assets    =   CommonHelper::getAssignmentsAssetsByAssignmentID($model->id);

                                    if($assignments_assets) {
                                        foreach ($assignments_assets as $assignments) {

                                            $upload_file_url = '';
                                            if ($assignments->upload_file) {
                                                $upload_file_url = $assignments->upload_file;
                                            } else if ($assignments->upload_url) {
                                                $upload_file_url = $assignments->upload_url;
                                            } else {
                                                $upload_file_url = 'N/A';
                                            }

                                            $assetDeleteUrl = yii\helpers\Url::to(['/assignments/assignments/units-assets-delete?id=' . $assignments->id]);

                                            $assignmentName = CommonHelper::getAssignmentsById($assignments->assignment_id);
                                            $developmentName = CommonHelper::developmentListById($assignmentName->development_id);
                                            ?>
                                            <tr>
                                                <td> <?= $assignments->id ?> </td>
                                                <td> <?= $developmentName->development_name ?> </td>
                                                <td> <?= $assignments->assignment_assets ?> </td>
                                                <td><a href="<?= $upload_file_url ?>"
                                                       target="_blank"><?= substr($upload_file_url, 0, 40) ?>....</a>
                                                </td>
                                                <td>
                                                    <?= Html::a(Yii::t('yii', '<span class="glyphicon glyphicon-trash"></span> Remove'), $assetDeleteUrl, [
                                                        'class' => 'btn btn-danger small-button-cls',
                                                        'title' => Yii::t('yii', 'Remove'),
                                                        'style' => Yii::t('yii', 'padding:3px 8px;'),
                                                        'aria-label' => Yii::t('yii', 'Remove'),
                                                        'onclick' => "
                                                                if (confirm('Are you sure you want to delete this item?')) {
                                                                    return true;
                                                                }
                                                                return false;
                                                            ",
                                                    ]);
                                                    ?>
                                                    <?= Html::button('<span class="glyphicon glyphicon-edit"></span> Edit', ['id' => 'edit-assignmentsAssets-modelButton' . $assignments->id . '', 'value' => \yii\helpers\Url::to(['/assignments/assignments/units-assets-update?id=' . $assignments->id . '']), 'class' => 'btn btn-primary small-button-cls']) ?>
                                                </td>
                                            </tr>

                                            <script>
                                                $(function () {
                                                    //load edit-assignmentsAssets-modelButton modal
                                                    $('#edit-assignmentsAssets-modelButton<?= $assignments->id ?>').click(function () {
                                                        $('#model-interaction').modal('show')
                                                            .find('#edit-assignmentsAssets-modelContent')
                                                            .load($(this).attr('value'));
                                                    });
                                                });
                                            </script>

                                            <?php
                                        }
                                    }
                                    //echo '<pre>'; print_r($assignments_assets); echo '</pre>';
                                    ?>

                                </table>

                                <p style="text-align: right">
                                    <?= Html::button('<span class="glyphicon glyphicon-plus"></span> Add New One', ['id' => 'addmore-assignmentsAssets-modelButton', 'value' => \yii\helpers\Url::to(['/assignments/assignments/units-assets-addmore?id='.$model->id.'']), 'class' => 'btn btn-primary small-button-cls']) ?>
                                </p>

                                <script>
                                    $(function(){
                                        //load edit-assignmentsAssets-modelButton modal
                                        $('#addmore-assignmentsAssets-modelButton').click(function(){
                                            $('#addmore-assignmentsAssets-model-interaction').modal('show')
                                                .find('#addmore-assignmentsAssets-modelContent')
                                                .load($(this).attr('value'));
                                        });
                                    });
                                </script>

                                <h4>Building Assets</h4>
                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th style="width: 5%;">ID</th>
                                        <th style="width: 20%;">Category</th>
                                        <th style="width: 20%;">Name</th>
                                        <th style="width: 35%;">File</th>
                                        <th style="width: 20%;"></th>
                                    </tr>
                                    </thead>
                                    <?php
                                    $building_assets    =   array();
                                    $building_assets    =   CommonHelper::getBuildingAssetsByAssignmentID($model->id);
                                    if($building_assets){
                                        foreach ($building_assets as $building){

                                            $upload_file_url   = '';
                                            if($building->upload_file){
                                                $upload_file_url   = $building->upload_file;
                                            }else if($building->upload_url){
                                                $upload_file_url   = $building->upload_url;
                                            }else{
                                                $upload_file_url   = 'N/A';
                                            }

                                            $assetDeleteUrl     = yii\helpers\Url::to(['/assignments/assignments/building-assets-delete?id='.$building->id]);

                                            $assignmentName     = CommonHelper::getAssignmentsById($building->assignment_id);
                                            $developmentName    = CommonHelper::developmentListById($assignmentName->development_id);
                                            ?>
                                            <tr>
                                                <td> <?= $building->id ?> </td>
                                                <td> <?= $developmentName->development_name ?> </td>
                                                <td> <?= $building->building_assets ?> </td>
                                                <td> <a href="<?= $upload_file_url ?>" target="_blank"><?= substr($upload_file_url, 0, 40) ?>....</a> </td>
                                                <td>
                                                    <?= Html::a(Yii::t('yii', '<span class="glyphicon glyphicon-trash"></span> Remove'), $assetDeleteUrl, [
                                                        'class' => 'btn btn-danger small-button-cls',
                                                        'title' => Yii::t('yii', 'Remove'),
                                                        'style' => Yii::t('yii', 'padding:3px 8px;'),
                                                        'aria-label' => Yii::t('yii', 'Remove'),
                                                        'onclick' => "
                                                                    if (confirm('Are you sure you want to delete this item?')) {
                                                                        return true;
                                                                    }
                                                                    return false;
                                                                ",
                                                    ]);
                                                    ?>
                                                    <?= Html::button('<span class="glyphicon glyphicon-edit"></span> Edit', ['id' => 'edit-buildingAssets-modelButton'.$building->id.'', 'value' => \yii\helpers\Url::to(['/assignments/assignments/building-assets-update?id='.$building->id.'']), 'class' => 'btn btn-primary small-button-cls']) ?>
                                                </td>
                                            </tr>

                                            <script>
                                                $(function(){
                                                    //load edit-buildingAssets-modelButton modal
                                                    $('#edit-buildingAssets-modelButton<?= $building->id ?>').click(function(){
                                                        $('#model-interaction').modal('show')
                                                            .find('#edit-assignmentsAssets-modelContent')
                                                            .load($(this).attr('value'));
                                                    });
                                                });
                                            </script>

                                            <?php
                                        }
                                    }
                                    //echo '<pre>'; print_r($assignments_assets); echo '</pre>';
                                    ?>

                                </table>

                                <p style="text-align: right">
                                    <?= Html::button('<span class="glyphicon glyphicon-plus"></span> Add New One', ['id' => 'addmore-buildingAssets-modelButton', 'value' => \yii\helpers\Url::to(['/assignments/assignments/building-assets-addmore?id='.$model->id.'']), 'class' => 'btn btn-primary small-button-cls']) ?>
                                </p>

                                <script>
                                    $(function(){
                                        //load edit-assignmentsAssets-modelButton modal
                                        $('#addmore-buildingAssets-modelButton').click(function(){
                                            $('#addmore-assignmentsAssets-model-interaction').modal('show')
                                                .find('#addmore-assignmentsAssets-modelContent')
                                                .load($(this).attr('value'));
                                        });
                                    });
                                </script>


                                <?php
                                $floor_plane=CommonHelper::getAssignmentsAssetsByName($model->id,'floor_plans');
                                //echo '<pre>'; print_r($floor_plane); exit();

                                $floor_plane_url='';
                                if (!empty($floor_plane->upload_url)){
                                    $floor_plane_url=$floor_plane->upload_url;
                                }
                                elseif (!empty($floor_plane->upload_file)){
                                    $floor_plane_url=$floor_plane->upload_file;
                                }
                                ?>
                               <!-- <label for="assignmentsassets-upload_url" class="control-label col-sm-4">Floor Plane</label>
                                <div class="col-md-8" style="margin-bottom: 15px;">
                                <input type="text" readonly class="form-control" value="<?php /*echo $floor_plane_url;*/?>">
                                </div>-->
                                <?php
                                $Renderings=CommonHelper::getBuildingAssetsByName($model->id,'renderings');
                                $brochures=CommonHelper::getBuildingAssetsByName($model->id,'brochures');
                                $investor_incentives=CommonHelper::getBuildingAssetsByName($model->id,'investor_incentives');
                                $videos=CommonHelper::getBuildingAssetsByName($model->id,'videos');
                                $features_finishes=CommonHelper::getBuildingAssetsByName($model->id,'features_finishes');

                                $rendering_url=$brochures_url=$investor_incentives_url=$videos_url=$features_finishes_url='';
                                if (!empty($Renderings->upload_url)){
                                    $rendering_url=$Renderings->upload_url;
                                }
                                elseif (!empty($Renderings->upload_file)){
                                    $rendering_url=$Renderings->upload_file;
                                }
                                if (!empty($brochures->upload_url)){
                                    $brochures_url=$brochures->upload_url;
                                }
                                elseif (!empty($brochures->upload_file)){
                                    $brochures_url=$brochures->upload_file;
                                }

                                if (!empty($investor_incentives->upload_url)){
                                    $investor_incentives_url=$investor_incentives->upload_url;
                                }
                                elseif (!empty($investor_incentives->upload_file)){
                                    $investor_incentives_url=$investor_incentives->upload_file;
                                }

                                if (!empty($videos->upload_url)){
                                    $videos_url=$videos->upload_url;
                                }
                                elseif (!empty($videos->upload_file)){
                                    $videos_url=$videos->upload_file;
                                }

                                if (!empty($features_finishes->upload_url)){
                                    $features_finishes_url=$features_finishes->upload_url;
                                }
                                elseif (!empty($features_finishes->upload_file)){
                                    $features_finishes_url=$features_finishes->upload_file;
                                }
                                ?>

                                <!--<label for="assignmentsassets-upload_url" class="control-label col-sm-4">Renderings</label>
                                <div class="col-md-8" style="margin-bottom: 15px;">
                                    <input type="text" readonly class="form-control" value="<?php
/*                                        echo $rendering_url;
                                    */?>">
                                </div>

                                <label for="assignmentsassets-upload_url" class="control-label col-sm-4">Brochures</label>
                                <div class="col-md-8" style="margin-bottom: 15px;">
                                    <input type="text" readonly class="form-control" value="<?php
/*                                        echo $brochures_url;
                                    */?>">
                                </div>

                                <label for="assignmentsassets-upload_url" class="control-label col-sm-4">Investor Incentives</label>
                                <div class="col-md-8" style="margin-bottom: 15px;">
                                    <input type="text" readonly class="form-control" value="<?php
/*                                        echo $investor_incentives_url;*/?>">
                                </div>

                                <label for="assignmentsassets-upload_url" class="control-label col-sm-4">Videos</label>
                                <div class="col-md-8" style="margin-bottom: 15px;">
                                    <input type="text" readonly class="form-control" value="<?php
/*                                        echo $videos_url;
                                    */?>">
                                </div>

                                <label for="assignmentsassets-upload_url" class="control-label col-sm-4">Features and Finishes</label>
                                <div class="col-md-8" style="margin-bottom: 15px;">
                                    <input type="text" readonly class="form-control" value="<?php
/*                                    echo $features_finishes_url;
                                    */?>">
                                </div>-->

                            </div>
                        </div>
                    </tab>
                </tabs>
            </div>
            <div class="divider"></div>

            <div class="modal bd-example-modal-lg" id="email" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="myLargeModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog modal-xl modal-dialog-scrollable modal-dialog-centered" style="min-width: 770px;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4>Assignments Email !</h4>
                        </div>

                        <div class="modal-body">

                            <div class="row mail-content-div">
                                <?php $form = ActiveForm::begin(
                                    [
                                        'id' => 'email-send-form',
                                        'action' => Url::to(['assignments/assignment-single-email']),
                                        'layout' => 'horizontal',
                                        'fieldConfig' => [
                                            'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
                                            'horizontalCssClasses' => [
                                                'label' => 'col-sm-2',
                                                'offset' => '',
                                                'wrapper' => '',
                                                'hint' => '',
                                            ],
                                        ],

                                    ]
                                ); ?>
                                <div class="col-md-12">
                                    <?= $form->field($assignments, 'sendemail')->widget(Select2::classname(),
                                        [ 'data' => User::getActiveUser(),
                                            'options' =>
                                                ['placeholder' => 'Select email...', 'multiple' => true],
                                            'pluginOptions' =>
                                                [ 'tags' => false,
                                                    'allowClear' => true,
                                                    'maximumInputLength' => 10
                                                ],
                                        ]);
                                    ?>
                                    <?= $form->field($assignments, 'subject')->textInput(['maxlength' => true]) ?>

                                    <?= $form->field($assignments, 'message_body')->widget(\yii\redactor\widgets\Redactor::className()) ?>
                                </div>
                                <?php ActiveForm::end(); ?>
                                <div class="col-md-12 email-send-btn">
                                    <button type="submit" name="action" value="save" class="btn btn-primary send-email" style="float: right;margin: 10px 0px 10px 5px;"><svg class="svg-inline--fa fa-check fa-w-16" aria-hidden="true" data-prefix="fa" data-icon="check" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"></path></svg><!-- <i class="fa fa-check"></i> --> Send</button>
                                </div>
                                <div class="divider"></div>
                                <div id="main" style="max-height: 500px;overflow-y: scroll;">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="button_group">
                <div class="row">
                    <div class="pull-right update_btn form_action_btn_2">
                        <?= FormButtons::widget(['item' => ['save', 'cancel']]) ?>
                    </div>
                </div>
                <div class="update_btn">
                    <div class="row">
                        <?php //FormButtons::widget(['item' => ['save', 'cancel']]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$baseUrl = Yii::getAlias('@baseUrl');
$modelId=$model->id;
$js = <<< JS
  $(function(){
        
        //load edit-assignmentsAssets-modelButton modal
        $('#edit-assignmentsAssets-modelButton').click(function(){
            $('#model-interaction').modal('show')
                .find('#edit-assignmentsAssets-modelContent')
                .load($(this).attr('value'));
        });
        
        $('.update_btn button').click(function(){
            var form = $("#update-form");
            var formData = form.serialize();
            console.log(formData);
            $.ajax({
 
                url: form.attr("action"),
        
                type: form.attr("method"),
        
                data: formData,
            
                success: function (data) {
                    var obj = jQuery.parseJSON( JSON.stringify(data) );
                    console.log(obj);
                    $('button.btn.btn-primary').text("Save");
                },
                error: function () {
                    //$('button.btn.btn-primary').text("Save");
                    //alert("Something went wrong");
                    //$('button.btn.btn-primary').text("Save");
                },
                headers: {'Authorization': 'Basic bWFkaHNvbWUxMjM='},
                beforeSend: function (xhr) {
                    $('button.btn.btn-primary').text("Sending....");
                },
        
            });
        });
        
        //Email Send Modal Show
         $('#send-assignment-email-btn').click(function() {
                 $('#email').modal('show');
                 $('#email-close-button').click(function () {
                        $('#email').modal('hide');
                        $("#nav li a").removeClass('current');
                 });
                 var main_url='$baseUrl/backend/web/assignments/assignments/emai-template-load';
                 var message_body=$(".redactor-editor").html();
                 var user_id=$modelId;
                 var list = [];
                 list.push(user_id);
                 $('#main').html('<div> <center><img style="height: 85px;" src="https://connectassetmgmt.connectcondos.com/backend/web/themes/connecterp/images/loader.gif"></center> </div>');
                $.ajax({
                    type: 'post',
                    url:main_url,
                    data: {selection: list},
                      complete: function(data) {
                         $('#main').html(data.responseText);
                         /*$("#assignments-message_body").keyup(function() {
                              $(".message-top-content").text(this.value ? this.value : "Video title");
                        });*/
                        $(".redactor-editor").keyup(function() {
                        $(".message-top-content").html($(".redactor-editor").html());
                     });
                     }               
                });
         });
         
         $('.email-send-btn').click(function() {
             var email=$('#assignmentsassets-sendemail').val();
            var subject=$('#assignmentsassets-subject').val();
             var message_body=$(".redactor-editor").html();
             if (email=='' || subject==''){
                $('ul.select2-selection__rendered').css('border', '1px solid red');
                $('#assignmentsassets-subject').css('border', '1px solid red');
                if (email!=''){
                   $('ul.select2-selection__rendered').css('border', '1px solid green');
                }
                if (subject!=''){
                    $('#assignmentsassets-subject').css('border', '1px solid green');
                }
                return false;
             }
            else {
                $('ul.select2-selection__rendered').css('border', '1px solid green');
                $('#assignmentsassets-subject').css('border', '1px solid green');
                
                var form=$('#email-send-form');
                var form_url=form.attr('action');
                var user_id=$modelId;
                var main_url=form_url+'?email_id='+email+'&subject='+subject+'&message_body='+message_body+'&id='+user_id+'';
               
                var list = [];
                list.push(user_id);
                
                $.ajax({
                    type: 'post',
                    url:main_url,
                    data: {selection: list},
                     beforeSend: function (xhr) {
                    $('.email-send-btn button').text("Sending....");
                    },
                 });
            }
         });
         
         
         //Validation Form   
        //Listing Price
      $("#assignments-listing_price").on("input", function(evt) {
            var self = $(this);
            self.val(self.val().replace(/[^\d].+/, ""));
            if ((evt.which < 48 || evt.which > 57))
            {
                evt.preventDefault();
            }
        });
        $("#assignments-listing_price").change("input", function(evt) {
            var self = $(this);
            var str=Number(self.val()).toString();
            self.val('$'+str.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        }); 
        
        //Original Price
      $("#assignments-original_price").on("input", function(evt) {
            var self = $(this);
            self.val(self.val().replace(/[^\d].+/, ""));
            if ((evt.which < 48 || evt.which > 57))
            {
                evt.preventDefault();
            }
        });
        $("#assignments-original_price").change("input", function(evt) {
            var self = $(this);
            var str=Number(self.val()).toString();
            self.val('$'+str.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        });   
        
         //Deposite Price
        $("#assignments-deposit_price").on("input", function(evt) {
            var self = $(this);
            self.val(self.val().replace(/[^\d].+/, ""));
            if ((evt.which < 48 || evt.which > 57))
            {
                evt.preventDefault();
            }
        });
      
        $("#assignments-deposit_price").change("input", function(evt) {
            var self = $(this);
            var str=Number(self.val()).toString();
            self.val('$'+str.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        });
        
        //Remaining Deposite Price
      $("#assignments-remaining_deposit").on("input", function(evt) {
            var self = $(this);
            self.val(self.val().replace(/[^\d].+/, ""));
            if ((evt.which < 48 || evt.which > 57))
            {
                evt.preventDefault();
            }
        });
      
        $("#assignments-remaining_deposit").change("input", function(evt) {
            var self = $(this);
            var str=Number(self.val()).toString();
            self.val('$'+str.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        });
        
        //Commission Percentage
        $('#assignments-commission').change(function() {
            $(this).val(function(index, old) { return old.match(/^\d+\.?\d{0,1}/, '') + '%'; });
        });

        $("#assignments-commission").on("input", function(evt) {
           this.value = this.value.match(/^\d+\.?\d{0,1}/);
        });
       
        
        if ($('#assignmentsunits-balcony input[type=checkbox]').is(':checked')) {
            $('#assignmentsunits-balcony_size').removeAttr('readonly');
        }
        
        $("#assignmentsunits-parking_total").prop('disabled', true);
        
       if ($('#assignmentsunits-parking input[type=checkbox]').is(':checked')) {
           $('#assignmentsunits-parking_total').removeAttr('readonly');
           $("#assignmentsunits-parking_total").prop('disabled', false);
       }
       
        $('#assignmentsunits-balcony input[type=checkbox]').click(function() {
             if ($(this).is(':checked')) {
                 $('#assignmentsunits-balcony_size').removeAttr('readonly');
             }else {
                 $('#assignmentsunits-balcony_size').attr('readonly', 'readonly');
             }
        });
        
        $('#assignmentsunits-parking input[type=checkbox]').click(function() {
            if ($(this).is(':checked')) {
                 $("#assignmentsunits-parking_total").prop('disabled', false);
                 $('#assignmentsunits-parking_total').removeAttr('readonly');
             }else {
                  $("#assignmentsunits-parking_total").prop('disabled', true);
                 $('#assignmentsunits-parking_total').attr('readonly', 'readonly');
             }
        });
        
        //validate belcony
        $('#assignmentsunits-balcony_size').keyup(function(e){
              if (/\D/g.test(this.value))
              {
                this.value = this.value.replace(/\D/g, '');
              }
        });
        
         //validate Parking total
        $('#assignmentsunits-parking_total').keyup(function(e){
              if (/\D/g.test(this.value))
              {
                this.value = this.value.replace(/\D/g, '');
              }
        });
        //validate baths
        $('#assignmentsunits-baths').keyup(function(e){
              if (/\D/g.test(this.value))
              {
                this.value = this.value.replace(/\D/g, '');
              }
        });
        
      /*  //PSF Price
          $(".assignments-psf").change("input", function(evt) {
            var self = $(this);
            var str=Number(self.val()).toString();
            self.val('$'+str.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        });*/
          
       $("#assignments-psf").on("input", function(evt) {
            var self = $(this);
            self.val(self.val().replace(/[^\d].+/, ""));
            if ((evt.which < 48 || evt.which > 57))
            {
                evt.preventDefault();
            }
        });
        $("#assignments-psf").change("input", function(evt) {
             var self = $(this);
             var str=Number(self.val()).toString();
             self.val('$'+str.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        });
        
  });


JS;
$this->registerJs($js);
?>

<style>
    .small-button-cls{padding:5px 10px !important;}
</style>

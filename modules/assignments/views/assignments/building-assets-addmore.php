<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use unlock\modules\core\widgets\DetailView;
use unlock\modules\core\buttons\BackButton;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtons;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use unlock\modules\generalsettings\models\City;
use unlock\modules\usermanager\user\models\User;

$this->title = Yii::t('app', 'View Assignments');
/*$this->params['breadcrumbs'][] = ['label' => 'Assignments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/

?>

<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
    'options' => ['enctype' => 'multipart/form-data'],
    'id' => 'add-contact-form-3',
    'validateOnBlur' =>true,
    'enableAjaxValidation'=>false,
    'errorCssClass'=>'has-error',
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-sm-8\">\n{input}\n{hint}\n{error}\n</div>",
        'options' => ['class' => 'form-group'],
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => '',
            'wrapper' => '',
            'hint' => '',
        ],
    ],
]); ?>


<h3 class="assignments-create-title">Add New Building Assets</h3>

<?= $form->field($buildingAssets, 'building_assets')->dropDownList(CommonHelper::buildingAssetsDropDownList(), ['prompt' => 'Select assignment assets'])->label('Asset') ?>
<?php

    if($buildingAssets->upload_url){
        $buildingAssets->option = 'Url';
    }else{
        $buildingAssets->option = 'File';
    }

?>
<?= $form->field($buildingAssets, 'assignment_id')->hiddenInput(['value' => $assignment_id])->label(false) ?>
<?= $form->field($buildingAssets, 'option')->radioList([ 'Url' => 'Url', 'File' => 'File', ], ['prompt' => '']) ?>
<?= $form->field($buildingAssets, 'upload_url')->textInput(['maxlength' => true]) ?>
<?= $form->field($buildingAssets, 'upload_file')->fileInput(['maxlength' => true]) ?>

<?php

$upload_file_url   = '';
if($buildingAssets->upload_file){
    $upload_file_url   = $buildingAssets->upload_file;
}else if($buildingAssets->upload_url){
    $upload_file_url   = $buildingAssets->upload_url;
}

if($upload_file_url){ ?>

    <div class="control-label col-sm-4">
        &nbsp;
    </div>
    <div class="col-sm-8">
        <a href="<?= $upload_file_url ?>" target="_blank"><?= substr($upload_file_url, 0, 40) ?>....</a>
    </div>
    <hr>

<?php } ?>

<div class="admin-form-button">
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-4">
            <?= FormButtons::widget() ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>


<?php
$js = <<< JS
    $(function(){
        
        $('input[name="AssignmentsBuildingAssets[option]"]:radio').change(function () {
            var radioValue = $("input[name='AssignmentsBuildingAssets[option]']:checked").val();
            if(radioValue){
                if(radioValue == 'Url'){
                    $('.field-assignmentsbuildingassets-upload_url').show();
                    $('.field-assignmentsbuildingassets-upload_file').hide();
                }else{
                    $('.field-assignmentsbuildingassets-upload_url').hide();
                    $('.field-assignmentsbuildingassets-upload_file').show();
                }
            }
        });
        
        $(document).ready(function(){
        
            var radioValue = $("input[name='AssignmentsBuildingAssets[option]']:checked").val();
            if(radioValue){
                if(radioValue == 'Url'){
                    $('.field-assignmentsbuildingassets-upload_url').show();
                    $('.field-assignmentsbuildingassets-upload_file').hide();
                }else{
                    $('.field-assignmentsbuildingassets-upload_url').hide();
                    $('.field-assignmentsbuildingassets-upload_file').show();
                }
            }
            
         });
        
    });
JS;
$this->registerJs($js);
?>




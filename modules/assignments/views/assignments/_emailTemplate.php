<?php
use \unlock\modules\core\helpers\CommonHelper;
use unlock\modules\assignments\models\AssignmentsUnits;
use unlock\modules\assignments\models\AssignmentsAssets;
$messsage_top_content=   !empty($model['message_body']) ? $model['message_body'] : '';

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="format-detection" content="telephone=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Assignments email</title>
    <style type="text/css">
        body {
            -webkit-text-size-adjust: 100% !important;
            -ms-text-size-adjust: 100% !important;
            -webkit-font-smoothing: antialiased !important;
        }
        img {
            border: 0 !important;
            outline: none !important;
        }
        p {
            Margin: 0px !important;
            padding: 0px !important;
        }
        table {
            border-collapse: collapse;
            mso-table-lspace: 0px;
            mso-table-rspace: 0px;
        }
        td {
            border-collapse: collapse;
            mso-line-height-rule: exactly;
        }
        a {
            border-collapse: collapse;
            mso-line-height-rule: exactly;
        }
        span {
            border-collapse: collapse;
            mso-line-height-rule: exactly;
        }
        .ExternalClass * {
            line-height: 100%;
        }
        span.MsoHyperlink {
            mso-style-priority: 99;
            color: inherit;
        }
        span.MsoHyperlinkFollowed {
            mso-style-priority: 99;
            color: inherit;
        }
        .em_defaultlink a {
            color: inherit !important;
            text-decoration: none !important;
        }
        .em_white a {
            color: #ffffff;
            text-decoration: none;
        }
        .em_blue1 a {
            color: #2c3e50;
            text-decoration: none;
        }
        .em_blue2 a {
            color: #4777bb;
            text-decoration: none;
        }
        .em_blue3 a {
            color: #2f1d4f;
            text-decoration: none;
        }
        .em_pink a {
            color: #eb47c7;
            text-decoration: none;
        }
        .em_grey a {
            color: #808080;
            text-decoration: none;
        }
        .em_lightblue a {
            color: #4777bb;
            text-decoration: none;
        }
        .em_pink1 a {
            color: #f16a5f;
            text-decoration: none;
        }
        .em_grey1 a {
            color: #666666;
            text-decoration: none;
        }
        .em_pink2 a {
            color: #d436b1;
            text-decoration: none;
        }

        @media only screen and (max-width:480px) {
            .em_wrapper {
                width: 100% !important;
            }

            .em_main_table {
                width: 100% !important;
            }

            .em_center {
                text-align: center !important;
            }

            .em_spc_20 {
                height: 20px !important;
            }

            .em_gap_bottom {
                padding-bottom: 20px !important;
            }

            .em_font1 {
                font-size: 18px !important;
                line-height: 20px !important;
            }
        }
        @media only screen and (min-width:481px) and (max-width:619px) {
            .em_wrapper {
                width: 100% !important;
            }
            .em_main_table {
                width: 100% !important;
            }
            .em_center {
                text-align: center !important;
            }
            .em_spc_20 {
                height: 20px !important;
            }
            .em_gap_bottom {
                padding-bottom: 20px !important;
            }

            .em_font1 {
                font-size: 20px !important;
                line-height: 22px !important;
            }
        }
    </style>
    <!--[if gte mso 9]><xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <style type="text/css">
        body, table, td, span {
            font-family: Arial, sans-serif !important;
        }
    </style>
    <![endif]-->
</head>
<body style="margin:0px; padding:0px;" bgcolor="#ffffff">

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff">
    <tr>
        <td valign="top" align="center">
            <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" style="table-layout:fixed;" class="em_main_table">
                <tr><!--Main header-->
                    <td align="center" valign="top">
                <tr>
                    <td height="20" class="em_spc_20">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" valign="top" class="em_gap_bottom"><a href="http://www.connectassetmanagement.com/" target="_blank" style="text-decoration:none;" class="inf-track-46582"><img src="http://www.connectassetmanagement.com/newsletter/common/logo.jpg" width="258" height="70" alt="Connect asset management Plan | Invest | Retire" style="display:block; font-family:Arial, sans-serif; font-size:18px; line-height:23px; color:#000000;" border="0" /></a></td>
                </tr>
                <tr>
                    <td height="20" class="em_spc_20">&nbsp;</td>
                </tr>
                </td>
                </tr>

                <tr><!--Content-->
                    <td align="center" valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                            <tr>
                                <td valign="top">
                                    <table width="620" border="0" cellspacing="0" cellpadding="0" align="left" class="em_wrapper">
                                        <tr>
                                            <td height="15" class="em_spc_20">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="em_center message-top-content" align="left" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:15px; line-height:24px; color:#000;">
                                                <?php  if (!empty($messsage_top_content)){
                                                    echo $messsage_top_content;
                                                    ?>
                                                <?php }else{?>
                                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised.
                                                <?php }?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="35" class="em_spc_20">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                                <!--[if gte mso 9]>
                                </td>
                                <td valign="top" >
                                <![endif]-->
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>
        </td>
    </tr>

    <tr>  <!--Development List block-->
        <td class="em_center1" align="justify" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:20px; line-height:26px; color:#4677BA;"><!--excerpt-->
            <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" style="table-layout:fixed;" class="em_main_table">
                <tr>
                    <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="10">&nbsp;</td>
                                <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr>
                                            <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                    <tr>
                                                        <td valign="top"><!--Development Root-->
                                                            <?php foreach ($model['selection'] as $item) {
                                                                $assignment_id      =   $item;
                                                                $model              =   CommonHelper::getAssignmentsById($assignment_id);

                                                                //development
                                                                $development_id     =   $model->development_id;
                                                                $developmentData    =   CommonHelper::developmentListById($development_id);
                                                                $development_name   =   $developmentData->development_name;
                                                                $development_image  =   $developmentData->image_url;
                                                                $development_url    =   !empty($developmentData->development_url) ? $developmentData->development_url : '';

                                                                //assignment units
                                                                $assignmentUnits    =   AssignmentsUnits::find()->where(['assignment_id' => $model->id])->one();
                                                                $assignmentsAssets  =   AssignmentsAssets::find()->where(['assignment_id' => $model->id])->all();
                                                                $bedNameModel       =   CommonHelper::getBedName($assignmentUnits->beds_id);

                                                                //Data wrapper
                                                                $sqft               =  !empty($assignmentUnits->sqft) ? $assignmentUnits->sqft : '-';
                                                                $listing_price      =  !empty($model->listing_price) ? $model->listing_price : '-';
                                                                $original_price     =  !empty($model->original_price) ? $model->original_price : '-';
                                                                $deposit_price      =  !empty($model->deposit_price) ? $model->deposit_price : '-';
                                                                $occupancy          =  !empty($model->occupancy) ? $model->occupancy : '-';
                                                                $bed                =  !empty($bedNameModel->suite_type_name) ? $bedNameModel->suite_type_name : '-';
                                                                $baths              =  !empty($assignmentUnits->baths) ? $assignmentUnits->baths : '-';
                                                                $psf                =  !empty($assignmentUnits->psf) ? $assignmentUnits->psf : '-';
                                                                $balcony_size       =  !empty($assignmentUnits->balcony_size) ? $assignmentUnits->balcony_size : '-';
                                                                $parking_total      =  !empty($assignmentUnits->parking_total) ? $assignmentUnits->parking_total : '-';
                                                                $exposure           =  !empty($assignmentUnits->parking_total) ? $assignmentUnits->exposure : '-';
                                                                $parking            =  '';
                                                                $locker             =  '';

                                                                $assignmentsAssetsHTML   = '';
                                                                $increement              = 1;
                                                                foreach ($assignmentsAssets as $assignmentsAsset){

                                                                    if($assignmentsAsset->assignment_assets == 'floor_plans'){

                                                                        $upload_file_url        = '';
                                                                        if($assignmentsAsset->upload_file){
                                                                            $upload_file_url    = $assignmentsAsset->upload_file;
                                                                        }else if($assignmentsAsset->upload_url){
                                                                            $upload_file_url    = $assignmentsAsset->upload_url;
                                                                        }

                                                                        $assignmentsAssetsHTML .= '<tr><td align="left" style="border-top: solid 1px #ccc;padding:4px;font-family: \'Open Sans\', Arial, sans-serif; font-size:14px; line-height:22px; color:#2c3e50;"><a href="'.$development_url.'" target="_blank" style="text-decoration:underline; color:#4777bb;" class="inf-track-46604">'.substr($upload_file_url, 0, 70).'....</a></td></tr>';
                                                                        $assignmentsAssetsHTML .= '<tr><td height="15" style="line-height:1px; font-size:1px;">&nbsp;</td></tr>';

                                                                        $increement++;

                                                                    }
                                                                }

                                                                if ($assignmentUnits->parking==0){
                                                                    $parking='No';
                                                                }
                                                                else{
                                                                    $parking='Yes';
                                                                }

                                                                if ($assignmentUnits->parking==0){
                                                                    $locker='No';
                                                                }
                                                                else{
                                                                    $locker='Yes';
                                                                }
                                                                ?>
                                                                <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="em_wrapper"><!--Development #1-->
                                                                    <tr><!--Title Name #1-->
                                                                        <td valign="top">
                                                                            <table width="620" border="0" cellspacing="0" cellpadding="0" align="left" class="em_wrapper">
                                                                                <tr>
                                                                                    <td class="em_font1" align="left" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:24px; line-height:26px; color:#2c3e50; font-weight:bold;"><span class="em_blue1"><?php echo  $development_name;?></span></td>
                                                                                </tr>
                                                                            </table>
                                                                            <!--[if gte mso 9]>
                                                                            </td>
                                                                            <td valign="top" >
                                                                            <![endif]-->
                                                                        </td>
                                                                    </tr>

                                                                    <tr><!--Development Image-->
                                                                        <td valign="top">
                                                                            <table width="300" border="0" cellspacing="0" cellpadding="0" align="left" class="em_wrapper">
                                                                                <tr>
                                                                                    <td height="15" style="line-height:1px; font-size:1px;">&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="center" valign="top"><a href="<?php echo  $development_url;?>" target="_blank" style="text-decoration:underline; color:#4777bb;" class="inf-track-46604"><img src="<?php echo  $development_image;?>" width="300" height="220" alt="How a pair of young parents turned their attic into a grown up retreat" style="display:block;" border="0" /></a></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td height="10" style="line-height:1px; font-size:1px;">&nbsp;</td>
                                                                                </tr>
                                                                            </table>

                                                                            <!--[if gte mso 9]>
                                                                            </td>
                                                                            <td valign="top" >
                                                                            <![endif]-->

                                                                            <table width="290" border="0" cellspacing="0" cellpadding="0" align="right" class="em_wrapper">
                                                                                <tr>
                                                                                    <td height="4" style="line-height:1px; font-size:1px;">&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td height="10" style="line-height:1px; font-size:1px;">&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <table width="290" border="1" cellspacing="0" cellpadding="0" align="right" class="em_wrapper">
                                                                                        <tr>
                                                                                            <td width="150px" class="em_font1" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:18px; line-height:34px; color:#2c3e50; font-weight:bold;">Address</td>
                                                                                            <td width="150px" class="em_font1" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:18px; line-height:34px; color:#2c3e50; font-weight:bold;">Data</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td width="150px" class="em_font1" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:14px; line-height:30px; color:#2c3e50; font-weight:bold;">SQFT</td>
                                                                                            <td width="150px" class="em_font1" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:14px; line-height:30px; color:#2c3e50;font-weight:bold;"><?php echo  $sqft;?></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td width="150px" class="em_font1" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:14px; line-height:30px; color:#2c3e50; font-weight:bold;">Listing Price</td>
                                                                                            <td width="150px" class="em_font1" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:14px; line-height:30px; color:#2c3e50;font-weight:bold;"><?php echo  $listing_price;?></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td width="150px" class="em_font1" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:14px; line-height:30px; color:#2c3e50; font-weight:bold;">Beds</td>
                                                                                            <td width="150px" class="em_font1" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:14px; line-height:30px; color:#2c3e50;font-weight:bold;"><?php echo  $bed;?></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td width="150px" class="em_font1" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:14px; line-height:30px; color:#2c3e50; font-weight:bold;">Baths</td>
                                                                                            <td width="150px" class="em_font1" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:14px; line-height:30px; color:#2c3e50;font-weight:bold;"><?php echo  $baths;?></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td width="150px" class="em_font1" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:14px; line-height:30px; color:#2c3e50; font-weight:bold;">Parking</td>
                                                                                            <td width="150px" class="em_font1" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:14px; line-height:30px; color:#2c3e50;font-weight:bold;"><?php echo  $parking;?></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td width="150px" class="em_font1" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:14px; line-height:30px; color:#2c3e50; font-weight:bold;">Locker</td>
                                                                                            <td width="150px" class="em_font1" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:14px; line-height:30px; color:#2c3e50;font-weight:bold;"><?php echo  $locker;?></td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td height="10" style="line-height:1px; font-size:1px;">&nbsp;</td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>

                                                                <table class="em_main_table" style="table-layout:fixed;" width="620" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                    <tr>
                                                                        <td height="15" style="line-height:1px; font-size:1px;">&nbsp;</td>
                                                                    </tr>
                                                                    <?php echo $assignmentsAssetsHTML; ?>
                                                                    <tr>
                                                                        <td height="10" style="line-height:1px; font-size:1px;">&nbsp;</td>
                                                                    </tr>
                                                                </table>

                                                                <!--[if gte mso 9]>
                                                                </td>
                                                                <td valign="top" >
                                                                <![endif]-->
                                                            <?php }?>
                                                        </td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                                <td width="10">&nbsp;</td>
                            </tr>
                        </table></td>
                </tr>
            </table></td>
    </tr>
</table>
</body>
</html>
<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use unlock\modules\core\grid\GridView;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\buttons\NewButton;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\usermanager\user\models\User;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use unlock\modules\core\buttons\FormButtons;
use yii\redactor\widgets\Redactor;

/* @var $this yii\web\View */
/* @var $searchModel unlock\modules\assignments\models\AssignmentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Assignments');
?>
<div class="main-container" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-grid-filter">
      <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <div class="admin-grid-content">
        <div class="admin-grid-toolbar">
            <div class="admin-grid-toolbar-left">
                <?= NewButton::widget(['url' => Url::toRoute(['create'])]) ?>
            </div>
            <div class="admin-grid-toolbar-right">
                <div class="col-md-12" id="summary">
                    <?= Summary::widget(['dataProvider' => $dataProvider]) ?>
                </div>
                <div class="email-btn-wrapper" id="email-wrapper">
                </div>
            </div>
        </div>
        <div class="admin-grid-view">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
                'pager' => ['class' => LinkPager::className()],
                'columns' => [
                    [
                        'attribute' => 'id',
                        'format'    => 'raw',
                        'value'    => function($model){
                            $id = Html::a($model->id, ['view', 'id' => $model->id]);
                            return $id;
                        }
                    ],
                    [
                        'attribute' => 'development_id',
                        'format'    => 'raw',
                        'value'    => function($model){
                            $developmentList = CommonHelper::developmentListById($model->development_id);
                            //$building_name = Html::a($developmentList->development_name, ['view', 'id' => $model->id]);
                            $building_name = $developmentList->development_name;
                            return $building_name;
                        }
                    ],
                    [
                        'attribute' => 'development_id',
                        'label'=>'Address',
                        'format'    => 'raw',
                        'value'    => function($model){
                            $developmentList = CommonHelper::developmentListById($model->development_id);
                            //$development_address = Html::a($developmentList->development_address, ['view', 'id' => $model->id]);
                            $development_address = $developmentList->development_address;
                            return $development_address;
                        }
                    ],

                    [
                        'attribute' => 'suite_number',
                        'headerOptions' => ['style' => 'color:#337ab7;width: 119px;'],
                        'format'    => 'raw',
                        'value'    => function($model){
                            $assignmentUnitsModel = CommonHelper::getAssignmentUnitsByAssignmentsId($model->id);
                            $suite_type_number='';
                            if (!empty($assignmentUnitsModel)){
                                $suite_type_number=$assignmentUnitsModel->suite_number;
                            }
                            return $suite_type_number;
                        }
                    ],
                    [
                        'attribute' => 'beds_id',
                        'headerOptions' => ['style' => 'color:#337ab7;'],
                        'format'    => 'raw',
                        'value'    => function($model){
                            $SuiteTypeModel = CommonHelper::getSuiteTypeByAssignmentsId($model->id);
                            $suite_type_name='';
                             if (!empty($SuiteTypeModel)){
                                $suite_type_name=$SuiteTypeModel->suite_type_name;
                             }
                             return $suite_type_name;
                        }
                    ],
                    [
                        'attribute'  => 'status',
                        'format'    => 'raw',
                        'headerOptions' => ['style' => 'width:90px;'],
                        'value' => function($model){
                            $urlInterac = yii\helpers\Url::to(['assignments/change-status?id='.$model->id]);
                            return Html::dropDownList('status-dropdown', $model->status, CommonHelper::assignmentStatusDropDownList(), [
                                'class' => 'status-dropdown',
                                'onchange' => "
                                            var statusid = $(this).val();
                                            console.log(statusid);
                                            $.ajax('$urlInterac&status='+statusid+'', {
                                                type: 'POST'
                                            }).done(function(data) {
                                                //$.pjax.reload({container: '#pjax-container'});
                                            });
                                    ",
                            ]);
                            ?>
                            <?php
                        }
                    ],
                    [
                        'attribute' => 'listing_price',
                        'format'    => 'raw',
                        'headerOptions' => ['style' => 'width:115px;'],
                        'value'    => function($model){
                            return $model->listing_price;
                        }
                    ],
                    [
                        'attribute' => 'occupancy',
                        'format'    => 'raw',
                        'headerOptions' => ['style' => 'width:115px;'],
                        'value' => function($model){
                            $date=$model->occupancy;
                            $occupancyDate =date('F',strtotime($date)).'  '.date('Y',strtotime($date));
                            return  $occupancyDate;

                        }
                    ],
                    [
                        'class' => 'yii\grid\CheckboxColumn',
                        'header' => Html::checkBox('selection_all', false, [
                            'class' => '',
                            'label' => '',
                        ]),
                        'checkboxOptions' => function($model) {
                           $mail_status = ["value" => $model->id, "class" => 'mail_status', "label" => '', "checked" => false];
                            return $mail_status;
                        },
                    ],

                ],
            ]); ?>
        </div>

        <div class="modal bd-example-modal-lg" id="email" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="myLargeModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-xl modal-dialog-scrollable modal-dialog-centered" style="min-width: 770px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4>Assignments Email !</h4>
                    </div>

                    <div class="modal-body">

                        <div class="row mail-content-div">
                            <?php $form = ActiveForm::begin(
                                [
                                    'id' => 'email-send-form',
                                    'action' => Url::to(['assignments/assignment-multiple-email']),
                                    'layout' => 'horizontal',
                                    'fieldConfig' => [
                                        'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
                                        'horizontalCssClasses' => [
                                            'label' => 'col-sm-2',
                                            'offset' => '',
                                            'wrapper' => '',
                                            'hint' => '',
                                        ],
                                    ],

                                ]
                            ); ?>
                            <div class="col-md-12">
                                <?= $form->field($assignments, 'sendemail')->widget(Select2::classname(),
                                    [ 'data' => User::getActiveUser(),
                                        'options' =>
                                            ['placeholder' => 'Select email...', 'multiple' => true],
                                            'pluginOptions' =>
                                            [ 'tags' => false,
                                                'allowClear' => true,
                                                'maximumInputLength' => 10
                                            ],
                                    ]);
                                ?>
                                <?= $form->field($assignments, 'subject')->textInput(['maxlength' => true]) ?>
                                <?= $form->field($assignments, 'message_body')->widget(\yii\redactor\widgets\Redactor::className()) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                            <div class="col-md-12 email-send-btn">
                                <button type="submit" name="action" value="save" class="btn btn-primary send-email" style="float: right;margin: 10px 0px 10px 5px;"><svg class="svg-inline--fa fa-check fa-w-16" aria-hidden="true" data-prefix="fa" data-icon="check" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"></path></svg><!-- <i class="fa fa-check"></i> --> Send</button>
                            </div>
                            <div class="divider"></div>
                            <div id="main" style="max-height: 500px;overflow-y: scroll;">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<?php
$baseUrl = Yii::getAlias('@baseUrl');
$js = <<< JS
$(function(){
     var select_all_checkbox=$('input[name="selection_all"');
     var single_select_checkbox=$('input[name="selection[]"');
     var row = $(".email-btn-wrapper");
     
    $('.mail_status ,input[name="selection_all"] ').click(function() {
        if (select_all_checkbox.filter(':checked') || single_select_checkbox.filter(':checked') ) {
            $("#summary").attr('class', 'col-md-8'); //change class
            $("#email-wrapper").attr('class', 'col-md-4');//change class
             var cols=''; //new colum add 
             cols += '<button type="button" id="send-assignment-email-btn" value="$baseUrl/backend/web/assignments/assignments/" class="btn btn-success">Email</button>';
             row.html(cols);
             
             //Check uncheck condition
             
              if (select_all_checkbox.filter(':checked').length>0){
                  modal_show();
             }
            else if (select_all_checkbox.filter(':checked').length==0){
                   if (single_select_checkbox.filter(':checked').length>0){
                        if ($(this).is(':checked')) {
                          modal_show();
                        }
                   }
            }
            
            /*if (select_all_checkbox.filter(':checked').length>0){
                 if ($(this).is(':checked')) {
                     modal_show();
                 }
             }
             $('input[name="selection[]"]:checked').each(function(){
                    modal_show();
                });*/
            
           /*if (single_select_checkbox.filter(':checked').length>0){
                if ($(this).is(':checked')) {
                  modal_show();
                }
           }*/
        }
    });
    
    //Email Send Modal Show
    function modal_show(){
         $('#send-assignment-email-btn').click(function() {
                 $('#email').modal('show');
                 $('#email-close-button').click(function () {
                        $('#email').modal('hide');
                        $("#nav li a").removeClass('current');
                 });
                 var main_url='$baseUrl/backend/web/assignments/assignments/email-multiple-template-load';
                 var message_body=$(".redactor-editor").html();
                 var user_id;
                 var list = [];
                $('input[name="selection[]"]:checked').each(function(){
                    user_id = this.value;
                     list.push(user_id); 
                });
                $('#main').html('<div> <center><img style="height: 85px;" src="https://connectassetmgmt.connectcondos.com/backend/web/themes/connecterp/images/loader.gif"></center> </div>');
                
                $.ajax({
                    type: 'post',
                    url:main_url,
                    data: {selection: list},
                      complete: function(data) {
                        $('#main').html(data.responseText);
                        /* $(".redactor-editor").keyup(function() {
                              $(".message-top-content").html(message_body ? message_body : "Video title");
                        });*/
                         $(".redactor-editor").keyup(function() {
                            $(".message-top-content").html($(".redactor-editor").html());
                         });
                     }               
                });
         });
    }
    
   // Url:assignments/quick-create-email?email_id=55
    $('.email-send-btn button').click(function(){
          var email=$('#assignments-sendemail').val();
          var subject=$('#assignments-subject').val();
          var message_body=$(".redactor-editor").html();
            if (email=='' || subject==''){
                $('ul.select2-selection__rendered').css('border', '1px solid red');
                $('#assignments-subject').css('border', '1px solid red');
                if (email!=''){
                   $('ul.select2-selection__rendered').css('border', '1px solid green');
                }
                if (subject!=''){
                    $('#assignments-subject').css('border', '1px solid green');
                }
                return false;
             }
            else {
                $('ul.select2-selection__rendered').css('border', '1px solid green');
                $('#assignments-subject').css('border', '1px solid green');
                
                var form=$('#email-send-form');
                var form_url=form.attr('action');
                var main_url=form_url+'?email_id='+email+'&subject='+subject+'&message_body='+message_body+'';
                var user_id; 
                var list = [];
                $('input[name="selection[]"]:checked').each(function(){
                    user_id = this.value;
                     list.push(user_id); 
                });
                $.ajax({
                    type: 'post',
                    url:main_url,
                    data: {selection: list},
                     beforeSend: function (xhr) {
                    $('button.btn.btn-primary.send-email').text("Sending....");
                    },
                });
            }
      });
    
});
JS;
$this->registerJs($js);
?>

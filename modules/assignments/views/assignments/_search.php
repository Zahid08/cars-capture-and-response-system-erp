<?php

use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\buttons\ExportCsvButton;
use unlock\modules\core\buttons\ExportPdfButton;
use unlock\modules\core\buttons\ResetFilterButton;
use unlock\modules\core\buttons\SearchFilterButton;
use unlock\modules\core\helpers\CommonHelper;
use kartik\select2\Select2;
use unlock\modules\setting\models\SuiteType;

/* @var $this yii\web\View */
/* @var $model unlock\modules\assignments\models\AssignmentsSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="admin-search-form-container assignment-search">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
        'validateOnBlur' => false,
        'enableAjaxValidation' => false,
        'errorCssClass'=>'has-error',
        'fieldConfig' => [
            'template' => "{label}\n{input}\n",
            'options' => ['class' => 'col-sm-4'],
            'horizontalCssClasses' => [
                'label' => '',
                'offset' => '',
                'wrapper' => '',
                'hint' => '',
            ],
        ],
    ]); ?>
    <div class="admin-search-form-fields">
        <div class="row">
            <div class="col-sm-12 col-md-2 col-lg-2">
                <?php
                echo $form->field($model, 'development_id')->widget(Select2::classname(), [
                    'data' => CommonHelper::developmentDropdownList(),
                    'language' => 'en',
                    'options' => ['placeholder' => 'Select Building'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="col-sm-12 col-md-2 col-lg-2">
                <?= $form->field($model, 'status')->dropDownList(CommonHelper::assignmentStatusDropDownList(), ['prompt' => 'Select Status']) ?>
            </div>
            <div class="col-sm-12 col-md-2 col-lg-2">
                <?php
                echo $form->field($model, 'listing_price')->widget(Select2::classname(), [
                    'data' => CommonHelper::getListingPriceDropdownlist(),
                    'language' => 'en',
                    'options' => ['placeholder' => 'Select Listing'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="col-sm-12 col-md-2 col-lg-2">
                <?php
                echo $form->field($model, 'beds_id')->widget(Select2::classname(), [
                    'data' =>CommonHelper::getActiveSuiteType(),
                    'language' => 'en',
                    'options' => ['placeholder' => 'Suite Type'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="col-sm-12 col-md-2 col-lg-2">
                <?php
                echo $form->field($model, 'occupancy')->widget(Select2::classname(), [
                    'data' => CommonHelper::getOccupancyDropdownlist(),
                    'language' => 'en',
                    'options' => ['placeholder' => 'Occupancy'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>

            <div class="col-sm-12 col-md-2 col-lg-2">
                <div class="col-md-6" style="padding: 0;">
                    <div style="margin: 25px 0 0 0px;" class="filter-custom-search-btn">
                        <?= SearchFilterButton::widget() ?>
                    </div>
                </div>
                <div class="col-md-6" style="padding: 0;">
                    <div style="margin: 25px 0 0 0px;" class="filter-custom-search-btn">
                        <a style="padding: 6px 10px;" class="btn btn-default" href="<?php echo Url::toRoute(['index']);?>"> Reset</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php /*
    <div class="admin-search-form-fields">

        <div class="row">
            <?php // $form->field($model, 'id') ?>

            <?php // $form->field($model, 'development_id') ?>

            <?php //  $form->field($model, 'developer_id') ?>

			<?php // echo $form->field($model, 'development_name') ?>

			<?php // echo $form->field($model, 'sales_status') ?>

			<?php // echo $form->field($model, 'grade') ?>

			<?php // echo $form->field($model, 'created_at') ?>

			<?php // echo $form->field($model, 'created_by') ?>

			<?php // echo $form->field($model, 'updated_at') ?>

			<?php // echo $form->field($model, 'updated_by') ?>

        </div>
 */ ?>
    <?php /*
        <div class="row">
            <div class="col-sm-6 filter-custom-search-btn">
                <?= SearchFilterButton::widget() ?>
                <?= ResetFilterButton::widget(['url' => Url::toRoute(['index'])]) ?>
            </div>
            <div class="col-sm-6 filter-custom-export-btn">
                <?= ExportCsvButton::widget(['url' => Url::toRoute(['export-csv'])]) ?>
                <?= ExportPdfButton::widget(['url' => Url::toRoute(['export-pdf'])]) ?>
            </div>
        </div>
*/ ?>
</div>

<?php ActiveForm::end(); ?>

</div>
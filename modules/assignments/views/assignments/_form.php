<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtons;
use unlock\modules\core\buttons\BackButton;
/* @var $this yii\web\View */
/* @var $model unlock\modules\assignments\models\Assignments */
/* @var $form yii\widgets\ActiveForm */
$this->title= ($model->isNewRecord) ? Yii::t('app', 'Create Assignments') : Yii::t('app', 'Update Assignments');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Assignments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="main-container main-container-form assignments-form" role="main">
    <div class="admin-form-container assignmentst-panel">
        <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="panel-heading-title">
                        <h3 class="panel-title"><i class="fas fa-pencil-alt"></i> <?= Html::encode($this->title) ?></h3>
                    </div>
                    <div class="panel-heading-button">
                        <?= BackButton::widget() ?>
                    </div>
                </div>
                <div class="panel-body">
                    <?php $form = ActiveForm::begin([
                        'options' => ['enctype' => 'multipart/form-data'],
                        'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
                        'validateOnBlur' =>true,
                        'enableAjaxValidation'=>false,
                        'errorCssClass'=>'has-error',
                        'fieldConfig' => [
                            'template' => "{label}\n<div class=\"col-sm-8\">\n{input}\n{hint}\n{error}\n</div>",
                            'options' => ['class' => 'form-group'],
                            'horizontalCssClasses' => [
                                'label' => 'col-sm-4',
                                'offset' => '',
                                'wrapper' => '',
                                'hint' => '',
                            ],
                        ],
                    ]); ?>

                        <div class="col-md-12">
                            <h3 class="assignments-create-title">General Information</h3>
                            <div class="row">
                                <div class="col-md-6">
                                <?php

                                $dev_id         = '';
                                if( isset($_GET['dev_id']) ){
                                    $dev_id     = $_GET['dev_id'];
                                }

                                echo $form->field($model, 'development_id')->widget(Select2::classname(), [
                                    'data' => CommonHelper::developmentDropdownList(),
                                    'language'  => 'en',
                                    'options'   => ['placeholder' => 'Select Building Name', 'value' => $dev_id],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]);
                                ?>

                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($model, 'status')->dropDownList(CommonHelper::assignmentStatusDropDownList(), ['prompt' => 'Select Status']) ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                <?= $form->field($model, 'listing_price')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-md-6">
                                <?= $form->field($model, 'original_price')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                <?= $form->field($model, 'deposit_price')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-md-6">
                                <?= $form->field($model, 'remaining_deposit')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                <?= $form->field($model, 'remaining_due_date')->widget(
                                    DatePicker::className(), [
                                    'name' => 'remaining_due_date',
                                    'value' => '12-31-2010',
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'format' => 'yyyy-mm-dd'
                                    ]
                                ]);?>
                                </div>
                                <div class="col-md-6">
                                <?= $form->field($model, 'occupancy')->widget(
                                    DatePicker::className(), [
                                    'name' => 'occupancy',
                                    'value' => '12-31-2010',
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'format' => 'yyyy-mm'
                                    ]
                                ]);?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                <?= $form->field($model, 'purchaser_name')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-md-6">
                                <?= $form->field($model, 'commission')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                <?= $form->field($model, 'expire_date')->widget(
                                    DatePicker::className(), [
                                    'name' => 'expire_date',
                                    'value' => '12-31-2010',
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'format' => 'yyyy-mm-dd'
                                    ]
                                ]);?>
                                </div>
                                <div class="col-md-6">
                                <?= $form->field($model, 'service')->dropDownList(CommonHelper::assingmentServiceDropDownList(), ['prompt' => 'Select']) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                <?= $form->field($model, 'psf')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <label class="break"></label>
                        </div>

                        <div class="divider"></div>
                        <div class="col-md-12">
                            <h3 class="assignments-create-title">Unit Details</h3>
                            <div class="row">
                                <div class="col-md-6">
                                    <?= $form->field($assignmentUnits, 'suite_number')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-md-6">
                                    <?php
                                    echo $form->field($assignmentUnits, 'beds_id')->widget(Select2::classname(), [
                                        'data' => CommonHelper::getActiveSuiteType(),
                                        'language' => 'en',
                                        'options' => ['placeholder' => 'Select Type'],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group field-assignmentsunits-balcony" style="margin-left: 15px;">
                                        <label for="assignmentsunits-balcony" class="control-label col-sm-4">Balcony</label>
                                        <div class="col-sm-8">
                                            <?= $form->field($assignmentUnits,'balcony')->checkBox(['label' => 'do you have a balcony?',
                                                'uncheck' => '0', 'checked' => '1'])->checkbox(['checked'=>false]) ?>
                                            <p class="help-block help-block-error "></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <?= $form->field($assignmentUnits, 'balcony_size')->textInput(['maxlength' => true,'readonly'=>true]) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group field-assignmentsunits-parking" style="margin-left: 15px;">
                                        <label for="assignmentsunits-parking" class="control-label col-sm-4">Parking</label>
                                        <div class="col-sm-8">
                                            <?= $form->field($assignmentUnits,'parking')->checkBox(['label' => 'do you have a parking spot ?',
                                                'uncheck' => '0', 'checked' => '1'])->checkbox(['checked'=>false]) ?>
                                            <p class="help-block help-block-error "></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($assignmentUnits, 'parking_total')->dropDownList(CommonHelper::parkingTotalDropDownList(), ['readonly'=>true]) ?>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group field-assignmentsunits-locker" style="margin-left: 15px;">
                                        <label for="assignmentsunits-locker" class="control-label col-sm-4">Locker</label>
                                        <div class="col-sm-8">
                                            <?= $form->field($assignmentUnits,'locker')->checkBox(['label' => 'is there a locker ?',
                                                'uncheck' => '0', 'checked' => '1'])->checkbox(['checked'=>false]) ?>
                                            <p class="help-block help-block-error "></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($assignmentUnits, 'baths')->dropDownList(CommonHelper::parkingTotalDropDownList()) ?>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <?= $form->field($assignmentUnits, 'sqft')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($assignmentUnits, 'exposure')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>


                            <label class="break"></label>
                        </div>
                        <div class="divider" style=""></div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3 class="assignments-create-title">Building Assets</h3>
                                        <div class="building-assets-wrapper">
                                            <?php foreach ($assignmentBuildingAssets as $index => $item) { ?>
                                                <div class="building-assets-list">
                                                    <div class="col-md-9" style="margin-left: 38px;">
                                                        <?= $form->field($item, '['.$index.']building_assets')->dropDownList(CommonHelper::buildingAssetsDropDownList(), ['prompt' => 'Select building assets']) ?>
                                                        <?= $form->field($item, '['.$index.']option')->radioList([ 'Url' => 'Url', 'File' => 'File', ], ['prompt' => '']) ?>
                                                        <div class="building_assets<?php echo $index;?>"></div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <div style="" class="pull-right">
                                                <?= Html::button('Add', ['class' => 'btn btn-success', 'id' => 'building_assets_id']) ?>
                                            </div>
                                        </div>
                                </div>
                                    <div class="col-md-6">
                                        <h3 class="assignments-create-title">Assignment Assets</h3>
                                        <div class="assignment-assets-wrapper">
                                            <?php

                                            //echo '<pre>'; print_r($assignmentAssets); echo '</pre>';

                                            foreach ($assignmentAssets as $indexassignment => $item_assignmentassets) { ?>
                                                <div class="assignment-assets-list">
                                                    <div class="col-md-9" style="margin-left: 38px;">
                                                        <?= $form->field($item_assignmentassets, '['.$indexassignment.']assignment_assets')->dropDownList(CommonHelper::assignmentAssetsDropDownList(), ['prompt' => 'Select assignment assets']) ?>
                                                        <?= $form->field($item_assignmentassets, '['.$indexassignment.']option')->radioList([ 'Url' => 'Url', 'File' => 'File', ], ['prompt' => '']) ?>
                                                        <div class="assignments_assets<?php echo $indexassignment;?>"></div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <div style="" class="pull-right">
                                                <?= Html::button('Add', ['class' => 'btn btn-success', 'id' => 'assignment_assets_id']) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <label class="break"></label>
                        </div>

                    <div class="divider" style=""></div>

                    <div class="footer-button">
                        <div class="row">
                            <div class="col-md-4 text-center">
                                <?php if(!$model->isNewRecord){?>
                                    <?= Html::a('Preview', Url::toRoute(['formtools/pre-view', 'id' => $model->id]), ['class' => 'btn btn-success btn-download-pdf btn-show-after-save','target'=>'_blank']) ?>
                                    <?= Html::a('Download HTML', Url::toRoute(['formtools/view', 'id' => $model->id]), ['class' => 'btn btn-success btn-download-html btn-show-after-save']) ?>
                                <?php }?>
                            </div>
                            <div class="col-md-4 text-center">
                            </div>
                            <div class="col-md-4 text-right">
                                <div class="admin-form-button">
                                    <?= FormButtons::widget() ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
        </div>
    </div>
</div>

<?php
$js = <<< JS
    $(function(){
        //Building Assets
    var buildingassetscount = $('.building-assets-wrapper .building-assets-list').length;
    $("#building_assets_id").on("click", function () {
        var newRow = $("<div class='building-assets-list'>");
        var cols = "";
        var count = buildingassetscount;
         var assets=count+1;
         
        cols += '<div class="col-md-9" style="margin-left: 38px;"><div class="form-group field-assignmentsbuildingassets-'+count+'-building_assets">' +
         '<label for="assignmentsbuildingassets-'+count+'-building_assets" class="control-label col-sm-4">Assets-'+assets+'</label> ' +
          '<div class="col-sm-8">' +
           '<select id="assignmentsbuildingassets-'+count+'-building_assets" name="AssignmentsBuildingAssets['+count+'][building_assets]" class="form-control">' +
                '<option value="">Select building assets</option> <option value="brochures">Brochures</option>' +
                '<option value="renderings">Renderings</option>' +
                '<option value="investor_incentives">Investor Incentives</option> ' +
                '<option value="videos">Videos</option>' +
                '<option value="features_finishes">Features and Finishes</option>' +
                '<option value="images">Images</option>' +
            '</select>' +
            '<p class="help-block help-block-error "></p>' +
          '</div></div>';
         cols += '<div class="form-group field-assignmentsbuildingassets-'+count+'-option required"><label class="control-label col-sm-4">Upload</label> <div class="col-sm-8"><input type="hidden" name="AssignmentsBuildingAssets['+count+'][option]" value=""><div id="assignmentsbuildingassets-'+count+'-option" prompt="" role="radiogroup"><div class="radio"><label><input type="radio" name="AssignmentsBuildingAssets['+count+'][option]" value="Url"> Url</label></div> <div class="radio"><label><input type="radio" name="AssignmentsBuildingAssets['+count+'][option]" value="File"> File</label></div></div> <p class="help-block help-block-error "></p></div></div>';
         cols+='<div class="building_assets'+count+'"></div></div>';
         
        cols += '<div style="text-align:center" class="col-md-1">';
            cols += '<input type="button" class="ibtnDel btn btn-md btn-danger "  value="x">';
        cols += '</div>';        
        
        newRow.append(cols);
        $(".building-assets-wrapper").append(newRow);
        
        $(".building-assets-wrapper").on("click", ".ibtnDel", function (event) {
            $(this).closest(".building-assets-list").remove();       
            buildingassetscount -= 1
        });
        
        calling_building_assets_options(count);
        buildingassetscount++;
        });
    
       //assignment assets
      var assignments_assets_count = $('.assignment-assets-wrapper .assignment-assets-list').length;
    $("#assignment_assets_id").on("click", function () {
        var newRow = $("<div class='assignment-assets-list'>");
        var cols = "";
        var count = assignments_assets_count;
        var assets=count+1;
        
        cols += '<div class="col-md-9" style="margin-left: 38px;">' +
         '<div class="form-group field-assignmentsassets-'+count+'-assignment_assets">' +
          '<label for="assignmentsassets-'+count+'-assignment_assets" class="control-label col-sm-4">Assets-'+assets+'</label> ' +
           '<div class="col-sm-8">' +
                '<select id="assignmentsassets-'+count+'-assignment_assets" name="AssignmentsAssets['+count+'][assignment_assets]" class="form-control">' +
                    '<option value="">Select assignment assets</option> ' +
                     '<option value="floor_plans">Floor Plane</option>' +
                     '<option value="images">Image</option>' +
                 '</select> ' +
                 '<p class="help-block help-block-error "></p>' +
            '</div>' +
          '</div>';
        
        cols +='<div class="form-group field-assignmentsassets-'+count+'-option"><label class="control-label col-sm-4">Upload</label> <div class="col-sm-8"><input type="hidden" name="AssignmentsAssets['+count+'][option]" value=""><div id="assignmentsassets-'+count+'-option" prompt="" role="radiogroup"><div class="radio"><label><input type="radio" name="AssignmentsAssets['+count+'][option]" value="Url"> Url</label></div> <div class="radio"><label><input type="radio" name="AssignmentsAssets['+count+'][option]" value="File"> File</label></div></div> <p class="help-block help-block-error "></p></div></div>';
        cols+='<div class="assignments_assets'+count+'"></div></div>';
        
        cols += '<div style="text-align:center" class="col-md-1">';
            cols += '<input type="button" class="ibtnDel btn btn-md btn-danger "  value="x">';
        cols += '</div>';        
        
        newRow.append(cols);
        $(".assignment-assets-wrapper").append(newRow);
        
        $(".assignment-assets-wrapper").on("click", ".ibtnDel", function (event) {
            $(this).closest(".assignment-assets-list").remove();       
            assignments_assets_count -= 1
        });
        calling_assignments_assets_options(count);
        assignments_assets_count++;
    });
    
         calling_building_assets_options(0);
         calling_assignments_assets_options(0);
         
        //Assignment building assets option change
        function calling_building_assets_options(index){
            $('input[type=radio][name="AssignmentsBuildingAssets['+index+'][option]"]').change(function () {   
              var value=this.value;
               var row = $(".building_assets"+index+"");
                     var cols = "";
              if (value=='Url'){
                    cols+='<div class="form-group field-assignmentsbuildingassets-upload_url has-success" style="margin-bottom: 17px!important;"><label for="assignmentsbuildingassets-upload_url" class="control-label col-sm-4">Upload Url</label> <div class="col-sm-8"><input type="text" id="assignmentsbuildingassets-upload_url" name="AssignmentsBuildingAssets['+index+'][upload_url]" maxlength="200" class="form-control" aria-invalid="false"> <p class="help-block help-block-error "></p></div></div>';
                    row.html(cols);
              } 
              else if (value=='File'){
                   cols+='<div class="form-group field-assignmentsbuildingassets-upload_file" style="margin-bottom: 17px!important;"><label for="assignmentsbuildingassets-upload_file" class="control-label col-sm-4">Upload File</label> <div class="col-sm-8"><input type="hidden" name="AssignmentsBuildingAssets['+index+'][upload_file]" value=""><input type="file" id="assignmentsbuildingassets-upload_file" name="AssignmentsBuildingAssets['+index+'][upload_file]"> <p class="help-block help-block-error "></p></div></div>';
                   row.html(cols);
              }
            });
        }
        //Assignment assets option change
        function calling_assignments_assets_options(index){
               $('input[type=radio][name="AssignmentsAssets['+index+'][option]"]').change(function () {   
                  var value=this.value;
                   var row = $(".assignments_assets"+index+"");
                         var cols = "";
                  if (value=='Url'){
                        cols+='<div class="form-group field-assignmentsassets-upload_url"><label for="assignmentsassets-upload_url" class="control-label col-sm-4">Upload Url</label> <div class="col-sm-8"><input type="text" id="assignmentsassets-upload_url" name="AssignmentsAssets['+index+'][upload_url]" maxlength="200" class="form-control"> <p class="help-block help-block-error "></p></div></div>';
                        row.html(cols);
                  } 
                  else if (value=='File'){
                       cols+='<div class="form-group field-assignmentsAssets-upload_file" style="margin-bottom: 17px!important;"><label for="assignmentsAssets-upload_file" class="control-label col-sm-4">Upload File</label> <div class="col-sm-8"><input type="hidden" name="AssignmentsAssets['+index+'][upload_file]" value=""><input type="file" id="assignmentsAssets-upload_file" name="AssignmentsAssets['+index+'][upload_file]"> <p class="help-block help-block-error "></p></div></div>';
                       row.html(cols);
                  }
                });
           }
           
       //Validation Form   
        //Listing Price
      $("#assignments-listing_price").on("input", function(evt) {
            var self = $(this);
            self.val(self.val().replace(/[^\d].+/, ""));
            if ((evt.which < 48 || evt.which > 57))
            {
                evt.preventDefault();
            }
        });
        $("#assignments-listing_price").change("input", function(evt) {
            var self = $(this);
            var str=Number(self.val()).toString();
            self.val('$'+str.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        }); 
        
        //Original Price
      $("#assignments-original_price").on("input", function(evt) {
            var self = $(this);
            self.val(self.val().replace(/[^\d].+/, ""));
            if ((evt.which < 48 || evt.which > 57))
            {
                evt.preventDefault();
            }
        });
        $("#assignments-original_price").change("input", function(evt) {
            var self = $(this);
            var str=Number(self.val()).toString();
            self.val('$'+str.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        });  
        
        //PSF Price
        $("#assignments-psf").on("input", function(evt) {
            var self = $(this);
            self.val(self.val().replace(/[^\d].+/, ""));
            if ((evt.which < 48 || evt.which > 57))
            {
                evt.preventDefault();
            }
        });
        $("#assignments-psf").change("input", function(evt) {
            var self = $(this);
             var str=Number(self.val()).toString();
            self.val('$'+str.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        });
         //Deposite Price
        $("#assignments-deposit_price").on("input", function(evt) {
            var self = $(this);
            self.val(self.val().replace(/[^\d].+/, ""));
            if ((evt.which < 48 || evt.which > 57))
            {
                evt.preventDefault();
            }
        });
      
        $("#assignments-deposit_price").change("input", function(evt) {
            var self = $(this);
            var str=Number(self.val()).toString();
            self.val('$'+str.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        });
        
        //Remaining Deposite Price
      $("#assignments-remaining_deposit").on("input", function(evt) {
            var self = $(this);
            self.val(self.val().replace(/[^\d].+/, ""));
            if ((evt.which < 48 || evt.which > 57))
            {
                evt.preventDefault();
            }
        });
      
        $("#assignments-remaining_deposit").change("input", function(evt) {
            var self = $(this);
            var str=Number(self.val()).toString();
            self.val('$'+str.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        });
        
        //Commission Percentage
        $('#assignments-commission').change(function() {
            $(this).val(function(index, old) { return old.match(/^\d+\.?\d{0,1}/, '') + '%'; });
        });

        $("#assignments-commission").on("input", function(evt) {
           this.value = this.value.match(/^\d+\.?\d{0,1}/);
        });
       
        
        $('#assignmentsunits-balcony').click(function() {
             if ($(this).is(':checked')) {
                 $('#assignmentsunits-balcony_size').removeAttr('readonly');
             }else {
                 $('#assignmentsunits-balcony_size').attr('readonly', 'readonly');
             }
        });
        
        $("#assignmentsunits-parking_total").prop('disabled', true);
        
        $('#assignmentsunits-parking').click(function() {
             if ($(this).is(':checked')) {
                 $("#assignmentsunits-parking_total").prop('disabled', false);
                 $('#assignmentsunits-parking_total').removeAttr('readonly');
             }else {
                  $("#assignmentsunits-parking_total").prop('disabled', true);
                 $('#assignmentsunits-parking_total').attr('readonly', 'readonly');
             }
        });
        
        //validate belcony
        $('#assignmentsunits-balcony_size').keyup(function(e){
              if (/\D/g.test(this.value))
              {
                this.value = this.value.replace(/\D/g, '');
              }
        });
        
   /*     //validate baths
        $('#assignmentsunits-baths').keyup(function(e){
              if (/\D/g.test(this.value))
              {
                this.value = this.value.replace(/\D/g, '');
              }
        });*/
        
    });
JS;
$this->registerJs($js);
?>

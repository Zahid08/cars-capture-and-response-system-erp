<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model unlock\modules\assignments\models\Assignments */
?>
<div class="assignments-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('view', [
        'model' => $model,
        'assignmentUnits' => $assignmentUnits,
    ]) ?>

</div>

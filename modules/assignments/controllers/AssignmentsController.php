<?php

namespace unlock\modules\assignments\controllers;

use api\common\maropost\maropost;
use unlock\modules\assignments\models\AssignmentsAssets;
use unlock\modules\assignments\models\AssignmentsBuildingAssets;
use unlock\modules\assignments\models\AssignmentsUnits;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\usermanager\user\models\User;
use Yii;
use unlock\modules\assignments\models\Assignments;
use unlock\modules\assignments\models\AssignmentsSearch;
use yii\base\Model;
use yii\db\Exception;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * AssignmentsController implements the CRUD actions for Assignments model.
 */
class AssignmentsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Assignments models.
     * @return mixed
     */
    public function actionIndex()
    {
        $assignments    =new Assignments();
        $searchModel    = new AssignmentsSearch();
        $dataProvider   = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'assignments' => $assignments,
        ]);
    }

    /**
     * Displays a single Assignments model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model          =$this->findModel($id);
        $mediaList      =CommonHelper::getMediaList();
        $assignments    =new Assignments();
        $assignmentUnits=AssignmentsUnits::find()->where(['assignment_id'=>$id])->one();

        $developmentData=CommonHelper::developmentListById($model->development_id);
       if (!empty($developmentData)){
           $development_address=$developmentData->development_address;
       }
        return $this->render('view', [
            'model' => $model,
            'assignmentUnits' => $assignmentUnits,
            'mediaList'       => $mediaList,
            'assignments'     => $assignments,
            'development_address' => $development_address,
        ]);
    }

    public function actionEmaiTemplateLoad(){

        $model  =   $_REQUEST;

        try {
            $messageBody = $this->renderAjax('_emailSingleTemplate', [
                'model' => $model,
            ]);
           return $messageBody;
        }
        catch (Exception $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

    }

    public function actionEmailMultipleTemplateLoad(){

        $model  =   $_REQUEST;

        try {
            $messageBody = $this->renderAjax('_emailTemplate', [
                'model' => $model,
            ]);
            return $messageBody;
        }
        catch (Exception $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

    }

    /*
     * Multiple Assignment Send To Selected User Email Form Index Page
     */
    public function actionAssignmentMultipleEmail(){
        $model      = $_REQUEST;
        $user_id    = $model['email_id'];
        $user_info=preg_split ("/\,/", $user_id);

        $subject    = !empty($model['subject']) ? $model['subject'] : '';
        $messsage_top_content=   !empty($model['message_body']) ? $model['message_body'] : '';

        $messageBody = $this->renderAjax('_emailTemplate', [
            'model' => $model,
            'messsage_top_content' => $messsage_top_content,
        ]);

        //Put template to maropost
        $maropost=new maropost();
        $arrayNew = array(
            "content" => array(
                "name" =>'Assignments Email[All]',
                //"text_part" => "text part of the content",
                "html_part" => $messageBody,
                "full_email" => "false",
                "footer_type" => "default_footer",
                "footer_id" => ""
            ),
            "id" =>255
        );
        $maropost->put_template_content($arrayNew);
        foreach ($user_info as $dataId) {
            $user_data  =User::find()->where(['id'=>$dataId])->one();
            $response_register = $this->registrationUserForAllAssignments($user_data, $subject);  //register user first then auto journey send
            if ($response_register['http_status'] == 201) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Email Successfully Send'));
            }
        }
        return $this->redirect('index');
    }

    /*
         * Single Assignment Send To Selected User Email Form view Page
     */
    public function actionAssignmentSingleEmail(){
        $model=$_REQUEST;
        $user_id=$model['email_id'];
        $user_info=preg_split ("/\,/", $user_id);
        $assignment_id=$model['id'];

        $subject=   !empty($model['subject']) ? $model['subject'] : '';
        $messsage_top_content=   !empty($model['message_body']) ? $model['message_body'] : '';

        $messageBody = $this->renderAjax('_emailSingleTemplate', [
            'model' => $model,
            'messsage_top_content' => $messsage_top_content,
        ]);

        //Put template
        $maropost=new maropost();
        $arrayNew = array(
            "content" => array(
                "name" =>'Assignments Email[Single]',
                "html_part" => $messageBody,
                "full_email" => "false",
                "footer_type" => "default_footer",
                "footer_id" => ""
            ),
            "id" =>256
        );
        $maropost->put_template_content($arrayNew);
        foreach ($user_info as $dataId) {
            $user_data=User::find()->where(['id'=>$dataId])->one();
            $response_register=$this->registrationUserForSingleAssignments($user_data,$subject);
            if ($response_register['http_status']==201){
                Yii::$app->session->setFlash('success', Yii::t('app', 'Email Successfully Send'));
            }
        }
        return $this->redirect(['view', 'id' => $assignment_id]);
    }

    /**
     * Creates a new Assignments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Assignments();
        $assignmentUnits = new AssignmentsUnits();
        $assignmentAssets = [new AssignmentsAssets()];
        $assignmentBuildingAssets = [new AssignmentsBuildingAssets()];

        //Building assets
        $assignmentsBulidingAssetsArray = Yii::$app->request->post('AssignmentsBuildingAssets');
        if($assignmentsBulidingAssetsArray){
            $assignmentBuildingAssets = [];
            foreach ($assignmentsBulidingAssetsArray as $key => $item) {
                $assignmentBuildingAssets[$key] = new AssignmentsBuildingAssets();
            }
        }
        Model::loadMultiple($assignmentBuildingAssets, Yii::$app->request->post());

        //Assignment Assets
        $assignmentsAssetsArray = Yii::$app->request->post('AssignmentsAssets');
        if($assignmentsAssetsArray){
            $assignmentAssets = [];
            foreach ($assignmentsAssetsArray as $key => $item) {
                $assignmentAssets[$key] = new AssignmentsAssets();
            }
        }
        Model::loadMultiple($assignmentAssets, Yii::$app->request->post());

        if ($model->load(Yii::$app->request->post()) && $assignmentUnits->load(Yii::$app->request->post()) &&  Model::loadMultiple($assignmentAssets, Yii::$app->request->post()) && Model::loadMultiple($assignmentBuildingAssets, Yii::$app->request->post()) ) {
            $this->saveData($model, $assignmentUnits, $assignmentAssets, $assignmentBuildingAssets);
        }
        return $this->render('_form', [
            'model' => $model,
            'assignmentUnits' => $assignmentUnits,
            'assignmentAssets' => $assignmentAssets,
            'assignmentBuildingAssets' => $assignmentBuildingAssets,
        ]);
    }

    /**
     * Updates an existing Assignments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $assignmentUnits = AssignmentsUnits::find()->where(['assignment_id' => $id])->one();
        if ($model->load(Yii::$app->request->post()) && $assignmentUnits->load(Yii::$app->request->post())) {
            if (isset($model) && isset($assignmentUnits)){
                try {
                    if ($model->save()){
                        $assignmentUnits->updated_at = date('Y-m-d H:i:s');
                        if (!$assignmentUnits->save()) {
                            throw new Exception(Yii::t('app', Html::errorSummary($assignmentUnits)));
                        }
                        Yii::$app->session->setFlash('success', Yii::t('app', 'Record successfully updated.'));
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                }
                catch (Exception $e) {
                    Yii::$app->session->setFlash('error', $e->getMessage());
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'assignmentUnits' => $assignmentUnits,
        ]);
    }

    public function actionUnitsAssetsAddmore($id, $type="", $developmentID = ""){

        $assignmentAssets       = new AssignmentsAssets();
        $upload_file_old_data   = '';

        if ( $assignmentAssets->load(Yii::$app->request->post()) ) {

            if($assignmentAssets->save()){

                if( UploadedFile::getInstance($assignmentAssets, 'upload_file') ){

                    //File Directory Create
                    if (!file_exists(Yii::getAlias('@assignments') . '/assets-' . $assignmentAssets->assignment_id)) {
                        mkdir(Yii::getAlias('@assignments') . '/assets-' . $assignmentAssets->assignment_id, 0777, true);
                    }
                    // file directory create
                    $assignmentAssets->upload_file = UploadedFile::getInstance($assignmentAssets, 'upload_file');
                    $assignmentAssets->upload_file->saveAs(Yii::getAlias('@assignments') . '/assets-' . $assignmentAssets->assignment_id . '/' . $assignmentAssets->upload_file->name);
                    $assignmentAssets->upload_file = Yii::getAlias('@baseUrl').'/assignments/common/assets-'. $assignmentAssets->assignment_id . '/' . $assignmentAssets->upload_file->name;
                    $assignmentAssets->save();
                }else{
                    $assignmentAssets->upload_file = $upload_file_old_data;
                    $assignmentAssets->save();
                }

                Yii::$app->session->setFlash('success', Yii::t('app', 'Record successfully Addedd.'));
                if($type == 'development'){
                    return $this->redirect(['/developments/developments/view', 'id' => $developmentID ]);
                }else{
                    return $this->redirect(['assignments/view', 'id' => $assignmentAssets->assignment_id ]);
                }
            }else{
                Yii::$app->session->setFlash('error', Yii::t('app', 'Record did not added. Please try again') );
                if($type == 'development'){
                    return $this->redirect(['/developments/developments/view', 'id' => $developmentID ]);
                }else{
                    return $this->redirect(['assignments/view', 'id' => $assignmentAssets->assignment_id ]);
                }
            }

        }

        return $this->renderAjax('unit-assets-addmore', [
            'assignmentAssets'  => $assignmentAssets,
            'assignment_id'     => $id,
        ]);
    }
    public function actionUnitsAssetsUpdate($id, $type="", $developmentID = ""){

        $assignmentAssets       = AssignmentsAssets::find()->where(['id' => $id])->one();
        $upload_file_old_data   = '';

        if( Yii::$app->request->post() ){
            $upload_file = UploadedFile::getInstance($assignmentAssets, 'upload_file');
            if(empty($upload_file)){
                $upload_file_old_data   = $assignmentAssets->upload_file;
            }
        }

        if ( $assignmentAssets->load(Yii::$app->request->post()) ) {

            if($assignmentAssets->save()){

                if( UploadedFile::getInstance($assignmentAssets, 'upload_file') ){

                    //File Directory Create
                    if (!file_exists(Yii::getAlias('@assignments') . '/assets-' . $assignmentAssets->assignment_id)) {
                        mkdir(Yii::getAlias('@assignments') . '/assets-' . $assignmentAssets->assignment_id, 0777, true);
                    }
                    // file directory create
                    $assignmentAssets->upload_file = UploadedFile::getInstance($assignmentAssets, 'upload_file');
                    $assignmentAssets->upload_file->saveAs(Yii::getAlias('@assignments') . '/assets-' . $assignmentAssets->assignment_id . '/' . $assignmentAssets->upload_file->name);
                    $assignmentAssets->upload_file = Yii::getAlias('@baseUrl').'/assignments/common/assets-'. $assignmentAssets->assignment_id . '/' . $assignmentAssets->upload_file->name;
                    $assignmentAssets->save();
                }else{
                    $assignmentAssets->upload_file = $upload_file_old_data;
                    $assignmentAssets->save();
                }

                Yii::$app->session->setFlash('success', Yii::t('app', 'Record successfully updated.'));

                if($type == 'development'){
                    return $this->redirect(['/developments/developments/view', 'id' => $developmentID ]);
                }else{
                    return $this->redirect(['assignments/view', 'id' => $assignmentAssets->assignment_id ]);
                }
            }else{
                Yii::$app->session->setFlash('error', Yii::t('app', 'Record did not updated. Please try again') );

                if($type == 'development'){
                    return $this->redirect(['/developments/developments/view', 'id' => $developmentID ]);
                }else{
                    return $this->redirect(['assignments/view', 'id' => $assignmentAssets->assignment_id ]);
                }
            }

        }

        return $this->renderAjax('unit-assets-update', [
            'assignmentAssets'  => $assignmentAssets,
        ]);
    }

    public function actionUnitsAssetsDelete($id){

        $assignmentAssets    = AssignmentsAssets::find()->where(['id' => $id])->one();

        if($assignmentAssets->delete()){
            Yii::$app->session->setFlash('success', Yii::t('app', 'Record successfully deleted.'));
            return $this->redirect(['assignments/view', 'id' => $assignmentAssets->assignment_id ]);
        }else{
            Yii::$app->session->setFlash('error', Yii::t('app', 'Record did not deleted. Please try again') );
            return $this->redirect(['assignments/view', 'id' => $assignmentAssets->assignment_id ]);
        }
    }

    public function actionBuildingAssetsAddmore($id, $type="", $developmentID = ""){

        $buildingAssets         = new AssignmentsBuildingAssets();
        $upload_file_old_data   = '';

        if ( $buildingAssets->load(Yii::$app->request->post()) ) {

            if($buildingAssets->save()){

                if( UploadedFile::getInstance($buildingAssets, 'upload_file') ){

                    //File Directory Create
                    if (!file_exists(Yii::getAlias('@assignments') . '/assets-' . $buildingAssets->assignment_id)) {
                        mkdir(Yii::getAlias('@assignments') . '/assets-' . $buildingAssets->assignment_id, 0777, true);
                    }
                    // file directory create
                    $buildingAssets->upload_file = UploadedFile::getInstance($buildingAssets, 'upload_file');
                    $buildingAssets->upload_file->saveAs(Yii::getAlias('@assignments') . '/assets-' . $buildingAssets->assignment_id . '/' . $buildingAssets->upload_file->name);
                    $buildingAssets->upload_file = Yii::getAlias('@baseUrl').'/assignments/common/assets-'. $buildingAssets->assignment_id . '/' . $buildingAssets->upload_file->name;
                    $buildingAssets->save();
                }else{
                    $buildingAssets->upload_file = $upload_file_old_data;
                    $buildingAssets->save();
                }

                Yii::$app->session->setFlash('success', Yii::t('app', 'Record successfully added.'));
                if($type == 'development'){
                    return $this->redirect(['/developments/developments/view', 'id' => $developmentID ]);
                }else{
                    return $this->redirect(['assignments/view', 'id' => $buildingAssets->assignment_id ]);
                }

            }else{
                Yii::$app->session->setFlash('error', Yii::t('app', 'Record did not added. Please try again') );
                if($type == 'development'){
                    return $this->redirect(['/developments/developments/view', 'id' => $developmentID ]);
                }else{
                    return $this->redirect(['assignments/view', 'id' => $buildingAssets->assignment_id ]);
                }

            }

        }

        return $this->renderAjax('building-assets-addmore', [
            'buildingAssets'    => $buildingAssets,
            'assignment_id'     => $id,
        ]);
    }

    public function actionBuildingAssetsUpdate($id, $type="", $developmentID = ""){

        $buildingAssets         = AssignmentsBuildingAssets::find()->where(['id' => $id])->one();
        $upload_file_old_data   = '';

        if( Yii::$app->request->post() ){
            $upload_file = UploadedFile::getInstance($buildingAssets, 'upload_file');
            if(empty($upload_file)){
                $upload_file_old_data   = $buildingAssets->upload_file;
            }
        }

        if ( $buildingAssets->load(Yii::$app->request->post()) ) {

            if($buildingAssets->save()){

                if( UploadedFile::getInstance($buildingAssets, 'upload_file') ){

                    //File Directory Create
                    if (!file_exists(Yii::getAlias('@assignments') . '/assets-' . $buildingAssets->assignment_id)) {
                        mkdir(Yii::getAlias('@assignments') . '/assets-' . $buildingAssets->assignment_id, 0777, true);
                    }
                    // file directory create
                    $buildingAssets->upload_file = UploadedFile::getInstance($buildingAssets, 'upload_file');
                    $buildingAssets->upload_file->saveAs(Yii::getAlias('@assignments') . '/assets-' . $buildingAssets->assignment_id . '/' . $buildingAssets->upload_file->name);
                    $buildingAssets->upload_file = Yii::getAlias('@baseUrl').'/assignments/common/assets-'. $buildingAssets->assignment_id . '/' . $buildingAssets->upload_file->name;
                    $buildingAssets->save();
                }else{
                    $buildingAssets->upload_file = $upload_file_old_data;
                    $buildingAssets->save();
                }

                Yii::$app->session->setFlash('success', Yii::t('app', 'Record successfully updated.'));
                if($type == 'development'){
                    return $this->redirect(['/developments/developments/view', 'id' => $developmentID ]);
                }else{
                    return $this->redirect(['assignments/view', 'id' => $buildingAssets->assignment_id ]);
                }

            }else{
                Yii::$app->session->setFlash('error', Yii::t('app', 'Record did not updated. Please try again') );
                if($type == 'development'){
                    return $this->redirect(['/developments/developments/view', 'id' => $developmentID ]);
                }else{
                    return $this->redirect(['assignments/view', 'id' => $buildingAssets->assignment_id ]);
                }

            }

        }

        return $this->renderAjax('building-assets-update', [
            'buildingAssets'  => $buildingAssets,
        ]);
    }

    public function actionBuildingAssetsDelete($id){

        $buildingAssets    = AssignmentsBuildingAssets::find()->where(['id' => $id])->one();

        if($buildingAssets->delete()){
            Yii::$app->session->setFlash('success', Yii::t('app', 'Record successfully deleted.'));
            return $this->redirect(['assignments/view', 'id' => $buildingAssets->assignment_id ]);
        }else{
            Yii::$app->session->setFlash('error', Yii::t('app', 'Record did not deleted. Please try again') );
            return $this->redirect(['assignments/view', 'id' => $buildingAssets->assignment_id ]);
        }
    }

    /**
     * Deletes an existing Assignments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Assignments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Assignments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Assignments::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

     /*
         * Save Assignments Form Data
     */
    public function saveData($model, $assignmentUnits, $assignmentAssets, $assignmentBuildingAssets){
        if (isset($model) && isset($assignmentUnits) && isset($assignmentAssets) && isset($assignmentBuildingAssets) ){
            try {
                if ($model->save()) {
                    $assignment_insert_id=$model->id;

                    //Image Directory Create
                    if (!file_exists(Yii::getAlias('@assignments') . '/assets-' . $model->id)) {
                        mkdir(Yii::getAlias('@assignments') . '/assets-' . $model->id, 0777, true);
                    }

                    //Save Building Assignments
                    $upload_count = 0;
                    foreach ($assignmentBuildingAssets as $keyindex=>$assignmentBuildingAssetsItem){
                        $oldImgUrl = isset($assignmentBuildingAssetsItem->oldAttributes['upload_file']) ? $assignmentBuildingAssetsItem->oldAttributes['['.$upload_count.']upload_file'] : null;
                        $upload_assignment_building_assets_file = UploadedFile::getInstance($assignmentBuildingAssetsItem, '['.$upload_count.']upload_file');
                        $upload_assignment_assets_url=$assignmentBuildingAssetsItem->upload_url;

                        if (!empty($upload_assignment_building_assets_file)){
                            if ($assignmentBuildingAssetsItem->upload_file =$upload_assignment_building_assets_file) {
                                $assignmentBuildingAssetsItem->upload_file->saveAs(Yii::getAlias('@assignments').'/assets-'.$model->id.'/'.$assignmentBuildingAssetsItem->upload_file->name);
                            }
                            $imagepath=Yii::getAlias('@baseUrl').'/assignments/common/assets-'.$model->id.'/'.$assignmentBuildingAssetsItem->upload_file->name;
                            $assignmentBuildingAssetsItem->upload_file=$imagepath;
                        }
                        else{
                            $assignmentBuildingAssetsItem->upload_url = $upload_assignment_assets_url;
                        }
                        $assignmentBuildingAssetsItem->assignment_id = $assignment_insert_id;
                        if (!$assignmentBuildingAssetsItem->save()) {
                            throw new Exception(Yii::t('app', Html::errorSummary($assignmentBuildingAssetsItem)));
                         }
                        $upload_count++;
                    }

                    //Save Asignments Assets
                    $count=0;
                    foreach ($assignmentAssets as $key=>$assignmentAssetsItem){
                            $oldImgUrl = isset($assignmentAssetsItem->oldAttributes['upload_file']) ? $assignmentAssetsItem->oldAttributes['[' . $count . ']upload_file'] : null;
                            $upload_assignment_assets_file = UploadedFile::getInstance($assignmentAssetsItem, '[' . $count . ']upload_file');
                            $upload_assignment_assets_url=$assignmentAssetsItem->upload_url;

                            if (!empty($upload_assignment_assets_file)) {
                                if ($assignmentAssetsItem->upload_file = $upload_assignment_assets_file) {
                                    $assignmentAssetsItem->upload_file->saveAs(Yii::getAlias('@assignments') . '/assets-' . $model->id . '/' . $assignmentAssetsItem->upload_file->name);
                                }
                                $imagepath =Yii::getAlias('@baseUrl').'/assignments/common/assets-'. $model->id . '/' . $assignmentAssetsItem->upload_file->name;
                                $assignmentAssetsItem->upload_file = $imagepath;
                            }
                            else{
                                $assignmentAssetsItem->upload_url = $upload_assignment_assets_url;
                            }
                            $assignmentAssetsItem->assignment_id = $assignment_insert_id;
                            if (!$assignmentAssetsItem->save()) {
                                throw new Exception(Yii::t('app', Html::errorSummary($assignmentAssetsItem)));
                            }
                        $count++;
                    }

                    //save assignment Units
                    $assignmentUnits->assignment_id = $assignment_insert_id;
                    if (!$assignmentUnits->save()) {
                        throw new Exception(Yii::t('app', Html::errorSummary($assignmentUnits)));
                    }
                    Yii::$app->session->setFlash('success', Yii::t('app', 'Record successfully saved'));
                    return $this->redirect(['index']);
                }
            }
            catch (Exception $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

    }

    private function _redirect($model){
        $action = Yii::$app->request->post('action', 'save');
        switch ($action) {
            case 'save':
                return $this->redirect(['index']);
                break;
            case 'apply':
                return $this->redirect(['update', 'id' => $model->id]);
                break;
            case 'save2new':
                return $this->redirect(['create']);
                break;
            default:
                return $this->redirect(['index']);
        }
    }

    private function registrationUserForAllAssignments($user_data,$subject){
        $first_name =   !empty($user_data->first_name) ? $user_data->first_name : '';
        $last_name =   !empty($user_data->last_name) ? $user_data->last_name : '';
        $mobile =   !empty($user_data->mobile) ? $user_data->mobile : '';
        $email =   !empty($user_data->email) ? $user_data->email : '';
        $list_id=50;
        $contactsNew=new maropost();
        $contactsNew = $contactsNew->contacts();
        $arrayNew = array(
            'list_id' => $list_id,
            'contact' =>
                array(
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'email' => $email,
                    'subscribe' => 'true',
                    'custom_field' =>
                        array(
                            'phone_1' =>$mobile,
                            'company' => 'CONNECT asset management',
                            'realtor' => 'False',
                            'prospect' => 'true',
                            'assignment_subject' => $subject,
                            'assignment_email_all' => time()
                        )
                ));
        $response=$contactsNew->post_new_contact_into_list($arrayNew);
        return $response;
    }

    private function registrationUserForSingleAssignments($user_data,$subject){
        $first_name =   !empty($user_data->first_name) ? $user_data->first_name : '';
        $last_name =   !empty($user_data->last_name) ? $user_data->last_name : '';
        $mobile =   !empty($user_data->mobile) ? $user_data->mobile : '';
        $email =   !empty($user_data->email) ? $user_data->email : '';
        $list_id=50;
        $contactsNew=new maropost();
        $contactsNew = $contactsNew->contacts();
        $arrayNew = array(
            'list_id' => $list_id,
            'contact' =>
                array(
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'email' => $email,
                    'subscribe' => 'true',
                    'custom_field' =>
                        array(
                            'phone_1' =>$mobile,
                            'company' => 'CONNECT asset management',
                            'realtor' => 'False',
                            'prospect' => 'true',
                            'assignment_subject' => $subject,
                            'assignment_email_single' => time()
                        )
                ));
        $response=$contactsNew->post_new_contact_into_list($arrayNew);
        return $response;
    }


}

<?php

use yii\db\Migration;

/**
 * Class m170123_114524_auth_role_action_permission_map_tbl
 */
class m170123_114524_auth_role_action_permission_map_tbl extends Migration
{
    /**
     * This method contains the logic to be executed when applying this migration.
     */
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%auth_role_action_permission_map}}', [
            'id'            => $this->primaryKey(),
            'role_id'       => $this->integer()->notNull(),
            'action_id'     => $this->integer()->notNull(),
            'created_by'    => $this->integer(),
            'created_at'    => $this->dateTime(),
            'updated_by'    => $this->integer(),
            'updated_at'    => $this->dateTime(),
            'status'        => $this->smallInteger(2)->defaultValue(1),
        ], $tableOptions);

    }

    /**
     * This method contains the logic to be executed when removing this migration.
     */
    public function down()
    {
        $this->dropTable('{{%auth_role_action_permission_map}}');
    }
}

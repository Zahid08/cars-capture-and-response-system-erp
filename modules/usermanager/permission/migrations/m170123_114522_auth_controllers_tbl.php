<?php

use yii\db\Migration;

/**
 * Class m170123_114522_auth_controllers_tbl
 */
class m170123_114522_auth_controllers_tbl extends Migration
{
    /**
     * This method contains the logic to be executed when applying this migration.
     */
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%auth_controllers}}', [
            'id'                => $this->primaryKey()->unsigned(),
            'path'              => $this->string(255)->notNull()->comment('user/admin'),
            'module_name'       => $this->string(255)->comment('Module name'),
            'controller_name'   => $this->string(255)->notNull()->comment('Controller Name'),
            'description'       => $this->string(255)->comment('Controller Description'),
            'created_at'        => $this->dateTime(),
            'updated_at'        => $this->dateTime(),
            'status'            => $this->smallInteger(2)->defaultValue(1)->comment('1=>Active, 2=>Inactive'),
        ], $tableOptions);

    }

    /**
     * This method contains the logic to be executed when removing this migration.
     */
    public function down()
    {
        $this->dropTable('{{%auth_controllers}}');
    }
}

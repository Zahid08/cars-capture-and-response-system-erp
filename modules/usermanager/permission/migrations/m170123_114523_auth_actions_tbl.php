<?php

use yii\db\Migration;

/**
 * Class m170123_114523_auth_actions_tbl
 */
class m170123_114523_auth_actions_tbl extends Migration
{
    /**
     * This method contains the logic to be executed when applying this migration.
     */
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%auth_actions}}', [
            'id'            => $this->primaryKey(),
            'controller_id' => $this->integer()->notNull(),
            'name'          => $this->string(255)->notNull()->comment('Controller Action Name'),
            'description'   => $this->string(255)->comment('Controller Action Description'),
            'created_at'    => $this->dateTime(),
            'updated_at'    => $this->dateTime(),
            'manually'      => $this->smallInteger(2)->defaultValue(0),
            'status'        => $this->smallInteger(2)->defaultValue(1),
        ], $tableOptions);

    }

    /**
     * This method contains the logic to be executed when removing this migration.
     */
    public function down()
    {
        $this->dropTable('{{%auth_actions}}');
    }
}

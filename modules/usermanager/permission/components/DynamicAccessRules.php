<?php 
namespace unlock\modules\usermanager\permission\components;

use Yii;
use yii\base\Component;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use unlock\modules\usermanager\permission\models\AuthControllers;
use unlock\modules\usermanager\permission\models\AuthActions;
use unlock\modules\usermanager\permission\models\AuthRoleActionPermissionMap;
use yii\db\ActiveRecord;
use unlock\modules\usermanager\role\models\Role;
use unlock\modules\core\helpers\CommonHelper;

class DynamicAccessRules extends Component 
{
    private $controllerPath;

	/**
	* Set Controller Behaviors
	*/
	public function customAccess($path)
	{
        $this->controllerPath = $path;

	    // Guest User
        if(Yii::$app->user->isGuest){
            if(Yii::$app->session->has('sessionActionAssignArray')) Yii::$app->session->remove('sessionActionAssignArray');
        }
        else{
            // user type session not found
            if(empty(CommonHelper::getLoggedInUserRoleId()))
            {
                if(Yii::$app->session->has('sessionActionAssignArray')) Yii::$app->session->remove('sessionActionAssignArray');
                Yii::$app->user->logout();
            }

            // check user identity
            if(empty(Yii::$app->user->identity))
            {
                if(Yii::$app->session->has('sessionActionAssignArray')) Yii::$app->session->remove('sessionActionAssignArray');
                Yii::$app->user->logout();
            }
        }

	    if(empty(Yii::$app->session->get('sessionActionAssignArray')))
	    {
            AuthRoleActionPermissionMap::reloadUserPermission();
		}

		$actions = $this->controllerAction();

	    $allow = empty($actions) ?  false : true;

        $rules = [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => $actions,
                        'allow' => $allow,
                    ],
                ],
        ];

        return $rules;
	}

	/**
	* actions permission by controller
	*/
	protected function controllerAction()
	{
        $controllerPath	= strtolower($this->controllerPath);
        if(!empty(Yii::$app->session->get('sessionActionAssignArray'))){
            return (array_key_exists($controllerPath, Yii::$app->session->get('sessionActionAssignArray'))) ? Yii::$app->session->get('sessionActionAssignArray')[$controllerPath] : array($this->controllerPath => '');
        }
        else {
            return [];
        }
	}
}


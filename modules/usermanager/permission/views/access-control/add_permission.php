<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use unlock\modules\usermanager\role\models\Role;
use unlock\modules\usermanager\permission\models\AuthControllers;
use unlock\modules\core\buttons\BackButton;
use unlock\modules\core\buttons\FormButtons;

//use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model unlock\modules\usermanager\permission\models\AuthRoles */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Create Permissions');
?>
<style type="text/css">
    #allculps,#allculpsoff{
        cursor: pointer;
    }
</style>
<div class="main-container main-container-view" role="main">
    <div class="page-header">
       <!-- <div class="page-header-title">
            <h1 class="page-title"><?/*= Yii::t('app', 'Permissions') */?></h1>
        </div>-->
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li><?= Html::a('Users', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li><?= Html::a('Permissions', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li><?= Html::a('Add Permissions ', Yii::$app->homeUrl, ['class' => '']) ?></li>
            </ul>
        </div>
    </div>

    <div class="admin-form-container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fas fa-pencil-alt"></i> <?= Html::encode($this->title) ?></h3>
                </div>
                <div class="panel-heading-button">
                    <?= BackButton::widget() ?>
                </div>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
                    /*'options' => ['enctype' => 'multipart/form-data'],*/
                    'validateOnBlur' =>true,
                    'enableAjaxValidation'=>true,
                    'errorCssClass'=>'has-error',
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
                        'options' => ['class' => 'form-group'],
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-2',
                            'offset' => '',
                            'wrapper' => '',
                            'hint' => '',
                        ],
                    ],
                ]); ?>

                <?= $form->field($model, 'controller_id')->dropDownList(AuthControllers::dropDownList()); ?>

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <div class="admin-form-button">
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <?= FormButtons::widget() ?>
                        </div>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

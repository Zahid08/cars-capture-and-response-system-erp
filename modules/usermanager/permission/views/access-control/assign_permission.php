<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use unlock\modules\usermanager\role\models\Role;
use unlock\modules\core\helpers\CommonHelper;

//use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model unlock\modules\usermanager\permission\models\AuthRoles */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Permissions');
?>
<style type="text/css">
    #allculps,#allculpsoff{
        cursor: pointer;
    }
    .admin-grid-toolbar{
        overflow: hidden;
        margin-bottom: 15px;
    }
    .permission-drop-down-role{
        float: left;
    }
    .admin-grid-toolbar .permission-drop-down-role label{
        float: left;
        font-weight:bold;
        line-height: 28px;
        padding-right: 10px;
        margin: 0;
    }
    .admin-grid-toolbar .permission-drop-down-role select{
        float: left;
        width: 200px;
    }
    .admin-grid-toolbar .permission-drop-down-role button{
        float: left;
        margin-left: 10px;
        padding: 4px 10px;
        font-weight: bold;
    }
    #toolbar-new{
        float: right;
    }
    .panel-heading{
        overflow: hidden;
    }
    .panel-title-permission{
        float: left;
    }
    .all-collapse-on{
        cursor: pointer;
    }
    .collapse-on-off-btn{
        cursor: pointer;
        float: right;
    }
    .panel-default > .panel-heading{
        background: #f5f5f5;
        border: 1px solid #ddd;
    }
</style>
<div class="main-container main-container-view" role="main">
    <div class="page-header">
       <!-- <div class="page-header-title">
            <h1 class="page-title"><?/*= Html::encode($this->title) */?></h1>
        </div>-->
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li><?= Html::a('Users', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li><?= Html::a('Permissions', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li><?= Html::a('Assign User Permissions ', Yii::$app->homeUrl, ['class' => '']) ?></li>
            </ul>
        </div>
    </div>

    <div class="admin-grid-toolbar">
        <div class="row">
            <div class="col-md-6">
                <div class="permission-drop-down-role">
                    <label>Department</label>
                    <?php
                    echo Html::dropDownList('user_group', '', CommonHelper::getUserGroupList() + [CommonHelper::USER_GROUP_GUEST => 'Guest'],
                        [
                            'id'=>'dep-drop-user-group',
                            'class'=>'form-control',
                            'prompt'=>'-- Select --',
                        ]);
                    ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="permission-drop-down-role">
                    <label>Roles</label>
                    <select id="select-role-drop-down" class="form-control" name="role_id" disabled="disabled"></select>
                </div>
            </div>   
        </div>

        <div id="toolbar-new" class="btn-wrapper">
            <button type="button" disabled="disabled" class="btn btn-primary btn-lg" id="load-role-permission" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Load Permission">Load Permission</button>
            <a id="addItems" class="btn btn-success" href="<?= \yii\helpers\Url::toRoute(['/permission/access-control/create'])?>">
                <i class="fa fa-plus"></i> Create Permission
            </a>
        </div>
    </div>

    <div class="admin-permission-container" style="display: none">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title panel-title-permission"><i class="fa fa-lock"></i> Permissions List</h3>
                <div class="collapse-on-off-btn">
                    <span id="all-collapse-on">
                       <i class="fa fa-plus"></i> Expend All
                    </span>
                    <span id="all-collapse-off" style="display: none;">
                       <i class="fa fa-minus"></i> Collapse All
                    </span>
                </div>
            </div>
            <div class="panel-body">
                <div id="ajLoad">&nbsp;</div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function()
    {
        $('#all-collapse-on').click(function()
        {
            $(".panel-title a" ).removeClass( "collapsed" ).addClass( "accordion-toggle" );
            $(".panel-collapse" ).removeClass( "collapse" ).addClass( "collapse in" );
            $('#all-collapse-on').hide();
            $('#all-collapse-off').show();
        });

        $('#all-collapse-off').click(function()
        {
            $(".panel-title a" ).removeClass( "accordion-toggle" ).addClass( "accordion-toggle collapsed" );
            $(".panel-collapse" ).removeClass( "collapse in" ).addClass( "collapse" );
            $('#all-collapse-on').show();
            $('#all-collapse-off').hide();
        });

        // Load Roles
        $('#dep-drop-user-group').change(function(){
            var userGroup = $(this).val();
            loadRolesDropDown(userGroup);
        });

        $('#load-role-permission').click(function(){
            var $btn = $(this);
            var roleId = $('#select-role-drop-down').val();
            $("#ajLoad").html("");

            if(roleId == ""){
                $("#select-role-drop-down").css({"border": "1px solid red"});
            }
            else{
                $.ajax({
                    url: '<?php echo Yii::$app->request->baseUrl. '/permission/access-control/ajax-assign-permission' ?>',
                    type: 'POST',
                    data:{
                        roleId: roleId,
                        _csrf : '<?=Yii::$app->request->getCsrfToken()?>'
                    },
                    beforeSend: function(){
                        $btn.button('loading');
                        $('.admin-permission-container').hide();
                    },
                    success: function (response) {
                        $btn.button('reset');
                        $('.admin-permission-container').show();
                        $("#ajLoad").html(response);
                    }
                });
            }
        });
    });
    
    function loadRolesDropDown(userGroup) {
        $.ajax({
            url: '<?php echo Yii::$app->request->baseUrl. '/permission/access-control/ajax-roles-list' ?>',
            type: 'POST',
            data:{
                userGroup: userGroup,
                _csrf : '<?=Yii::$app->request->getCsrfToken()?>'
            },
            beforeSend: function(){
                $("#select-role-drop-down").css({"border": "1px solid #acd6ee"});
                $("#select-role-drop-down").attr('disabled', true);
                $("#load-role-permission").attr('disabled', true);
                $("#select-role-drop-down").empty();
            },
            success: function (response) {
                if(response){
                    $("#select-role-drop-down").attr('disabled', false);
                    $("#load-role-permission").attr('disabled', false);
                    $("#select-role-drop-down").empty().append(response);
                }
                else {
                    $("#select-role-drop-down").attr('disabled', true);
                    $("#load-role-permission").attr('disabled', true);
                    $("#select-role-drop-down").empty();
                }
            }
        });
    }
</script>

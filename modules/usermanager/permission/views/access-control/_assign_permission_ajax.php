<?php 
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use unlock\modules\usermanager\permission\models\AuthActions;
use unlock\modules\usermanager\permission\models\AuthRoleActionPermissionMap;
?>
<style type="text/css">
    .main-box{
        box-shadow:none;
    }
    .panel-body{
        padding: 15px 15px 0
    }
    .preload-img{
        display: none;
    }
    .main-box-body{
        margin-bottom: 0;
        padding-bottom:0
    }
    .permission-item{
        width: 48%;
        float: left;
        padding-bottom: 5px;
    }
    .controller-permission-action-table{
        overflow: hidden;
    }
    .main-box .main-box-body{
        padding: 0 20px;
    }
    .select-all-permission{
        color: #2d6fb2;
        font-weight: bold;
        padding-bottom: 5px;
    }
    .custom-permission{
        border-top: 1px solid #e1e1e1;
        color: #2d6fb2;
        font-size: 16px;
        font-weight: bold;
        margin: 10px 0;
        padding: 10px 0 0 0;
    }
</style>
 <div class="main-box-body clearfix">
     <?= Html::hiddenInput('roleId', $role_id, ['id'=>'roleId']);?>
    <?php
    if(!empty($controller)) :
        $count = 0;
        foreach($controller as $controllerItem) :
            $allActionAutomatic = AuthActions::getAllActionByControllerId($controllerItem->id);
            $allActionManually = AuthActions::getAllActionByControllerId($controllerItem->id, 'manually');
            $checkAllPermission = true;
            foreach($allActionAutomatic as $action){
                if(!AuthRoleActionPermissionMap::checkActionPermissionByRoleId($role_id, $action->id)){
                    $checkAllPermission = false;
                }
            }
            foreach($allActionManually as $action){
                if(!AuthRoleActionPermissionMap::checkActionPermissionByRoleId($role_id, $action->id)){
                    $checkAllPermission = false;
                }
            }

            $clear = 'float:right; ';
            if(($count % 2) == 0) $clear = 'float:left;clear: left;';
            ?>
            <div class="col-lg-6" style="<?= $clear ?>">
                <div id="accordion-<?= $count ?>" class="panel-group accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title" data-controller="<?= $controllerItem->id;?>">
                                <a href="#collapse-for-<?= $count ?>" data-toggle="collapse" class="accordion-toggle collapsed">
                                    <i class="fa fa-flask"></i> <?= $controllerItem->description;?>
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapse-for-<?= $count ?>">
                            <div class="panel-body">
                                <div class="main-box clearfix">
                                    <div class="main-box-body clearfix">
                                        <div class="table-responsive">
                                            <div class="select-all-permission">
                                                <input type="checkbox" <?php if($checkAllPermission){ echo 'checked'; }?> name="check-all-action" class="ajax-check-all-action" value="1">
                                                All Permissions
                                            </div>

                                            <?php if($allActionAutomatic){ ?>
                                            <div class="controller-permission-action-table automatic-controller-action">
                                                    <?php
                                                    $actionCount = 0;
                                                    foreach($allActionAutomatic as $action) :
                                                    ?>
                                                    <div class="permission-item permission-item-<?= $actionCount ?> permission-item-<?= ($actionCount % 2) ?>">
                                                        <?php
                                                        echo Html::checkbox('check-single-action', AuthRoleActionPermissionMap::checkActionPermissionByRoleId($role_id, $action->id), ['class'=>'ajax-check-action']).'&nbsp;';
                                                        echo Html::hiddenInput('controller_id', $controllerItem->id, ['class' => 'controller-id']);
                                                        echo Html::hiddenInput('action_id', $action->id, ['class' => 'action-id']);
                                                        echo $action->name
                                                        ?>
                                                        <span class='preload-img'>
                                                            <?= Html::img(Yii::getAlias('@baseUrl') . '/modules/usermanager/permission/images/rabc-preload.gif',['class'=>'spinner-img','alt'=>'']);?>
                                                        </span>
                                                    </div>
                                                    <?php $actionCount++; endforeach;?>
                                            </div>
                                            <?php } ?>

                                            <?php if($allActionManually){ ?>
                                            <div class="controller-permission-action-table manually-controller-action">
                                                <h3 class="custom-permission">Custom Permission</h3>
                                                <?php
                                                $actionCount = 0;
                                                foreach($allActionManually as $action) :
                                                ?>
                                                    <div class="permission-item permission-item-<?= $actionCount ?> permission-item-<?= ($actionCount % 2) ?>">
                                                        <?php
                                                        echo Html::checkbox('check-single-action', AuthRoleActionPermissionMap::checkActionPermissionByRoleId($role_id, $action->id), ['class'=>'ajax-check-action']).'&nbsp;';
                                                        echo Html::hiddenInput('controller_id', $controllerItem->id, ['class' => 'controller-id']);
                                                        echo Html::hiddenInput('action_id', $action->id, ['class' => 'action-id']);
                                                        echo $action->name
                                                        ?>
                                                        <span class='preload-img'>
                                                            <?= Html::img(Yii::getAlias('@baseUrl') . '/modules/usermanager/permission/images/rabc-preload.gif',['class'=>'spinner-img','alt'=>'']);?>
                                                        </span>
                                                    </div>
                                                    <?php $actionCount++; endforeach;?>
                                            </div>
                                            <?php } ?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php $count++; endforeach; endif; ?>
</div>

<script type="text/javascript">
    $(document).ready(function (){
        var roleId  = $('#roleId').val();

        $('.ajax-check-action').click(function(){
            var thisObj = $(this);
            var controllerId = $(this).parent().find('input.controller-id').val();
            var actionId = $(this).parent().find('input.action-id').val();
            var checked = 'no';
            if($(this).is(':checked')){ checked = 'yes'; }

            setPermissionActionByRole(thisObj, roleId, controllerId, actionId, checked);
        });

        // All Action
        $('.ajax-check-all-action').click(function () {
            var checked = ($(this).is(':checked') == true) ? 'yes' : 'no';

            $(this).parent().parent().find('.permission-item').each(function(index) {
                var thisObj = $(this).find('.ajax-check-action');
                var controllerId = $(this).find('.controller-id').val();
                var actionId = $(this).find('.action-id').val();

                if(checked == 'yes'){
                    $(this).find('.ajax-check-action').prop('checked', true);
                }
                else{
                    $(this).find('.ajax-check-action').prop('checked', false);
                }

                setPermissionActionByRole(thisObj, roleId, controllerId, actionId, checked);
            });
        });
    });
    
    function setPermissionActionByRole(thisObj, roleId, controllerId, actionId, checked) {
        $.ajax({
            url: '<?php echo Yii::$app->request->baseUrl. '/permission/access-control/save-action-permission' ?>',
            type: 'post',
            data:{
                roleId: roleId,
                controllerId: controllerId,
                checked: checked,
                actionId: actionId,
                _csrf : '<?=Yii::$app->request->getCsrfToken()?>'
            },
            beforeSend: function(){
                $(thisObj).parent().find('.preload-img').show('slow');
            },
            success: function (data) {
                $(thisObj).parent().find('.preload-img').hide('slow');
            }
        });
    }
</script>

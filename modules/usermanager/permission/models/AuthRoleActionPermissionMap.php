<?php

namespace unlock\modules\usermanager\permission\models;

use Yii;
use yii\db\ActiveRecord;
use unlock\modules\usermanager\permission\models\AuthActions;
use unlock\modules\usermanager\role\models\Role;
use unlock\modules\usermanager\user\models\User;
use unlock\modules\core\helpers\CommonHelper;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%auth_role_action_permission_map}}".
 *
 * @property integer $id
 * @property integer $role_id
 * @property integer $action_id
 * @property integer $created_by
 * @property string $created_at
 * @property integer $updated_by
 * @property string $updated_at
 * @property integer $status
 *
 */
class AuthRoleActionPermissionMap extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%auth_role_action_permission_map}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role_id', 'action_id'], 'required'],
            [['role_id', 'action_id', 'status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'role_id'    => 'Role ID',
            'action_id'  => 'Action ID',
            'created_by' => 'Created By',
            'created_at' => 'Created Date',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated Date',
            'status'     => 'Status',
        ];
    }

    // before save data
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
                $this->status = CommonHelper::STATUS_ACTIVE;
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }

    public static function saveRolesActionWisePermission($roleId, $actionId, $checked = null)
    {
        $model = self::find()
            ->where('role_id =:role_id', [':role_id' => $roleId])
            ->andWhere('action_id =:action_id', [':action_id' => $actionId])
            ->one();

        if ($model === null) {
            $model = new self;
            $model->role_id = $roleId;
            $model->action_id = $actionId;
        }
        else {
            if($checked == 'yes'){
                $model->status = CommonHelper::STATUS_ACTIVE;
            }
            else if($checked == 'no'){
                $model->status = CommonHelper::STATUS_INACTIVE;
            }
        }

        if(!$model->save()){
            return false;
        }

        return $model->id;
    }

    public static function getPermissionByRoleId($roleId){
        $additionalRoles = User::getLoggedInUserAdditionalRoles();

        $subtractionRoles = User::getLoggedInUserSubtractionRoles();
        $subtractionActions = self::getActionByRoleIds($subtractionRoles);
        $subtractionActions = !empty($subtractionActions) ? $subtractionActions : null;

        $model = self::find()
            ->where(['OR',
                ['role_id' => $roleId],
                ['role_id' => $additionalRoles]
            ])
            ->andFilterWhere(['NOT IN', 'action_id', $subtractionActions])
            ->andWhere('status =:status', [':status' => CommonHelper::STATUS_ACTIVE])
            ->all();

        return $model;
    }

    public static function checkActionPermission($roleId, $actionId){
        $additionalRoles = User::getLoggedInUserAdditionalRoles();

        $subtractionRoles = User::getLoggedInUserSubtractionRoles();
        $subtractionActions = self::getActionByRoleIds($subtractionRoles);
        $subtractionActions = !empty($subtractionActions) ? $subtractionActions : null;

        $model = self::find()
            ->where(['OR',
                ['role_id' => $roleId],
                ['role_id' => $additionalRoles]
            ])
            ->andFilterWhere(['NOT IN', 'action_id', $subtractionActions])
            ->andWhere('action_id =:action_id', [':action_id' => $actionId])
            ->andWhere('status =:status', [':status' => CommonHelper::STATUS_ACTIVE])
            ->one();

        return ($model !== null) ? true : false;
    }

    public static function checkActionPermissionByRoleId($roleId, $actionId){
        $model = self::find()
            ->where(['role_id' => $roleId])
            ->andWhere('action_id =:action_id', [':action_id' => $actionId])
            ->andWhere('status =:status', [':status' => CommonHelper::STATUS_ACTIVE])
            ->one();

        return ($model !== null) ? true : false;
    }

    public static function getActionByRoleIds($roleIds) {
        $list = self::find()
            ->where(['role_id' => $roleIds])
            ->all();

        $actions = ArrayHelper::map($list, 'id', 'action_id');
        return $actions;
    }

    public function getAction() {
        return $this->hasOne(AuthActions::className(), ['id' => 'action_id']);
    }

    public static function reloadUserPermission(){
        $roleId = CommonHelper::getLoggedInUserRoleId();

        //Remove previous session
        Yii::$app->session->remove('sessionActionAssignArray');

        if(empty(Yii::$app->session->get('sessionActionAssignArray'))) {

            // Get all action that assigned to user role
            $permissionArray = [];
            $assignedAction = self::getPermissionByRoleId($roleId);
            foreach ($assignedAction as $item) {
                $actionInfo = AuthActions::getActionInfoById($item->action_id);
                if(!empty($actionInfo->controller->path)){
                    $controllerName = strtolower($actionInfo->controller->path);
                    $actionName = $actionInfo->name;
                    $permissionArray[$controllerName][] = $actionName;
                }
            }

            Yii::$app->session->set('sessionActionAssignArray', $permissionArray);
        }
    }
}

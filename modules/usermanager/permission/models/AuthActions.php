<?php

namespace unlock\modules\usermanager\permission\models;

use unlock\modules\core\helpers\CommonHelper;
use Yii;
use unlock\modules\usermanager\permission\models\AuthControllers;

/**
 * This is the model class for table "{{%auth_actions}}".
 *
 * @property integer $id
 * @property integer $controller_id
 * @property string $name
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * @property integer $manually
 * @property integer $status
 */
class AuthActions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%auth_actions}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['controller_id', 'name'], 'required'],
            [['controller_id', 'status', 'manually'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'controller_id' => 'Controller',
            'name' => 'Permission Name',
            'description' => 'Description',
            'created_at' => 'Created Date',
            'updated_at' => 'Updated Date',
            'manually' => 'Manually',
            'status' => 'Status',
        ];
    }

    // before save data
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->status = CommonHelper::STATUS_ACTIVE;
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }

    public static function saveControllerAction($controllerId, $action){
        $model = self::find()
            ->where('controller_id =:controller_id', [':controller_id' => $controllerId])
            ->andWhere('name =:name', [':name' => $action])
            ->one();

        if ($model === null) {
            $model = new self;
            $model->controller_id = $controllerId;
            $model->name = $action;
            $model->description = $action;
            if(!$model->save()){
                return false;
            }
        }

        return $model->id;
    }

    public static function getActionId($controllerId, $actionName){
        $model = self::find()
                    ->where('controller_id =:controller_id', [':controller_id' =>$controllerId])
                    ->andWhere('name =:name', [':name' => $actionName])
                    ->one();
        if ($model === null) {
            return false;
        }
        return $model->id;
    }

    public static function getActionInfoById($actionId){
        $model = self::find()
            ->where('id =:id', [':id' => $actionId])
            ->one();
        if ($model === null) {
            return false;
        }
        return $model;
    }

    public function getController(){
        return $this->hasOne(AuthControllers::className(), ['id' => 'controller_id']);
    }

    public static function getAllActionByControllerId($id, $type = 'automatic'){
        if($type == 'manually'){
            return self::find()->where('controller_id = :controller_id', [':controller_id' =>$id])->andWhere('manually = :manually', [':manually' => 1])->all();
        }
        else {
            return self::find()->where('controller_id = :controller_id', [':controller_id' =>$id])->andWhere('manually != :manually', [':manually' => 1])->all();
        }
    }

}

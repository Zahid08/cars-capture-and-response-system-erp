<?php

namespace unlock\modules\usermanager\permission\models;

use Yii;
use unlock\modules\core\helpers\CommonHelper;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%auth_controllers}}".
 *
 * @property integer $id
 * @property string $path
 * @property string $module_name
 * @property string $controller_name
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * @property integer $status
 */
class AuthControllers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%auth_controllers}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['path', 'controller_name'], 'required'],
            [['status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['controller_name', 'path', 'module_name'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'path' => 'Path',
            'module_name' => 'Module Name',
            'controller_name' => 'Controller Name',
            'description' => 'Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
        ];
    }

    // before save data
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->status = CommonHelper::STATUS_ACTIVE;
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }

    public static function saveControllerInfo($controllerName, $controllerDescription){
        $path = $controllerName;
        $controllerDescription = ucwords(str_replace('/', ' - ', $controllerDescription));
        $moduleName = null;
        if (strpos($controllerName, '/') !== false) {
            $dataArray = explode('/', $controllerName);
            $moduleName = $dataArray[0];
            $controllerName = $dataArray[1];
        }

        $model = self::find()
            ->where('path = :path', [':path' => $path])
            ->one();

        if ($model === null) {
            $model = new self;
            $model->path = $path;
            $model->module_name = $moduleName;
            $model->controller_name = $controllerName;
            $model->description = $controllerDescription;
            if(!$model->save()){
                return false;
            }
        }
        return $model->id;
    }

    public static function getControllerId($controllerName){
        $model = self::find()
            ->where('path = :path', [':path' => $controllerName])
            ->one();

        if ($model === null) {
            return false;
        }

        return $model->id;
    }

    public static function getControllerList(){
        return self::find()->where(['status' => CommonHelper::STATUS_ACTIVE])->orderBy('description')->all();
    }

    public static function dropDownList(){
        $listData = self::find()
            ->where(['status' => CommonHelper::STATUS_ACTIVE])
            ->orderBy(['description' => SORT_ASC])
            ->all();

        return ArrayHelper::map($listData, 'id', 'description');
    }
}

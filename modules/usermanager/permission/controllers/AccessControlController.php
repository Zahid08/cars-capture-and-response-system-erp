<?php

namespace unlock\modules\usermanager\permission\controllers;

use unlock\modules\core\helpers\CommonHelper;
use Yii;
use unlock\modules\usermanager\role\models\Role;
use unlock\modules\usermanager\permission\models\AuthControllers;
use unlock\modules\usermanager\permission\models\AuthActions;
use unlock\modules\usermanager\permission\models\AuthRoleActionPermissionMap;
use yii\db\Query;
use yii\db\Connection;
use yii\db\ActiveRecord;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;

/** @#@ Role Based Access Control @#@
 * AccessControlController implements the CRUD actions for AccessControl model.
 */
class AccessControlController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return  [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => Yii::$app->DynamicAccessRules->customAccess('permission/accesscontrol'),
        ];
    }

    public function actionCreate()
    {
        $model = new AuthActions();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->description = $model->name;
            $model->manually = 1;
            $model->status = 1;

            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Record successfully saved.'));
                return $this->redirect(['assign-permission']);
            }
            else {
                Yii::$app->session->setFlash('error', Html::errorSummary($model));
            }
        }

        return $this->render('add_permission', [
            'model' => $model,
        ]);
    }

    /*
    * External Actions
    * "External Action List" is very important. Those are use user role wise permissions. Please do not remove it.
    * External Action List: allow-previous-month, allow-previous-financial-year
    * */
    
    // reload permission
    public function actionReloadPermission()
    {
        $controllerAction = []; // all controller and action name
        $controllerInfo = []; // Controller Description

        $dirName =  Yii::getAlias('@backend') . '/../';
        $controllerFileList = $this->getDirContents($dirName, '/Controller.php$/');
        if(count($controllerFileList)){
            $applyPermissionControllerList = array();
            foreach ($controllerFileList as $file) {
                // Read file
                $fileData = fopen($file, "r");
                if ($fileData)
                {
                    while (($fileLine = fgets($fileData)) !== false)
                    {
                        if(preg_match('/Yii(.*)app->DynamicAccessRules->customAccess\(/', $fileLine, $controllerInfo)){
                            $applyPermissionControllerList[] = $file;
                        }
                    }
                }

                fclose($fileData);
            }

            if(count($applyPermissionControllerList) > 0){
                foreach ($applyPermissionControllerList as $file) {
                    $fileName = StringHelper::basename($file);
                    $moduleName = StringHelper::basename(realpath(dirname($file) . '/..'));
                    $controllerName = str_replace('Controller.php','',$fileName);
                    $controllerName = $moduleName . '/' . $controllerName;
                    $controllerName = trim(strtolower($controllerName));
                    $controllerInfo[$controllerName] = ucwords($controllerName);

                    // Read file
                    $fileData = fopen($file, "r");
                    if ($fileData)
                    {
                        while (($fileLine = fgets($fileData)) !== false)
                        {
                            if (preg_match('/public\s+function\s+action(.*?)\(/', $fileLine, $methodName)){
                                if (strlen($methodName[1]) > 2) {
                                    $methodNameProcess = $methodName[1];

                                    $methodNameAfterProcess = $this->checkUpperLower($methodNameProcess);

                                    if(isset($controllerName)){
                                        $controllerAction[$controllerName][] = trim(strtolower($methodNameAfterProcess));
                                    }
                                }
                            }
                            else if (preg_match('/External Action List:(.*)/im', $fileLine, $externalMethods)){
                                $externalMethods = explode(',', $externalMethods[1]);
                                if($externalMethods){
                                    foreach ($externalMethods as $externalMethod) {
                                        $methodNameAfterProcess = $this->checkUpperLower($externalMethod);

                                        if(isset($controllerName)){
                                            $controllerAction[$controllerName][] = trim(strtolower($methodNameAfterProcess));
                                        }
                                    }
                                }

                            }
                        }
                    }

                    fclose($fileData);
                }
            }
        }

		if(!empty($controllerAction)){
            $this->prcessionIntoDb($controllerAction, $controllerInfo);
        }

        $this->redirect(Yii::$app->request->referrer);
    }

    /**
	* Controller and action processing in to DB
    */
	protected function prcessionIntoDb($controllerAction, $controllerInfo)
	{
	    $controllerIds = [];
        $controllerActionIds = [];

	    // Save Controller Name
        foreach ($controllerInfo as $controllerKey => $controllerValue) {
            $controllerId = AuthControllers::saveControllerInfo($controllerKey, $controllerValue);
            array_push($controllerIds, $controllerId);
        }

        // Save Controller Action
        foreach ($controllerAction as $controllerActionKey => $controllerActionValue) {
            $controllerId = AuthControllers::getControllerId($controllerActionKey);
            foreach ($controllerActionValue as $actionItem) {
                if (preg_match("#^[a-zA-Z0-9-_]+$#", $actionItem)) {
                    $controllerActionId = AuthActions::saveControllerAction($controllerId, $actionItem);
                    array_push($controllerActionIds, $controllerActionId);
                }
            }
        }

        //Save Super Admin Permission
        $superAdminRoleId = CommonHelper::ROLE_SUPER_ADMIN_PK;
        foreach ($controllerAction as $controllerActionKey => $controllerActionValue) {
            $controllerId = AuthControllers::getControllerId($controllerActionKey);
            foreach ($controllerActionValue as $actionItem) {
                $actionId = AuthActions::getActionId($controllerId, $actionItem);
                AuthRoleActionPermissionMap::saveRolesActionWisePermission($superAdminRoleId, $actionId);
            }
        }

        // Remove Unnecessary Controller
        AuthControllers::deleteAll(['NOT IN', 'id', $controllerIds]);

        // Remove Unnecessary Controller Action
        AuthActions::deleteAll(['NOT IN', 'controller_id', $controllerIds]);
        AuthActions::deleteAll(['AND', 'manually!=1', ['NOT IN', 'id', $controllerActionIds]]);

        // Remove Unnecessary Permission
        $actionIds = AuthActions::find()->select('id')->where('manually!=1');
        $actionIds->createCommand()->getRawSql();
        $actionIds = ArrayHelper::map($actionIds, 'id', 'id');
        AuthRoleActionPermissionMap::deleteAll(['NOT IN', 'action_id', $actionIds]);
	}
	/*
	* Assign Permission
	*/
	public function actionAssignPermission()
	{
		// All active Controller		
		return $this->render('assign_permission',[]);
	}

	/**
	* ajax-assign-permission
	*/
	public function actionAjaxAssignPermission()
	{
        ob_start();
        $post = Yii::$app->request->post();
	    $roleId = $post['roleId'];

		// All active Controller
		$controller = AuthControllers::getControllerList();
        echo $this->renderAjax('_assign_permission_ajax',[
			'controller' => $controller,
			'role_id' => $roleId,
        ]);
	}

	//ajax-roles-list
    public function actionAjaxRolesList()
    {
        $post = Yii::$app->request->post();
        $userGroup = $post['userGroup'];

        if(empty($userGroup)){
            return '';
        }

        $html = '';
        $roles = Role::rolesDropDownList($userGroup);
        if($roles){
            $html .= '<option value="">-- Select --</option>';
            foreach ($roles as $key => $value) {
                $html .= '<option value="'.$key.'">'.$value.'</option>';
            }
        }

        return $html;
    }

	/**
	* Save Controller action and role 
	*/
	public function actionSaveActionPermission()
	{
		if (Yii::$app->request->isAjax)
        {
		    $post = Yii::$app->request->post();
            $roleId = $post['roleId'];
            $actionId = $post['actionId'];
            $checked = $post['checked'];
            AuthRoleActionPermissionMap::saveRolesActionWisePermission($roleId, $actionId, $checked);
		}
	}

	/*
	* Remove Revious Access role session 
	*/
	public function actionReloadPermissionSession() {
        AuthRoleActionPermissionMap::reloadUserPermission();
		$this->redirect(Yii::$app->request->referrer);
	}

    public function getDirContents($dir, $filter = '', &$results = array()){
        $files = scandir($dir);

        if(isset($files) && count($files) > 0){
            foreach($files as $key => $value){
                $path = realpath($dir . DIRECTORY_SEPARATOR . $value);

                if(!is_dir($path)) {
                    if(empty($filter) || preg_match($filter, $path)){
                        if (strlen($value) > 14 ) {
                            if (strpos($path, '\\vendor\\') === false) {
                                $results[] = $path;
                            }
                        }
                    }
                }
                elseif($value != "." && $value != "..") {
                    $this->getDirContents($path, $filter, $results);
                }
            }
        }

        return $results;
    }

    /*
	 *
	 */
    public function checkUpperLower($str)
    {
        $st = '';
        $chr =str_split($str);
        foreach($chr as $k=>$val)
        {
            if($k!=0)
            {
                if(preg_match_all('/[A-Z]/', $val, $matches, PREG_OFFSET_CAPTURE)) $st .= '-'.$val;
                else $st .= $val;
            }else if($k==0) $st .=$val;
        }
        return $st;
    }

}

<?php

namespace unlock\modules\usermanager\permission;

/**
 * Class PermissionModule
 *
 * @package yii2mod\rabc
 */
class PermissionModule extends \yii\base\Module
{
    /**
     * @var string the namespace that controller classes are in
     */
    public $controllerNamespace = 'unlock\modules\usermanager\permission\controllers';

    /**
     * @var string the namespace that view file are in
     */
    public $viewNamespace = '@unlock/modules/usermanager/permission/views/';
}

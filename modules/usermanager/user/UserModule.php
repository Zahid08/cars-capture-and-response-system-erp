<?php

namespace unlock\modules\usermanager\user;

/**
 * Class UserModule
 */
class UserModule extends \yii\base\Module
{
    /**
     * @var string the namespace that controller classes are in
     */
    public $controllerNamespace = 'unlock\modules\usermanager\user\controllers';

    /**
    * @var string the namespace that view file are in
    */
    public $viewNamespace = '@unlock/modules/usermanager/user/views/';

    public $defaultRoute = 'default';

    /**
     * @var bool If true, users can enter an email. This is automatically set to true if $requireEmail = true
     */
    public $useEmail = true;

    /**
     * @var bool If true, users can enter a username. This is automatically set to true if $requireUsername = true
     */
    public $useUsername = true;

    public function createController($route) {
        // check valid routes
        $validRoutes  = [$this->defaultRoute, "admin", "auto-login",'password-reset'];
        $isValidRoute = false;
        foreach ($validRoutes as $validRoute) {
            if (strpos($route, $validRoute) === 0) {
                $isValidRoute = true;
                break;
            }
        }

        return (empty($route) or $isValidRoute)
            ? parent::createController($route)
            : parent::createController("{$this->defaultRoute}/{$route}");
    }

}

User
=======================================================================================================================

User for every Site

Migration
-----------------------------------------------------------------------------------------------------------------------

**Create Database**

php yii migrate/up --migrationPath=@unlock/modules/usermanager/user/migrations

**Delete Database**

php yii migrate/down --migrationPath=@unlock/modules/usermanager/user/migrations

Configuration
-----------------------------------------------------------------------------------------------------------------------

**Module Setup**

common/config/main.php

'modules' => [
    'user' => [
        'class' => 'unlock\modules\usermanager\user\UserModule',
    ],
],


Usage
-----------------------------------------------------------------------------------------------------------------------
**Left Admin Menu**
// Users
[
    'label' => '<i class="fa fa-users"></i> <span>Users</span>',
    'url' => ['#'],
    'visible'=> Yii::$app->user->checkAccess('user/admin', 'index') ||
                Yii::$app->user->checkAccess('role/role', 'index') ||
                Yii::$app->user->checkAccess('permission/accesscontrol', 'assign-permission'),
    'template' => '<a class="dropdown-toggle" href="{url}" >{label}</a>',
    'items' => [
        [
            'label' => '<span>Users</span>',
            'url' => ['/user/admin/index'],
            'visible'=> Yii::$app->user->checkAccess('user/admin', 'index'),
            'active' => LeftMenu::isActive(['user/admin/']),
        ],
        [
            'label' => '<span>Roles</span>',
            'url' => ['/role/role/index'],
            'visible'=> Yii::$app->user->checkAccess('role/role', 'index'),
            'active' => LeftMenu::isActive(['role/role/']),
        ],
        [
            'label' => '<span>Permissions</span>',
            'url' => ['/permission/access-control/assign-permission'],
            'visible'=> Yii::$app->user->checkAccess('permission/accesscontrol', 'assign-permission'),
            'active' => LeftMenu::isActive(['permission/access-control/']),
        ],
    ],
],

URLs
-----------------------------------------------------------------------------------------------------------------------
Login: /user/login
Logout: /user/logout
Change Password: /user/account
Profile: /user/view-profile
Forgot password: /user/forgot-password


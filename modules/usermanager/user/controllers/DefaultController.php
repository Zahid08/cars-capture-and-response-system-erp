<?php

namespace unlock\modules\usermanager\user\controllers;

use unlock\modules\usermanager\user\models\ResetPasswordForm;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\base\Exception;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use unlock\modules\core\helpers\Collection;
use unlock\modules\core\helpers\FileHelper;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\actions\ExportCsvAction;
use unlock\modules\core\actions\ExportPdfAction;
use unlock\modules\usermanager\user\models\User;
use unlock\modules\usermanager\user\models\UserSearch;
use unlock\modules\usermanager\user\models\Account;
use unlock\modules\usermanager\user\models\Profile;
use unlock\modules\usermanager\user\models\LoginForm;
use unlock\modules\usermanager\user\models\SignupForm;
use unlock\modules\usermanager\user\models\PasswordResetRequestForm;
use unlock\modules\usermanager\permission\models\AuthRoleActionPermissionMap;
use unlock\modules\core\helpers\SmsApi;

/**
 * DefaultController implements the CRUD actions for User model.
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => Yii::$app->DynamicAccessRules->customAccess('user/default'),
        ];
    }

    /**
     * Display index - debug page, login page, or account page
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(["/user/login"]);
        }
        else {
            return $this->redirect(["/user/account"]);
        }
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {

        if(preg_match('@frontend@', Yii::$app->request->BaseUrl)){
            $this->layout = "@app/web/themes/connecterp/views/layouts/login-layout.php";
        }
        else{
            $this->layout = "@app/web/themes/connecterp/views/layouts/login-layout.php";
        }

        if (Yii::$app->user->isGuest) {
            if(preg_match('`backend`', Yii::$app->request->url) || preg_match('`frontend`', Yii::$app->request->url)){
                $url = Yii::getAlias('@baseUrl');
                //return $this->redirect($url);
            }
        }
        else {
            $userGroup = Yii::$app->user->identity->user_group;
            if($userGroup == CommonHelper::USER_GROUP_REGISTERED){
                $url = Yii::getAlias('@baseUrl') . '/frontend/web/site/index';
                return $this->redirect($url);
            }
            else {
                return $this->redirect(Url::toRoute('/site/index'));
            }
        }

        $model = new LoginForm();
        $post = Yii::$app->request->post();
        if ($model->load($post)) {
            if($model->validate()){
                $userGroup = $model->user->user_group;
                if($userGroup == CommonHelper::USER_GROUP_REGISTERED){
                    $token = $post['LoginForm']['username'] . '::' . $post['LoginForm']['password'];
                    $token = base64_encode($token);

                    $url = Yii::getAlias('@baseUrl') . '/frontend/web/frontend-login/user-login?token='.$token;
                    return $this->redirect($url);
                }

                if($model->login()){
                    AuthRoleActionPermissionMap::reloadUserPermission();
                    return $this->goBack();
                }
            }
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionRegister()
    {
        if(preg_match('@frontend@', Yii::$app->request->BaseUrl)){
            $this->layout = "@app/themes/unlock_site/views/layouts/login.php";
        }
        else{
            $this->layout = "@app/themes/unlock_admin/views/layouts/login.php";
        }

        $model = new SignupForm();
        $model->scenario = $model::SCENARIO_REGISTER;

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            try {
                $post = Yii::$app->request->post();
                $model->password = $model->newPassword;

                if (!$model->validate()) {
                    throw new Exception(Yii::t('app', Html::errorSummary($model)));
                }

                $verification_code = CommonHelper::generatePIN(CommonHelper::MOBILE_VERIFICATION_CODE_LENGTH);

                $session = Yii::$app->session;

                if (isset($session['userData'])){
                    unset($_SESSION['userData']);
                }

                $_SESSION['userData'] = $post;
                $_SESSION['userData']['method'] = 'register';
                $_SESSION['userData']['verification_code'] = $verification_code;

                if(CommonHelper::isValidateEmail($post['SignupForm']['username'])){
                    $_SESSION['userData']['verification_type'] = 'email';

                    // Send Register Email
                    $model->sendRegisterEmail($post['SignupForm']['username'], $verification_code);
                }
                else{
                    $_SESSION['userData']['verification_type'] = 'mobile';

                    // Send Email
                    $smsText = $verification_code . " is your mobile verification code.";
                    $return = SmsApi::sendSms($post['SignupForm']['username'], $smsText);
                }

                return $this->redirect(Url::toRoute(['/user/verification-code']) );
            }
            catch (Exception $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('register', [
            'model' => $model,
        ]);
    }

    public function completeRegistration()
    {
        $session = Yii::$app->session;

        $model = new SignupForm();
        $model->load($session['userData']);
        $model->password = $session['userData']['SignupForm']['newPassword'];

        if(isset($session['userData']['verification_type']) && $session['userData']['verification_type'] == 'mobile'){
            $model->mobile = $model->username;
        }
        else{
            $model->email = $model->username;
        }

        if (!$model->validate()) {
            throw new Exception(Yii::t('app', Html::errorSummary($model)));
        }

        if ($user = $model->signup()) {
            $userModel = $this->findUserModel($user->id);

            if (isset($session['userData'])){
                unset($_SESSION['userData']);
            }

            $userGroup = $userModel->user_group;
            if($userGroup == CommonHelper::USER_GROUP_REGISTERED){
                $token = $userModel->username . '::' . $model->password;
                $token = base64_encode($token);

                $url = Yii::getAlias('@baseUrl') . '/frontend/web/frontend-login/user-login?token='.$token;
                return $this->redirect($url);
            }

            if (Yii::$app->user->login($userModel)) {
                AuthRoleActionPermissionMap::reloadUserPermission();
                return $this->redirect(Url::toRoute(['/organization/enlistment/update', 'id' => CommonHelper::getLoggedInUserOrgId()]));
            }
        }
    }

    public function actionVerificationCode()
    {
        if(preg_match('@frontend@', Yii::$app->request->BaseUrl)){
            $this->layout = "@app/themes/unlock_site/views/layouts/login.php";
        }
        else{
            $this->layout = "@app/themes/unlock_admin/views/layouts/login.php";
        }

        $model = new SignupForm();
        $model->scenario = $model::SCENARIO_VERIFICATION_CODE;
        $model->verificationCode = null;

        $session = Yii::$app->session;

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }


        if ($model->load(Yii::$app->request->post())) {
            try {
                if($session['userData']['verification_code'] != $model->verificationCode){
                    throw new Exception('Invalid Verification Code');
                }

                if($session['userData']['method'] == 'register'){
                    $this->completeRegistration();
                }
                else if($session['userData']['method'] == 'forgot-password'){
                    return $this->redirect(Url::toRoute(['/user/reset-password', 'token' => $session['userData']['token']]) );
                }
            }
            catch (Exception $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $instructionText = 'We have sent a 6-digit PIN code to your email. Enter it below';
        if(isset($session['userData']['verification_type']) && $session['userData']['verification_type'] == 'mobile'){
            $instructionText = 'We have sent a 6-digit PIN code to your mobile phone. Enter it below';
        }

        $headerTitle = '';
        $buttonText = '';
        if($session['userData']['method'] == 'register'){
            $headerTitle = 'Create an account';
            $buttonText = 'Create Account 2/2';
        }
        else if($session['userData']['method'] == 'forgot-password'){
            $headerTitle = 'Reset Password';
            $buttonText = 'Next';
        }

        return $this->render('verification-code', [
            'model' => $model,
            'headerTitle' => $headerTitle,
            'instructionText' => $instructionText,
            'buttonText' => $buttonText,
        ]);
    }

    public function actionResendVerificationCode()
    {
        $session = Yii::$app->session;

        $verification_code = CommonHelper::generatePIN(CommonHelper::MOBILE_VERIFICATION_CODE_LENGTH);

        if($session['userData']['method'] == 'register'){
            $model = new SignupForm();
            $model->scenario = $model::SCENARIO_REGISTER;

            if(isset($session['userData']['verification_type']) && $session['userData']['verification_type'] == 'mobile'){
                // Send SMS
                $smsText = $verification_code . " is your mobile verification code.";
                $return = SmsApi::sendSms($session['userData']['SignupForm']['username'], $smsText);
            }
            else{
                $model->sendRegisterEmail($session['userData']['SignupForm']['username'], $verification_code);
            }
        }
        else if($session['userData']['method'] == 'forgot-password'){
            $model = new PasswordResetRequestForm();

            if(isset($session['userData']['verification_type']) && $session['userData']['verification_type'] == 'mobile'){
                // Send SMS
                $smsText = $verification_code . " is your mobile verification code.";
                $return = SmsApi::sendSms($session['userData']['PasswordResetRequestForm']['username'], $smsText);
            }
            else{
                // Send Register Email
                $model->sendEmail($session['userData']['PasswordResetRequestForm']['username'], $verification_code);
            }
        }

        $_SESSION['userData']['verification_code'] = $verification_code;
        return $this->redirect(Url::toRoute(['/user/verification-code']) );
    }

    public function actionAccount()
    {
        $user = Yii::$app->user->identity;
        $user->setScenario("account");

        if (Yii::$app->request->isAjax && $user->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($user);
        }

        if ($user->load(Yii::$app->request->post()) && $user->validate()) {

            if(isset($user->newPassword)){
                $user->setPassword($user->newPassword);
                $user->generateAuthKey();
            }

            if($user->force_password_change == 1){
                $user->force_password_change = 0;
            }

            if (!$user->save(false)) {
                Yii::$app->session->setFlash('error', Html::errorSummary($user));
            }

            Yii::$app->session->setFlash("success", Yii::t("app", "Account updated successfully"));
            return $this->refresh();
        }

        return $this->render('account', [
            'user' => $user,
        ]);
    }


    public function actionProfile()
    {
        $user = Yii::$app->user->identity;
        $user->setScenario("profile");

        if (Yii::$app->request->isAjax && $user->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($user);
        }

        if ($user->load(Yii::$app->request->post()) && $user->validate()) {
            if (!$user->save(false)) {
                Yii::$app->session->setFlash('error', Html::errorSummary($user));
            }

            Yii::$app->session->setFlash("success", Yii::t("app", "Profile updated successfully"));
            return $this->redirect(['view-profile']);
        }

        return $this->render('profile', [
            'user' => $user,
        ]);
    }

    /**
     * Profile
     */
    public function actionViewProfile()
    {
        $profile = Yii::$app->user->identity;

        return $this->render('view-profile', ['profile' => $profile]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionForgotPassword()
    {
        if(preg_match('@frontend@', Yii::$app->request->BaseUrl)){
            $this->layout = "@app/themes/unlock_site/views/layouts/login.php";
        }
        else{
            $this->layout = "@app/themes/unlock_admin/views/layouts/login.php";
        }

        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            try {
                $post = Yii::$app->request->post();
                $session = Yii::$app->session;

                $verification_code = CommonHelper::generatePIN(CommonHelper::MOBILE_VERIFICATION_CODE_LENGTH);

                /* @var $user User */
                $user = User::findOne([
                    'status' => User::STATUS_ACTIVE,
                    'username' => $post['PasswordResetRequestForm']['username'],
                ]);

                if (!$user) {
                    throw new Exception('User not exits');
                }

                $user->generatePasswordResetToken();
                if (!$user->save()) {
                    throw new Exception(Html::errorSummary($user));
                }

                if (isset($session['userData'])){
                    unset($_SESSION['userData']);
                }

                $_SESSION['userData'] = $post;
                $_SESSION['userData']['method'] = 'forgot-password';
                $_SESSION['userData']['verification_code'] = $verification_code;
                $_SESSION['userData']['token'] = $user->password_reset_token;

                if(CommonHelper::isValidateEmail($post['PasswordResetRequestForm']['username'])){
                    $_SESSION['userData']['verification_type'] = 'email';

                    // Send Register Email
                    $model->sendEmail($post['PasswordResetRequestForm']['username'], $verification_code);
                }
                else{
                    $_SESSION['userData']['verification_type'] = 'mobile';

                    // Send Email
                    $smsText = $verification_code . " is your mobile verification code.";
                    $return = SmsApi::sendSms($post['PasswordResetRequestForm']['username'], $smsText);
                }

                return $this->redirect(Url::toRoute(['/user/verification-code']) );
            }
            catch (Exception $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('forgot-password', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        if(preg_match('@frontend@', Yii::$app->request->BaseUrl)){
            $this->layout = "@app/themes/unlock_site/views/layouts/login.php";
        }
        else{
            $this->layout = "@app/themes/unlock_admin/views/layouts/login.php";
        }

        try {
            $model = new ResetPasswordForm($token);
        }
        catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'Password changed successfully.');

            $session = Yii::$app->session;
            if (isset($session['userData'])){
                unset($_SESSION['userData']);
            }

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }


    protected function findUserModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}

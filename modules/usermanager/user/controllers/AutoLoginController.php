<?php
namespace unlock\modules\usermanager\user\controllers;

use Yii;
use yii\base\Exception;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use unlock\modules\usermanager\user\models\User;
use unlock\modules\usermanager\permission\models\AuthRoleActionPermissionMap;

/**
 * AutoLoginController
 */
class AutoLoginController extends Controller
{
    public function behaviors()
    {
        return  [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            /*'access' => Yii::$app->DynamicAccessRules->customAccess('user/autologin'),*/
        ];
    }

    // frontend/web/user/auto-login/user-login-frontend
    public function actionUserLoginFrontend($token)
    {
        try {
            $token = base64_decode($token);
            $tokenArray = explode('::', $token);

            $username = isset($tokenArray[0]) ? $tokenArray[0] : '';
            $password = isset($tokenArray[1]) ? $tokenArray[1] : '';

            if(empty($username) && empty($password)){
                throw new Exception(Yii::t('app', 'Login Failed! Invalid Username or Password, Please Try Again.'));
            }

            $user = User::find()
                ->where(['username' => $username])
                ->orWhere(['email' => $username])
                ->andWhere(['auth_key' => $password])
                ->one();

            if(!$user){
                throw new Exception(Yii::t('app', 'Login Failed! Invalid Username or Password, Please Try Again.'));
            }

            // Logout
            // Yii::$app->user->logout();
            Yii::$app->getSession()->destroy();

            // Login
            Yii::$app->user->login($user);
            AuthRoleActionPermissionMap::reloadUserPermission();

            return $this->redirect(['/site/index']);
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            Yii::$app->session->setFlash('error', $message);

            $url = Yii::getAlias('@baseUrl');
            return $this->redirect($url);
        }

    }

    // backend/web/user/auto-login/user-login-backend
    public function actionUserLoginBackend($token)
    {
        try {
            $token = base64_decode($token);
            $tokenArray = explode('::', $token);

            $username = isset($tokenArray[0]) ? $tokenArray[0] : '';
            $password = isset($tokenArray[1]) ? $tokenArray[1] : '';

            if(empty($username) && empty($password)){
                throw new Exception(Yii::t('app', 'Login Failed! Invalid Username or Password, Please Try Again.'));
            }

            $user = User::find()
                ->where(['username' => $username])
                ->orWhere(['email' => $username])
                ->andWhere(['auth_key' => $password])
                ->one();

            if(!$user){
                throw new Exception(Yii::t('app', 'Login Failed! Invalid Username or Password, Please Try Again.'));
            }

            // Logout
            // Yii::$app->user->logout();
            Yii::$app->getSession()->destroy();

            // Login
            Yii::$app->user->login($user);
            AuthRoleActionPermissionMap::reloadUserPermission();

            return $this->redirect(['/site/index']);
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            Yii::$app->session->setFlash('error', $message);

            $url = Yii::getAlias('@baseUrl');
            return $this->redirect($url);
        }

    }

}

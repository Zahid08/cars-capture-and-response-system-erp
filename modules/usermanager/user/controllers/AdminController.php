<?php

namespace unlock\modules\usermanager\user\controllers;

use Yii;
use yii\helpers\Html;
use yii\base\Exception;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use unlock\modules\usermanager\user\models\User;
use unlock\modules\usermanager\user\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use unlock\modules\core\helpers\Collection;
use unlock\modules\core\helpers\FileHelper;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\actions\ExportCsvAction;
use unlock\modules\core\actions\ExportPdfAction;

/**
 * AdminController implements the CRUD actions for User model.
 */
class AdminController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => Yii::$app->DynamicAccessRules->customAccess('user/admin'),
        ];
    }
    
    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        Collection::setData($dataProvider);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
        $model->scenario = $model::SCENARIO_ADMIN_CREATE_USER;

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $this->saveData($model);
        }

        return $this->render('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = $model::SCENARIO_ADMIN_UPDATE_USER;

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $this->saveData($model);
        }

        return $this->render('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        try {
            if (!$model->delete()) {

                $appendText = ' (Deleted ' . date('d M Y g:i A') .')';
                $userName = base64_encode($model->username . $appendText);

                $email = date('d-m-Y.H-i-s'). '-' . $model->email;

                $model->username = $userName;
                $model->email = $email;
                $model->status = $model::STATUS_DELETED;

                if (!$model->save()) {
                    throw new Exception(Yii::t('app', 'The record cannot be deleted.'));
                }
            }

            // Delete Image:
            // FileHelper::removeFile($model->image, 'sanction');
            Yii::$app->session->setFlash('success', Yii::t('app', 'The record have been successfully deleted.'));
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            if($e->getName() == CommonHelper::INTEGRITY_CONSTRAINT_VIOLATION){
                $message = "This record has relational data. You must delete the following data first";
                $message .= "<ul>";
                $message .= "<li>Example => Example List</li>";
                $message .= "</ul>";
            }
            Yii::$app->session->setFlash('error', $message);
        }

        return $this->redirect(['index']);
    }

    /**
    * Save Data.
    * @param object $model
    * @return mixed
    */
    private function saveData($model){
        if (isset($model)) {
            //$transaction = Yii::$app->db->beginTransaction();
            try {
                $post = Yii::$app->request->post();

                // Process Image and File
                //$this->uploadFile($model);

                if(isset($model->password)){
                    $model->setPassword($model->password);
                    $model->generateAuthKey();
                }

                if(isset($model->additional_roles) && is_array($model->additional_roles)){
                    $model->additional_roles = implode(',', $model->additional_roles);
                }

                if(isset($model->subtraction_roles) && is_array($model->subtraction_roles)){
                    $model->subtraction_roles = implode(',', $model->subtraction_roles);
                }

                // User Group
                if(empty($model->user_group)){
                    $model->user_group = CommonHelper::getLoggedInUserGroup();
                }

                if (!$model->save()) {
                    throw new Exception(Yii::t('app', Html::errorSummary($model)));
                }

                //$transaction->commit();
                Yii::$app->session->setFlash('success', Yii::t('app', 'Record successfully saved.'));
                $this->_redirect($model);
            }
            catch (Exception $e) {
                //$transaction->rollBack();
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
    }

    /**
    * Upload Image.
    * @param object $model
    */
    public function uploadFile(&$model){
        $post = Yii::$app->request->post();

        $prevImage = isset($model->oldAttributes['image']) ? $model->oldAttributes['image'] : '';
        $image = UploadedFile::getInstance($model, 'image');
        if($image) {
            $imagePath = FileHelper::getUniqueFilenameWithPath($image->name, 'user');
            $model->image = FileHelper::getFilename($imagePath);
            if($image->saveAs($imagePath)){
                if(!empty($prevImage) && $model->picture != $prevImage){
                    FileHelper::removeFile($prevImage, 'sample');
                }
            }
            else{
                $model->image = $prevImage;
                throw new Exception($model->getAttributeLabel('image') . ' did not upload.');
            }
        }
        elseif (isset($post['FileDelete']['image'])) {
            FileHelper::removeFile($model->image, 'user');
            $model->image = '';
        }
        else {
            $model->image = $prevImage;
        }
    }

    public  function actionChangeSalesStatus($id, $sales_status){
        $model =$this->findModel($id);
        $model->sales_status =$sales_status;
        if (!$model->save()) {
            throw new Exception(Yii::t('app', Html::errorSummary($model)));
        }
    }

    private function _redirect($model){
        $action = Yii::$app->request->post('action', 'save');
        switch ($action) {
            case 'save':
                return $this->redirect(['index']);
                break;
            case 'apply':
                return $this->redirect(['update', 'id' => $model->id]);
                break;
            case 'save2new':
                return $this->redirect(['create']);
                break;
            default:
                return $this->redirect(['index']);
        }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}

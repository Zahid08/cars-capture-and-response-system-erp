<?php

namespace unlock\modules\usermanager\user\controllers;

use Yii;
use yii\web\Controller;
use unlock\modules\usermanager\user\models\PasswordResetByUserNameAndEmail;
use unlock\modules\usermanager\user\models\User;
use unlock\modules\usermanager\user\models\ResetPassword;

class PasswordResetController extends Controller {

    public function actionRequestPasswordReset() {
        $this->layout = "@app/web/themes/connecterp/views/layouts/login-layout.php";

        $model = new PasswordResetByUserNameAndEmail();
        
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $userData = PasswordResetByUserNameAndEmail::getUserDataByNameOrEmail($model->usernameemail);
            $userData->auth_key = Yii::$app->security->generateRandomString();
            $userData->password_reset_token = rand(111111111, 999999999) . Yii::$app->security->generateRandomString() . $userData->id;
            $userData->save(false);
            if ($model->sendEmail($userData)) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('/default/request_password_reset', [
                    'model' => $model,
        ]);
    }

    public function actionReset($token = NULL) {
        if (!empty($token)) {
            $model = User::find()->where('password_reset_token =:password_reset_token', [':password_reset_token' => $token])->one();
            if ($model) {
                $this->layout = "@app/web/themes/connecterp/views/layouts/login-layout.php";
                $re_pasa_mod = new ResetPassword();

                if (Yii::$app->request->isAjax && $re_pasa_mod->load(Yii::$app->request->post())) {
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return \yii\widgets\ActiveForm::validate($re_pasa_mod);
                }
                if ($re_pasa_mod->load(Yii::$app->request->post()) && $re_pasa_mod->validate()) {

                    $model->setPassword($re_pasa_mod->password);
                    if ($model->save()) {
                        Yii::$app->session->setFlash('success', 'Password has been changed successfully');
                        return $this->goHome();
                    }
                }

                return $this->render('/default/password_reset', [
                            'model' => $model,
                            're_pasa_mod' => $re_pasa_mod,
                ]);
            } else {
                Yii::$app->session->setFlash('error', 'Url is not valid. Please try again.');
                return $this->goHome();
            }
        } else {
            Yii::$app->session->setFlash('error', 'Url is not valid. Please try again.');
            return $this->goHome();
        }
    }

}

<?php

namespace unlock\modules\usermanager\user\components;

use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\helpers\PermissionHelper;
use unlock\modules\usermanager\permission\models\AuthActions;
use unlock\modules\usermanager\permission\models\AuthControllers;
use unlock\modules\usermanager\permission\models\AuthRoleActionPermissionMap;
use unlock\modules\usermanager\role\models\Role;
use Yii;

/**
 * User component
 */
class User extends \yii\web\User
{
    /**
     * @inheritdoc
     */
    public $identityClass = 'unlock\modules\usermanager\user\models\User';

    /**
     * @inheritdoc
     */
    public $enableAutoLogin = true;

    /**
     * @inheritdoc
     */
    public $loginUrl = ["/user/login"];

    /**
     * @inheritdoc
     */
    public function getIsGuest()
    {
        /** @var \unlock\modules\usermanager\user\models\User $user */

        // check if user is banned. if so, log user out and redirect home
        // https://github.com/amnah/yii2-user/issues/99
        $user = $this->getIdentity();
        if ($user && $user->status == 0) {
            $this->logout();
            Yii::$app->getResponse()->redirect(['/'])->send();
        }

        return $user === null;
    }

    /**
     * Check if user is logged in
     * @return bool
     */
    public function getIsLoggedIn()
    {
        return !$this->getIsGuest();
    }

    /**
     * @inheritdoc
     */
    public function afterLogin($identity, $cookieBased, $duration)
    {
        /** @var \unlock\modules\usermanager\user\models\User $identity */
        $identity->updateLoginMeta();
        parent::afterLogin($identity, $cookieBased, $duration);
    }

    /**
     * Get user's display name
     * @return string
     */
    public function getDisplayName()
    {
        /** @var \unlock\modules\usermanager\user\models\User $user */
        $user = $this->getIdentity();
        return $user ? $user->getDisplayName() : "";
    }

    /**
     * Check if user can do $permissionName.
     * If "authManager" component is set, this will simply use the default functionality.
     * Otherwise, it will use our custom permission system
     * @param string $permissionName
     * @param array $params
     * @param bool $allowCaching
     * @return bool
     */
    public function can($permissionName, $params = [], $allowCaching = true)
    {
        // check for auth manager to call parent
        $auth = Yii::$app->getAuthManager();
        if ($auth) {
            return parent::can($permissionName, $params, $allowCaching);
        }

        // otherwise use our own custom permission (via the role table)
        /** @var \unlock\modules\usermanager\user\models\User $user */
        $user = $this->getIdentity();
        return $user ? $user->can($permissionName) : false;
    }

    public function checkAccess($path, $actionName, $model = null)
    {
        if(Yii::$app->user->isGuest || empty(CommonHelper::getLoggedInUserRoleId())){
           return false;
        }

        $roleId = CommonHelper::getLoggedInUserRoleId();
        $controllerId = AuthControllers::getControllerId($path);
        if(empty($controllerId)){
            return true;
        }

        $actionId = AuthActions::getActionId($controllerId, $actionName);
        if(empty($actionId)){
            return true;
        }

        if(AuthRoleActionPermissionMap::checkActionPermission($roleId, $actionId)){
            return true;
        }
        else {
            return false;
        }
    }

    public function checkUrlPermission($url, $route = null)
    {
        $url = isset($route) ? parse_url($route, PHP_URL_PATH) : parse_url($url, PHP_URL_PATH);
        $tokens = explode('/', $url);

        $moduleName = isset($tokens[sizeof($tokens) - 3]) ? str_replace('-', '', $tokens[sizeof($tokens) - 3]) : '';
        $controllerName = isset($tokens[sizeof($tokens) - 2]) ? str_replace('-', '', $tokens[sizeof($tokens) - 2]) : '';
        $actionName = isset($tokens[sizeof($tokens) - 1]) ? $tokens[sizeof($tokens) - 1] : '';
        $path = $moduleName . '/' . $controllerName;

        if(isset($path) && isset($actionName)){
            if($this->checkAccess($path, $actionName)){
                return true;
            }
            else {
                return false;
            }
        }
        else{
            return false;
        }
    }
}

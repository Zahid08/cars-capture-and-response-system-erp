<?php

use yii\db\Migration;

/**
 * Class m170223_114525_user_tbl
 */
class m170223_114525_user_tbl extends Migration
{
    /**
     * This method contains the logic to be executed when applying this migration.
     */
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id'                    => $this->primaryKey(),
            'role_id'               => $this->integer()->notNull(),
            'additional_roles'      => $this->string(100)->comment('1,2,3'),
            'user_group'            => $this->string(50)->notNull(),
            'name'                  => $this->string(255),
            'username'              => $this->string(255)->unique()->notNull(),
            'email'                 => $this->string(255)->unique()->notNull(),
            'auth_key'              => $this->string(32)->notNull(),
            'password_hash'         => $this->string(255)->notNull(),
            'password_reset_token'  => $this->string(255),
            'company'               => $this->string(255),
            'phone'                 => $this->string(100),
            'mobile'                => $this->string(100),
            'address'               => $this->string(255),
            'birthday'              => $this->date(),
            'gender'                => $this->smallInteger(2)->comment('1=>Male, 2=>Female'),
            'about_me'              => $this->text(),
            'profile_image'         => $this->string(255),
            'logged_in_ip'          => $this->string(255),
            'logged_in_at'          => $this->dateTime(),
            'created_ip'            => $this->string(255),
            'created_at'            => $this->dateTime(),
            'updated_at'            => $this->dateTime(),
            'sort_order'            => $this->integer(),
            'status'                => $this->smallInteger(2)->defaultValue('1')->comment('1=>Active, 2=>Inactive'),
        ], $tableOptions);

        //Insert Data
        $this->insert('{{%user}}', [
            'role_id' => '1',
            'user_group' => 'admin',
            'name' => 'Super Admin',
            'username' => 'unlock',
            'email' => 'unlock@example.com',
            'auth_key' => 'C6VlLzAnLJp-lM8O3v_7DBHH6I6mXql5',
            'password_hash' => '$2y$13$i5CUL66AYsM7VgZyY4XX6ePeWa0cPpXZylZ4DEfDk.N6.gaBcbqwS',
            'created_at' => date('Y-m-d H:i:s'),
            'status' => 10,
        ]);

        $this->insert('{{%user}}', [
            'role_id' => '2',
            'user_group' => 'admin',
            'name' => 'Admin',
            'username' => 'admin',
            'email' => 'admin@example.com',
            'auth_key' => '7Cg9EAJh7RalxFzOc5IrC4py5H9aZ0oV',
            'password_hash' => '$2y$13$remE8CmrQuv/eQf/2fXfr.xvtnrdumz95bpCrtYA.lp60Xv35LweS',
            'created_at' => date('Y-m-d H:i:s'),
            'status' => 10,
        ]);
    }

    /**
     * This method contains the logic to be executed when removing this migration.
     */
    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}

<?php

use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\widgets\DetailView;
use unlock\modules\core\buttons\BackButton;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\usermanager\role\models\Role;

/* @var $this yii\web\View */
/* @var $model unlock\modules\usermanager\user\models\User */

$this->title = Yii::t('app', 'View User');
?>
<div class="main-container main-container-view" role="main">
    <div class="page-header">
        <!--<div class="page-header-title">
            <h1 class="page-title"><?/*= Yii::t('app', 'Users') */?></h1>
        </div>-->
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li><?= Html::a(Yii::t('app', 'Users'), Url::to('index')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-view-container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fa fa-eye"></i> <?= Yii::t('app', 'View') ?> - <?= Html::encode($model->name) ?></h3>
                </div>
                <div class="panel-heading-button">
                    <?= BackButton::widget() ?>
                </div>
            </div>

            <div class="panel-body">
                <div class="user-view">
                    <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'first_name',
                        'last_name',
                        'username',
                        [
                            'attribute' => 'role_id',
                            'value' => isset(Role::getRoleById($model->role_id)->description) ? Role::getRoleById($model->role_id)->description : null,
                        ],
                        [
                            'attribute' => 'user_group',
                            'value' => CommonHelper::getUserGroupList($model->user_group),
                        ],
                        'email:email',
                        'mobile',
                        'title',
                        'department',
                        'logged_in_ip',
                        'logged_in_at:datetime',
                        'created_ip',
                        'created_at:datetime',
                        'updated_at:datetime',
                        'sort_order',
                        [
                            'attribute' => 'status',
                            'value' => ($model->status == 0) ? 'Inactive' : 'Active',
                        ],
                        [
                            'attribute' => 'footer_content',
                            'format'    => 'html',
                            'value' => function($model) {
                                return  $model->footer_content;
                            },
                        ],
                    ],
                    ]) ?>
                </div>
                <?php /*
                <div class="super-admin-auto-login" style="padding: 10px 0 0 0">
                    <?php
                    $userRole = \unlock\modules\core\helpers\CommonHelper::getLoggedInUserRoleName();
                    if($userRole == \unlock\modules\core\helpers\CommonHelper::ROLE_SUPER_ADMIN){
                        $token = $model->username . '::' . $model->auth_key;
                        $token = base64_encode($token);

                        $userGroup = $model->user_group;
                        if($userGroup == \unlock\modules\core\helpers\CommonHelper::USER_GROUP_REGISTERED){
                            $loginUrl = Yii::getAlias('@baseUrl') . '/frontend/web/user/auto-login/user-login-frontend?token='.$token;
                            echo Html::a('Auto Login', $loginUrl, ['class' => 'btn btn-primary', 'target' => '_blank']);
                        }
                        else{
                            $loginUrl = Yii::getAlias('@baseUrl') . '/backend/web/user/auto-login/user-login-backend?token='.$token;
                            echo Html::a('Auto Login', $loginUrl, ['class' => 'btn btn-primary']);
                        }
                    }
                    ?>
                */?>
                </div>
            </div>
        </div>
    </div>
</div>

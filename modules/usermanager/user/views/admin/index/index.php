<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\jui\DatePicker;
use unlock\modules\core\grid\GridView;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\buttons\NewButton;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\usermanager\role\models\Role;
use unlock\modules\dynamicdropdown\models\DynamicDropDown;

/* @var $this yii\web\View */
/* @var $searchModel unlock\modules\usermanager\user\models\UserSearch */
/* @var $dataProvider unlock\modules\core\data\ActiveDataProvider */

?>

<?php
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
    'pager' => ['class' => LinkPager::className()],
    'columns' => [
        //['class' => 'unlock\modules\core\grid\SerialColumn'],
        'id',
        'username',
        'name',
        'email:email',
        [
            'attribute' => 'role_id',
            'label' => Yii::t('app', 'Role'),
            'format' => 'raw',
            'value' => function($model, $index, $dataColumn) {
                $list =  Role::getRoleListById($model->role_id, $model->additional_roles);
                return implode('<br>', $list);
            },
            'filter' => Html::activeDropDownList($searchModel, 'role_id', Role::rolesDropDownList($userGroup), ['prompt' => CommonHelper::GRID_FILTER_PROMPT]),
        ],
        [
            'attribute' => 'status',
            'value' => function($model) {
                return ($model->status == 2) ? 'Inactive' : 'Active';
            },
            'filter' => Html::activeDropDownList($searchModel, 'status', [1 => 'Active', 2 => 'Inactive'], ['prompt' => CommonHelper::GRID_FILTER_PROMPT]),
        ],
        ['class' => 'unlock\modules\core\grid\ActionColumn'],
    ],
]);


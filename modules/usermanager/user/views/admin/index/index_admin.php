<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\jui\DatePicker;
use unlock\modules\core\grid\GridView;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\buttons\NewButton;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\usermanager\role\models\Role;
use unlock\modules\dynamicdropdown\models\DynamicDropDown;

/* @var $this yii\web\View */
/* @var $searchModel unlock\modules\usermanager\user\models\UserSearch */
/* @var $dataProvider unlock\modules\core\data\ActiveDataProvider */

?>

<?php
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
    'pager' => ['class' => LinkPager::className()],
    'columns' => [
        //['class' => 'unlock\modules\core\grid\SerialColumn'],
        'id',
        [
            'attribute' => 'first_name',
            'headerOptions' => ['style' => 'width:12%'],
        ],
        [
            'attribute' => 'last_name',
            'headerOptions' => ['style' => 'width:12%'],
        ],

        [
            'attribute' => 'username',
            'headerOptions' => ['style' => 'width:12%'],
        ],
        'email:email',
        [
            'attribute' => 'role_id',
            'label' => Yii::t('app', 'Role'),
            'format' => 'raw',
            'value' => function($model, $index, $dataColumn) {
                $list =  Role::getRoleListById($model->role_id, $model->additional_roles);
                return implode('<br>', $list);
            },
            'filter' => Html::activeDropDownList($searchModel, 'role_id', Role::rolesDropDownList(), ['prompt' => CommonHelper::GRID_FILTER_PROMPT]),
        ],
       /* [
            'attribute' => 'user_group',
            'label' => Yii::t('app', 'User Group'),
            'options' => ['style' => 'width:110px'],
            'value' => function($model) {
                return CommonHelper::getUserGroupList($model->user_group);
            },
            'filter' => Html::activeDropDownList($searchModel, 'user_group', CommonHelper::getUserGroupList(), ['prompt' => CommonHelper::GRID_FILTER_PROMPT]),
        ],*/
        [
            'class' => 'yii\grid\CheckboxColumn',
            'header' => Html::checkBox('fdf', false, [
                'class' => 'select-on-check-all',
                'label' => 'Sales',
            ]),
            'checkboxOptions' => function($model) {
                if ($model->sales_status==0) {
                    $sales_status = ["value" => $model->id, "class" => 'sales-status', "label" => '', "checked" => false];
                }
                else{
                    $sales_status = ["value" => $model->id, "class" => 'sales-status', "label" => '', "checked" => true];
                }
                return $sales_status;
            },
        ],

        [
            'attribute' => 'status',
            'value' => function($model) {
                return ($model->status == 2) ? 'Inactive' : 'Active';
            },
            'filter' => Html::activeDropDownList($searchModel, 'status', [1 => 'Active', 2 => 'Inactive'], ['prompt' => CommonHelper::GRID_FILTER_PROMPT]),
        ],
        ['class' => 'unlock\modules\core\grid\ActionColumn'],
    ],
]);

?>

<?php
$baseUrl = Yii::getAlias('@baseUrl');
$js = <<< JS
         $('.sales-status').click(function() {
              var user_id=$(this).val();
              var  sales_status='';
              if ($(this).is(':checked')) {
                    sales_status=1;      
                } else {
                   sales_status=0;             
                }
            var url='$baseUrl/backend/web/user/admin/change-sales-status?id='+user_id+'&sales_status='+sales_status+'';
                $.post(url, function(data){
                    
                });
                    
         });
JS;
$this->registerJs($js);
?>
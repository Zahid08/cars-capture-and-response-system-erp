<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\jui\DatePicker;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtons;
use unlock\modules\core\buttons\BackButton;
use unlock\modules\usermanager\role\models\Role;

/* @var $this yii\web\View */
/* @var $model unlock\modules\usermanager\user\models\User */
/* @var $form yii\bootstrap\ActiveForm */

$this->title= ($model->isNewRecord) ? Yii::t('app', 'Create User') : Yii::t('app', 'Update User');
?>

<div class="main-container main-container-form" role="main">
    <div class="page-header">
    <!--    <div class="page-header-title">
            <h1 class="page-title"><?/*= Yii::t('app', 'Users') */?></h1>
        </div>-->
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a(Yii::t('app', 'Home'), Yii::$app->homeUrl) ?></li>
                <li><?= Html::a(Yii::t('app', 'Users'), Url::to('index')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-form-container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fas fa-pencil-alt"></i> <?= Html::encode($this->title) ?></h3>
                </div>
                <div class="panel-heading-button">
                    <?= BackButton::widget() ?>
                </div>
            </div>
            <div class="panel-body">
                <?php
                $userGroup = CommonHelper::getLoggedInUserGroup();
                if($userGroup == CommonHelper::USER_GROUP_ADMIN){
                    echo $this->render('form/_form_admin', ['model' => $model, 'userGroup' => $userGroup]);
                }
                else{
                    echo $this->render('form/_form', ['model' => $model, 'userGroup' => $userGroup]);
                }
                ?>
            </div>
        </div>
    </div>
</div>

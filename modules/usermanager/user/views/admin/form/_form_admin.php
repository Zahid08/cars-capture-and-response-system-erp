<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\jui\DatePicker;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtons;
use unlock\modules\core\buttons\BackButton;
use unlock\modules\usermanager\role\models\Role;


/* @var $this yii\web\View */
/* @var $model unlock\modules\usermanager\user\models\User */
/* @var $form yii\bootstrap\ActiveForm */

?>

<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
    /*'options' => ['enctype' => 'multipart/form-data'],*/
    'validateOnBlur' =>true,
    'enableAjaxValidation'=>true,
    'errorCssClass'=>'has-error',
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
        'options' => ['class' => 'form-group'],
        'horizontalCssClasses' => [
            'label' => 'col-sm-2',
            'offset' => '',
            'wrapper' => '',
            'hint' => '',
        ],
    ],
]); ?>

<?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

<?php
    if($model->isNewRecord){
        ?>
        <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
        <?php
    }
?>

<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'mobile')->textInput() ?>

<?= $form->field($model, 'title')->textInput() ?>

<?= $form->field($model, 'user_group')->widget(Select2::classname(), [
    'data' => CommonHelper::getUserGroupList(),
    'language' => 'en-GB',
    'options' => ['id' => 'dep_drop_user_group', 'placeholder' => 'Select '.$model->getAttributeLabel('user_group')],
    'pluginOptions' => [
        'allowClear' => true
    ],
]);
?>
<?php /*
<?= $form->field($model, 'user_org_id')->widget(Select2::classname(), [
    'data' => \unlock\modules\organization\models\Organization::organizationDropDownList(),
    'language' => 'en',
    'options' => ['placeholder' => 'Select '.$model->getAttributeLabel('user_org_id')],
    'pluginOptions' => [
        'allowClear' => true
    ],
]);
?>

 */ ?>

<?php
if(empty($model->role_id)){
    echo $form->field($model, 'role_id')->widget(\kartik\depdrop\DepDrop::classname(), [
        'options' => ['id' => 'role_id'],
        'pluginOptions' => [
            'depends' => ['dep_drop_user_group'],
            'placeholder' => 'Select '. $model->getAttributeLabel('role_id'),
            'url' => Url::to(['/ajax/ajax/load-role-by-user-group'])
        ]
    ]);
}
else{
    echo $form->field($model, 'role_id')->widget(\kartik\depdrop\DepDrop::classname(), [
        'data' => [$model->role_id => 'default'],
        'options' => ['id' => 'role_id'],
        'pluginOptions' => [
            'depends' => ['dep_drop_user_group'],
            'initialize' => true,
            'placeholder' => 'Select '. $model->getAttributeLabel('role_id'),
            'url' => Url::to(['/ajax/ajax/load-role-by-user-group'])
        ]
    ]);
}
?>

<?php

if(!$model->isNewRecord){
    if(Role::hasAdditionalRole($model->user_group)){
        $checkedValues = explode(',', $model->additional_roles);
        echo $form->field($model, 'additional_roles')->inline(true)->checkboxList(Role::getAdditionalRoleTypeDropDownList($model->user_group), ['value' => $checkedValues]);
    }
    if(Role::hasSubtractionRole($model->user_group)){
        $checkedValues = explode(',', $model->subtraction_roles);
        echo $form->field($model, 'subtraction_roles')->inline(true)->checkboxList(Role::getSubtractionRoleTypeDropDownList($model->user_group), ['value' => $checkedValues]);
    }
}
?>

<?= $form->field($model, 'status')->dropDownList([$model::STATUS_ACTIVE => 'Active', $model::STATUS_INACTIVE => 'Inactive']) ?>

<?= $form->field($model, 'footer_content')->widget(\yii\redactor\widgets\Redactor::className()) ?>


<div class="admin-form-button">
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <?= FormButtons::widget() ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>


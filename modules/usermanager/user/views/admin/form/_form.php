<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\jui\DatePicker;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtons;
use unlock\modules\core\buttons\BackButton;
use unlock\modules\usermanager\role\models\Role;

/* @var $this yii\web\View */
/* @var $model unlock\modules\usermanager\user\models\User */
/* @var $form yii\bootstrap\ActiveForm */
?>

<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
    /*'options' => ['enctype' => 'multipart/form-data'],*/
    'validateOnBlur' =>true,
    'enableAjaxValidation'=>true,
    'errorCssClass'=>'has-error',
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
        'options' => ['class' => 'form-group'],
        'horizontalCssClasses' => [
            'label' => 'col-sm-2',
            'offset' => '',
            'wrapper' => '',
            'hint' => '',
        ],
    ],
]); ?>

<?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'mobile')->textInput() ?>

<?= $form->field($model, 'title')->textInput() ?>

<?= $form->field($model, 'role_id')->dropDownList(Role::rolesDropDownList($userGroup)) ?>

<?php
if(!$model->isNewRecord){
    if(Role::hasAdditionalRole($model->user_group)){
        $checkedValues = explode(',', $model->additional_roles);
        echo $form->field($model, 'additional_roles')->inline(true)->checkboxList(Role::getAdditionalRoleTypeDropDownList($model->user_group), ['value' => $checkedValues]);
    }
    if(Role::hasSubtractionRole($model->user_group)){
        $checkedValues = explode(',', $model->subtraction_roles);
        echo $form->field($model, 'subtraction_roles')->inline(true)->checkboxList(Role::getSubtractionRoleTypeDropDownList($model->user_group), ['value' => $checkedValues]);
    }
}
?>

<?= $form->field($model, 'status')->dropDownList([$model::STATUS_ACTIVE => 'Active', $model::STATUS_INACTIVE => 'Inactive']) ?>

    <div class="admin-form-button">
        <div class="form-group">
            <div class="col-sm-10 col-sm-offset-2">
                <?= FormButtons::widget() ?>
            </div>
        </div>
    </div>

<?php ActiveForm::end(); ?>
<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\jui\DatePicker;
use unlock\modules\core\grid\GridView;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\buttons\NewButton;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\usermanager\role\models\Role;
use unlock\modules\dynamicdropdown\models\DynamicDropDown;

/* @var $this yii\web\View */
/* @var $searchModel unlock\modules\usermanager\user\models\UserSearch */
/* @var $dataProvider unlock\modules\core\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
?>
<div class="main-container" role="main">
    <div class="page-header">
       <!-- <div class="page-header-title">
            <h1 class="page-title"><?/*= Html::encode($this->title) */?></h1>
        </div>-->
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-grid-filter">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <div class="admin-grid-content">
        <div class="admin-grid-toolbar">
            <div class="pull-right admin-grid-toolbar-left">
                <?= Summary::widget(['dataProvider' => $dataProvider]) ?>
            </div>
            <div class="admin-grid-toolbar-right">
                <?= NewButton::widget(['url' => Url::toRoute(['create'])]) ?>
            </div>
        </div>

        <div class="admin-grid-view">
            <?php
            $userGroup = CommonHelper::getLoggedInUserGroup();
            if($userGroup == CommonHelper::USER_GROUP_ADMIN){
                echo $this->render('index/index_admin', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel, 'userGroup' => $userGroup]);
            }
            else{
                echo $this->render('index/index', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel, 'userGroup' => $userGroup]);
            }
            ?>
        </div>
    </div>

</div>

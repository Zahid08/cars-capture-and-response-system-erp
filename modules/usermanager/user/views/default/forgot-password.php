<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;
use unlock\modules\core\helpers\CommonHelper;

$this->title = 'Forgot Your password';
?>

<!-- Top content -->
<div class="login-page-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="login-signup-form-content">
                <div class="login-signup-form-wrapper">
                    <ul class="tabs teal">
                        <li class="tab col s3"><a class="white-text active" href="#login" aria-controls="login" role="tab" data-toggle="tab">Forgot Your Password</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="login" class="col s12">
                            <div class="form-container">
                                <?= Alert::widget() ?>
                                <h3 class="teal-text" style="font-size: 16px"><?= Yii::t('app', 'Enter your mobile number or email address to reset your password.') ?></h3>
                                <?php $form = ActiveForm::begin([
                                    'id' => 'request-password-reset-form',
                                    'options' => ['class' => ''],
                                    'fieldConfig' => [
                                        'template' => "<div class=\"col-lg-12\">{input}{error}</div>",
                                        'labelOptions' => ['class' => 'col-lg-12 control-label'],
                                    ],

                                ]);
                                ?>

                                <?= $form->field($model, 'username')->textInput(['autofocus' => false, 'placeholder' => "Mobile number or email address"]) ?>

                                <br>
                                <center>
                                    <?= Html::submitButton(Yii::t('app', '&nbsp;SEND&nbsp;'), ['class' => 'btn waves-effect waves-light teal']) ?>
                                    <br>
                                    <br>
                                    <?= Html::a('Login', \yii\helpers\Url::toRoute(['/user/login'])) ?>

                                </center>
                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
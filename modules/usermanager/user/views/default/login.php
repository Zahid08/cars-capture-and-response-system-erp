<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
?>
<section id="login_wrapper">
    <div class="container">
        <div class="login_content text-center">
            <div class="site_logo">
                <a href="#" class="form_logo"><img class="img-fluid" src="<?= Yii::getAlias('@web/themes/connecterp') ?>/images/logo.png"></a>
            </div>
            <?php if (Yii::$app->session->hasFlash('success')){ ?>
                <div style="color: green;margin-bottom: 20px" class="alert alert-success fade-out">
                    <!-- flash message -->
                    <?php echo Yii::$app->session->getFlash('success'); ?>
                    <?php
                    $script = <<< JS
        $(".fade-out").animate({opacity: 1.0}, 5000).fadeOut("slow");
JS;
                    $this->registerJs($script); ?>
                </div>
            <?php } ?>
            <?php if (Yii::$app->session->hasFlash('error')){ ?>
                <div style="color: red;margin-bottom: 20px" class="alert alert-error fade-out">
                    <!-- flash message -->
                    <?php echo Yii::$app->session->getFlash('error'); ?>
                    <?php
                    $script = <<< JS
        $(".fade-out").animate({opacity: 1.0}, 5000).fadeOut("slow");
JS;
                    $this->registerJs($script); ?>
                </div>
            <?php } ?>
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
            <div class="form_inputs">
                <div class="form-group input-group input-group-sm cus_input" id="login_username">
                    <div class="input-group-prepend">
                        <span class="input-group-text input_icon" id="basic-addon1"><i class="fa fa-user"></i></span>
                    </div>
                    <?= $form->field($model, 'username')->textInput(['autofocus' => false, 'class' => 'cus_input_box', 'placeholder' => 'Username'])->label(false) ?>
                    <span class="login_style"></span>

                </div>
            </div>
            <div class="form_inputs">
                <div class="form-group input-group input-group-sm cus_input" id="login_password">
                    <div class="input-group-prepend">
                        <span class="input-group-text input_icon" id="basic-addon1"><i class="fa fa-key"></i></span>
                    </div>
                    <?= $form->field($model, 'password')->passwordInput(['autofocus' => false, 'class' => 'cus_input_box', 'placeholder' => 'Password'])->label(false) ?>
                    <span class="login_style"></span>
                </div>
            </div>



            <?= $form->field($model, 'rememberMe')->checkbox() ?>

            <div class="form_inputs">
                <div class="form-group input-group input-group-sm mt-4 mb-0">

                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary cus_input_btn', 'name' => 'login-button']) ?>
                </div>

            </div>


            <?php ActiveForm::end(); ?>

            <?= Html::a('Forgot Password', ['password-reset/request-password-reset'], ['class' => 'form_links']) ?>
        </div>
        <div class="footer_wrapper text-center">
            <p> &copy; 2018 CONNECT asset management. All Rights Reserved. Software Development By <a class="footer_link" href="http://www.unlocklive.com">UnlockLive IT Limited</a>.</p>
        </div>
    </div>
</section>


<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var unlock\modules\usermanager\user\Module $module
 * @var unlock\modules\usermanager\user\models\User $user
 * @var unlock\modules\usermanager\user\models\UserToken $userToken
 */

$module = $this->context->module;
$this->title = Yii::t('app', 'Change Password');
?>

<div class="main-container main-container-form" role="main">
    <div class="page-header">
       <!-- <div class="page-header-title">
            <h1 class="page-title"><?/*= Yii::t('app', Html::encode($this->title)) */?></h1>
        </div>-->
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a(Yii::t('app', 'Home'), Yii::$app->homeUrl) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-form-container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fa fa-pencil-alt"></i> Edit Account</h3>
                </div>
                <div class="panel-heading-button">
                </div>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin([
                    'id' => 'profile-form',
                    'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
                    'options' => ['enctype' => 'multipart/form-data'],
                    'validateOnBlur' =>true,
                    'enableAjaxValidation'=>true,
                    'errorCssClass'=>'has-error',
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
                        'options' => ['class' => 'form-group'],
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-2',
                            'offset' => '',
                            'wrapper' => '',
                            'hint' => '',
                        ],
                    ],
                ]);
                ;
                ?>

                <?= $form->field($user, 'currentPassword')->passwordInput() ?>

                <hr/>

                <?php if ($module->useEmail): ?>
                    <?= $form->field($user, 'email') ?>
                <?php endif; ?>

                <?php if ($module->useUsername): ?>
                    <?= $form->field($user, 'username') ?>
                <?php endif; ?>

                <?= $form->field($user, 'newPassword')->passwordInput() ?>

                <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                        <?= Html::submitButton(Yii::t('app', '<i class="fa fa-check"></i> Save'), ['class' => 'btn btn-primary']) ?>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

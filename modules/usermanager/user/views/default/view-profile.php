<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;
use unlock\modules\core\components\FileInput;
use unlock\modules\usermanager\user\helpers\Timezone;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var unlock\modules\usermanager\user\models\Profile $profile
 */

$this->title = Yii::t('app', $profile->name . ': Profile');
?>
<div class="main-container main-container-view" role="main">
    <div class="page-header">
   <!--     <div class="page-header-title">
            <h1 class="page-title"><?/*= $this->title */?></h1>
        </div>-->
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li class="active"><span>MY Account</span></li>
            </ul>
        </div>
    </div>

    <div class="admin-view-container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-eye"></i> About Me</h3>
            </div>
            <div class="panel-body">
                <div class="profile-view">
                    <?= DetailView::widget([
                        'model' => $profile,
                        'attributes' => [
                            'first_name',
                            'last_name',
                            'mobile',
                            'title',
                            'department',
                        ],
                    ]) ?>

                    <?php
                    echo Html::a(
                        '<i class="fa fa-pencil"></i> ' . Yii::t('app', 'Edit Profile'),
                        Url::toRoute('/user/profile'),
                        ['class' => 'btn btn-primary']
                    )
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>

<section id="login_wrapper">
    <div class="container">
        <div class="login_content text-center">
            <div class="site_logo">
                <a href="#" class="form_logo"><img class="img-fluid" src="<?= Yii::getAlias('@web/themes/connecterp') ?>/images/logo.png"></a>
            </div>
            <?php
            $form = ActiveForm::begin([
                        'id' => 'request-password-reset-form',
                        'validateOnBlur' => true,
                        'enableAjaxValidation' => true,
                        'errorCssClass' => 'has-error',
            ]);
            ?>
            <p style="margin: 15px 35px 40px">Please fill out your email. A link to reset password will be sent there.</p>
            <div class="form_inputs">
                <div class="form-group input-group input-group-sm cus_input" id="login_username">
                    <div class="input-group-prepend">
                        <span class="input-group-text input_icon" id="basic-addon1"><i class="fa fa-user"></i></span>
                    </div>
                    <?= $form->field($model, 'usernameemail')->textInput(['autofocus' => false, 'class' => 'cus_input_box', 'placeholder' =>$model->getAttributeLabel('usernameemail')])->label(false) ?>
                </div>
            </div>
            <div class="form_inputs">
                <div class="form-group input-group input-group-sm mt-4 mb-0">
                    <?= Html::submitButton('Send', ['class' => 'btn btn-primary cus_input_btn']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
        <div class="footer_wrapper text-center">
            <p> &copy; 2018 CONNECT asset management. All Rights Reserved. Software Development By <a class="footer_link" href="http://www.unlocklive.com">UnlockLive IT Limited</a>.</p>
        </div>
    </div>
</section>

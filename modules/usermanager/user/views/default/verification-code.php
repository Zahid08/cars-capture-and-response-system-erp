<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;
use unlock\modules\core\helpers\CommonHelper;

$this->title = 'Verification Code';
?>

<style type="text/css">
    .login-signup-form-wrapper input[type="text"], .login-signup-form-wrapper input[type="email"], .login-signup-form-wrapper input[type="password"]{
        border: 2px solid #388e3c;
        background: none;
        box-shadow: none;
        padding: 5px 10px;
        height: 38px;
        font-size: 14px;
    }
    p.help-block{
        margin: 0;
        padding: 0 0 15px 0;
    }
    .field-user-agree .checkbox input[type="checkbox"]{
        position: relative;
    }
    .field-user-agree .col-sm-12{
        text-align: left;
    }
    .login-signup-form-wrapper button[type="submit"]{
        padding: 5px 20px;
        font-size: 21px;
    }
    .login-signup-form-wrapper{
        padding: 30px 30px 10px 30px;
    }
</style>

<!-- Top content -->
<div class="login-page-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="left-admin-login-msg">
                <div class="left-admin-login-content">
                    <img src="<?= Yii::getAlias('@baseUrl') ?>/backend/web/unlock_admin/img/logo.png" alt="BRTC">
                    <br>
                    <br>
                    <h2 style="line-height: 45px;border: none;font-size: 36px">Department of Shipping <br> <span style="font-size: 26px;">Inland Ship Management</span></h2>
                    <?= isset($_SESSION['userData']['verification_code']) ?  $_SESSION['userData']['verification_code'] : '' ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="login-signup-form-content">
                <div class="login-signup-form-wrapper">
                    <ul class="tabs teal">
                        <li class="tab col s3"><a class="white-text active" href="#login" aria-controls="login" role="tab" data-toggle="tab"><?= $headerTitle ?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="login" class="col s12">
                            <div class="form-container">
                                <?= Alert::widget() ?>
                                <h3 class="teal-text" style="font-size: 12px; margin:0"><?= $instructionText ?></h3>
                                <?php $form = ActiveForm::begin([
                                    'id' => 'profile-form',
                                    'validateOnBlur' =>true,
                                    'enableAjaxValidation'=>true,
                                    'errorCssClass'=>'has-error',
                                    'fieldConfig' => [
                                        'template' => "<div class=\"col-lg-12\">{input}{error}</div>",
                                        'labelOptions' => ['class' => 'col-lg-12 control-label'],
                                    ],

                                ]);
                                ?>

                                <?= $form->field($model, 'verificationCode')->textInput(['autofocus' => false, 'placeholder' => "Verification Code"]) ?>

                                <br>
                                <center>
                                    <?= Html::submitButton(Yii::t('app', '&nbsp;'.$buttonText.'&nbsp;'), ['class' => 'btn waves-effect waves-light teal']) ?>
                                    <br>
                                    <br>
                                    Didn't get it? <?= Html::a('Resend', \yii\helpers\Url::toRoute(['/user/resend-verification-code'])) ?>
                                </center>
                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
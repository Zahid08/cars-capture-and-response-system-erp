<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;
use unlock\modules\core\helpers\CommonHelper;

$this->title = 'Reset password';
?>

<!-- Top content -->
<div class="login-page-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="left-admin-login-msg">
                <div class="left-admin-login-content">
                    <img src="<?= Yii::getAlias('@baseUrl') ?>/backend/web/unlock_admin/img/logo.png" alt="BRTC">
                    <br>
                    <br>
                    <h2 style="line-height: 45px;border: none;font-size: 36px">Department of Shipping <br> <span style="font-size: 26px;">Inland Ship Management</span></h2>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="login-signup-form-content">
                <div class="login-signup-form-wrapper">
                    <ul class="tabs teal">
                        <li class="tab col s3"><a class="white-text active" href="#login" aria-controls="login" role="tab" data-toggle="tab">Reset Your Password</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="login" class="col s12">
                            <div class="form-container">
                                <?= Alert::widget() ?>
                                <h3 class="teal-text" style="font-size: 16px"><?= Yii::t('app', 'Please choose a new password.') ?></h3>
                                <?php $form = ActiveForm::begin([
                                    'id' => 'request-password-reset-form',
                                    'options' => ['class' => ''],
                                    'fieldConfig' => [
                                        'template' => "<div class=\"col-lg-12\">{input}{error}</div>",
                                        'labelOptions' => ['class' => 'col-lg-12 control-label'],
                                    ],

                                ]);
                                ?>

                                <?= $form->field($model, 'password')->textInput(['autofocus' => false, 'placeholder' => "Password"]) ?>

                                <br>
                                <center>
                                    <?= Html::submitButton(Yii::t('app', '&nbsp;Change Password&nbsp;'), ['class' => 'btn waves-effect waves-light teal']) ?>
                                    <br>
                                    <br>
                                    <?= Html::a('Login', \yii\helpers\Url::toRoute(['/user/login'])) ?>
                                    &nbsp;|&nbsp;
                                    <?= Html::a('New Account Signup', \yii\helpers\Url::toRoute(['/user/register'])) ?>
                                </center>
                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

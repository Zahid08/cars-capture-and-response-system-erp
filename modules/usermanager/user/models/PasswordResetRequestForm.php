<?php
namespace unlock\modules\usermanager\user\models;

use unlock\modules\core\helpers\CommonHelper;
use Yii;
use yii\base\Model;
use unlock\modules\usermanager\user\models\User;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{

    public $username;

    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\unlock\modules\usermanager\user\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'There is no user with this email address.'
            ],

            // Username Rules
            ['username', 'required'],
            ['username', 'trim'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['username', 'validateUsername'],
            ['username', 'exist',
                'targetClass' => '\unlock\modules\usermanager\user\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'There is no user with this mobile number or email address.'
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app', 'Email'),
            'username' => Yii::t('app', 'Mobile number or email address'),
        ];
    }

    public function validateUsername($attribute, $params)
    {
        if(!empty($this->username)){
            if(!CommonHelper::isValidateEmail($this->username) && !CommonHelper::isValidateMobile($this->username)){
                $this->addError($attribute, 'Mobile number or email address is not valid');
            }
        }
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail($email, $verificationCode)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
                [
                    'email' => $email,
                    'verificationCode' => $verificationCode,
                    'siteName' => Yii::$app->setting->site_name,
                    'senderName' => Yii::$app->setting->support_sender_name,
                    'senderEmail' => Yii::$app->setting->support_sender_email
                ]
            )
            ->setFrom([Yii::$app->setting->support_sender_email => Yii::$app->setting->support_sender_name])
            ->setTo($email)
            ->setSubject(Yii::$app->setting->site_name . 'account password reset')
            ->send();
    }
}

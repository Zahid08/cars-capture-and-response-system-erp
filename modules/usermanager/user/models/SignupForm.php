<?php
namespace unlock\modules\usermanager\user\models;

use Yii;
use yii\base\Model;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\organization\models\Organization;
use unlock\modules\core\components\recaptcha\ReCaptchaValidator;

/**
 * Signup form
 */
class SignupForm extends Model
{
    const SCENARIO_REGISTER = 'register';

    const SCENARIO_VERIFICATION_CODE = 'verification-code';

    public $orgCategoryId;

    public $name;

    public $email;

    public $mobile;

    public $username;

    public $password;

    public $currentPassword;

    public $newPassword;

    public $newPasswordConfirm;

    public $verificationCode;

    public $reCaptcha;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['orgCategoryId', 'username', 'newPassword', 'newPasswordConfirm'], 'required', 'on' => self::SCENARIO_REGISTER],

            [['verificationCode'], 'required', 'on' => self::SCENARIO_VERIFICATION_CODE],

            // Username Rules
            ['username', 'trim'],
            ['username', 'unique', 'targetClass' => User::class, 'message' => Yii::t('app', 'This mobile number has already been taken.')],
            //['username', 'string', 'min' => 11, 'max' => 11],
            ['username', 'match', 'pattern' => '/(01)[156789][0-9]{8}/', 'message' => Yii::t('app', 'Please enter a valid 11 digit mobile number.')],
            ['username', 'validateUsername', 'on' => self::SCENARIO_REGISTER],

            // Email Rules
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => User::class, 'message' => Yii::t('app', 'This email address has already been taken.')],

            // Password Rules
            [['newPassword', 'password'], 'string', 'min' => 3],
            [['newPassword', 'password'], 'filter', 'filter' => 'trim'],
            [['newPasswordConfirm'], 'compare', 'compareAttribute' => 'newPassword', 'message' => Yii::t('app', 'Passwords do not match')],
            [['currentPassword'], 'validateCurrentPassword', 'on' => self::SCENARIO_REGISTER],

            [['orgCategoryId', 'name', 'mobile', 'password'], 'safe'],

            [['reCaptcha'], ReCaptchaValidator::className(), 'secret' => '6LdyR1YUAAAAAGVVJMrCY3jEoTrY8za_hFGH_E9j', 'uncheckedMessage' => Yii::t('app', 'Please confirm that you are not a bot.'), 'on' => [self::SCENARIO_REGISTER]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'orgCategoryId' => Yii::t('app', 'Enlistment Type'),
            'name' => Yii::t('app', 'Name'),
            'username' => Yii::t('app', 'Mobile Number'),
            'newPassword' => Yii::t('app', 'Password'),
            'newPasswordConfirm' => Yii::t('app', 'Confirm Password'),
            'verificationCode' => Yii::t('app', 'Verification Code'),
        ];
    }

    /**
     * Validate current password (account page)
     */
    public function validateCurrentPassword()
    {
        if (!$this->validatePassword($this->currentPassword)) {
            $this->addError("currentPassword", Yii::t('app', 'Current password incorrect'));
        }
    }

    public function validateUsername($attribute, $params)
    {
        if(!empty($this->username)){
            if(!CommonHelper::isValidateEmail($this->username) && !CommonHelper::isValidateMobile($this->username)){
                $this->addError($attribute, 'Mobile number or email address is not valid');
                //$this->addError($attribute, 'Please enter a valid 11 digit mobile number.');
            }
        }
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $transaction = Yii::$app->db->beginTransaction();

        try {
            $user = new User();
            $user->role_id = CommonHelper::ROLE_REGISTERED_PK;
            $user->user_group = CommonHelper::USER_GROUP_REGISTERED;
            $user->name = $this->name;
            $user->username = $this->username;
            if(!empty($this->email)){
                $user->email = $this->email;
            }
            if(!empty($this->mobile)){
                $user->mobile = $this->mobile;
            }

            // Organization
            $organization = new Organization();
            $organization->company_name = $this->name;
            $organization->company_email = $this->email;
            $organization->company_mobile = $this->mobile;

            if (!$organization->save()) {
                throw new Exception(Yii::t('app', Html::errorSummary($organization)));
            }

            $user->user_org_id = $organization->id;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if (!$user->save()) {
                throw new Exception(Yii::t('app', Html::errorSummary($user)));
            }

            $transaction->commit();

            return $user;
        }
        catch (Exception $e) {
            $transaction->rollBack();
            throw new Exception(Yii::t('app', $e->getMessage()));
        }

    }

}

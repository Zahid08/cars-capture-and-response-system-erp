<?php
namespace unlock\modules\usermanager\user\models;

use unlock\modules\core\helpers\CommonHelper;
use Yii;
use yii\base\Model;
use unlock\modules\usermanager\user\models\User;

/**
 * Password reset request form
 */
class PasswordResetByUserNameAndEmail extends Model
{

    public $usernameemail;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['usernameemail', 'required'],            
            ['usernameemail', 'trim'],            
            ['usernameemail', 'validateUsername'],
            
        ];
    }

    public function attributeLabels()
    {
        return [
            'usernameemail' => Yii::t('app', 'User Name / Email Address'),          
        ];
    }

    public function validateUsername($attribute, $params)
    {
        if(!empty($this->usernameemail)){
            $userMod  = $this->getUserDataByNameOrEmail($this->usernameemail);
            if(empty($userMod))           
                $this->addError($attribute, 'User Name / Email Address is not valid');
            
        }
    }

    public static function getUserDataByNameOrEmail($userName){
       return User::find()
                    ->where('username=:username',[':username'=>$userName])
                    ->orWhere('email=:email',[':email'=>$userName])->one(); 
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail($model)
    {
     
      $resetLink = Yii::$app->urlManager->createAbsoluteUrl(['/user/password-reset/reset', 'token' => $model->password_reset_token]);
      $html = '';
      $html .= '<p>Hi <b>'.$model->first_name.'</b>. Your user name is <b>'.$model->username.'</b><p>';
      $html .= '<p>Your Password Reset Link is  <b><a href='.$resetLink.' target="_blank">Click Here</a></b> <p>';
      
      return  Yii::$app->mailer->compose()
        ->setFrom('support@unlocklive.com')
        ->setTo($model->email)
        ->setSubject(Yii::$app->setting->site_name .' Account password reset')
        ////->setTextBody('Plain text content')
        ->setHtmlBody($html)
        ->send();       
       
    }
}

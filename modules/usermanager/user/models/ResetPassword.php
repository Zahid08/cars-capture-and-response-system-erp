<?php
namespace unlock\modules\usermanager\user\models;

use Yii;
use yii\base\Model;
use yii\base\InvalidParamException;
use unlock\modules\usermanager\user\models\User;

/**
 * Password reset form
 */
class ResetPassword extends Model
{
    public $password;
    /**
     * @var \unlock\modules\usermanager\user\models\User
     */
    public $retypepassword;


     public function rules()
    {
        return [
            [['password','retypepassword'], 'required'], 
            ['retypepassword','compare','compareAttribute'=>'password'],
            ['password', 'string', 'length' => [5, 12]],
        ];
    }
    
    
    public function attributeLabels()
    {
        return [
            'password' => Yii::t('app', 'Password'),          
            'retypepassword' => Yii::t('app', 'Re-Password'),          
        ];
    }

   

   
}

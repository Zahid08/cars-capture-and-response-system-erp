<?php

namespace unlock\modules\usermanager\user\models;

use unlock\modules\organization\models\Organization;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\usermanager\role\models\Role;
use unlock\modules\core\components\recaptcha\ReCaptchaValidator;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property integer $id
 * @property integer $role_id
 * @property integer $additional_roles
 * @property integer $subtraction_roles
 * @property string $user_group
 * @property string $user_org_id
 * @property string $name
 * @property string $username
 * @property string $email
 * @property string $mobile
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property integer $force_password_change
 * @property string $company
 * @property string $phone
 * @property string $address
 * @property string $birthday
 * @property integer $gender
 * @property string $about_me
 * @property string $profile_image
 * @property string $logged_in_ip
 * @property string $logged_in_at
 * @property string $created_ip
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $sort_order
* @property integer $org_enlisted_status
* @property integer $status
 *
 * @property Sample[] $samples
 * @property Role $role
 * @property Sample[] $samples0
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_ACTIVE = 1;

    const STATUS_INACTIVE = 2;

    const STATUS_DELETED = -2;

    const SCENARIO_ADMIN_CREATE_USER = 'create-user';

    const SCENARIO_ADMIN_UPDATE_USER = 'update-user';

    const SCENARIO_PROFILE= 'profile';

    const SCENARIO_ACCOUNT = 'account';

    const SCENARIO_REGISTER = 'register';

    const SCENARIO_RESET = 'reset';

    const SCENARIO_VERIFICATION_CODE = 'verification-code';

    public $password;

    public $currentPassword;

    public $newPassword;

    public $newPasswordConfirm;

    public $verificationCode;

    public $reCaptcha;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            // Required
            [['username',  'password', 'user_group', 'role_id','email'], 'required', 'on' => self::SCENARIO_ADMIN_CREATE_USER],
            [['username', 'user_group', 'role_id','email'], 'required', 'on' => self::SCENARIO_ADMIN_UPDATE_USER],
            [['first_name', 'last_name'], 'required', 'on' => self::SCENARIO_PROFILE],
            [['currentPassword'], 'required', 'on' => self::SCENARIO_ACCOUNT],
            [['username', 'newPassword', 'newPasswordConfirm'], 'required', 'on' => self::SCENARIO_REGISTER],
            [['newPassword', 'newPasswordConfirm'], 'required', 'on' => self::SCENARIO_RESET],

            [['verificationCode'], 'required', 'on' => self::SCENARIO_VERIFICATION_CODE],

            // Username Rules
            ['username', 'trim'],
            ['username', 'unique', 'message' => Yii::t('app', 'This username has already been taken.'),'on' => self::SCENARIO_ADMIN_CREATE_USER],
            ['username', 'string', 'message' => Yii::t('app', 'This username has already been taken.'),'on' => self::SCENARIO_ADMIN_UPDATE_USER],
            ['username', 'string', 'min' => 2, 'max' => 255],

            // Email Rules
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'message' => Yii::t('app', 'This email address has already been taken.'),'on' => self::SCENARIO_ADMIN_CREATE_USER],
            ['email', 'string', 'message' => Yii::t('app', 'This email address has already been taken.'),'on' => self::SCENARIO_ADMIN_UPDATE_USER],

            // Password Rules
            [['newPassword', 'password'], 'string', 'min' => 3],
            [['newPassword', 'password'], 'filter', 'filter' => 'trim'],
            [['newPasswordConfirm'], 'compare', 'compareAttribute' => 'newPassword', 'message' => Yii::t('app', 'Passwords do not match')],
            [['currentPassword'], 'validateCurrentPassword'],

            // admin crud rules
            [['user_org_id', 'status', 'sort_order', 'role_id', 'force_password_change'], 'integer'],
            [['title','logged_in_at', 'created_at', 'updated_at', 'additional_roles', 'subtraction_roles'], 'safe'],
            [['user_group'], 'string', 'max' => 50],
            [['password_hash', 'password_reset_token', 'profile_image', 'logged_in_ip', 'created_ip'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['mobile'], 'string', 'max' => 100],
            [['first_name', 'last_name'], 'required'],
            [['footer_content'], 'safe'],
            [['sales_status'], 'safe'],

            [['reCaptcha'], ReCaptchaValidator::className(), 'secret' => '6LdyR1YUAAAAAGVVJMrCY3jEoTrY8za_hFGH_E9j', 'uncheckedMessage' => 'Please confirm that you are not a bot.', 'on' => [self::SCENARIO_REGISTER]],

        ];
    }

    /**
     * Validate current password (account page)
     */
    public function validateCurrentPassword()
    {
        if (!$this->validatePassword($this->currentPassword)) {
            $this->addError("currentPassword", Yii::t('app', 'Current password incorrect'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'role_id' => Yii::t('app', 'Role'),
            'additional_roles' => Yii::t('app', 'Additional Role'),
            'subtraction_roles' => Yii::t('app', 'Subtraction Role'),
            'user_group' => Yii::t('app', 'Department'),
            'user_org_id' => Yii::t('app', 'User Organization'),
            'username' => Yii::t('app', 'Username'),
            'email' => Yii::t('app', 'Email'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'newPassword' => Yii::t('app', 'Password'),
            'newPasswordConfirm' => Yii::t('app', 'Confirm Password'),
            'password_hash' => Yii::t('app', 'Password'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'force_password_change' => Yii::t('app', 'Force Password Change'),
            'verificationCode' => Yii::t('app', 'Verification Code'),
            'title' => Yii::t('app', 'Title'),
            'department' => Yii::t('app', 'Department'),
            'profile_image' => Yii::t('app', 'Profile Image'),
            'logged_in_ip' => Yii::t('app', 'Logged In Ip'),
            'logged_in_at' => Yii::t('app', 'Logged In At'),
            'created_ip' => Yii::t('app', 'Created Ip'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'sort_order' => Yii::t('app', 'Sort Order'),
            'status' => Yii::t('app', 'Status'),
            'footer_content' => Yii::t('app', 'User Footer'),
        ];
    }

    /** Get All Active User Drop Down List*/
    public static function userDropDownList($status = self::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'name');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_at = date('Y-m-d H:i:s');
                //$this->org_enlisted_status = CommonHelper::STATUS_PENDING;
            }
            else
            {
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }

    public static function countUsersByRole($roleId)
    {
        return User::find()
            ->where('role_id=:role_id', [':role_id' => $roleId])
            ->count();
    }

    public function countUsersByOrgId($orgId)
    {
        return User::find()
            ->where('user_org_id=:user_org_id', [':user_org_id' => $orgId])
            ->count();
    }


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(Role::className(), ['id' => 'role_id']);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Update login info (ip and time)
     * @return bool
     */
    public function updateLoginMeta()
    {
        $this->logged_in_ip = Yii::$app->request->userIP;
        $this->logged_in_at = gmdate("Y-m-d H:i:s");
        return $this->save(false, ["logged_in_ip", "logged_in_at"]);
    }

    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->role_id = CommonHelper::ROLE_REGISTERED_PK;
        $user->user_group = CommonHelper::USER_GROUP_REGISTERED;
        $user->first_name = $this->first_name;
        $user->last_name = $this->last_name;
        $user->username = $this->username;
        if(!empty($this->email)){
            $user->email = $this->email;
        }
        if(!empty($this->mobile)){
            $user->mobile = $this->mobile;
        }

        $user->setPassword($this->password);
        $user->generateAuthKey();
        if (!$user->save()) {
            throw new Exception(Yii::t('app', Html::errorSummary($user)));
        }

        return $user;
    }

    public static function getLoggedInUserAdditionalRoles()
    {
        $roleIds = [];

        $roles = isset(Yii::$app->user->identity->additional_roles) ? Yii::$app->user->identity->additional_roles : null;

        if($roles){
            $roleIds = array_map('intval', explode(',', $roles));
        }

        array_push($roleIds, CommonHelper::ROLE_GUEST_PK);

        return $roleIds;
    }

    public static function getLoggedInUserSubtractionRoles()
    {
        $roles = isset(Yii::$app->user->identity->subtraction_roles) ? Yii::$app->user->identity->subtraction_roles : null;

        if($roles){
            $roleIds = array_map('intval', explode(',', $roles));
            return $roleIds;
        }

        return null;
    }

    public static function setForcePasswordChange($userId)
    {
        if(empty($userId)){
            return false;
        }

        $user = User::find()
            ->where(['id' => $userId])
            ->one();

        if($user){
            $user->force_password_change = 1;
            $user->save(false);

            return true;
        }
    }

    public function sendRegisterEmail($email, $verificationCode)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'createAccount-html', 'text' => 'createAccount-text'],
                [
                    'email' => $email,
                    'verificationCode' => $verificationCode,
                    'siteName' => Yii::$app->setting->site_name,
                    'senderName' => Yii::$app->setting->support_sender_name,
                    'senderEmail' => Yii::$app->setting->support_sender_email
                ]
            )
            ->setFrom([Yii::$app->setting->support_sender_email => Yii::$app->setting->support_sender_name])
            ->setTo($email)
            ->setSubject('Verify your email address')
            ->send();
    }

    public function updateOrgEnlistedStatus($userId, $orgEnlistedStatus){
        $model = User::findIdentity($userId);
        if($model){
            $model->org_enlisted_status = $orgEnlistedStatus;
            $model->save();
        }
    }
    
    
    public static function getActiveUser()
    {
        $model = self::find()
            ->where('status =:status', [':status' => 1])         
            ->andWhere('role_id =:role_id', [':role_id' => 2])
            ->orderBy(  ['first_name' => SORT_ASC])
            ->all();
        return  ArrayHelper::map( $model,'id',  function($model) {
            return $model['first_name'].' '.$model['last_name'];
        });
    }

    public static function getActiveSalesUser()
    {
        $model = self::find()
            ->where('status =:status', [':status' => 1])
            /*->andWhere('role_id =:role_id', [':role_id' => 2])*/
            ->andWhere('sales_status =:sales_status', [':sales_status' => 1])
            ->orderBy(  ['first_name' => SORT_ASC])
            ->all();
        return  ArrayHelper::map( $model,'id',  function($model) {
            return $model['first_name'].' '.$model['last_name'];
        });
    }


}

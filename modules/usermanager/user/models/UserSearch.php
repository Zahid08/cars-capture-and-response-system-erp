<?php

namespace unlock\modules\usermanager\user\models;

use Yii;
use yii\base\Model;
use unlock\modules\core\data\ActiveDataProvider;
use unlock\modules\usermanager\user\models\User;
use unlock\modules\core\helpers\CommonHelper;

/**
 * UserSearch represents the model behind the search form about `unlock\modules\usermanager\user\models\User`.
 */
class UserSearch extends User
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'role_id', 'force_password_change', 'status', 'created_at', 'updated_at', 'sort_order', 'additional_roles', 'subtraction_roles'], 'integer'],
            [['first_name', 'last_name', 'title', 'department', 'user_group', 'username', 'auth_key', 'password_hash', 'password_reset_token', 'email', 'name', 'company', 'mobile', 'profile_image', 'logged_in_ip', 'logged_in_at', 'created_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        // add conditions that should always apply here
        $pageSize = isset($params['per-page']) ? intval($params['per-page']) : CommonHelper::GRID_PER_PAGE;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' =>  ['pageSize' => $pageSize],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'role_id' => $this->role_id,
            'force_password_change' => $this->force_password_change,
            'status' => $this->status,
            'logged_in_at' => $this->logged_in_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'sort_order' => $this->sort_order,
        ]);

        $query->andFilterWhere(['like', 'user_group', $this->user_group])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'mobile', $this->mobile])
            ->andFilterWhere(['like', 'profile_image', $this->profile_image])
            ->andFilterWhere(['like', 'logged_in_ip', $this->logged_in_ip])
            ->andFilterWhere(['like', 'created_ip', $this->created_ip]);

        $query->orFilterWhere(['like', 'additional_roles', $this->role_id]);

        // Deleted User
        $query->andWhere(['!=', 'status', User::STATUS_DELETED]);

        // User Group
        $userGroup = CommonHelper::getLoggedInUserGroup();
        if($userGroup != CommonHelper::USER_GROUP_ADMIN){
            $query->andWhere(['user_group' => $userGroup]);
        }

        return $dataProvider;
    }
}

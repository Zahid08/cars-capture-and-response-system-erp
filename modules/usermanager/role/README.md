Role Based Access Control (RBAC)
=======================================================================================================================

Authorization is the process of verifying that a user has enough permission to do something. 
Yii provides two authorization methods: Access Control Filter (ACF) and Role-Based Access Control (RBAC).

Migration
-----------------------------------------------------------------------------------------------------------------------

**Create Database**

php yii migrate/up --migrationPath=@unlock/modules/usermanager/role/migrations

**Delete Database**

php yii migrate/down --migrationPath=@unlock/modules/usermanager/role/migrations

Configuration
-----------------------------------------------------------------------------------------------------------------------

**Module Setup**

'modules' => [
    'role' => [
        'class' => 'unlock\modules\usermanager\role\RoleModule',
    ],
],

Left Menu
-----------------------------------------------------------------------------------------------------------------------
[
    'label' => '<i class="fa fa-users"></i> <span>Users</span>',
    'url' => ['#'],
    'visible'=> Yii::$app->user->checkAccess('user/admin', 'index') ||
                Yii::$app->user->checkAccess('role/role', 'index') ||
                Yii::$app->user->checkAccess('permission/accesscontrol', 'assign-permission'),
    'template' => '<a class="dropdown-toggle" href="{url}" >{label}</a>',
    'items' => [
        [
            'label' => '<span>Users</span>',
            'url' => ['/user/admin/index'],
            'visible'=> Yii::$app->user->checkAccess('user/admin', 'index'),
            'active' => LeftMenu::isActive(['user/admin/']),
        ],
        [
            'label' => '<span>Roles</span>',
            'url' => ['/role/role/index'],
            'visible'=> Yii::$app->user->checkAccess('role/role', 'index'),
            'active' => LeftMenu::isActive(['role/role/']),
        ],
        [
            'label' => '<span>Permissions</span>',
            'url' => ['/permission/access-control/assign-permission'],
            'visible'=> Yii::$app->user->checkAccess('permission/accesscontrol', 'assign-permission'),
            'active' => LeftMenu::isActive(['permission/access-control/']),
        ],
    ],
],

URLs
-----------------------------------------------------------------------------------------------------------------------
/role/role/index


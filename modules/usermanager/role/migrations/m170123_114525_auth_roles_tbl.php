<?php

use yii\db\Migration;

/**
 * Class m170123_114525_auth_roles_tbl
 */
class m170123_114525_auth_roles_tbl extends Migration
{
    /**
     * This method contains the logic to be executed when applying this migration.
     */
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%auth_roles}}', [
            'id'            => $this->primaryKey(),
            'role_type'    => $this->smallInteger(2)->notNull()->defaultValue('1')->comment('1=>Main Rule, 2=>Additional Rule'),
            'user_group'    => $this->string(50)->notNull(),
            'name'          => $this->string(255)->notNull(),
            'description'   => $this->string(255)->notNull(),
            'is_super_admin'=> $this->smallInteger(2)->defaultValue(0)->comment('0=>No, 1=>Yes'),
            'created_by'    => $this->integer(),
            'created_at'    => $this->dateTime(),
            'updated_by'    => $this->integer(),
            'updated_at'    => $this->dateTime(),
            'status'        => $this->smallInteger(2)->defaultValue('1')->comment('1=>Active, 2=>Inactive'),
        ], $tableOptions);

        //Insert Data
        $this->insert('{{%auth_roles}}', [
            'role_type' => 1,
            'user_group' => 'admin',
            'name' => 'superAdmin',
            'description' => 'Super Admin',
            'is_super_admin' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'status' => 1,
        ]);

        $this->insert('{{%auth_roles}}', [
            'role_type' => 1,
            'user_group' => 'admin',
            'name' => 'admin',
            'description' => 'Administrator',
            'created_at' => date('Y-m-d H:i:s'),
            'status' => 1,
        ]);

        $this->insert('{{%auth_roles}}', [
            'role_type' => 1,
            'user_group' => 'registered',
            'name' => 'registered',
            'description' => 'Registered',
            'created_at' => date('Y-m-d H:i:s'),
            'status' => 1,
        ]);

        $this->insert('{{%auth_roles}}', [
            'role_type' => 1,
            'user_group' => '',
            'name' => 'guest',
            'description' => 'Guest',
            'created_at' => date('Y-m-d H:i:s'),
            'status' => 1,
        ]);

        // Foreign Keys
        $this->addForeignKey('fk_authRoles_user_createdBy', '{{%auth_roles}}', 'created_by', '{{%user}}', 'id');
        $this->addForeignKey('fk_authRoles_user_updatedBy', '{{%auth_roles}}', 'updated_by', '{{%user}}', 'id');
    }

    /**
     * This method contains the logic to be executed when removing this migration.
     */
    public function down()
    {
        // Foreign Keys
        $this->dropForeignKey('fk_authRoles_user_createdBy', '{{%auth_roles}}');
        $this->dropForeignKey('fk_authRoles_user_updatedBy', '{{%auth_roles}}');

        $this->dropTable('{{%auth_roles}}');
    }
}

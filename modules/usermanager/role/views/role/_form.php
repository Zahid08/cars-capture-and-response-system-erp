<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\jui\DatePicker;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\components\FileInput;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\BackButton;
use unlock\modules\core\buttons\FormButtons;

/* @var $this yii\web\View */
/* @var $model unlock\modules\usermanager\role\models\Role */
/* @var $form yii\bootstrap\ActiveForm */

$this->title= ($model->isNewRecord) ? Yii::t('app', 'Create Role') : Yii::t('app', 'Update Role');
?>

<div class="main-container" role="main">
    <div class="page-header">
        <div class="page-header-title">
            <h1 class="page-title"><?= Yii::t('app', 'Roles') ?></h1>
        </div>
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a(Yii::t('app', 'Home'), Yii::$app->homeUrl) ?></li>
                <li><a href="<?= Url::toRoute(['/user/admin/index']) ?>" target="_blank">Users</a></li>
                <li><?= Html::a(Yii::t('app', 'Roles'), Url::to('index')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-form-container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fas fa-pencil-alt"></i> <?= Html::encode($this->title) ?></h3>
                </div>
                <div class="panel-heading-button">
                    <?= BackButton::widget() ?>
                </div>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
                    'validateOnBlur' =>true,
                    'enableAjaxValidation'=>true,
                    'errorCssClass'=>'has-error',
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
                        'options' => ['class' => 'form-group'],
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-2',
                            'offset' => '',
                            'wrapper' => '',
                            'hint' => '',
                        ],
                    ],
                ]); ?>

                <?= $form->field($model, 'user_group')->dropDownList(CommonHelper::getUserGroupList(), ['prompt'=>'Select '.$model->getAttributeLabel('user_group')]) ?>

                <?php

                if($model->isNewRecord){
                    echo $form->field($model, 'name')->textInput(['maxlength' => true]);
                }
                else{
                    echo $form->field($model, 'name', ['inputTemplate' => $model->name])->textInput(['maxlength' => true]);
                }
                ?>

                <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'role_type')->dropDownList(CommonHelper::roleTypeDropDownList()) ?>

                <?= $form->field($model, 'status')->dropDownList(CommonHelper::statusDropDownList()) ?>

                <div class="admin-form-button">
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <?= FormButtons::widget() ?>
                        </div>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

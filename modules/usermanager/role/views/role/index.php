<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\buttons\NewButton;
use unlock\modules\core\buttons\ExportCsvButton;
use unlock\modules\core\buttons\ExportPdfButton;
use unlock\modules\core\buttons\ResetFilterButton;
use unlock\modules\usermanager\user\models\User;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $searchModel unlock\modules\usermanager\role\models\RoleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Roles');
?>
<style type="text/css">
    .badge{
        margin: 0 0 5px 0;
    }
</style>
<div class="main-container" role="main">
    <div class="page-header">
        <!--<div class="page-header-title">
            <h1 class="page-title"><?/*= Html::encode($this->title) */?></h1>
        </div>-->
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li><a href="<?= Url::toRoute(['/user/admin/index']) ?>" target="_blank">Users</a></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-grid-filter">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <div class="admin-grid-content">
        <div class="admin-grid-toolbar">
            <div class="pull-right admin-grid-toolbar-left">
                <?= Summary::widget(['dataProvider' => $dataProvider]) ?>
            </div>
            <div class="admin-grid-toolbar-right">
                <?= NewButton::widget(['url' => Url::toRoute(['create'])]) ?>
            </div>
        </div>

        <div class="admin-grid-view">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
                'pager' => ['class' => LinkPager::className()],
                'columns' => [
                    ['class' => 'unlock\modules\core\grid\SerialColumn'],
                    'name',
                    'description',
                    [
                        'attribute' => 'user_group',
                        'value' => function($data){
                            return CommonHelper::getUserGroupList($data->user_group);
                        },
                        'filter' => Html::activeDropDownList($searchModel, 'user_group', CommonHelper::getUserGroupList(), ['prompt' => CommonHelper::GRID_FILTER_PROMPT]),
                    ],
                    [
                        'attribute' => 'id',
                        'value' => function($data){
                            return '<span class="badge">'. User::countUsersByRole($data->id) . '</span>';
                        },
                        'label' => 'Number of Users',
                        'format' => 'raw',
                        'contentOptions' =>['style'=>'text-align:center;'],
                    ],
                    [
                        'attribute' => 'role_type',
                        'value' => function($data){
                            return CommonHelper::roleTypeDropDownList($data->role_type);
                        },
                        'filter' => Html::activeDropDownList($searchModel, 'role_type', CommonHelper::roleTypeDropDownList(), ['prompt' => CommonHelper::GRID_FILTER_PROMPT]),
                    ],
                    [
                        'attribute' => 'status',
                        'format' => 'status',
                        'filter' => Html::activeDropDownList($searchModel, 'status', CommonHelper::statusDropDownList(), ['prompt' => CommonHelper::GRID_FILTER_PROMPT]),
                    ],
                    ['class' => 'unlock\modules\core\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>

</div>

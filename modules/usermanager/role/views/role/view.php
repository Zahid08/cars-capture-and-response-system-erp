<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use unlock\modules\core\buttons\BackButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $model unlock\modules\usermanager\role\models\Role */

$this->title = Yii::t('app', 'View Roles');
?>
<div class="main-container" role="main">
    <div class="page-header">
        <!--<div class="page-header-title">
            <h1 class="page-title"><?/*= Yii::t('app', 'Roles') */?></h1>
        </div>-->
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li><a href="<?= Url::toRoute(['/user/admin/index']) ?>" target="_blank">Users</a></li>
                <li><?= Html::a(Yii::t('app', 'Roles'), Url::to('index')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-view-container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fa fa-eye"></i> <?= Yii::t('app', 'View') ?> - <?= Html::encode($model->description) ?></h3>
                </div>
                <div class="panel-heading-button">
                    <?= BackButton::widget() ?>
                </div>
            </div>
            <div class="panel-body">
                <div class="sample-view">
                    <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'name',
                        'description',
                        [
                            'attribute' => 'role_type',
                            'value' => function($model){
                                return CommonHelper::roleTypeDropDownList($model->role_type);
                            }
                        ],
                        'user_group',
                        'is_super_admin:boolean',
                        'status:status',
                    ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

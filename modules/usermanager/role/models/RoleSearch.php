<?php

namespace unlock\modules\usermanager\role\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use unlock\modules\usermanager\role\models\Role;
use unlock\modules\core\helpers\CommonHelper;

/**
 * RoleSearch represents the model behind the search form about `unlock\modules\usermanager\role\models\Role`.
 */
class RoleSearch extends Role
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_super_admin', 'role_type', 'created_by', 'updated_by', 'status'], 'integer'],
            [['name', 'description', 'created_at', 'updated_at', 'user_group'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Role::find();

        // add conditions that should always apply here
        $pageSize = isset($params['per-page']) ? intval($params['per-page']) : CommonHelper::GRID_PER_PAGE;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' =>  ['pageSize' => $pageSize],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'role_type' => $this->role_type,
            'is_super_admin' => $this->is_super_admin,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'user_group', $this->user_group])
            ->andFilterWhere(['like', 'description', $this->description]);

        $query->andWhere(['!=', 'name', CommonHelper::ROLE_GUEST]);
        $query->andWhere(['!=', 'name', CommonHelper::ROLE_REGISTERED]);

        return $dataProvider;
    }
}

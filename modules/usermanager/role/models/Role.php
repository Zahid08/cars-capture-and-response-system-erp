<?php

namespace unlock\modules\usermanager\role\models;

use Yii;
use yii\helpers\ArrayHelper;
use unlock\modules\core\helpers\CommonHelper;

/**
 * This is the model class for table "{{%auth_roles}}".
 *
 * @property string $id
 * @property string $role_type
 * @property string $user_group
 * @property string $name
 * @property string $description
 * @property integer $is_super_admin
 * @property integer $created_by
 * @property string $created_at
 * @property integer $updated_by
 * @property string $updated_at
 * @property integer $status
 */
class Role extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%auth_roles}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_group', 'name', 'description'], 'required'],
            [['is_super_admin', 'role_type', 'created_by', 'updated_by', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'description', 'user_group'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'role_type' => Yii::t('app', 'Role Type'),
            'user_group' => Yii::t('app', 'Department'),
            'name' => Yii::t('app', 'Role Name'),
            'description' => Yii::t('app', 'Description'),
            'is_super_admin' => Yii::t('app', 'Is Super Admin'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
                $this->role_type = isset($this->role_type) ? $this->role_type : CommonHelper::ROLE_TYPE_MAIN;
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
                $this->role_type = isset($this->role_type) ? $this->role_type : CommonHelper::ROLE_TYPE_MAIN;
            }
            return true;
        }
        else return false;
    }

    public static function rolesDropDownList($group = null){
        $roles = self::find()->select('id, description')
            ->where(['status' => CommonHelper::STATUS_ACTIVE])
            ->andFilterWhere(['=', 'user_group', $group])
            ->all();

        return ArrayHelper::map($roles, 'id', 'description');
    }

    public static function getRoleListById($roleId, $additionalRoles)
    {
        $rolesIds = [];
        $roles[] = $roleId;
        if($additionalRoles){
            $additionalRoles = explode(',', $additionalRoles);
            $rolesIds = array_merge($roles, $additionalRoles);
        }
        else{
            $rolesIds = $roles;
        }

        $roles = self::find()->select('id, description')
            ->where(['status' => CommonHelper::STATUS_ACTIVE])
            ->andFilterWhere(['id' => $rolesIds])
            ->andFilterWhere(['!=', 'id', CommonHelper::ROLE_GUEST_PK])
            ->all();

        return ArrayHelper::map($roles, 'id', 'description');
    }

    public function checkPermission($permission)
    {
        return (bool)$this->is_super_admin;
    }

    public static function getRoleById($id)
    {
        return Role::find()->where('id=:id', [':id' => $id])->one();
    }


    public function getRoleListByName($role)
    {
        $list = Role::find()
                ->select('id')
                ->where(['name' => $role])
                ->all();

        return ArrayHelper::map($list, 'id', 'id');
    }

    public function getRoleIdByUserGroup($group)
    {
        $list = Role::find()
            ->select('id')
            ->where(['user_group' => $group])
            ->all();

        return ArrayHelper::map($list, 'id', 'id');
    }

    public static function hasAdditionalRole($userGroup = null)
    {
        $count = self::find()
            ->where(['status' => CommonHelper::STATUS_ACTIVE])
            ->andWhere(['role_type' => CommonHelper::ROLE_TYPE_ADDITIONAL])
            ->andFilterWhere(['user_group' => $userGroup])
            ->count();

        return ($count == 0) ? false : true;
    }

    public static function hasSubtractionRole($userGroup = null)
    {
        $count = self::find()
            ->where(['status' => CommonHelper::STATUS_ACTIVE])
            ->andWhere(['role_type' => CommonHelper::ROLE_TYPE_SUBTRACTION])
            ->andFilterWhere(['user_group' => $userGroup])
            ->count();

        return ($count == 0) ? false : true;
    }

    public function getAdditionalRoleTypeDropDownList($userGroup = null)
    {
        $list = Role::find()
            ->where(['role_type' => CommonHelper::ROLE_TYPE_ADDITIONAL])
            ->andFilterWhere(['user_group' => $userGroup])
            ->orderBy(['user_group' => SORT_ASC, 'role_type' => SORT_ASC])
            ->all();

        return ArrayHelper::map($list, 'id', 'description');
    }

    public function getSubtractionRoleTypeDropDownList($userGroup = null)
    {
        $list = Role::find()
            ->where(['role_type' => CommonHelper::ROLE_TYPE_SUBTRACTION])
            ->andFilterWhere(['user_group' => $userGroup])
            ->orderBy(['user_group' => SORT_ASC, 'role_type' => SORT_ASC])
            ->all();

        return ArrayHelper::map($list, 'id', 'description');
    }
}

<?php

namespace unlock\modules\usermanager\role;

/**
 * Class RoleModule
 */
class RoleModule extends \yii\base\Module
{
    /**
     * @var string the namespace that controller classes are in
     */
    public $controllerNamespace = 'unlock\modules\usermanager\role\controllers';

    /**
     * @var string the namespace that view file are in
     */
    public $viewNamespace = '@unlock/modules/usermanager/role/views/';
}

<?php

namespace unlock\modules\generalsettings\models;

use Yii;
use yii\helpers\ArrayHelper;
use unlock\modules\core\helpers\CommonHelper;

/**
 * This is the model class for table "{{%city}}".
 *
 * @property integer $id
 * @property string $city_name
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property integer $status
 */
class City extends \yii\db\ActiveRecord
{
    public $allsearch;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%setting_city}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['province_id','created_by', 'updated_by'], 'integer'],
            [['city_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'allsearch' => Yii::t('app', 'Search City Name'),
            'id' => Yii::t('app', 'ID'),
            'province_id' => Yii::t('app', 'Province Name'),
            'city_name' => Yii::t('app', 'City Name'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            //'status' => Yii::t('app', 'Status'),
        ];
    }

    /** Get All Active City Drop Down List*/
    public static function cityDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }
}

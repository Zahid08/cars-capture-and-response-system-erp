<?php

namespace unlock\modules\generalsettings;

/**
 * Class GeneralSettings
 */
class GeneralSettings extends \yii\base\Module
{
    /**
     * @var string the namespace that controller classes are in
     */
    public $controllerNamespace = 'unlock\modules\generalsettings\controllers';

    /**
    * @var string the namespace that view file are in
    */
    public $viewNamespace = '@unlock/modules/generalsettings/views/';
}

<?php

use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\grid\GridView;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\buttons\NewButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $searchModel unlock\modules\generalsettings\models\ProvinceSearch */
/* @var $dataProvider unlock\modules\core\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Province');
?>
<div class="main-container" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li><?= Html::a(Yii::t('app', 'Settings'), Yii::$app->homeUrl.Url::to('/site/settings')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-grid-filter">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <div class="admin-grid-content">
        <div class="admin-grid-toolbar">
            <div class="admin-grid-toolbar-left">
                <?= NewButton::widget(['url' => Url::toRoute(['create'])]) ?>
            </div>
            <div class="admin-grid-toolbar-right">
                <?= Summary::widget(['dataProvider' => $dataProvider]) ?>
            </div>
        </div>

        <div class="admin-grid-view">
                            <?php 
                echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
                'pager' => ['class' => LinkPager::className()],
                'columns' => [
                    ['class' => 'unlock\modules\core\grid\SerialColumn'],

                    [
                        'attribute' => 'province_name',
                        'format'    => 'html',
                        'value' => function($model){
                            $name = Html::a($model->province_name, ['update', 'id' => $model->id]);
                            return $name;
                        }
                    ],
                    'province_code',
                    [
                            'attribute' => 'country_id',
                            'headerOptions' => ['style' => 'width:20%'],
                            'value' => function($model){
                                $country = CommonHelper::countryListByCountryId($model->country_id);
                                $countryName = '';
                                if(!empty($country->country_name)){
                                    $countryName = $country->country_name;
                                }
                                return $countryName;
                            }
                    ],

                    //'created_at:datetime',
			// 'created_by',
			// 'updated_at',
			// 'updated_by',
                //['class' => 'unlock\modules\core\grid\ActionColumn'],
                ],
                ]);   ?>
                    </div>
    </div>

</div>

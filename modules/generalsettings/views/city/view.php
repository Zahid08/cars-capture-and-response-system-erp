<?php

use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\widgets\DetailView;
use unlock\modules\core\buttons\BackButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $model unlock\modules\generalsettings\models\City */

$this->title = Yii::t('app', 'View City');
?>
<div class="main-container main-container-view" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li><?= Html::a(Yii::t('app', 'City'), Url::to('index')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-view-container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fa fa-eye"></i> <?= Yii::t('app', 'View') ?> - <?= Html::encode($model->id) ?></h3>
                </div>
                <div class="panel-heading-button">
                    <?= BackButton::widget() ?>
                </div>
            </div>

            <div class="panel-body">
                <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                	'id',
                    [
                        'attribute' => 'province_id',
                        'value' => function($model){
                            $province = CommonHelper::provinceListByProvinceId($model->province_id);
                            $provinceName = '';
                            if(!empty($province->province_name)){
                                $provinceName = $province->province_name;
                            }
                            return $provinceName;
                        }
                    ],
					'city_name',
					'created_at:datetime',
					'created_by:user',
					'updated_at:datetime',
					'updated_by:user',
                ],
                ]) ?>

                <div class="row">
                    <div class="col-md-4 text-left">
                        <?= Html::a('Back to City List', ['city/index'], ['class' => 'btn btn-primary']) ?>
                    </div>
                    <div class="col-md-4 text-center">
                        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    </div>
                    <div class="col-md-4 text-right">
                        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<?php

namespace unlock\modules\core\components;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\widgets\InputWidget;

/**
 * Widget of the phone input
 * @package borales\extensions\phoneInput
 */
class FileInput extends InputWidget
{
    public $thumbSize = ['100%', '100%']; // ['120px', '120px']

    public $template = '{preview}{input}'; // {preview}{delete}{input}{download}

    public $fileBaseUrl = '';

    public $dirName = '';

    public function init()
    {
        parent::init();

        if(empty($this->fileBaseUrl)){
            $this->fileBaseUrl = Yii::getAlias('@baseUrl') . '/media/' . $this->dirName . '/';
        }
    }

    /**
     * @return string
     */
    public function run()
    {
        $modelName = StringHelper::basename(get_class($this->model));

        /* START: {preview} */
        if ($this->hasModel()) {
            $src = Html::getAttributeValue($this->model, $this->attribute);
            $this->options['hiddenOptions']['value'] = $src;
        }
        else {
            $src = $this->value;
            $this->options['hiddenOptions']['value'] = $src;
        }

        $preview = '';
        $previewId = $this->attribute . '_preview_img';
        if (!empty($src)) {
            $src = $this->fileBaseUrl . $src;
            $preview = '<span class="image-input-preview">';
            $preview .= '<img src="'.$src.'" id="'.$previewId.'" class="small-image-preview v-middle">';
            $preview .= '</span>';
        }
        $replace['{preview}'] = $preview;
        /* END: {preview} */

        /* START: {download} */
        if ($this->hasModel()) {
            $src = Html::getAttributeValue($this->model, $this->attribute);
        } else {
            $src = $this->value;
        }

        $downloadHtml = '';
        $downloadId = $this->attribute . '_download_file';
        if (!empty($src)) {
            $src = $this->fileBaseUrl . $src;
            $downloadHtml = '<span class="download-file">';
            $downloadHtml .=  Html::a('<span class="fa fa-download"></span> Download</a>', $src, ['id' => $downloadId, 'class' => 'btn btn-primary download-file-link', 'target' => '_blank', 'title' => 'Download']);
            $downloadHtml .= '</span>';
        }
        $replace['{download}'] = $downloadHtml;
        /* END: {download} */

        /* START: {input} */
        if ($this->hasModel()) {
            $fileInputField = Html::activeFileInput($this->model, $this->attribute, $this->options);
        }
        else {
            $fileInputField = Html::fileInput($this->name, $this->value, $this->options);
        }
        $fileInput = '<span class="image-input-browse">';
        $fileInput .= $fileInputField;
        $fileInput .= '</span>';

        $replace['{input}'] = $fileInput;
        /* END: {input} */

        /* START: {delete} */
        $delete = '';
        $deleteId = $this->id . '_preview_img_delete';
        if (!empty($src)) {
            $delete = '<span class="image-input-delete">';
            $delete .= '<input name="FileDelete['.$this->attribute.']" value="1" class="checkbox" id="'.$deleteId.'" type="checkbox">';
            $delete .= '<label for="'.$deleteId.'"> Delete Image</label>';
            $delete .= '</span>';
        }
        $replace['{delete}'] = $delete;
        /* END: {delete} */

        echo $this->template = strtr($this->template, $replace);

        $this->registerClientScript();
    }

    public function registerClientScript()
    {
        $width = $this->thumbSize[0];
        $height = $this->thumbSize[1];

        $this->view->registerCss('.small-image-preview {max-width:'.$width.'; max-height:'.$height.'}');

        $id = "#" . $this->options['id'];
        $this->view->registerJs("jQuery('" . $id . "').change(function(){var fileName = jQuery(this).val();jQuery(this).parent().find('input[type=hidden]').val(fileName);});");
    }
}
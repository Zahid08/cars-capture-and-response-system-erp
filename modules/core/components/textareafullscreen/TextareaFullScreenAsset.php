<?php

namespace unlock\modules\core\components\textareafullscreen;

use yii\web\AssetBundle;

class TextareaFullScreenAsset extends AssetBundle
{
    public $sourcePath = '@unlock/modules/core/components/textareafullscreen/assets';

    public $js = [
        'js/jquery.textareafullscreen.js',
    ];

    public $css = [
        'css/textareafullscreen.css'
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}

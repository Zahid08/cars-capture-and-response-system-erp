<?php

namespace unlock\modules\core\components\textareafullscreen;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\InputWidget;

/**
 * Widget of the phone input
 * @package borales\extensions\phoneInput
 */
class TextareaFullScreen extends InputWidget
{
    public $options = ['class' => 'form-control'];

    var $overlay = true;

    var $maxWidth = '40%';

    var $maxHeight = '40%';

    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        /* START: {input} */
        if ($this->hasModel()) {
            echo Html::activeTextarea($this->model, $this->attribute, $this->options);
        }
        else {
            echo Html::textarea($this->name, $this->value, $this->options);
        }
        /* END: {input} */

        $this->registerAssets();
    }

    public function registerAssets()
    {
        $view = $this->getView();
        TextareaFullScreenAsset::register($view);

        $id = "#" . $this->options['id'];
        $clientOptions = Json::encode([
            'overlay' => $this->overlay,
            'maxWidth' => $this->maxWidth,
            'maxHeight' => $this->maxHeight,
        ]);

        $view->registerJs("jQuery('" . $id . "').textareafullscreen(" . $clientOptions . ");");
    }
}
<?php

namespace unlock\modules\core\components\fancybox;

use yii\web\AssetBundle;

/**
 * How to Use
 *
 * Load:
 * 1.
 * FancyBoxAsset::register($this);
 *
 * 2.
 * FancyBoxAsset::register(Yii::$app->view);
 *
 * 3.
 * $view = $this->getView();
 * FancyBoxAsset::register($view);
 */

class FancyBoxAsset extends AssetBundle{

    public $sourcePath = '@unlock/modules/core/components/fancybox/assets';
    
    public $css = [
        'css/jquery.fancybox.css',
        'css/jquery.fancybox-buttons.css',
        'css/jquery.fancybox-thumbs.css',
    ];
    
    public $js = [
        'js/jquery.fancybox.pack.js',
        'js/jquery.mousewheel.pack.js',
        'js/jquery.fancybox-buttons.js',
        'js/jquery.fancybox-thumbs.js',
        'js/jquery.fancybox-media.js',
    ];
    
    public $depends = [
        'yii\web\JqueryAsset',
    ];
    
}

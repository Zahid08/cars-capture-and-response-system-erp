<?php

namespace unlock\modules\core\components;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\widgets\InputWidget;
use kartik\select2\Select2;
use unlock\modules\shiporg\models\ShipOrg;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\helpers\PermissionHelper;

/**
 * Widget of the phone input
 * @package borales\extensions\phoneInput
 */
class FinancialYearField extends InputWidget
{
    public $inputType = 'hidden'; // hidden | drop-down

    public function init()
    {
        parent::init();

        if (!isset($this->model->financial_year)) {
            $this->model->financial_year = CommonHelper::getCurrentFinancialYear();
        }
    }

    /**
     * @return string
     */
    public function run()
    {
        $modelName = StringHelper::basename(get_class($this->model));

        /* START: {input} */
        if ($this->hasModel()) {
            if(PermissionHelper::checkIsAllowEntryPreviousFinancialYear() || $this->inputType == 'drop-down'){
                echo Html::activeDropDownList($this->model, $this->attribute, CommonHelper::financialYearDropDownList(), ['class'=>'form-control']);
            }
            else{
                echo Html::activeHiddenInput($this->model, $this->attribute, $this->options);
                echo CommonHelper::getCurrentFinancialYear();
            }
        }
        else {
            if(PermissionHelper::checkIsAllowEntryPreviousFinancialYear() || $this->inputType == 'drop-down'){
                echo Html::dropDownList($modelName.'['.$this->attribute.']', $this->name, CommonHelper::financialYearDropDownList(), ['class'=>'form-control']);
            }
            else{
                echo Html::hiddenInput($modelName.'['.$this->attribute.']', CommonHelper::getCurrentFinancialYear());
                echo CommonHelper::getCurrentFinancialYear();
            }
        }
        /* END: {input} */

    }
}
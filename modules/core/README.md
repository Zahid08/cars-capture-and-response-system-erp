Core
=======================================================================================================================

Core for every Site

Migration
-----------------------------------------------------------------------------------------------------------------------

**Create Database**

php yii migrate/up --migrationPath=@unlock/modules/core/migrations

**Delete Database**

php yii migrate/down --migrationPath=@unlock/modules/core/migrations

Configuration
-----------------------------------------------------------------------------------------------------------------------

**Module Setup**

common/config/main.php

'modules' => [
    'core' =>  [
        'class' => 'unlock\modules\core\CoreModule',
    ],
],

Usage
-----------------------------------------------------------------------------------------------------------------------
Yii::$app->getModule('core')->wysiwyg_class_name(),
Yii::$app->getModule('core')->wysiwyg_params()


Bulk Button
-----------------------------------------------------------------------------------------------------------------------
New 
<?= NewButton::widget(['url' => Url::toRoute(['create'])]) ?>

Edit
<?= EditButton::widget(['url' => Url::toRoute(['update', 'id' => ''])]) ?>

Delete
<?= DeleteButton::widget(['url' => Url::toRoute(['mass-delete'])]); ?>

Active
<?= ActiveButton::widget(['url' => Url::toRoute(['mass-active'])]) ?>

Inactive
<?= InactiveButton::widget(['url' => Url::toRoute(['mass-inactive'])]) ?>

Export
<?= ExportCsvButton::widget(['url' => Url::toRoute(['export-csv'])]) ?>

Reset Filter
<?= ResetFilterButton::widget() ?>

Advance Search
<?= AdvancedSearchButton::widget(['url' => Url::toRoute(['advanced-search'])]) ?>

Divider
<?= VerticalDivider::widget(['url' => Url::toRoute(['vertical-divider-1'])]); ?>

GII
-----------------------------------------------------------------------------------------------------------------------
File Path: backend/config/main-local.php

$config['modules']['gii'] = [
    'class' => 'yii\gii\Module',
    'generators' => [
        'crud' => [
            'class' => 'unlock\modules\core\gii\generators\crud\Generator',
            'templates' => [
                'unlock' => '@unlock/modules/core/gii/generators/crud/default',
            ]
        ],
        'model' => [
            'class' => 'yii\gii\generators\model\Generator',
            'templates' => [
                'unlock' => '@unlock/modules/core/gii/generators/model/default',
            ]
        ],
        'module' => [
            'class' => 'unlock\modules\core\gii\generators\module\Generator',
            'templates' => [
                'unlock' => '@unlock/modules/core/gii/generators/module/default',
            ]
        ],
    ],
];

Favourite Extensions
-----------------------------------------------------------------------------------------------------------------------
1. 
Name: Redactor
Path: use yii\redactor\widgets\Redactor;
URL: https://github.com/yiidoc/yii2-redactor

2.
Name: DatePicker
Path: use yii\jui\DatePicker;
URL: https://github.com/yiisoft/yii2-jui

3.
Name: FileInput
Path: use kartik\file\FileInput;
URL: https://github.com/kartik-v/yii2-widget-fileinput

4.
Name: Select2
Path: use kartik\widgets\DateTimePicker;
URL: https://github.com/kartik-v/yii2-widget-select2
1. Usage: Basic 
2. Tagging Support 
3. Usage: Ajax Loading

5. 
Name: Dependent Drop Downs
Path: use kartik\depdrop\DepDrop;
Url: https://github.com/kartik-v/yii2-widget-depdrop

6.
Name: Dynamic Field
Path: use unclead\multipleinput\MultipleInput;
Url: https://github.com/unclead/yii2-multiple-input
http://formvalidation.io/examples/adding-dynamic-field/

7.
Name: DateTimePicker
Path: use kartik\widgets\DateTimePicker;
URL: https://github.com/kartik-v/yii2-widget-datetimepicker

8.
Name: Captcha
Path: use yii\captcha\Captcha;

9.
Name: TagsInput
Path: use wbraganca\tagsinput\TagsinputWidget;
URL: https://github.com/wbraganca/yii2-tagsinput

10.
Name: SwitchInput
Path: use kartik\widgets\SwitchInput;
URL: https://github.com/kartik-v/yii2-widget-switchinput

11. 
name: Dependent Dropdown
path: use kartik\widgets\DepDrop;
URL: https://github.com/kartik-v/yii2-widget-depdrop

// Normal parent select
echo $form->field($model, 'cat')->dropDownList($catList, ['id'=>'cat-id']);

// Dependent Dropdown
echo $form->field($model, 'subcat')->widget(DepDrop::classname(), [
    'options' => ['id' => 'subcat-id'],
    'pluginOptions' => [
        'depends' => ['cat-id'],
        'placeholder' => 'Select...',
        'url' => Url::to(['/site/subcat'])
    ]
]);


Group Tab Edit Page
-----------------------------------------------------------------------------------------------------------------------
1.
<div class="main-container main-container-group-tab" role="main"></div>

2.
<div class="admin-form-group-tab">
    <ul class="tabs group-tabs">
        <li class="active"><a href="#" class="active">List</a></li>
        <li><a href="#">Update</a></li>
        <li><a href="#">Uninstall</a></li>
    </ul>
</div>


Grid Filter Fields
-----------------------------------------------------------------------------------------------------------------------

1. Toggle Update
[
    'class' => 'unlock\modules\core\actions\ToggleColumn',
    'attribute' => 'status',
    'filter' => Html::activeDropDownList($searchModel, 'status', CommonHelper::statusDropDownList(), ['prompt' => CommonHelper::GRID_FILTER_PROMPT]),
],

2.
use yii\jui\DatePicker;

[
    'attribute' => 'created_at',
    'format' => 'datetime',
    'filter' => DatePicker::widget([
            'name'  => 'SampleSearch[created_at]',
            'dateFormat' => 'yyyy-MM-dd'
    ]),
],
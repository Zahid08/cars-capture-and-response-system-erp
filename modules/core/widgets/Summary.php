<?php
namespace unlock\modules\core\widgets;

use yii\helpers\Html;
use yii\base\Widget;


class Summary extends Widget
{
    public $dataProvider;

	public function run()
	{
		return $this->getSummary();
	}

    public function getSummary()
    {
        $count = $this->dataProvider->getCount();
        if ($count <= 0) {
            return '<p>No results found.</p>';
        }
        if (($pagination = $this->dataProvider->getPagination()) !== false) {
            $totalCount = $this->dataProvider->getTotalCount();
            $begin = $pagination->getPage() * $pagination->pageSize + 1;
            $end = $begin + $count - 1;
            if ($begin > $end) {
                $begin = $end;
            }
            $page = $pagination->getPage() + 1;
            $pageCount = $pagination->pageCount;

            $html = '<div class="grid-summary">';
            $html .= "<p>Showing <span>$begin-$end</span> of <span>$totalCount</span> Items</p>";
            $html .= '</div>';

            return $html;
        }
        else {
            $totalCount = $this->dataProvider->getTotalCount();
            $begin = 1;
            $end = $begin + $count - 1;
            if ($begin > $end) {
                $begin = $end;
            }

            $html = '<div class="grid-summary">';
            $html .= "<p>Showing <span>$begin-$end</span> of <span>$totalCount</span> Items</p>";
            $html .= '</div>';

            return $html;
        }
    }

}

<?php

namespace unlock\modules\core\widgets;

class ListView extends \yii\widgets\ListView
{
    public $options = ['class' => 'list-view'];
}
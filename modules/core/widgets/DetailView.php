<?php

namespace unlock\modules\core\widgets;

class DetailView extends \yii\widgets\DetailView
{
    public $options = ['class' => 'table table-striped table-bordered detail-view'];
}
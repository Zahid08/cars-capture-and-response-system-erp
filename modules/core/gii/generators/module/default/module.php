<?php
/**
 * This is the template for generating a module class file.
 */

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\module\Generator */

$className = $generator->moduleClass;
$pos = strrpos($className, '\\');
$ns = ltrim(substr($className, 0, $pos), '\\');
$viewNamespace = '@' . str_replace('\\', '/', $ns) . '/views/';
$className = substr($className, $pos + 1);

echo "<?php\n";
?>

namespace <?= $ns ?>;

/**
 * Class <?= $className ?>

 */
class <?= $className ?> extends \yii\base\Module
{
    /**
     * @var string the namespace that controller classes are in
     */
    public $controllerNamespace = '<?= $generator->getControllerNamespace() ?>';

    /**
    * @var string the namespace that view file are in
    */
    public $viewNamespace = '<?= $viewNamespace ?>';
}

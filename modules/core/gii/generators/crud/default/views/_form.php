<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

/* @var $model \yii\db\ActiveRecord */
$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}
$pageTitle = StringHelper::basename($generator->modelClass);

echo "<?php\n";
?>

use yii\helpers\Url;
use yii\helpers\Html;
use yii\jui\DatePicker;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtons;
use unlock\modules\core\buttons\BackButton;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
/* @var $form yii\bootstrap\ActiveForm */

$this->title= ($model->isNewRecord) ? Yii::t('app', 'Create <?= $pageTitle ?>') : Yii::t('app', 'Update <?= $pageTitle ?>');
?>

<div class="main-container main-container-form" role="main">
    <div class="page-header">
        <div class="page-header-title">
            <h1 class="page-title"><?= "<?= " ?>Yii::t('app', '<?= Inflector::camel2words(StringHelper::basename($generator->modelClass)) ?>') ?></h1>
        </div>
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= "<?= " ?>Html::a(Yii::t('app', 'Home'), Yii::$app->homeUrl) ?></li>
                <li><?= "<?= " ?>Html::a(Yii::t('app', '<?= Inflector::camel2words(StringHelper::basename($generator->modelClass)) ?>'), Url::to('index')) ?></li>
                <li class="active"><span><?= "<?= " ?>Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-form-container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fa fa-pencil"></i> <?= "<?= " ?>Html::encode($this->title) ?></h3>
                </div>
                <div class="panel-heading-button">
                    <?= "<?= " ?>BackButton::widget() ?>
                </div>
            </div>
            <div class="panel-body">
                <?= "<?php " ?>$form = ActiveForm::begin([
                    'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
                    /*'options' => ['enctype' => 'multipart/form-data'],*/
                    'validateOnBlur' =>true,
                    'enableAjaxValidation'=>true,
                    'errorCssClass'=>'has-error',
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
                        'options' => ['class' => 'form-group'],
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-2',
                            'offset' => '',
                            'wrapper' => '',
                            'hint' => '',
                        ],
                    ],
                ]); ?>

                <?php
                $count = 0;
                foreach ($generator->getColumnNames() as $attribute) {
                    if (in_array($attribute, $safeAttributes) && $attribute != 'status') {
                        if($count == 0){
                            echo "<?= " . $generator->generateActiveField($attribute) . " ?>\n\n";
                        }
                        else{
                            echo "\t\t\t\t<?= " . $generator->generateActiveField($attribute) . " ?>\n\n";
                        }
                        $count++;

                    }
                } ?>
                <?= "<?= " ?>$form->field($model, 'status')->dropDownList(CommonHelper::statusDropDownList()) ?>

                <div class="admin-form-button">
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <?= "<?= " ?>FormButtons::widget() ?>
                        </div>
                    </div>
                </div>

                <?= "<?php " ?>ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

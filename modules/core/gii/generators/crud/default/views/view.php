<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();

echo "<?php\n";
?>

use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\widgets\DetailView;
use unlock\modules\core\buttons\BackButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */

$this->title = <?= $generator->generateString(Inflector::camel2words('View' . StringHelper::basename($generator->modelClass))) ?>;
?>
<div class="main-container main-container-view" role="main">
    <div class="page-header">
        <div class="page-header-title">
            <h1 class="page-title"><?= "<?= " ?><?= $generator->generateString(Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?> ?></h1>
        </div>
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= "<?= " ?>Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li><?= "<?= " ?>Html::a(Yii::t('app', '<?= Inflector::camel2words(StringHelper::basename($generator->modelClass)) ?>'), Url::to('index')) ?></li>
                <li class="active"><span><?= "<?= " ?>Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-view-container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fa fa-eye"></i> <?= "<?= " ?>Yii::t('app', 'View') ?> - <?= "<?= " ?>Html::encode($model->id) ?></h3>
                </div>
                <div class="panel-heading-button">
                    <?= "<?= " ?>BackButton::widget() ?>
                </div>
            </div>

            <div class="panel-body">
                <?= "<?= " ?>DetailView::widget([
                'model' => $model,
                'attributes' => [
                <?php
                $count = 0;
                if (($tableSchema = $generator->getTableSchema()) === false) {
                    foreach ($generator->getColumnNames() as $name) {
                        if($count == 0){
                            echo "\t'" . $name . "',\n";
                        }
                        else{
                            echo "\t\t\t\t\t'" . $name . "',\n";
                        }
                        $count++;
                    }
                } else {
                    foreach ($generator->getTableSchema()->columns as $column) {
                        $format = $generator->generateColumnFormat($column);
                        if($count == 0){
                            echo "\t'" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
                        }
                        else {
                            echo "\t\t\t\t\t'" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
                        }

                        $count++;
                    }
                }
                ?>
                ],
                ]) ?>
            </div>

        </div>
    </div>
</div>

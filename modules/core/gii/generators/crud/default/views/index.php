<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();

echo "<?php\n";
?>

use yii\helpers\Url;
use yii\helpers\Html;
use yii\jui\DatePicker;
use <?= $generator->indexWidgetType === 'grid' ? "unlock\\modules\\core\\grid\\GridView" : "unlock\\modules\\core\\widgets\\ListView" ?>;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\buttons\NewButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
<?= !empty($generator->searchModelClass) ? "/* @var \$searchModel " . ltrim($generator->searchModelClass, '\\') . " */\n" : '' ?>
/* @var $dataProvider unlock\modules\core\data\ActiveDataProvider */

$this->title = <?= $generator->generateString(Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>;
?>
<div class="main-container" role="main">
    <div class="page-header">
        <div class="page-header-title">
            <h1 class="page-title"><?= "<?= " ?>Html::encode($this->title) ?></h1>
        </div>
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= "<?= " ?>Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li class="active"><span><?= "<?= " ?>Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-grid-filter">
        <?= "<?php // echo " ?>$this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <div class="admin-grid-content">
        <div class="admin-grid-toolbar">
            <div class="admin-grid-toolbar-left">
                <?= "<?= " ?>Summary::widget(['dataProvider' => $dataProvider]) ?>
            </div>
            <div class="admin-grid-toolbar-right">
                <?= "<?= " ?>NewButton::widget(['url' => Url::toRoute(['create'])]) ?>
            </div>
        </div>

        <div class="admin-grid-view">
            <?php if ($generator->indexWidgetType === 'grid'): ?>
                <?= "<?php \n" ?>
                echo GridView::widget([
                'dataProvider' => $dataProvider,
                <?= !empty($generator->searchModelClass) ? "'filterModel' => \$searchModel,\n" : ""; ?>
                'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
                'pager' => ['class' => LinkPager::className()],
                'columns' => [
                ['class' => 'unlock\modules\core\grid\SerialColumn'],
                <?php
                $count = 0;
                if (($tableSchema = $generator->getTableSchema()) === false) {
                    foreach ($generator->getColumnNames() as $name) {
                        if (++$count < 6) {
                            if($count == 1){
                                echo "\t'" . $name . "',\n";
                            }
                            else{
                                echo "\t\t\t'" . $name . "',\n";
                            }
                        }
                        else {
                            echo "\t\t\t// '" . $name . "',\n";
                        }
                    }
                } else {
                    foreach ($tableSchema->columns as $column) {
                        $format = $generator->generateColumnFormat($column);
                        if (++$count < 6) {
                            if($count == 1){
                                echo "\t'" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
                            }
                            else{
                                echo "\t\t\t'" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
                            }
                        }
                        else {
                            echo "\t\t\t// '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
                        }
                    }
                }
                ?>
                [
                'attribute' => 'created_at',
                'format' => 'datetime',
                'filter' => DatePicker::widget([
                'name'  => 'SampleSearch[created_at]',
                'dateFormat' => 'yyyy-MM-dd'
                ]),
                ],
                [
                'attribute' => 'status',
                'format' => 'status',
                'filter' => Html::activeDropDownList($searchModel, 'status', CommonHelper::statusDropDownList(), ['prompt' => CommonHelper::GRID_FILTER_PROMPT]),
                ],
                ['class' => 'unlock\modules\core\grid\ActionColumn'],
                ],
                ]);   ?>
            <?php else: ?>
                <?= "<?= " ?>ListView::widget([
                'dataProvider' => $dataProvider,
                'itemOptions' => ['class' => 'item'],
                'itemView' => function ($model, $key, $index, $widget) {
                return Html::a(Html::encode($model-><?= $nameAttribute ?>), ['view', <?= $urlParams ?>]);
                },
                ]) ?>
            <?php endif; ?>
        </div>
    </div>

</div>

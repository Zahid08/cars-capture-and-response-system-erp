<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();

echo "<?php\n";
?>

use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\grid\GridView;
use unlock\modules\core\helpers\CommonHelper;
?>
<div style="opacity: 1;" class="row">
    <div class="col-lg-12">
        <div class="row page-header">
            <div class="page-header-title">
                <h1 class="page-title"><?= "<?= " ?><?= $generator->generateString(Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?> ?></h1>
            </div>
            <?= "<?php" ?> if(isset($asOnDate)){ ?>
            <div class="as-on-date-time">
                <p><?= "<?= " ?>$asOnDate ?></p>
            </div>
            <?= "<?php" ?> } ?>
        </div>
        <?= "<?= " ?>$dataProvider->sort = false; ?>
        <?= "<?= " ?>GridView::widget([
            'dataProvider' => $dataProvider,
            /*'summary' => "<strong>Total : {totalCount}</strong>",*/
            'summary' => false,
            'layout'=> "{summary}\n{items}",
            'emptyCell' => "",
            'tableOptions' => ['class' => 'table table-striped pdf-table'],
            'rowOptions'=>function ($model, $key, $index, $grid){
                $class = $index % 2 ? 'odd' : 'even';
                return array('key'=>$key, 'index'=>$index, 'class'=>$class);
            },
            'columns' => [
            <?php
            $count = 0;
        if (($tableSchema = $generator->getTableSchema()) === false) {
            foreach ($generator->getColumnNames() as $name) {
                if (++$count < 6) {
                    echo "            '" . $name . "',\n";
                } else {
                    echo "            // '" . $name . "',\n";
                }
            }
        } else {
            foreach ($tableSchema->columns as $column) {
                $format = $generator->generateColumnFormat($column);
                if (++$count < 6) {
                    echo "            '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
                } else {
                    echo "            // '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
                }
            }
        }
        ?>
            ],
        ]);
        ?>
    </div>
</div>

<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();

echo "<?php\n";
?>

use unlock\modules\core\helpers\ExportCsv;
use unlock\modules\core\helpers\CommonHelper;

/*$columns = ['id', 'status'];*/

if($dataProvider->getCount()){
    $models = array_values($dataProvider->getModels());
    $keys = $dataProvider->getKeys();

    // START: CSV Header Column
    $header = ExportCsv::csvHeaderColumn($columns, $models);
    // END: CSV Header Column

    //START: CSV Data Column
    $data = [];
    $count = 0;
    foreach ($models as $index => $model) {
        foreach ($header as $key => $value) {
            if($key == 'SerialNo'){
                $data[$count][$key] = ($count + 1);
            }
            else if($key == 'status'){
                // Formatter
                $data[$count][$key] = Yii::$app->formatter->format($model->$key, ['status']);
            }
            else{
                $data[$count][$key] = $model->$key;
            }
        }
        $count++;
    }
    // END: CSV Data Column

    // Export To CSV
    ExportCsv::exportGridCsv($header, $data, $fileName);
}
else{
    Yii::$app->session->setFlash('error', 'Sorry, No Record Found');
}

<?php

namespace unlock\modules\core\buttons;

use Yii;
use yii\base\InvalidParamException;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * How To Use:
 *
 * HTML:
 *
 * <?= VerticalDivider::widget(['url' => Url::toRoute(['vertical-divider-1'])]); ?>
 *
 * <?= VerticalDivider::widget(['url' => Url::toRoute(['vertical-divider-2'])]); ?>
 *
 * <?= VerticalDivider::widget(['url' => Url::toRoute(['vertical-divider-3'])]); ?>
 *
 *
 */

class VerticalDivider extends Widget
{
    public $url;
    public $route;
    public $visible;

    public function init()
    {
        if (!isset($this->url)) {
            $this->visible = false;
        }
        else{
            $this->visible = Yii::$app->user->checkUrlPermission($this->url, $this->route);
        }
    }

    public function run()
    {
        if(!$this->visible){ return false; }
        return $this->renderButtons();
    }

    protected function renderButtons()
    {
        return Html::tag('span', '|&nbsp;&nbsp;', ['class' => 'v-divider']);
    }
}
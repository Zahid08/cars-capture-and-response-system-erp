<?php

namespace unlock\modules\core\buttons;

use Yii;
use yii\base\InvalidParamException;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * How To Use:
 *
 * HTML:
 *
 *  <?= FormButtons::widget(['item' => ['save']]) ?>
 *
 *
 */

class FormButtonsCustomAction extends Widget
{
    public $item = ['cancel','save','done']; //save, apply, save2new, cancel

    public function run()
    {
        return $this->renderButton();
    }

    protected function renderButton()
    {
        $buttonHtml = '';
        // Cancel
        if(in_array('cancel', $this->item)) {
            $buttonHtml .= Html::a(
                '<i class="fa fa-times"></i> ' . Yii::t('app', 'Cancel'),
                Yii::$app->request->referrer,
                ['class' => 'btn btn-danger cancel-btn']
            );
        }


        // Save
        if(in_array('save', $this->item)){
            $buttonHtml .=  Html::submitButton(
                '<i class="fa fa-check"></i> ' . Yii::t('app', 'Save'),
                [
                    'class' => 'btn save-btn-id',
                    'name' => 'action',
                    'value' => 'save',
                ]
            );
        }

        // Save
        if(in_array('done', $this->item)){
            $buttonHtml .=  Html::submitButton(
                '<i class="fa fa-check"></i> ' . Yii::t('app', 'Done'),
                [
                    'class' => 'btn btn-check done-btn-id',
                    'name' => 'action',
                    'value' => 'done',
                ]
            );
        }

        // Apply
        if(in_array('apply', $this->item)){
            $buttonHtml .= Html::submitButton(
                '<i class="fa fa-check"></i> ' . Yii::t('app', 'Save and Continue Edit'),
                [
                    'class' => 'btn btn-default',
                    'name' => 'action',
                    'value' => 'apply',
                ]
            );
        }

        // Save & New
        if(in_array('save2new', $this->item)){
            $buttonHtml .=   Html::submitButton(
                '<i class="fa fa-check"></i> ' . Yii::t('app', 'Save & New'),
                [
                    'class' => 'btn btn-default',
                    'name' => 'action',
                    'value' => 'save2new',
                ]
            );
        }



        return $buttonHtml;
    }
}
 
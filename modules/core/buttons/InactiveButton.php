<?php

namespace unlock\modules\core\buttons;

use Yii;
use yii\base\InvalidParamException;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * How To Use:
 *
 * HTML:
 *
 * <?= InactiveButton::widget(['url' => Url::toRoute(['mass-inactive'])]) ?>
 *
 * PHP:
 *
 * public function actions()
  {
    return [
        'mass-inactive' => [
            'class' => MassInactiveAction::class,
            'modelName' => Ship::className(),
        ],
    ];
  }
 *
 */

class InactiveButton extends Widget
{
    public $title = 'Inactive';
    public $url;
    public $route;
    public $gridSelector = '.grid-view';
    public $htmlOptions = [];
    public $visible;

    public function init()
    {
        if (!isset($this->url)) {
            throw new InvalidParamException(Yii::t('app', 'Attribute \'url\' is not set'));
        }
        if (!isset($this->htmlOptions['id'])) {
            $this->htmlOptions['id'] = 'inactiveItems';
        }
        if (isset($this->htmlOptions['class'])) {
            $this->htmlOptions['class'] = $this->htmlOptions['class'] . ' btn-bulk-action';
        }
        else{
            $this->htmlOptions['class'] = 'btn btn-default btn-bulk-action';
        }

        $this->visible = Yii::$app->user->checkUrlPermission($this->url, $this->route);

        $this->htmlOptions['data-title'] = $this->title;
        $this->htmlOptions['data-url'] = $this->url;
        $this->htmlOptions['data-action'] = 'inactive';
        $this->htmlOptions['data-grid-selector'] = $this->gridSelector;

        Html::addCssClass($this->htmlOptions, 'btn');
    }

    public function run()
    {
        if(!$this->visible){ return false; }
        return $this->renderButtons();
    }

    protected function renderButtons()
    {
        $buttons = Html::button(
            '<i class="fa fa-remove"></i> ' . Yii::t('app', $this->title),
            $this->htmlOptions
        );

        return Html::tag('div', $buttons, [
            'id' => 'toolbar-inactive',
            'class' => 'btn-wrapper',
        ]);
    }
}
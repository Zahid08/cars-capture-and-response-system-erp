<?php

namespace unlock\modules\core\buttons;

use Yii;
use yii\base\InvalidParamException;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * How To Use:
 *
 * HTML:
 *
 *  <?= NewButton::widget(['url' => Url::toRoute(['create'])]) ?>
 *
 *
 */

class NewButton extends Widget
{
    public $title;
    public $url;
    public $route;
    public $htmlOptions = [];
    public $visible;

    public function init()
    {
        if (!isset($this->url)) {
            throw new InvalidParamException('Attribute \'url\' is not set');
        }
        if (true === empty($this->title)) {
            $this->title = Yii::t('app', 'Create');
        }
        if (!isset($this->htmlOptions['class'])) {
            $this->htmlOptions['class'] = 'btn btn-success';
        }
        if (!isset($this->htmlOptions['data-action'])) {
            $this->htmlOptions['data-action'] = 'add-new';
        }
        if (!isset($this->htmlOptions['id'])) {
            $this->htmlOptions['id'] = 'addItems';
        }

        $this->visible = Yii::$app->user->checkUrlPermission($this->url, $this->route);
        Html::addCssClass($this->htmlOptions, 'btn');
    }

    public function run()
    {
        if(!$this->visible){ return false; }

        return $this->renderButton();
    }

    protected function renderButton()
    {
        $item = Html::a(
            '<i class="fa fa-plus"></i> ' .
            Yii::t('app', $this->title),
            $this->url,
            $this->htmlOptions
        );

        return Html::tag('div', $item, [
            'id' => 'toolbar-new',
            'class' => 'btn-wrapper',
        ]);

    }
}
 
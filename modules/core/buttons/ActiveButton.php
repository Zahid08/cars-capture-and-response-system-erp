<?php

namespace unlock\modules\core\buttons;

use Yii;
use yii\base\InvalidParamException;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * How To Use:
 *
 * HTML:
 *
 * <?= ActiveButton::widget(['url' => Url::toRoute(['mass-active'])]) ?>
 *
 * PHP:
 *
 * public function actions()
  {
    return [
        'mass-active' => [
            'class' => MassActiveAction::class,
            'modelName' => Ship::className(),
        ],
    ];
  }
 *
 */

class ActiveButton extends Widget
{
    public $title = 'Active';
    public $url;
    public $route;
    public $gridSelector = '.grid-view';
    public $htmlOptions = [];
    public $visible;

    public function init()
    {
        if (!isset($this->url)) {
            throw new InvalidParamException(Yii::t('app', 'Attribute \'url\' is not set'));
        }
        if (!isset($this->htmlOptions['id'])) {
            $this->htmlOptions['id'] = 'activeItems';
        }
        if (isset($this->htmlOptions['class'])) {
            $this->htmlOptions['class'] = $this->htmlOptions['class'] . ' btn-bulk-action';
        }
        else{
            $this->htmlOptions['class'] = 'btn btn-default btn-bulk-action';
        }

        $this->visible = Yii::$app->user->checkUrlPermission($this->url, $this->route);

        $this->htmlOptions['data-title'] = $this->title;
        $this->htmlOptions['data-url'] = $this->url;
        $this->htmlOptions['data-action'] = 'active';
        $this->htmlOptions['data-grid-selector'] = $this->gridSelector;

        Html::addCssClass($this->htmlOptions, 'btn');
    }

    public function run()
    {
        if(!$this->visible){ return false; }
        return $this->renderButtons();
    }

    protected function renderButtons()
    {
        $buttons = Html::button(
            '<i class="fa fa-check"></i> ' . Yii::t('app', $this->title),
            $this->htmlOptions
        );

        return Html::tag('div', $buttons, [
            'id' => 'toolbar-active',
            'class' => 'btn-wrapper',
        ]);
    }
}
 
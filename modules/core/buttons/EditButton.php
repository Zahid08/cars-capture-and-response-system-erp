<?php

namespace unlock\modules\core\buttons;

use Yii;
use yii\base\InvalidParamException;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * How To Use:
 *
 * HTML:
 *
 *  <?= EditButton::widget(['url' => Url::toRoute(['update', 'id' => ''])]) ?>
 *
 *
 */

class EditButton extends Widget
{
    public $title;
    public $url;
    public $route;
    public $gridSelector;
    public $htmlOptions = [];
    public $visible;

    public function init()
    {
        if (!isset($this->url)) {
            throw new InvalidParamException('Attribute \'url\' is not set');
        }
        if (true === empty($this->gridSelector)) {
            $this->gridSelector = '.grid-view';
        }
        if (true === empty($this->title)) {
            $this->title = Yii::t('app', 'Edit');
        }
        if (!isset($this->htmlOptions['id'])) {
            $this->htmlOptions['id'] = 'editItems';
        }
        if (isset($this->htmlOptions['class'])) {
            $this->htmlOptions['class'] = $this->htmlOptions['class'] . ' btn-bulk-action';
        }
        else{
            $this->htmlOptions['class'] = 'btn btn-default btn-bulk-action';
        }

        $this->visible = Yii::$app->user->checkUrlPermission($this->url, $this->route);

        $this->htmlOptions['data-title'] = $this->title;
        $this->htmlOptions['data-url'] = $this->url;
        $this->htmlOptions['data-action'] = 'edit';
        $this->htmlOptions['data-grid-selector'] = $this->gridSelector;

        Html::addCssClass($this->htmlOptions, 'btn');
    }

    public function run()
    {
        if(!$this->visible){ return false; }
        return $this->renderButton();
    }

    protected function renderButton()
    {

        $buttons = Html::button(
            '<i class="fa fa-edit"></i> ' .
            Yii::t('app', $this->title),
            $this->htmlOptions
        );

        return Html::tag('div', $buttons, [
            'id' => 'toolbar-edit',
            'class' => 'btn-wrapper',
        ]);

    }
}
 
<?php

namespace unlock\modules\core\buttons;

use Yii;
use yii\base\InvalidParamException;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * How To Use:
 *
 * HTML:
 *
 *  <?= BackButton::widget() ?>
 *
 *
 */

class BackButton extends Widget
{
    public $title;

    public function run()
    {
        return $this->renderButton();
    }

    protected function renderButton()
    {
        $item = Html::a(
                '<i class="fa fa-arrow-left"></i> ' . Yii::t('app', 'Back'),
                Yii::$app->request->referrer,
                ['class' => 'btn btn-default back-button']
            );

        return $item;

    }
}
 
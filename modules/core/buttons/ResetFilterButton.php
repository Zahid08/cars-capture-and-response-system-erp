<?php

namespace unlock\modules\core\buttons;

use Yii;
use yii\base\InvalidParamException;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * How To Use:
 *
 * HTML:
 *
 *  <?= ResetFilterButton::widget(['url' => Url::toRoute(['index'])]) ?>
 *
 *
 */

class ResetFilterButton extends Widget
{
    public $title = 'Reset Filter';
    public $url;
    public $route;
    public $htmlOptions = [];
    public $visible;

    public function init()
    {
        if (!isset($this->url)) {
            throw new InvalidParamException('Attribute \'url\' is not set');
        }
        if (!isset($this->htmlOptions['id'])) {
            $this->htmlOptions['id'] = 'resetFilterItems';
        }
        if (isset($this->htmlOptions['class'])) {
            $this->htmlOptions['class'] = $this->htmlOptions['class'];
        }
        else{
            $this->htmlOptions['class'] = 'btn btn-default';
        }

        $this->visible = Yii::$app->user->checkUrlPermission($this->url, $this->route);

        $this->htmlOptions['data-title'] = $this->title;
        $this->htmlOptions['data-url'] = $this->url;
        $this->htmlOptions['data-action'] = 'reset-filter';

        Html::addCssClass($this->htmlOptions, 'btn');
    }

    public function run()
    {
        if(!$this->visible){ return false; }
        return $this->renderButtons();
    }

    protected function renderButtons()
    {
        $buttons = Html::button(
            '<i class="fa fa-refresh"></i> ' .
            Yii::t('app', $this->title),
            $this->htmlOptions
        );

        return $buttons;
    }
}
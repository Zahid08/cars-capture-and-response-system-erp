<?php

namespace unlock\modules\core\buttons;

use Yii;
use yii\base\InvalidParamException;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * How To Use:
 *
 * HTML:
 *
 * <?= ExportPdfButton::widget(['url' => Url::toRoute(['export-pdf'])]) ?>
 *
 * PHP:
 *
 * public function actions()
  {
        return [
            'export-pdf' => [
                'class' => ExportPdfAction::class,
                'modelName' => GovtReceipt::className(),
                'fileName' => 'GovtReceipt_' . date('d-m-Y_H.i.s') . '.pdf',
                'controllerAction' => 'index',
            ],
        ];
  }
 *
 */

class ExportPdfButton extends Widget
{
    public $title = 'Export To PDF';
    public $url;
    public $route;
    public $gridSelector = '.grid-view';
    public $htmlOptions = [];
    public $visible;

    public function init()
    {
        if (!isset($this->url)) {
            throw new InvalidParamException(Yii::t('app', 'Attribute \'url\' is not set'));
        }
        if (!isset($this->htmlOptions['id'])) {
            $this->htmlOptions['id'] = 'exportPdfItems';
        }
        if (isset($this->htmlOptions['class'])) {
            $this->htmlOptions['class'] = $this->htmlOptions['class'] . ' btn-export-action';
        }
        else{
            $this->htmlOptions['class'] = 'btn btn-default btn-export-action';
        }

        $this->visible = Yii::$app->user->checkUrlPermission($this->url, $this->route);

        $this->htmlOptions['data-title'] = $this->title;
        $this->htmlOptions['data-url'] = $this->url;
        $this->htmlOptions['data-action'] = 'export-pdf';
        $this->htmlOptions['data-grid-selector'] = $this->gridSelector;

        Html::addCssClass($this->htmlOptions, 'btn');
    }

    public function run()
    {
        if(!$this->visible){ return false; }
        return $this->renderButtons();
    }

    protected function renderButtons()
    {
        $buttons = Html::button(
            '<i class="fa fa-file-pdf-o"></i> ' . Yii::t('app', $this->title),
            $this->htmlOptions
        );

        return Html::tag('div', $buttons, [
            'id' => 'toolbar-pdf-export',
            'class' => 'btn-wrapper',
        ]);
    }
}
 
<?php

namespace unlock\modules\core\buttons;

use Yii;
use yii\base\InvalidParamException;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * How To Use:
 *
 * HTML:
 *
 *  <?= SearchFilterButton::widget() ?>
 *
 *
 */

class SearchFilterButton extends Widget
{
    public $title = 'Search';

    public $htmlOptions = [];

    public function init()
    {
        if (!isset($this->htmlOptions['id'])) {
            $this->htmlOptions['id'] = 'searchBtn';
        }
        if (isset($this->htmlOptions['class'])) {
            $this->htmlOptions['class'] = $this->htmlOptions['class'];
        }
        else{
            $this->htmlOptions['class'] = 'btn btn-primary';
        }

        Html::addCssClass($this->htmlOptions, 'btn');
    }

    public function run()
    {
        return $this->renderButtons();
    }

    protected function renderButtons()
    {
        $buttons = Html::submitButton(
            '<i class="fa fa-search"></i>' .
            Yii::t('app', $this->title),
            $this->htmlOptions
        );

        return $buttons;
    }
}
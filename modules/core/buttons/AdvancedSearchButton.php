<?php

namespace unlock\modules\core\buttons;

use Yii;
use yii\base\InvalidParamException;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * How To Use:
 *
 * HTML:
 *
 *  <?= AdvancedSearchButton::widget(['url' => Url::toRoute(['advanced-search'])]) ?>
 *
 *
 */

class AdvancedSearchButton extends Widget
{
    public $title = 'Advanced Search';
    public $url;
    public $route;
    public $htmlOptions = [];
    public $visible;

    public function init()
    {
        if (!isset($this->htmlOptions['id'])) {
            $this->htmlOptions['id'] = 'advancedSearchItems';
        }
        if (isset($this->htmlOptions['class'])) {
            $this->htmlOptions['class'] = $this->htmlOptions['class'];
        }
        else{
            $this->htmlOptions['class'] = 'btn btn-default';
        }

        if (!isset($this->url)) {
            $this->visible = false;
        }
        else{
            $this->visible = Yii::$app->user->checkUrlPermission($this->url, $this->route);
        }

        $this->htmlOptions['data-title'] = $this->title;
        $this->htmlOptions['data-action'] = 'advanced-search';

        Html::addCssClass($this->htmlOptions, 'btn');
    }

    public function run()
    {
        if(!$this->visible){ return false; }
        return $this->renderButtons();
    }

    protected function renderButtons()
    {
        $buttons = Html::button(
            '<i class="fa fa-filter"></i> ' .
            Yii::t('app', $this->title),
            $this->htmlOptions
        );

        return Html::tag('div', $buttons, [
            'id' => 'toolbar-advanced-search',
            'class' => 'btn-wrapper',
        ]);
    }
}
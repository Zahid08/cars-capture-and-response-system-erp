<?php
use yii\db\Migration;

/**
 * Class m170202_114525_auto_increment_number_tbl
 */
class m170202_114525_auto_increment_number_tbl extends Migration
{
    /**
     * This method contains the logic to be executed when applying this migration.
     */
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%auto_increment_number}}', [
            'id'                => $this->primaryKey()->unsigned()->notNull(),
            'increment_type'    => $this->string(50)->notNull()->comment('noc-pdf-no'),
            'increment_prefix'  => $this->string(50)->comment('405.008.019.00.00.151.2017/'),
            'increment_last_id' => $this->string(50)->notNull()->comment('17000001'),
        ], $tableOptions);

    }

    /**
     * This method contains the logic to be executed when removing this migration.
     */
    public function down()
    {
        $this->dropTable('{{%auto_increment_number}}');
    }
}

<?php
/**
 * Configuration:
'components' => [
'formatter' => [
'class' => 'app\components\Formatter',
],
],

 * Usage:
1.
Syntax: "attribute:format:label"
'amount:currency:Total Amount',

2.
[
'attribute' => 'is_active',
'format' => 'raw',
],

3.
[
'attribute' => 'createdon',
'format' => ['date', 'php:Y-m-d H:i:s'],
],

4.
echo Yii::$app->formatter->asDate('2014-01-01')
echo Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');

5.
echo Yii::$app->formatter->format('2014-01-01', 'date');
 */

namespace unlock\modules\core\i18n;

use unlock\modules\core\helpers\CommonHelper;
use Yii;
use yii\helpers\Html;
use unlock\modules\dynamicdropdown\models\DynamicDropDown;
use unlock\modules\usermanager\user\models\User;

class Formatter extends \yii\i18n\Formatter
{
    public $nullDisplay = '';

    public function asImagePreview ($value, $options = []) {
        if ($value === null) {
            return $this->nullDisplay;
        }

        $fileUrl = Yii::getAlias('@baseUrl') . '/media';
        if(isset($options['dir'])){
            $fileUrl = $fileUrl . '/' . $options['dir'];
            unset( $options['dir']);
        }

        $defaultOptions = ['width' => 50, 'height' => 50];

        $options = array_merge($defaultOptions, $options);

        $fileUrl = $fileUrl . '/' . $value;

        return Html::img($fileUrl, $options);
    }

    public function asCleanHtml ($value, $limit = null) {
        if ($value === null) {
            return $this->nullDisplay;
        }

        if(isset($limit)){
            $value = $this->asTruncate($value, $limit);
        }

        return strip_tags($value);
    }

    public function asTruncate($value, $limit = 100){
        $value = trim(strip_tags($value));

        if(strlen($value) <= $limit){
            return $value;
        }
        else{
            $lastCharacter = $value[$limit - 1];
            $lastCharacterPlusOne = $value[$limit];
            $value = trim(substr($value, 0, $limit));

            if(!($lastCharacter == " " || $lastCharacterPlusOne == " ")){
                $value = preg_replace('~\s+\S+$~', '', $value);
            }
        }

        return $value;
    }

    public function asStatus($value){
        $value = trim(strip_tags($value));
        $status = '';
        if($value == 1){
            $status = 'Active';
        }
        else{
            $status = 'Inactive';
        }
        return $status;
    }

    public function asUser($value){
        if ($value === null) {
            return $this->nullDisplay;
        }

        $value = trim(strip_tags($value));

        $user = User::find()
            ->where(['id' => $value])
            ->one();

        if($user == null){
            return $this->nullDisplay;
        }

        $userName = isset($user->username) ? $user->username : '';

        if(isset($user->status) && $user->status == CommonHelper::STATUS_DELETED){
            $userName = base64_decode($user->username);
        }

        return $userName;
    }

    public function asTruncateNumber($value, $decimals = null){
        if ($value === null) {
            return $this->nullDisplay;
        }

        $value = $this->normalizeNumericValue($value);

        if ($decimals === null) {
            $decimals = 2;
        }

        return number_format($value, $decimals, null, '');
    }

    public function asDynamicDropDown($value, $type = null)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }

        $parentId = null;
        if($type){
            $parentId = DynamicDropDown::getIdByKey($type);
        }

        $data = DynamicDropDown::find()
            ->where('`key` =:key', [':key' => $value])
            ->andFilterWhere(['parent_id' => $parentId])
            ->one();

        return isset($data->value) ? $data->value : '';

    }

    public function asDate2($value)
    {
        if ($value === null || $value == '0000-00-00' || $value == '0000-00-00 00:00:00') {
            return $this->nullDisplay;
        }

        $format = 'd M Y';
        return date($format, strtotime($value));
    }

    public function asMoney($value, $default = '0.00')
    {
        if ($value === null) {
            return $default;
        }

        $value = $value + 0;
        return $this->moneyFormatBdt($value);
    }

    function moneyFormatBdt($amount)
    {
        $sign = '';
        $sep = array();
        $dec = '00';

        if (substr($amount, 0, 1) == '-') {
            $sign = '-';
            $amount = substr($amount, 1);
        }

        $amount = explode('.', $amount);

        if (count($amount) > 1){
            $dec = $amount[1];
        }

        if (strlen($amount[0]) < 4) {
            return $sign . $amount[0] . '.' . $dec;
        }

        $th = substr($amount[0], -3);
        $hu = substr($amount[0], -9, -3);
        $rest = substr($amount[0], 0, -9);

        while(strlen($hu) > 0) {
            $sep[] = substr($hu, -2);
            $hu = substr($hu, 0, -2);
        }

        return $sign . $rest . implode(',', array_reverse($sep)) . ',' . $th . '.' . $dec;
    }

}

<?php
namespace unlock\modules\core\actions;

use Yii;
use yii\base\Action;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\web\BadRequestHttpException;

/**
 * How To Use:
 *
 * HTML:
 *
 *
 *
 * PHP:
 *
 * public function actions()
  {
    return [
        'toggle-update' => [
            'class' => ToggleUpdateAction::class,
            'modelName' => Ship::className(),
        ],
    ];
  }
 *
 */

class ToggleUpdateAction extends Action
{
    public $modelName = '';

    public $onValue = 1;

    public $offValue = 0;

    public $successTemplateOn = '<span class="toggle-column-active"><i class="fa fa-check"></i> Active</span>';

    public $successTemplateOff = '<span class="toggle-column-inactive"><i class="fa fa-remove"></i> Inactive</span>';

    public $primaryKey = 'id';

    public $redirect = ['index'];


    public function init()
    {
        parent::init();

        if (!isset($this->modelName)) {
            throw new InvalidConfigException("Model name should be set in controller actions");
        }
        if (!class_exists($this->modelName)) {
            throw new InvalidConfigException("Model class does not exists");
        }
    }

    public function run($id, $attribute)
    {
        $returnHtml = '';
        $model = $this->findModel($this->modelName, $id);

        if (!$model->hasAttribute($attribute)) {
            throw new InvalidConfigException("Model '{$this->modelName}' has no '{$attribute}' field'");
        }

        if ($model->$attribute == $this->onValue) {
            $model->$attribute = $this->offValue;
            $returnHtml = $this->successTemplateOff;
        }
        else {
            $model->$attribute = $this->onValue;
            $returnHtml = $this->successTemplateOn;
        }

        if (!$model->save(true, [$attribute])) {
            $returnHtml = false;
        }

        if (Yii::$app->request->getIsAjax()) {
            echo $returnHtml;
            Yii::$app->end();
        }

        if (!empty($this->redirect)) {
            return $this->controller->redirect($this->redirect);
        }

        return $this->controller->redirect(Yii::$app->request->getReferrer());
    }

    public function findModel($modelName, $id)
    {
        if (($model = $modelName::findOne([$this->primaryKey => $id])) !== null) {
            return $model;
        } else {
            throw new BadRequestHttpException('Entity not found by primary key ' . $id);
        }
    }
}

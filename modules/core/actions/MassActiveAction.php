<?php
namespace unlock\modules\core\actions;

use Yii;
use yii\base\Action;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\web\BadRequestHttpException;

/**
 * Class ToggleAction
 *
 */
class MassActiveAction extends Action
{
    public $modelName = '';

    public $attribute = 'status';

    public $activeFieldValue = 1;

    public $primaryKey = 'id';

    public $redirect = ['index'];


    public function init()
    {
        parent::init();

        if (!isset($this->modelName)) {
            throw new InvalidConfigException(Yii::t('app', 'Model name should be set in controller actions'));
        }
        if (!class_exists($this->modelName)) {
            throw new InvalidConfigException(Yii::t('app', 'Model class does not exists'));
        }
    }

    public function run()
    {
        try {
            $modelName = $this->modelName;
            $items = explode(',', Yii::$app->request->post('items'));

            if(!is_array($items)) {
                throw new Exception(Yii::t('app', 'Please select item(s)'));
            }

            $items = $modelName::find()->where(['in', $this->primaryKey, $items])->all();
            foreach ($items as $item) {
                $item->setAttribute($this->attribute, $this->activeFieldValue);
                if (!$item->save()) {
                    throw new Exception(Yii::t('app', Html::errorSummary($item)));
                }
            }

            Yii::$app->session->setFlash('success', Yii::t('app', count($items).' record(s) have been successfully updated.'));

        }
        catch (Exception $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        return $this->controller->redirect(Yii::$app->request->getReferrer());
    }
}

<?php
namespace unlock\modules\core\actions;

use Yii;
use yii\base\Action;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\data\ArrayDataProvider;
use yii\data\SqlDataProvider;
use yii\db\ActiveRecord;
use yii\web\BadRequestHttpException;
use unlock\modules\core\helpers\Collection;
use unlock\modules\core\helpers\ExportCsv;
use yii\data\ActiveDataProvider;

/**
 * Class ExportCsvAllAction
 *
 */
class ExportCsvAction extends Action
{
    public $modelName = '';

    public $view;

    public $primaryKey = 'id';

    public $redirect = ['index'];

    public $fileName = 'export.csv';

    public $sort = true; // true|false

    public $pagination = false; // true|false

    public $dataProviderType; // ActiveDataProvider | SqlDataProvider | ArrayDataProvider

    public $collection;

    public $controllerAction;

    public function init()
    {
        parent::init();

        if (!isset($this->controllerAction)) {
            $this->controllerAction = 'index';
        }
        if (!isset($this->modelName)) {
            throw new InvalidConfigException(Yii::t('app', 'Model name should be set in controller actions'));
        }
        if (!class_exists($this->modelName)) {
            throw new InvalidConfigException(Yii::t('app', 'Model class does not exists'));
        }
        if (!isset($this->view)) {
            $this->view = Yii::$app->getModule(Yii::$app->controller->module->id)->viewNamespace . Yii::$app->controller->id . '/export/grid-export-csv.php';
        }
        else{
            $this->view = Yii::$app->getModule(Yii::$app->controller->module->id)->viewNamespace . Yii::$app->controller->id . '/' . $this->view . '.php';
        }

        // Set Collection
        $collection = Collection::getData($this->controllerAction);
        if (!isset($collection)) {
            throw new InvalidConfigException(Yii::t('app', 'Need to set collection'));
        }
        else{
            $this->collection = $collection;
        }
    }

    public function run()
    {
        if(empty(Yii::$app->request->post('items')) || Yii::$app->request->post('items') == 'export-csv'){
            $this->exportCsvAll();
        }
        else {
            $this->exportCsvSelectedItem();
        }

        return $this->controller->redirect(Yii::$app->request->getReferrer());
    }

    public function exportCsvAll(){
        try {

            if(empty($this->collection['dataProvider'])){
                throw new Exception(Yii::t('app', 'No record found.'));
            }

            $sort = ($this->sort == false) ? false : $this->collection['dataProvider']->sort;

            $pagination = ($this->pagination == false) ? ['pageSize' => false] : $this->collection['dataProvider']->pagination;

            if($this->dataProviderType == 'SqlDataProvider'){
                $provider = new SqlDataProvider([
                    'sql' =>  $this->collection['dataProvider']->sql,
                    'sort' => $sort,
                    'pagination' => $pagination,
                ]);
            }
            else if($this->dataProviderType == 'ArrayDataProvider'){
                $provider = new ArrayDataProvider([
                    'allModels' =>  $this->collection['dataProvider']->allModels,
                    'sort' => $sort,
                    'pagination' => $pagination,
                ]);
            }
            else{
                $provider = new ActiveDataProvider([
                    'query' =>  $this->collection['dataProvider']->query,
                    'sort' => $sort,
                    'pagination' => $pagination,
                ]);
            }

            $data['dataProvider'] = $provider;

            if(!empty($this->collection['params'])){
                foreach ($this->collection['params'] as $key => $param) {
                    $data[$key] = $param;
                }
            }

            $data['fileName'] = $this->fileName;
            Yii::$app->controller->renderPartial($this->view, $data, true, false);
        }
        catch (Exception $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
    }

    public function exportCsvSelectedItem(){
        try {
            $modelName = $this->modelName;
            $items = explode(',', Yii::$app->request->post('items'));

            if(!is_array($items)) {
                throw new Exception(Yii::t('app', 'No record found.'));
            }

            $query = $modelName::find()->where(['in', $this->primaryKey, $items]);

            $provider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => ['pageSize' => false],
                'sort' => false,
            ]);

            $data = ['dataProvider' => $provider];
            Yii::$app->controller->renderPartial($this->view, $data, true, false);
        }
        catch (Exception $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
    }
}

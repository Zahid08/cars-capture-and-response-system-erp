<?php
namespace unlock\modules\core\actions;

use Yii;
use yii\base\Action;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\data\ArrayDataProvider;
use yii\data\SqlDataProvider;
use yii\db\ActiveRecord;
use yii\web\BadRequestHttpException;
use unlock\modules\core\helpers\Collection;
use unlock\modules\core\helpers\ExportPdf;
use yii\data\ActiveDataProvider;

/**
 * Class ExportPdfAction
 *
 */
class ExportPdfAction extends Action
{
    public $modelName;

    public $view;

    public $primaryKey = 'id';

    public $redirect = ['index'];

    public $fileName = 'exportPdf.pdf';

    public $mode;

    public $format;

    public $orientation;

    public $sort = true; // true|false

    public $pagination = false; // true|false

    public $pageSize;

    public $dataProviderType; // ActiveDataProvider | SqlDataProvider | ArrayDataProvider

    public $collection;

    public $controllerAction;

    public function init()
    {
        parent::init();

        if (!isset($this->controllerAction)) {
            $this->controllerAction = 'index';
        }
        if (!isset($this->modelName)) {
            throw new InvalidConfigException(Yii::t('app', 'Model name should be set in controller actions'));
        }
        if (!class_exists($this->modelName)) {
            throw new InvalidConfigException(Yii::t('app', 'Model class does not exists'));
        }
        if (isset($this->fileName)) {
            ExportPdf::$fileName = $this->fileName;
        }
        if (isset($this->mode)) {
            ExportPdf::$mode = $this->mode;
        }
        if (isset($this->format)) {
            ExportPdf::$format = $this->format;
        }
        if (isset($this->orientation)) {
            ExportPdf::$orientation = $this->orientation;
        }
        if (!isset($this->view)) {
            $this->view = Yii::$app->getModule(Yii::$app->controller->module->id)->viewNamespace . Yii::$app->controller->id . '/export/grid-export-pdf.php';
            ExportPdf::$view = $this->view;
        }
        else{
            $this->view = Yii::$app->getModule(Yii::$app->controller->module->id)->viewNamespace . Yii::$app->controller->id . '/' . $this->view . '.php';
            ExportPdf::$view = $this->view;
        }

        // Set Collection
        $collection = Collection::getData($this->controllerAction);
        if (!isset($collection)) {
            throw new InvalidConfigException(Yii::t('app', 'Need to set collection'));
        }
        else{
            $this->collection = $collection;
        }
    }

    public function run()
    {
        if(empty(Yii::$app->request->post('items')) || Yii::$app->request->post('items') == 'export-pdf'){
            $this->exportPdf();
        }
        else {
            $this->exportPdfSelectedItem();
        }

        return $this->controller->redirect(Yii::$app->request->getReferrer());
    }

    public function exportPdf(){
        try {
            $data = [];

            if(empty($this->collection['dataProvider'])){
                throw new Exception(Yii::t('app', 'No record found.'));
            }

            $sort = ($this->sort == false) ? false : $this->collection['dataProvider']->sort;

            $totalCount = $this->collection['dataProvider']->getTotalCount();

            if($this->pagination == true){
                $pagination = $this->collection['dataProvider']->pagination;
            }
            else if($totalCount > 500){
                $pagination = ['pageSize' => 500];
            }
            else if(isset($this->pageSize)){
                $pagination = ['pageSize' => $this->pageSize];
            }
            else{
                $pagination = ['pageSize' => false];
            }


            if($this->dataProviderType == 'SqlDataProvider'){
                $provider = new SqlDataProvider([
                    'sql' =>  $this->collection['dataProvider']->query->createCommand()->getRawSql(),
                    'sort' => $sort,
                    'pagination' => $pagination,
                ]);
            }
            else if($this->dataProviderType == 'ArrayDataProvider'){
                $provider = new ArrayDataProvider([
                    'allModels' =>  $this->collection['dataProvider']->allModels,
                    'sort' => $sort,
                    'pagination' => $pagination,
                ]);
            }
            else{
                $provider = new ActiveDataProvider([
                    'query' =>  $this->collection['dataProvider']->query,
                    'sort' => $sort,
                    'pagination' => $pagination,
                ]);
            }

            $data['dataProvider'] = $provider;

            if(!empty($this->collection['params'])){
                foreach ($this->collection['params'] as $key => $param) {
                    $data[$key] = $param;
                }
            }

            ExportPdf::exportGridPdf($data);
        }
        catch (Exception $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
    }

    public function exportPdfSelectedItem(){
        try {
            $modelName = $this->modelName;
            $items = explode(',', Yii::$app->request->post('items'));

            if(!is_array($items)) {
                throw new Exception(Yii::t('app', 'No record found.'));
            }

            $query = $modelName::find()->where(['in', $this->primaryKey, $items]);

            $provider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => ['pageSize' => false],
                'sort' => false,
            ]);

            $data = ['dataProvider' => $provider];
            ExportPdf::exportGridPdf($data);
        }
        catch (Exception $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
    }
}

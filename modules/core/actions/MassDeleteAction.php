<?php
namespace unlock\modules\core\actions;

use unlock\modules\core\helpers\CommonHelper;
use Yii;
use yii\base\Action;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\web\BadRequestHttpException;

/**
 * Class ToggleAction
 *
 * @package yii2mod\toggle\actions
 */
class MassDeleteAction extends Action
{
    public $modelName = '';

    public $attribute = '';

    public $markAsDeleted = false;

    public $deletedMarkValue = 0;

    public $primaryKey = 'id';

    public $redirect = ['index'];


    public function init()
    {
        parent::init();

        if (!isset($this->modelName)) {
            throw new InvalidConfigException(Yii::t('app', 'Model name should be set in controller actions'));
        }
        if (!class_exists($this->modelName)) {
            throw new InvalidConfigException(Yii::t('app', 'Model class does not exists'));
        }
    }

    public function run()
    {
        try {
            $modelName = $this->modelName;
            $items = explode(',', Yii::$app->request->post('items'));

            if(!is_array($items)) {
                throw new Exception(Yii::t('app', 'Please select item(s)'));
            }

            $items = $modelName::find()->where(['in', $this->primaryKey, $items])->all();
            foreach ($items as $item) {
                if ($this->markAsDeleted === true) {
                    $item->setAttribute($this->attribute, $this->deletedMarkValue);
                    if (!$item->save()) {
                        throw new Exception(Yii::t('app', Html::errorSummary($item)));
                    }
                }
                else {
                    if (!$item->delete()) {
                        throw new Exception(Yii::t('app', 'The record cannot be deleted.'));
                    }
                }
            }

            Yii::$app->session->setFlash('success', Yii::t('app', count($items) . ' record(s) have been successfully deleted.'));

        }
        catch (Exception $e) {
            $message = $e->getMessage();
            if($e->getName() == CommonHelper::INTEGRITY_CONSTRAINT_VIOLATION){
                $message = "Record has relational data. You must delete the relational data first";
            }
            Yii::$app->session->setFlash('error', $message);
        }

        return $this->controller->redirect(Yii::$app->request->getReferrer());
    }
}

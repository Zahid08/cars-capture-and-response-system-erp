<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 7/8/2019
 * Time: 3:19 PM
 */

namespace unlock\modules\core\helpers;

use Yii;
use api\common\maropost\maropost;
use unlock\modules\contacts\models\ContactsEmailLog;
use unlock\modules\developments\models\Developments;
use unlock\modules\usermanager\user\models\User;

class MaropostHelper
{

    public  function sendNewLeadEmail($contact_system_model,$cars_contact_url) {
        $baseAuthhorId = Yii::getAlias('@baseAuthhor');
        $checkAssignedUser=Developments::find()->where(['development_id' => $contact_system_model->development_id])->one();
        $developement='for'.' '.$contact_system_model->development_name;
        $developmentname=$contact_system_model->development_name;
        if (!empty($contact_system_model->realtor)){
            $realtor='Yes';
        }
        else{
            $realtor='No';
        }
        $subject =' '.$contact_system_model->first_name.' '.$contact_system_model->last_name.' for '.$developmentname;
        $message='<table width="100%" cellspacing="0" cellpadding="0" style="table-layout:fixed;" >
                <tr>
                    <td>Howdy Partner,</td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                <tr>
                    <td>We have a new lead '.$developement.'!</td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                <tr>
                     <td style="line-height: 20px;">
                        First Name:<span>'.' '.$contact_system_model->first_name .'</span><br/>
                        Last Name:<span>'.' '.$contact_system_model->last_name .'</span><br/>
                        Phone:<span>'.' '.$contact_system_model->phone .'</span><br/>
                        Email:<span>'.' '.$contact_system_model->email .'</span><br/>
                        Is a Realtor?:<span>'.' '.$realtor .'</span>
                     </td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                <tr>
                     <td style="line-height: 20px;">
                        Development Name:<span>'.' '.$developmentname.'</span><br/>
                        Media Source:<span>'.' '.'Website'.'</span><br/>
                        Page URL:<span>'.' '.$contact_system_model->page_url.'</span><br/>
                        Tag Applied:<span>'.' '.$contact_system_model->affiliate_tag.'</span><br/>
                        Brand: <span>CONNECT asset Management</span>
                     </td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                 <tr>
                     <td>
                        <a href="'.$cars_contact_url.'" style="text-decoration: none;font-size: 16px;border-radius: 3px;">Learn More....</a>
                     </td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                <tr> 
                    <td>Thanks,</td>
                </tr>
                 <tr>
                     <td height="30"></td>
                </tr>
                 <tr> 
                    <td>Reggie</td>
                </tr>
                <tr>
                     <td height="30"></td>
                </tr>
                <tr> 
                    <td>Confidential Information - Obtain Release Permission</td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                <tr> 
                    <td>www.connectassetmanagement.com </td>
                </tr>
            </table>';

        $maropost=new maropost();
        $arrayNew = array(
            "content" => array(
                "name" =>'Admin System Global Mail Content [Daily]',
                //"text_part" => "text part of the content",
                "html_part" => $message,
                "full_email" => "false",
                "footer_type" => "default_footer",
                "footer_id" => ""
            ),
            "id" =>191
        );
        $maropost->put_template_content($arrayNew);

        $assign_id='';
        if (!empty($checkAssignedUser->assignment_id)){
            $assign_id=$checkAssignedUser->assignment_id;
        }
        else{
            $assign_id=$baseAuthhorId;
        }
        $assign_user=User::find()->where(['id'=>$assign_id])->one();
        $response_register=self::register_user_for_new_lead_email($assign_user,$subject);
        if ($response_register['http_status']==201){
            $email_log=new ContactsEmailLog();
            $email_log->contact_id=$contact_system_model->id;
            $email_log->user_id=$assign_user->id;
            $email_log->subject=$subject;
            $email_log->mail_header=$subject;
            $email_log->mail_body=$message;
            $email_log->sent_date_time=date('Y-m-d H:i:s');
            $email_log->status=1;
            if (!$email_log->save(false)) {
                throw new Exception(json_encode($email_log->errors));
            }
        }
    }
    public static function  customerMail($contact_system_model){
        $InfusionTag=[];
        if (!empty($contact_system_model->development_id)) {
            $develomentList = Developments::find()->where(['development_id' => $contact_system_model->development_id])->one();
            if (!empty($develomentList)) {
                $projectTitle = $develomentList->development_name;
                $project_sales_status = $develomentList->sales_status;
                $projectName = $develomentList->development_name;
                $projectUrl =$contact_system_model->page_url;
                $InfusionTag['infusionsoftTag'] = $develomentList->development_name . ' ' . 'Registration Journey';
            } else {
                $projectTitle =$contact_system_model->development_name;
                $project_sales_status ='comingsoon';
                $projectName =$contact_system_model->development_name;
                $projectUrl = $contact_system_model->page_url;
                $InfusionTag['infusionsoftTag'] =$contact_system_model->development_name.' ' . 'Registration Journey';
            }
        }
        else{
            $projectTitle =$contact_system_model->development_name;
            $project_sales_status ='comingsoon';
            $projectName =$contact_system_model->development_name;
            $projectUrl = $contact_system_model->page_url;
            $InfusionTag['infusionsoftTag'] =$contact_system_model->development_name.' ' . 'Registration Journey';
        }

        $realtor = '';
        $prospect = '';
        $realtor_tag = '';

        if (!empty($contact_system_model->realtor)) {
            $realtor_ = 'true';
            $prospect = 'False';
            $realtor = 1;
            $realtor_tag = 'Realtor';
            $list_id = 17;
        } else {
            $realtor_ = 'False';
            $prospect = 'true';
            $realtor_tag = 'Prospect';
            $realtor = 0;
            $list_id = 1;
        }

        //get sales_status
        $sales_status = '';

        if ($project_sales_status == 'comingsoon') {
            $sales_status = 'Registration';
        } else if ($project_sales_status == 'active') {
            $sales_status = 'Available';
        } else if ($project_sales_status == 'archived') {
            $sales_status = 'Sold Out';
        }

        $projectDownloadsEnabled =0;
        $contact_system_model['media_source']='Original Media Source -'.$contact_system_model->media_source;


        $tags=new tags();
        //tag created if tag not exist in maropost
        $array = array(
            "tags" => array(
                "name" =>$InfusionTag['infusionsoftTag']
            )
        );
        $tags->post_tags($array);


        $array1 = array(
            "tags" => array(
                "name" =>$contact_system_model['media_source']
            )
        );
        $tags->post_tags($array1);

        /*
         * data prepare for maropost
         */
        $contactsNew=new maropost();
        $contactsNew = $contactsNew->contacts();

        $get_contact_by_email = $contactsNew->get_contact_by_contact_email($contact_system_model->email);


        //echo '<pre>'; print_r($get_contact_by_email); exit();

        $user_existing_status = 1;
        $subscriptions_status = 1;


        if ($get_contact_by_email['http_status'] == 404) {
            $user_existing_status = 0;
        } else {

            $list_subscriptions = $get_contact_by_email['list_subscriptions'];

            foreach ($list_subscriptions as $subscriptions) {
                //
            }
            if ($subscriptions['status'] == 'Unsubscribed') {
                $subscriptions_status = 0;
            }
        }


        if ($sales_status == 'Registration') {

            if ($realtor == 1) {

                $arrayNew = array(
                    'list_id' => $list_id,
                    'contact' =>
                        array(
                            'first_name' => $contact_system_model->first_name,
                            'last_name' => $contact_system_model->last_name,
                            'email' => $contact_system_model->email,
                            'subscribe' => 'true',
                            'custom_field' =>
                                array(
                                    'phone_1' => $contact_system_model->phone,
                                    'company' => 'CONNECT asset management',
                                    'realtor' => $realtor_,
                                    'prospect' => $prospect,
                                    'comment_question' => $projectName,
                                    'development_name' => $projectTitle,
                                    'reference_link' => $projectUrl,
                                    'development_reg_realtor' => time(),
                                ),
                            'add_tags' =>
                                array(
                                    $InfusionTag['infusionsoftTag'],
                                    $contact_system_model['media_source'],
                                    $realtor_tag
                                )
                        ));


            } else {

                if ($user_existing_status == 1) {

                    if ($subscriptions_status == 1) {
                        $arrayNew = array(
                            'list_id' => $list_id,
                            'contact' =>
                                array(
                                    'first_name' => $contact_system_model->first_name,
                                    'last_name' => $contact_system_model->last_name,
                                    'email' => $contact_system_model->email,
                                    'subscribe' => 'true',
                                    'custom_field' =>
                                        array(
                                            'phone_1' => $contact_system_model->phone,
                                            'company' => 'CONNECT asset management',
                                            'realtor' => $realtor_,
                                            'prospect' => $prospect,
                                            'comment_question' => $projectName,
                                            'development_name' => $projectTitle,
                                            'reference_link' => $projectUrl,
                                            'development_reg_existing' => time(),
                                        ),
                                    'add_tags' =>
                                        array(
                                            $InfusionTag['infusionsoftTag'],
                                            $contact_system_model['media_source'],
                                            $realtor_tag
                                        )
                                ));
                    }

                } else if ($user_existing_status == 0) {
                    $arrayNew = array(
                        'list_id' => $list_id,
                        'contact' =>
                            array(
                                'first_name' => $contact_system_model->first_name,
                                'last_name' => $contact_system_model->last_name,
                                'email' => $contact_system_model->email,
                                'custom_field' =>
                                    array(
                                        'phone_1' => $contact_system_model->email,
                                        'company' => 'CONNECT asset management',
                                        'realtor' => $realtor_,
                                        'prospect' => $prospect,
                                        'comment_question' => $projectName,
                                        'development_name' => $projectTitle,
                                        'reference_link' => $projectUrl,
                                        'development_reg_new_user' => time(),
                                    ),
                                'add_tags' =>
                                    array(
                                        $InfusionTag['infusionsoftTag'],
                                        $contact_system_model['media_source'],
                                        $realtor_tag
                                    )
                            ));
                }


            }

        } else if ($sales_status == 'Available') {

            if ($projectDownloadsEnabled != 1) {

                if ($realtor == 1) {

                    $arrayNew = array(
                        'list_id' => $list_id,
                        'contact' =>
                            array(
                                'first_name' => $contact_system_model->first_name,
                                'last_name' => $contact_system_model->last_name,
                                'email' => $contact_system_model->email,
                                'subscribe' => 'true',
                                'custom_field' =>
                                    array(
                                        'phone_1' =>  $contact_system_model->phone,
                                        'company' => 'CONNECT asset management',
                                        'realtor' => $realtor_,
                                        'prospect' => $prospect,
                                        'comment_question' => $projectName,
                                        'development_name' => $projectTitle,
                                        'reference_link' => $projectUrl,
                                        'development_reg_realtor' => time(),
                                    ),
                                'add_tags' =>
                                    array(
                                        $InfusionTag['infusionsoftTag'],
                                        $contact_system_model['media_source'],
                                        $realtor_tag
                                    )
                            ));


                } else {

                    if ($user_existing_status == 1) {

                        if ($subscriptions_status == 1) {
                            $arrayNew = array(
                                'list_id' => $list_id,
                                'contact' =>
                                    array(
                                        'first_name' => $contact_system_model->first_name,
                                        'last_name' => $contact_system_model->last_name,
                                        'email' => $contact_system_model->email,
                                        'subscribe' => 'true',
                                        'custom_field' =>
                                            array(
                                                'phone_1' => $contact_system_model->phone,
                                                'company' => 'CONNECT asset management',
                                                'realtor' => $realtor_,
                                                'prospect' => $prospect,
                                                'comment_question' => $projectName,
                                                'development_name' => $projectTitle,
                                                'reference_link' => $projectUrl,
                                                'development_reg_existing' => time(),
                                            ),
                                        'add_tags' =>
                                            array(
                                                $InfusionTag['infusionsoftTag'],
                                                $contact_system_model['media_source'],
                                                $realtor_tag
                                            )
                                    ));
                        }

                    } else if ($user_existing_status == 0) {
                        $arrayNew = array(
                            'list_id' => $list_id,
                            'contact' =>
                                array(
                                    'first_name' => $contact_system_model->first_name,
                                    'last_name' => $contact_system_model->last_name,
                                    'email' => $contact_system_model->email,
                                    'subscribe' => 'true',
                                    'custom_field' =>
                                        array(
                                            'phone_1' =>  $contact_system_model->phone,
                                            'company' => 'CONNECT asset management',
                                            'realtor' => $realtor_,
                                            'prospect' => $prospect,
                                            'comment_question' => $projectName,
                                            'development_name' => $projectTitle,
                                            'reference_link' => $projectUrl,
                                            'development_reg_new_user' => time(),
                                        ),
                                    'add_tags' =>
                                        array(
                                            $InfusionTag['infusionsoftTag'],
                                            $contact_system_model['media_source'],
                                            $realtor_tag
                                        )
                                ));
                    }


                }

            }

        } else if ($sales_status == 'Sold Out') {

            if ($realtor == 1) {

                $arrayNew = array(
                    'list_id' => $list_id,
                    'contact' =>
                        array(
                            'first_name' => $contact_system_model->first_name,
                            'last_name' => $contact_system_model->last_name,
                            'email' => $contact_system_model->email,
                            'subscribe' => 'true',
                            'custom_field' =>
                                array(
                                    'phone_1' => $contact_system_model->phone,
                                    'company' => 'CONNECT asset management',
                                    'realtor' => $realtor_,
                                    'prospect' => $prospect,
                                    'comment_question' => $projectName,
                                    'development_name' => $projectTitle,
                                    'reference_link' => $projectUrl,
                                    'development_reg_soldout_realtor' => time(),
                                ),
                            'add_tags' =>
                                array(
                                    $InfusionTag['infusionsoftTag'],
                                    $contact_system_model['media_source'],
                                    $realtor_tag
                                )
                        ));

            } else {

                if ($user_existing_status == 1) {

                    if ($subscriptions_status == 1) {

                        $arrayNew = array(
                            'list_id' => $list_id,
                            'contact' =>
                                array(
                                    'first_name' => $contact_system_model->first_name,
                                    'last_name' => $contact_system_model->last_name,
                                    'email' => $contact_system_model->email,
                                    'subscribe' => 'true',
                                    'custom_field' =>
                                        array(
                                            'phone_1' => $contact_system_model->phone,
                                            'company' => 'CONNECT asset management',
                                            'realtor' => $realtor_,
                                            'prospect' => $prospect,
                                            'comment_question' => $projectName,
                                            'development_name' => $projectTitle,
                                            'reference_link' => $projectUrl,
                                            'development_reg_soldout_existing' => time(),
                                        ),
                                    'add_tags' =>
                                        array(
                                            $InfusionTag['infusionsoftTag'],
                                            $contact_system_model['media_source'],
                                            $realtor_tag
                                        )
                                ));
                    }

                } else if ($user_existing_status == 0) {

                    $arrayNew = array(
                        'list_id' => $list_id,
                        'contact' =>
                            array(
                                'first_name' => $contact_system_model->first_name,
                                'last_name' => $contact_system_model->last_name,
                                'email' => $contact_system_model->email,
                                'subscribe' => 'true',
                                'custom_field' =>
                                    array(
                                        'phone_1' => $contact_system_model->phone,
                                        'company' => 'CONNECT asset management',
                                        'realtor' => $realtor_,
                                        'prospect' => $prospect,
                                        'comment_question' => $projectName,
                                        'development_name' => $projectTitle,
                                        'reference_link' => $projectUrl,
                                        'development_reg_soldout_new_user' => time(),
                                    ),
                                'add_tags' =>
                                    array(
                                        $InfusionTag['infusionsoftTag'],
                                        $contact_system_model['media_source'],
                                        $realtor_tag
                                    )
                            ));
                }

            }
        }

        if (empty($arrayNew)) {
            $arrayNew = array(
                'list_id' => $list_id,
                'contact' =>
                    array(
                        'first_name' => $contact_system_model->first_name,
                        'last_name' => $contact_system_model->last_name,
                        'email' => $contact_system_model->email,
                        'subscribe' => 'true',
                        'custom_field' =>
                            array(
                                'phone_1' =>$contact_system_model->phone,
                                'company' => 'CONNECT asset management',
                                'realtor' => $realtor_,
                                'prospect' => $prospect,
                                'comment_question' => $projectName,
                                'development_name' => $projectTitle,
                                'reference_link' => $projectUrl,
                            ),
                        'add_tags' =>
                            array(
                                $InfusionTag['infusionsoftTag'],
                                $contact_system_model['media_source'],
                                $realtor_tag
                            )
                    ));
        }

        $arrayNewsletter = $arrayNew;
        if($arrayNewsletter['list_id'] == 1){
            $arrayNewsletter['list_id'] = 3;
            $contactsNew->post_new_contact_into_list($arrayNewsletter);
        }
        $response=$contactsNew->post_new_contact_into_list($arrayNew);
    }
    //@@New Lead Register Automation Mail Maropost Function Calling
    public function register_user_for_new_lead_email($assign_user,$subject){
        $first_name =   !empty($assign_user->first_name) ? $assign_user->first_name : '';
        $last_name =   !empty($assign_user->last_name) ? $assign_user->last_name : '';
        $mobile =   !empty($assign_user->mobile) ? $assign_user->mobile : '';
        $email =   !empty($assign_user->email) ? $assign_user->email : '';
        $list_id=50;
        $contactsNew=new maropost();
        $contactsNew = $contactsNew->contacts();
        $arrayNew = array(
            'list_id' => $list_id,
            'contact' =>
                array(
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'email' => $email,
                    'subscribe' => 'true',
                    'custom_field' =>
                        array(
                            'phone_1' =>$mobile,
                            'company' => 'CONNECT asset management',
                            'realtor' => 'False',
                            'prospect' => 'true',
                            'system_mail_subject' => $subject,
                            'system_email_status' => time()
                        )
                ));
        $response=$contactsNew->post_new_contact_into_list($arrayNew);
        return $response;
    }
}
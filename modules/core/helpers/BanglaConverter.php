<?php
namespace unlock\modules\core\helpers;

use Yii;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

class BanglaConverter {

    public static $eng_to_bn_number = array('1'=>'১', '2'=>'২', '3'=>'৩', '4'=>'৪', '5'=>'৫','6'=>'৬', '7'=>'৭', '8'=>'৮', '9'=>'৯', '0'=>'০');

    public static $eng_to_bn_day = array('Saturday' => 'শনিবার', 'Sunday' => 'রবিবার', 'Monday' => 'সোমবার', 'Tuesday' => 'মঙ্গলবার', 'Wednesday' => 'বুধবার', 'Thursday' => 'বৃহস্পতিবার', 'Friday' => 'শুক্রবার');

    public static $eng_to_bn_month = array('January' => 'জানুয়ারী', 'February' => 'ফেব্রুয়ারী', 'March' => 'মার্চ', 'April' => 'এপ্রিল', 'May' => 'মে', 'June' => 'জুন', 'July' => 'জুলাই', 'August' => 'আগস্ট', 'September' => 'সেপ্টেম্বর', 'October' => 'অক্টোবর', 'November' => 'নভেম্বর', 'December' => 'ডিসেম্বর');

    public static function engToBnNumber($number) {
        return strtr($number, self::$eng_to_bn_number);
    }

    public static function engToBnDay($day) {
        return strtr($day, self::$eng_to_bn_day);
    }

    public static function engToBnMonth($month) {
        return strtr($month, self::$eng_to_bn_month);
    }

    public static function engToBnDate($date) {
        $eng_to_bn_date = self::$eng_to_bn_number + self::$eng_to_bn_month + self::$eng_to_bn_day;
        return strtr($date, $eng_to_bn_date);
    }

    public static function getMonthList($id=null)
    {
        $data = [
            1  => 'জানুয়ারী',
            2  => 'ফেব্রুয়ারি',
            3  => 'মার্চ',
            4  => 'এপ্রিল',
            5  => 'মে',
            6  => 'জুন ',
            7  => 'জুলাই',
            8  => 'অগাস্ট',
            9  => 'সেপ্টেম্বর',
            10 => 'অক্টোবর',
            11 => 'নভেম্বর',
            12 => 'ডিসেম্বর',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }

    // number in words
    public static function numberToBanglaWords($number)
    {
        if(empty($number)){
            return '';
        }

        $number = $number + 0;

        $hyphen      = ' ';
        $conjunction = ' '; // and
        $separator   = ' '; // ,
        $negative    = 'ঋণাত্মক ';
        $decimal     = ' দশমিক '; // point
        $dictionary = array(
            1 => 'এক',
            2 => 'দুই',
            3 => 'তিন',
            4 => 'চার',
            5 => 'পাঁচ',
            6 => 'ছয়',
            7 => 'সাত',
            8 => 'আট',
            9 => 'নয়',
            10 => 'দশ',
            11 => 'এগার',
            12 => 'বার',
            13 => 'তের',
            14 => 'চৌদ্দ',
            15 => 'পনের',
            16 => 'ষোল',
            17 => 'সতের',
            18 => 'আঠার',
            19 => 'ঊনিশ',
            20 => 'বিশ',
            21 => 'একুশ',
            22 => 'বাইশ',
            23 => 'তেইশ',
            24 => 'চব্বিশ',
            25 => 'পঁচিশ',
            26 => 'ছাব্বিশ',
            27 => 'সাতাশ',
            28 => 'আঠাশ',
            29 => 'ঊনত্রিশ',
            30 => 'ত্রিশ',
            31 => 'একত্রিশ',
            32 => 'বত্রিশ',
            33 => 'তেত্রিশ',
            34 => 'চৌত্রিশ',
            35 => 'পঁয়ত্রিশ',
            36 => 'ছত্রিশ',
            37 => 'সাঁইত্রিশ',
            38 => 'আটত্রিশ',
            39 => 'ঊনচল্লিশ',
            40 => 'চল্লিশ',
            41 => 'একচল্লিশ',
            42 => 'বিয়াল্লিশ',
            43 => 'তেতাল্লিশ',
            44 => 'চুয়াল্লিশ',
            45 => 'পঁয়তাল্লিশ',
            46 => 'ছেচল্লিশ',
            47 => 'সাতচল্লিশ',
            48 => 'আটচল্লিশ',
            49 => 'ঊনপঞ্চাশ',
            50 => 'পঞ্চাশ',
            51 => 'একান্ন',
            52 => 'বায়ান্ন',
            53 => 'তিপ্পান্ন',
            54 => 'চুয়ান্ন',
            55 => 'পঞ্চান্ন',
            56 => 'ছাপ্পান্ন',
            57 => 'সাতান্ন',
            58 => 'আটান্ন',
            59 => 'ঊনষাট',
            60 => 'ষাট',
            61 => 'একষট্টি',
            62 => 'বাষট্টি',
            63 => 'তেষট্টি',
            64 => 'চৌষট্টি',
            65 => 'পঁয়ষট্টি',
            66 => 'ছেষট্টি',
            67 => 'সাতষট্টি',
            68 => 'আটষট্টি',
            69 => 'ঊনসত্তর',
            70 => 'সত্তর',
            71 => 'একাত্তর',
            72 => 'বাহাত্তর',
            73 => 'তিয়াত্তর',
            74 => 'চুয়াত্তর',
            75 => 'পঁচাত্তর',
            76 => 'ছিয়াত্তর',
            77 => 'সাতাত্তর',
            78 => 'আটাত্তর',
            79 => 'ঊনআশি',
            80 => 'আশি',
            81 => 'একাশি',
            82 => 'বিরাশি',
            83 => 'তিরাশি',
            84 => 'চুরাশি',
            85 => 'পঁচাশি',
            86 => 'ছিয়াশি',
            87 => 'সাতাশি',
            88 => 'আটাশি',
            89 => 'ঊননব্বই',
            90 => 'নব্বই',
            91 => 'একানব্বই',
            92 => 'বিরানব্বই',
            93 => 'তিরানব্বই',
            94 => 'চুরানব্বই',
            95 => 'পঁচানব্বই',
            96 => 'ছিয়ানব্বই',
            97 => 'সাতানব্বই',
            98 => 'আটানব্বই',
            99 => 'নিরানব্বই',
            100 => 'শত',
            1000 => 'হাজার',
            100000 => 'লক্ষ',
            10000000 => 'কোটি',
            1000000000000 => 'ট্রিলিয়ান',
            1000000000000000 => 'quadrillion',
            1000000000000000000 => 'quintillion'
        );

        if (!is_numeric($number)) {
            return false;
        }
        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                'amountToWords only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
                E_USER_WARNING
            );
            return false;
        }
        if ($number < 0) {
            return $negative.self::numberToBanglaWords(abs($number));
        }
        $string = $fraction = null;
        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }
        switch (true) {
            case $number < 100:
                $string = $dictionary[$number];
                break;

            case $number < 1000:
                $hundreds  = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction.self::numberToBanglaWords($remainder);
                }
                break;

            case $number < 100000:
                $thousands   = ((int) ($number / 1000));
                $remainder = $number % 1000;

                $thousands = self::numberToBanglaWords($thousands);

                $string .= $thousands . ' ' . $dictionary[1000];
                if ($remainder) {
                    $string .= $separator . self::numberToBanglaWords($remainder);
                }
                break;

            case $number < 10000000:
                $lakhs   = ((int) ($number / 100000));
                $remainder = $number % 100000;

                $lakhs = self::numberToBanglaWords($lakhs);

                $string = $lakhs . ' ' . $dictionary[100000];
                if ($remainder) {
                    $string .= $separator . self::numberToBanglaWords($remainder);
                }
                break;

            case $number < 1000000000:
                $crores   = ((int) ($number / 10000000));
                $remainder = $number % 10000000;

                $crores = self::numberToBanglaWords($crores);

                $string = $crores . ' ' . $dictionary[10000000];
                if ($remainder) {
                    $string .= $separator . self::numberToBanglaWords($remainder);
                }
                break;

            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = self::numberToBanglaWords($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= self::numberToBanglaWords($remainder);
                }
                break;
        }
        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }
        return $string;
    }

}

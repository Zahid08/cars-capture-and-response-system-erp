<?php

namespace unlock\modules\core\helpers;

use Yii;
use yii\helpers\Html;
use yii\base\Widget;
use unlock\modules\core\helpers\FileHelper;

class ExportCsv
{
    public static  $fileName = "export.csv";

    public static $filePath = 'export/';

    public static $deliminator = ";";

    public static $csvCharset = 'UTF-8';

    public static $download = true;

    public static function exportGridCsv($header, $data, $fileName)
    {
        // Export To CSV
        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=" . $fileName);

        $fp = fopen('php://output', 'w');

        // CSV Header Column
        if($header){
            fputcsv($fp, $header);
        }


        // CSV Data
        if($data){
            foreach ($data as $row) {
                fputcsv($fp, $row);
            }
        }

        exit();
    }

    public static function csvHeaderColumn($columns, $models){
        $header = [];

        if(is_object($models[0])){
            $headerLabel = $models[0]->attributeLabels();
        }
        else{
            if(isset($columns) && count($columns) > 0) {
                foreach ($columns as $key => $column) {
                    $key = is_numeric($key) ? $column : $key;
                    $headerLabel[$key] =$column;
                }
            }
        }

        if(isset($columns) && count($columns) > 0){
            foreach ($columns as $key => $column) {
                if(is_numeric($key)){
                    $header[$column] = isset($headerLabel[$column]) ? $headerLabel[$column] : $column;
                }
                else{
                    $header[$key] = isset($column) ? $column : $headerLabel[$key];
                }
            }
        }
        else{
            $header = $headerLabel;
        }

        return $header;
    }

}

<?php
namespace unlock\modules\core\helpers;

use api\modules\v1\models\ContactsBrands;
use api\modules\v1\models\ContactsSysteminfo;
use api\modules\v1\models\ContactsTagsNew;
use unlock\modules\allocation\models\Allocation;
use unlock\modules\assignments\models\Assignments;
use unlock\modules\assignments\models\AssignmentsAssets;
use unlock\modules\assignments\models\AssignmentsBuildingAssets;
use unlock\modules\assignments\models\AssignmentsUnits;
use unlock\modules\contacts\models\Agent;
use unlock\modules\contacts\models\Contacts;
use unlock\modules\contacts\models\ContactsInteractions;
use unlock\modules\contacts\models\ContactsPurchases;
use unlock\modules\contacts\models\ContactsRegistration;
use unlock\modules\contacts\models\ContactsReservations;
use unlock\modules\contacts\models\ContactsRsvp;
use unlock\modules\contacts\models\ContactsTags;
use unlock\modules\contacts\models\ContactsTagsCategories;
use unlock\modules\contacts\models\ContactsTagsRelation;
use unlock\modules\developers\models\Developers;
use unlock\modules\developers\models\DevelopersContacts;
use unlock\modules\developers\models\DevelopersContactsInteractions;
use unlock\modules\developers\models\DevelopersRecognitions;
use unlock\modules\developments\models\DevelopersDevelopmentsRelation;
use unlock\modules\developments\models\Developments;
use unlock\modules\developments\models\DevelopmentsCampaigns;
use unlock\modules\events\models\Events;
use unlock\modules\formtools\models\FormtoolsEvent;
use unlock\modules\formtools\models\FormtoolsEventRealtion;
use unlock\modules\generalsettings\controllers\QuoteController;
use unlock\modules\generalsettings\models\City;
use unlock\modules\generalsettings\models\Country;
use unlock\modules\generalsettings\models\Province;
use unlock\modules\generalsettings\models\Quote;
use unlock\modules\inventory\models\Inventory;
use unlock\modules\newsletters\models\NewslettersComingSoon;
use unlock\modules\newsletters\models\NewslettersFeatureResale;
use unlock\modules\newsletters\models\NewslettersFooter;
use unlock\modules\newsletters\models\NewslettersInvestToday;
use unlock\modules\newsletters\models\NewslettersNews;
use unlock\modules\setting\models\AgentType;
use unlock\modules\setting\models\Brands;
use unlock\modules\setting\models\InteractionType;
use unlock\modules\setting\models\LeadSource;
use unlock\modules\setting\models\MediaType;
use unlock\modules\setting\models\NewsletterStatus;
use unlock\modules\setting\models\RegistrationStatus;
use unlock\modules\setting\models\ReservationStatus;
use unlock\modules\setting\models\RsvpStatus;
use unlock\modules\setting\models\SuiteType;
use unlock\modules\usermanager\user\models\User;
use Yii;
use yii\helpers\ArrayHelper;
use unlock\modules\dynamicdropdown\models\DynamicDropDown;
use unlock\modules\core\models\AutoIncrementNumber;
use yii\helpers\Html;


class CommonHelper {

    const GRID_FILTER_PROMPT = 'All';
    const GRID_PER_PAGE = 50;
    const GRID_PER_PAGE_VALUES = '5,10,20,30,40,50,100,200,500';
    const INTEGRITY_CONSTRAINT_VIOLATION = 'Integrity constraint violation';

    // Status
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
    const STATUS_DELETED = -2;

    const STATUS_PAID = 1;
    const STATUS_APPROVED = 2;
    const STATUS_CANCEL = 3;
    const STATUS_PENDING = 4;
    const STATUS_VERIFIED = 5;

    // Boolean
    const YES = 1;
    const NO = 0;

    // Role Type
    const  ROLE_TYPE_MAIN = 1;
    const  ROLE_TYPE_ADDITIONAL = 2;
    const  ROLE_TYPE_SUBTRACTION = 3;

    // Role
    const  ROLE_SUPER_ADMIN_PK = 1;
    const  ROLE_ADMIN_PK = 2;
    const  ROLE_REGISTERED_PK = 3;
    const  ROLE_GUEST_PK = 4;

    const  ROLE_SUPER_ADMIN = 'superAdmin';
    const  ROLE_ADMIN = 'admin';
    const  ROLE_REGISTERED = 'registered';
    const  ROLE_GUEST = 'guest';

    // User Group
    const USER_GROUP_ADMIN = 'admin';
    const USER_GROUP_REGISTERED = 'registered';
    const USER_GROUP_SALES = 'sales';
    const USER_GROUP_MARKETING = 'marketing';
    const USER_GROUP_GUEST = 'guest';

    const MOBILE_VERIFICATION_CODE_LENGTH = 6;

    // Language
    const LANGUAGE_BANGLA = 'bd';
    const LANGUAGE_ENGLISH = 'en-US';
    const LANGUAGE_CATEGORY_APP = 'app';
    const LANGUAGE_CATEGORY_YII = 'yii';


    //Developers Status  (1 = Active, 0 = In-Active, 2 = Archive, 3 = Delete)
    const DEVELOPERS_ACTIVE_STATUS = '1';
    const DEVELOPERS_INACTIVE_STATUS = '0';
    const DEVELOPERS_ARCHIVE_STATUS = '2';
    const DEVELOPERS_DELETE_STATUS = '3';

    //Developers Grade  (1 = A+, 2 = A, 3 = B+,4 = B,5 = C, 6 = D)
    const DEVELOPERS_SELECT_GRADE = '';
    const DEVELOPERS_A_PLUS_GRADE = '1';
    const DEVELOPERS_A_GRADE = '2';
    //const DEVELOPERS_B_PLUS_GRADE = '3';
    const DEVELOPERS_B_GRADE = '4';
    const DEVELOPERS_C_GRADE = '5';
    const DEVELOPERS_D_GRADE = '6';


    // Developer Province Dropdown
    const DEVELOPERS_PROVINCE_ONTARIO = '1';
    const DEVELOPERS_PROVINCE_QUEBEC = '2';
    const DEVELOPERS_PROVINCE_MANITOBA = '3';


    // Development Campaigns Type
    const CAMPAIGNS_TYPE_1 = '1'; // Search Ads
    const CAMPAIGNS_TYPE_2 = '2'; // Display Ads
    const CAMPAIGNS_TYPE_3 = '3'; // Digital Flyer
    const CAMPAIGNS_TYPE_4 = '4'; // Investor Alert
    const CAMPAIGNS_TYPE_5 = '5'; // Broadcast

    public static function campaignsTypeDropDownList($id = null){
        $data = [
            self::CAMPAIGNS_TYPE_1 => 'Search Ads',
            self::CAMPAIGNS_TYPE_2 => 'Display Ads',
            self::CAMPAIGNS_TYPE_3 => 'Digital Flyer',
            self::CAMPAIGNS_TYPE_4 => 'Investor Alert',
            self::CAMPAIGNS_TYPE_5 => 'Broadcast',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }

    // Subscribe/Contact/Registration/RSVP/Appointment/Reservation/Survey
    const form_TYPE_1 = '1'; // Subscribe Ads
    const form_TYPE_2 = '2'; // Contact Ads
    const form_TYPE_3 = '3'; // Registration Flyer
    const form_TYPE_4 = '4'; // RSVP Alert
    const form_TYPE_5 = '5'; // Appointment
    const form_TYPE_6 = '6'; // Reservation
    const form_TYPE_7 = '7'; // Survey

    public static function formTypeDropDownList($id = null){
        $data = [
            self::form_TYPE_1 => 'Subscribe',
            self::form_TYPE_2 => 'Contact',
            self::form_TYPE_3 => 'Registration',
            self::form_TYPE_4 => 'RSVP',
            self::form_TYPE_5 => 'Appointment',
            self::form_TYPE_6 => 'Reservation',
            self::form_TYPE_7 => 'Survey',
        ];
        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }

    public static function hourDropdownlist($id=null){
        $data=[];
        for ($i=1;$i<=12;$i++){
            $data[$i] =$i;
        }
        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }


    // Development Campaigns Type
    const CAMPAIGNS_STATE_1 = '1'; // Live

    public static function campaignsStateDropDownList($id = null){
        $data = [
            self::CAMPAIGNS_STATE_1 => 'Live',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }


    // Inventory UNIT Status Type
    const UNIT_STATUS_TYPE_1 = '1'; // Propose
    const UNIT_STATUS_TYPE_2 = '2'; // Discussing
    const UNIT_STATUS_TYPE_3 = '3'; // Rejected
    const UNIT_STATUS_TYPE_4 = '4'; // Available

    public static function unitStatusDropDownList($id = null){
        $data = [
            self::UNIT_STATUS_TYPE_1 => 'Propose',
            self::UNIT_STATUS_TYPE_2 => 'Discussing',
            self::UNIT_STATUS_TYPE_3 => 'Rejected',
            self::UNIT_STATUS_TYPE_4 => 'Available',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }

    // Contacts/Prospect Type
    const CONTACTS_TYPE_1 = '1'; // Registration
    const CONTACTS_TYPE_2 = '2'; // Reservation
    const CONTACTS_TYPE_3 = '3'; // Purchasers
    const CONTACTS_TYPE_4 = '4'; // Contact
    const CONTACTS_TYPE_5 = '5'; // Subscribe

    public static function contactTypeDropDownList($id = null){
        $data = [
            self::CONTACTS_TYPE_1 => 'Registration',
            self::CONTACTS_TYPE_2 => 'Reservation',
            self::CONTACTS_TYPE_3 => 'Purchasers',
            self::CONTACTS_TYPE_4 => 'Contact',
            self::CONTACTS_TYPE_5 => 'Subscribe',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }



    // Contacts/Prospect Deal Status Type
    const CONTACTS_DEAL_STATUS_1 = '1'; // Pending
    const CONTACTS_DEAL_STATUS_2 = '2'; // Submitted
    const CONTACTS_DEAL_STATUS_3 = '3'; // Open


    public static function contactDealStatusDropDownList($id = null){
        $data = [
            self::CONTACTS_DEAL_STATUS_1 => 'Pending',
            self::CONTACTS_DEAL_STATUS_2 => 'Submitted',
            self::CONTACTS_DEAL_STATUS_3 => 'Open',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }


    // Contacts/Prospect RSVP Source Type

    const CONTACTS_RSVP_SOURCE_1 = '1'; // Landing Page
    const CONTACTS_RSVP_SOURCE_2 = '2'; // Email
    const CONTACTS_RSVP_SOURCE_3 = '3'; // SMS
    const CONTACTS_RSVP_SOURCE_4 = '4'; // In Person

    public static function contactRSVPSourceDropDownList($id = null){
        $data = [
            self::CONTACTS_RSVP_SOURCE_1 => 'Landing Page',
            self::CONTACTS_RSVP_SOURCE_2 => 'Email',
            self::CONTACTS_RSVP_SOURCE_3 => 'SMS',
            self::CONTACTS_RSVP_SOURCE_4 => 'In Person',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }

    // Contacts/Prospect RSVP status Type

    const CONTACTS_RSVP_STATUS_1 = '1'; // Confirmed
    const CONTACTS_RSVP_STATUS_2 = '2'; // Attended
    const CONTACTS_RSVP_STATUS_3 = '3'; // Declined

    public static function contactRSVPStatusDropDownList($id = null){
        $data = [
            self::CONTACTS_DEAL_STATUS_1 => 'Confirmed',
            self::CONTACTS_RSVP_STATUS_2 => 'Attended',
            self::CONTACTS_RSVP_STATUS_3 => 'Declined',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }


    public static function getRsvpStatus($id = null){
        if($id){
            $model = RsvpStatus::find()->where(['id' => $id])->one();
        }else{
            $model = RsvpStatus::find()->orderBy(  ['created_at' => SORT_DESC])->all();
        }
        return $model;
    }
    public static function rsvfDropdownList(){
        $model = RsvpStatus::find()->all();
        $city = ArrayHelper::map($model, 'id', 'rsvp_status_name');
        return $city;
    }

    public static function mediaSourceTypeDropdownList(){
        $model=MediaType::find()->all();
        $media_type=ArrayHelper::map($model,'id','media_type_name');
        return $media_type;
    }

    // Contact ID by Interactions

    public static function contactIdByInteraction($id = null){

        if($id){
            $model = ContactsInteractions::find()->where(['contact_id' => $id])->orderBy(  ['created_at' => SORT_DESC])->all();
        }else{
            $model = ContactsInteractions::find()->all();
        }
        return $model;

    }

    // Contact ID by Reservations

    public static function contactIdByReservation($id = null){
        if($id){
            $model = ContactsReservations::find()->where(['contact_id' => $id])->orderBy(  ['id' => SORT_DESC])->all();
        }else{
            $model = ContactsReservations::find()->all();
        }
        return $model;
    }

    // Contact ID by Purchases

    public static function contactIdByPurchases($id = null){
        if($id){
            $model = ContactsPurchases::find()->where(['contact_id' => $id])->all();
        }else{
            $model = ContactsPurchases::find()->all();
        }
        return $model;
    }

    // Contact ID by RSVP

    public static function contactIdByRSVP($id = null){
        if($id){
            $model = ContactsRsvp::find()->where(['contact_id' => $id])->orderBy(['id' => SORT_DESC])->all();
        }else{
            $model = ContactsRsvp::find()->all();
        }
        return $model;
    }

    // Development Id by RSVP

    public static function developentIdByRSVP($id = null){
        if($id){
            $model = ContactsRsvp::find()->where(['development_id' => $id])->orderBy(['id' => SORT_DESC])->all();
        }else{
            $model = ContactsRsvp::find()->all();
        }
        return $model;
    }
    // Contact ID by Tags

    public static function contactIdByTags($id = null){
        if($id){
            $model = ContactsTags::find()->where(['contact_id' => $id])->orderBy(['id' => SORT_DESC])->all();
        }else{
            $model = ContactsTags::find()->all();
        }
        return $model;
    }

    public static function contactIdSearchByTagsNew($id = null){
        if($id){
            $searchModel=ContactsTagsRelation::find()->where(['contact_id' => $id])->orderBy(['tag_id' => SORT_DESC])->groupBy('contact_id,tag_id')->all();
        }else{
            $searchModel = ContactsTagsRelation::find()->all();
        }
        return $searchModel;
    }

    public static function contactIdByTagsNew($tag_id = null){
        if($tag_id){
            $model =ContactsTagsNew::find()->where(['id' => $tag_id])->limit('1')->one();
        }else{
            $model = ContactsTagsNew::find()->all();
        }
        return $model;
    }
    public static function eventDateByFormId($form_id = null){
        $events=[];
        $eventsRelation=[];
        if($form_id){
            $eventmodel =FormtoolsEvent::find()->where(['form_id' => $form_id])->orderBy(['event_date' => SORT_ASC])->all();
            foreach ($eventmodel as $eventModelItem){
                $getEventModelRelation=self::eventTimeByEventId($eventModelItem->id);
                foreach ($getEventModelRelation as $eventModelRelationItem){
                    $eventsRelation[]= $eventModelRelationItem->event_start_time.' '.$eventModelRelationItem->time_slote_start.'-'.$eventModelRelationItem->event_end_time.' '.$eventModelRelationItem->time_slote_end;
                }
                $events[CommonHelper::changeDateFormat($eventModelItem->event_date,'m/d/Y')] = $eventsRelation;
                $eventsRelation=[];
            }
        }else{
            $model = FormtoolsEvent::find()->all();
        }

        return $events;
    }
    public static function getEventDateByFormID($form_id = null){
        $events=[];
        if(!empty($form_id)){
            $eventmodel =FormtoolsEvent::find()->where(['form_id' => $form_id])->orderBy(['event_date' => SORT_ASC])->all();
            foreach ($eventmodel as $eventModelItem){
                $events[] .=CommonHelper::changeDateFormat($eventModelItem->event_date,'m/d/Y');
            }
        }
        return $events;
    }

    public static function eventTimeByEventId($event_id = null){
        if($event_id){
            $model =FormtoolsEventRealtion::find()->where(['event_id' => $event_id])->all();
        }else{
            $model = FormtoolsEventRealtion::find()->all();
        }
        return $model;
    }


    // Contact ID by Brand

    public static function contactIdByBrand($id = null){
        if($id){
            $model = ContactsBrands::find()->where(['contact_id' => $id])->all();
        }else{
            $model = ContactsBrands::find()->all();
        }
        return $model;
    }


    // Contact ID by Tags

    public static function contactIdBySystemInfo($id = null){
        if($id){
            $model = ContactsSysteminfo::find()->where(['contact_id' => $id])->orderBy(['id' => SORT_DESC])->all();
        }else{
            $model = ContactsSysteminfo::find()->all();
        }
        return $model;
    }
    public static function contactIdBySystemInfoContactType($id = null){
        if($id){
            $model = ContactsSysteminfo::findOne($id);
        }else{
            $model = ContactsSysteminfo::find()->all();
        }
        return $model;
    }

   //system ingfo appointment
    public static function contactSystemInfoAppointment($id=null){
        if($id){
            $model = ContactsSysteminfo::find()->where(['contact_id' => $id])->andWhere(['event'=>'true'])->andWhere(['form_type'=>'appointment'])->orderBy(['id' => SORT_DESC])->all();
        }else{
            $model = ContactsSysteminfo::find()->all();
        }
        return $model;
    }

    //From development get appoint ment
    public static function contactSystemInfoAppointmentDevelopments($development_id){
        if (!empty($development_id)){
            $model = ContactsSysteminfo::find()->where(['development_id' => $development_id])->andWhere(['event'=>'true'])->andWhere(['form_type'=>'appointment'])->orderBy(['id' => SORT_DESC])->all();
        }
        else{
            $model = ContactsSysteminfo::find()->where(['development_id' => $development_id])->andWhere(['event'=>'true'])->andWhere(['form_type'=>'appointment'])->orderBy(['id' => SORT_DESC])->all();
        }
        return $model;
    }

    public static function contactSystemInfoAll($development_id){
        if (!empty($development_id)){
            $model = ContactsSysteminfo::find()->where(['development_id' => $development_id])->andWhere(['event'=>'true'])->orderBy(['id' => SORT_DESC])->all();
        }
        else{
            $model = ContactsSysteminfo::find()->where(['development_id' => $development_id])->andWhere(['event'=>'true'])->orderBy(['id' => SORT_DESC])->all();
        }
        return $model;
    }

    // City Dropdown List
    public static function cityDropdownList($id = null){
        $model = City::find()->all();
        $city = ArrayHelper::map($model, 'id', 'city_name');
        return $city;
    }
    public static function cityListByCityId($id = null){
        if($id){
            $model = City::find()->where(['id' => $id])->one();
        }else{
            $model = City::find()->all();
        }
        return $model;
    }


    // Province Dropdown List
    public static function provinceDropDownList(){
        $model = Province::find()->all();
        $province = ArrayHelper::map($model, 'id', 'province_name');
        return $province;
    }
    public static function provinceListByProvinceId($id = null){
        if($id){
            $model = Province::find()->where(['id' => $id])->one();
            return $model;
        }
    }

    //tag category Listi
    public static function TagCategoryById($id = null){
        if($id){
            $model = ContactsTagsCategories::find()->where(['id' => $id])->one();
            return $model;
        }
    }


    // Country Dropdown List
    public static function countryDropDownList(){
        $model = Country::find()->all();
        $country = ArrayHelper::map($model, 'id', 'country_name');
        return $country;
    }
    public static function countryListByCountryId($id = null){
        if($id){
            $model = Country::find()->where(['id' => $id])->one();
            return $model;
        }

    }


    // Agent Type Dropdown List
    public static function agentTypeDropDownList(){
        $model = AgentType::find()->where(['status' => '1'])->all();
        $agent = ArrayHelper::map($model, 'id', 'agent_type_name');
        return $agent;
    }
    public static function agentTypeListByAgentId($id = null){
        if($id){
            $model = AgentType::find()->where(['id' => $id, 'status' => '1'])->one();
        }else{
            $model = AgentType::find()->where(['status' => '1'])->all();
        }
        return $model;
    }

    // Brands Dropdown List
    public static function brandsDropDownList(){
        $model = Brands::find()->where(['status' => '1'])->all();
        $agent = ArrayHelper::map($model, 'id', 'brand_name');
        return $agent;
    }
    public static function brandsListByBrandId($id = null){
        if(!empty($id)){
            $model = Brands::find()->where(['id' => $id, 'status' => '1'])->one();
            return $model;
        }
    }
    public static function brandsByList($id = null){
        $model = Brands::find()->where(['status' => '1'])->all();
        return $model;
    }


    // Interaction Type List
    public static function interactionTypeDropDownList(){
        $model = InteractionType::find()->all();
        $agent = ArrayHelper::map($model, 'id', 'interaction_name');
        return $agent;
    }
    public static function interactionTypeListById($id = null){
        if($id){
            $model = InteractionType::find()->where(['id' => $id])->one();
        }else{
            $model = InteractionType::find()->all();
        }
        return $model->interaction_name;
    }

    // Events List
    public static function eventsTypeDropDownList(){
        $model = Events::find()->all();
        $event = ArrayHelper::map($model, 'id', 'event_name');
        return $event;
    }
    public static function eventsListById($id = null){
        if($id){
            $model = Events::find()->where(['id' => $id])->one();
        }else{
            $model = Events::find()->all();
        }

        return $model;
    }

    // QuoteList
    public static function quoteList(){
        $model = Quote::find()->all();
        /*$quote = ArrayHelper::map($model, 'id', 'quote');*/
        return $model;
    }


    // Recognition Type
    const DEVELOPERS_RECOGNITION_TYPE_1 = '1'; // Award
    const DEVELOPERS_RECOGNITION_TYPE_2 = '2'; // Member
    const DEVELOPERS_RECOGNITION_TYPE_3 = '3'; //

    public static function developersRecognitionTypeDropDownList($id = null){
        $data = [
            self::DEVELOPERS_RECOGNITION_TYPE_1 => 'Award',
            self::DEVELOPERS_RECOGNITION_TYPE_2 => 'Member',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }


    // Primary Contact ID by Developer Contact
    public static function developerContactByPrimaryContactId($id){
        if($id){
            return DevelopersContacts::find()->where(['id' => $id])->one();
        }

    }

    // Developers Status Dropdown
    public static function developerStatusDropDownList($id = null){
        $data = [
            self::DEVELOPERS_ACTIVE_STATUS => 'Active',
            self::DEVELOPERS_INACTIVE_STATUS => 'In-Active',
            self::DEVELOPERS_ARCHIVE_STATUS => 'Archive',
            self::DEVELOPERS_DELETE_STATUS => 'Delete',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }


    // Developer Grade Drop Down List
    public static function developerGradeDropDownList($id = null){
        $data = [
            self::DEVELOPERS_SELECT_GRADE => 'Grade',
            self::DEVELOPERS_A_PLUS_GRADE => 'A+',
            self::DEVELOPERS_A_GRADE => 'A',
            //self::DEVELOPERS_B_PLUS_GRADE => 'B+',
            self::DEVELOPERS_B_GRADE => 'B',
            self::DEVELOPERS_C_GRADE => 'C',
            self::DEVELOPERS_D_GRADE => 'D',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }


    // Developers Province Dropdown
    public static function developerProvinceDropDownList($id = null){
        $data = [
            self::DEVELOPERS_PROVINCE_ONTARIO => 'Ontario',
            self::DEVELOPERS_PROVINCE_QUEBEC => 'Quebec',
            self::DEVELOPERS_PROVINCE_MANITOBA => 'Manitoba',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }

    // Developer List By ID
    public static function developerById($id){
            $developerlists = Developers::find()->where(['id' => $id])->one();
            return $developerlists;
    }


    // Development Slaes Status
    const DEVELOPMENT_SALES_STATUS_1 = 'comingsoon'; // comingsoon
    const DEVELOPMENT_SALES_STATUS_2 = 'vip'; // vip
    const DEVELOPMENT_SALES_STATUS_3 = 'active'; // active
    const DEVELOPMENT_SALES_STATUS_4 = 'archived'; // archived
    const DEVELOPMENT_SALES_STATUS_5 = 'canceled'; // canceled

    public static function developmentSalesStatusDropDownList($id = null){
        $data = [
            self::DEVELOPMENT_SALES_STATUS_1 => 'Registration',
            self::DEVELOPMENT_SALES_STATUS_2 => 'VIP',
            self::DEVELOPMENT_SALES_STATUS_3 => 'Available',
            self::DEVELOPMENT_SALES_STATUS_4 => 'Sold Out',
            self::DEVELOPMENT_SALES_STATUS_5 => 'Canceled',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }
    // Development List By Development ID
    public static function developmentListById($id = null){
        if(!empty($id)){
            $model = Developments::find()->where(['development_id' => $id])->one();
            return $model;
        }
    }

    // Development List By user ID
    public static function developmentListByUserId($id = null){
        if(!empty($id)){
            $model = Developments::find()->where(['assignment_id' => $id])->all();
            return $model;
        }
    }

    public static function developmentList(){
        $model = Developments::find()->all();
        return $model;
    }
    public static function developmentDropdownList(){
        $model = Developments::find()->orderBy(['development_name' => SORT_ASC])->all();
        $developmentList = ArrayHelper::map($model, 'development_id', 'development_name');
        return $developmentList;
    }

    // Development Campaigns List By Development ID
    public static function developmentCampaignsListById($id = null){
        if(!empty($id)){
            $model = DevelopmentsCampaigns::find()->where(['development_id' => $id])->orderBy(['id' => SORT_DESC])->all();
            return $model;
        }
    }
    public static function developmentCampaignsList(){
        $model = DevelopmentsCampaigns::find()->all();
        return $model;
    }
    public static function developmentCampaignsDropdownList(){
        $model = DevelopmentsCampaigns::find()->all();
        $developmentList = ArrayHelper::map($model, 'development_id', 'development_name');
        return $developmentList;
    }


    // Development ID By Inventory
    public static function developmentIdByInventory($id = null){
        if(!empty($id)){
            $model = Inventory::find()->where(['development_id' => $id])->orderBy(['id' => SORT_DESC])->all();
            return $model;
        }
    }

    // Newsletter Footer By Newsletter ID
    public static function newsletterFooterListById($id = null){
        if($id){
            $model = NewslettersFooter::find()->where(['newsletter_id' => $id])->all();
        }else{
            $model = NewslettersFooter::find()->all();
        }
        return $model;
    }

    // Newsletters Status Type....
    public static function newsletterStatusTypeDropDownList(){
        $model = NewsletterStatus::find()->all();
        $agent = ArrayHelper::map($model, 'id', 'status_name');
        return $agent;
    }
    public static function newsletterStatusTypeById($id = null){
        if($id){
            $model = NewsletterStatus::find()->where(['id' => $id])->one();
        }else{
            $model = NewsletterStatus::find()->all();
        }
        return $model;
    }

   /* const NEWSLETTER_STATUS_TYPE_1 = '1'; // Not Started
    const NEWSLETTER_STATUS_TYPE_2 = '2'; // In Progress
    const NEWSLETTER_STATUS_TYPE_3 = '3'; // Pending  Review
    const NEWSLETTER_STATUS_TYPE_4 = '4'; // Published
    const NEWSLETTER_STATUS_TYPE_5 = '5'; // Cancelled

    public static function newsletterStatusTypeDropDownList($id = null){
        $data = [
            self::NEWSLETTER_STATUS_TYPE_1 => 'Not Started',
            self::NEWSLETTER_STATUS_TYPE_2 => 'In Progress',
            self::NEWSLETTER_STATUS_TYPE_3 => 'Pending  Review',
            self::NEWSLETTER_STATUS_TYPE_4 => 'Published',
            self::NEWSLETTER_STATUS_TYPE_5 => 'Cancelled',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }*/


    // Newsletter In The News
    public static function newsletterInTheNews($id = null){
        if($id){
            $model = NewslettersNews::find()->where(['newsletter_id' => $id])->all();
        }else{
            $model = NewslettersNews::find()->all();
        }
        return $model;
    }

    // Newsletter Invest Today
    public static function newsletterInvestToday($id = null){
        if($id){
            $model = NewslettersInvestToday::find()->where(['newsletter_id' => $id])->all();
        }else{
            $model = NewslettersInvestToday::find()->all();
        }
        return $model;
    }

    // Newsletter Coming Soon
    public static function newsletterComingSoon($id = null){
        if($id){
            $model = NewslettersComingSoon::find()->where(['newsletter_id' => $id])->all();
        }else{
            $model = NewslettersComingSoon::find()->all();
        }
        return $model;
    }

    // Newsletter FEATURE RESALE
    public static function newsletterFeatureResale($id = null){
        if($id){
            $model = NewslettersFeatureResale::find()->where(['newsletter_id' => $id])->all();
        }else{
            $model = NewslettersFeatureResale::find()->all();
        }
        return $model;
    }


    // Dynamic Drop Down
    public static function dynamicDropDownList($key)
    {
        $parent = DynamicDropDown::find()
            ->where(['status' => self::STATUS_ACTIVE])
            ->andWhere(['key' => $key])
            ->orderBy(['sort_order' => SORT_ASC])
            ->one();

        if($parent === null){
            return [];
        }

        $listData = DynamicDropDown::find()
            ->where(['status' => self::STATUS_ACTIVE])
            ->andWhere(['parent_id' => $parent->id])
            ->orderBy(['sort_order' => SORT_ASC])
            ->all();

        return ArrayHelper::map($listData, 'key', 'value');
    }

    // Status Drop Down List
    public static function statusDropDownList($id = null)
    {
        $data = [
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_INACTIVE => 'Inactive',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }

    // Redirect Status Drop Down List
    const REDIRECT_STATUS_TRUE = 1;
    const REDIRECT_STATUS_FALSE = 0;
    public static function redirectStatusDropDownList($id = null)
    {
        $data = [
            self::REDIRECT_STATUS_FALSE => 'False',
            self::REDIRECT_STATUS_TRUE => 'True',

        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }

    const TIME_SLOTE_ONE = 'AM';
    const TIME_SLOTE_TWO ='PM';
    public static function timeSloteDropDownList($id = null)
    {
        $data = [
            self::TIME_SLOTE_ONE => 'AM',
            self::TIME_SLOTE_TWO => 'PM',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }



    public static function fontSizeDropDownList($id = null)
    {
        $data = [
            '' => 'Select font Size',
            '5px' => '5px',
            '10px' => '10px',
            'x-small' => 'X-Small',
            'small' => 'Small',
            'large' => 'Large',
            'x-large' => 'X-Large',
            'smaller' => 'Smaller',
            'larger' => 'Larger',
            'inherit' => 'Inherit',
        ];
        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }

    // Status Drop Down List
    public static function roleTypeDropDownList($id = null)
    {
        $data = [
            self::ROLE_TYPE_MAIN => 'Main Rule',
            self::ROLE_TYPE_ADDITIONAL => 'Additional Rule',
            self::ROLE_TYPE_SUBTRACTION => 'Subtraction Rule',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }

    public static function getPaymentStatusList($id = null)
    {
        $data = [
            self::STATUS_PAID => 'Paid',
            self::STATUS_APPROVED => 'Approved',
            self::STATUS_CANCEL => 'Canceled',
            self::STATUS_PENDING => 'Pending',
            self::STATUS_VERIFIED => 'Verified',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }

    //Development ID By Developer List
    public static function developmentIdByDevelopersList($development_id = null){
        $developerIdList = DevelopersDevelopmentsRelation::find()->where(['development_id' => $development_id])->all();
        $developerName = [];
        foreach ($developerIdList as $developerId){
            $developerslist = Developers::find()->where(['developer_id' => $developerId->developer_id])->one();
            $developerName[] = Html::a($developerslist['developer_name'], ['/developers/developers/view', 'id' => $developerslist['id']]);
        }
        return $developerName;
    }
    //Development ID By Developer List Without Designe
    public static function developerListByDevelopementId($development_id = null){
        $developerList = DevelopersDevelopmentsRelation::find()->where(['development_id' => $development_id])->all();
        $developerName = [];
        foreach ($developerList as $developer){
            $developerslist = Developers::find()->where(['developer_id' => $developer->developer_id])->one();
            $developerName[]=$developerslist['developer_name'];
        }
       return implode(', ', $developerName);
    }
    public static function systemInfoFormTypeByContactId($contact_id = null,$development_id = null){
        $contact_form_typelist =ContactsSysteminfo::find()->where(['contact_id' => $contact_id])->andWhere(['development_id' => $development_id])->groupBy('form_type')->all();
        $form_type_name = [];
        $incree = 1;
        $aaa    = '';
        foreach ($contact_form_typelist as $key=>$form_type){
            $form_type_name = $form_type->form_type;

            if ($form_type->form_type AND $incree == 1) {
                $aaa    .=   $form_type_name;
                $incree++;
            }else if($form_type->form_type){
                $aaa    .=   ', '.$form_type_name;
                $incree++;
            }
        }

        return $aaa;
    }

    //Developer ID By Development List
    public static function developerIdByDevelopmentList($developer_id = null){
        $developmentIdList = DevelopersDevelopmentsRelation::find()->where(['developer_id' => $developer_id])->all();
        $developmentList = [];
        foreach ($developmentIdList as $developmentId){
            $developmentList[] = Developments::find()->where(['development_id' => $developmentId])->one();
        }
        return $developmentList;
    }


    // Developer ID By Developer List
    public static function developerIdByList($id = null){
        if($id){
            $model = Developers::find()->where(['developer_id' => $id])->one();
            return $model;
        }
    }
    // Developer ID wise get Developer List
    public static function developerListById($id = null){
        if($id){
            $model = Developers::find()->where(['id' => $id])->one();
             return $model;
        }
    }
    // Contact by Contact List
    public static function developersDropdownList($id = null){    
        $developers = ArrayHelper::map(Developers::find()->orderBy(['developer_name'=>SORT_ASC])->all(), 'developer_id',function($model) {
            return $model['developer_name'];
        });
        return $developers;
    }
    
      public static function developersDropdownListById($id = null){    
        $developers = ArrayHelper::map(Developers::find()->orderBy(['developer_name'=>SORT_ASC])->all(), 'id',function($model) {
            return $model['developer_name'];
        });
        return $developers;
    }
    public static function contactsCategoryTagsDropdownlist($id = null){
        $contactsCategoryTags = ArrayHelper::map(ContactsTagsCategories::find()->orderBy(['tag_category_name'=>SORT_ASC])->all(), 'id',function($model) {
            return $model['tag_category_name'].' '.' (ID-'.$model->id.')';
        });
        return $contactsCategoryTags;

    }

    // Developer ID By Development List
    /*public static function developerIdByDevelopmentList($id = null){
        if($id){
            $model = Developments::find()->where(['developer_id' => $id])->one();
            return $model;
        }
    }*/

    // Developer by Developer Contact
    public static function developerByDeveloperContact($id = null){

            if($id){
                $model = DevelopersContacts::find()->where(['developer_id' => $id])->all();
            }else{
                $model = DevelopersContacts::find()->all();
            }
            return $model;

    }

    //  Developer Contact
    public static function developerContactById($id = null){

        if($id){
            $model = DevelopersContacts::find()->where(['id' => $id])->one();
        }else{
            $model = DevelopersContacts::find()->all();
        }
        return $model;

    }

    public static function getUserById($id = null){

        if($id){
            $model = User::find()->where(['id' => $id])->one();
        }else{
            $model = User::find()->all();
        }

        return $model;

    }

    public static function getUserList(){
            $model = User::find()->all();
            return $model;
    }

    // Developer by Developer Contact
    public static function developerByDeveloperContactInteraction($id = null){

        if($id){
            $model = DevelopersContactsInteractions::find()->where(['developer_id' => $id])->orderBy(['id' => SORT_DESC])->all();
        }else{
            $model = DevelopersContactsInteractions::find()->all();
        }
        return $model;

    }


    // Developer by Developer Recognition
    public static function developerByDeveloperRecognition($id = null){

        if($id){
            $model = DevelopersRecognitions::find()->where(['developer_id' => $id])->orderBy(['id' => SORT_DESC])->all();
        }else{
            $model = DevelopersRecognitions::find()->all();
        }
        return $model;

    }

    public static function contactListByDate($start_date,$end_date){

    }

    // Contact by Contact List
    public static function contactNameList($id = null){
        $contacts = ArrayHelper::map(Contacts::find()->limit(10)->all(), 'id',function($model) {
            return $model['first_name'].' '.$model['last_name'].' (ID-'.$model->id.')';
        });
        return $contacts;
    }

    //contactTagCategory id wise get category
    public static function contactTagCategory($id = null){
        $contactsCategoryTags = ArrayHelper::map(ContactsTagsCategories::find()->orderBy(['tag_category_name'=>SORT_ASC])->all(), 'id',function($model) {
            return $model['tag_category_name'].' '.' (ID-'.$model->id.')';
        });
        return $contactsCategoryTags;
    }

    public static function contactIdByName($id = null){
        if($id){
            $contacts = Contacts::findOne($id);
            return $contacts->first_name.' '.$contacts->last_name;
        }
    }

    public static function developerIdByName($id = null){
        if($id){
            $contacts_developer = Developers::findOne($id);
            if (!empty($contacts_developer)){
                return $contacts_developer->developer_name;
            }
        }
    }


    // Contact get all contact list by id
    public static function contactIdByContacts($id = null){

        if($id){
            $model = Contacts::find()->where(['id' => $id])->one();

        }else{
            $model = Contacts::find()->all();

        }
        return $model;

    }
    // Agent  get all agent list by id
    public static function agentListByContactsId($id = null){

        if($id){
            $model = Agent::find()->where(['id' => $id])->one();

        }else{
            $model = Agent::find()->all();

        }
        return $model;

    }
    // Contact List Get Previous 7 Days
    public static function contactListSevenDays($id = null,$start_date,$end_date){
        if($id){
            $model = Contacts::find()
                ->where(['id' => $id])
                ->where(['between', 'created_at', $start_date,$end_date ])
                ->orderBy(['id' => SORT_DESC])
                ->one();
        }else{
            $model = Contacts::find()->all();
        }
        return $model;
    }

    // Contact ID by Registration List
    public static function contactIdByRegistrationList($id = null){
        if($id){
            $model = ContactsRegistration::find()->where(['contact_id' => $id])->orderBy(['reg_datetime' => SORT_DESC])->all();
        }else{
            $model = ContactsRegistration::find()->all();
        }
        return $model;
    }

    // Contact ID by Registration List
    public static function contactIdByRegistrationListDevelopmentId($id = null){
        $baseAuthhorId = Yii::getAlias('@baseAuthhor');
        $username=[];
        if($id) {
            $ContactsRegistrationModel = ContactsRegistration::find()->where(['contact_id' => $id])->orderBy(['reg_datetime' => SORT_DESC])->groupBy('development_id')->all();
            foreach ($ContactsRegistrationModel as $ContactsRegistrationModelItem) {
                $development_id = $ContactsRegistrationModelItem->development_id;
                if (!empty($development_id)) {
                    $development_data   = Developments::find()->where(['development_id' => $development_id])->one();

                    if($development_data){
                        $assigned_id        = $development_data->assignment_id;
                        if ($assigned_id == 0) {
                            $assign_user = User::find()->where(['id' => $baseAuthhorId])->one();
                        } else {
                            $assign_user = User::find()->where(['id' => $assigned_id])->one();
                        }
                        $name = $assign_user->first_name . ' ' . $assign_user->last_name;
                        if(!in_array($name, $username))
                        $username[]= $name;
                    }
                }
            }
        }
        return implode(', ', $username);
    }



    // Development ID by Registration List
    public static function developmentIdByRegistrationList($id = null){
        if(!empty($id)){
            $model = ContactsRegistration::find()->where(['development_id' => $id])->orderBy(['reg_datetime' => SORT_DESC])->all();
        }
        else{
            $model = ContactsRegistration::find()->all();
        }
        return $model;
    }

    // Contact Lead Source Dropdown List
    public static function leadSourceDropDownList(){
        $model = LeadSource::find()->where(['status' => '1'])->all();
        $leadSource = ArrayHelper::map($model, 'id', 'lead_source_name');
        return $leadSource;
    }
    // Tag Name  Dropdown List
    public static function tagNameDropDownList(){
        $model = ContactsTagsNew::find()->all();
        $tag_name = ArrayHelper::map($model, 'id', 'tag_name');
        return $tag_name;
    }
    // Brand Name  Dropdown List
    public static function brandNameDropDownList(){
        $model = Brands::find()->all();
        $brand_name = ArrayHelper::map($model, 'id', 'brand_name');
        return $brand_name;
    }

    public static function leadSourceByBrandId($id = null){
        if(!empty($id)){
            $model = LeadSource::find()->where(['id' => $id, 'status' => '1'])->one();
            return $model['lead_source_name'];
        }
    }

    public static function leadSourceByList($id = null){
        $model = LeadSource::find()->where(['status' => '1'])->all();
        return $model['lead_source_name'];
    }

    // Contact Reservation Status Dropdown List
    public static function reservationStatusDropDownList(){
        $model = ReservationStatus::find()->where(['status' => '1'])->all();
        $revStatus = ArrayHelper::map($model, 'id', 'reservation_status_name');
        return $revStatus;
    }
    public static function reservationStatusById($id = null){
        if($id){
            $model = ReservationStatus::find()->where(['id' => $id, 'status' => '1'])->one();
        }else{
            $model = ReservationStatus::find()->where(['status' => '1'])->all();
        }

        return $model['reservation_status_name'];
    }
    public static function developmentIdByReservationList($id){
        if($id) {
            $model = ContactsReservations::find()->where(['development_id' => $id])->all();
            return $model;
        }
    }


    // Contact Registration  Type

    const CONTACT_REGISTRATION_TYPE_1 = '1'; // prospect
    const CONTACT_REGISTRATION_TYPE_2 = '2'; // agent
    const CONTACT_REGISTRATION_TYPE_3 = '3'; // agent

    public static function contactRegistrationTypeDropDownList($id = null){
        $data = [
            self::CONTACT_REGISTRATION_TYPE_1 => 'Prospect',
            self::CONTACT_REGISTRATION_TYPE_2 => 'Agent',
            self::CONTACT_REGISTRATION_TYPE_3 => 'Purchaser',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }

    // Contact Registration Status

    public static function contactStatusTypeDropDownList(){
        $model = RegistrationStatus::find()->where(['status' => '1'])->all();
        $regStatus = ArrayHelper::map($model, 'id', 'registration_status_name');
        return $regStatus;
    }
    public static function rsvpStatusTypeDropDownList(){
        $model = RsvpStatus::find()->where(['status' => '1'])->all();
        $regStatus = ArrayHelper::map($model, 'id', 'rsvp_status_name');
        return $regStatus;
    }

    //reservation Status
    public static function ReservationContactStatusTypeDropDownList(){
        $model = ReservationStatus::find()->where(['status' => '1'])->all();
        $regStatus = ArrayHelper::map($model, 'id', 'reservation_status_name');
        return $regStatus;
    }

    public static function contactStatusTypeById($id = null){
        if($id){
            $model = RegistrationStatus::find()->where(['id' => $id, 'status' => '1'])->one();
        }else{
            $model = RegistrationStatus::find()->where(['status' => '1'])->all();
        }

        return $model['registration_status_name'];
    }




    // Allocation Module
    public static function allocationListById($id = null){
        if($id){
            $model = Allocation::find()->where(['id' => $id, 'status' => '1'])->one();
            return $model;
        }
    }
    public static function allocationDropdownlist(){
        $model = Allocation::find()->where(['status' => '1'])->all();
        $allocation = ArrayHelper::map($model, 'id', 'allocation');
        return $allocation;
    }
    // Allocation Agent Yes/No Dropdown
    const ALLOCATION_AGENT_1 = '1'; // Yes
    const ALLOCATION_AGENT_2 = '2'; // No

    public static function allocationAgentStatus($id = null){
        $data = [
            self::ALLOCATION_AGENT_1 => 'Yes',
            self::ALLOCATION_AGENT_2 => 'No',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }

    // Allocation Alloc Status Dropdown
    const ALLOCATION_ALLOC_STATUS_1 = '1'; // New
    const ALLOCATION_ALLOC_STATUS_2 = '2'; // In Progress
    const ALLOCATION_ALLOC_STATUS_3 = '3'; // Cooling
    const ALLOCATION_ALLOC_STATUS_4 = '4'; // Signed
    const ALLOCATION_ALLOC_STATUS_5 = '5'; // Signing

    public static function allocationAllocStatus($id = null){
        $data = [
            self::ALLOCATION_ALLOC_STATUS_1 => 'New',
            self::ALLOCATION_ALLOC_STATUS_2 => 'In Progress',
            self::ALLOCATION_ALLOC_STATUS_3 => 'Cooling',
            self::ALLOCATION_ALLOC_STATUS_4 => 'Signed',
            self::ALLOCATION_ALLOC_STATUS_5 => 'Signing',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }

    // Allocation Accepted
    const ALLOCATION_ACCEPTED_1 = '1'; // Yes
    const ALLOCATION_ACCEPTED_2 = '2'; // No

    public static function allocationAcceptedStatus($id = null){
        $data = [
            self::ALLOCATION_ACCEPTED_1 => 'Yes',
            self::ALLOCATION_ACCEPTED_2 => 'No',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }




    // Boolean  Drop Down List
    public static function booleanDropDownList($id=null)
    {
        $data = [
            self::YES => 'Yes',
            self::NO => 'No',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }

    public static function getUserGroupList($id = null)
    {
        $data = [
            self::USER_GROUP_ADMIN => 'Admin',
            //self::USER_GROUP_REGISTERED => 'Registered',
            self::USER_GROUP_SALES => 'Sales',
            self::USER_GROUP_MARKETING => 'Marketing',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }

    public static function languageCategoryDropDownList($id=NULL)
    {
        $data = [
            CommonHelper::LANGUAGE_CATEGORY_APP => 'app',
            CommonHelper::LANGUAGE_CATEGORY_YII => 'yii',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }

    public static function getLoggedInUserId()
    {
        return isset(Yii::$app->user->identity->id) ? Yii::$app->user->identity->id : null;
    }

    public static function getLoggedInUserName()
    {
        return isset(Yii::$app->user->identity->username) ? Yii::$app->user->identity->username : null;
    }
    //getLoggedInUserNameByUserId
    public static function getLoggedInUserNameByUserId($id=null)
    {
        if(!empty($id)){
            $model = User::find()->where(['id' => $id])->one();
            return $model['first_name'].' '.$model['last_name'];
        }
    }
    //getRegistrationStatusById
    public static function getRegistrationStatusById($id=null)
    {
        if(!empty($id)){
            $model = RegistrationStatus::find()->where(['id' => $id])->one();
            return $model['registration_status_name'];
        }
    }

    public static function getLoggedInUserGroup()
    {
        return isset(Yii::$app->user->identity->user_group) ? Yii::$app->user->identity->user_group : null;
    }

    public static function getLoggedInUserOrgId()
    {
        return isset(Yii::$app->user->identity->user_org_id) ? Yii::$app->user->identity->user_org_id : null;
    }

    public static function getLoggedInUserRoleId()
    {
        return  isset(Yii::$app->user->identity->role_id) ? Yii::$app->user->identity->role_id : null;
    }

    public static function getLoggedInUserRoleName()
    {
        return  isset(Yii::$app->user->identity->role->name) ? Yii::$app->user->identity->role->name : null;
    }

    public static function getLoggedInUserFullName()
    {
         $firstname=isset(Yii::$app->user->identity->first_name) ? Yii::$app->user->identity->first_name : null;
         $lastname=isset(Yii::$app->user->identity->last_name) ? Yii::$app->user->identity->last_name : null;
         $fullname=$firstname.' '.$lastname;
         return $fullname;
    }


    public static function isChangedAttribute($model, $compareAttribute){
        $isAttributeChange = false;

        if($compareAttribute){
            foreach ($compareAttribute as $name) {
                if (isset($model->attributes[$name], $model->oldAttributes[$name])) {
                    //Date and DateTime
                    if (in_array($model->getTableSchema()->columns[$name]->type, ['date', 'datetime'])) {
                        if (CommonHelper::changeDateFormat($model->attributes[$name], 'Y-m-d') != CommonHelper::changeDateFormat($model->oldAttributes[$name], 'Y-m-d')) {
                            $isAttributeChange = true;
                        }
                    }
                    else {
                        if ($model->attributes[$name] != $model->oldAttributes[$name]) {
                            $isAttributeChange = true;
                        }
                    }
                }
            }
        }

        return $isAttributeChange;
    }

    public static function getCurrentDate()
    {
        $current_date = date('d-m-Y');
        return $current_date;
    }

    public static function dateTimeDifference($date)
    {
        $currentDate=strtotime(date('d M Y G.i.s'));
        $reviewDate = strtotime($date);
        $all = round(($currentDate - $reviewDate) / 60);
        $d = floor ($all / 1440);
        $h = floor (($all - $d * 1440) / 60);
        $m = $all - ($d  *1440) - ($h* 60);//Since you need just hours and mins
        return array(
            'd'=>$d,
            'h'=>$h,
            'm'=>$m
        );
    }

    public function dateDifference($startDate, $endDate)
    {
        $dateOne = date_create($startDate);
        $dateTwo = date_create($endDate);
        return $dateDifference = date_diff($dateTwo, $dateOne);
    }

    public static function getCurrentMonth()
    {
        $current_month = date('m');
        return (int)$current_month;
    }

    public static function getNextMonth($month = null)
    {
        if($month){
            $date = date('Y') . '-' . $month . '-' . date('d');
            $next_month = date('m', strtotime('+1 month', strtotime($date)));
        }
        else{
            $next_month = date('m', strtotime('+1 month'));
        }

        return (int)$next_month;
    }

    /// Month List
    public static function getMonthList($id=null)
    {
        $data = [
            1 =>  'January',
            2 =>  'February',
            3 =>  'March',
            4 =>  'April',
            5 =>  'May',
            6 =>  'June ',
            7 =>  'July',
            8 =>  'August',
            9 =>  'September',
            10 => 'October',
            11 => 'November',
            12 => 'December',
        ];

        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }

    public static function getMonthListFromDate($startDate, $endDate){
        $months = [];

        $begin = new \DateTime($startDate);
        $end = new \DateTime($endDate);

        while ($begin <= $end) {
            $months[] = (int) $begin->format('m');
            $begin->modify('first day of next month');
        }

        return $months;
    }

    public static function getCurrentFinancialYear()
    {
        if (date('m') > 6) {
            $year = date('Y')."-".(date('Y') +1);
        }
        else {
            $year = (date('Y')-1)."-".date('Y');
        }

        return $year;
    }

    public static function getPreviousFinancialYear($financialYear) {
        $array = explode('-', $financialYear);
        $firstYear = $array[0]-1;
        $lastYear = $array[1]-1;
        return $previousFinancialYear = $firstYear.'-'.$lastYear;
    }

    public static function getYearFromFinancialYearAndMonth($financialYear, $month)
    {
        $yearArray = explode('-', $financialYear);
        $startYear = $yearArray[0];
        $endYear = $yearArray[1];

        if ($month > 6) {
            $year = $startYear;
        }
        else {
            $year = $endYear;
        }

        return $year;
    }

    public static function getNextFinancialYear($financialYear) {
        $array = explode('-', $financialYear);
        $firstYear = $array[0]+1;
        $lastYear = $array[1]+1;
        return $previousFinancialYear = $firstYear.'-'.$lastYear;
    }

    public static function getFinancialYearStartDate($financialYear)
    {
        $yearArray = explode('-',$financialYear);
        $startYear = $yearArray[0];

        // Start Date
        $startDate = $startYear . '-07-01';

        return $startDate;
    }

    public static function getFinancialYearEndDate($financialYear)
    {
        $yearArray = explode('-',$financialYear);
        $endYear = $yearArray[1];

        // Start Date
        $endDate = $endYear . '-06-30';

        return $endDate;
    }

    public static function percentCalculation($smallAmount, $bigAmount)
    {
        if(empty($smallAmount) || empty($bigAmount)){
            return 0;
        }

        if($bigAmount > $smallAmount){
            $diffPercent = ($smallAmount / $bigAmount) * 100;
        }
        else{
            $diffPercent = ($bigAmount / $smallAmount) * 100;
        }

        return number_format($diffPercent, 2);
    }

    public static function changeDateFormat($date, $format = 'Y-m-d',$f_staus='')
    {
        if (empty($date) || $date === null) {
            return null;
        }
        $date = date($format, strtotime($date));
        return $date;
    }

    // number in words
    public static function amountToWords($number)
    {
        $hyphen      = ' ';
        $conjunction = ' '; // and
        $separator   = ' '; // ,
        $negative    = 'negative ';
        $decimal     = ' and paisa '; // point
        $dictionary  = array(
            0                   => 'zero',
            1                   => 'one',
            2                   => 'two',
            3                   => 'three',
            4                   => 'four',
            5                   => 'five',
            6                   => 'six',
            7                   => 'seven',
            8                   => 'eight',
            9                   => 'nine',
            10                  => 'ten',
            11                  => 'eleven',
            12                  => 'twelve',
            13                  => 'thirteen',
            14                  => 'fourteen',
            15                  => 'fifteen',
            16                  => 'sixteen',
            17                  => 'seventeen',
            18                  => 'eighteen',
            19                  => 'nineteen',
            20                  => 'twenty',
            30                  => 'thirty',
            40                  => 'fourty',
            50                  => 'fifty',
            60                  => 'sixty',
            70                  => 'seventy',
            80                  => 'eighty',
            90                  => 'ninety',
            100                 => 'hundred',
            1000                => 'thousand',
            100000              => 'lac',
            10000000            => 'crore',
            1000000000000       => 'trillion',
            1000000000000000    => 'quadrillion',
            1000000000000000000 => 'quintillion'
        );
        if (!is_numeric($number)) {
            return false;
        }
        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                'amountToWords only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
                E_USER_WARNING
            );
            return false;
        }
        if ($number < 0) {
            return $negative.self::amountToWords(abs($number));
        }
        $string = $fraction = null;
        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }
        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens   = ((int) ($number / 10)) * 10;
                $units  = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds  = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction.self::amountToWords($remainder);
                }
                break;

            case $number < 100000:
                $thousands   = ((int) ($number / 1000));
                $remainder = $number % 1000;

                $thousands = self::amountToWords($thousands);

                $string .= $thousands . ' ' . $dictionary[1000];
                if ($remainder) {
                    $string .= $separator . self::amountToWords($remainder);
                }
                break;

            case $number < 10000000:
                $lakhs   = ((int) ($number / 100000));
                $remainder = $number % 100000;

                $lakhs = self::amountToWords($lakhs);

                $string = $lakhs . ' ' . $dictionary[100000];
                if ($remainder) {
                    $string .= $separator . self::amountToWords($remainder);
                }
                break;

            case $number < 1000000000:
                $crores   = ((int) ($number / 10000000));
                $remainder = $number % 10000000;

                $crores = self::amountToWords($crores);

                $string = $crores . ' ' . $dictionary[10000000];
                if ($remainder) {
                    $string .= $separator . self::amountToWords($remainder);
                }
                break;

            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = self::amountToWords($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= self::amountToWords($remainder);
                }
                break;
        }
        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }
        return $string;
    }

    /*
     * Array values into ranges
     * Sample Input: Array(1,2,3,4,5,6,10,11,12,13,20,24)
     * Output: Array([0] => 1-6, [1] => 10-13, [2] => 20, [3] => 24)
     * */
    function getRangeString($aNumbers) {
        $aNumbers = array_unique( $aNumbers );
        sort( $aNumbers );
        $aGroups = array();
        for( $i = 0; $i < count( $aNumbers ); $i++ ) {
            if( $i > 0 && ( $aNumbers[$i-1] == $aNumbers[$i] - 1 ))
                array_push( $aGroups[count($aGroups)-1], $aNumbers[$i] );
            else
                array_push( $aGroups, array( $aNumbers[$i] ));
        }
        $aRanges = array();
        foreach( $aGroups as $aGroup ) {
            if( count( $aGroup ) == 1 )
                $aRanges[] = $aGroup[0];
            else
                $aRanges[] = $aGroup[0] . '-' . $aGroup[count($aGroup)-1];
        }
        return implode( ',', $aRanges );
    }

    function addCurrencySymbol($amount, $currency) {
        if($currency == 'USD'){
            return '$'. $amount;
        }
        else if($currency == 'USD'){
            return '€'. $amount;
        }
        else if($currency == 'GBP'){
            return '£'. $amount;
        }
        else if($currency == 'BDT'){
            return 'Tk'. $amount;
        }

        return $amount;
    }

    // Report As On Date
    public static function reportAsOnDateTime($endDate = null, $startDate = null)
    {

        $startDate = !empty($startDate) ? CommonHelper::changeDateFormat($startDate, 'd M Y') : null;
        $endDate = !empty($endDate) ? CommonHelper::changeDateFormat($endDate, 'd M Y') : date('d-M-Y');

        $asOnDate = $endDate;

        if(!empty($startDate) && !empty($endDate)){
            $asOnDate = $startDate . ' TO ' . $endDate;
        }

        $asOnDate = '(As On ' . $asOnDate . ' AT ' . date('h:i') . ')';

        return $asOnDate;
    }

    public static function getNextIncrementNumber($type)
    {
        $lastIncrementNumber = AutoIncrementNumber::getLastIncrementNumber($type);

        $increment_last_id = $lastIncrementNumber + 1;
        AutoIncrementNumber::updateIncrementLastIdField($type, $increment_last_id);

        return $lastIncrementNumber;
    }

    function isValidateEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    function isValidateMobile($mobile)
    {
        return preg_match('/^(?:\+?88)?01[15-9]\d{8}$/', $mobile);
    }

    function generatePIN($digits = 4){
        $i = 0; //counter
        $pin = ""; //our default pin is blank.
        while($i < $digits){
            //generate a random number between 0 and 9.
            $pin .= mt_rand(0, 9);
            $i++;
        }
        return $pin;
    }


    /* Assignments Common Feature */

    const ASSIGNMENT_STATUS_1 = '1'; // NEW
    const ASSIGNMENT_STATUS_2 = '2'; // SOLD
    const ASSIGNMENT_STATUS_3 = '3'; // REMOVE
    const ASSIGNMENT_STATUS_4 = '4'; // EXPIRED
    const ASSIGNMENT_STATUS_5 = '5'; // DRAFT

    const ASSIGNMENT_SERVICE_1 = '1'; // MLS
    const ASSIGNMENT_SERVICE_2 = '2'; // Exclusive

    const BUILDING_ASSETS_1 = 'brochures'; // brochures
    const BUILDING_ASSETS_2 = 'renderings'; // renderings
    const BUILDING_ASSETS_3 = 'investor_incentives'; // investor_incentives
    const BUILDING_ASSETS_4 = 'videos'; // videos
    const BUILDING_ASSETS_5 = 'features_finishes'; // features_finishes
    const BUILDING_ASSETS_6 = 'images'; // images

    const ASSIGNMENTS_ASSETS_1 = 'floor_plans'; // floor_plans
    const ASSIGNMENTS_ASSETS_2 = 'images'; // floor_plans


    // Assignments Status
    public static function assignmentStatusDropDownList($id = null){
        $data = [
            self::ASSIGNMENT_STATUS_1 => 'New',
            self::ASSIGNMENT_STATUS_2 => 'Sold',
            self::ASSIGNMENT_STATUS_3 => 'Remove',
            self::ASSIGNMENT_STATUS_4 => 'Expired',
            self::ASSIGNMENT_STATUS_5 => 'Draft',
        ];
        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }

    public static function parkingTotalDropDownList($id = null){
        $data = [];
        for ($i=0;$i<=5;$i++){
            $data[$i]=$i;
        }
        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }

    // MLS/Exclusive Status
    public static function assingmentServiceDropDownList($id = null){
        $data = [
            self::ASSIGNMENT_SERVICE_1 => 'MLS',
            self::ASSIGNMENT_SERVICE_2 => 'Exclusive',
        ];
        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }

    //Building Assets Data
    public static function buildingAssetsDropDownList($id = null){
        $data = [
            self::BUILDING_ASSETS_1 => 'Brochures',
            self::BUILDING_ASSETS_2 => 'Renderings',
            self::BUILDING_ASSETS_3 => 'Investor Incentives',
            self::BUILDING_ASSETS_4 => 'Videos',
            self::BUILDING_ASSETS_5 => 'Features and Finishes',
            self::BUILDING_ASSETS_6 => 'Images',
        ];
        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }

    public static function assignmentAssetsDropDownList($id = null){
        $data = [
            self::ASSIGNMENTS_ASSETS_1 => 'Floor Plane',
            self::ASSIGNMENTS_ASSETS_2 => 'Image',
        ];
        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }

    public static function assignmentsAssetsDropDownList($id = null){
        $data = [
            self::ASSIGNMENTS_ASSETS_1 => 'Floor Plans',
        ];
        if (func_num_args() == 0) return $data;
        else if (array_key_exists($id, $data)) return $data[$id];
        else return '';
    }


    public static function getActiveSuiteType()
    {
        $model = SuiteType::find()
            ->where('status =:status', [':status' => 1])
            ->orderBy(  ['id' => SORT_ASC])
            ->all();
        return  ArrayHelper::map( $model,'id',  function($model) {
            return $model['suite_type_name'];
        });
    }
    //Occupancy Dropdownlist
    public static function getOccupancyDropdownlist()
    {
        $model = Assignments::find()->orderBy(  ['created_at' => SORT_DESC])->all();
        return  ArrayHelper::map( $model,'occupancy',  function($model) {
            $date=$model['occupancy'];
            $date_occupancy =date('F',strtotime($date)).'  '.date('Y',strtotime($date));
            $occupancyDate=$date_occupancy;
            return $occupancyDate;
        });
    }
    //Listing Price Dropdownlist
    public static function getListingPriceDropdownlist()
    {
        $model = Assignments::find()->orderBy(  ['created_at' => SORT_DESC])->all();
        return  ArrayHelper::map( $model,'listing_price',  function($model) {
            $listingPrice=$model['listing_price'];
            return $listingPrice;
        });
    }

    //Suite Type Name Get By Assignmensts Id
    public static function getSuiteTypeByAssignmentsId($assignments_id)
    {
        $AssignmentsUnitsModel = AssignmentsUnits::find()->where(['assignment_id'=>$assignments_id])->one();
        if (!empty($AssignmentsUnitsModel)){
            if (!empty($AssignmentsUnitsModel->beds_id)) {
                $bedsId = $AssignmentsUnitsModel->beds_id;
                if (!empty($bedsId)) {
                    $SuiteTypeModel = SuiteType::find()->where(['id' => $bedsId])->one();
                }
                return $SuiteTypeModel;
            }
        }
      //$SuiteTypeModel;*/
    }

    public static function getAssignmentsById($id = null){

        if($id){
            $model = Assignments::find()->where(['id' => $id])->one();
        }else{
            $model = Assignments::find()->all();
        }
        return $model;

    }

    public static function getAssignmentsByDevelopmentId($id = null){

        if($id){
            $model = Assignments::find()->where(['development_id' => $id])->one();
        }else{
            $model = Assignments::find()->all();
        }
        return $model;

    }

    //get assignment by assignment assets name
    public static function getAssignmentsAssetsByName($viewId=null,$assets_name=null){
        if($viewId){
            $model =AssignmentsAssets::find()->where(['assignment_id'=>$viewId])->andWhere(['assignment_assets'=>$assets_name])->one();
        }else{
            $model = AssignmentsAssets::find()->all();
        }
        return $model;
    }

    //get assignment by assignment assets id
    public static function getAssignmentsAssetsByAssignmentID($viewId=null){
        if($viewId){
            $model = AssignmentsAssets::find()->where(['assignment_id'=>$viewId])->all();
        }else{
            $model = array(); //AssignmentsAssets::find()->all();
        }
        return $model;
    }

    //get building assets by building assets name
    public static function getBuildingAssetsByName($viewId=null,$assets_name=null){
        if($viewId){
            $model =AssignmentsBuildingAssets::find()->where(['assignment_id'=>$viewId])->andWhere(['building_assets'=>$assets_name])->one();
        }else{
            $model = AssignmentsBuildingAssets::find()->all();
        }
        return $model;
    }

    //get building assets by building assets id
    public static function getBuildingAssetsByAssignmentID($viewId=null,$assets_name=null){
        if($viewId){
            $model =AssignmentsBuildingAssets::find()->where(['assignment_id'=>$viewId])->all();
        }else{
            $model = array(); //AssignmentsBuildingAssets::find()->all();
        }
        return $model;
    }

    //get All user Email Address
    public static function getUserEmailAddress(){
        $model=User::find()->all();
        return $model;
    }
    //get All Media List
    public static function getMediaList(){
        $model=LeadSource::find()->orderBy(['created_by'=>SORT_DESC])->all();
        return $model;
    }

    public static function getBedName($bed_id=null){
        if (!empty($bed_id)){
            $model = SuiteType::find()->where(['id' => $bed_id])->one();
        }
        else{
            $model=SuiteType::find()->all();
        }
        return $model;
    }

    public static function getAssignmentUnitsByAssignmentsId($assignment_id){
        if (!empty($assignment_id)){
            $model = AssignmentsUnits::find()->where(['id' => $assignment_id])->one();
        }
        else{
            $model=AssignmentsUnits::find()->all();
        }
        return $model;
    }

}

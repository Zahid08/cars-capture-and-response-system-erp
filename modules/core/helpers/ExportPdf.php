<?php

namespace unlock\modules\core\helpers;

use Yii;
use yii\helpers\Html;
use yii\base\Widget;
use unlock\modules\core\helpers\FileHelper;

class ExportPdf
{
    public static $view;

    public static $fileName;

    public static $mode = 'c';

    public static $format = 'A4';

    public static $orientation = 'P';

    public static function exportGridPdf($data)
    {
        $view = static::$view;
        $fileName = static::$fileName;
        $mode = static::$mode;
        $format = static::$format;
        $orientation = static::$orientation;

        // Pdf library
        $mpdf = new \mPDF(
            $mode,       // mode - default ''
            $format,     // format - A4, for example, default ''

            '10',        // font size - default 0
            'arial',     // default font family

            '5',        // margin left
            '5',        // margin right

            '5',        // margin top
            '10',        // margin bottom

            '9',        // margin header
            '9',        // margin footer

            $orientation // $orientation L - landscape, P - portrait
        );

        $mpdf->showImageErrors = false;

        $mpdf->SetDisplayMode('fullpage');

        // Css
        $stylesheet = file_get_contents(Yii::getAlias('@webroot') . "/unlock_admin/css/bootstrap.min.css");
        $mpdf->WriteHTML($stylesheet, 1);
        $stylesheet2 = file_get_contents(Yii::getAlias('@webroot') . "/unlock_admin/css/pdf.css");
        $mpdf->WriteHTML($stylesheet2, 1);

        //Html
        $html = Yii::$app->controller->renderPartial($view, $data, true, false);
        $mpdf->WriteHTML($html);

        $filePath = Yii::getAlias('@basePath') . '/media/tmp/pdf/' . $fileName;
        $fileUrl = Yii::getAlias('@baseUrl') . '/media/tmp/pdf/' . $fileName;

        // Save File In Server
        //$mpdf->Output($filePath, 'F');
        $mpdf->Output($filePath, 'I');

        // Browser Output
        //header('Location: ' . $fileUrl);
        exit();
    }

    public static function exportPdf($data)
    {
        $view = static::$view;
        $fileName = static::$fileName;
        $mode = static::$mode;
        $format = static::$format;
        $orientation = static::$orientation;

        // Pdf library
        $mpdf = new \Mpdf\Mpdf();

        $mpdf->showImageErrors = false;

        $mpdf->SetDisplayMode('fullpage');

        // Css
        //$stylesheet = file_get_contents(Yii::getAlias('@webroot') . "/unlock_admin/css/bootstrap.min.css");
        //$mpdf->WriteHTML($stylesheet, 1);
        //$stylesheet2 = file_get_contents(Yii::getAlias('@webroot') . "/unlock_admin/css/pdf.css");
        //$mpdf->WriteHTML($stylesheet2, 1);

        //Html
        $html = Yii::$app->controller->renderPartial($view, $data, true, false);
        $mpdf->WriteHTML($html);

        $filePath = Yii::getAlias('@basePath') . '/media/tmp/pdf/' . $fileName;
        $fileUrl = Yii::getAlias('@baseUrl') . '/media/tmp/pdf/' . $fileName;

        // Save File In Server
        //$mpdf->Output($filePath, 'F');
        $mpdf->Output($filePath, 'I');

        // Browser Output
        //header('Location: ' . $fileUrl);
        exit();
    }
}

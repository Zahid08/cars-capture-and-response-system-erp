<?php

namespace unlock\modules\core\helpers;

use Yii;

class FileHelper extends \yii\helpers\FileHelper
{

    public static function getUniqueFilename($fileName, $filePath = null) {

        if(empty($filePath)){
            $filePath = Yii::getAlias('@basePath');
        }

        $name = pathinfo($fileName, PATHINFO_FILENAME);
        $extension = pathinfo($fileName, PATHINFO_EXTENSION);

        $newFileName = $fileName;
        $newFilePath = $filePath . '/' . $fileName;

        $counter = 1;
        while (file_exists($newFilePath)) {
            $newFileName = $name . '_' . $counter . $extension;
            $newFilePath = $filePath . '/' . $newFileName;
            $counter++;
        }

        return $newFileName;
    }

    public static function getUniqueFilenameWithPath($fileName, $dirName = null, $basePath = null) {

        if(empty($basePath)){
            $basePath = Yii::getAlias('@basePath') . '/media';
        }

        $filePath = $basePath;
        if(isset($dirName)){
            $filePath = $filePath . '/' . $dirName;
        }

        $name = pathinfo($fileName, PATHINFO_FILENAME);
        $extension = pathinfo($fileName, PATHINFO_EXTENSION);

        $newFilePath = $filePath . '/' . $fileName;

        $counter = 1;
        while (file_exists($newFilePath)) {
            $newFileName = $name . '_' . $counter . '.' . $extension;
            $newFilePath = $filePath . '/' . $newFileName;
            $counter++;
        }

        return $newFilePath;
    }

    public static function getFilename($filePath) {
        return $baseName = pathinfo($filePath, PATHINFO_BASENAME);
    }

    public static function getFileExt($fileName) {
        $extension = pathinfo($fileName, PATHINFO_EXTENSION);
        return $extension;
    }

    public static function getDirectoryExt($directoryPath) {
        $extension = pathinfo($directoryPath, PATHINFO_EXTENSION);
        return $extension;
    }

    function removeFile($fileName, $dirName = null, $basePath = null) {

        if(empty($basePath)){
            $basePath = Yii::getAlias('@basePath') . '/media';
        }


        $filePath = $basePath;
        if(isset($dirName)){
            $filePath = $filePath . '/' . $dirName;
        }

        $filePath = $filePath . '/' . $fileName;

        if (empty($filePath) || !file_exists($filePath)) {
            return false;
        }

        if(@unlink($filePath)) {
            return true;
        }

        return false;
    }

    function isImage($fileName)
    {
        static $imageTypes = 'xcf|odg|gif|jpg|png|bmp';
        return preg_match("/$imageTypes/i",$fileName);
    }


    function Thumbnail($sourcePath,$thumbPath,$thumbWidth,$thumbHeight) {

        //make sure the GD library is installed
        if(!function_exists("gd_info")) {
            echo 'You do not have the GD Library installed.  This class requires the GD library to function properly.' . "\n";
            echo 'visit http://us2.php.net/manual/en/ref.image.php for more information';
            exit;
        }
        //initialize variables
        $errmsg               = '';
        $error                = false;
        $fileName             = $sourcePath;

        //check to see if file exists
        if(!file_exists($fileName)) {
            $errmsg = 'File not found';
            $error = true;
        }
        //check to see if file is readable
        elseif(!is_readable($fileName)) {
            $errmsg = 'File is not readable';
            $error = true;
        }

        //if there are no errors, determine the file format
        if($error == false) {
            //check if gif
            if(stristr(strtolower($fileName),'.gif')) $format = 'GIF';
            //check if jpg
            elseif(stristr(strtolower($fileName),'.jpg') || stristr(strtolower($fileName),'.jpeg')) $format = 'JPG';
            //check if png
            elseif(stristr(strtolower($fileName),'.png')) $format = 'PNG';
            //unknown file format
            else {
                $errmsg = 'Unknown file format';
                $error = true;
            }
        }

        //initialize resources if no errors
        if($error == false) {
            switch($format) {
                case 'GIF':
                    $oldImage = ImageCreateFromGif($fileName);

                    $sourceImage = imagecreatefromgif($sourcePath);
                    $sourceWidth = imagesx($sourceImage);
                    $sourceHeight = imagesy($sourceImage);
                    $targetImage = imagecreate($thumbWidth,$thumbWidth);
                    imagecopyresized($targetImage,$sourceImage,0,0,0,0,$thumbWidth,$thumbHeight,imagesx($sourceImage),imagesy($sourceImage));
                    imagegif($targetImage,$thumbPath);
                    break;
                case 'JPG':

                    $sourceImage = imagecreatefromjpeg($sourcePath);
                    $sourceWidth = imagesx($sourceImage);
                    $sourceHeight = imagesy($sourceImage);
                    $targetImage = imagecreate($thumbWidth,$thumbWidth);
                    imagecopyresized($targetImage,$sourceImage,0,0,0,0,$thumbWidth,$thumbHeight,imagesx($sourceImage),imagesy($sourceImage));
                    imagejpeg($targetImage,$thumbPath, 100);

                    break;
                case 'PNG':
                    $oldImage = ImageCreateFromPng($fileName);

                    $sourceImage = imagecreatefrompng($sourcePath);
                    $sourceWidth = imagesx($sourceImage);
                    $sourceHeight = imagesy($sourceImage);
                    $targetImage = imagecreate($thumbWidth,$thumbWidth);
                    imagecopyresized($targetImage,$sourceImage,0,0,0,0,$thumbWidth,$thumbHeight,imagesx($sourceImage),imagesy($sourceImage));
                    imagepng($targetImage,$thumbPath);
                    break;
            }

        }

        if($error == true) {
            return false;
        }
        return true;
    }

}

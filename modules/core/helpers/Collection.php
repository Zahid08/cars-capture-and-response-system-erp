<?php

namespace unlock\modules\core\helpers;

use Yii;
use yii\helpers\Html;

class Collection
{
    public static function setData($dataProvider, $params = []){
        $collectionPath = static::getCollectionPath();

        $data['dataProvider'] = $dataProvider;
        $data['params'] = $params;

        $session = Yii::$app->session;

        if (isset($session['collection'][$collectionPath])){
            unset($_SESSION['collection'][$collectionPath]);
        }

        $_SESSION['collection'][$collectionPath] = $data;
    }

    public static function getData($controllerAction){
        $collectionPath = static::getCollectionPath($controllerAction);
        $session = Yii::$app->session;

        return $session['collection'][$collectionPath];
    }

    public static function getCollectionPath($controllerAction = null){
        $moduleName = isset(Yii::$app->controller->module->id) ? str_replace('-', '', Yii::$app->controller->module->id) : '';

        $controllerName = isset(Yii::$app->controller->id) ? str_replace('-', '', Yii::$app->controller->id) : '';

        if($controllerAction){
            $actionName = str_replace('-', '', $controllerAction);
        }
        else{
            $actionName = isset(Yii::$app->controller->action->id) ? str_replace('-', '', Yii::$app->controller->action->id) : '';
        }

        $actionPath = $moduleName . '_' . $controllerName . '_' . $actionName;

        return $actionPath;
    }


}

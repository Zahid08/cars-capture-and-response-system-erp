<?php

namespace unlock\modules\core\helpers;

use Yii;
use yii\helpers\Html;
use yii\base\Widget;
use unlock\modules\core\helpers\FileHelper;

class SmsApi
{
    public static $apiUrl = 'http://bulksms.teletalk.com.bd/link_sms_send.php';

    public static $username = 'UnlockLive';

    public static $password = 'UnlockLive';

    public static $charset = 'GSM'; //GSM, UTF-8

    public static $op = 'SMS';

    public static function sendSms($mobile, $smsText){
        $apiUrl = static::$apiUrl;
        $username = static::$username;
        $password = static::$password;
        $charset = static::$charset;
        $op = static::$op;

        if(substr($mobile, 0, 3) == '+88'){
            $mobile = str_replace('+', '', $mobile);
        }
        else if(substr($mobile, 0, 2) != '88'){
            $mobile = '88'.$mobile;
        }

        $fields = [
            "op" => $op,
            "user" => $username,
            "pass" => $password,
            "charset" => $charset,
            "mobile" => $mobile,
            "sms" => $smsText,
        ];

        $url = $apiUrl . '?'. http_build_query($fields);

        $options = array(
            CURLOPT_RETURNTRANSFER => true,   // return web page
            CURLOPT_HEADER         => false,  // don't return headers
            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
            CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
            CURLOPT_ENCODING       => "",     // handle compressed
            CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
            CURLOPT_TIMEOUT        => 120,    // time-out on response
        );

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        curl_close($ch);

        $result = trim(strip_tags($response));
        $result = "DELIVERY_STATUS=" . $result;
        $resultText = str_replace(',', '&', $result);

        parse_str($resultText, $output);

        return  $output;
    }
}

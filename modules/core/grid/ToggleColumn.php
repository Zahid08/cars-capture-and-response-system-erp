<?php
/*
 * https://github.com/yii2mod/yii2-toggle-column
 * */

namespace unlock\modules\core\grid;

use Yii;
use yii\grid\DataColumn;
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;

/**
 * Class ToggleColumn
 *
 * @package yii2mod\toggle
 */
class ToggleColumn extends DataColumn
{

    public $action = 'toggle-update';

    public $onValue = 1;

    public $offValue = 0;

    public $linkTemplateOn    = '<a class="toggle-column" data-method="post" data-pjax="0" href="{url}"><span class="toggle-column-active"><i  class="fa fa-check"></i> Active</span></a>';

    public $linkTemplateOff   = '<a class="toggle-column" data-method="post" data-pjax="0" href="{url}"><span class="toggle-column-inactive"><i  class="fa fa-remove"></i> Inactive</span></a>';

    public $enableAjax = true;

    public function init()
    {
        parent::init();

        if ($this->enableAjax) {
            $this->registerJs();
        }
    }

    /**
     * @inheritdoc
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        $attribute = $this->attribute;
        $value = $model->$attribute;

        $url = Url::toRoute([$this->action, 'id' => $model->id, 'attribute' => $attribute]);

        if ($value == $this->onValue) {
            $replace['{url}'] = $url;
            return strtr($this->linkTemplateOn, $replace);
        }
        else {
            $replace['{url}'] = $url;
            return strtr($this->linkTemplateOff, $replace);
        }

    }

    /**
     * Registers the ajax JS
     */
    public function registerJs()
    {
        $js = <<< JS
            $("a.toggle-column").on("click", function(e) {
                e.preventDefault();
                var parentThis = $(this);
                $.post($(this).attr("href"), function(data) {
                    if(data){
                        parentThis.html(data);                        
                    }                      
                });
                return false;
            });
JS;
        $this->grid->view->registerJs($js, View::POS_READY, 'yii2mod-toggle-column');
    }
}

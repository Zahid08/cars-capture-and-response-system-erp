<?php

namespace unlock\modules\core\grid;

class GridView extends \yii\grid\GridView
{
    public $summary = "";

    public $layout = "{summary}\n{items}\n{pager}";
}
<?php

namespace unlock\modules\core\grid;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

class ActionColumn extends \yii\grid\ActionColumn
{
    public $headerOptions = ['class' => 'action-column column-auto-fit'];


    public $header = 'Action';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->initDefaultButtons();
    }

    /**
     * Initializes the default button rendering callbacks.
     */
    protected function initDefaultButtons()
    {
        if (!isset($this->buttons['view'])) {
            $this->buttons['view'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('yii', 'View'),
                    'aria-label' => Yii::t('yii', 'View'),
                    'data-pjax' => '0',
                ], $this->buttonOptions);
                return Html::a("<i class='fa fa-eye'></i> <span>".Yii::t('yii', '')."</span>", $url, $options);
            };
        }
        if (!isset($this->buttons['update'])) {
            $this->buttons['update'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('yii', 'Edit'),
                    'aria-label' => Yii::t('yii', 'Edit'),
                    'data-pjax' => '0',
                ], $this->buttonOptions);
                return Html::a("<i class='fa fa-edit'></i> <span>".Yii::t('yii', '')."</span>", $url, $options);
            };
        }
        if (!isset($this->buttons['delete'])) {
            $this->buttons['delete'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('yii', 'Delete'),
                    'aria-label' => Yii::t('yii', 'Delete'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                    'data-method' => 'post',
                    'data-pjax' => '0',
                ], $this->buttonOptions);
                return Html::a("<i class='fa fa-trash'></i> <span>" . Yii::t('yii', '') . "</span>", $url, $options);
            };
        }
    }

    /**
     * Creates a URL for the given action and model.
     * This method is called for each button and each row.
     * @param string $action the button name (or action ID)
     * @param \yii\db\ActiveRecord $model the data model
     * @param mixed $key the key associated with the data model
     * @param integer $index the current row index
     * @return string the created URL
     */
    public function createUrl($action, $model, $key, $index)
    {
        if (is_callable($this->urlCreator)) {
            return call_user_func($this->urlCreator, $action, $model, $key, $index, $this);
        } else {
            $params = is_array($key) ? $key : ['id' => (string) $key];
            $params[0] = $this->controller ? $this->controller . '/' . $action : $action;

            return Url::toRoute($params);
        }
    }

    /**
     * @inheritdoc
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        $htmlDropDown = '';

        if(preg_match_all('/{+(.*?)}/', $this->template, $matches)){
            $count = 0;
            foreach ($matches[1] as $name) {

                if (isset($this->visibleButtons[$name])) {
                    $isVisible = $this->visibleButtons[$name] instanceof \Closure
                        ? call_user_func($this->visibleButtons[$name], $model, $key, $index)
                        : $this->visibleButtons[$name];
                }
                else {
                    $isVisible = true;
                }

                $url = $this->createUrl($name, $model, $key, $index);
                if ($isVisible && isset($this->buttons[$name]) && Yii::$app->user->checkUrlPermission($url)) {
                    $buttonItem = call_user_func($this->buttons[$name], $url, $model, $key);

                    $htmlDropDown .= $buttonItem;

                    $count++;
                }
            }

            // Add class
            $html = '<div class="grid-action-column">';
            $html .= $htmlDropDown;
            $html .= '</div>';

            return $html;
        }
        else{
            return '';
        }
    }
}

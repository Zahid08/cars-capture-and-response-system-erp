<?php

namespace unlock\modules\core\models;

use Yii;
use yii\helpers\Html;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use unlock\modules\core\helpers\CommonHelper;

/**
 * This is the model class for table "{{%revision}}".
 *
 * @property integer $id
 * @property string $increment_type
 * @property string $increment_prefix
 * @property string $increment_last_id
 */
class AutoIncrementNumber extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%auto_increment_number}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['increment_type', 'increment_last_id'], 'required'],
            [['increment_type', 'increment_prefix', 'increment_last_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'increment_type' => Yii::t('app', 'Increment Type'),
            'increment_prefix' => Yii::t('app', 'Increment Prefix'),
            'increment_last_id' => Yii::t('app', 'Increment Last Id'),
        ];
    }

    public static function getLastIncrementNumber($type) {
        $model = self::find()
            ->where('increment_type=:increment_type', [':increment_type' => $type])
            ->one();

        $lastIncrementNumber = isset($model->increment_last_id) ? $model->increment_last_id : 0;

        return $lastIncrementNumber;
    }

    public static function updateIncrementLastIdField($type, $increment_last_id) {
        $model = self::find()
            ->where('increment_type=:increment_type', [':increment_type' => $type])
            ->one();

        $model->increment_last_id = $increment_last_id;

        if (!$model->save()) {
            throw new Exception(Yii::t('app', Html::errorSummary($model)));
        }
    }

}

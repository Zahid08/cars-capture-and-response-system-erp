<?php

namespace unlock\modules\core;

use Yii;
use yii\helpers\Url;
use yii\redactor\widgets\Redactor;
use yii\web\JsExpression;
/**
 * Core module handles base DotPlant2 functions
 * @package app\modules\user
 */
class CoreModule extends \yii\base\Module
{
    /**
     * @return string Active WYSIWYG widget class name for use in backend forms
     */
    public function wysiwyg_class_name() {
        return Redactor::className();
    }

    /**
     * @return array WYSIWYG params for widget
     */
    public function wysiwyg_params(array $options = []) {
        $params = [
            'clientOptions' => [
                'lang' => 'en',
                'plugins' => ['clips', 'fontcolor', 'imagemanager', 'fontsize'],
                'minHeight' => 300,
            ]
        ];

        if(is_array($options) && count($options) > 0){
            $options = array_merge($params['clientOptions'], $options);
            $params['clientOptions'] = $options;
        }

        return $params;
    }

    public function datePickerTemplate() {
        $template = '{label}<div class="input-group input-group-datepicker date">{input}<span class="input-group-addon"><i class="glyphicon glyphicon-calendar datepicker-calender-icon"></i></span></div>{error}{hint}';
        return $template;
    }

}

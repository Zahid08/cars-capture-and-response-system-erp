<?php

namespace unlock\modules\core\behaviors;

use yii;
use yii\base\Exception;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use unlock\modules\core\helpers\PermissionHelper;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\helpers\CompilationSubmitTo;

class AccessRestrictable extends Behavior
{
    private $_model = null;

    public $path = null;

    public $field = null;

    private $disallowAction = ['update', 'delete'];

    public function init()
    {
        if (true === empty($this->path)) {
            $this->path = false;
        }
        if (true === empty($this->field)) {
            $this->field = false;
        }
    }

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND  => 'checkAccess',
        ];
    }

    public function checkAccess()
    {
        $moduleName = isset(Yii::$app->controller->module->id) ? str_replace('-', '', Yii::$app->controller->module->id) : '';
        $controllerName = isset(Yii::$app->controller->id) ? str_replace('-', '', Yii::$app->controller->id) : '';
        $currentPath = $moduleName . '/' . $controllerName;

        // Role: ship clerk / ship So
        // Item Status: Not Pending
        if(isset($this->path) && $this->path == $currentPath){
            if (in_array(Yii::$app->controller->action->id, $this->disallowAction)){
                // Sanction
                if($currentPath == 'sanction/sanction'){
                    $this->checkSanctionAccess();
                }
            }
        }

    }

    function checkSanctionAccess(){
        $moduleName = isset(Yii::$app->controller->module->id) ? str_replace('-', '', Yii::$app->controller->module->id) : '';
        $controllerName = isset(Yii::$app->controller->id) ? str_replace('-', '', Yii::$app->controller->id) : '';
        $currentPath = $moduleName . '/' . $controllerName;

        if (in_array(Yii::$app->controller->action->id, $this->disallowAction)) {

            $model = $this->owner;

            if($model->status == CommonHelper::STATUS_ACTIVE){
                Yii::$app->session->setFlash('error', Yii::t('app', 'Not allow to edit, update and delete Approved sanction'));
                Yii::$app->getResponse()->redirect(Yii::$app->request->referrer)->send();
                exit();
            }
        }

    }

}
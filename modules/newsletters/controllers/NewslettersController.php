<?php

namespace unlock\modules\newsletters\controllers;


use common\models\Autoload;
use unlock\modules\core\helpers\ExportPdf;
use unlock\modules\newsletters\models\NewslettersComingSoon;
use unlock\modules\newsletters\models\NewslettersFeatureResale;
use unlock\modules\newsletters\models\NewslettersFooter;
use unlock\modules\newsletters\models\NewslettersInvestToday;
use unlock\modules\newsletters\models\NewslettersNews;
use Yii;
use yii\helpers\Html;
use yii\base\Exception;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use unlock\modules\newsletters\models\Newsletters;
use unlock\modules\newsletters\models\NewslettersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use unlock\modules\core\helpers\Collection;
use unlock\modules\core\helpers\FileHelper;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\actions\ExportCsvAction;
use unlock\modules\core\actions\ExportPdfAction;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use unlock\modules\developments\models\Developments;
use yii\base\Model;
use common\models\maropost\maropost;
use  common\models\PHPImageWorkshop\Exception\ImageWorkshopBaseException;
use common\models\PHPImageWorkshop\Exception\ImageWorkshopException;
use  common\models\PHPImageWorkshop\Exif\ExifOrientations;
use common\models\PHPImageWorkshop\Core\Exception\ImageWorkshopLayerException;
use common\models\PHPImageWorkshop\Core\ImageWorkshopLib;
use common\models\PHPImageWorkshop\Core\ImageWorkshopLayer;
use common\models\PHPImageWorkshop\ImageWorkshop;

/**
 * NewslettersController implements the CRUD actions for Newsletters model.
 */
class NewslettersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => Yii::$app->DynamicAccessRules->customAccess('newsletters/newsletters'),
        ];
    }
    
    /**
     * Lists all Newsletters models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewslettersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        Collection::setData($dataProvider);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Newsletters model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $html = $this->renderAjax('view', [
            'model' => $model,
        ]);

        $filename = "CONNECT News"." ".$model->publish_date.".html";
        header('Content-disposition: attachment; filename=' . $filename);
        header('Content-type: text/html');
        return $html;
    }

    public function actionPdf($id)
    {
        $model = $this->findModel($id);

        ExportPdf::$view = 'newsletterPdf.php';
        ExportPdf::$fileName = 'Download.pdf';

        ExportPdf::exportPdf(['model' => $model]);
    }

    /**
     * Creates a new Newsletters model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Newsletters();
        $modelNews = [new NewslettersNews()];
        $modelInvestToday = [new NewslettersInvestToday()];
        $modelComingSoon = [new NewslettersComingSoon()];
        $modelFeatureResale = [new NewslettersFeatureResale()];
        $modelFooter = [new NewslettersFooter()];


        // Newsletter in The News Items Create
        $hasNewslettersNewsItem = true;
        $newsItemArray = Yii::$app->request->post('NewslettersNews');
        if($newsItemArray){
            $modelNews = [];
            foreach ($newsItemArray as $key => $item) {
                $modelNews[$key] = new NewslettersNews();
            }
        }
        Model::loadMultiple($modelNews, Yii::$app->request->post());


        // Newsletter Invest Today Items Create
        $hasNewslettersInvestItem = true;
        $investTodayItemArray = Yii::$app->request->post('NewslettersInvestToday');
        if($investTodayItemArray){
            $modelInvestToday = [];
            foreach ($investTodayItemArray as $key => $item) {
                $modelInvestToday[$key] = new NewslettersInvestToday();
            }
        }
        Model::loadMultiple($modelInvestToday, Yii::$app->request->post());

        // Newsletter Coming Soon Items Create
        $hasNewslettersComingSoonItem = true;
        $comingSoonItemArray = Yii::$app->request->post('NewslettersComingSoon');
        if($comingSoonItemArray){
            $modelComingSoon = [];
            foreach ($comingSoonItemArray as $key => $item) {
                $modelComingSoon[$key] = new NewslettersComingSoon();
            }
        }
        Model::loadMultiple($modelComingSoon, Yii::$app->request->post());



        // Newsletter Feature Resale Items Create
        $hasNewslettersFeatureResaleItem = true;
        $featureResaleItemArray = Yii::$app->request->post('NewslettersFeatureResale');
        if($featureResaleItemArray){
            $modelFeatureResale = [];
            foreach ($featureResaleItemArray as $key => $item) {
                $modelFeatureResale[$key] = new NewslettersFeatureResale();
            }
        }
        Model::loadMultiple($modelFeatureResale, Yii::$app->request->post());



        // Newsletter Footer Items Create
        $hasNewslettersFooter = true;
        $footerItemArray = Yii::$app->request->post('NewslettersFooter');
        if($footerItemArray){
            $modelFooter = [];
            foreach ($footerItemArray as $key => $item) {
                $modelFooter[$key] = new NewslettersFooter();
            }
        }
        Model::loadMultiple($modelFooter, Yii::$app->request->post());



        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return array_merge(
                \yii\widgets\ActiveForm::validate($model),
                \yii\widgets\ActiveForm::validateMultiple($modelNews),
                \yii\widgets\ActiveForm::validateMultiple($modelInvestToday),
                \yii\widgets\ActiveForm::validateMultiple($modelComingSoon),
                \yii\widgets\ActiveForm::validateMultiple($modelFeatureResale),
                \yii\widgets\ActiveForm::validateMultiple($modelFooter)
            );
        }

        if ($model->load(Yii::$app->request->post()) && Model::loadMultiple($modelNews, Yii::$app->request->post()) && Model::loadMultiple($modelInvestToday, Yii::$app->request->post()) && Model::loadMultiple($modelComingSoon, Yii::$app->request->post()) && Model::loadMultiple($modelFeatureResale, Yii::$app->request->post()) && Model::loadMultiple($modelFooter, Yii::$app->request->post())) {
            $this->saveData($model, $modelNews, $modelInvestToday, $modelComingSoon, $modelFeatureResale, $modelFooter);
        }


        return $this->render('_form', [
            'model' => $model,
            'modelNews' => $modelNews,
            'modelInvestToday' => $modelInvestToday,
            'modelComingSoon' => $modelComingSoon,
            'modelFeatureResale' => $modelFeatureResale,
            'modelFooter' => $modelFooter,
            'hasNewslettersNewsItem' => $hasNewslettersNewsItem,

        ]);
    }

    /**
     * Updates an existing Newsletters model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        // In The News Items Update
        $hasNewslettersNewsItem = true;
        $modelNewslettersNewsItems = $model->newslettersNews;
        if (empty($modelNewslettersNewsItems)) {
            $hasNewslettersNewsItem = false;
            $modelNewslettersNewsItems = [new NewslettersNews()];
        }
        $newsItemArray = Yii::$app->request->post('NewslettersNews');
        if($newsItemArray){
            $modelNewslettersNewsItems = [];
            foreach ($newsItemArray as $key => $item) {
                $modelNewslettersNewsItems[$key] = $this->findNewslettersNewsModel($item);
            }
        }

        // Invest Today Items Update
        $modelNewslettersInvestTodayItems = $model->newslettersInvest;
        if (empty($modelNewslettersInvestTodayItems)) {
            $modelNewslettersInvestTodayItems = [new NewslettersInvestToday()];
        }
        $investItemArray = Yii::$app->request->post('NewslettersInvestToday');

        if($investItemArray){
            $modelNewslettersInvestTodayItems = [];
            foreach ($investItemArray as $key => $item) {
                $modelNewslettersInvestTodayItems[$key] = $this->findNewslettersInvestModel($item);
            }
        }


        // Coming Soon Items Update
        $hasNewslettersComingSoon = true;
        $modelNewslettersComingSoonItems = $model->newslettersComingsoon;

        if (empty($modelNewslettersComingSoonItems)) {
            $hasNewslettersComingSoon = false;
            $modelNewslettersComingSoonItems = [new NewslettersComingSoon()];
        }
        $comingsoonItemArray = Yii::$app->request->post('NewslettersComingSoon');

        if($comingsoonItemArray){
            $modelNewslettersComingSoonItems = [];
            foreach ($comingsoonItemArray as $key => $item) {

                $modelNewslettersComingSoonItems[$key] = $this->findNewslettersComingSoonModel($item);
            }
        }



        // Feature Resale Items Update
        $hasNewslettersFeatureResale = true;
        $modelNewslettersFeatureResaleItems = $model->newslettersFeatureresale;

        if (empty($modelNewslettersFeatureResaleItems)) {
            $hasNewslettersFeatureResale = false;
            $modelNewslettersFeatureResaleItems = [new NewslettersFeatureResale()];
        }
        $featureresaleItemArray = Yii::$app->request->post('NewslettersFeatureResale');


        if($featureresaleItemArray){
            $modelNewslettersFeatureResaleItems = [];
            foreach ($featureresaleItemArray as $key => $item) {
                $modelNewslettersFeatureResaleItems[$key] = $this->findNewslettersFeatureResaleModel($item);
            }
        }


        // Footer Items Update
        $modelNewslettersFooterItems = $model->newslettersFooter;
        if (empty($modelNewslettersFooterItems)) {
            $modelNewslettersFooterItems = [new NewslettersFooter()];
        }
        $footerItemArray = Yii::$app->request->post('NewslettersFooter');
        if($footerItemArray){
            $modelNewslettersFooterItems = [];
            foreach ($footerItemArray as $key => $item) {
                $modelNewslettersFooterItems[$key] = $this->findNewslettersFooterModel($item);
            }
        }



        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && Model::loadMultiple($modelNewslettersNewsItems, Yii::$app->request->post()) && Model::loadMultiple($modelNewslettersInvestTodayItems, Yii::$app->request->post()) && Model::loadMultiple($modelNewslettersComingSoonItems, Yii::$app->request->post()) && Model::loadMultiple($modelNewslettersFeatureResaleItems, Yii::$app->request->post()) && Model::loadMultiple($modelNewslettersFooterItems, Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return array_merge(\yii\widgets\ActiveForm::validate($model), \yii\widgets\ActiveForm::validateMultiple($modelNewslettersNewsItems), \yii\widgets\ActiveForm::validateMultiple($modelNewslettersInvestTodayItems), \yii\widgets\ActiveForm::validateMultiple($modelNewslettersComingSoonItems), \yii\widgets\ActiveForm::validateMultiple($modelNewslettersFeatureResaleItems), \yii\widgets\ActiveForm::validateMultiple($modelNewslettersFooterItems));
        }


        if ($model->load(Yii::$app->request->post()) && Model::loadMultiple($modelNewslettersNewsItems, Yii::$app->request->post()) && Model::loadMultiple($modelNewslettersInvestTodayItems, Yii::$app->request->post()) && Model::loadMultiple($modelNewslettersComingSoonItems, Yii::$app->request->post()) && Model::loadMultiple($modelNewslettersFeatureResaleItems, Yii::$app->request->post()) && Model::loadMultiple($modelNewslettersFooterItems, Yii::$app->request->post()))
        {
            $this->saveData($model, $modelNewslettersNewsItems, $modelNewslettersInvestTodayItems, $modelNewslettersComingSoonItems, $modelNewslettersFeatureResaleItems, $modelNewslettersFooterItems);
        }


        return $this->render('_form', [
            'model' => $model,
            'modelInvestToday' => $modelNewslettersInvestTodayItems,
            'modelComingSoon' => $modelNewslettersComingSoonItems,
            'modelFeatureResale' => $modelNewslettersFeatureResaleItems,
            'modelFooter' => $modelNewslettersFooterItems,
            'hasNewslettersNewsItem' => $hasNewslettersNewsItem,
            'modelNews' => $modelNewslettersNewsItems,
        ]);
    }

    /**
     * Deletes an existing Newsletters model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        try {
            if (!$model->delete()) {
                throw new Exception(Yii::t('app', 'The record cannot be deleted.'));
            }

            // Delete Image:
            // FileHelper::removeFile($model->image, 'sanction');
            Yii::$app->session->setFlash('success', Yii::t('app', 'The record have been successfully deleted.'));
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            if($e->getName() == CommonHelper::INTEGRITY_CONSTRAINT_VIOLATION){
                $message = "This record has relational data. You must delete the following data first";
                $message .= "<ul>";
                $message .= "<li>Example => Example List</li>";
                $message .= "</ul>";
            }
            Yii::$app->session->setFlash('error', $message);
        }

        return $this->redirect(['index']);
    }


    // News Items Delete
    public function actionDeleteNewsItem($id){
        $model = $this->findNewsModel($id);
        try {
            if (!$model->delete()) {
                throw new Exception(Yii::t('app', 'The record cannot be deleted.'));
            }
            Yii::$app->session->setFlash('success', Yii::t('app', 'The record have been successfully deleted.'));
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            if($e->getName() == CommonHelper::INTEGRITY_CONSTRAINT_VIOLATION){
                $message = "This record has relational data. You must delete the following data first";
                $message .= "<ul>";
                $message .= "<li>Example => Example List</li>";
                $message .= "</ul>";
            }
            Yii::$app->session->setFlash('error', $message);
        }
        return $this->redirect(['newsletters/update?id='.$_REQUEST['newsletterid']]);
    }

    // Invest Items Delete
    public function actionDeleteInvestItem($id){
        $model = $this->findInvestModel($id);
        try {
            if (!$model->delete()) {
                throw new Exception(Yii::t('app', 'The record cannot be deleted.'));
            }
            Yii::$app->session->setFlash('success', Yii::t('app', 'The record have been successfully deleted.'));
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            if($e->getName() == CommonHelper::INTEGRITY_CONSTRAINT_VIOLATION){
                $message = "This record has relational data. You must delete the following data first";
                $message .= "<ul>";
                $message .= "<li>Example => Example List</li>";
                $message .= "</ul>";
            }
            Yii::$app->session->setFlash('error', $message);
        }
        return $this->redirect(['newsletters/update?id='.$_REQUEST['newsletterid']]);
    }


    // Comming Soon Items Delete
    public function actionDeleteComingItem($id){
        $model = $this->findComingSoonModel($id);
        try {
            if (!$model->delete()) {
                throw new Exception(Yii::t('app', 'The record cannot be deleted.'));
            }
            Yii::$app->session->setFlash('success', Yii::t('app', 'The record have been successfully deleted.'));
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            if($e->getName() == CommonHelper::INTEGRITY_CONSTRAINT_VIOLATION){
                $message = "This record has relational data. You must delete the following data first";
                $message .= "<ul>";
                $message .= "<li>Example => Example List</li>";
                $message .= "</ul>";
            }
            Yii::$app->session->setFlash('error', $message);
        }
        return $this->redirect(['newsletters/update?id='.$_REQUEST['newsletterid']]);
    }

    // Feature Resale Items Delete
    public function actionDeleteFeatureResaleItem($id){
        $model = $this->findFeatureResaleModel($id);
        try {
            if (!$model->delete()) {
                throw new Exception(Yii::t('app', 'The record cannot be deleted.'));
            }
            Yii::$app->session->setFlash('success', Yii::t('app', 'The record have been successfully deleted.'));
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            if($e->getName() == CommonHelper::INTEGRITY_CONSTRAINT_VIOLATION){
                $message = "This record has relational data. You must delete the following data first";
                $message .= "<ul>";
                $message .= "<li>Example => Example List</li>";
                $message .= "</ul>";
            }
            Yii::$app->session->setFlash('error', $message);
        }
        return $this->redirect(['newsletters/update?id='.$_REQUEST['newsletterid']]);
    }
    // Footer Items Delete
    public function actionDeleteFooterItem($id){
        $model = $this->findFootereModel($id);
        try {
            if (!$model->delete()) {
                throw new Exception(Yii::t('app', 'The record cannot be deleted.'));
            }
            Yii::$app->session->setFlash('success', Yii::t('app', 'The record have been successfully deleted.'));
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            if($e->getName() == CommonHelper::INTEGRITY_CONSTRAINT_VIOLATION){
                $message = "This record has relational data. You must delete the following data first";
                $message .= "<ul>";
                $message .= "<li>Example => Example List</li>";
                $message .= "</ul>";
            }
            Yii::$app->session->setFlash('error', $message);
        }
        return $this->redirect(['newsletters/update?id='.$_REQUEST['footerid']]);
    }

    /**
    * Save Data.
    * @param object $model
    * @return mixed
    */
    private function saveData($model, $modelNewslettersNewsItems, $modelInvestTodayItems, $modelNewslettersComingSoonItems, $modelNewslettersFeatureResaleItems, $modelNewslettersFooterItems){
        if (isset($model) && isset($modelNewslettersNewsItems) && isset($modelInvestTodayItems) && isset($modelNewslettersComingSoonItems) && isset($modelNewslettersFeatureResaleItems) && isset($modelNewslettersFooterItems))  {
            //$transaction = Yii::$app->db->beginTransaction();
            try {
                if (!$model->save()) {
                    throw new Exception(Yii::t('app', Html::errorSummary($model)));
                }

                $imageUrlPath = Yii::getAlias('@webliveUrl').'/newsletter/'.$model->id.'/';


                $images = [];

                // Newsletter Image Directory Create
                if (!file_exists(Yii::getAlias('@newsletter').'/common/tempt'.$model->id)) {
                    mkdir(Yii::getAlias('@newsletter').'/common/tempt'.$model->id, 0777, true);
                    //mkdir(Yii::getAlias('@newsletter').'/tempt'.$model->id.'/thum', 0777, true);
                }


                if(!empty($model->feature_img_url)){
                    $images[] = $model->feature_img_url;

                    $allowed_extension = array("jpg", "png", "jpeg", "gif");
                    $url_array = explode("/", $model->feature_img_url);
                    $image_name = end($url_array);
                    $model->feature_img_url = $imageUrlPath.$image_name;


                    /*$image_array = explode(".", $image_name);
                    $extension = end($image_array);
                    if(in_array($extension, $allowed_extension))
                    {
                        $image_data = file_get_contents($model->feature_img_url);
                        $new_image_path = Yii::getAlias('@newsletter').'/tempt'.$model->id.'/'.$image_array[0].'.'. $extension;
                        file_put_contents($new_image_path, $image_data);
                        $model->feature_img_url = Yii::getAlias('@baseUrl').'/newsletter/common/tempt'.$model->id.'/'.$image_array[0].'.'.$extension;
                    }*/
                }
                if (!$model->save()) {
                    throw new Exception(Yii::t('app', Html::errorSummary($model)));
                }

                // In the News Items
                foreach ($modelNewslettersNewsItems as $modelNewslettersNewsItem){
                    if(!empty($modelNewslettersNewsItem->img_url)){
                        $images[] = $modelNewslettersNewsItem->img_url;

                        $allowed_extension = array("jpg", "png", "jpeg", "gif");
                        $url_array = explode("/", $modelNewslettersNewsItem->img_url);
                        $image_name = end($url_array);
                        $modelNewslettersNewsItem->img_url = $imageUrlPath.$image_name;


                        /*$image_array = explode(".", $image_name);
                        $extension = end($image_array);
                        if(in_array($extension, $allowed_extension))
                        {
                            $image_data = file_get_contents($modelNewslettersNewsItem->img_url);
                            $new_image_path = Yii::getAlias('@newsletter').'/tempt'.$model->id.'/'.$image_array[0].'.'. $extension;
                            file_put_contents($new_image_path, $image_data);
                            $modelNewslettersNewsItem->img_url = Yii::getAlias('@baseUrl').'/newsletter/common/tempt'.$model->id.'/'.$image_array[0].'.'.$extension;

                        }*/
                    }
                    $modelNewslettersNewsItem->newsletter_id = $model->id;
                    if (!$modelNewslettersNewsItem->save()) {
                        throw new Exception(Yii::t('app', Html::errorSummary($modelNewslettersNewsItem)));
                    }

                    /*$sourcePath = $new_image_path;
                    $thumbPath = Yii::getAlias('@newsletter').'/tempt'.$model->id.'/'.$image_array[0].'.'. $extension;;
                    $thumbWidth = '290';
                    $thumbHeight = '150';
                    FileHelper::Thumbnail($sourcePath,$thumbPath,$thumbWidth,$thumbHeight);*/
                }

                // Invest Today Items
                foreach ($modelInvestTodayItems as $modelInvestTodayItem){
                    if(!empty($modelInvestTodayItem->img_url)){
                        $images[] = $modelInvestTodayItem->img_url;


                        $allowed_extension = array("jpg", "png", "jpeg", "gif");
                        $url_array = explode("/", $modelInvestTodayItem->img_url);
                        $image_name = end($url_array);
                        $modelInvestTodayItem->img_url = $imageUrlPath.$image_name;

                        /*$image_array = explode(".", $image_name);
                        $extension = end($image_array);
                        if(in_array($extension, $allowed_extension))
                        {
                            $image_data = file_get_contents($modelInvestTodayItem->img_url);
                            $new_image_path = Yii::getAlias('@newsletter').'/tempt'.$model->id.'/'.$image_array[0].'.'. $extension;
                            file_put_contents($new_image_path, $image_data);
                            $modelInvestTodayItem->img_url = Yii::getAlias('@baseUrl').'/newsletter/common/tempt'.$model->id.'/'.$image_array[0].'.'.$extension;
                        }*/
                    }
                    $modelInvestTodayItem->newsletter_id = $model->id;
                      if (!$modelInvestTodayItem->save()) {
                          throw new Exception(Yii::t('app', Html::errorSummary($modelInvestTodayItem)));
                      }
                }

                // Coming Soon Items
                foreach ($modelNewslettersComingSoonItems as $modelNewslettersComingSoonItem){
                    if(!empty($modelNewslettersComingSoonItem->img_url)){
                        $images[] = $modelNewslettersComingSoonItem->img_url;


                        $allowed_extension = array("jpg", "png", "jpeg", "gif");
                        $url_array = explode("/", $modelNewslettersComingSoonItem->img_url);


                        $image_name = end($url_array);
                        $modelNewslettersComingSoonItem->img_url = $imageUrlPath.$image_name;

                        /*$image_array = explode(".", $image_name);
                        $extension = end($image_array);
                        if(in_array($extension, $allowed_extension))
                        {
                            $image_data = file_get_contents($modelNewslettersComingSoonItem->img_url);
                            $new_image_path = Yii::getAlias('@newsletter').'/tempt'.$model->id.'/'.$image_array[0].'.'. $extension;
                            file_put_contents($new_image_path, $image_data);
                            $modelNewslettersComingSoonItem->img_url = Yii::getAlias('@baseUrl').'/newsletter/common/tempt'.$model->id.'/'.$image_array[0].'.'.$extension;
                        }*/
                    }
                    $modelNewslettersComingSoonItem->newsletter_id = $model->id;
                    if (!$modelNewslettersComingSoonItem->save()) {
                      throw new Exception(Yii::t('app', Html::errorSummary($modelNewslettersComingSoonItem)));
                    }
                }

                // Feature Resale Items

                $count = 0;
                foreach ($modelNewslettersFeatureResaleItems as $modelNewslettersFeatureResaleItem){
                    $oldImgUrl = isset($modelNewslettersFeatureResaleItem->oldAttributes['img_url']) ? $modelNewslettersFeatureResaleItem->oldAttributes['img_url'] : null;

                    if ($modelNewslettersFeatureResaleItem->img_url = UploadedFile::getInstance($modelNewslettersFeatureResaleItem, '['.$count.']img_url')) {
                        $modelNewslettersFeatureResaleItem->img_url->saveAs(Yii::getAlias('@newsletter').'/common/tempt'.$model->id.'/'.$modelNewslettersFeatureResaleItem->img_url->name);
                        //$images[]           = Yii::getAlias('@baseUrl').'/newsletter/common/tempt'.$model->id.'/'.$modelNewslettersFeatureResaleItem->img_url->name;
                        if (!empty($modelNewslettersFeatureResaleItem->resale_bed)){
                            $feture_bed=$modelNewslettersFeatureResaleItem->resale_bed.' BED';}
                        else{$feture_bed='Studio';}

                        if (!empty($modelNewslettersFeatureResaleItem->resale_bath)){
                            $resale_bath=$modelNewslettersFeatureResaleItem->resale_bath;}
                        else{$resale_bath='0';}

                        if (!empty($modelNewslettersFeatureResaleItem->resale_price)){
                            //$resale_price = Yii::$app->formatter->format($modelNewslettersFeatureResaleItem->resale_price, 'money');
                            //$resale_price = number_format($modelNewslettersFeatureResaleItem->resale_price);
                            $resalePrice = Yii::$app->formatter->format($modelNewslettersFeatureResaleItem->resale_price, 'money');
                            $resale_price = str_replace('.00', '', $resalePrice);
                        }
                        else{$resale_price='';}

                        if (!empty($modelNewslettersFeatureResaleItem->ribbon_text)){$ribbon_text=$modelNewslettersFeatureResaleItem->ribbon_text;}
                        else{$ribbon_text='';}

                        $main_image         = ImageWorkshop::initFromPath(Yii::getAlias('@baseUrl').'/newsletter/common/tempt'.$model->id.'/'.$modelNewslettersFeatureResaleItem->img_url->name);
                        $main_image->resizeInPixel(190, 190, false,0,0,'MM');

                        $text1              = ImageWorkshop::initTextLayer($feture_bed,Yii::getAlias('@newsletter').'/common/Resources/fonts/Rajdhani-Bold.ttf','14');
                        $text2              = ImageWorkshop::initTextLayer($resale_bath.' BATH',Yii::getAlias('@newsletter').'/common/Resources/fonts/Rajdhani-Bold.ttf','14');
                        $text3              = ImageWorkshop::initTextLayer('$'.$resale_price,Yii::getAlias('@newsletter').'/common/Resources/fonts/Rajdhani-Bold.ttf','14');

                        if ($modelNewslettersFeatureResaleItem->image_desc==1) {
                            $box_image = ImageWorkshop::initFromPath(Yii::getAlias('@newsletter') . '/common/Resources/images/banner.png');
                        }
                        /*$blue_banner        =ImageWorkshop::initFromPath(Yii::getAlias('@newsletter').'/common/Resources/images/blue_banner.png');
                        $banner_text        =ImageWorkshop::initTextLayer($ribbon_text,Yii::getAlias('@newsletter').'/common/Resources/fonts/Oswald-SemiBold.ttf');*/

                        if (!empty($modelNewslettersFeatureResaleItem->ribbon_text)) {
                            $blue_banner = ImageWorkshop::initFromPath(Yii::getAlias('@newsletter') . '/common/Resources/images/blue_banner.png');
                            $banner_text = ImageWorkshop::initTextLayer($modelNewslettersFeatureResaleItem->ribbon_text, Yii::getAlias('@newsletter') . '/common/Resources/fonts/Rajdhani-Bold.ttf');
                        }
                        if ($modelNewslettersFeatureResaleItem->image_desc==1) {
                            $box_image->addLayerOnTop($text1, 10, 70, "RB");
                            $box_image->addLayerOnTop($text2, 10, 40, "RB");
                            $box_image->addLayerOnTop($text3, 10,10, "RB");
                        }
                        if (!empty($modelNewslettersFeatureResaleItem->ribbon_text)) {
                            $blue_banner->addLayerOnTop($banner_text, 45, 30, "LB");
                            $blue_banner->rotate(-45);
                            $main_image->addLayer(1, $blue_banner, -30, 85, "LB");
                        }
                        if ($modelNewslettersFeatureResaleItem->image_desc==1) {
                            $main_image->addLayer(1, $box_image, 95, 0, "LB");
                        }
                        $dirPath                =Yii::getAlias('@newsletter').'/common/tempt'.$model->id.'/';
                        $filename               = 'f1-'.$modelNewslettersFeatureResaleItem->img_url->name;
                        $createFolders          = true;
                        $backgroundColor        = null;
                        $imageQuality           = 95;

                        $main_image->save($dirPath, $filename, $createFolders, $backgroundColor, $imageQuality);

                        $images[]           = Yii::getAlias('@baseUrl').'/newsletter/common/tempt'.$model->id.'/'.'f1-'.$modelNewslettersFeatureResaleItem->img_url->name;

                        $modelNewslettersFeatureResaleItem->img_url = $imageUrlPath.'f1-'.$modelNewslettersFeatureResaleItem->img_url->name;
                    }else{
                        if (!empty($oldImgUrl)) {
                            preg_match("/[^\/]+$/", $oldImgUrl, $matches);
                            $image_name = str_replace('f1-', '', $matches[0]);

                            if (!empty($modelNewslettersFeatureResaleItem->resale_bed)) {
                                $feture_bed = $modelNewslettersFeatureResaleItem->resale_bed. ' BED';
                            } else {
                                $feture_bed = 'Studio';
                            }

                            if (!empty($modelNewslettersFeatureResaleItem->resale_bath)) {
                                $resale_bath = $modelNewslettersFeatureResaleItem->resale_bath;
                            } else {
                                $resale_bath = '0';
                            }

                            if (!empty($modelNewslettersFeatureResaleItem->resale_price)) {
                                //$resale_price = $modelNewslettersFeatureResaleItem->resale_price;
                                $resalePrice = Yii::$app->formatter->format($modelNewslettersFeatureResaleItem->resale_price, 'money');
                                $resale_price = str_replace('.00', '', $resalePrice);

                                //$resale_price = number_format($modelNewslettersFeatureResaleItem->resale_price);

                            } else {
                                $resale_price = '';
                            }

                            $main_image = ImageWorkshop::initFromPath(Yii::getAlias('@baseUrl') . '/newsletter/common/tempt' . $model->id . '/' . $image_name);
                            $main_image->resizeInPixel(190, 190, false, 0, 0, 'MM');

                            $text1 = ImageWorkshop::initTextLayer($feture_bed, Yii::getAlias('@newsletter') . '/common/Resources/fonts/Rajdhani-Bold.ttf','14');
                            $text2 = ImageWorkshop::initTextLayer($resale_bath . ' BATH', Yii::getAlias('@newsletter') . '/common/Resources/fonts/Rajdhani-Bold.ttf','14');
                            $text3 = ImageWorkshop::initTextLayer('$' . $resale_price, Yii::getAlias('@newsletter') . '/common/Resources/fonts/Rajdhani-Bold.ttf','14');

                            if ($modelNewslettersFeatureResaleItem->image_desc==1) {
                                $box_image = ImageWorkshop::initFromPath(Yii::getAlias('@newsletter') . '/common/Resources/images/banner.png');
                            }
                            if (!empty($modelNewslettersFeatureResaleItem->ribbon_text)) {
                                $blue_banner = ImageWorkshop::initFromPath(Yii::getAlias('@newsletter') . '/common/Resources/images/blue_banner.png');
                                $banner_text = ImageWorkshop::initTextLayer($modelNewslettersFeatureResaleItem->ribbon_text, Yii::getAlias('@newsletter') . '/common/Resources/fonts/Rajdhani-Bold.ttf');
                            }
                            if ($modelNewslettersFeatureResaleItem->image_desc==1) {
                                $box_image->addLayerOnTop($text1, 10, 70, "RB");
                                $box_image->addLayerOnTop($text2, 10, 40, "RB");
                                $box_image->addLayerOnTop($text3, 10,10, "RB");
                            }

                            if (!empty($modelNewslettersFeatureResaleItem->ribbon_text)) {
                                $blue_banner->addLayerOnTop($banner_text, 45, 30, "LB");
                                $blue_banner->rotate(-45);
                                $main_image->addLayer(1, $blue_banner, -30, 85, "LB");
                            }
                            if ($modelNewslettersFeatureResaleItem->image_desc==1) {
                                $main_image->addLayer(1, $box_image, 95, 0, "LB");
                            }
                            $dirPath = Yii::getAlias('@newsletter') . '/common/tempt' . $model->id . '/';
                            $filename = 'f1-' . $image_name;
                            $createFolders = true;
                            $backgroundColor = null;
                            $imageQuality = 95;
                            $main_image->save($dirPath, $filename, $createFolders, $backgroundColor, $imageQuality);

                            $images[] = Yii::getAlias('@baseUrl') . '/newsletter/common/tempt' . $model->id . '/' . 'f1-' . $image_name;

                            $modelNewslettersFeatureResaleItem->img_url = $imageUrlPath . 'f1-' . $image_name;
                        }
                        //$images[] =$imag_url;
                        //$modelNewslettersFeatureResaleItem->img_url = $oldImgUrl;
                    }

                    $modelNewslettersFeatureResaleItem->newsletter_id = $model->id;
                    if (!$modelNewslettersFeatureResaleItem->save()) {
                      throw new Exception(Yii::t('app', Html::errorSummary($modelNewslettersFeatureResaleItem)));
                    }
                    $count++;
                }

                foreach ($modelNewslettersFooterItems as $modelNewslettersFooterItem){
                    $modelNewslettersFooterItem->newsletter_id = $model->id;
                    if (!$modelNewslettersFooterItem->save()) {
                        throw new Exception(Yii::t('app', Html::errorSummary($modelNewslettersFooterItem)));
                    }
                }

              $newsletterImages = [
                  'ID' => $model->id,
                  'newsletter' => $images
              ];

                $webliveUrl = Yii::getAlias('@webliveUrl');

              $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL,$webliveUrl."/wp-json/wp-connect/newsletter?access_token=Y29uZG8tZGV2ZWxvcG1lbnRzVW5sb&".time());
                curl_setopt($ch, CURLOPT_POST, 1);

              curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($newsletterImages));

              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              $server_output = curl_exec($ch);


              curl_close ($ch);


                //$transaction->commit();
                Yii::$app->session->setFlash('success', Yii::t('app', 'Record successfully saved.'));
                $this->_redirect($model);
            }
            catch (Exception $e) {
                //$transaction->rollBack();
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
    }
    
    private function _redirect($model){
        $action = Yii::$app->request->post('action', 'save');
        switch ($action) {
            case 'save':
                //return $this->redirect(['index']);
                return $this->redirect(['update', 'id' => $model->id]);
                break;
            case 'apply':
                return $this->redirect(['update', 'id' => $model->id]);
                break;
            case 'save2new':
                return $this->redirect(['create']);
                break;
            default:
                return $this->redirect(['index']);
        }
    }


    // Development Details
    public function actionDevelopmentDetails($id){
        if (isset($id)){
            $developmentModel = Developments::find()->where(['development_id' => $id])->one();
            echo Json::encode($developmentModel);
        }
    }
    public function actionDevelopmentlist($status){
        //Developers Grade  (1 = A+, 2 = A, 3 = B+,4 = B,5 = C, 6 = D)
        $developmentModel = Developments::find()->where(['sales_status' => $status])->orWhere(['sales_status' => 'vip'])->andWhere(['grade' => ['1','2','4']])->orderBy(['development_name' => SORT_ASC])->all();
        $developmentlist = ArrayHelper::map($developmentModel, 'development_id', 'development_name');
        echo Json::encode($developmentlist);
    }



    // maropost Template Push
    public function actionMaropost($id)
    {
        $model = $this->findModel($id);

        $html = $this->renderAjax('view', [
            'model' => $model,
        ]);

        $maropost 		= new maropost();
        $contactsNew 	= $maropost->template();

        $arrayNew = array(
            "content"=>array(
                "name"=> $model->subject,
                //"text_part"=>"text part of the content",
                "html_part"=> $html,
                "full_email"=> "false",
                "footer_type"=> "default_footer",
                "footer_id"=> ""
            )
        );

        $resposeNew =  $contactsNew->create_template($arrayNew);
        if($resposeNew['http_status'] == 422){
            $resposeNew=$contactsNew->get_template_content();

            foreach ($resposeNew as $data){
                if ($data['name'] == $model->subject ) {
                    $arrayNew = array(
                        "content" => array(
                            "name" => $model->subject,
                            //"text_part" => "text part of the content",
                            "html_part" => $html,
                            "full_email" => "false",
                            "footer_type" => "default_footer",
                            "footer_id" => ""
                        ),
                        "id" => $data["id"]
                    );
                    $contactsNew->put_template_content($arrayNew);
                    Yii::$app->session->setFlash('success', Yii::t('app', 'Successfully Update Template.'));
                    return $this->redirect(['update', 'id' => $model->id]);
                }

            }
        }else{
            Yii::$app->session->setFlash('success', Yii::t('app', 'Successfully Push Template.'));
            return $this->redirect(['update', 'id' => $model->id]);
        }
    }


    /**
     * Finds the Newsletters model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Newsletters the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Newsletters::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    protected function findNewsModel($id)
    {
        if (($model = NewslettersNews::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    protected function findNewslettersNewsModel($item)
    {
        if(array_key_exists('newsid', $item)){
            if (($model = NewslettersNews::findOne($item['newsid'])) !== null) {
                return $model;
            }
            else {
                return new NewslettersNews();
            }
        }
        else{
            return new NewslettersNews();
        }
    }

    protected function findInvestModel($id)
    {
        if (($model = NewslettersInvestToday::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
    protected function findNewslettersInvestModel($item)
    {
        if(array_key_exists('investid', $item)){
            if (($model = NewslettersInvestToday::findOne($item['investid'])) !== null) {
                return $model;
            }
            else {
                return new NewslettersInvestToday();
            }
        }
        else{
            return new NewslettersInvestToday();
        }
    }



    protected function findComingSoonModel($id)
    {
        if (($model = NewslettersComingSoon::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
    protected function findNewslettersComingSoonModel($item)
    {
        if(array_key_exists('comingsoonid', $item)){
            if (($model = NewslettersComingSoon::findOne($item['comingsoonid'])) !== null) {
                return $model;
            }
            else {
                return new NewslettersComingSoon();
            }
        }
        else{
            return new NewslettersComingSoon();
        }
    }


    protected function findFeatureResaleModel($id)
    {
        if (($model = NewslettersFeatureResale::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
    protected function findNewslettersFeatureResaleModel($item)
    {
        if(array_key_exists('featureresaleid', $item)){
            if (($model = NewslettersFeatureResale::findOne($item['featureresaleid'])) !== null) {
                return $model;
            }
            else {
                return new NewslettersFeatureResale();
            }
        }
        else{
            return new NewslettersFeatureResale();
        }
    }


    protected function findFootereModel($id)
    {
        if (($model = NewslettersFooter::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
    protected function findNewslettersFooterModel($item)
    {
        if(array_key_exists('footerid', $item)){
            if (($model = NewslettersFooter::findOne($item['footerid'])) !== null) {
                return $model;
            }
            else {
                return new NewslettersFooter();
            }
        }
        else{
            return new NewslettersFooter();
        }
    }
}

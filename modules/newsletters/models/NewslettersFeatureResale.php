<?php

namespace unlock\modules\newsletters\models;

use Yii;
use yii\helpers\ArrayHelper;
use unlock\modules\core\helpers\CommonHelper;

/**
 * This is the model class for table "{{%newsletters_feature_resale}}".
 *
 * @property integer $id
 * @property integer $newsletter_id
 * @property string $top_title
 * @property string $title
 * @property string $url
 * @property string $img_url
 * @property string $excerpt
 */
class NewslettersFeatureResale extends \yii\db\ActiveRecord
{
    public $featureresaleid;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%newsletters_feature_resale}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['newsletter_id'], 'integer'],
            [['excerpt'], 'string'],
            [['image_desc'], 'integer'],
            [['ribbon_text'], 'string'],
            [['ribbon_color'], 'string'],
            [['resale_bed'], 'string'],
            [['resale_bath'], 'string'],
            [['resale_price'], 'string'],
            [['resale_color'], 'string'],
            /*      [['title', 'excerpt', 'top_title'], 'required'],*/
            [['top_title', 'title'], 'string', 'max' => 100],
            [['img_url'], 'file'],
            [['url', 'img_url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'newsletter_id' => Yii::t('app', 'Newsletter ID'),
            'top_title' => Yii::t('app', 'Top Title'),
            'title' => Yii::t('app', 'Title'),
            'url' => Yii::t('app', 'URL'),
            'img_url' => Yii::t('app', 'Image URL'),
            'excerpt' => Yii::t('app', 'Excerpt'),
            'image_desc' => Yii::t('app', 'Show'),
            'ribbon_text' => Yii::t('app', 'Ribon Text'),
            'ribbon_color' => Yii::t('app', 'Ribon Color'),
            'resale_bed' => Yii::t('app', 'Bed'),
            'resale_bath' => Yii::t('app', 'Bath'),
            'resale_price' => Yii::t('app', 'Price'),
            'resale_color' => Yii::t('app', 'Box Color'),
        ];
    }

    /** Get All Active NewslettersFeatureResale Drop Down List*/
    public static function newslettersFeatureResaleDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
     * before save data
     */

}

<?php

namespace unlock\modules\newsletters\models;

use Yii;
use yii\helpers\ArrayHelper;
use unlock\modules\core\helpers\CommonHelper;

/**
 * This is the model class for table "{{%newsletters_news}}".
 *
 * @property integer $id
 * @property integer $newsletter_id
 * @property string $title
 * @property string $url
 * @property string $img_url
 * @property string $excerpt
 */
class NewslettersNews extends \yii\db\ActiveRecord
{

    public $newsid;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%newsletters_news}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['newsletter_id'], 'integer'],
            [['excerpt'], 'string'],
            [['title', 'url', 'img_url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'newsletter_id' => Yii::t('app', 'Newsletter ID'),
            'title' => Yii::t('app', 'Title'),
            'url' => Yii::t('app', 'URL'),
            'img_url' => Yii::t('app', 'Image URL'),
            'excerpt' => Yii::t('app', 'Excerpt'),
        ];
    }

    /** Get All Active NewslettersNews Drop Down List*/
    public static function newslettersNewsDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
   /* public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }*/
}

<?php

namespace unlock\modules\newsletters\models;

use Yii;
use yii\base\Model;
use unlock\modules\core\data\ActiveDataProvider;
use unlock\modules\newsletters\models\Newsletters;
use unlock\modules\core\helpers\CommonHelper;

/**
 * NewslettersSearch represents the model behind the search form about `unlock\modules\newsletters\models\Newsletters`.
 */
class NewslettersSearch extends Newsletters
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by', 'status'], 'integer'],
            [['name', 'subject', 'publish_date', 'feature_url', 'feature_excerpt', 'feature_img_url', 'stats_title', 'ln_amount', 'ln_percent', 'sn_amount', 'sn_percent', 'tv_percent', 'tv_quarter', 'pc_quarter', 'pc_percent', 'stats_tweet', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Newsletters::find();

        // add conditions that should always apply here
        $pageSize = isset($params['per-page']) ? intval($params['per-page']) : CommonHelper::GRID_PER_PAGE;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created_at' => SORT_DESC]],
            'pagination' =>  ['pageSize' => $pageSize],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'publish_date' => $this->publish_date,
            'stats_tweet' => $this->stats_tweet,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'subject', $this->subject])
            ->andFilterWhere(['like', 'feature_url', $this->feature_url])
            ->andFilterWhere(['like', 'feature_excerpt', $this->feature_excerpt])
            ->andFilterWhere(['like', 'feature_img_url', $this->feature_img_url])
            ->andFilterWhere(['like', 'stats_title', $this->stats_title]);

        return $dataProvider;
    }
}

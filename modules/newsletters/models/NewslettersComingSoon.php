<?php

namespace unlock\modules\newsletters\models;

use Yii;
use yii\helpers\ArrayHelper;
use unlock\modules\core\helpers\CommonHelper;

/**
 * This is the model class for table "{{%newsletters_coming_soon}}".
 *
 * @property integer $id
 * @property integer $newsletter_id
 * @property string $title
 * @property string $url
 * @property string $img_url
 * @property string $excerpt
 */
class NewslettersComingSoon extends \yii\db\ActiveRecord
{
    public $comingsoonid;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%newsletters_coming_soon}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['newsletter_id', 'development_id'], 'integer'],
            [['excerpt','coming_soon_bed','coming_soon_bath','coming_soon_price','coming_soon_color'], 'string'],
            [['image_desc'], 'integer'],
            [['title'], 'string', 'max' => 100],
            [['img_url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'newsletter_id' => Yii::t('app', 'Newsletter ID'),
            'development_id' => Yii::t('app', 'Development ID'),
            'title' => Yii::t('app', 'Title'),
            'img_url' => Yii::t('app', 'Image URL'),
            'excerpt' => Yii::t('app', 'Excerpt'),
            'image_desc' => Yii::t('app', 'Show'),
            'coming_soon_bed' => Yii::t('app', 'Bed'),
            'coming_soon_bath' => Yii::t('app', 'Bath'),
            'coming_soon_price' => Yii::t('app', 'Price'),
            'coming_soon_color' => Yii::t('app', 'Box Color'),
        ];
    }

    /** Get All Active NewslettersComingSoon Drop Down List*/
    public static function newslettersComingSoonDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }


}

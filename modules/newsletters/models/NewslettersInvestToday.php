<?php

namespace unlock\modules\newsletters\models;

use Yii;
use yii\helpers\ArrayHelper;
use unlock\modules\core\helpers\CommonHelper;

/**
 * This is the model class for table "{{%newsletters_invest_today}}".
 *
 * @property integer $id
 * @property integer $newsletter_id
 * @property string $title
 * @property string $url
 * @property string $img_url
 * @property string $excerpt
 */
class NewslettersInvestToday extends \yii\db\ActiveRecord
{

    public $investid;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%newsletters_invest_today}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['newsletter_id', 'development_id'], 'integer'],
            [['excerpt','invest_today_bed','invest_today_bath','invest_today_price','invest_today_color'], 'string'],
            [['image_desc'], 'integer'],
            [['title', 'img_url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'newsletter_id' => Yii::t('app', 'Newsletter ID'),
            'development_id' => Yii::t('app', 'Development ID'),
            'title' => Yii::t('app', 'Title'),
            'img_url' => Yii::t('app', 'Image URL'),
            'excerpt' => Yii::t('app', 'Excerpt'),
            'image_desc' => Yii::t('app', 'Show'),
            'invest_today_bed' => Yii::t('app', 'Bed'),
            'invest_today_bath' => Yii::t('app', 'Bath'),
            'invest_today_price' => Yii::t('app', 'Price'),
            'invest_today_color' => Yii::t('app', 'Box Color'),
        ];
    }
}

<?php

namespace unlock\modules\newsletters\models;

use Yii;
use yii\helpers\ArrayHelper;
use unlock\modules\core\helpers\CommonHelper;

/**
 * This is the model class for table "{{%newsletters}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $subject
 * @property string $publish_date
 * @property string $feature_url
 * @property string $feature_excerpt
 * @property string $feature_img_url
 * @property string $stats_title
 * @property string $stats_title_1
 * @property string $stats_amount_1
 * @property string $stats_day_1
 * @property string $stats_percent_1
 * @property string $stats_year_1
 * @property string $stats_title_2
 * @property string $stats_amount_2
 * @property string $stats_day_2
 * @property string $stats_percent_2
 * @property string $stats_year_2
 * @property integer $stats_title_3
 * @property integer $stats_amount_3
 * @property integer $stats_day_3
 * @property integer $stats_percent_3
 * @property integer $stats_year_3
 * @property integer $stats_tweet
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property integer $status
 */
class Newsletters extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%newsletters}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['publish_date', 'created_at', 'updated_at'], 'safe'],
            [['feature_excerpt', 'stats_tweet'], 'string'],
            [['publish_date', 'subject'], 'required'],
            [['created_by', 'updated_by', 'status'], 'integer'],
            [['name', 'stats_title'], 'string', 'max' => 100],
            [['subject', 'feature_url', 'feature_img_url'], 'string', 'max' => 255],
            [['ln_amount', 'ln_percent', 'sn_amount', 'sn_percent', 'tv_percent', 'tv_quarter', 'pc_quarter', 'pc_percent'], 'string', 'max' => 80],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {

        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'File Name'),
            'subject' => Yii::t('app', 'Subject Line'),
            'publish_date' => Yii::t('app', 'Publish Date'),
            'feature_url' => Yii::t('app', 'Feature URL'),
            'feature_excerpt' => Yii::t('app', 'Feature Excerpt'),
            'feature_img_url' => Yii::t('app', 'Feature Image URL'),
            'stats_title' => Yii::t('app', 'Stats Title'),
            'ln_amount' => Yii::t('app', 'Amount'),
            'ln_percent' => Yii::t('app', 'Percent'),
            'sn_amount' => Yii::t('app', 'Amount'),
            'sn_percent' => Yii::t('app', 'Percent'),
            'tv_percent' => Yii::t('app', 'Percent'),
            'tv_quarter' => Yii::t('app', 'Quarter'),
            'pc_quarter' => Yii::t('app', 'Quarter'),
            'pc_percent' => Yii::t('app', 'Percent'),
            'stats_tweet' => Yii::t('app', 'Stats Tweet'),
            'created_at' => Yii::t('app', 'Created Date'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'status' => Yii::t('app', 'Status'),
        ];
    }


    public function  getNewslettersNews(){
        return $this->hasMany(NewslettersNews::className(), ['newsletter_id' => 'id']);
    }
     public function  getNewslettersInvest(){
        return $this->hasMany(NewslettersInvestToday::className(), ['newsletter_id' => 'id']);
    }
    public function  getNewslettersComingsoon(){
        return $this->hasMany(NewslettersComingSoon::className(), ['newsletter_id' => 'id']);
    }
    public function  getNewslettersFeatureresale(){
        return $this->hasMany(NewslettersFeatureResale::className(), ['newsletter_id' => 'id']);
    }
    public function  getNewslettersFooter(){
        return $this->hasMany(NewslettersFooter::className(), ['newsletter_id' => 'id']);
    }


    /** Get All Active Newsletters Drop Down List*/
    public static function newslettersDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }
}

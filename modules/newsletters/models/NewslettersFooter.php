<?php

namespace unlock\modules\newsletters\models;

use Yii;
use yii\helpers\ArrayHelper;
use unlock\modules\core\helpers\CommonHelper;

/**
 * This is the model class for table "{{%newsletters_footer}}".
 *
 * @property integer $id
 * @property string $title
 */
class NewslettersFooter extends \yii\db\ActiveRecord
{

    public $footerid;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%newsletters_footer}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['newsletter_id'], 'integer'],
            [['note'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'newsletter_id' => Yii::t('app', 'Newsletter ID'),
            'note' => Yii::t('app', 'Note'),
        ];
    }
}

<?php

use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\widgets\DetailView;
use unlock\modules\core\buttons\BackButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $model unlock\modules\newsletters\models\Newsletters */

$this->title = Yii::t('app', 'View Newsletters');
?>
<?php
//$filename =date('Y-M-D')."-Newsletter.html";
$modelInTheNews = CommonHelper::newsletterInTheNews($model->id);
$modelInInvestToday = CommonHelper::newsletterInvestToday($model->id);
$modelComingSoon = CommonHelper::newsletterComingSoon($model->id);
$modelFeatureResale = CommonHelper::newsletterFeatureResale($model->id);
$optionalFooter = CommonHelper::newsletterFooterListById($model->id);
$webliveUrl = Yii::getAlias('@webliveUrl');

$feature_img_url=$date=$stats_tweet_link=$stats_tweet=$feature_excerpt=$feature_pageLink=$state_title=$sates_disclaimer=$ln_amount=$ln_percent=$sn_amount=$sn_percent=$tv_percent=$tv_quarter=$pc_quarter=$pc_percent='';
if (!empty($model)){
    $feature_img_url=$model->feature_img_url;
    $feature_excerpt=$model->feature_excerpt;
    $feature_pageLink=$model->feature_url;
    $state_title=$model->stats_title;
    $ln_amount=$model->ln_amount;
    $ln_percent=$model->ln_percent;
    $sn_amount=$model->sn_amount;
    $sn_percent=$model->sn_percent;
    $tv_percent=$model->tv_percent;
    $tv_quarter=$model->tv_quarter;
    $pc_quarter=$model->pc_quarter;
    $pc_percent=$model->pc_percent;

    $sates_disclaimer="*Based on TREB data,".' '.date("Y");
    $stats_tweet=$model->stats_tweet;
    $stats_tweet_link='https://twitter.com/intent/tweet?'. http_build_query(array(
            'text' =>$stats_tweet
        ));
    $date=strtoupper (date('l',strtotime($model->publish_date))).' '.date('M', strtotime($model->publish_date)).' '.date('j', strtotime($model->publish_date)).'<sup>th</sup>'.''.' '.date('Y', strtotime($model->publish_date));
}

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="format-detection" content="telephone=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link href="https://fonts.googleapis.com/css?family=Rajdhani:600,700" rel="stylesheet">
    <title>Connect asset management</title>
    <style type="text/css">
        body {
            -webkit-text-size-adjust: 100% !important;
            -ms-text-size-adjust: 100% !important;
            -webkit-font-smoothing: antialiased !important;
        }
        img {
            border: 0 !important;
            outline: none !important;
        }
        p {
            Margin: 0px !important;
            padding: 0px !important;
        }
        table {
            border-collapse: collapse;
            mso-table-lspace: 0px;
            mso-table-rspace: 0px;
        }
        td {
            border-collapse: collapse;
            mso-line-height-rule: exactly;
        }
        a {
            border-collapse: collapse;
            mso-line-height-rule: exactly;
        }
        span {
            border-collapse: collapse;
            mso-line-height-rule: exactly;
        }
        .ExternalClass * {
            line-height: 100%;
        }
        span.MsoHyperlink {
            mso-style-priority: 99;
            color: inherit;
        }
        span.MsoHyperlinkFollowed {
            mso-style-priority: 99;
            color: inherit;
        }
        .em_defaultlink a {
            color: inherit !important;
            text-decoration: none !important;
        }
        .em_white a {
            color: #ffffff;
            text-decoration: none;
        }
        .em_blue1 a {
            color: #2c3e50;
            text-decoration: none;
        }
        .em_blue2 a {
            color: #4777bb;
            text-decoration: none;
        }
        .em_blue3 a {
            color: #2f1d4f;
            text-decoration: none;
        }
        .em_pink a {
            color: #eb47c7;
            text-decoration: none;
        }
        .em_grey a {
            color: #808080;
            text-decoration: none;
        }
        .em_lightblue a {
            color: #4777bb;
            text-decoration: none;
        }
        .em_pink1 a {
            color: #f16a5f;
            text-decoration: none;
        }
        .em_grey1 a {
            color: #666666;
            text-decoration: none;
        }
        .em_pink2 a {
            color: #d436b1;
            text-decoration: none;
        }

        @media only screen and (max-width:480px) {
            .em_wrapper {
                width: 100% !important;
            }
            .em_main_table {
                width: 100% !important;
            }
            .em_hide {
                display: none !important;
            }
            .em_full_img {
                width: 100% !important;
                height: auto !important;
            }
            .em_img200 {
                width: 200px !important;
                height: auto !important;
            }
            .em_side {
                width: 10px !important;
            }
            .em_center {
                text-align: center !important;
            }
            .em_center1 {
                text-align: center !important;
                font-size: 18px !important;
                line-height: 20px !important;
            }
            .em_spc_20 {
                height: 20px !important;
            }
            .em_gap_bottom {
                padding-bottom: 20px !important;
            }
            .em_text2 {
                font-size: 14px !important;
                line-height: 16px !important;
            }
            .em_text2_1 {
                font-size: 17px !important;
                line-height: 20px !important;
            }
            .em_text3 {
                font-size: 24px !important;
                line-height: 26px !important;
            }
            .em_text4 {
                font-size: 24px !important;
                line-height: 26px !important;
                text-align: center !important;
            }
            .em_br {
                display: block !important;
            }
            .em_font1 {
                font-size: 18px !important;
                line-height: 20px !important;
            }
            .em_bg {
                background: none !important;
            }
            .em_auto {
                height: auto !important;
            }
        }

        @media only screen and (min-width:481px) and (max-width:619px) {
            .em_wrapper {
                width: 100% !important;
            }
            .em_main_table {
                width: 100% !important;
            }
            .em_hide {
                display: none !important;
            }
            .em_full_img {
                width: 100% !important;
                height: auto !important;
            }
            .em_side {
                width: 10px !important;
            }
            .em_center {
                text-align: center !important;
            }
            .em_center1 {
                text-align: center !important;
                font-size: 20px !important;
                line-height: 22px !important;
            }
            .em_spc_20 {
                height: 20px !important;
            }
            .em_spc_51 {
                height: 64px !important;
            }
            .em_gap_bottom {
                padding-bottom: 20px !important;
            }
            .em_br {
                display: block !important;
            }
            .em_font1 {
                font-size: 20px !important;
                line-height: 22px !important;
            }
            .em_bg {
                background: none !important;
            }
            .em_text4 {
                text-align: center !important;
            }
            .em_auto {
                height: auto !important;
            }
        }
    </style>
    <!--[if gte mso 9]><xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <style type="text/css">
        body, table, td, span {
            font-family: Arial, sans-serif !important;
        }
    </style>
    <![endif]-->

</head>
<body style="margin:0px; padding:0px;" bgcolor="#ffffff">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff">
    <tr>
        <td valign="top" align="center" bgcolor="#dee5ef"><table width="620" border="0" cellspacing="0" cellpadding="0" align="center" style="table-layout:fixed;" class="em_main_table">
                <tr>
                    <td style="line-height:0px; font-size:0px;" width="620" class="em_hide" bgcolor="#dee5ef"><img src="<?php echo $webliveUrl;?>/newsletter/common/spacer.gif" height="1" width="620" style="max-height:1px; min-height:1px; display:block; width:620px; min-width:620px;" border="0" alt="" /></td>
                </tr>
                <tr>
                    <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="10">&nbsp;</td>
                                <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr>
                                            <td height="8" style="line-height:1px; font-size:1px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                    <tr>
                                                        <td valign="top"><table width="300" border="0" cellspacing="0" cellpadding="0" align="left" class="em_wrapper">
                                                                <tr>
                                                                    <td class="em_center" align="left" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:12px; line-height:20px; color:#2c3e50;"><span class="em_blue1">CONNECT asset management Newsletter</span></td>
                                                                </tr>
                                                            </table>
                                                            <table width="150" border="0" cellspacing="0" cellpadding="0" align="right" class="em_wrapper">
                                                                <tr>
                                                                    <td class="em_center" align="right" valign="top" style="font-family:'Open Sans', Arial, sans-serif; font-size:12px; line-height:20px; color:#2c3e50;"><span class="em_blue1"><a href="#" target="_blank" style="text-decoration:underline; color:#2c3e50;" class="inf-track-no">View online</a></span></td>
                                                                </tr>
                                                            </table></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                        <tr>
                                            <td height="10" style="line-height:1px; font-size:1px;">&nbsp;</td>
                                        </tr>
                                    </table></td>
                                <td width="10">&nbsp;</td>
                            </tr>
                        </table></td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td valign="top" align="center"><!--Main header-->

            <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" style="table-layout:fixed;" class="em_main_table">
                <tr>
                    <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="10">&nbsp;</td>
                                <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr>
                                            <td height="30" class="em_spc_20">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                    <tr>
                                                        <td valign="top"><table width="258" border="0" cellspacing="0" cellpadding="0" align="left" class="em_wrapper">
                                                                <tr>
                                                                    <td align="center" valign="top" class="em_gap_bottom"><a href="<?php echo $webliveUrl;?>" target="_blank" style="text-decoration:none;" class="inf-track-46582"><img src="<?php echo $webliveUrl; ?>/newsletter/common/logo.jpg" width="258" height="70" alt="CONNECT asset management Plan | Invest | Retire" style="display:block; font-family:Arial, sans-serif; font-size:18px; line-height:23px; color:#000000;" border="0" /></a></td>
                                                                </tr>
                                                            </table>
                                                            <table width="300" border="0" cellspacing="0" cellpadding="0" align="right" class="em_wrapper">
                                                                <tr>
                                                                    <td align="center" valign="top"><table width="300" border="0" cellspacing="0" cellpadding="0" align="center">
                                                                            <tr>
                                                                                <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="right">
                                                                                        <tr>
                                                                                            <td class="em_blue1" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:30px; line-height:30px; color:#2c3e50;"> STAY CONNECTED</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td height="10" class="em_spc_20">&nbsp;</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="center"><table border="0" cellspacing="0" cellpadding="0" align="center">
                                                                                                    <tr>
                                                                                                        <td align="right" valign="top"><a href="https://www.facebook.com/connectasset?__hstc=224915397.e15e5da4fdd2e607da7b84ae8931ab71.1473658871234.1473658871234.1473658871235.2&__hssc=224915397.1.1473658871235&__hsfp=3487301858" target="_blank" style="text-decoration:none;" class="inf-track-46584"><img src="<?php echo $webliveUrl; ?>/newsletter/common/fb.jpg" width="34" height="34" alt="FaceBook" style="display:block; font-family:Arial, sans-serif; font-size:15px; line-height:34px; color:#2b3990; text-align:center;" border="0" /></a></td>
                                                                                                        <td width="10">&nbsp;</td>
                                                                                                        <td align="center" valign="top"><a href="https://twitter.com/connectasset?__hstc=224915397.e15e5da4fdd2e607da7b84ae8931ab71.1473658871234.1473658871235.1473667521956.3&__hssc=224915397.1.1473667521956&__hsfp=3487301858" target="_blank" style="text-decoration:none;" class="inf-track-46586"><img src="<?php echo $webliveUrl; ?>/newsletter/common/tweet.jpg" width="34" height="34" alt="Twitter" style="display:block; font-family:Arial, sans-serif; font-size:15px; line-height:34px; color:#27aae1; text-align:center;" border="0" /></a></td>
                                                                                                        <td width="10">&nbsp;</td>
                                                                                                        <td align="center" valign="top"><a href="https://www.instagram.com/connectviews/" target="_blank" style="text-decoration:none;" class="inf-track-46588"><img src="<?php echo $webliveUrl; ?>/newsletter/common/insta.jpg" width="34" height="34" alt="Instagram" style="display:block; font-family:Arial, sans-serif; font-size:15px; line-height:34px; color:#6f90aa; text-align:center;" border="0" /></a></td>
                                                                                                        <td width="10">&nbsp;</td>
                                                                                                        <td align="center" valign="top"><a href="https://plus.google.com/112900440057904120654/posts?cfem=1" target="_blank" style="text-decoration:none;" class="inf-track-46590"><img src="<?php echo $webliveUrl; ?>/newsletter/common/g+.jpg" width="34" height="34" alt="Google+" style="display:block; font-family:Arial, sans-serif; font-size:15px; line-height:34px; color:#e14a35; text-align:center;" border="0" /></a></td>
                                                                                                        <td width="10">&nbsp;</td>
                                                                                                        <td align="center" valign="top"><a href="https://www.youtube.com/channel/UCblpp0AhYsyBnYS8cac3rIA" target="_blank" style="text-decoration:none;" class="inf-track-46592"><img src="<?php echo $webliveUrl; ?>/newsletter/common/yt.jpg" width="34" height="34" alt="Youtube" style="display:block; font-family:Arial, sans-serif; font-size:15px; line-height:34px; color:#ed1c24; text-align:center;" border="0" /></a></td>
                                                                                                    </tr>
                                                                                                </table></td>
                                                                                        </tr>
                                                                                    </table></td>
                                                                                <td width="5"></td>
                                                                                <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                                                        <tr>
                                                                                            <td height="20" style="line-height:1px; font-size:1px;">&nbsp;</td>
                                                                                        </tr>
                                                                                    </table></td>
                                                                            </tr>
                                                                        </table></td>
                                                                </tr>
                                                            </table></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                        <tr>
                                            <td height="20" class="em_spc_20">&nbsp;</td>
                                        </tr>
                                    </table></td>
                                <td width="10">&nbsp;</td>
                            </tr>
                        </table></td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td valign="top" align="center"><table width="620" border="0" cellspacing="0" cellpadding="0" align="center" style="table-layout:fixed;" class="em_main_table">
                <tr>
                    <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="10">&nbsp;</td>
                                <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr>
                                            <td class="em_text2 em_center em_blue2" align="left" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:15px; line-height:20px; color:#4777bb; font-weight:bold;"><a href="<?php echo $webliveUrl; ?>" target="_blank" style="text-decoration:none; color:#4777bb;" class="inf-track-46594"></a><?php echo $date; ?></td>
                                        </tr>
                                        <tr>
                                            <td height="20" style="line-height:0px; font-size:0px;"><img src="<?php echo $webliveUrl; ?>/newsletter/common/spacer.gif" width="1" height="1" alt=" " style="display:block;" border="0" /></td>
                                        </tr>
                                    </table></td>
                                <td width="10">&nbsp;</td>
                            </tr>
                        </table></td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td valign="top" align="center" bgcolor="#f9f9f9"><table width="620" border="0" cellspacing="0" cellpadding="0" align="center" style="table-layout:fixed;" class="em_main_table">
                <tr>
                    <td height="45" class="em_spc_20">&nbsp;</td>
                </tr>
                <tr>
                    <td class="em_font1" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:24px; line-height:26px; color:#2c3e50; font-weight:bold;"><span class="em_blue1">CONNECT BLOG</span></td>
                </tr>
                <tr>
                    <td height="10" style="line-height:1px; font-size:1px;">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" valign="top"><table width="70" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#2c3e50">
                            <tr>
                                <td height="3" style="line-height:0px; font-size:0px;"><img src="<?php echo $webliveUrl; ?>/newsletter/common/spacer.gif" width="1" height="1" alt=" " style="display:block;" border="0" /></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="40" class="em_spc_20">&nbsp;</td>
                </tr>

                <tr>
                    <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                            <tr>
                                <td valign="top"><table width="500" border="0" cellspacing="0" cellpadding="0" align="center" class="em_wrapper">
                                        <tr>
                                            <td align="center" valign="top" class="em_full_img"><a
                                                        href="<?php echo  $feature_pageLink; ?>" target="_blank" style="text-decoration:underline; color:#4777bb;" class="inf-track-46600"><img
                                                            src="<?php echo $feature_img_url;?>" width="620" height="500"
                                                            alt="Feature Post" style="display:block; font-family:Arial, sans-serif; font-size:17px; line-height:20px; color:#000000;max-width:620px;"  class="em_full_img" border="0" /></a></td>
                                        </tr>
                                        <tr>
                                            <td height="20" style="line-height:1px; font-size:1px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="em_center1" align="justify" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:20px; line-height:26px; color:##4677BA;"><!--excerpt-->

                                                <!--In the news block-->
                                                <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" style="table-layout:fixed;" class="em_main_table">
                                                    <tr>
                                                        <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td width="10">&nbsp;</td>
                                                                    <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                                            <tr>
                                                                                <td valign="top" align="center"><?php echo  $feature_excerpt; ?><a href="<?php  echo  $feature_pageLink; ?>" target="_blank" style="text-decoration:underline; color:#4777bb;"><br/>Read on</a></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td height="25" class="em_spc_20">&nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="em_font1" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:24px; line-height:26px; color:#2c3e50; font-weight:bold;"><span class="em_blue1">IN THE NEWS</span></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td height="10" style="line-height:1px; font-size:1px;">&nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center" valign="top"><table width="70" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#2c3e50">
                                                                                        <tr>
                                                                                            <td height="3" style="line-height:0px; font-size:0px;"><img src="<?php echo $webliveUrl; ?>/newsletter/common/spacer.gif" width="1" height="1" alt=" " style="display:block;" border="0" /></td>
                                                                                        </tr>
                                                                                    </table></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td height="40" class="em_spc_20">&nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center" valign="top">
                                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                                                        <tr>
                                                                                            <td valign="top"><!--News article #1-->
                                                                                                <?php $i=1; foreach ($modelInTheNews as $news) { ?>
                                                                                                    <table width="290" border="0" cellspacing="0" cellpadding="0" align="left" class="em_wrapper" style="<?php if($i == 1){ echo 'margin-right:19px;';} ?>">
                                                                                                        <tr>
                                                                                                            <td align="center" valign="top"><a href="<?php echo $news->url; ?>" target="_blank" style="text-decoration:underline; color:#4777bb;" class="inf-track-46604"><img src="<?php echo $news->img_url;?>" width="290" height="150" alt="In The News <?php echo $i;?>" style="display:block;" border="0" /></a></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td height="20" style="line-height:1px; font-size:1px;">&nbsp;</td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="em_grey1" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:18px; line-height:24px; color:#4777bb;height:72px;padding-bottom: 15px;    text-align: left;"><!--title-->
                                                                                                                <?php echo $news->title;?></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="em_center" align="left" valign="top" style="height:96px;font-family: 'Open Sans', Arial, sans-serif; font-size:15px; line-height:24px; color:#808080;"><span class="em_grey">
                                                                                                        <?php
                                                                                                        echo  substr($news->excerpt,0,150);
                                                                                                        ?>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td height="20" style="line-height:1px; font-size:1px;">&nbsp;</td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="em_center" align="left" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:14px; line-height:17px; color:#4777bb; font-weight:bold; text-decoration:underline;"><span class="em_lightblue"><a href="<?php echo $news->url; ?>" target="_blank" style="text-decoration:underline; color:#4777bb;" class="inf-track-46606">Read More</a></span></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td height="45" class="em_spc_20">&nbsp;</td>
                                                                                                        </tr>
                                                                                                    </table>

                                                                                                    <!--[if gte mso 9]>
                                                                                                    </td>
                                                                                                    <td valign="top" >
                                                                                                    <![endif]-->


                                                                                                    <?php  $i++;} ?>
                                                                                            </td>

                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>

                                                                        </table></td>
                                                                    <td width="10">&nbsp;</td>
                                                                </tr>
                                                            </table></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>


                <tr>
                    <td valign="top" align="center"><!--C01 District stat block-->

                        <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" style="table-layout:fixed;" class="em_main_table">
                            <tr>
                                <td width="10">&nbsp;</td>
                                <td valign="top" align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="em_clr1" vlign="top" align="center" style="font-family:Helvetica, Arial, sans-serif; font-size:22px; line-height:28px; color:#4c4c4c; font-weight:bold;"> <?php echo $state_title; ?></td>
                                        </tr>
                                        <tr>
                                            <td background="<?php echo $webliveUrl; ?>/newsletter/common/blueregion.jpg" bgcolor="#ffffff" style="background-repeat:no-repeat; background-position:center top;" height="300" valign="top">

                                                <!--[if gte mso 9]>
                                                <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:600px;height:300px;">
                                                    <v:fill type="tile" src="<?php echo $webliveUrl; ?>/newsletter/common/blueregion.jpg" color="#ffffff" />
                                                    <v:textbox inset="0,0,0,0">
                                                <![endif]-->


                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td valign="top"><!--Average Lease-->

                                                            <table width="550" align="center" border="0" cellspacing="0" cellpadding="0" class="em_wrapper">
                                                                <tr>
                                                                    <td valign="top">
                                                                        <table align="left" class="em_wrapper" border="0" cellspacing="0" cellpadding="0" width="175">
                                                                            <tr>
                                                                                <td height="38">&nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="em_clr1" valign="middle" height="40" align="center" style="font-family:Helvetica, Arial, sans-serif; font-size:18px; line-height:20px; color:#4c4c4c; font-weight:bold;"><!--title-->
                                                                                    AVERAGE LEASE</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td height="16" style="font-size:0px; line-height:0px;">&nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="em_clr1" valign="top" align="center" style="font-family:Helvetica, Arial, sans-serif; font-size:35px; line-height:35px; color:#4c4c4c; font-weight:bold;"><?php echo $ln_amount;?><br />
                                                                                    <span style="font-size:16px;">/PAST 14 DAYS</span></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td height="5" class="em_spc_20" style="font-size:0px; line-height:0px;">&nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="em_green" valign="top" align="center" style="font-family:Helvetica, Arial, sans-serif; font-size:16px; line-height:20px; color:#2cb132; font-weight:bold;"><?php echo $ln_percent;?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td height="10" style="font-size:0px; line-height:0px;">&nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="em_blue" valign="top" align="center" style="font-family:Helvetica, Arial, sans-serif; font-size:18px; line-height:20px; color:#4b79ba; font-weight:bold;"> year over year </td>
                                                                            </tr>
                                                                        </table>

                                                                        <!--[if gte mso 9]>
                                                                        </td>
                                                                        <td valign="top" >
                                                                        <![endif]-->

                                                                        <table width="12" align="left" class="em_hide" border="0" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                            <tr>
                                                                                <td width="12"></td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>

                                                                        <!--[if gte mso 9]>
                                                                        </td>

                                                                        <td valign="top" >
                                                                        <![endif]-->
                                                                        <table align="right" class="em_wrapper" border="0" cellspacing="0" cellpadding="0" width="363">
                                                                            <tr>
                                                                                <td valign="top"><table align="left" class="em_wrapper" border="0" cellspacing="0" cellpadding="0" width="175">
                                                                                        <tr>
                                                                                            <td height="38">&nbsp;</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="em_clr1" valign="middle" height="40" align="center" style="font-family:Helvetica, Arial, sans-serif; font-size:18px; line-height:20px; color:#4c4c4c; font-weight:bold;"> AVERAGE SOLD </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td height="16" style="font-size:0px; line-height:0px;">&nbsp;</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td height="70" class="em_clr1" valign="top" align="center" style="font-family:Helvetica, Arial, sans-serif; font-size:35px; line-height:35px; color:#4c4c4c; font-weight:bold;"><?php echo $sn_amount;?><br/>
                                                                                                <span style="font-size:16px;">/PAST 30 DAYS</span></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td height="4" style="font-size:0px; line-height:0px;"><img src="<?php echo $webliveUrl; ?>/newsletter/common/spacer.gif" width="1" height="1" alt=" " style="display:block;" border="0" /></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="em_green" valign="top" align="center" style="font-family:Helvetica, Arial, sans-serif; font-size:16px; line-height:20px; color:#2cb132; font-weight:bold;"><?php echo $sn_percent;?> </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td height="10" style="font-size:0px; line-height:0px;">&nbsp;</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="em_blue" valign="top" align="center" style="font-family:Helvetica, Arial, sans-serif; font-size:
                                                18px; line-height:20px; color:#4b79ba; font-weight:bold;">year over year</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    <table width="13" align="left" class="em_hide" border="0" cellspacing="0" cellpadding="0">
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td width="10"></td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                    <!--[if gte mso 9]>    </td>
                                                                                    <td valign="top" >
                                                                                    <![endif]-->
                                                                                    <table align="right" class="em_wrapper" border="0" cellspacing="0" cellpadding="0" width="175">
                                                                                        <tr>
                                                                                            <td height="38">&nbsp;</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="em_clr1" valign="top" align="center" style="font-family:Helvetica, Arial, sans-serif; font-size:18px; line-height:20px; color:#4c4c4c; font-weight:bold;"> TORONTO <br />
                                                                                                VACANCY RATE </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td height="16" style="font-size:0px; line-height:0px;">&nbsp;</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="em_clr1" valign="top" align="center" style="font-family:Helvetica, Arial, sans-serif; font-size:35px; line-height:35px; color:#4c4c4c; font-weight:bold;"><?php echo $tv_percent;?> <br/>
                                                                                                <span style="font-size: 16px;">in <?php echo $tv_quarter;?></span></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td height="5" class="em_spc_20" style="font-size:0px; line-height:0px;">&nbsp;</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="em_green" valign="top" align="center" style="font-family:Helvetica, Arial, sans-serif; font-size:16px; line-height:20px; color:#2cb132; font-weight:bold;"> <?php echo $pc_percent;?> in <?php echo $pc_quarter;?></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td height="10" style="font-size:0px; line-height:0px;">&nbsp;</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="em_blue" valign="top" align="center" style="font-family:Helvetica, Arial, sans-serif; font-size:18px; line-height:20px; color:#4b79ba; font-weight:bold;"> year over year </td>
                                                                                        </tr>
                                                                                    </table></td>
                                                                            </tr>
                                                                        </table></td>
                                                                </tr>
                                                            </table></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="27" class="em_spc_20" style="font-size:0px; line-height:0px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" align="center" style="font-family:Helvetica, Arial, sans-serif; font-size:8px; line-height:10px; color:#494949"> <?php echo $sates_disclaimer; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10" style="line-height:1px; font-size:1px;" class="em_spc_20">&nbsp;</td>
                                                    </tr>
                                                </table>
                                                <!--[if gte mso 9]>
                                                </v:textbox>
                                                </v:rect>
                                                <![endif]--></td>
                                        </tr>
                                    </table></td>
                                <td width="10">&nbsp;</td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td valign="top" align="center" bgcolor="#00aced"><!--Twitter tweet-->

                        <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" style="table-layout:fixed;" class="em_main_table">
                            <tr>
                                <td width="10">&nbsp;</td>
                                <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td height="20">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="center"><img src="<?php echo $webliveUrl; ?>/newsletter/common/unnamed.png" width="202" height="28" alt="Twitter Tweet" border="0" style="display:block;" /></td>
                                        </tr>
                                        <tr>
                                            <td height="20">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="center" style="font-family:Arial, sans-serif; font-size:16px; line-height:30px; color:#ffffff; letter-spacing:1px;">   <?php echo $stats_tweet; ?></td>
                                        </tr>
                                        <tr>
                                            <td height="20">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="center"><table align="center" width="240" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td height="51" align="center" bgcolor="#000000" valign="middle" style="font-family: 'Open Sans', Arial, sans-serif; font-size:14px; color:#ffffff; font-weight:bold; text-transform:uppercase;"><a href="<?php echo $stats_tweet_link;?>" target="_blank" style="text-decoration:none; color:#ffffff; line-height:51px; display:block;" class="inf-track-46612">TWEET THIS</a></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                        <tr>
                                            <td height="20">&nbsp;</td>
                                        </tr>
                                    </table></td>
                                <td width="10">&nbsp;</td>
                            </tr>
                        </table></td>
                </tr>

                <tr>
                    <td valign="top" align="center"  bgcolor="#deecff"><!--invest today -->

                        <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" style="table-layout:fixed;" class="em_main_table">
                            <tr>
                                <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="10">&nbsp;</td>
                                            <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                    <tr>
                                                        <td height="45" class="em_spc_20">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="em_font1" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:24px; line-height:26px; color:#2c3e50; font-weight:bold;"><span class="em_blue1">INVEST TODAY</span></td>
                                                    </tr>

                                                    <tr>
                                                        <td height="10" style="line-height:1px; font-size:1px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" valign="top"><table width="70" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#2c3e50">
                                                                <tr>
                                                                    <td height="3" style="line-height:0px; font-size:0px;"><img src="<?php echo $webliveUrl; ?>/newsletter/common/spacer.gif" width="1" height="1" alt=" " style="display:block;" border="0" /></td>
                                                                </tr>
                                                            </table></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="40" class="em_spc_20">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                                <tr>
                                                                    <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="left" class="em_wrapper">
                                                                            <tr>
                                                                                <td valign="top"><!--coming soon 1-->
                                                                                    <?php $j=1; foreach ($modelInInvestToday as $InvestToday) { $developmentList = CommonHelper::developmentListById($InvestToday->development_id); ?>
                                                                                        <table width="190" border="0" cellspacing="0" cellpadding="0" align="left" class="em_wrapper" style="<?php if($j == 1){ echo 'margin-right:15px;';}elseif ($j==2){echo 'margin-right:15px;';} ?>">
                                                                                            <tr>
                                                                                                <td align="center" valign="top"><img
                                                                                                            src="<?php echo  $InvestToday->img_url;?>" width="190" height="190" alt="Invest Today <?php echo $j;?>" style="display:block; font-family:Arial, sans-serif; font-size:20px; line-height:190px; color:#000000; text-align:center;" border="0" /></td>
                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td style="line-height:1px; height:20px; font-size:1px;">&nbsp;</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="em_grey1" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:22px; line-height:24px; color:#666666;height:60px"><!--title-->
                                                                                                    <?php echo $InvestToday->title;?></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="em_grey" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:15px; line-height:24px; color:#808080;height:170px;margin-bottom:5px"><!--excerpt-->
                                                                                                    <?php echo substr($InvestToday->excerpt,0,150);?></td>
                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td align="center" valign="top"><table width="140" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#0093b9">
                                                                                                        <tr>
                                                                                                            <td class="em_white" align="center" valign="middle" height="44" style="font-family: 'Open Sans', Arial, sans-serif; font-size:14px; color:#ffffff; font-weight:bold; text-transform:uppercase;"><a href="<?php if (!empty($developmentList->development_url)) {echo$developmentList->development_url;}else{echo "#";}  ?>" target="_blank" style="text-decoration:none; color:#ffffff; display:block; line-height:44px;" class="inf-track-46614">LEARN MORE</a></td>
                                                                                                        </tr>
                                                                                                    </table></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td height="50" class="em_spc_20">&nbsp;</td>
                                                                                            </tr>
                                                                                        </table>

                                                                                        <!--[if gte mso 9]>
                                                                                        </td>
                                                                                        <td valign="top" >
                                                                                        <![endif]-->
                                                                                        <?php $j++;}?>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>

                                                                </tr>
                                                            </table></td>
                                                    </tr>
                                                </table></td>
                                            <td width="10">&nbsp;</td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>

                <tr>
                    <td valign="top" align="center" bgcolor="#f9f9f9"><!--coming soon-->

                        <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" style="table-layout:fixed;" class="em_main_table">
                            <tr>
                                <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="10">&nbsp;</td>
                                            <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                    <tr>
                                                        <td height="45" class="em_spc_20">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="em_font1" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:24px; line-height:26px; color:#2c3e50; font-weight:bold;"><span class="em_blue1">COMING SOON</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10" style="line-height:1px; font-size:1px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" valign="top"><table width="70" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#2c3e50">
                                                                <tr>
                                                                    <td height="3" style="line-height:0px; font-size:0px;"><img src="<?php echo $webliveUrl; ?>/newsletter/common/spacer.gif" width="1" height="1" alt=" " style="display:block;" border="0" /></td>
                                                                </tr>
                                                            </table></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="40" class="em_spc_20">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                                <tr>
                                                                    <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="left" class="em_wrapper">
                                                                            <tr>
                                                                                <td valign="top"><!--coming soon 1-->
                                                                                    <?php $k=1; foreach ($modelComingSoon as $ComingSoon) {$developmentList = CommonHelper::developmentListById($ComingSoon->development_id); ?>
                                                                                        <table width="190" border="0" cellspacing="0" cellpadding="0" align="left" class="em_wrapper" style="<?php if($k == 1){ echo 'margin-right:15px;';}elseif ($k==2){echo 'margin-right:15px;';} ?>">
                                                                                            <tr>
                                                                                                <td align="center" valign="top"><img src="<?php echo  $ComingSoon->img_url;?>" width="190" height="190" alt="Coming Soon <?php echo $k;?>" style="display:block; font-family:Arial, sans-serif; font-size:20px; line-height:190px; color:#000000; text-align:center;" border="0" /></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td height="24" style="line-height:1px; font-size:1px;">&nbsp;</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="em_grey1" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:22px; line-height:24px; color:#666666;height:60px"><?php echo $ComingSoon->title; ?></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="em_grey" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:15px; line-height:24px; color:#808080;height:180px;margin-bottom:5px"> <?php echo substr($ComingSoon->excerpt,0,150);?></td>
                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td align="center" valign="top"><table width="140" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#2c3e50">
                                                                                                        <tr>
                                                                                                            <td class="em_white" align="center" valign="middle" height="44" style="font-family: 'Open Sans', Arial, sans-serif; font-size:14px; color:#ffffff; font-weight:bold; text-transform:uppercase;"><a href="<?php if (!empty($developmentList->development_url)) {echo$developmentList->development_url;}else{echo "#";}  ?>" target="_blank" style="text-decoration:none; color:#ffffff; display:block; line-height:44px;" class="inf-track-46620">REGISTER NOW</a></td>
                                                                                                        </tr>
                                                                                                    </table></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td height="50" class="em_spc_20">&nbsp;</td>
                                                                                            </tr>
                                                                                        </table>

                                                                                        <!--[if gte mso 9]>
                                                                                        </td>
                                                                                        <td valign="top" >
                                                                                        <![endif]-->
                                                                                        <?php $k++;}?>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>

                                                                </tr>
                                                            </table></td>
                                                    </tr>
                                                </table></td>
                                            <td width="10">&nbsp;</td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td valign="top" align="center"><!--feature resale listings-->

                        <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" style="table-layout:fixed;" class="em_main_table">
                            <tr>
                                <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="10">&nbsp;</td>
                                            <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                    <tr>
                                                        <td height="45" class="em_spc_20">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="em_font1" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:24px; line-height:26px; color:#2c3e50; font-weight:bold;"><span class="em_blue1">FEATURED RESALE LISTINGS</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10" style="line-height:1px; font-size:1px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" valign="top"><table width="70" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#2c3e50">
                                                                <tr>
                                                                    <td height="3" style="line-height:0px; font-size:0px;"><img src="<?php echo $webliveUrl; ?>/newsletter/common/spacer.gif" width="1" height="1" alt=" " style="display:block;" border="0" /></td>
                                                                </tr>
                                                            </table></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10" class="em_spc_20">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                                <tr>
                                                                    <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="left" class="em_wrapper">
                                                                            <tr>
                                                                                <td valign="top"><!--resale listing #1-->
                                                                                    <?php $l=1; foreach ($modelFeatureResale as $FeatureResale) { ?>
                                                                                        <table width="190" border="0" cellspacing="0" cellpadding="0" align="left" class="em_wrapper" style="<?php if($l == 1){ echo 'margin-right:15px;';}elseif ($l==2){echo 'margin-right:15px;';} ?>">
                                                                                            <tr>
                                                                                                <td class="em_grey1" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:16px; font-weight: bold; color:#4777bb;"><?php echo  $FeatureResale->top_title;?></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td height="10" style="line-height:1px; font-size:1px;">&nbsp;</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="center" valign="top"><a href="<?php echo $FeatureResale->url;?>" target="_blank" style="text-decoration:none; color:#ffffff; display:block; line-height:44px;" class="inf-track-46626"><img src="<?php echo  $FeatureResale->img_url.'?time='.time();?>" width="190" height="190" alt="Resale <?php echo $l;?>" style="display:block; font-family:Arial, sans-serif; font-size:20px; line-height:190px; color:#000000; text-align:center;" border="0" /></a></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td height="20" style="line-height:1px; font-size:1px;">&nbsp;</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="em_grey1" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:22px; line-height:24px; color:#666666;height:70px">
                                                                                                    <?php
                                                                                                    if (strpos($FeatureResale->title, ".") !== false) {
                                                                                                        list($part1, $part2) = explode('.', $FeatureResale->title);
                                                                                                        echo  $part1.'.';
                                                                                                        ?>
                                                                                                        <br/> <?php echo $part2; }
                                                                                                    else{
                                                                                                        echo $FeatureResale->title;
                                                                                                    }
                                                                                                    ?>
                                                                                                </td>
                                                                                            </tr>

                                                                                            <tr style="padding-right:20px;">
                                                                                                <td class="em_grey em_auto" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:15px; line-height:24px; color:#808080;height:180px;margin-bottom:5px;"><?php echo substr($FeatureResale->excerpt,0,150);?></td>
                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td align="center" valign="top"><table width="140" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#4777bb">
                                                                                                        <tr>
                                                                                                            <td class="em_white" align="center" valign="middle" height="44" style="font-family: 'Open Sans', Arial, sans-serif; font-size:14px; color:#ffffff; font-weight:bold; text-transform:uppercase;"><a href="<?php echo  $FeatureResale->url;?>" target="_blank" style="text-decoration:none; color:#ffffff; display:block; line-height:44px;" class="inf-track-46628">LEARN MORE</a></td>
                                                                                                        </tr>
                                                                                                    </table></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td height="50" class="em_spc_20">&nbsp;</td>
                                                                                            </tr>
                                                                                        </table>

                                                                                        <!--[if gte mso 9]>
                                                                                        </td>
                                                                                        <td valign="top" >
                                                                                        <![endif]-->
                                                                                        <?php $l++;}?>

                                                                                </td>
                                                                            </tr>
                                                                        </table>

                                                                        <!--[if gte mso 9]>
                                                                        </td>
                                                                        <td valign="top" >
                                                                        <![endif]-->
                                                                    </td>
                                                                </tr>
                                                            </table></td>
                                                    </tr>
                                                </table></td>
                                            <td width="10">&nbsp;</td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td valign="top" align="center" bgcolor="#c9f4ff"><!--ROI calculator-->

                        <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" style="table-layout:fixed;" class="em_main_table">
                            <tr>
                                <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="10">&nbsp;</td>
                                            <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                    <tr>
                                                        <td height="45" class="em_spc_20">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="em_font1" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:24px; line-height:26px; color:#2c3e50; font-weight:bold;"><span class="em_blue1">CONNECT ROI CALCULATOR</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10" style="line-height:1px; font-size:1px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" valign="top"><table width="70" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#2c3e50">
                                                                <tr>
                                                                    <td height="3" style="line-height:0px; font-size:0px;"><img src="<?php echo $webliveUrl; ?>/newsletter/common/spacer.gif" width="1" height="1" alt=" " style="display:block;" border="0" /></td>
                                                                </tr>
                                                            </table></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="30" class="em_spc_20">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" >
                                                                <tr>
                                                                    <td align="center" valign="top"><table width="300" border="0" cellspacing="0" cellpadding="0" align="right" class="em_wrapper" >
                                                                            <tr>
                                                                                <td height="42" class="em_hide">&nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="em_blue2" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:18px; line-height:26px; color:#4777bb; font-weight:bold;"> CHECK OUT WHAT<br class="em_hide" />
                                                                                    YOUR CONDO<br class="em_hide" />
                                                                                    INVESTMENT IS WORTH </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td height="25" class="em_spc_20">&nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center" valign="top"><table width="140" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#4777bb">
                                                                                        <tr>
                                                                                            <td class="em_white" align="center" valign="middle" height="44" style="font-family: 'Open Sans', Arial, sans-serif; font-size:14px; color:#ffffff; font-weight:bold; text-transform:uppercase;"><a href="<?php echo $webliveUrl; ?>/roi-calculator/" target="_blank" style="text-decoration:none; color:#ffffff; display:block; line-height:44px;" class="inf-track-46638">try it now</a></td>
                                                                                        </tr>
                                                                                    </table></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td height="2" style="line-height:1px; font-size:1px;" class="em_spc_20">&nbsp;</td>
                                                                            </tr>
                                                                        </table>


                                                                        <table width="280" border="0" cellspacing="0" cellpadding="0" align="left" class="em_wrapper" >
                                                                            <tr>
                                                                                <td align="center" valign="top"><a href="<?php echo $webliveUrl; ?>/roi-calculator/" target="_blank" style="text-decoration:none;" class="inf-track-46640"><img src="<?php echo $webliveUrl; ?>/newsletter/common/roi.jpg" width="280" height="234" alt="Return on Investment Calculator" style="display:block; font-family:Arial, sans-serif; font-size:30px; line-height:234px; color:#000000; font-weight:bold; text-align:center;" border="0" /></a></td>
                                                                            </tr>
                                                                        </table></td>
                                                                </tr>
                                                            </table></td>
                                                    </tr>
                                                </table></td>
                                            <td width="10">&nbsp;</td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
            </table></td>
    </tr>
    <!--footer-->
    <tr>
        <td valign="top"><table width="620" border="0" cellspacing="0" cellpadding="0" align="center" style="table-layout:fixed;" class="em_main_table">
                <tr>
                    <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="10">&nbsp;</td>
                                <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr>
                                            <td height="50" class="em_spc_20">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="center"><table border="0" cellspacing="0" cellpadding="0" align="center">
                                                    <tr>
                                                        <td align="right" valign="top"><a href="https://www.facebook.com/connectasset?__hstc=224915397.e15e5da4fdd2e607da7b84ae8931ab71.1473658871234.1473658871234.1473658871235.2&__hssc=224915397.1.1473658871235&__hsfp=3487301858" target="_blank" style="text-decoration:none;" class="inf-track-46584"><img src="<?php echo $webliveUrl; ?>/newsletter/common/fb.jpg" width="34" height="34" alt="FaceBook" style="display:block; font-family:Arial, sans-serif; font-size:15px; line-height:34px; color:#2b3990; text-align:center;" border="0" /></a></td>
                                                        <td width="10">&nbsp;</td>
                                                        <td align="center" valign="top"><a href="https://twitter.com/connectasset?__hstc=224915397.e15e5da4fdd2e607da7b84ae8931ab71.1473658871234.1473658871235.1473667521956.3&__hssc=224915397.1.1473667521956&__hsfp=3487301858" target="_blank" style="text-decoration:none;" class="inf-track-46586"><img src="<?php echo $webliveUrl; ?>/newsletter/common/tweet.jpg" width="34" height="34" alt="Twitter" style="display:block; font-family:Arial, sans-serif; font-size:15px; line-height:34px; color:#27aae1; text-align:center;" border="0" /></a></td>
                                                        <td width="10">&nbsp;</td>
                                                        <td align="center" valign="top"><a href="https://www.instagram.com/connectviews/" target="_blank" style="text-decoration:none;" class="inf-track-46588"><img src="<?php echo $webliveUrl; ?>/newsletter/common/insta.jpg" width="34" height="34" alt="Instagram" style="display:block; font-family:Arial, sans-serif; font-size:15px; line-height:34px; color:#6f90aa; text-align:center;" border="0" /></a></td>
                                                        <td width="10">&nbsp;</td>
                                                        <td align="center" valign="top"><a href="https://plus.google.com/112900440057904120654/posts?cfem=1" target="_blank" style="text-decoration:none;" class="inf-track-46590"><img src="<?php echo $webliveUrl; ?>/newsletter/common/g+.jpg" width="34" height="34" alt="Google+" style="display:block; font-family:Arial, sans-serif; font-size:15px; line-height:34px; color:#e14a35; text-align:center;" border="0" /></a></td>
                                                        <td width="10">&nbsp;</td>
                                                        <td align="center" valign="top"><a href="https://www.youtube.com/channel/UCblpp0AhYsyBnYS8cac3rIA" target="_blank" style="text-decoration:none;" class="inf-track-46592"><img src="<?php echo $webliveUrl; ?>/newsletter/common/yt.jpg" width="34" height="34" alt="Youtube" style="display:block; font-family:Arial, sans-serif; font-size:15px; line-height:34px; color:#ed1c24; text-align:center;" border="0" /></a></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                        <tr>
                                            <td height="10" class="em_spc_20">&nbsp;</td>
                                        </tr>
                                        <?php if (!empty($optionalFooter)){
                                            foreach ($optionalFooter as $footer){
                                                ?>
                                                <tr>
                                                    <td class="em_blue1" align="center" valign="top" style="padding-bottom: 10px;font-family: 'Open Sans', Arial, sans-serif; font-size:12px; line-height:18px; color:#2c3e50;"><?php echo  $footer->note;?><br />
                                                </tr>
                                                <?php
                                            }
                                        }
                                        else{
                                            ?>
                                            <tr>
                                                <td class="em_blue1" align="center" valign="top" style="padding-bottom: 10px;font-family: 'Open Sans', Arial, sans-serif; font-size:12px; line-height:18px; color:#2c3e50;">*5% deposit with $1,000 per month until you reach 10% total deposit.<br />
                                            </tr>
                                            <?php
                                        }?>


                                        <tr>
                                            <td height="10" class="em_spc_20">&nbsp;</td>
                                        </tr>

                                        <tr>
                                            <td class="em_blue1" align="center" valign="top" style="font-family: 'Open Sans', Arial, sans-serif; font-size:14px; line-height:18px; color:#2c3e50;">&copy; 2018 CONNECT asset management.<span class="em_br"></span> All Rights Reserved.<br />
                                                <br />
                                                <a href="<?php echo $webliveUrl; ?>/privacy-policy/" target="_blank" style="text-decoration:underline; color:#2c3e50;" class="inf-track-no">Privacy Policy</a></td>
                                        </tr>
                                        <tr>
                                            <td height="30" class="em_spc_20">&nbsp;</td>
                                        </tr>
                                    </table></td>
                                <td width="10">&nbsp;</td>
                            </tr>
                        </table></td>
                </tr>
            </table></td>
    </tr>
</table></td>
</tr>
</table></td>
</tr>
</table>
<div class="em_hide" style="white-space:nowrap; font:20px courier; color:#ffffff; background-color:#ffffff;"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </div>
</body>
</html>
<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtons;
use unlock\modules\core\buttons\BackButton;
use kartik\date\DatePicker;
use unlock\modules\developments\models\Developments;
use yii\helpers\ArrayHelper;
use kartik\color\ColorInput;

/* @var $this yii\web\View */
/* @var $model unlock\modules\newsletters\models\Newsletters */
/* @var $form yii\bootstrap\ActiveForm */

$this->title= ($model->isNewRecord) ? Yii::t('app', 'Create Newsletters') : Yii::t('app', 'Update Newsletters');
?>

<div class="main-container main-container-form" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a(Yii::t('app', 'Home'), Yii::$app->homeUrl) ?></li>
                <li><?= Html::a(Yii::t('app', 'Newsletters'), Url::to('index')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>
    <div class="admin-form-container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <div class="pull-left">
                        <h3 class="panel-title"><i class="fas fa-pencil-alt"></i> <?= Html::encode($this->title) ?></h3>
                    </div>
                    <div class="pull-right">
                        <p style="margin:0px;font-size: 14px">Version 1.0</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-heading-button">
                    <?= BackButton::widget() ?>
                </div>
            </div>
            <div class="panel-body newsletter-wrapper">
                <?php $form = ActiveForm::begin([
                    'options' => ['enctype' => 'multipart/form-data'],
                    /* 'layout' => 'horizontal',
                     'validateOnBlur' =>true,
                     'enableAjaxValidation'=>true,
                     'errorCssClass'=>'has-error',
                     'fieldConfig' => [
                         'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
                         'options' => ['class' => 'form-group'],
                         'horizontalCssClasses' => [
                             'label' => 'col-sm-2',
                             'offset' => '',
                             'wrapper' => '',
                             'hint' => '',
                         ],
                     ],*/
                ]); ?>
                <div class="row">
                    <div class="col-md-11">
                        <?= $form->field($model, 'subject')->textInput() ?>
                    </div>
                    <div class="col-md-1">
                        <?= Html::button('Copy',['id' => 'CopySubject', 'class' => 'btn btn-success']) ?>
                    </div>
                    <div class="clearfix"></div>
                    <?php if ($model->isNewRecord){ ?>
                    <div class="col-md-6">
                        <?php }else{ ?>
                        <div class="col-md-3">
                            <?php } ?>
                            <?= $form->field($model, 'publish_date')->widget(
                                DatePicker::className(), [
                                'name' => 'publish_date',
                                'value' => '12-31-2010',
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'yyyy-mm-dd'
                                ]
                            ]);?>
                        </div>
                        <?php if (!$model->isNewRecord){ ?>
                            <div class="col-md-3">
                                <?= $form->field($model, 'created_at')->textInput(['maxlength' => true,'readonly'=>true]) ?>
                            </div>
                        <?php }?>

                        <div class="col-md-6">
                            <?= $form->field($model, 'status')->dropDownList(CommonHelper::newsletterStatusTypeDropDownList()) ?>
                        </div>
                    </div>
                    <div class="divider"></div>
                    <div class="newsletter-form-title">
                        <h3>FEATURE POST</h3>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <?php
                            $webliveUrl = Yii::getAlias('@webliveUrl');
                            /*$webliveUrl ='//connectassetmanagement.com';*/

                            $videoch = curl_init($webliveUrl.'/wp-json/wp-connect/api?access_token=Y29uZG8tZGV2ZWxvcG1lbnRzVW5sb&post_type=videos&time=' . time());
                            curl_setopt($videoch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($videoch, CURLOPT_SSL_VERIFYPEER, 0);
                            curl_setopt($videoch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                            curl_setopt($videoch, CURLOPT_CONNECTTIMEOUT, 5);
                            curl_setopt($videoch, CURLOPT_TIMEOUT, 3);
                            curl_setopt($videoch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
                            $videoresult = curl_exec($videoch);
                            $videosDecode = json_decode($videoresult,true);

                            $postsch = curl_init($webliveUrl.'/wp-json/wp-connect/api?access_token=Y29uZG8tZGV2ZWxvcG1lbnRzVW5sb&post_type=post&time=' . time());
                            curl_setopt($postsch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($postsch, CURLOPT_SSL_VERIFYPEER, 0);
                            curl_setopt($postsch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                            curl_setopt($postsch, CURLOPT_CONNECTTIMEOUT, 5);
                            curl_setopt($postsch, CURLOPT_TIMEOUT, 3);
                            curl_setopt($postsch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
                            $postesult = curl_exec($postsch);
                            $postDecode = json_decode($postesult,true);


                            //$posts = file_get_contents($webliveUrl.'/wp-json/wp-connect/api?access_token=Y29uZG8tZGV2ZWxvcG1lbnRzVW5sb&post_type=post');
                            //$postsDecode = json_decode($posts,true);

                            $featureData = [];
                            if(!empty($postDecode)){
                                foreach ($postDecode as $key => $value){
                                    if($value['feature'] == '1'){
                                        $featureData[] = $value;
                                    }
                                }
                            }
                            if(!empty($videosDecode)){
                                foreach ($videosDecode as $key => $value){
                                    if($value['feature'] == '1'){
                                        $featureData[] = $value;
                                    }
                                }
                            }

                            $featureDataItems = ArrayHelper::map($featureData, 'pageLink', 'title');
                            ?>

                            <?= $form->field($model, 'feature_url')->dropDownList($featureDataItems, ['prompt' => 'Select a Feature..'])->label(false) ?>
                        </div>
                        <div class="col-md-4">
                            <?= Html::button('Load From Connect', ['class' => 'btn btn-success loadButton', 'id' => 'load-form-connect-feature']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'feature_img_url')->hiddenInput()->label(false) ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'feature_excerpt')->textarea(['rows' => 3]) ?>
                        </div>
                    </div>
                    <div class="divider"></div>

                    <div class="row">
                        <div class="news-wrapper">

                            <?php
                            //$NewsData = Yii::getAlias('@webliveUrl').'/wp-json/wp-connect/api?access_token=Y29uZG8tZGV2ZWxvcG1lbnRzVW5sb&post_type=post&&cat_type=In%20the%20News';
                            //$contentData = file_get_contents($NewsData);

                            $newsch = curl_init(Yii::getAlias('@webliveUrl').'/wp-json/wp-connect/api?access_token=Y29uZG8tZGV2ZWxvcG1lbnRzVW5sb&post_type=post&&cat_type=In%20the%20News&time='.time());
                            curl_setopt($newsch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($newsch, CURLOPT_SSL_VERIFYPEER, 0);
                            curl_setopt($newsch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                            curl_setopt($newsch, CURLOPT_CONNECTTIMEOUT, 5);
                            curl_setopt($newsch, CURLOPT_TIMEOUT, 3);
                            curl_setopt($newsch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
                            $newsesult = curl_exec($newsch);
                            $newsDecode = json_decode($newsesult,true);
                            $newsDataItems = [];
                            if(!empty($newsDecode)){
                                $newsDataItems = ArrayHelper::map($newsDecode, 'pageLink', 'title');
                            }
                            $news=1;$totalnews=count($modelNews);
                            ?>
                            <?php foreach ($modelNews as $index => $item){  ?>
                                <div class="news-list">
                                    <div class="newsletter-form-title">
                                        <h3 class="news-title">IN THE NEWS <?php echo $news;?> of <?php echo $totalnews;?></h3>
                                    </div>
                                    <div class="col-md-8">
                                        <?= $form->field($item, '['.$index.']newsid')->hiddenInput(['value'=> $item->id])->label(false); ?>
                                        <?= $form->field($item, '['.$index.']url')->dropDownList($newsDataItems, ['prompt'=>'Select a News'])->label(false) ?>
                                    </div>
                                    <div class="col-md-2">
                                        <?= Html::button('Load From Connect', ['class' => 'btn btn-success loadButton', 'data-id' => $index, 'id' => 'load-form-connect-'.$index.'-news']) ?>
                                    </div>
                                    <?php if(!$model->isNewRecord){?>
                                        <div class="col-md-2 text-center">
                                            <?php
                                            $newsdelete = yii\helpers\Url::to(['newsletters/delete-news-item?id='.$item->id.'&newsletterid='.$model->id]);
                                            ?>
                                            <?= Html::a(Yii::t('yii', '<span style="padding: 5px 20px;margin-top: 10px;" class="btn btn-md btn-danger">Delete</span>'), '#', [
                                                'title' => Yii::t('yii', 'Delete'),
                                                'aria-label' => Yii::t('yii', 'Delete'),
                                                'onclick' => "
                                            if (confirm('Are you sure you want to delete this item?')) {
                                                $.ajax('$newsdelete', {
                                                    type: 'POST'
                                                }).done(function(data) {
                                                    $.pjax.reload({container: '#pjax-container'});
                                                });
                                            }
                                            return false;
                                        ",
                                            ]); ?>
                                        </div>
                                    <?php } ?>
                                    <div class="clearfix"></div>
                                    <?= $form->field($item, '['.$index.']img_url')->hiddenInput()->label(false) ?>
                                    <div class="col-md-12">
                                        <?= $form->field($item, '['.$index.']title')->textInput(['maxlength' => true, 'readonly'=> true]) ?>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12">
                                        <?= $form->field($item, '['.$index.']excerpt')->textarea(['rows' => 2]) ?>
                                    </div>
                                </div>
                                <?php $news++;} ?>
                        </div>

                        <div style="margin-right: 15px" class="pull-right">
                            <?= Html::button('Add New IN THE NEWS', ['class' => 'btn btn-success', 'id' => 'InTheNews']) ?>
                        </div>
                    </div>

                    <div class="divider"></div>

                    <div class="newsletter-form-title">
                        <h3>Stats Block</h3>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'stats_title')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-3">
                            <div class="panel panel-primary">
                                <div style="padding: 5px 12px;" class="panel-heading">
                                    Average Lease
                                </div>

                                <div class="panel-body">
                                    <?= $form->field($model, 'ln_amount')->textInput(['maxlength' => true, 'placeholder' => "Amount"])->label(false) ?>
                                    <?= $form->field($model, 'ln_percent')->textInput(['maxlength' => true, 'placeholder' => "Percent"])->label(false) ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="panel panel-primary">
                                <div style="padding: 5px 12px;" class="panel-heading">
                                    Average Sold
                                </div>
                                <div class="panel-body">
                                    <?= $form->field($model, 'sn_amount')->textInput(['maxlength' => true, 'placeholder' => "Amount"])->label(false) ?>
                                    <?= $form->field($model, 'sn_percent')->textInput(['maxlength' => true, 'placeholder' => "Percent"])->label(false) ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">

                            <div class="panel panel-primary">
                                <div style="padding: 5px 12px;" class="panel-heading">
                                    Toronto Vacancy Rate
                                </div>
                                <div class="panel-body">
                                    <?= $form->field($model, 'tv_percent')->textInput(['maxlength' => true, 'placeholder' => "Percent"])->label(false) ?>
                                    <?= $form->field($model, 'tv_quarter')->textInput(['maxlength' => true, 'placeholder' => "Quarter"])->label(false) ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="panel panel-primary">
                                <div style="padding: 5px 12px;" class="panel-heading">
                                    Previous Comparable
                                </div>
                                <div class="panel-body">
                                    <?= $form->field($model, 'pc_quarter')->textInput(['maxlength' => true, 'placeholder' => "Quarter"])->label(false) ?>
                                    <?= $form->field($model, 'pc_percent')->textInput(['maxlength' => true, 'placeholder' => "Percent"])->label(false) ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'stats_tweet')->textarea(['rows' => 3]) ?>
                        </div>
                    </div>
                    <div class="divider"></div>


                    <div class="row">
                        <div class="invest-wrapper">
                            <?php
                            //Developers Grade  (1 = A+, 2 = A, 3 = B+,4 = B,5 = C, 6 = D)
                            $developmentList = Developments::find()->where(['sales_status' => 'active'])->orWhere(['sales_status' => 'vip'])->andWhere(['grade' => ['1', '2', '4']])->orderBy(['development_name' => SORT_ASC])->all();
                            $developmentArrayList = ArrayHelper::map($developmentList, 'development_id', 'development_name');
                            $int=1;$totalint=count($modelInvestToday);
                            foreach ($modelInvestToday as $index => $item){ ?>
                                <div class="invest-today-list">
                                    <div class="newsletter-form-title">
                                        <h3 id="invest-today-title" class="invest-today-title">Invest Today <?php echo $int;?> of <?php echo $totalint;?></h3>
                                    </div>
                                    <div class="col-md-8">
                                        <?= $form->field($item, '['.$index.']investid')->hiddenInput(['value'=> $item->id])->label(false); ?>
                                        <?= $form->field($item, '['.$index.']development_id')->dropDownList($developmentArrayList, ['prompt'=>'Select a Development'])->label(false) ?>
                                    </div>
                                    <div class="col-md-2">
                                        <?= Html::button('Load From Connect', ['class' => 'btn btn-success loadButton', 'data-id' => $index, 'id' => 'load-form-connect-'.$index.'-investToday']) ?>
                                    </div>
                                    <?php if(!$model->isNewRecord){?>
                                        <div class="col-md-2 text-center">
                                            <?php
                                            $investdelete = yii\helpers\Url::to(['newsletters/delete-invest-item?id='.$item->id.'&newsletterid='.$model->id]);
                                            ?>
                                            <?= Html::a(Yii::t('yii', '<span style="padding: 5px 20px;margin-top: 10px;" class="btn btn-md btn-danger">Delete</span>'), '#', [
                                                'title' => Yii::t('yii', 'Delete'),
                                                'aria-label' => Yii::t('yii', 'Delete'),
                                                'onclick' => "
                                            if (confirm('Are you sure you want to delete this item?')) {
                                                $.ajax('$investdelete', {
                                                    type: 'POST'
                                                }).done(function(data) {
                                                    $.pjax.reload({container: '#pjax-container'});
                                                });
                                            }
                                            return false;
                                        ",
                                            ]); ?>
                                        </div>
                                    <?php } ?>
                                    <div class="clearfix"></div>

                                    <?= $form->field($item, '['.$index.']img_url')->hiddenInput()->label(false) ?>

                                    <div class="col-md-12">
                                        <?= $form->field($item, '['.$index.']title')->textInput(['maxlength' => true, 'readonly'=> true]) ?>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12">
                                        <?= $form->field($item, '['.$index.']excerpt')->textarea(['rows' => 2]) ?>
                                    </div>
                                    <!--<div class="clearfix"></div>
                                    <div class="col-md-6">
                                        <?/*= $form->field($item, '['.$index.']invest_today_bed')->textInput(['maxlength' => true]) */?>
                                    </div>
                                    <div class="col-md-6">
                                        <?/*= $form->field($item, '['.$index.']invest_today_bath')->textInput(['maxlength' => true]) */?>
                                    </div>
                                    <div class="col-md-6">
                                        <?/*= $form->field($item, '['.$index.']invest_today_price')->textInput(['maxlength' => true]) */?>
                                    </div>
                                    <div class="col-md-6">
                                        <?/*= $form->field($item, '['.$index.']invest_today_color')->textInput(['maxlength' => true]) */?>
                                    </div>
                                    <div class="col-md-6 ">
                                        <?/*= $form->field($item, '['.$index.']image_desc')->checkBox(['label' => 'Do You Want To Add Corner Box ? Yes/No',
                                            'uncheck' => '0', 'checked' => '1']) */?>
                                    </div>-->
                                </div>

                                <?php $int++;} ?>
                        </div>
                        <div style="margin-right: 15px" class="pull-right">
                            <?= Html::button('Add New Invest Today', ['class' => 'btn btn-success', 'id' => 'InvestToday']) ?>
                        </div>
                    </div>
                    <div class="divider"></div>


                    <div class="row">
                        <div class="comingsoon-wrapper">
                            <?php  $comingsoon=1; $total=count($modelComingSoon); foreach ($modelComingSoon as $index => $item) { ?>
                                <div class="newsletter-form-title">
                                    <h3 class="comingsoon-title">COMING SOON <?php echo $comingsoon;?> OF <?php echo  $total;?></h3>
                                </div>
                                <div class="comingsoon-list">
                                    <div class="col-md-8">
                                        <?php
                                        //Developers Grade  (1 = A+, 2 = A, 3 = B+,4 = B,5 = C, 6 = D)
                                        $developmentRegList = Developments::find()->where(['sales_status' => 'comingsoon'])->andWhere(['grade' => ['1', '2', '4']])->orderBy(['development_name' => SORT_ASC])->all();
                                        $developmentArrayRegList = ArrayHelper::map($developmentRegList, 'development_id', 'development_name');
                                        ?>
                                        <?= $form->field($item, '['.$index.']comingsoonid')->hiddenInput(['value'=> $item->id])->label(false); ?>
                                        <?= $form->field($item, '['.$index.']development_id')->dropDownList($developmentArrayRegList, ['prompt' => 'Select a Development...'])->label(false) ?>
                                    </div>
                                    <div class="col-md-2">
                                        <?= Html::button('Load From Connect', ['class' => 'btn btn-success loadButton', 'id' => 'load-form-connect-'.$index.'-comingSoon']) ?>
                                    </div>
                                    <?php if(!$model->isNewRecord){?>
                                        <div class="col-md-2 text-center">
                                            <?php
                                            $comingdelete = yii\helpers\Url::to(['newsletters/delete-coming-item?id='.$item->id.'&newsletterid='.$model->id]);
                                            ?>
                                            <?= Html::a(Yii::t('yii', '<span style="padding: 5px 20px;margin-top: 10px;" class="btn btn-md btn-danger">Delete</span>'), '#', [
                                                'title' => Yii::t('yii', 'Delete'),
                                                'aria-label' => Yii::t('yii', 'Delete'),
                                                'onclick' => "
                                            if (confirm('Are you sure you want to delete this item?')) {
                                                $.ajax('$comingdelete', {
                                                    type: 'POST'
                                                }).done(function(data) {
                                                    $.pjax.reload({container: '#pjax-container'});
                                                });
                                            }
                                            return false;
                                        ",
                                            ]); ?>
                                        </div>
                                    <?php } ?>
                                    <div class="clearfix"></div>

                                    <?= $form->field($item, '['.$index.']img_url')->hiddenInput()->label(false) ?>
                                    <div class="col-md-12">
                                        <?= $form->field($item, '['.$index.']title')->textInput(['maxlength' => true, 'readonly'=> true]) ?>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12">
                                        <?= $form->field($item, '['.$index.']excerpt')->textarea(['rows' => 2]) ?>
                                    </div>
                                    <!--<div class="clearfix"></div>
                                    <div class="col-md-6">
                                        <?/*= $form->field($item, '['.$index.']coming_soon_bed')->textInput(['maxlength' => true]) */?>
                                    </div>
                                    <div class="col-md-6">
                                        <?/*= $form->field($item, '['.$index.']coming_soon_bath')->textInput(['maxlength' => true]) */?>
                                    </div>
                                    <div class="col-md-6">
                                        <?/*= $form->field($item, '['.$index.']coming_soon_price')->textInput(['maxlength' => true]) */?>
                                    </div>
                                    <div class="col-md-6">
                                        <?/*= $form->field($item, '['.$index.']coming_soon_color')->textInput(['maxlength' => true]) */?>
                                    </div>
                                    <div class="col-md-6 ">
                                        <?/*= $form->field($item, '['.$index.']image_desc')->checkBox(['label' => 'Do You Want To Add Corner Box ? Yes/No',
                                            'uncheck' => '0', 'checked' => '1']) */?>
                                    </div>-->
                                </div>
                                <?php $comingsoon++; }?>
                        </div>
                        <div style="margin-right: 15px" class="pull-right">
                            <?= Html::button('Add New Coming Soon', ['class' => 'btn btn-success', 'id' => 'ComingSoon']) ?>
                        </div>

                    </div>
                    <div class="divider"></div>

                    <div class="row">
                        <div class="featureresale-wrapper">
                            <?php
                            $nms = 1;
                            $totalfeature=count($modelFeatureResale);
                            ?>
                            <?php foreach ($modelFeatureResale as $index => $item) { ?>
                                <div class="featureresale-list">
                                    <!--                              <div class="newsletter-form-title">-->
                                    <!--                                    <h3>FEATURE RESALE --><?php //echo $nms;?><!-- of 3</h3>-->
                                    <!--                                </div>-->
                                    <div class="newsletter-form-title">
                                        <h3 class="feture-resale-title">FEATURE RESALE <?php echo $nms;?> of <?php echo  $totalfeature;?></h3>
                                    </div>

                                    <div class="col-md-6">
                                        <?= $form->field($item, '['.$index.']url')->textInput(['maxlength' => true]) ?>
                                        <?= $form->field($item, '['.$index.']featureresaleid')->hiddenInput(['value'=> $item->id])->label(false); ?>
                                    </div>
                                    <div class="col-md-3">
                                        <?= $form->field($item, '['.$index.']img_url')->fileInput() ?>
                                    </div>
                                    <div class="col-md-2">
                                        <?php
                                        if(!empty($item->img_url)){
                                            echo '<img class="logo-imgprev" style="margin-left: 0;width: 100px;border: 3px solid rgb(221, 221, 221);border-radius: 2px;background: rgb(221, 221, 221);padding: 0px;margin-bottom: 20px;" src="'.$item->img_url.'?time='.time().'" />';
                                        }else{
                                            echo '<img class="logo-imgprev" style="margin-left: 0;width: 100px;border: 3px solid rgb(221, 221, 221);border-radius: 2px;background: rgb(221, 221, 221);padding: 0px;margin-bottom: 20px;" src="'.Yii::$app->request->BaseUrl.'/uploads/not-found-img.png" alt="#" />';
                                        }
                                        ?>
                                    </div>
                                    <?php if(!$model->isNewRecord){?>
                                        <div class="col-md-1 text-center">
                                            <?php
                                            $featureresaleiddelete = yii\helpers\Url::to(['newsletters/delete-feature-resale-item?id='.$item->id.'&newsletterid='.$model->id]);
                                            ?>
                                            <?= Html::a(Yii::t('yii', '<span style="padding: 5px 20px;margin-top: 10px;" class="btn btn-md btn-danger">Delete</span>'), '#', [
                                                'title' => Yii::t('yii', 'Delete'),
                                                'aria-label' => Yii::t('yii', 'Delete'),
                                                'onclick' => "
                                            if (confirm('Are you sure you want to delete this item?')) {
                                                $.ajax('$featureresaleiddelete', {
                                                    type: 'POST'
                                                }).done(function(data) {
                                                    $.pjax.reload({container: '#pjax-container'});
                                                });
                                            }
                                            return false;
                                        ",
                                            ]); ?>
                                        </div>
                                        <?php $nms++; } ?>
                                    <div class="clearfix"></div>
                                    <div class="col-md-6">
                                        <?= $form->field($item, '['.$index.']top_title')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?= $form->field($item, '['.$index.']title')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12">
                                        <?= $form->field($item, '['.$index.']excerpt')->textarea(['rows' => 2]) ?>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-6">
                                        <?= $form->field($item, '['.$index.']ribbon_text')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?= $form->field($item, '['.$index.']resale_bed')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?= $form->field($item, '['.$index.']resale_bath')->textInput(['maxlength' => true]) ?>
                                    </div>
                                   <!-- <div class="col-md-6">
                                        <?/*= $form->field($item, '['.$index.']ribbon_color')->textInput(['maxlength' => true]) */?>
                                    </div>-->
                                    <div class="col-md-6">
                                        <?= $form->field($item, '['.$index.']resale_price')->textInput(['maxlength' => true]) ?>
                                    </div>
                                   <!-- <div class="col-md-3">
                                        <?/*= $form->field($item, '['.$index.']resale_color')->textInput(['maxlength' => true]) */?>
                                    </div>-->
                                    <div class="col-md-6 ">
                                        <?= $form->field($item, '['.$index.']image_desc')->checkBox(['label' => 'Do You Want To Add Corner Box ? Yes/No',
                                            'uncheck' => '0', 'checked' => '1']) ?>
                                    </div>

                                </div>
                            <?php } ?>
                        </div>
                        <div style="margin-right: 15px" class="pull-right">
                            <?= Html::button('Add New Feature Resale', ['class' => 'btn btn-success', 'id' => 'FeatureResale']) ?>
                        </div>
                    </div>
                    <div class="divider"></div>
                    <div class="newsletter-form-title">
                        <h3>Optional Footer</h3>
                    </div>
                    <div class="row">
                        <div class="footer-wrapper">
                            <?php foreach ($modelFooter as $index => $item) { ?>
                                <div class="footer-list">
                                    <div class="col-md-10">
                                        <?= $form->field($item, '['.$index.']note')->textInput(['maxlength' => true]) ?>
                                        <?= $form->field($item, '['.$index.']footerid')->hiddenInput(['value'=> $item->id])->label(false); ?>
                                    </div>
                                    <?php if(!$model->isNewRecord){?>
                                        <div class="col-md-1 text-center">
                                            <?php
                                            $featureresaleiddelete = yii\helpers\Url::to(['newsletters/delete-footer-item?id='.$item->id.'&footerid='.$model->id]);
                                            ?>
                                            <?= Html::a(Yii::t('yii', '<span style="padding: 5px 20px;margin-top: 21px;" class="btn btn-md btn-danger">Delete</span>'), '#', [
                                                'title' => Yii::t('yii', 'Delete'),
                                                'aria-label' => Yii::t('yii', 'Delete'),
                                                'onclick' => "
                                            if (confirm('Are you sure you want to delete this item?')) {
                                                $.ajax('$featureresaleiddelete', {
                                                    type: 'POST'
                                                }).done(function(data) {
                                                    $.pjax.reload({container: '#pjax-container'});
                                                });
                                            }
                                            return false;
                                        ",
                                            ]); ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <div style="margin-right: 10px;margin-top: 20px;" class="pull-right">
                                <?= Html::button('Add', ['class' => 'btn btn-success', 'id' => 'Footer']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="divider"></div>
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                    <br><br>
                    <div class="footer-button">
                        <div class="row">
                            <div class="col-md-4 text-left">
                                <?= Html::a('Push To Maropost', Url::toRoute(['newsletters/maropost', 'id' => $model->id]), ['class' => 'btn btn-success btn-push-to-maropost btn-show-after-save']) ?>
                            </div>
                            <div class="col-md-4 text-center">
                                <?= Html::a('Download HTML', Url::toRoute(['newsletters/view', 'id' => $model->id]), ['class' => 'btn btn-success btn-download-html btn-show-after-save']) ?>
                                <?= Html::a('Download PDF', Url::toRoute(['newsletters/pdf', 'id' => $model->id]), ['class' => 'btn btn-success btn-download-pdf btn-show-after-save']) ?>
                            </div>
                            <div class="col-md-4 text-right">
                                <div class="admin-form-button">
                                    <?= FormButtons::widget() ?>
                                </div>
                            </div>
                        </div>
                    </div>


                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
    <?php

    $baseUrl = Yii::getAlias('@baseUrl');
    /*$baseUrl='//connectassetmgmt.com';*/
    $script = <<< JS
    $('#CopySubject').on("click",function() {
      var value=$('#newsletters-subject').val();
      $('#newsletters-subject').select();
      document.execCommand("copy");
    });

    var counter = 1;
	var c=0;
    // In The News Add New Row
    var newsrowCount = $('.news-wrapper .news-list').length;
    var newsRowcounter = newsrowCount;
    $("#InTheNews").on("click", function () {
        var newRow = $("<div class='news-list'>");
        var cols = "";
        var count = newsRowcounter;
		var newcount=count -1;
	    var text_value = $('#newslettersnews-'+newcount+'-title').val();
		if(text_value ==''){
			$('#newslettersnews-'+newcount+'-title').css('border-color','red');
			$('#newslettersnews-'+newcount+'-excerpt').css('border-color','red');
		    return false;
		}
		else{
			$('#newslettersnews-'+newcount+'-title').css('border-color','green');
			$('#newslettersnews-'+newcount+'-excerpt').css('border-color','green');
		}
		 cols +='<div class="newsletter-form-title">';
		cols +='<h3 class="news-title" >IN THE NEWS</h3>';
		cols +='</div">';
        cols += '<div class="col-md-8">';
            cols += '<div class="form-group field-newslettersnews-'+ count +'-url"><select id="newslettersnews-'+count+'-url" name="NewslettersNews['+ count +'][url]" class="form-control select3" aria-invalid="false"><option value="">Select a News..</option></select></div>';
        cols += '</div>';
        cols += '<div class="col-md-2">';
            cols += '<button type="button" id="load-form-connect-'+ count +'-news" class="btn btn-success loadButton">Load From Connect</button>';
        cols += '</div>';
         cols += '<div style="text-align:center" class="col-md-2">';
            cols += '<input type="button" class="ibtnDel btn btn-md btn-danger "  value="Delete">';
        cols += '</div>';
       
        cols += '<div class="clearfix"></div>';
        cols += '<div class="form-group field-newslettersnews-'+ count +'-img_url"><input id="newslettersnews-'+ count +'-img_url" type="hidden" class="form-control" name="NewslettersNews['+ count +'][img_url]"/></div>';
      
        cols += '<div class="col-md-12">';
           cols += '<div class="form-group field-newslettersnews-'+ count +'-title"><label for="newslettersnews-'+count+'-title" class="control-label">Title</label><input id="newslettersnews-'+ count +'-title" readonly type="text" class="form-control" name="NewslettersNews['+ count +'][title]"/></div>';
        cols += '</div>';
        
        cols += '<div class="clearfix"></div>';
        cols += '<div class="col-md-12">';
             cols += '<div class="form-group field-newslettersnews-'+ count +'-excerpt"><label for="newslettersnews-'+count+'-excerpt" class="control-label">Excerpt</label><textarea id="newslettersnews-'+ count +'-excerpt" name="NewslettersNews['+ count +'][excerpt]" rows="2" class="form-control"></textarea></div>';
        cols += '</div>';
        
        newRow.append(cols);
        $(".news-wrapper").append(newRow);
        $(".news-wrapper").on("click", ".ibtnDel", function (event) {
            $(this).closest(".news-list").remove();       
            count -= 1
            blockTitleRearrange('.news-title', 'IN THE NEWS');
       
        });
        
        $('#load-form-connect-'+ count +'-news').click(function (index, value) {
           var loadurl = $('#newslettersnews-'+ count +'-url').val();
           var slugname = loadurl.split('/');
           var siteUrl = '$webliveUrl/wp-json/wp-connect/api?access_token=Y29uZG8tZGV2ZWxvcG1lbnRzVW5sb&post_type=post&slug='+slugname[4]+'';
           $.ajax({ 
                type: 'GET', 
                url: siteUrl, 
                //data: { login: login }, 
                dataType: 'json',
                success: function (data) {
                    //var jsonss = $.parseJSON(data);
                    var obj = jQuery.parseJSON( JSON.stringify(data) );
                    console.log(obj);
                    $.each(data, function(index, element) {
                        $('#newslettersnews-'+ count +'-img_url').val(element.imageurl);
                        $('#newslettersnews-'+ count +'-title').val(element.title);
                        $('#newslettersnews-'+ count +'-excerpt').val(element.excerpt);
                    });
                    $('#load-form-connect-'+ count +'-news').text("Load From Connect");
                },
                error: function (data) {
                    console.log('Loading Error.');
                    alert('URL Data not found. please check again blog url.');
                    $('#load-form-connect-'+ count +'-news').text("Load From Connect");
                },
                headers: {'Authorization': 'Basic bWFkaHNvbWUxMjM='},
                beforeSend: function (xhr) {
                    $('#load-form-connect-'+ count +'-news').text("Loading....");
                },
           });
       });
        
        
        var siteUrl = '$webliveUrl/wp-json/wp-connect/api?access_token=Y29uZG8tZGV2ZWxvcG1lbnRzVW5sb&post_type=post&cat_type=In%20the%20News';
        $.ajax({
            type: 'GET', 
            url: siteUrl, 
            dataType: 'json',
            success: function(data) { 
               $.each(data, function(index, element) {
                $('select#newslettersnews-'+ count +'-url')
                 .append($("<option></option>")
                        .attr("value",element.pageLink)
                        .text(element.title)); 
                });
            },
            error: function() { alert('Failed!'); },
            beforeSend: function (xhr) {
                
            },
        });
        
	   
       //$('button#InTheNews').attr('disabled', true);
     /*  var textarea_value = $('textarea#newslettersnews-'+ count +'-excerpt').val();
       var text_value = $('#newslettersnews-'+ count +'-title').val();
       if(text_value != ''){
           $('button#InTheNews').attr('disabled', false);
       }*/
       newsRowcounter++;
       blockTitleRearrange('.news-title', 'IN THE NEWS');
        
    });
    
    // In the News Blog Data Load
    $('.news-list').each(function(i, obj) {
       $('#load-form-connect-'+ i +'-news').click(function (index, value) {
           var loadurl = $('#newslettersnews-'+ i +'-url').val();
          
           var slugname = loadurl.split('/');
            console.log(slugname);
           var siteUrl = '$webliveUrl/wp-json/wp-connect/api?access_token=Y29uZG8tZGV2ZWxvcG1lbnRzVW5sb&post_type=post&slug='+slugname[4]+'';
           $.ajax({ 
                type: 'GET', 
                url: siteUrl, 
                dataType: 'json',
                success: function (data) {
                    //var jsonss = $.parseJSON(data);
                    var obj = jQuery.parseJSON( JSON.stringify(data) );
                    console.log(obj);
                    $.each(data, function(index, element) {
                        $('#newslettersnews-'+ i +'-img_url').val(element.imageurl);
                        $('#newslettersnews-'+ i +'-title').val(element.title);
                        $('#newslettersnews-'+ i +'-excerpt').val(element.excerpt);
                    });
                    $('#load-form-connect-'+ i +'-news').text("Load From Connect");
                },
                error: function (data) {
                    console.log('Loading Error.');
                    alert('URL Data not found. please check again blog url.');
                    $('#load-form-connect-'+ i +'-news').text("Load From Connect");
                },
                headers: {'Authorization': 'Basic bWFkaHNvbWUxMjM='},
                beforeSend: function (xhr) {
                    $('#load-form-connect-'+ i +'-news').text("Loading....");
                },
           });
           
         });
     
    });
    
    
    
    var investTodayrowCount = $('.invest-wrapper .invest-today-list').length;
    // Invest Today Add New Row
    var investTodayLabelCount=2;
    $("#InvestToday").on("click", function () {
        var newRow = $("<div class='invest-today-list'>");
        var cols = "";
        var count = investTodayrowCount;
		var newcount=count -1;
	    var text_value = $('#newslettersinvesttoday-'+newcount+'-title').val();
		if(text_value ==''){
			$('#newslettersinvesttoday-'+newcount+'-title').css('border-color','red');
			$('#newslettersinvesttoday-'+newcount+'-excerpt').css('border-color','red');
		    return false;
		}
		else{
			$('#newslettersinvesttoday-'+newcount+'-title').css('border-color','green');
			$('#newslettersinvesttoday-'+newcount+'-excerpt').css('border-color','green');
		}
		
		cols +='<div class="newsletter-form-title">';
		cols +='<h3 class="invest-today-title">INVEST TODAY '+investTodayLabelCount+'</h3>';
		cols +='</div">';
        cols += '<div class="col-md-8">';
            cols += '<div class="form-group field-newslettersinvesttoday-'+ count +'-development_id"><select id="newslettersinvesttoday-'+ count +'-development_id" name="NewslettersInvestToday['+ count +'][development_id]" class="form-control" aria-invalid="false"><option value="">Select  a Development...</option></select></div>';
        cols += '</div>';
        
        cols += '<div class="col-md-2">';
            cols += '<button type="button" id="load-form-connect-'+ count +'-investToday" class="btn btn-success loadButton">Load From Connect</button>';
        cols += '</div>';
         cols += '<div style="text-align:center" class="col-md-2">';
            cols += '<input type="button" class="ibtnDel btn btn-md btn-danger "  value="Delete">';
        cols += '</div>';
       
        cols += '<div class="clearfix"></div>';
        
        cols += '<div class="form-group field-newslettersinvesttoday-'+ count +'-img_url"><input id="newslettersinvesttoday-'+ count +'-img_url" type="hidden" class="form-control" name="NewslettersInvestToday['+ count +'][img_url]"/></div>';
        cols += '<div class="col-md-12">';
           cols += '<div class="form-group field-newslettersinvesttoday-'+ count +'-title"><label for="newslettersinvesttoday-'+ count +'-title" class="control-label">Title</label><input id="newslettersinvesttoday-'+ count +'-title" type="text" readonly class="form-control" name="NewslettersInvestToday['+ count +'][title]"/></div>';
        cols += '</div>';
        
        cols += '<div class="clearfix"></div>';
        cols += '<div class="col-md-12">';
             cols += '<div class="form-group field-newslettersinvesttoday-'+ count +'-excerpt"><label for="newslettersinvesttoday-'+ count +'-excerpt" class="control-label">Excerpt</label><textarea id="newslettersinvesttoday-'+ count +'-excerpt" name="NewslettersInvestToday['+ count +'][excerpt]" rows="2" class="form-control"></textarea></div>';
        cols += '</div>';
        
        
        /*cols += '<div class="clearfix"></div>';
        cols += '<div class="col-md-6">';
           cols += '<div class="form-group field-newslettersinvesttoday-'+ count +'-invest_today_bed"><label for="newslettersinvesttoday-'+ count +'-invest_today_bed" class="control-label">Bed</label><input id="newslettersinvesttoday-'+ count +'-invest_today_bed" type="text" class="form-control" name="NewslettersInvestToday['+ count +'][invest_today_bed]"/></div>';
        cols += '</div>';
        
         cols += '<div class="col-md-6">';
           cols += '<div class="form-group field-newslettersinvesttoday-'+ count +'-invest_today_bath"><label for="newslettersinvesttoday-'+ count +'-invest_today_bath" class="control-label">Bath</label><input id="newslettersinvesttoday-'+ count +'-invest_today_bath" type="text" class="form-control" name="NewslettersInvestToday['+ count +'][invest_today_bath]"/></div>';
        cols += '</div>';
        
        cols += '<div class="col-md-6">';
           cols += '<div class="form-group field-newslettersinvesttoday-'+ count +'-invest_today_price"><label for="newslettersinvesttoday-'+ count +'-invest_today_price" class="control-label">Price</label><input id="newslettersinvesttoday-'+ count +'-invest_today_price" type="text" class="form-control" name="NewslettersInvestToday['+ count +'][invest_today_price]"/></div>';
        cols += '</div>';
        
        cols += '<div class="col-md-6">';
           cols += '<div class="form-group field-newslettersinvesttoday-'+ count +'-invest_today_color"><label for="newslettersinvesttoday-'+ count +'-invest_today_color" class="control-label">Box Color</label><input id="newslettersinvesttoday-'+ count +'-invest_today_color" type="text" class="form-control" name="NewslettersInvestToday['+ count +'][invest_today_color]"/></div>';
        cols += '</div>';
        
         cols += '<div class="col-md-6 show-status">';
           cols += '<div class="form-group field-newslettersinvesttoday-'+ count +'-image_desc"><label for="newslettersinvesttoday-'+ count +'-image_desc"><input type="hidden" id="newslettersinvesttoday-'+ count +'-image_desc"  name="NewslettersInvestToday['+ count +'][image_desc]" value="0"><input type="checkbox" id="newslettersinvesttoday-'+ count +'-image_desc"  name="NewslettersInvestToday['+ count +'][image_desc]" value="1" >Do You Want To Add Corner Box ? Yes/No</label></div>';
          cols += '</div>';*/
          
        
        
        newRow.append(cols);
        $(".invest-wrapper").append(newRow);
        
        $(".invest-wrapper").on("click", ".ibtnDel", function (event) {
            $(this).closest(".invest-today-list").remove();       
            investTodayrowCount -= 1;
            blockTitleRearrange('.invest-today-title', 'INVEST TODAY');
        });
        
        
        // In the News Blog Post Load
        $('#load-form-connect-'+ count +'-investToday').click(function (index, value) {
            var loadurl = $('#newslettersinvesttoday-'+ count +'-development_id').val();
            
            /*var siteUrl = 'https://connectassetmgmt.com/backend/web/newsletters/newsletters/development-details?id='+loadurl;*/
            var siteUrl = '$baseUrl/backend/web/newsletters/newsletters/development-details?id='+loadurl;
            
            
            $.ajax({ 
                type: 'GET', 
                url: siteUrl, 
                dataType: 'json',
                success: function (data) {
                    //var jsonss = $.parseJSON(data);
                    var obj = jQuery.parseJSON( JSON.stringify(data) );
                    $.each(data, function(index, element) {
                        $('#newslettersinvesttoday-'+ count +'-img_url').val(data.image_url);
                        $('#newslettersinvesttoday-'+ count +'-title').val(data.development_name);
                        $('#newslettersinvesttoday-'+ count +'-excerpt').val(data.excerpt);
                    });
                    $('#load-form-connect-'+ count +'-investToday').text("Load From Connect");
                },
                error: function (data) {
                    console.log('Loading Error.');
                    alert('URL Data not found. please check again.');
                    $('#load-form-connect-'+ count +'-investToday').text("Load From Connect");
                },
                headers: {'Authorization': 'Basic bWFkaHNvbWUxMjM='},
                beforeSend: function (xhr) {
                    $('#load-form-connect-'+ count +'-investToday').text("Loading....");
                },
            });
        });
        
         $.get('$baseUrl/backend/web/newsletters/newsletters/developmentlist?status=active', function(data) {   
            $.each(data, function(key, value) {   
         
            $('#newslettersinvesttoday-'+ count +'-development_id')
             .append($("<option></option>")
                        .attr("value",key)
                        .text(value)); 
            });
        
        },'json');
         
        investTodayrowCount++;
        investTodayLabelCount++;
        
        blockTitleRearrange('.invest-today-title', 'INVEST TODAY');
    });
    
   
     
    $('.invest-today-list').each(function(i, obj) {
       $('#load-form-connect-'+ i +'-investToday').click(function (index, value) {
       var loadurl = $('#newslettersinvesttoday-'+ i +'-development_id').val();
       
       var siteUrl = '$baseUrl/backend/web/newsletters/newsletters/development-details?id='+loadurl;
       
       $.ajax({ 
            type: 'GET', 
            url: siteUrl, 
            dataType: 'json',
            success: function (data) {
                //var jsonss = $.parseJSON(data);
                var obj = jQuery.parseJSON( JSON.stringify(data) );
                $.each(data, function(index, element) {
                    $('#newslettersinvesttoday-'+ i +'-img_url').val(data.image_url);
                    $('#newslettersinvesttoday-'+ i +'-title').val(data.development_name);
                    $('#newslettersinvesttoday-'+ i +'-excerpt').val(data.excerpt);
                });
                $('#load-form-connect-'+ i +'-investToday').text("Load From Connect");
            },
            error: function (data) {
                console.log('Loading Error.');
                alert('URL Data not found. please check again.');
                $('#load-form-connect-'+ i +'-investToday').text("Load From Connect");
            },
            headers: {'Authorization': 'Basic bWFkaHNvbWUxMjM='},
            beforeSend: function (xhr) {
                $('#load-form-connect-'+ i +'-investToday').text("Loading....");
            },
       });
     });
    });
    
    // Comming Soon Add New Row
    var comingSoonrowCount = $('.comingsoon-wrapper .comingsoon-list').length;
     var comingSoon=2;
    $("#ComingSoon").on("click", function () {
        var newRow = $("<div class='comingsoon-list'>");
        var cols = "";
        var count = comingSoonrowCount;
		
		var newcount=count -1;	
	    var text_value = $('#newsletterscomingsoon-'+newcount+'-title').val();
		if(text_value ==''){
			$('#newsletterscomingsoon-'+newcount+'-title').css('border-color','red');
			$('#newsletterscomingsoon-'+newcount+'-excerpt').css('border-color','red');
		    return false;
		}
		else{
			$('#newsletterscomingsoon-'+newcount+'-title').css('border-color','green');
			$('#newsletterscomingsoon-'+newcount+'-excerpt').css('border-color','green');
		}
		cols +='<div class="newsletter-form-title">';
		cols +='<h3 class="comingsoon-title">COMING SOON'+comingSoon+'</h3>';
		cols +='</div">';
        cols += '</div><div class="col-md-8">';
            cols += '<div class="form-group field-newsletterscomingsoon-'+ count +'-development_id"><select id="newsletterscomingsoon-'+ count +'-development_id" name="NewslettersComingSoon['+ count +'][development_id]" class="form-control"><option value="">Select a Development...</option></select></div>';
        cols += '</div>';
        
        cols += '<div class="col-md-2">';
            cols += '<button type="button" id="load-form-connect-'+ count +'-comingSoon" class="btn btn-success">Load From Connect</button>';
        cols += '</div>';
         cols += '<div style="text-align:center" class="col-md-2">';
            cols += '<input type="button" class="ibtnDel btn btn-md btn-danger "  value="Delete">';
        cols += '</div>';
       
        cols += '<div class="clearfix"></div>';
        cols += '<div class="form-group field-newsletterscomingsoon-'+ count +'-img_url"><input id="newsletterscomingsoon-'+ count +'-img_url" type="hidden" class="form-control" name="NewslettersComingSoon['+ count +'][img_url]"/></div>';
  
        cols += '<div class="col-md-12">';
           cols += '<div class="form-group field-newsletterscomingsoon-'+ count +'-title"><label for="newsletterscomingsoon-'+ count +'-title" class="control-label">Title</label><input id="newsletterscomingsoon-'+ count +'-title" type="text" class="form-control" readonly name="NewslettersComingSoon['+ count +'][title]"/></div>';
        cols += '</div>';
        
        cols += '<div class="clearfix"></div>';
        cols += '<div class="col-md-12">';
             cols += '<div class="form-group field-newsletterscomingsoon-'+ count +'-excerpt"><label for="newsletterscomingsoon-'+ count +'-excerpt" class="control-label">Excerpt</label><textarea id="newsletterscomingsoon-'+ count +'-excerpt" name="NewslettersComingSoon['+ count +'][excerpt]" rows="2" class="form-control"></textarea></div>';
         cols += '</div>';
        
 		/*cols += '<div class="clearfix"></div>';
        cols += '<div class="col-md-6">';
           cols += '<div class="form-group field-newsletterscomingsoon-'+ count +'-coming_soon_bed"><label for="newsletterscomingsoon-'+ count +'-coming_soon_bed" class="control-label">Bed</label><input id="newsletterscomingsoon-'+ count +'-coming_soon_bed" type="text" class="form-control" name="NewslettersComingSoon['+ count +'][coming_soon_bed]"/></div>';
        cols += '</div>';
        
         cols += '<div class="col-md-6">';
           cols += '<div class="form-group field-newsletterscomingsoon-'+ count +'-coming_soon_bath"><label for="newsletterscomingsoon-'+ count +'-coming_soon_bath" class="control-label">Bath</label><input id="newsletterscomingsoon-'+ count +'-coming_soon_bath" type="text" class="form-control" name="NewslettersComingSoon['+ count +'][coming_soon_bath]"/></div>';
        cols += '</div>';
        
        cols += '<div class="col-md-6">';
           cols += '<div class="form-group field-newsletterscomingsoon-'+ count +'-coming_soon_price"><label for="newsletterscomingsoon-'+ count +'-coming_soon_price" class="control-label">Price</label><input id="newsletterscomingsoon-'+ count +'-coming_soon_price" type="text" class="form-control" name="NewslettersComingSoon['+ count +'][coming_soon_price]"/></div>';
        cols += '</div>';
        
        cols += '<div class="col-md-6">';
           cols += '<div class="form-group field-newsletterscomingsoon-'+ count +'-coming_soon_color"><label for="newsletterscomingsoon-'+ count +'-coming_soon_color" class="control-label">Box Color</label><input id="newsletterscomingsoon-'+ count +'-coming_soon_color" type="text" class="form-control" name="NewslettersComingSoon['+ count +'][coming_soon_color]"/></div>';
        cols += '</div>';
        
         cols += '<div class="col-md-6 show-status">';
           cols += '<div class="form-group field-newsletterscomingsoon-'+ count +'-image_desc"><label for="newsletterscomingsoon-'+ count +'-image_desc"><input type="hidden" id="newsletterscomingsoon-'+ count +'-image_desc"  name="NewslettersComingSoon['+ count +'][image_desc]" value="0"><input type="checkbox" id="newsletterscomingsoon-'+ count +'-image_desc"  name="NewslettersComingSoon['+ count +'][image_desc]" value="1" >Do You Want To Add Corner Box ? Yes/No</label></div>';
          cols += '</div>';*/
        
        
        newRow.append(cols);
        $(".comingsoon-wrapper").append(newRow);
        
        $(".comingsoon-wrapper").on("click", ".ibtnDel", function (event) {
            $(this).closest(".comingsoon-list").remove();       
            comingSoonrowCount -= 1
            blockTitleRearrange('.comingsoon-title', 'COMING SOON');
        });
        
        
         // In the News Blog Post Load
        $('#load-form-connect-'+ count +'-comingSoon').click(function (index, value) {
            var loadurl = $('#newsletterscomingsoon-'+ count +'-development_id').val();
            
            var siteUrl = '$baseUrl/backend/web/newsletters/newsletters/development-details?id='+loadurl;
            
            $.ajax({ 
                type: 'GET', 
                url: siteUrl, 
                dataType: 'json',
                success: function (data) {
                    //var jsonss = $.parseJSON(data);
                    var obj = jQuery.parseJSON( JSON.stringify(data) );
                    $.each(data, function(index, element) {
                        $('#newsletterscomingsoon-'+ count +'-img_url').val(data.image_url);
                        $('#newsletterscomingsoon-'+ count +'-title').val(data.development_name);
                        $('#newsletterscomingsoon-'+ count +'-excerpt').val(data.excerpt);
                    });
                    $('#load-form-connect-'+ count +'-comingSoon').text("Load From Connect");
                },
                error: function (data) {
                    console.log('Loading Error.');
                    alert('URL Data not found. please check again.');
                    $('#load-form-connect-'+ count +'-comingSoon').text("Load From Connect");
                },
                headers: {'Authorization': 'Basic bWFkaHNvbWUxMjM='},
                beforeSend: function (xhr) {
                    $('#load-form-connect-'+ count +'-comingSoon').text("Loading....");
                },
            });
        });
        
      
         $.get('$baseUrl/backend/web/newsletters/newsletters/developmentlist?status=comingsoon', function(data) {   
            $.each(data, function(key, value) {   
         
            $('#newsletterscomingsoon-'+ count +'-development_id')
             .append($("<option></option>")
                        .attr("value",key)
                        .text(value)); 
            });
        
        },'json');
         
        comingSoonrowCount++;
        blockTitleRearrange('.comingsoon-title', 'COMING SOON');
    });
    $('.comingsoon-list').each(function(i, obj) {
        $('#load-form-connect-'+ i +'-comingSoon').click(function (index, value) {
            var loadurl = $('#newsletterscomingsoon-'+ i +'-development_id').val();
            var siteUrl = '$baseUrl/backend/web/newsletters/newsletters/development-details?id='+loadurl;
            $.ajax({ 
                type: 'GET', 
                url: siteUrl, 
                dataType: 'json',
                success: function (data) {
                    //var jsonss = $.parseJSON(data);
                    var obj = jQuery.parseJSON( JSON.stringify(data) );
                    $.each(data, function(index, element) {
                        $('#newsletterscomingsoon-'+ i +'-img_url').val(data.image_url);
                        $('#newsletterscomingsoon-'+ i +'-title').val(data.development_name);
                        $('#newsletterscomingsoon-'+ i +'-excerpt').val(data.excerpt);
                    });
                    $('#load-form-connect-'+ i +'-comingSoon').text("Load From Connect");
                },
                error: function (data) {
                    console.log(data);
                    alert('URL Data not found. please check again.');
                    $('#load-form-connect-'+ i +'-comingSoon').text("Load From Connect");
                },
                headers: {'Authorization': 'Basic bWFkaHNvbWUxMjM='},
                beforeSend: function (xhr) {
                    $('#load-form-connect-'+ i +'-comingSoon').text("Loading....");
                },
            });
        });
    });
    
    var featureresaleListrowCount = $('.featureresale-wrapper .featureresale-list').length;
    var featureResale=2;
     $("#FeatureResale").on("click", function () {
        var newRow = $("<div class='featureresale-list'>");
        var cols = "";
        var count = featureresaleListrowCount;
		var newcount=count -1;	
	    var url = $('#newslettersfeatureresale-'+newcount+'-url').val();
	    var top_title = $('#newslettersfeatureresale-'+newcount+'-top_title').val();
	    var title = $('#newslettersfeatureresale-'+newcount+'-title').val();
	    var excerpt = $('#newslettersfeatureresale-'+newcount+'-excerpt').val();
		if(url =='' || top_title =='' || title=='' || excerpt==''){
			$('#newslettersfeatureresale-'+newcount+'-url').css('border-color','red');
			$('#newslettersfeatureresale-'+newcount+'-top_title').css('border-color','red');
			$('#newslettersfeatureresale-'+newcount+'-title').css('border-color','red');
			$('#newslettersfeatureresale-'+newcount+'-excerpt').css('border-color','red');
			return false;
		}
		else{
			$('#newslettersfeatureresale-'+newcount+'-url').css('border-color','green');
			$('#newslettersfeatureresale-'+newcount+'-top_title').css('border-color','green');
			$('#newslettersfeatureresale-'+newcount+'-title').css('border-color','green');
			$('#newslettersfeatureresale-'+newcount+'-excerpt').css('border-color','green');
		}
		cols +='<div class="newsletter-form-title">';
		cols +='<h3 class="feture-resale-title">FEATURE RESALE '+featureResale+'</h3>';
		cols +='</div">';
        cols += '<div class="col-md-8">';
            cols += '<div class="form-group field-newslettersfeatureresale-'+ count +'-url"><label for="newslettersfeatureresale-'+ count +'-url" class="control-label">URL</label><input id="newslettersfeatureresale-'+ count +'-url" type="text" class="form-control" name="NewslettersFeatureResale['+ count +'][url]"/></div>';
        cols += '</div>';
        
        cols += '<div class="col-md-2">';
            cols += '<div class="form-group field-newslettersfeatureresale-'+ count +'-img_url"><label for="newslettersfeatureresale-'+ count +'-img_url" class="control-label">Image URL</label><input type="file" id="newslettersfeatureresale-'+ count +'-img_url" name="NewslettersFeatureResale['+ count +'][img_url]"></div>';
        cols += '</div>';
         cols += '<div style="text-align:center" class="col-md-2">';
            cols += '<input style="margin-top:20px" type="button" class="ibtnDel btn btn-md btn-danger "  value="Delete">';
        cols += '</div>';
       
        cols += '<div class="clearfix"></div>';
        cols += '<div class="col-md-6">';
           cols += '<div class="form-group field-newslettersfeatureresale-'+ count +'-title"><label for="newslettersfeatureresale-'+ count +'-top_title" class="control-label">Top Title</label><input id="newslettersfeatureresale-'+ count +'-top_title" type="text" class="form-control" name="NewslettersFeatureResale['+ count +'][top_title]"/></div>';
        cols += '</div>';
        cols += '<div class="col-md-6">';
           cols += '<div class="form-group field-newslettersfeatureresale-'+ count +'-title"><label for="newslettersfeatureresale-'+ count +'-title" class="control-label">Title</label><input id="newslettersfeatureresale-'+ count +'-title" type="text" class="form-control" name="NewslettersFeatureResale['+ count +'][title]"/></div>';
        cols += '</div>';
        cols += '<div class="clearfix"></div>';
        cols += '<div class="col-md-12">';
             cols += '<div class="form-group field-newslettersfeatureresale-'+ count +'-excerpt"><label for="newslettersfeatureresale-'+ count +'-excerpt" class="control-label">Excerpt</label><textarea id="newslettersfeatureresale-'+ count +'-excerpt" name="NewslettersFeatureResale['+ count +'][excerpt]" rows="2" class="form-control"></textarea></div>';
        cols += '</div>';
        
        cols += '<div class="clearfix"></div>';
        cols += '<div class="col-md-6">';
           cols += '<div class="form-group field-newslettersfeatureresale-'+ count +'-ribbon_tex"><label for="newslettersfeatureresale-'+ count +'-ribbon_tex" class="control-label">Ribon Text</label><input id="newslettersfeatureresale-'+ count +'-ribbon_tex" type="text" class="form-control" name="NewslettersFeatureResale['+ count +'][ribbon_tex]"/></div>';
        cols += '</div>';
        
          cols += '<div class="col-md-6">';
           cols += '<div class="form-group field-newslettersfeatureresale-'+ count +'-resale_bed"><label for="newslettersfeatureresale-'+ count +'-resale_bed" class="control-label">Bed</label><input id="newslettersfeatureresale-'+ count +'-resale_bed" type="text" class="form-control" name="NewslettersFeatureResale['+ count +'][resale_bed]"/></div>';
        cols += '</div>';
        
         cols += '<div class="col-md-6">';
           cols += '<div class="form-group field-newslettersfeatureresale-'+ count +'-resale_bath"><label for="newslettersfeatureresale-'+ count +'-resale_bath" class="control-label">Bath</label><input id="newslettersfeatureresale-'+ count +'-resale_bath" type="text" class="form-control" name="NewslettersFeatureResale['+ count +'][resale_bath]"/></div>';
        cols += '</div>';
        
        /* cols += '<div class="col-md-6">';
           cols += '<div class="form-group field-newslettersfeatureresale-'+ count +'-ribbon_color"><label for="newslettersfeatureresale-'+ count +'-ribbon_color" class="control-label">Box Color</label><input id="newslettersfeatureresale-'+ count +'-ribbon_color" type="text" class="form-control" name="NewslettersFeatureResale['+ count +'][ribbon_color]"/></div>';
         cols += '</div>';*/
         
          cols += '<div class="col-md-6">';
           cols += '<div class="form-group field-newslettersfeatureresale-'+ count +'-resale_price"><label for="newslettersfeatureresale-'+ count +'-resale_price" class="control-label">Price</label><input id="newslettersfeatureresale-'+ count +'-resale_price" type="text" class="form-control" name="NewslettersFeatureResale['+ count +'][resale_price]"/></div>';
          cols += '</div>';
        
          /*cols += '<div class="col-md-3 ">';
           cols += '<div class="form-group field-newslettersfeatureresale-'+ count +'-resale_color"><label for="newslettersfeatureresale-'+ count +'-resale_color" class="control-label">Color</label><input id="newslettersfeatureresale-'+ count +'-resale_color" type="text" class="form-control" name="NewslettersFeatureResale['+ count +'][resale_color]"/></div>';
          cols += '</div>';*/
        
         cols += '<div class="col-md-6 show-status">';
           cols += '<div class="form-group field-newslettersfeatureresale-'+ count +'-image_desc"><label for="newslettersfeatureresale-'+ count +'-image_desc"><input type="hidden" id="newslettersfeatureresale-'+ count +'-image_desc"  name="NewslettersFeatureResale['+ count +'][image_desc]" value="0"><input type="checkbox" id="newslettersfeatureresale-'+ count +'-image_desc"  name="NewslettersFeatureResale['+ count +'][image_desc]" value="1" >Do You Want To Add Corner Box ? Yes/No</label></div>';
          cols += '</div>';
        
        newRow.append(cols);
        $(".featureresale-wrapper").append(newRow);
        
        $(".featureresale-wrapper").on("click", ".ibtnDel", function (event) {
            $(this).closest(".featureresale-list").remove();       
            featureresaleListrowCount -= 1
             blockTitleRearrange('.feture-resale-title', 'FEATURE RESALE ');
        });
        featureresaleListrowCount++;
        blockTitleRearrange('.feture-resale-title', 'FEATURE RESALE ');
    });
     
    // Footer Add New Row
    var footerrowCount = $('.footer-wrapper .footer-list').length;
    $("#Footer").on("click", function () {
        var newRow = $("<div class='footer-list'>");
        var cols = "";
        var count = footerrowCount;
		
		var newcount=count -1;	
		
        cols += '<div class="col-md-10">';
            cols += '<div class="form-group field-newslettersfooter-'+ count +'-url"><label for="newslettersfooter-'+ count +'-url" class="control-label">Note</label><input id="newslettersfooter-'+ count +'-url" type="text" class="form-control" name="NewslettersFooter['+ count +'][note]"/></div>';
        cols += '</div>';
         cols += '<div style="text-align:center" class="col-md-1">';
            cols += '<input style="margin-top:20px" type="button" class="ibtnDel btn btn-md btn-danger "  value="Delete">';
        cols += '</div>';
        
        newRow.append(cols);
        $(".footer-wrapper").append(newRow);
        
        $(".footer-wrapper").on("click", ".ibtnDel", function (event) {
            $(this).closest(".footer-list").remove();       
            footerrowCount -= 1
        });
        footerrowCount++;
    });
    
    
    // Feature Blog Post Load
    jQuery(document).ready(function($) {
       $('#load-form-connect-feature').click(function (index, value) {
           var loadurl = $('#newsletters-feature_url').val();
           var slugname = loadurl.split('/');
           console.log(slugname);
           if(slugname[3] == 'videos'){
               var siteUrl = '$webliveUrl/wp-json/wp-connect/api?access_token=Y29uZG8tZGV2ZWxvcG1lbnRzVW5sb&post_type=videos&slug='+slugname[4]+'';
           }else{
               var siteUrl = '$webliveUrl/wp-json/wp-connect/api?access_token=Y29uZG8tZGV2ZWxvcG1lbnRzVW5sb&post_type=post&slug='+slugname[4]+'';
           }
           
           
           $.ajax({ 
                type: 'GET', 
                url: siteUrl, 
                dataType: 'json',
                success: function (data) {
                    //var jsonss = $.parseJSON(data);
                    var obj = jQuery.parseJSON( JSON.stringify(data) );
                    console.log(obj);
                    $.each(data, function(index, element) {
                        $('#newsletters-feature_img_url').val(element.imageurl);
                        $('#newsletters-feature_excerpt').val(element.excerpt);
                    });
                    $('#load-form-connect-feature').text("Load From Connect");
                },
                error: function (data) {
                    console.log('Loading Error.');
                    alert('URL Data not found. please check again blog url.');
                    $('#load-form-connect-feature').text("Load From Connect");
                },
                headers: {'Authorization': 'Basic bWFkaHNvbWUxMjM='},
                beforeSend: function (xhr) {
                    $('#load-form-connect-feature').text("Loading....");
                },
           });
       });
     });
     $(document).ready(function(){
        //Average lease
        $("#newsletters-ln_amount").on("input", function(evt) {
            var self = $(this);
            self.val(self.val().replace(/[^\d].+/, ""));
            if ((evt.which < 48 || evt.which > 57))
            {
                evt.preventDefault();
            }
        });
        $("#newsletters-ln_amount").change("input", function(evt) {
            var self = $(this);
            self.val('$'+self.val().replace(/[^\d].+/, ""));
        });


        $('#newsletters-ln_percent').change(function() {
            $(this).val(function(index, old) { return old.match(/^\d+\.?\d{0,1}/, '') + '%'; });
        });

        $("#newsletters-ln_percent").on("input", function(evt) {
           this.value = this.value.match(/^\d+\.?\d{0,1}/);
        });

        //Average Sold
        $("#newsletters-sn_amount").on("input", function(evt) {
            var self = $(this);
            self.val(self.val().replace(/[^\d].+/, ""));
            if ((evt.which < 48 || evt.which > 57))
            {
                evt.preventDefault();
            }
        });

        $("#newsletters-sn_amount").change("input", function(evt) {
            var self = $(this);
            self.val('$'+self.val().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        });

        $('#newsletters-sn_percent').change(function() {
            $(this).val(function(index, old) { return old.match(/^\d+\.?\d{0,2}/, '') + '%'; });
        });
        $("#newsletters-sn_percent").on("input", function(evt) {
            this.value = this.value.match(/^\d+\.?\d{0,2}/);
        });

        //Toronto Vacancy
        $("#newsletters-tv_percent").on("input", function(evt) {
            this.value = this.value.match(/^\d+\.?\d{0,1}/);
        });

        $('#newsletters-tv_percent').change(function() {
            $(this).val(function(index, old) { return old.match(/^\d+\.?\d{0,1}/, '') + '%'; });
        });
        
        $("#newsletters-pc_percent").on("input", function(evt) {
            this.value = this.value.match(/^\d+\.?\d{0,1}/);
        });

        $('#newsletters-pc_percent').change(function() {
            $(this).val(function(index, old) { return old.match(/^\d+\.?\d{0,1}/, '') + '%'; });
        });
        
        // Show btn after save
        $('form').on('keyup change paste', 'input, select, textarea', function(){
            $('.btn-show-after-save').hide();
        });
        
        
    });
     function blockTitleRearrange(className, titlePrefix)
     {
         var countBlockTitle = $(className).length;
         var count = 1;
         $(className).each(function() {
             var blockTitle = titlePrefix + ' '+ count +' of '+countBlockTitle ;
            $( this ).html(blockTitle);
            count++;
         });

     }
    
    /*$("select#newslettersnews-0-url").addClass("select2");*/
JS;
    $this->registerJs($script);
    ?>

<?php

use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\buttons\ExportCsvButton;
use unlock\modules\core\buttons\ExportPdfButton;
use unlock\modules\core\buttons\ResetFilterButton;
use unlock\modules\core\buttons\SearchFilterButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $model unlock\modules\newsletters\models\NewslettersSearch */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="admin-search-form-container newsletters-search">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
        'validateOnBlur' => false,
        'enableAjaxValidation' => false,
        'errorCssClass'=>'has-error',
        'fieldConfig' => [
            'template' => "{label}\n{input}\n",
            'options' => ['class' => 'col-sm-4'],
            'horizontalCssClasses' => [
                'label' => '',
                'offset' => '',
                'wrapper' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <div class="admin-search-form-fields">

        <div class="row">
        	<?= $form->field($model, 'id') ?>

			<?= $form->field($model, 'name') ?>

			<?= $form->field($model, 'subject') ?>

			<?php // echo $form->field($model, 'publish_date') ?>

			<?php // echo $form->field($model, 'feature_url') ?>

			<?php // echo $form->field($model, 'feature_excerpt') ?>

			<?php // echo $form->field($model, 'feature_img_url') ?>

			<?php // echo $form->field($model, 'stats_title') ?>

			<?php // echo $form->field($model, 'stats_title_1') ?>

			<?php // echo $form->field($model, 'stats_amount_1') ?>

			<?php // echo $form->field($model, 'stats_day_1') ?>

			<?php // echo $form->field($model, 'stats_percent_1') ?>

			<?php // echo $form->field($model, 'stats_year_1') ?>

			<?php // echo $form->field($model, 'stats_title_2') ?>

			<?php // echo $form->field($model, 'stats_amount_2') ?>

			<?php // echo $form->field($model, 'stats_day_2') ?>

			<?php // echo $form->field($model, 'stats_percent_2') ?>

			<?php // echo $form->field($model, 'stats_year_2') ?>

			<?php // echo $form->field($model, 'stats_title_3') ?>

			<?php // echo $form->field($model, 'stats_amount_3') ?>

			<?php // echo $form->field($model, 'stats_day_3') ?>

			<?php // echo $form->field($model, 'stats_percent_3') ?>

			<?php // echo $form->field($model, 'stats_year_3') ?>

			<?php // echo $form->field($model, 'stats_tweet') ?>

			<?php // echo $form->field($model, 'created_at') ?>

			<?php // echo $form->field($model, 'created_by') ?>

			<?php // echo $form->field($model, 'updated_at') ?>

			<?php // echo $form->field($model, 'updated_by') ?>

			<?php // echo $form->field($model, 'status') ?>

        </div>

        <div class="row">
            <div class="col-sm-6 filter-custom-search-btn">
                <?= SearchFilterButton::widget() ?>
                <?= ResetFilterButton::widget(['url' => Url::toRoute(['index'])]) ?>
            </div>
            <div class="col-sm-6 filter-custom-export-btn">
                <?= ExportCsvButton::widget(['url' => Url::toRoute(['export-csv'])]) ?>
                <?= ExportPdfButton::widget(['url' => Url::toRoute(['export-pdf'])]) ?>
            </div>
        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>

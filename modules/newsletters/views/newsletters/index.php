<?php

use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\grid\GridView;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\buttons\NewButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $searchModel unlock\modules\newsletters\models\NewslettersSearch */
/* @var $dataProvider unlock\modules\core\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Newsletters');
?>
<div class="main-container" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-grid-filter">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <div class="admin-grid-content">
        <div class="admin-grid-toolbar">
            <div class="admin-grid-toolbar-left">
                <?= NewButton::widget(['url' => Url::toRoute(['create'])]) ?>
            </div>
            <div class="admin-grid-toolbar-right">
                <?= Summary::widget(['dataProvider' => $dataProvider]) ?>
            </div>
        </div>

        <div class="admin-grid-view">
               <?php
                echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
                'pager' => ['class' => LinkPager::className()],
                'columns' => [
                    //['class' => 'unlock\modules\core\grid\SerialColumn'],
                    [
                        'attribute' => 'id',
                        'headerOptions' => ['style' => 'width:70px'],
                    ],
                    //'name',
                    [
                        'attribute' => 'subject',
                        'format' => 'html',
                        'value' => function($model){
                            $subjectName =  Html::a($model->subject, Url::toRoute(['newsletters/update', 'id' => $model->id]));
                            return $subjectName;
                        }
                    ],
                    [
                        'attribute' => 'publish_date',
                        'headerOptions' => ['style' => 'width:140px'],
                        'value' => function($model){
                            $status = CommonHelper::changeDateFormat($model->publish_date,'d M Y');
                            return $status;
                        }
                    ],
                    [
                        'attribute' => 'status',
                        'headerOptions' => ['style' => 'width:180px'],
                        'value' => function($model){
                            $status = CommonHelper::newsletterStatusTypeById($model->status);
                            return $status->status_name;
                        }
                    ],
                    //'feature_url:url',
                    // 'feature_excerpt:ntext',
                    // 'feature_img_url:url',
                    // 'stats_title',
                    // 'stats_title_1',
                    // 'stats_amount_1',
                    // 'stats_day_1',
                    // 'stats_percent_1',
                    // 'stats_year_1',
                    // 'stats_title_2',
                    // 'stats_amount_2',
                    // 'stats_day_2',
                    // 'stats_percent_2',
                    // 'stats_year_2',
                    // 'stats_title_3',
                    // 'stats_amount_3',
                    // 'stats_day_3',
                    // 'stats_percent_3',
                    // 'stats_year_3',
                    // 'stats_tweet',
                    // 'created_at',
                    // 'created_by',
                    // 'updated_at',
                    // 'updated_by',
                    // 'status',
                    //['class' => 'unlock\modules\core\grid\ActionColumn'],
                ],
                ]);   ?>
                    </div>
    </div>

</div>

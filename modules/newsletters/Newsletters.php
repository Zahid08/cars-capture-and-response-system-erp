<?php

namespace unlock\modules\newsletters;

/**
 * Class Newsletters
 */
class Newsletters extends \yii\base\Module
{
    /**
     * @var string the namespace that controller classes are in
     */
    public $controllerNamespace = 'unlock\modules\newsletters\controllers';

    /**
    * @var string the namespace that view file are in
    */
    public $viewNamespace = '@unlock/modules/newsletters/views/';
}

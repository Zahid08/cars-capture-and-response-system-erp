<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\jui\DatePicker;
use unlock\modules\core\grid\GridView;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\buttons\NewButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $searchModel unlock\modules\allocation\models\AllocationSearch */
/* @var $dataProvider unlock\modules\core\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Allocation');
?>
<div class="main-container" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-grid-filter">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <div class="admin-grid-content">
        <div class="admin-grid-toolbar">
            <div class="admin-grid-toolbar-left">
                <?= NewButton::widget(['url' => Url::toRoute(['create'])]) ?>
            </div>
            <div class="admin-grid-toolbar-right">
                <?= Summary::widget(['dataProvider' => $dataProvider]) ?>
            </div>
        </div>

        <div class="admin-grid-view">
                            <?php 
                echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
                'pager' => ['class' => LinkPager::className()],
                'columns' => [
                //['class' => 'unlock\modules\core\grid\SerialColumn'],
                	'id',
                    [
                        'attribute' => 'allocation',
                        'format'    => 'html',
                        'value' => function($model){
                            $name = Html::a($model->allocation, ['update', 'id' => $model->id]);
                            return $name;
                        }
                    ],

                    'unit',
                    [
                        'attribute' => 'price',
                        'value' => function($model){
                            return '$'.Yii::$app->formatter->format( $model->price, 'money');
                        }
                    ],
                    [
                        'attribute' => 'agent',
                        'value' => function($model){
                            return CommonHelper::allocationAgentStatus($model->agent);
                        }
                    ],
                    [
                        'attribute' => 'alloc_status',
                        'value' => function($model){
                            return CommonHelper::allocationAllocStatus($model->alloc_status);
                        }
                    ],
                    [
                        'attribute' => 'accepted',
                        'value' => function($model){
                            return CommonHelper::allocationAcceptedStatus($model->accepted);
                        }
                    ],

                    // 'accepted',
                    // 'created_at',
                    // 'created_by',
                    // 'updated_at',
                    // 'updated_by',
                    // 'status',
                [
                'attribute' => 'status',
                'format' => 'status',
                'filter' => Html::activeDropDownList($searchModel, 'status', CommonHelper::statusDropDownList(), ['prompt' => CommonHelper::GRID_FILTER_PROMPT]),
                ],
                //['class' => 'unlock\modules\core\grid\ActionColumn'],
                ],
                ]);   ?>
                    </div>
    </div>

</div>

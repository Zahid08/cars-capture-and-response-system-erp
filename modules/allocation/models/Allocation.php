<?php

namespace unlock\modules\allocation\models;

use Yii;
use yii\helpers\ArrayHelper;
use unlock\modules\core\helpers\CommonHelper;

/**
 * This is the model class for table "{{%allocation}}".
 *
 * @property integer $id
 * @property string $allocation
 * @property string $unit
 * @property string $price
 * @property integer $agent
 * @property integer $alloc_status
 * @property integer $accepted
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property integer $status
 */
class Allocation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%allocation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['agent', 'alloc_status', 'accepted', 'created_by', 'updated_by', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['allocation'], 'string', 'max' => 100],
            [['unit', 'price'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'allocation' => Yii::t('app', 'Allocation'),
            'unit' => Yii::t('app', 'Unit'),
            'price' => Yii::t('app', 'Price'),
            'agent' => Yii::t('app', 'Agent'),
            'alloc_status' => Yii::t('app', 'Alloc Status'),
            'accepted' => Yii::t('app', 'Accepted'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /** Get All Active Allocation Drop Down List*/
    public static function allocationDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }
}

<?php

namespace unlock\modules\allocation\models;

use Yii;
use yii\base\Model;
use unlock\modules\core\data\ActiveDataProvider;
use unlock\modules\allocation\models\Allocation;
use unlock\modules\core\helpers\CommonHelper;

/**
 * AllocationSearch represents the model behind the search form about `unlock\modules\allocation\models\Allocation`.
 */
class AllocationSearch extends Allocation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'agent', 'alloc_status', 'accepted', 'created_by', 'updated_by', 'status'], 'integer'],
            [['allocation', 'unit', 'price', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Allocation::find();

        // add conditions that should always apply here
        $pageSize = isset($params['per-page']) ? intval($params['per-page']) : CommonHelper::GRID_PER_PAGE;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' =>  ['pageSize' => $pageSize],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'agent' => $this->agent,
            'alloc_status' => $this->alloc_status,
            'accepted' => $this->accepted,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'allocation', $this->allocation])
            ->andFilterWhere(['like', 'unit', $this->unit])
            ->andFilterWhere(['like', 'price', $this->price]);

        return $dataProvider;
    }
}

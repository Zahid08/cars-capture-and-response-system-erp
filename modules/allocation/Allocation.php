<?php

namespace unlock\modules\allocation;

/**
 * Class Allocation
 */
class Allocation extends \yii\base\Module
{
    /**
     * @var string the namespace that controller classes are in
     */
    public $controllerNamespace = 'unlock\modules\allocation\controllers';

    /**
    * @var string the namespace that view file are in
    */
    public $viewNamespace = '@unlock/modules/allocation/views/';
}

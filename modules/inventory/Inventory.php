<?php

namespace unlock\modules\inventory;

/**
 * Class Inventory
 */
class Inventory extends \yii\base\Module
{
    /**
     * @var string the namespace that controller classes are in
     */
    public $controllerNamespace = 'unlock\modules\inventory\controllers';

    /**
    * @var string the namespace that view file are in
    */
    public $viewNamespace = '@unlock/modules/inventory/views/';
}

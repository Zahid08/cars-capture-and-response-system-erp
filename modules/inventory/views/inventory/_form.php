<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\jui\DatePicker;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtons;
use unlock\modules\core\buttons\BackButton;

/* @var $this yii\web\View */
/* @var $model unlock\modules\inventory\models\Inventory */
/* @var $form yii\bootstrap\ActiveForm */

$this->title= ($model->isNewRecord) ? Yii::t('app', 'Create Inventory') : Yii::t('app', 'Update Inventory');
?>

<div class="main-container main-container-form" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a(Yii::t('app', 'Home'), Yii::$app->homeUrl) ?></li>
                <li><?= Html::a(Yii::t('app', 'Inventory'), Url::to('index')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-form-container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fa fa-pencil-alt"></i> <?= Html::encode($this->title) ?></h3>
                </div>
                <div class="panel-heading-button">
                    <?= BackButton::widget() ?>
                </div>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
                    /*'options' => ['enctype' => 'multipart/form-data'],*/
                    'validateOnBlur' =>true,
                    'enableAjaxValidation'=>true,
                    'errorCssClass'=>'has-error',
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
                        'options' => ['class' => 'form-group'],
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-2',
                            'offset' => '',
                            'wrapper' => '',
                            'hint' => '',
                        ],
                    ],
                ]); ?>

                <?= $form->field($model, 'development_id')->dropDownList(CommonHelper::developmentDropdownList(), ['prompt' => 'Select a  Development']) ?>

				<?= $form->field($model, 'allocation_id')->dropDownList(CommonHelper::allocationDropdownlist(), ['prompt' => 'Select a  Allocation']) ?>

				<?= $form->field($model, 'unit')->textInput(['maxlength' => true]) ?>

				<?= $form->field($model, 'floor_plan')->textInput(['maxlength' => true]) ?>

				<?= $form->field($model, 'size')->textInput(['maxlength' => true]) ?>

				<?= $form->field($model, 'price')->textInput() ?>

				<?= $form->field($model, 'unit_status')->dropDownList(CommonHelper::unitStatusDropDownList(), ['prompt' => 'Select a UNIT Status']) ?>

                <?= $form->field($model, 'status')->dropDownList(CommonHelper::statusDropDownList()) ?>

                <div class="admin-form-button">
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <?= FormButtons::widget() ?>
                            <?php
                            $inventoryUrl = yii\helpers\Url::to(['inventory/delete?id='.$model->id]);
                            ?>
                            <?= Html::a(Yii::t('yii', '<span class="glyphicon glyphicon-trash"></span> Delete'), '#', [
                                'class' => 'btn btn-danger',
                                'title' => Yii::t('yii', 'Delete'),
                                'aria-label' => Yii::t('yii', 'Delete'),
                                'onclick' => "
                                if (confirm('Are you sure you want to delete this item?')) {
                                    $.ajax('$inventoryUrl', {
                                        type: 'POST'
                                    }).done(function(data) {
                                        $.pjax.reload({container: '#pjax-container'});
                                    });
                                }
                                return false;
                            ",
                            ]); ?>
                        </div>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

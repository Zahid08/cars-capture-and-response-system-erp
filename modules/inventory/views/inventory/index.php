<?php

use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\grid\GridView;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\buttons\NewButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $searchModel unlock\modules\inventory\models\InventorySearch */
/* @var $dataProvider unlock\modules\core\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Inventory');
?>
<div class="main-container" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-grid-filter">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <div class="admin-grid-content">
        <div class="admin-grid-toolbar">
            <div class="admin-grid-toolbar-left">
                <?= NewButton::widget(['url' => Url::toRoute(['create'])]) ?>
            </div>
            <div class="admin-grid-toolbar-right">
                <?= Summary::widget(['dataProvider' => $dataProvider]) ?>
            </div>
        </div>

        <div class="admin-grid-view">
                            <?php 
                echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
                'pager' => ['class' => LinkPager::className()],
                'columns' => [
                //['class' => 'unlock\modules\core\grid\SerialColumn'],
                    [
                        'attribute' => 'id',
                        'format'    => 'html',
                        'value' => function($model){
                            $name = Html::a($model->id, ['update', 'id' => $model->id]);
                            return $name;
                        }
                    ],
                    [
                        'attribute'  => 'development_id',
                        'format' => 'html',
                        'value' => function($model){
                            $developmentName = CommonHelper::developmentListById($model->development_id);
                            if (!empty($developmentName)){
                                $name = Html::a($developmentName['development_name'], ['/developments/developments/view', 'id' => $developmentName['id']]);
                                return $name;
                            }

                        }
                    ],
                    [
                        'attribute' => 'allocation_id',
                        'format' => 'html',
                        'value' => function($model){
                            $allocation = CommonHelper::allocationListById($model->allocation_id);
                            if(!empty($allocation)){
                                $name = Html::a($allocation->allocation, ['/allocation/allocation/update', 'id' => $model->allocation_id]);
                                return $name;
                            }

                        }
                    ],
                    'unit',
                    'floor_plan',
                    'size',
                    [
                        'attribute' => 'price',
                        'value' => function($model){
                                return '$'.Yii::$app->formatter->format( $model->price, 'money');
                        }
                    ],
                    [
                        'attribute' => 'unit_status',
                        'value' => function($model){
                            return CommonHelper::unitStatusDropDownList($model->unit_status);
                        }
                    ],
                    'created_at:datetime',

                    // 'created_by',
                    // 'updated_at',
                    // 'updated_by',
                    // 'status',

                    [
                    'attribute' => 'status',
                    'format' => 'status',
                    'filter' => Html::activeDropDownList($searchModel, 'status', CommonHelper::statusDropDownList(), ['prompt' => CommonHelper::GRID_FILTER_PROMPT]),
                    ],
                //['class' => 'unlock\modules\core\grid\ActionColumn'],
                ],
                ]);   ?>
                    </div>
    </div>

</div>

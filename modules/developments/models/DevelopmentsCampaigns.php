<?php

namespace unlock\modules\developments\models;

use Yii;
use yii\helpers\ArrayHelper;
use unlock\modules\core\helpers\CommonHelper;

/**
 * This is the model class for table "{{%developments_campaigns}}".
 *
 * @property integer $id
 * @property integer $development_id
 * @property integer $media_type
 * @property integer $type
 * @property integer $state
 * @property string $go_date
 * @property integer $budget
 * @property integer $est_deals
 * @property integer $est_return
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property integer $status
 *
 * @property Developments $development
 */
class DevelopmentsCampaigns extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%developments_campaigns}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['development_id', 'media_type', 'type', 'state', 'budget', 'est_deals', 'est_return', 'created_by', 'updated_by', 'status'], 'integer'],
            [['go_date', 'created_at', 'updated_at'], 'safe'],

            //[['development_id'], 'exist', 'skipOnError' => true, 'targetClass' => Developments::className(), 'targetAttribute' => ['development_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'development_id' => Yii::t('app', 'Development Name'),
            'media_type' => Yii::t('app', 'Media Type'),
            'type' => Yii::t('app', 'Type'),
            'state' => Yii::t('app', 'State'),
            'go_date' => Yii::t('app', 'Go Date'),
            'budget' => Yii::t('app', 'Budget'),
            'est_deals' => Yii::t('app', 'Est Deals'),
            'est_return' => Yii::t('app', 'Est Return'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevelopment()
    {
        return $this->hasOne(Developments::className(), ['id' => 'development_id']);
    }

    /** Get All Active DevelopmentsCampaigns Drop Down List*/
    public static function developmentsCampaignsDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }
}

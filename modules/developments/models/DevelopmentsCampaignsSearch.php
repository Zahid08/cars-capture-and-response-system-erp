<?php

namespace unlock\modules\developments\models;

use Yii;
use yii\base\Model;
use unlock\modules\core\data\ActiveDataProvider;
use unlock\modules\developments\models\DevelopmentsCampaigns;
use unlock\modules\core\helpers\CommonHelper;

/**
 * DevelopmentsCampaignsSearch represents the model behind the search form about `unlock\modules\developments\models\DevelopmentsCampaigns`.
 */
class DevelopmentsCampaignsSearch extends DevelopmentsCampaigns
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'development_id', 'media_type', 'type', 'state', 'budget', 'est_deals', 'est_return', 'created_by', 'updated_by', 'status'], 'integer'],
            [['go_date', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DevelopmentsCampaigns::find();

        // add conditions that should always apply here
        $pageSize = isset($params['per-page']) ? intval($params['per-page']) : CommonHelper::GRID_PER_PAGE;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' =>  ['pageSize' => $pageSize],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'development_id' => $this->development_id,
            'media_type' => $this->media_type,
            'type' => $this->type,
            'state' => $this->state,
            'go_date' => $this->go_date,
            'budget' => $this->budget,
            'est_deals' => $this->est_deals,
            'est_return' => $this->est_return,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'status' => $this->status,
        ]);

        return $dataProvider;
    }
}

<?php

namespace unlock\modules\developments\models;

use unlock\modules\contacts\models\ContactsRegistration;
use unlock\modules\contacts\models\ContactsReservations;
use unlock\modules\contacts\models\ContactsRsvp;
use unlock\modules\contacts\models\ContactsSysteminfo;
use Yii;
use yii\helpers\ArrayHelper;
use unlock\modules\core\helpers\CommonHelper;

/**
 * This is the model class for table "{{%developments}}".
 *
 * @property integer $id
 * @property integer $development_id
 * @property integer $developer_id
 * @property string $development_name
 * @property integer $sales_status
 * @property integer $grade
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 */
class Developments extends \yii\db\ActiveRecord
{

    public $performance;
    public $developer_id;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%developments}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['development_id', 'created_by', 'updated_by','assignment_id'], 'integer'],
            [['launch_date','development_address','appointment_content','appointment_content_agent','appointment_content_purchaser', 'development_overview', 'development_details'],'safe'],
            ['assignment_id','required','on'=>'assignment_required'],
            [['development_name'], 'required'],
            [['image_url', 'excerpt', 'sales_status', 'grade', 'development_url', 'created_at', 'updated_at','assignment_id','development_image_url'], 'safe'],
            [['development_name'], 'string', 'max' => 90],
            //[['developer_id'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'development_id' => Yii::t('app', 'ID'),
            'development_name' => Yii::t('app', 'Development Name'),
            'development_address' => Yii::t('app', 'Development Address'),
            'development_image_url' => Yii::t('app', 'Development Hero Image'),
            'appointment_content' => Yii::t('app', 'Appointment Content'),
            'appointment_content_agent' => Yii::t('app', 'Appointment Content'),
            'appointment_content_purchaser' => Yii::t('app', 'Appointment Content'),
            'assignment_id' => Yii::t('app', 'Assigned To'),
            'developer_id' => Yii::t('app', 'Developer Name'),
            'development_overview' => Yii::t('app', 'Development Overview'),
            'development_url' => Yii::t('app', 'Development URL'),
            'development_details' => Yii::t('app', 'Development Details'),
            'sales_status' => Yii::t('app', 'Sales Status'),
            'launch_date' => Yii::t('app', 'Launch Date'),
            'performance' => Yii::t('app', 'Performance'),
            'grade' => Yii::t('app', 'Grade'),
            'excerpt' => Yii::t('app', 'Excerpt'),
            'image_url' => Yii::t('app', 'Image URL'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /** Get All Active Developments Drop Down List*/
    public static function developmentsDropDownList()
    {
        $model = self::find()
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    public function  getRegistration(){
        return $this->hasMany(ContactsRegistration::className(), ['development_id' => 'development_id']);
    }
    public function  getReservation(){
        return $this->hasMany(ContactsReservations::className(), ['development_id' => 'id']);
    }
    public function  getRsvp(){
        return $this->hasMany(ContactsRsvp::className(), ['development_id' => 'id']);
    }
    public function  getAppointment(){
        return $this->hasMany(ContactsSysteminfo::className(), ['development_id' => 'id']);
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }
}

<?php

namespace unlock\modules\developments\models;

use Yii;
use yii\helpers\ArrayHelper;
use unlock\modules\core\helpers\CommonHelper;

/**
 * This is the model class for table "{{%developments}}".
 *
 * @property integer $id
 * @property integer $development_id
 * @property integer $developer_id
 * @property string $development_name
 * @property integer $sales_status
 * @property integer $grade
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 */
class DevelopersDevelopmentsRelation extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%developers_developments_relation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['development_id', 'developer_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'development_id' => Yii::t('app', 'Development ID'),
            'developer_id' => Yii::t('app', 'Developer ID'),
        ];
    }

}

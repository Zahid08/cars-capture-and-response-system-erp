<?php

namespace unlock\modules\developments\models;

use Yii;
use yii\base\Model;
use unlock\modules\core\data\ActiveDataProvider;
use unlock\modules\developments\models\Developments;
use unlock\modules\core\helpers\CommonHelper;

/**
 * DevelopmentsSearch represents the model behind the search form about `unlock\modules\developments\models\Developments`.
 */
class DevelopmentsSearch extends Developments
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'development_id', 'created_by', 'updated_by','assignment_id'], 'integer'],
            [['excerpt', 'sales_status', 'grade', 'development_name', 'development_url', 'created_at', 'updated_at','developer_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Developments::find();

        // add conditions that should always apply here
        $pageSize = isset($params['per-page']) ? intval($params['per-page']) : CommonHelper::GRID_PER_PAGE;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> [
                'defaultOrder' => ['created_at' => SORT_DESC]
            ],
            'pagination' =>  ['pageSize' => $pageSize],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
          ////   'development_id' => $this->development_id,
            'sales_status' => $this->sales_status,
            'assignment_id' => $this->assignment_id,
            'grade' => $this->grade,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);
        
        $query->andFilterWhere(['like', 'development_name', $this->development_name]);
        if($this->developer_id){
           
            $developPk = \unlock\modules\developers\models\Developers::findOne($this->developer_id);
            if($developPk && $developPk->developer_id!=0) {
                
           
            $develop = \unlock\modules\developers\models\Developers::find()->select(['id','developer_id','developer_name'])->where('developer_id=:developer_id',[':developer_id'=>$developPk->developer_id])->all();
            $developers_id = [];
            if($develop){
                foreach ($develop as $k=>$val){
                   $developers_id[]= $val->developer_id;
                }
            }
            $development_id = [];
            if($developers_id){
                $developMent = DevelopersDevelopmentsRelation::find()
                        ->where(['IN', 'developer_id', $developers_id])
                        ->all();
                
                if($developMent){
                    foreach ($developMent as $k=>$val){
                       $development_id[]= $val->development_id;
                    }
                }               
            }
            if($development_id){
              $query->andFilterWhere(['IN', 'development_id', $development_id]);  
            }  
             }
            
        }      
        ///echo $query->createCommand()->rawSql;
        return $dataProvider;
    }
}

<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtons;
use unlock\modules\core\buttons\BackButton;

/* @var $this yii\web\View */
/* @var $model unlock\modules\developments\models\Developments */
/* @var $form yii\bootstrap\ActiveForm */

$this->title= ($model->isNewRecord) ? Yii::t('app', 'Create Developments') : Yii::t('app', 'Update Developments');
?>

<div class="main-container main-container-form" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a(Yii::t('app', 'Home'), Yii::$app->homeUrl) ?></li>
                <li><?= Html::a(Yii::t('app', 'Developments'), Url::to('index')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-form-container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fas fa-pencil-alt"></i> <?= Html::encode($this->title) ?></h3>
                </div>
                <div class="panel-heading-button">
                    <?= BackButton::widget() ?>
                </div>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin([
                    'layout' => 'default',  //  'default', 'horizontal' or 'inline'
                    /*'options' => ['enctype' => 'multipart/form-data'],*/
                    'validateOnBlur' =>true,
                    'enableAjaxValidation'=>true,
                    'errorCssClass'=>'has-error',
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
                        'options' => ['class' => 'form-group'],
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-2',
                            'offset' => '',
                            'wrapper' => '',
                            'hint' => '',
                        ],
                    ],
                ]); ?>

                <div class="row">
                    <div class="col-md-3 developent_create">
                        <?= $form->field($model, 'development_name')->textInput() ?>
                    </div>
                    <div class="col-md-2 developent_create">
                            <?= $form->field($model, 'sales_status')->dropDownList(CommonHelper::developmentSalesStatusDropDownList(), ['prompt' => 'Select Status']) ?>
                    </div>
                    <div class="col-md-2 developent_create">
                        <?php
                        echo $form->field($model, 'developer_id')->widget(Select2::classname(), [
                            'data' => CommonHelper::developersDropdownListById(),
                            'language' => 'en',
                            'options' => ['placeholder' => 'Select Developer'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-md-3 developent_create">
                    <?= $form->field($model, 'launch_date')->widget(
                        DatePicker::className(), [
                        'name' => 'date_of_birth',
                        'value' => '12-31-2010',
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]);?>
                    </div>
                    <div class="col-md-2 developent_create">
                        <label for="developments-grade" class="control-label">Grade</label>
                        <div class="col-sm-10">
                            <select id="developments-grade" name="Developments[grade]" class="form-control" aria-invalid="false">
                                <option value="5" title="Make a Call">C</option>
                                <option value="1" title="Advertising + Event">A+</option>
                                <option value="2" title="Advertising">A</option>
                                <option value="4" title="Basic Web Page">B</option>
                                <option value="6" title="Don't bother">D</option></select>
                            <p class="help-block help-block-error"></p></div>
                    </div>
                </div>
                <div class="admin-form-button" style="margin-top: 50px;">
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-4">
                            <?= FormButtons::widget() ?>
                        </div>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

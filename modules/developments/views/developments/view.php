<?php

use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\widgets\DetailView;
use unlock\modules\core\buttons\BackButton;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\developments\models\DevelopmentsCampaigns;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\buttons\FormButtons;
use unlock\modules\usermanager\user\models\User;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model unlock\modules\developments\models\Developments */

$this->title = Yii::t('app', 'View Developments');
?>

<div class="main-container main-container-view" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li><?= Html::a(Yii::t('app', 'Developments'), Url::to('index')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-view-container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fa fa-eye"></i> <?= Yii::t('app', 'Development Details') ?> - <?= Html::encode($model->development_id) ?></h3>
                </div>
                <div class="panel-heading-button">
                    <?= BackButton::widget() ?>
                </div>
            </div>
            <?php
            Modal::begin([
                'header' => '<h4>Add Development Campaign</h4>',
                'id'     => 'model-campaign',
                'size'   => 'model-lg',
            ]);

            echo "<div id='add-campaign-modelContent'></div>";

            Modal::end();
            ?>
            <div class="panel-body">
                <div class="developer-heading">
                    <div class="row">
                        <div class="col-md-5 col-12 developer-name">
                            <?php
                            $developersList = CommonHelper::developerListByDevelopementId($model->development_id);
                            ?>
                            <h3 style="margin: 10px 0 !important;font-size: 21px;"><?php echo $model->development_name;?></h3>
                        </div>
                        <div class="col-md-2 col-12 developer-grade" style="text-align: left;">
                            <h3 style="margin: 11px 0px !important;font-size: 21px;">Grade: <span class="grade-value"><?php echo CommonHelper::developerGradeDropDownList($model->grade);?></span></h3>
                        </div>
                        <div class="col-md-2 col-12 developer-id">
                            <h3 style="width: 150px;margin: 10px -23px !important;font-size: 21px;">ID: <span class="grade-value"><?php echo $model->development_id;?></span></h3>
                        </div>
                       
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-12 developer-launch-date">
                            <h3 style="margin: 10px 0px !important;font-size: 21px;">Launch Date: <?php echo CommonHelper::changeDateFormat($model->launch_date, 'd M Y');?></h3>
                        </div>
                         <?php $form = ActiveForm::begin([
                            'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
                             'action'=>['/developments/developments/update-assignment','id'=>$model->id],
                             'options' => ['method' => 'post'],
                            /*'options' => ['enctype' => 'multipart/form-data'],*/
                            'validateOnBlur' =>true,
                            'enableAjaxValidation'=>true,
                            'errorCssClass'=>'has-error',
                        ]); ?>
                        <div class="col-md-5 col-12 developer-id" style="text-align: left;">
                            <h3 style="margin: 10px 0px !important;font-size: 21px;">
                                <div class="row">
                                    <div class="col-md-6 " style="text-align: left;font-size: 21px;">
                                        <?= $model->getAttributeLabel('assignment_id')?> :
                                    </div>
                                    <div class="col-md-6" style="text-align: left; padding: 0px;">
                                       <?php // $form->field($model, 'assignment_id',['template' => '{input}', 'options' => ['tag' => null]])->dropDownList(User::getActiveUser()) ?>  
                                        
                                        <?php 
                                        echo $form->field($model, 'assignment_id',['template' => '{input}', ])->widget(Select2::classname(), [
                                            'data' => User::getActiveSalesUser(),
                                            'language' => 'en',
                                            'options' => ['placeholder' => 'Select Assignment To'],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ])->label(false);

                                        ?>
                                    </div>                              
                              </div>
                            </h3>
                        </div>
                        <div class="col-md-2 col-12 developer-id" style="text-align: left;font-size: 21px;">
                            &nbsp;&nbsp;&nbsp;<?= Html::submitButton('Update', ['class' =>'btn btn-success','style'=>'margin-top:8px']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-12">
                            <h3 style="text-transform: none;font-size: 21px;"><?php echo ucfirst($developersList);?></h3>
                        </div>
                    </div>
                </div>
                <div class="divider"></div>
                <div class="row text-center">
                    <div class="col-md-3">
                        <?php
                        $totalBudget = DevelopmentsCampaigns::find()->where(['development_id' => $model->development_id])->sum('budget');
                        ?>
                        <h3 style="margin-top: 5px;margin-bottom: 5px;">Budget: <span style="color: red"><?php echo '$'.Yii::$app->formatter->format( $totalBudget, 'money'); ?></span></h3>
                    </div>
                    <div class="col-md-3">
                        <h3 style="margin-top: 5px;margin-bottom: 5px;">Used: <span style="color: red">$19.32</span></h3>
                    </div>
                    <div class="col-md-3">
                        <h3 style="margin-top: 5px;margin-bottom: 5px;">Plan: <span style="color: green">$80.00</span></h3>
                    </div>
                    <div class="col-md-3">
                        <h3 style="margin-top: 5px;margin-bottom: 5px;">Closed: <span style="color: green">$27.00</span></h3>
                    </div>
                </div>
                <div class="divider"></div>
                <div id="app" class="tab_section">
                    <tabs>
                        <tab name="Plan" :selected="true">
                            <br>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group field-developmentscampaigns-budget has-success">
                                        <label class="control-label col-sm-5" for="developmentscampaigns-budget">Estimated Number of Allocated Units</label>
                                        <div class="col-sm-7">
                                            <input type="text" id="developmentscampaigns-budget" class="form-control allocation_units" name="DevelopmentsCampaigns[budget]" aria-invalid="false" value="1">

                                            <p class="help-block help-block-error "></p>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group field-developmentscampaigns-budget has-success">
                                        <label class="control-label col-sm-5" for="developmentscampaigns-budget">Starting Sales Price</label>
                                        <div class="col-sm-7">
                                            <input type="text" id="developmentscampaigns-budget" class="form-control sale_price" name="DevelopmentsCampaigns[budget]" aria-invalid="false" value="$300,000 ">

                                            <p class="help-block help-block-error "></p>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group field-developmentscampaigns-budget has-success">
                                        <label class="control-label col-sm-5" for="developmentscampaigns-budget">Total Possible Gross Receipts</label>
                                        <div class="col-sm-7">
                                            <input type="text" id="developmentscampaigns-budget" class="form-control total_reciepts" name="DevelopmentsCampaigns[budget]" aria-invalid="false">

                                            <p class="help-block help-block-error "></p>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group field-developmentscampaigns-budget has-success">
                                        <label class="control-label col-sm-5" for="developmentscampaigns-budget">Estimated Agent Deals</label>
                                        <div class="col-sm-7">
                                            <input type="text" id="developmentscampaigns-budget" class="form-control" name="DevelopmentsCampaigns[budget]" aria-invalid="false" value="0">

                                            <p class="help-block help-block-error "></p>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group field-developmentscampaigns-budget has-success">
                                        <label class="control-label col-sm-5" for="developmentscampaigns-budget">Agent Commission</label>
                                        <div class="col-sm-7">
                                            <input type="text" id="developmentscampaigns-budget" class="form-control" name="DevelopmentsCampaigns[budget]" aria-invalid="false">

                                            <p class="help-block help-block-error "></p>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group field-developmentscampaigns-budget has-success">
                                        <label class="control-label col-sm-5" for="developmentscampaigns-budget">Brokerage Fees</label>
                                        <div class="col-sm-7">
                                            <input type="text" id="developmentscampaigns-budget" class="form-control" name="DevelopmentsCampaigns[budget]" aria-invalid="false">

                                            <p class="help-block help-block-error "></p>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group field-developmentscampaigns-budget has-success">
                                        <label class="control-label col-sm-5" for="developmentscampaigns-budget">Campaign Costs</label>
                                        <div class="col-sm-7">
                                            <input type="text" id="developmentscampaigns-budget" class="form-control" name="DevelopmentsCampaigns[budget]" aria-invalid="false">

                                            <p class="help-block help-block-error "></p>
                                        </div>
                                    </div>
                                    <div class="divider"></div>
                                    <div class="form-group field-developmentscampaigns-budget has-success">
                                        <label class="control-label col-sm-5" for="developmentscampaigns-budget">Net to CONNECT b4 Expense</label>
                                        <div class="col-sm-7">
                                            <input type="text" id="developmentscampaigns-budget" class="form-control" name="DevelopmentsCampaigns[budget]" aria-invalid="false">

                                            <p class="help-block help-block-error "></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6"></div>
                                        <div class="col-md-6"><button class="btn btn-default">Update</button></div>
                                    </div>

                                </div>
                            </div>

                        </tab>
                        <tab name="Profile">
                            <?php $form = ActiveForm::begin([
                                'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
                                'action'=>['/developments/developments/update-hero-image','id'=>$model->id],
                                'options' => ['method' => 'post'],
                                'validateOnBlur' =>true,
                                'enableAjaxValidation'=>false,
                                'errorCssClass'=>'has-error',
                                'fieldConfig' => [
                                    'template' => "{label}\n<div class=\"col-sm-12\">\n{input}\n{hint}\n{error}\n</div>",
                                    'options' => ['class' => 'form-group'],
                                    'horizontalCssClasses' => [
                                        'label' => 'col-sm-3',
                                        'offset' => '',
                                        'wrapper' => '',
                                        'hint' => '',
                                    ],
                                ],
                            ]); ?>

                            <div class="col-md-3">
                                <label for="developments-development_image_url" class="control-label col-sm-12">Development Overview</label>
                            </div>
                            <div class="col-md-8">
                                <?= $form->field($model, 'development_overview')->widget(\yii\redactor\widgets\Redactor::className())->label(false) ?>
                            </div>

                            <div class="col-md-3">
                                <label for="developments-development_image_url" class="control-label col-sm-12">Development Details</label>
                            </div>
                            <div class="col-md-8">
                                <?= $form->field($model, 'development_details')->widget(\yii\redactor\widgets\Redactor::className())->label(false) ?>
                            </div>

                            <div class="col-md-3">
                                <label for="developments-development_image_url" class="control-label col-sm-12">Development Hero Image</label>
                            </div>
                            <div class="col-md-8">
                                <?= $form->field($model, 'development_image_url')->textInput()->label(false); ?>
                            </div>
                            <div id="appointment_content">
                                <div class="col-md-3">
                                    <label for="developments-appointment_content" class="control-label col-sm-12">Appointment Content</label>
                                </div>
                                <div class="col-md-8">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#home">Purchaser</a></li>
                                        <li><a data-toggle="tab" href="#menu1">Prospect</a></li>
                                        <li><a data-toggle="tab" href="#menu2">Agent</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div id="home" class="tab-pane fade in active">
                                            <?= $form->field($model, 'appointment_content_purchaser')->widget(\yii\redactor\widgets\Redactor::className())->label(false) ?>
                                        </div>
                                        <div id="menu1" class="tab-pane fade">
                                            <?= $form->field($model, 'appointment_content')->widget(\yii\redactor\widgets\Redactor::className())->label(false) ?>
                                        </div>
                                        <div id="menu2" class="tab-pane fade">
                                            <?= $form->field($model, 'appointment_content_agent')->widget(\yii\redactor\widgets\Redactor::className())->label(false) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2 col-12 developer-id" style="margin-left: 237px;">
                                &nbsp;&nbsp;&nbsp;<?= Html::submitButton('Update', ['class' =>'btn btn-success','style'=>'margin-top:8px']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>

                        </tab>


                        <tab name="Assets">

                            <?php
                            Modal::begin([
                                'header' => '<h4>Edit Assets</h4>',
                                'id'     => 'model-interaction',
                                'size'   => 'model-lg',
                            ]);

                            echo "<div id='edit-assignmentsAssets-modelContent'></div>";

                            Modal::end();
                            ?>

                            <?php
                            Modal::begin([
                                'header' => '<h4>Add More Assets</h4>',
                                'id'     => 'addmore-assignmentsAssets-model-interaction',
                                'size'   => 'model-lg',
                            ]);

                            echo "<div id='addmore-assignmentsAssets-modelContent'></div>";

                            Modal::end();


                            $assignmentData        =   CommonHelper::getAssignmentsByDevelopmentId($model->development_id);
                            $assignmentID          =   !empty($assignmentData) ? $assignmentData->id: '';

                            if($assignmentID){
                            ?>

                            <h4>Units Assets</h4>
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">ID</th>
                                    <th style="width: 20%;">Category</th>
                                    <th style="width: 20%;">Name</th>
                                    <th style="width: 35%;">File</th>
                                    <th style="width: 20%;"></th>
                                </tr>
                                </thead>
                                <?php
                                $assignments_assets    =   array();
                                $assignments_assets    =   CommonHelper::getAssignmentsAssetsByAssignmentID($assignmentID);

                                if($assignments_assets) {
                                    foreach ($assignments_assets as $assignments) {

                                        $upload_file_url = '';
                                        if ($assignments->upload_file) {
                                            $upload_file_url = $assignments->upload_file;
                                        } else if ($assignments->upload_url) {
                                            $upload_file_url = $assignments->upload_url;
                                        } else {
                                            $upload_file_url = 'N/A';
                                        }

                                        $assetDeleteUrl = yii\helpers\Url::to(['/assignments/assignments/units-assets-delete?id=' . $assignments->id]);

                                        $assignmentName = CommonHelper::getAssignmentsById($assignments->assignment_id);
                                        $developmentName = CommonHelper::developmentListById($assignmentName->development_id);
                                        ?>
                                        <tr>
                                            <td> <?= $assignments->id ?> </td>
                                            <td> <?= $developmentName->development_name ?> </td>
                                            <td> <?= $assignments->assignment_assets ?> </td>
                                            <td><a href="<?= $upload_file_url ?>"
                                                   target="_blank"><?= substr($upload_file_url, 0, 40) ?>....</a>
                                            </td>
                                            <td>
                                                <?= Html::a(Yii::t('yii', '<span class="glyphicon glyphicon-trash"></span> Remove'), $assetDeleteUrl, [
                                                    'class' => 'btn btn-danger small-button-cls',
                                                    'title' => Yii::t('yii', 'Remove'),
                                                    'style' => Yii::t('yii', 'padding:3px 8px;'),
                                                    'aria-label' => Yii::t('yii', 'Remove'),
                                                    'onclick' => "
                                                                if (confirm('Are you sure you want to delete this item?')) {
                                                                    return true;
                                                                }
                                                                return false;
                                                            ",
                                                ]);
                                                ?>
                                                <?= Html::button('<span class="glyphicon glyphicon-edit"></span> Edit', ['id' => 'edit-assignmentsAssets-modelButton' . $assignments->id . '', 'value' => \yii\helpers\Url::to(['/assignments/assignments/units-assets-update?id=' . $assignments->id . '&type=development&developmentID='.$model->id.'']), 'class' => 'btn btn-primary small-button-cls']) ?>
                                            </td>
                                        </tr>

                                        <script>
                                            $(function () {
                                                //load edit-assignmentsAssets-modelButton modal
                                                $('#edit-assignmentsAssets-modelButton<?= $assignments->id ?>').click(function () {
                                                    $('#model-interaction').modal('show')
                                                        .find('#edit-assignmentsAssets-modelContent')
                                                        .load($(this).attr('value'));
                                                });
                                            });
                                        </script>

                                        <?php
                                    }
                                }
                                //echo '<pre>'; print_r($assignments_assets); echo '</pre>';
                                ?>

                            </table>

                            <p style="text-align: right">
                                <?= Html::button('<span class="glyphicon glyphicon-plus"></span> Add New One', ['id' => 'addmore-assignmentsAssets-modelButton', 'value' => \yii\helpers\Url::to(['/assignments/assignments/units-assets-addmore?id='.$assignmentID.'&type=development&developmentID='.$model->id.'']), 'class' => 'btn btn-primary small-button-cls']) ?>
                            </p>

                            <script>
                                $(function(){
                                    //load edit-assignmentsAssets-modelButton modal
                                    $('#addmore-assignmentsAssets-modelButton').click(function(){
                                        $('#addmore-assignmentsAssets-model-interaction').modal('show')
                                            .find('#addmore-assignmentsAssets-modelContent')
                                            .load($(this).attr('value'));
                                    });
                                });
                            </script>

                            <h4>Building Assets</h4>
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">ID</th>
                                    <th style="width: 20%;">Category</th>
                                    <th style="width: 20%;">Name</th>
                                    <th style="width: 35%;">File</th>
                                    <th style="width: 20%;"></th>
                                </tr>
                                </thead>
                                <?php
                                $building_assets    =   array();
                                $building_assets    =   CommonHelper::getBuildingAssetsByAssignmentID($assignmentID);
                                if($building_assets){
                                    foreach ($building_assets as $building){

                                        $upload_file_url   = '';
                                        if($building->upload_file){
                                            $upload_file_url   = $building->upload_file;
                                        }else if($building->upload_url){
                                            $upload_file_url   = $building->upload_url;
                                        }else{
                                            $upload_file_url   = 'N/A';
                                        }

                                        $assetDeleteUrl     = yii\helpers\Url::to(['/assignments/assignments/building-assets-delete?id='.$building->id]);

                                        $assignmentName     = CommonHelper::getAssignmentsById($building->assignment_id);
                                        $developmentName    = CommonHelper::developmentListById($assignmentName->development_id);
                                        ?>
                                        <tr>
                                            <td> <?= $building->id ?> </td>
                                            <td> <?= $developmentName->development_name ?> </td>
                                            <td> <?= $building->building_assets ?> </td>
                                            <td> <a href="<?= $upload_file_url ?>" target="_blank"><?= substr($upload_file_url, 0, 40) ?>....</a> </td>
                                            <td>
                                                <?= Html::a(Yii::t('yii', '<span class="glyphicon glyphicon-trash"></span> Remove'), $assetDeleteUrl, [
                                                    'class' => 'btn btn-danger small-button-cls',
                                                    'title' => Yii::t('yii', 'Remove'),
                                                    'style' => Yii::t('yii', 'padding:3px 8px;'),
                                                    'aria-label' => Yii::t('yii', 'Remove'),
                                                    'onclick' => "
                                                                    if (confirm('Are you sure you want to delete this item?')) {
                                                                        return true;
                                                                    }
                                                                    return false;
                                                                ",
                                                ]);
                                                ?>
                                                <?= Html::button('<span class="glyphicon glyphicon-edit"></span> Edit', ['id' => 'edit-buildingAssets-modelButton'.$building->id.'', 'value' => \yii\helpers\Url::to(['/assignments/assignments/building-assets-update?id='.$building->id.'&type=development&developmentID='.$model->id.'']), 'class' => 'btn btn-primary small-button-cls']) ?>
                                            </td>
                                        </tr>

                                        <script>
                                            $(function(){
                                                //load edit-buildingAssets-modelButton modal
                                                $('#edit-buildingAssets-modelButton<?= $building->id ?>').click(function(){
                                                    $('#model-interaction').modal('show')
                                                        .find('#edit-assignmentsAssets-modelContent')
                                                        .load($(this).attr('value'));
                                                });
                                            });
                                        </script>

                                        <?php
                                    }
                                }
                                //echo '<pre>'; print_r($assignments_assets); echo '</pre>';
                                ?>

                            </table>

                            <p style="text-align: right">
                                <?= Html::button('<span class="glyphicon glyphicon-plus"></span> Add New One', ['id' => 'addmore-buildingAssets-modelButton', 'value' => \yii\helpers\Url::to(['/assignments/assignments/building-assets-addmore?id='.$assignmentID.'&type=development&developmentID='.$model->id.'']), 'class' => 'btn btn-primary small-button-cls']) ?>
                            </p>

                            <script>
                                $(function(){
                                    //load edit-assignmentsAssets-modelButton modal
                                    $('#addmore-buildingAssets-modelButton').click(function(){
                                        $('#addmore-assignmentsAssets-model-interaction').modal('show')
                                            .find('#addmore-assignmentsAssets-modelContent')
                                            .load($(this).attr('value'));
                                    });
                                });
                            </script>

                            <?php
                            }else{ ?>
                                <p style="text-align: center">
                                    <?php
                                        echo Html::a('1st Need to Add a Assignment Under This Development', ['/assignments/assignments/create', 'dev_id' => $model->development_id]);
                                    ?>
                                </p>
                            <?php }
                            ?>

                        </tab>

                        <tab name="Campaigns">
                           <div style="overflow: hidden;" class="campaigns-wrapper">
                               <?php
                               $developmentCampaigns = CommonHelper::developmentCampaignsListById($model->development_id);
                               ?>
                               <table class="table table-striped table-bordered">
                                   <thead>
                                   <tr>
                                       <th>ID</th>
                                       <th>Media </th>
                                       <th>Type</th>
                                       <th>State</th>
                                       <th>Go Date</th>
                                       <th>Budget</th>
                                       <th>Est Deals</th>
                                       <th>Est Return</th>
                                   </tr>
                                   </thead>
                                   <tbody>
                                   <?php if(!empty($developmentCampaigns)){ foreach ($developmentCampaigns as $campaigns){ ?>
                                       <tr>
                                           <td>
                                               <?= Html::a($campaigns->id, ['developments-campaigns/update', 'id' => $campaigns->id]) ?>
                                           </td>
                                           <td><?php echo CommonHelper::leadSourceByBrandId($campaigns->media_type); ?></td>
                                           <td><?php echo CommonHelper::campaignsTypeDropDownList($campaigns->type); ?></td>
                                           <td><?php echo CommonHelper::campaignsStateDropDownList($campaigns->state); ?></td>
                                           <td><?php echo CommonHelper::changeDateFormat($campaigns->go_date, 'd M Y'); ?></td>
                                           <td style="text-align: right;"><?php echo '$'.Yii::$app->formatter->format( $campaigns->budget, 'money'); ?></td>
                                           <td><?php echo $campaigns->est_deals; ?></td>
                                           <td style="text-align: right;"><?php echo '$'.Yii::$app->formatter->format( $campaigns->est_return, 'money'); ?></td>
                                       </tr>
                                   <?php }} ?>
                                   </tbody>
                               </table>
                               <div class="clearfix"></div>
                               <table style="width: 360px;float: right;" class="table table-striped table-bordered">
                                   <?php
                                   $totalBudget = DevelopmentsCampaigns::find()->where(['development_id' => $model->development_id])->sum('budget');
                                   $totalDeals = DevelopmentsCampaigns::find()->where(['development_id' => $model->development_id])->sum('est_deals');
                                   $totalEstreturn = DevelopmentsCampaigns::find()->where(['development_id' => $model->development_id])->sum('est_return');
                                   ?>
                                   <tbody>
                                   <tr>
                                       <td><h4 style="margin: 0;color: #4677ba;">Total Budget</h4></td>
                                       <td><h4 style="margin: 0;color: red;"><?php echo '$'.Yii::$app->formatter->format( $totalBudget, 'money'); ?></h4></td>
                                   </tr>
                                   <tr>
                                       <td><h4 style="margin: 0;color: #4677ba;">Total Estimate Deals</h4></td>
                                       <td><h4 style="margin: 0;color: green;"><?php echo $totalDeals;?></h4></td>
                                   </tr>
                                   <tr>
                                       <td><h4 style="margin: 0;color: #4677ba;">Estimate to CONNECT</h4></td>
                                       <td><h4 style="margin: 0;color: green;"><?php echo '$'.Yii::$app->formatter->format( $totalEstreturn, 'money'); ?></h4></td>
                                   </tr>

                                   </tbody>
                               </table>
                           </div>
                        </tab>
                        <tab name="Registrations">
                                <div class="col-md-12" style="margin-bottom: 11px;">
                                    <div class="col-md-4">
                                        <h4>Total Registrations : <span id="total_registration" class="color-content"></span></h4>
                                    </div>
                                    <div class="col-md-4">
                                        <h4>Total Prospects : <span id="total_prospect" class="color-content"></span></h4>
                                    </div>
                                    <div class="col-md-4">
                                        <h4>Total Agents : <span id="total_agent" class="color-content"></span></h4>
                                    </div>
                                </div>
                                <div class="divider"></div>

                            <table id='match-table' class='display dataTable table table-striped table-bordered'>
                                <thead>
                                <tr>
                                    <th style="display: none;">ID</th>
                                    <th>ID</th>
                                    <th>Registration Name </th>
                                    <th>Phone </th>
                                    <th>Type</th>
                                    <th>Media Source</th>
                                    <th>Reg Date</th>
                                    <th>Reg Status</th>
                                </tr>
                                </thead>
                            </table>
                        </tab>
                        <tab name="RSVP">
                            <div class="col-md-12" style="margin-bottom: 11px;">
                                <div class="col-md-4">
                                    <h4>Total Rsvp : <span id="total_rsvp" class="color-content"></span></h4>
                                </div>
                                <div class="col-md-4">
                                    <h4>Total Prospects : <span id="total_rsvp_prospect" class="color-content"></span></h4>
                                </div>
                                <div class="col-md-4">
                                    <h4>Total Agents : <span id="total_rsvp_agent" class="color-content"></span></h4>
                                </div>
                            </div>
                            <div class="divider"></div>

                            <table id='match-table-rsvp' class='display dataTable table table-striped table-bordered'>
                                <thead>
                                <tr>
                                    <th style="display: none;">ID</th>
                                    <th>ID</th>
                                    <th>Contact</th>
                                    <th>Phone </th>
                                    <th>Type</th>
                                    <th>Media Source</th>
                                    <th>Submission Date</th>
                                    <th>RSVP Status</th>
                                </tr>
                                </thead>
                            </table>
                        </tab>
                        <tab name="Reservations">
                            <?php
                                $reservations = CommonHelper::developmentIdByReservationList($model->development_id);
                            ?>
                            <div style="overflow: hidden;" class="reservations-wrapper">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Reservation</th>
                                        <th>1st</th>
                                        <th>2nd</th>
                                        <th>3rd</th>
                                        <th>Res Date</th>
                                        <th>Res Status</th>
                                        <th>Agent</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if(!empty($reservations)){foreach ($reservations as $reservation){
                                        $contactName = CommonHelper::contactIdByName($reservation->contact_id);
                                        ?>
                                        <tr>
                                            <td>
                                                <?php
                                                echo Html::a($contactName, ['/contacts/contacts-reservations/update', 'id' => $reservation->id]);
                                                ?>
                                            </td>
                                            <td><?php echo $reservation->floor_plans_1st_choice; ?></td>
                                            <td><?php echo $reservation->floor_plans_2nd_choice; ?></td>
                                            <td><?php echo $reservation->floor_plans_3rd_choice; ?></td>
                                            <td><?php echo CommonHelper::changeDateFormat($reservation->created_at, 'd M Y'); ?></td>
                                            <td><?php echo CommonHelper::reservationStatusById($reservation->status);?></td>
                                            <td>
                                                <?php
                                                    $contactDetails = CommonHelper::contactIdByContacts($reservation->contact_id);
                                                    $agentCheck = \unlock\modules\contacts\models\Agent::find()->where(['email' => $contactDetails])->count();
                                                    if(!empty($agentCheck)){
                                                        echo 'Agent';
                                                    }else{
                                                        echo 'Prospect';
                                                    }
                                                ?>
                                            </td>
                                        </tr>
                                    <?php }} ?>
                                    </tbody>
                                </table>
                                <div class="clearfix"></div>
                                <table style="width: 360px;float: right;" class="table table-striped table-bordered">
                                    <?php
                                    $totalReservations = \unlock\modules\contacts\models\ContactsReservations::find()->where(['development_id' => $model->development_id])->count();
                                    $totalSubmitted = \unlock\modules\contacts\models\ContactsReservations::find()->andWhere(['development_id' => $model->development_id])->andWhere(['status' => 5])->count();
                                    ?>
                                    <tbody>
                                    <tr>
                                        <td><h4 style="margin: 0;color: #4677ba;">Total Reservations</h4></td>
                                        <td><h4 style="margin: 0;color: red;"><?php echo $totalReservations; ?></h4></td>
                                    </tr>
                                    <tr>
                                        <td><h4 style="margin: 0;color: #4677ba;">Total Submitted</h4></td>
                                        <td><h4 style="margin: 0;color: green;"><?php echo $totalSubmitted;?></h4></td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </tab>

                        <tab name="Appointments">
                            <div class="col-md-12" style="margin-bottom: 11px;">
                                <div class="col-md-4">
                                    <h4>Total Appointments : <span id="total_appointment" class="color-content"></span></h4>
                                </div>
                                <div class="col-md-4">
                                    <h4>Total Prospects Appointments : <span id="total_appointment_prospect" class="color-content"></span></h4>
                                </div>
                                <div class="col-md-4">
                                    <h4>Total Agents Appointments : <span id="total_appointment_agent" class="color-content"></span></h4>
                                </div>
                            </div>
                            <div class="divider"></div>
                            <table class="table table-striped table-bordered" id="match-table-appointment">
                                <thead>
                                <tr>
                                    <th>AppID</th>
                                    <th>Contact</th>
                                    <th>Phone </th>
                                    <th>Type</th>
                                    <th>Media Source</th>
                                    <th>On</th>
                                    <th>At</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $contactsSystemInfo = CommonHelper::contactSystemInfoAppointmentDevelopments($model->development_id);
                                /*echo "<pre>";print_r($contactsSystemInfo);exit();*/
                                if(!empty($contactsSystemInfo)){
                                    $count=count($contactsSystemInfo);
                                    foreach ($contactsSystemInfo as $key=>$systemInfo){
                                        $developmentName = CommonHelper::developmentListById($systemInfo->development_id);
                                        $devId = '';
                                        if(!empty($developmentName->id)){
                                            $devId = $developmentName->id;
                                        }

                                        $devName = '';
                                        if(!empty($developmentName->development_name)){
                                            $devName = $developmentName->development_name;
                                        }
                                            ?>
                                            <tr>
                                                <td><a style="cursor: pointer;" id="appointment_id<?php echo $count;?>"><?php echo $count;?></a><input type="hidden" value="<?php echo $systemInfo->id;?>" id="system_log_id<?php echo $count;?>"></td>
                                                <?php
                                                $name=$phone='';
                                                $media_source=$systemInfo->media_source;
                                                if (!empty($systemInfo->contact_id)) {
                                                    $agent = CommonHelper::agentListByContactsId($systemInfo->contact_id);
                                                    if (!empty($agent)) {
                                                        $name = Html::a($agent->first_name." ".$agent->last_name, ['/contacts/agent/update', 'id' => $agent->id]);
                                                        $phone=$agent->mobile;
                                                    }
                                                    else{
                                                        $contact = CommonHelper::contactIdByContacts($systemInfo->contact_id);
                                                        if (!empty($contact)) {
                                                            if (!empty($systemInfo->contact_id)) {
                                                                $name = Html::a($contact->first_name." ".$contact->last_name, ['/contacts/contacts/view', 'id' => $contact->id]);
                                                                $phone=$contact->phone;
                                                                //$media_source=CommonHelper::leadSourceByBrandId($contact->lead_source);
                                                            }
                                                        }
                                                    }
                                                }

                                                ?>
                                                <td><?php echo !empty($name)?$name:$systemInfo->first_name.' '.$systemInfo->last_name;?></td>
                                                <td><?php echo !empty($phone)?$phone:$systemInfo->phone;?></td>
                                                <td>
                                                    <?php
                                                    if (!empty($systemInfo->contact_type)){
                                                        echo  $systemInfo->contact_type;
                                                    }
                                                    else{
                                                        echo "Prospect";
                                                    }
                                                    ?>
                                                </td>
                                                <td><?php echo $media_source; ?> </td>
                                                <td><?php echo $systemInfo->event_on; ?></td>
                                                <td><?php echo $systemInfo->event_at; ?></td>
                                            </tr>
                                            <?php
                                        $count--;}} ?>
                                </tbody>
                            </table>
                            <div class="modal bd-example-modal-lg" id="appointment-details-modal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="myLargeModalLabel"
                                 aria-hidden="true">
                                <div class="modal-dialog modal-xl modal-dialog-scrollable modal-dialog-centered" style="min-width: 770px;">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4>Appointment Details !</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row ">
                                                <div id="appointment-content-div"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </tab>

                        <tab name="Inventory">
                            <div style="overflow: hidden" class="inventory-wrapper">
                                <?php
                                    $inventoryLists = CommonHelper::developmentIdByInventory($model->development_id);
                                ?>
                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>UNIT</th>
                                        <th>Floor Plan</th>
                                        <th>Size</th>
                                        <th>Price</th>
                                        <th>Allocation</th>
                                        <th>Unit Status</th>
                                        <th>Status Date</th>
                                        <th>REP</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if(!empty($inventoryLists)){foreach ($inventoryLists as $inventoryList){
                                        ?>
                                        <tr>
                                            <td>
                                                <?php
                                                    echo $inventoryList->unit;
                                                ?>
                                            </td>
                                            <td><?php echo $inventoryList->floor_plan; ?></td>
                                            <td><?php echo $inventoryList->size; ?></td>
                                            <td><?php echo '$'.Yii::$app->formatter->format( $inventoryList->price, 'money'); ?></td>
                                            <td>
                                                <?php
                                                    $allocation = CommonHelper::allocationListById($inventoryList->allocation_id);
                                                    if(!empty($allocation)){
                                                        echo Html::a($allocation->allocation, ['/allocation/allocation/update', 'id' => $inventoryList->allocation_id]);
                                                    }
                                                ?>
                                            </td>
                                            <td><?php echo CommonHelper::unitStatusDropDownList($inventoryList->unit_status);?></td>
                                            <td>
                                                <?php
                                                    echo CommonHelper::changeDateFormat($inventoryList->created_at, 'd M Y H g:i a');
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                    $user = CommonHelper::getUserById($inventoryList->created_by);
                                                    echo $user->first_name.' '.$user->last_name;
                                                ?>
                                            </td>
                                        </tr>
                                    <?php }} ?>
                                    </tbody>
                                </table>
                            </div>
                        </tab>
                        <tab name="Allocation">
                            <div style="overflow: hidden" class="allocation-wrapper">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Allocation</th>
                                        <th>UNIT</th>
                                        <th>Price</th>
                                        <th>Agent</th>
                                        <th>Alloc Status</th>
                                        <th>Rep</th>
                                        <th>Status Date</th>
                                        <th>Accepted</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </tab>
                        <tab name="Deals">
                            <div style="overflow: hidden" class="deals-wrapper">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Allocation</th>
                                        <th>UNIT</th>
                                        <th>Price</th>
                                        <th>Agent</th>
                                        <th>Alloc Status</th>
                                        <th>Rep</th>
                                        <th>Status Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Randy McBean</td>
                                            <td>888</td>
                                            <td><?php echo '$'.Yii::$app->formatter->format( 622000, 'money'); ?></td>
                                            <td>No</td>
                                            <td>New</td>
                                            <td>Veronica</td>
                                            <td>10 Oct 2018 11:21am</td>
                                        </tr>
                                        <tr>
                                            <td>Randy McBean</td>
                                            <td>888</td>
                                            <td><?php echo '$'.Yii::$app->formatter->format( 622000, 'money'); ?></td>
                                            <td>No</td>
                                            <td>New</td>
                                            <td>Veronica</td>
                                            <td>10 Oct 2018 11:21am</td>
                                        </tr>
                                        <tr>
                                            <td>Randy McBean</td>
                                            <td>888</td>
                                            <td><?php echo '$'.Yii::$app->formatter->format( 622000, 'money'); ?></td>
                                            <td>No</td>
                                            <td>New</td>
                                            <td>Veronica</td>
                                            <td>10 Oct 2018 11:21am</td>
                                        </tr>
                                        <tr>
                                            <td>Randy McBean</td>
                                            <td>888</td>
                                            <td><?php echo '$'.Yii::$app->formatter->format( 622000, 'money'); ?></td>
                                            <td>No</td>
                                            <td>New</td>
                                            <td>Veronica</td>
                                            <td>10 Oct 2018 11:21am</td>
                                        </tr>
                                        <tr>
                                            <td>Randy McBean</td>
                                            <td>888</td>
                                            <td><?php echo '$'.Yii::$app->formatter->format( 622000, 'money'); ?></td>
                                            <td>No</td>
                                            <td>New</td>
                                            <td>Veronica</td>
                                            <td>10 Oct 2018 11:21am</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </tab>
                    </tabs>
                </div>
                <div class="divider"></div>
                <div class="button_group">
                    <div class="row">
                        <div class="pull-left form_action_btn_2">
                            <?= Html::button('Add Campaigns', ['id' => 'add-campaign-modelButton', 'value' => \yii\helpers\Url::to(['developments-campaigns/quick-create?developmentid='.$model->development_id.'&devid='.$model->id]), 'class' => 'btn btn-success']) ?>
                        </div>
                        <div class="export_lead pull-left form_action_btn_2" style="padding-left: 10px;">
                            <a href="<?php echo \yii\helpers\Url::to(['developments/quick-export-csv?developmentid='.$model->development_id.'&devid='.$model->id]); ?>" class="btn btn-success btn-download-html btn-show-after-save">Export Leads</a>
                        </div>
                        <div class="pull-right form_action_btn_2">
                                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                                    'class' => 'btn btn-danger',
                                    'data' => [
                                        'confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post',
                                    ],
                                ]) ?>
                        </div>
                        <div class="pull-right form_action_btn_2">
                            <?= Html::a('Edit Development', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>
                </div>
                <?php /*
                <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                	'id',
					'development_id',
					'developer_id',
					'development_name',
					'sales_status',
					'grade',
                    'excerpt',
                    'image_url',
					'created_at',
					'created_by',
					'updated_at',
					'updated_by',
                ],
                ]) ?>
                */ ?>
            </div>

        </div>
    </div>
</div>
<?php
$webliveUrl = Yii::getAlias('@webliveUrl');
$baseUrl = Yii::getAlias('@baseUrl');
$development_id=$model->development_id;
$js = <<< JS
    $(function(){
        $(".sale_price").change("input", function(evt) {
            var self = $(this);
            self.val('$'+self.val().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
            change_value();
        });
         $(".allocation_units").change("input", function(evt) {
           change_value();
        });
         change_value();
    });

    function change_value(){
            var allocation_units=$('.allocation_units').val();
            var price=$('.sale_price').val().split("$");
            var sale_price=price[price.length-1];
            var final_sale_price = sale_price.replace(/,/g,"");
            var toal=allocation_units*final_sale_price;
            var value = toal.toLocaleString(
            { minimumFractionDigits: 2 });
           $('.total_reciepts').val('$'+value);
    }
    $(function(){
        $('#add-campaign-modelButton').click(function(){
            $('#model-campaign').modal('show')
                .find('#add-campaign-modelContent')
                .load($(this).attr('value'));
        });
   });
    $(document).on("beforeSubmit", "#add-campaigns-form-1", function () {
        var form = $(this);
        var formData = form.serialize();
        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: formData,
            success: function (data) {
                var obj = jQuery.parseJSON( JSON.stringify(data) );
                console.log(obj);
                $('button.btn.btn-primary').text("Save");
            },
            error: function () {
                //alert("Something went wrong");
            },
            headers: {'Authorization': 'Basic bWFkaHNvbWUxMjM='},
            beforeSend: function (xhr) {
                $('button.btn.btn-primary').text("Sending....");
            },
        });
     });
      var data='';
    $('#developments-contact_type').on('change', function() {
         data +='<div class="form-group field-developments-appointment_content"><label for="developments-appointment_content" class="control-label col-sm-3">Appointment Content</label> <div class="col-sm-8"><div class="redactor-box"><ul class="redactor-toolbar" id="redactor-toolbar-0" style="position: relative; width: auto; top: 0px; left: 0px; visibility: visible;"><li><a href="#" class="re-icon re-html" rel="html" tabindex="-1"></a></li><li><a href="#" class="re-icon re-formatting" rel="formatting" tabindex="-1"></a></li><li><a href="#" class="re-icon re-bold" rel="bold" tabindex="-1"></a></li><li><a href="#" class="re-icon re-italic" rel="italic" tabindex="-1"></a></li><li><a href="#" class="re-icon re-deleted" rel="deleted" tabindex="-1"></a></li><li><a href="#" class="re-icon re-unorderedlist" rel="unorderedlist" tabindex="-1"></a></li><li><a href="#" class="re-icon re-orderedlist" rel="orderedlist" tabindex="-1"></a></li><li><a href="#" class="re-icon re-outdent" rel="outdent" tabindex="-1"></a></li><li><a href="#" class="re-icon re-indent" rel="indent" tabindex="-1"></a></li><li><a href="#" class="re-icon re-image" rel="image" tabindex="-1"></a></li><li><a href="#" class="re-icon re-file" rel="file" tabindex="-1"></a></li><li><a href="#" class="re-icon re-link" rel="link" tabindex="-1"></a></li><li><a href="#" class="re-icon re-alignment" rel="alignment" tabindex="-1"></a></li><li><a href="#" class="re-icon re-horizontalrule" rel="horizontalrule" tabindex="-1"></a></li></ul><div class="redactor-editor" contenteditable="true" dir="ltr"><p>&#8203;</p></div><textarea id="developments-appointment_content" name="Developments[appointment_content]" class="form-control" dir="ltr" style="display: none;"></textarea></div> <p class="help-block help-block-error "></p></div></div>';
         $('#appointment_content').html();
    });
    
    //Appointment popup Show Each Record
    var appointmentCount = $('table#match-table-appointment tr:last').index() + 1;
    for (var i=appointmentCount;i>=0;i--){
        $('#appointment_id'+i+'').click(function() {
        var txt = $(this).text();
        
         $('#appointment-details-modal').modal('show');
         $('#email-close-button').click(function () {
                $('#appointment-details-modal').modal('hide');
                $("#nav li a").removeClass('current');
         });
          var system_log_id=$('#system_log_id'+txt+'').val();
          
           var Url='$baseUrl/backend/web/developments/developments/appointment-details?appointment_id='+system_log_id+'';
             $.ajax({
                    type: 'post',
                    url:Url,
                      complete: function(data) {
                       $('#appointment-content-div').html(data.responseText);
                     }               
             });
        });
    }
    
    //Registration Datatable Show
    var url_registration_load='$baseUrl/backend/web/developments/developments/developments-registration?development_id='+$development_id+'';
    $(document).ready(function(){
            $('#match-table').DataTable({
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                'ajax': {
                    'url':url_registration_load,
                    
                },
                'columns': [
                    { data: 'id' },
                    { data: 'contact_id' },
                    { data: 'registration_name' }, 
                    { data: 'phone' }, 
                    { data: 'contact_type' }, 
                    { data: 'media' }, 
                    { data: 'registration_date' }, 
                    { data: 'registration_status' }, 
                ],
            });
    });
    
    //Rsvp Datatable Load
     var url_rsvp_load='$baseUrl/backend/web/developments/developments/developments-rsvp?development_id='+$development_id+'';
    $(document).ready(function(){
            $('#match-table-rsvp').DataTable({
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                'ajax': {
                    'url':url_rsvp_load,
                    
                },
                 'columns': [
                    { data: 'id' },
                    { data: 'contact_id' },
                    { data: 'contact_name' }, 
                    { data: 'phone' }, 
                    { data: 'contact_type' }, 
                    { data: 'media' }, 
                    { data: 'submission_date' }, 
                    { data: 'rsvp_status' }, 
                ],
            });
    });
    
    //Count Section
   $('.tab_section .tabs ul li').click(function() {
       var tab_section_data=$(this).text();
        var count_url='';
       if (tab_section_data=='Registrations'){
           count_url='$baseUrl/backend/web/developments/developments/developments-registration-count?tab_section='+tab_section_data+'&development_id=$development_id';
       }else if (tab_section_data=='RSVP'){
            count_url='$baseUrl/backend/web/developments/developments/developments-registration-count?tab_section='+tab_section_data+'&development_id=$development_id';
       }
       else if (tab_section_data=='Appointments'){
            count_url='$baseUrl/backend/web/developments/developments/developments-registration-count?tab_section='+tab_section_data+'&development_id=$development_id';
       }
       $.ajax({
            type: 'post',
            url:count_url,
              complete: function(data) {
                 var parsed_result = JSON.parse(data.responseText);
                 var total=0;
                 var prospect=0;
                 var agent=0;
                 if (parsed_result.tab_section_result=='Registrations'){
                     total=parsed_result.total_reg;
                     prospect=parsed_result.prospect;
                     agent=parsed_result.agent;
                     $('#total_registration').html(total);
                     $('#total_prospect').html(prospect);
                     $('#total_agent').html(agent);
                 }
                 else if (parsed_result.tab_section_result=='RSVP') {
                     total=parsed_result.total_reg;
                     prospect=parsed_result.prospect;
                     agent=parsed_result.agent;
                     $('#total_rsvp').html(total);
                     $('#total_rsvp_prospect').html(prospect);
                     $('#total_rsvp_agent').html(agent);
                 }
                 else if (parsed_result.tab_section_result=='Appointments') {
                     total=parsed_result.total_reg;
                     prospect=parsed_result.prospect;
                     agent=parsed_result.agent;
                     $('#total_appointment').html(total);
                     $('#total_appointment_prospect').html(prospect);
                     $('#total_appointment_agent').html(agent);
                 }
            }
       });
   })
    
JS;
$this->registerJs($js);
?>


<?php
$baseUrl = Yii::getAlias('@baseUrl');
$modelId=$model->id;
$js = <<< JS
  $(function(){
        
        //load edit-assignmentsAssets-modelButton modal
        $('#edit-assignmentsAssets-modelButton').click(function(){
            $('#model-interaction').modal('show')
                .find('#edit-assignmentsAssets-modelContent')
                .load($(this).attr('value'));
        });
        
  });


JS;
$this->registerJs($js);
?>

<?php

use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\grid\GridView;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\buttons\NewButton;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\usermanager\user\models\User;

/* @var $this yii\web\View */
/* @var $searchModel unlock\modules\developments\models\DevelopmentsSearch */
/* @var $dataProvider unlock\modules\core\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Developments');
?>
<div class="main-container" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-grid-filter">
        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <div class="admin-grid-content">
        <div class="admin-grid-toolbar">
            <div class="admin-grid-toolbar-left">
                <?= NewButton::widget(['url' => Url::toRoute(['create'])]) ?>
            </div>
            <div class="admin-grid-toolbar-right">
                <div class="col-md-8">
                    <?= Summary::widget(['dataProvider' => $dataProvider]) ?>
                </div>
                <div class="col-md-4">
                    <?= Html::a('Load', ['/developments/developments/load-development'], ['class'=>'btn btn-primary pull-right']) ?>
                </div>
            </div>
        </div>


        <div class="admin-grid-view">
                            <?php

                echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
                'pager' => ['class' => LinkPager::className()],
                'columns' => [
                   //['class' => 'unlock\modules\core\grid\SerialColumn'],
                    [
                        'attribute' => 'development_name',
                        'format'    => 'raw',
                        'headerOptions' => ['style' => 'width:250px;'],
                        'value'    => function($model){
                            $name = Html::a($model->development_name, ['view', 'id' => $model->id]).'<input type="hidden" id="dev_id'.$model->id.'" value="'.$model->id.'">';
                            return $name;
                        }
                    ],
                    [
                        'attribute' => 'assignment_id',
                        'format'    => 'raw',
                        'headerOptions' => ['style' => 'width:90px;'],
                        'value'=> function($model){
                            return \kartik\select2\Select2::widget([
                                //'model' => $model,
                                'name'=>'assignment_id'.'('.$model->id,
                                'attribute' => 'state_2',
                                'value' => [$model->assignment_id],
                                'data' => User::getActiveSalesUser(),
                                'options' => ['placeholder' => 'Assigned To'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);
                        },
                    ],
                    [
                        'attribute'  => 'grade',
                        'format'    => 'raw',
                        'headerOptions' => ['style' => 'width:70px;'],
                        'value' => function($model){
                            $urlInterac = yii\helpers\Url::to(['developments/change-grade?id='.$model->id]);
                            return Html::dropDownList('status-dropdown', $model->grade, CommonHelper::developerGradeDropDownList(), [
                                'class' => 'status-dropdown',
                                'onchange' => "
                                            var statusid = $(this).val();
                                            console.log(statusid);
                                            $.ajax('$urlInterac&status='+statusid+'', {
                                                type: 'POST'
                                            }).done(function(data) {
                                                //$.pjax.reload({container: '#pjax-container'});
                                            });                               
                                        ",
                            ]);
                            ?>
                            <?php
                        }
                    ],
                    [
                        'attribute'  => 'sales_status',
                        'format'    => 'raw',
                        'headerOptions' => ['style' => 'width:90px;'],
                        'value' => function($model){
                            $urlInterac = yii\helpers\Url::to(['developments/change-status?id='.$model->id]);
                            return Html::dropDownList('status-dropdown', $model->sales_status, CommonHelper::developmentSalesStatusDropDownList(), [
                                'class' => 'status-dropdown',
                                'onchange' => "
                                            var statusid = $(this).val();
                                            console.log(statusid);
                                            $.ajax('$urlInterac&status='+statusid+'', {
                                                type: 'POST'
                                            }).done(function(data) {
                                                //$.pjax.reload({container: '#pjax-container'});
                                            });
                                          
                                        ",
                            ]);

                            ?>
                            <?php
                        }
                    ],
                    [
                        'attribute' => 'developer_id',
                        'format'    => 'raw',
                        'headerOptions' => ['style' => 'width:180px;'],
                        'value' => function($model){                        
                            $developersList = CommonHelper::developmentIdByDevelopersList($model->development_id);
                            //return $model->development_id;
                            if(!empty($developersList)){
                                return implode('</br>', $developersList);
                            }
                        }
                    ],
                    [
                            'attribute' => 'launch_date',
                            'format'    => 'raw',
                            'headerOptions' => ['style' => 'width:166px;'],
                            'value' => function($model){
                                return \kartik\date\DatePicker::widget([
                                    'name' => 'launch_date'.'('.$model->id,
                                    'value' =>CommonHelper::changeDateFormat($model->launch_date, 'd M Y'),
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'format' => 'd M yyyy'
                                    ]
                                ]);
                            }
                    ],

			        //'grade',
			// 'created_at',
			// 'created_by',
			// 'updated_at',
			// 'updated_by',
                /*[
                'attribute' => 'created_at',
                'format' => 'datetime',
                'filter' => DatePicker::widget([
                'name'  => 'SampleSearch[created_at]',
                'dateFormat' => 'yyyy-MM-dd'
                ]),
                ],*/
             /*   [
                'attribute' => 'status',
                'format' => 'status',
                'filter' => Html::activeDropDownList($searchModel, 'status', CommonHelper::statusDropDownList(), ['prompt' => CommonHelper::GRID_FILTER_PROMPT]),
                ],*/
                //['class' => 'unlock\modules\core\grid\ActionColumn'],
                ],
                ]);   ?>
                    </div>
    </div>

</div>

<?php
$baseUrl = Yii::getAlias('@baseUrl');
$js = <<< JS
                $('#w1 table tbody tr td select[id').each( function () {
                $(this).val( $(this).attr("id")).on('change', function() {
                    var user_id=$(this).val();
                    var name = $(this).attr("name");
                    var slug = name.split('(').pop();
                    var dev_id=$('#dev_id'+slug+'').val();
                     var url='$baseUrl/backend/web/developments/developments/change-assignment?id='+dev_id+'&assigned_to='+user_id+'';
                    $.post(url, function(data){
                        //alert("sad");
                    });
                     });
                });
                $('#w1 table tbody tr td div[id').each( function () {
                  $(this).val( $(this).attr("id")).on('change', function() {
                   event.preventDefault();
                   var div_id=this.value;
                   var launch_date=$('#'+div_id+' input').val();
                   var name = $('#'+div_id+' input').attr("name");
                   var slug = name.split('(').pop();
                   var dev_id=$('#dev_id'+slug+'').val();
                   var url='$baseUrl/backend/web/developments/developments/change-launch-date?id='+dev_id+'&launch_date='+launch_date+'';
                   $.post(url, function(data){
                   });
                   });
                });
JS;
$this->registerJs($js);
?>

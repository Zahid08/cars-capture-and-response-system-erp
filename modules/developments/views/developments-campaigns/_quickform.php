<?php

use yii\helpers\Url;
use yii\helpers\Html;

use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtons;
use unlock\modules\core\buttons\BackButton;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;


/* @var $this yii\web\View */
/* @var $model unlock\modules\developers\models\DevelopersContacts */
/* @var $form yii\bootstrap\ActiveForm */

$this->title= ($model->isNewRecord) ? Yii::t('app', 'Create Development Campaigns') : Yii::t('app', 'Update Development Campaigns');
?>

<div class="main-container main-container-form" role="main">
<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
    /*'options' => ['enctype' => 'multipart/form-data'],*/
    'id' => 'add-campaigns-form-1',
    'validateOnBlur' =>true,
    'enableAjaxValidation'=>false,
    'errorCssClass'=>'has-error',
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
        'options' => ['class' => 'form-group'],
        'horizontalCssClasses' => [
            'label' => 'col-sm-2',
            'offset' => '',
            'wrapper' => '',
            'hint' => '',
        ],
    ],
]); ?>

<?= $form->field($model, 'media_type')->dropDownList(CommonHelper::leadSourceDropDownList(), ['prompt'=> 'Select a Media Type..',]) ?>

<?= $form->field($model, 'type')->dropDownList(CommonHelper::campaignsTypeDropDownList(), ['prompt'=> 'Select a Type..',]) ?>

<?= $form->field($model, 'state')->dropDownList(CommonHelper::campaignsStateDropDownList(), ['prompt'=> 'Select a State..',]) ?>

<?= $form->field($model, 'go_date')->widget(
    DatePicker::className(), [
    'name' => 'go_date',
    'value' => '12-31-2010',
    'pluginOptions' => [
        'autoclose' => true,
        'format' => 'yyyy-mm-dd'
    ]
]);?>

<?= $form->field($model, 'budget')->textInput() ?>

<?= $form->field($model, 'est_deals')->textInput() ?>

<?= $form->field($model, 'est_return')->textInput() ?>

<?= $form->field($model, 'status')->dropDownList(CommonHelper::statusDropDownList()) ?>

<?= $form->field($model, 'development_id')->hiddenInput(['value' => $_REQUEST['developmentid']])->label('') ?>


<div class="admin-form-button">
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <?= FormButtons::widget() ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
<?php

namespace unlock\modules\developments\controllers;

use http\Exception\BadHeaderException;
use unlock\modules\contacts\models\Contacts;
use unlock\modules\contacts\models\ContactsRegistration;
use unlock\modules\contacts\models\ContactsReservations;
use unlock\modules\contacts\models\ContactsRsvp;
use unlock\modules\contacts\models\ContactsSysteminfo;
use unlock\modules\contacts\models\ContactsSysteminfoSearch;
use unlock\modules\core\helpers\ExportCsv;
use unlock\modules\developments\models\DevelopersDevelopmentsRelation;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\base\Exception;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use unlock\modules\developments\models\Developments;
use unlock\modules\developments\models\DevelopmentsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use unlock\modules\core\helpers\Collection;
use unlock\modules\core\helpers\FileHelper;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\actions\ExportCsvAction;
use unlock\modules\core\actions\ExportPdfAction;
use yii\data\ActiveDataProvider;

/**
 * DevelopmentsController implements the CRUD actions for Developments model.
 */
class DevelopmentsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => Yii::$app->DynamicAccessRules->customAccess('developments/developments'),
        ];
    }

    public function actionLoadDevelopment(){


        $webliveUrl = Yii::getAlias('@webliveUrl');
        $developmenturl = $webliveUrl.'/wp-json/wp-connect/api?access_token=Y29uZG8tZGV2ZWxvcG1lbnRzVW5sb&post_type=project&time='.time();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $developmenturl);
        $result = curl_exec($ch);
        curl_close($ch);

        $developmentData = json_decode($result);


        if(!empty($developmentData)){
            foreach ($developmentData as $value){
                $devRelation = new DevelopersDevelopmentsRelation();

                if(is_array($value->developer_id)){
                    foreach ($value->developer_id as $devId){
                        /*Check if exist */
                        $is_exist = DevelopersDevelopmentsRelation::find()
                            ->where('development_id=:development_id',[':development_id'=>$value->projectId])
                            ->andWhere('developer_id=:developer_id',[':developer_id'=>$devId])
                            ->one();
                        if(empty($is_exist)) {
                            $devRelation = new DevelopersDevelopmentsRelation();
                            $devRelation->development_id = $value->projectId;
                            $devRelation->developer_id = $devId;
                            if (!$devRelation->save()) {
                                throw new Exception(Yii::t('app', Html::errorSummary($devRelation)));
                            }
                        }
                    }
                }
                $checkId = CommonHelper::developmentListById($value->projectId);

                if(!empty($checkId)){
                    $model = Developments::findOne($checkId->id);

                    $model->development_name = str_replace("&amp;","&",$value->title);
                    $model->development_address =$value->projectAddress;
                    $model->development_url = $value->pageLink;
                    $model->sales_status = $value->projectSalesStatus;
                    $model->grade = $value->grade;
                    $model->excerpt = str_replace("&amp;","&",$value->excerpt);
                    $model->image_url = $value->image;
                    if($model->save(false)){
                        //echo '<h2 style="color: #1c7430">Successfully Update Development Data. '.$value->projectId.'</h2>';
                        Yii::$app->session->setFlash('success', Yii::t('app', 'Successfully Update Development Data.'));

                    }else{
                        //echo '<h2 style="color: red">Development Data Import Error!!!!.</h2>';
                        Yii::$app->session->setFlash('error', Yii::t('app', 'Development Data Import Error!!!!.'));
                    }
                }else{
                    $model = new Developments();
                    $model->development_id = $value->projectId;

                    $model->development_name = $value->title;
                    $model->development_address =$value->projectAddress;
                    $model->development_url = $value->pageLink;
                    $model->sales_status = $value->projectSalesStatus;
                    $model->grade = $value->grade;
                    $model->excerpt = $value->excerpt;
                    $model->image_url = $value->image;
                    if($model->save(false)){
                        //echo '<h2 style="color: #1c7430">Successfully Import All Development Data. '.$value->projectId.'</h2>';
                        Yii::$app->session->setFlash('success', Yii::t('app', 'Successfully Import All Development Data.'));
                    }else{
                        //echo '<h2 style="color: red">Development Data Import Error!!!!.</h2>';
                        Yii::$app->session->setFlash('error', Yii::t('app', 'Development Data Import Error!!!!.'));
                    }
                }

            }
        }else{
            echo '<h2 style="color: red">Please check development JSON URL.</h2>';
        }
        return $this->redirect(['index']);
        exit();
    }

    /**
     * Lists all Developments models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DevelopmentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        Collection::setData($dataProvider);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Developments model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Developments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Developments();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $this->saveData($model);
        }

        return $this->render('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Developments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $this->saveData($model);
        }

        return $this->render('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Developments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        try {
            if (!$model->delete()) {
                throw new Exception(Yii::t('app', 'The record cannot be deleted.'));
            }

            // Delete Image:
            // FileHelper::removeFile($model->image, 'sanction');
            Yii::$app->session->setFlash('success', Yii::t('app', 'The record have been successfully deleted.'));
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            if($e->getName() == CommonHelper::INTEGRITY_CONSTRAINT_VIOLATION){
                $message = "This record has relational data. You must delete the following data first";
                $message .= "<ul>";
                $message .= "<li>Example => Example List</li>";
                $message .= "</ul>";
            }
            Yii::$app->session->setFlash('error', $message);
        }

        return $this->redirect(['index']);
    }

    //Export Csv Functionlaity Created
    public function actionQuickExportCsv($developmentid,$devid){
            $basAuth=Yii::getAlias('@baseAuthhor');
            $development=$this->findModel($devid);
            $development_name=!empty($development->development_name)?$development->development_name:'';
            $current_date=CommonHelper::changeDateFormat(date('Y-m-d H:i:s'), 'd M Y');
            if(!empty($development->assignment_id)){
                $username=CommonHelper::getLoggedInUserNameByUserId($development->assignment_id);
                $assigned_username =$username;
            }else{
                $username=CommonHelper::getLoggedInUserNameByUserId($basAuth);
                $assigned_username =$username;
            }
            $title='Lead Export Report For "'.$development_name.'" by '.$assigned_username.' on '.$current_date.'';

            //Registration Details
            $registration=[];
            $registration_title='Registration Leads';
            $contactRegDetails = CommonHelper::developmentIdByRegistrationList($developmentid);
            foreach ($contactRegDetails as $contactRegDetail){
                $regName = CommonHelper::contactIdByContacts($contactRegDetail->contact_id);
                if(!empty($regName)){
                    $first_name=!empty($regName->first_name)?$regName->first_name:'';
                    $last_name=!empty($regName->last_name)?$regName->last_name:'';
                    $phone=!empty($regName->phone)?$regName->phone:'';
                    $email=!empty($regName->email)?$regName->email:'';
                    $full_name=$first_name.' '.$last_name;
                }
                $systemInfo=CommonHelper::contactIdBySystemInfoContactType($contactRegDetail->system_log_id);
                if (!empty($systemInfo->contact_type)){
                    $type=$systemInfo->contact_type;
                }
                else{
                    $type="Prospect";
                }
                $media_source=CommonHelper::leadSourceByBrandId($contactRegDetail->reg_source);;
                $registration_on= CommonHelper::changeDateFormat($contactRegDetail->reg_datetime, 'd M Y');
                $registration_status=CommonHelper::getRegistrationStatusById($contactRegDetail->status);
                $registration[]=[
                    'Reg ID'=>$contactRegDetail->id,
                    'Full Name'=>$full_name,
                    'Phone'=>$phone,
                    'Type'=>$type,
                    'Media Source'=>$media_source,
                    'Registered On'=>$registration_on,
                    'Email'=>$email,
                    'Status'=>$registration_status,
                ];
            }

            //Contact Rsvp
            $contactRsvp = CommonHelper::developentIdByRSVP($developmentid);
            $rsvp_array=[];
            $rsvp_title='RSVP Leads';
            foreach ($contactRsvp as $rsvp){
                $rsvp_contact = CommonHelper::contactIdByContacts($rsvp->contact_id);
                if(!empty($rsvp_contact)){
                    $first_name=!empty($rsvp_contact->first_name)?$rsvp_contact->first_name:'';
                    $last_name=!empty($rsvp_contact->last_name)?$rsvp_contact->last_name:'';
                    $phone=!empty($rsvp_contact->phone)?$rsvp_contact->phone:'';
                    $email=!empty($rsvp_contact->email)?$rsvp_contact->email:'';
                    $full_name=$first_name.' '.$last_name;
                    if (!empty($rsvp_contact->contact_type)&& $rsvp_contact->contact_type=='1'){
                        $type="Prospect";
                    }elseif (!empty($rsvp_contact->contact_type)&& $rsvp_contact->contact_type=='2'){
                        $type="Agent";
                    }else{
                        $type='Purchaser';
                    }
                }
                $media_source=CommonHelper::leadSourceByBrandId($rsvp->rsvp_source);;
                $rsvp_date= CommonHelper::changeDateFormat($rsvp->rsvp_date, 'd M Y');
                $rsvp_status = CommonHelper::getRsvpStatus($rsvp->rsvp_status);
                $rsvp_status_name=!empty($rsvp_status->rsvp_status_name)?$rsvp_status->rsvp_status_name:'Declined';
                $rsvp_array[]=[
                    'RSVP ID'=>$rsvp->id,
                    'Full Name'=>$full_name,
                    'Phone'=>$phone,
                    'Type'=>$type,
                    'Media Source'=>$media_source,
                    'RSVP On'=>$rsvp_date,
                    'Email'=>$email,
                    'Status'=>$rsvp_status_name,
                ];
            }

            //Appointments
            $contactsSystemInfo = CommonHelper::contactSystemInfoAppointmentDevelopments($developmentid);
            $appointments=[];
            $appointment_title='Appointment Leads';
            if(!empty($contactsSystemInfo)){
                $count=count($contactsSystemInfo);
                foreach ($contactsSystemInfo as $key=>$systemInfo){
                    //Name Configure
                    if (!empty($systemInfo->contact_id)) {
                        $agent = CommonHelper::agentListByContactsId($systemInfo->contact_id);
                        if (!empty($agent)) {
                            $first_name=!empty($agent->first_name)?$agent->first_name:'';
                            $last_name=!empty($regName->last_name)?$agent->last_name:'';
                            $phone=!empty($agent->mobile)?$agent->mobile:'';
                            $email=!empty($agent->email)?$agent->email:'';
                            $agent_name=$first_name.' '.$last_name;
                            $full_name=!empty($agent_name)?$agent_name:$systemInfo->first_name.' '.$systemInfo->last_name;
                        }
                        else{
                            $contact = CommonHelper::contactIdByContacts($systemInfo->contact_id);
                            if (!empty($contact)) {
                                if (!empty($systemInfo->contact_id)) {
                                    $first_name=!empty($contact->first_name)?$contact->first_name:'';
                                    $last_name=!empty($contact->last_name)?$contact->last_name:'';
                                    $phone=!empty($contact->phone)?$contact->phone:'';
                                    $email=!empty($contact->email)?$contact->email:'';
                                    $prospect_contact=$first_name.' '.$last_name;
                                    $full_name=!empty($prospect_contact)?$prospect_contact:$systemInfo->first_name.' '.$systemInfo->last_name;
                                }
                            }
                        }
                    }
                    //Extra
                    if (!empty($systemInfo->contact_type)){
                        $type=$systemInfo->contact_type;
                    }
                    else{
                        $type="Prospect";
                    }

                    $media_source=$systemInfo->media_source;
                    $appointment_on=$systemInfo->event_on;
                    $appointment_at=$systemInfo->event_at;

                    $appointments[]=[
                        'Appointment ID'=>$count,
                        'Full Name'=>$full_name,
                        'Phone'=>$phone,
                        'Type'=>$type,
                        'Media Source'=>$media_source,
                        'Appointment On'=>$appointment_on,
                        'At this time'=>$appointment_at,
                        'Email'=>$email,
                        'Status'=>'New',
                    ];
                    $count--;
                }
            }

            $footer_content='CONNECT asset management Confidential- Obtain Release Permission';
            $filename = $development_name.' - Lead Export -'.$current_date.'' . ".csv";

            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename=\"$filename\"");
            $this->ExportCSVFile($registration,$title,$registration_title);
            $this->ExportCSVFile($rsvp_array,'',$rsvp_title,'');
            $this->ExportCSVFile($appointments,'',$appointment_title,$footer_content);
            exit();
    }

    function ExportCSVFile($records,$title='',$subtile,$footer_content='') {
            $fh = fopen('php://output', 'w');
            $heading = false;
            if (!empty($records))
                if (!empty($title)) {
                    echo "\n$title\n\n";
                }
            if (!empty($subtile)) {
                echo "$subtile\n";
            }
            foreach ($records as $row) {
                if (!$heading) {
                    // output the column headings
                    fputcsv($fh, array_keys($row));
                    $heading = true;
                }
                // loop over the rows, outputting them
                fputcsv($fh, array_values($row));

            }
            echo "\n\n";
            if (!empty($footer_content)) {
                echo $footer_content;
            }
            fclose($fh);
    }

    /**
    * Save Data.
    * @param object $model
    * @return mixed
    */
    private function saveData($model){
        if (isset($model)) {
            //$transaction = Yii::$app->db->beginTransaction();
            try {
                $post = Yii::$app->request->post();
                
                if (!$model->save()) {
                    throw new Exception(Yii::t('app', Html::errorSummary($model)));
                }

                //$transaction->commit();
                Yii::$app->session->setFlash('success', Yii::t('app', 'Record successfully saved.'));
                $this->_redirect($model);
            }
            catch (Exception $e) {
                //$transaction->rollBack();
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
    }
    
    private function _redirect($model){
        $action = Yii::$app->request->post('action', 'save');
        switch ($action) {
            case 'save':
                return $this->redirect(['index']);
                break;
            case 'apply':
                return $this->redirect(['update', 'id' => $model->id]);
                break;
            case 'save2new':
                return $this->redirect(['create']);
                break;
            default:
                return $this->redirect(['index']);
        }
    }

    public function actionAppointmentDetails($appointment_id){
        try {
            $model=ContactsSysteminfo::findOne($appointment_id);
            if (!$model){
                throw new Exception('Not Found');
            }
            $appointment_body = $this->renderAjax('_appointmentDetails', [
                'model' => $model,
            ]);
            return $appointment_body;
        }
        catch (Exception $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
    }
    public function actionChangeGrade($id, $status){
        $model = $this->findModel($id);
        $model->grade = $status;
        $model->updated_at = date("Y-m-d H:i:s");

        if (!$model->save()) {
            throw new Exception(Yii::t('app', Html::errorSummary($model)));
        }

        //Developers Grade  (1 = A+, 2 = A, 3 = B+,4 = B,5 = C, 6 = D)
        $newstatus = '';
        if($status == 1){
            $newstatus = 'A+';
        }elseif ($status == 2){
            $newstatus = 'A';
        }elseif ($status == 3){
            $newstatus = 'B+';
        }elseif ($status == 4){
            $newstatus = 'B';
        }elseif ($status == 5){
            $newstatus = 'C';
        }elseif ($status == 6){
            $newstatus = 'D';
        }
        $development_grade = [
            'ID' =>$model->development_id,
            'development_grade' => $newstatus
        ];

        $this->developmentToConnect($development_grade);
        //return $this->redirect(['index']);
    }

    //change registration status
    public function actionChangeRegistrationStatus($id,$status){
        $model=ContactsRegistration::findOne($id);
        $model->status=$status;
        if (!$model->save()){
            throw new Exception(Yii::t('app', Html::errorSummary($model)));
        }
    }

    public function actionChangeAssignment($id, $assigned_to){
        $model = $this->findModel($id);
        $model->assignment_id = $assigned_to;
        if (!$model->save()) {
            throw new Exception(Yii::t('app', Html::errorSummary($model)));
        }
       // return $this->redirect(['index']);
    }

    public function actionChangeLaunchDate($id, $launch_date){
        $model = $this->findModel($id);
        $model->launch_date = $launch_date;
        if (!$model->save()) {
            throw new Exception(Yii::t('app', Html::errorSummary($model)));
        }
        //return $this->redirect(['index']);
    }

    public function actionChangeStatus($id, $status){

        $model = $this->findModel($id);
        $model->sales_status = $status;
        $model->updated_at = date("Y-m-d H:i:s");

        if (!$model->save()) {
            throw new Exception(Yii::t('app', Html::errorSummary($model)));
        }
        $development_grade = [
            'ID' =>$model->development_id,
            'development_status' => $status
        ];

        $this->developmentToConnect($development_grade);
       // return $this->redirect(['index']);
    }

    public function actionChangeRsvpStatus($id,$status){
        if (($model = ContactsRsvp::findOne($id)) !== null) {
            $model->rsvp_status = $status;
            if (!$model->save()) {
                throw new Exception(Yii::t('app', Html::errorSummary($model)));
            }
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * Finds the Developments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Developments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Developments::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    public function developmentToConnect($developemntList){
        $webliveUrl = Yii::getAlias('@webliveUrl');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$webliveUrl."/wp-json/wp-connect/development-edit?access_token=Y29uZG8tZGV2ZWxvcG1lbnRzVW5sb");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($developemntList));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);
        curl_close ($ch);
    }
    
    public function actionUpdateAssignment($id){
        $model = $this->findModel($id);
        $model->scenario ='assignment_required';
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }

         if ($model->load(Yii::$app->request->post())) {
             if (!$model->save()) {
                throw new Exception(Yii::t('app', Html::errorSummary($model)));
            }                
        }
        
        return $this->redirect(['/developments/developments/view','id'=>$id]);
        
    }
    public function actionUpdateHeroImage($id){
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            if (!$model->save()) {
                throw new Exception(Yii::t('app', Html::errorSummary($model)));
            }
        }
        return $this->redirect(['/developments/developments/view','id'=>$id]);
    }

    public function  actionDevelopmentsRegistration($development_id){
        $baseurl=Yii::getAlias('@baseUrl');
        $draw = $_POST['draw'];
        $row = $_POST['start'];
        $rowperpage = $_POST['length']; // Rows display per page
        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc*/
        if ($columnSortOrder=='asc'){
            $short_order='desc';
        }else{
            $short_order='asc';
        }
        if ($columnName=='contact_id'){
            $order='reg.id';
        }
        else if ($columnName=='registration_name'){
            $order='cont.first_name';
        }
        else if ($columnName=='phone'){
            $order='cont.phone';
        }
        else if ($columnName=='contact_type'){
            $order='sys.contact_type';
        }
        else if ($columnName=='media'){
            $order='leads.lead_source_name';
        }
        else{
            $order='reg.id';
        }

        $searchValue = $_POST['search']['value']; // Search value
        $connection = Yii::$app->getDb();

        $searchQuery = " ";
        $searchQuery = " ";
        if($searchValue != ''){
            $searchQuery = " and (reg.id like '%".$searchValue."%' or 
                    cont.first_name  like '%".$searchValue."%' or 
                    cont.phone  like '%".$searchValue."%' or 
                    leads.lead_source_name  like '%".$searchValue."%' or 
                    sys.contact_type  like '%".$searchValue."%' or 
                    cont.last_name like'%".$searchValue."%' ) ";
        }

        $totalRecords =ContactsRegistration::find()->where(['development_id'=>$development_id])->count();
        //Count and Search
        $registratation_query_count="select reg.id,reg.reg_datetime,reg.status,cont.first_name,cont.last_name,cont.email,sys.contact_type,leads.lead_source_name
                                                from `conn_contacts_registration` reg 
                                                JOIN `conn_contacts` cont ON cont.id=reg.contact_id 
                                                JOIN `conn_contacts_systeminfo` sys ON sys.contact_id=reg.contact_id 
                                                JOIN `conn_setting_lead_source` leads ON leads.id=reg.reg_source
                                                WHERE reg.development_id=".$development_id."
                                                 ".$searchQuery."
                                                GROUP by reg.id
                                                ORDER by ".$order." ".$short_order."";
        $command = $connection->createCommand($registratation_query_count);
        $records =$command->queryAll();
        $totalRecordwithFilter =count($records);

        $registratation_query   ="select reg.id,reg.contact_id,reg.reg_datetime,reg.status,cont.first_name,cont.last_name,cont.email,cont.phone,sys.contact_type,leads.lead_source_name
                                from `conn_contacts_registration` reg 
                                JOIN `conn_contacts` cont ON cont.id=reg.contact_id 
                                JOIN `conn_contacts_systeminfo` sys ON sys.contact_id=reg.contact_id 
                                JOIN `conn_setting_lead_source` leads ON leads.id=reg.reg_source
                                WHERE reg.development_id=".$development_id." 
                                ".$searchQuery."
                                GROUP by reg.id
                                ORDER by ".$order." ".$short_order."
                                LIMIT ".$row.",".$rowperpage."";

        $command_records =$connection->createCommand($registratation_query);
        $registration_record =$command_records->queryAll();


        $data=[];
        foreach ($registration_record as $regItem){
            $reg_id=$regItem['id'];
            $contact_id=$regItem['contact_id'];
            $first_name=!empty($regItem['first_name'])?$regItem['first_name']:'';
            $last_name=!empty($regItem['last_name'])?$regItem['last_name']:'';
            $name=$first_name.' '.$last_name;
            $phone=!empty($regItem['phone'])?$regItem['phone']:'';
            $contact_type=!empty($regItem['contact_type'])?$regItem['contact_type']:'Prospect';
            $media=!empty($regItem['lead_source_name'])?$regItem['lead_source_name']:'';
            $registration_date=CommonHelper::changeDateFormat($regItem['reg_datetime'], 'd M Y H g:i a');
            $id='<a class="" href="'.$baseurl.'/backend/web/contacts/contacts-registration/update?id='.$reg_id.'">'.$reg_id.'</a>';
            $full_name='<a class="" href="'.$baseurl.'/backend/web/contacts/contacts/view?id='.$contact_id.'">'.$name.'</a>';
            $urlInterac = $baseurl.'/backend/web/developments/developments/change-registration-status?id='.$regItem['id'];

            $reg_status=Html::dropDownList('contact_type-dropdown',$regItem['status'], CommonHelper::contactStatusTypeDropDownList(), [
                'class' => 'contact_type-dropdown form-control',
                'onchange' => "
                        var statusid = $(this).val();
                        console.log(statusid);
                        $.ajax('$urlInterac&status='+statusid+'', {
                            type: 'POST'
                        }).done(function(data) {
                            //$.pjax.reload({container: '#pjax-container'});
                        });                 
                        ",
            ]);
            $data[]=[
                'id'=>$reg_id,
                'contact_id'=>$id,
                'registration_name'=>$full_name,
                'phone'=>$phone,
                'contact_type'=>$contact_type,
                'media'=>$media,
                'registration_date'=>$registration_date,
                'registration_status'=>$reg_status,
            ];
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return json_encode($response);
    }

    //Load Rsvp Table With Data Table
    public function actionDevelopmentsRsvp($development_id){
        $baseurl=Yii::getAlias('@baseUrl');
        $draw = $_POST['draw'];
        $row = $_POST['start'];
        $rowperpage = $_POST['length']; // Rows display per page
        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc*/
        if ($columnSortOrder=='asc'){
            $short_order='desc';
        }else{
            $short_order='asc';
        }
        $searchValue = $_POST['search']['value']; // Search value
        $connection = Yii::$app->getDb();

        $searchQuery = " ";
        $searchQuery = " ";
        if($searchValue != ''){
            $searchQuery = " and (rsvp.id like '%".$searchValue."%' or 
                    cont.first_name  like '%".$searchValue."%' or 
                    cont.phone  like '%".$searchValue."%' or 
                    leads.lead_source_name  like '%".$searchValue."%' or 
                    sys.contact_type  like '%".$searchValue."%' or 
                    cont.last_name like'%".$searchValue."%' ) ";
        }
        //Count and Search
        $rsvp_query_count="select rsvp.id,rsvp.contact_id,rsvp.rsvp_date,rsvp.rsvp_status,cont.first_name,cont.last_name,cont.email,cont.phone,sys.contact_type,leads.lead_source_name
                            from `conn_contacts_rsvp` rsvp 
                            JOIN `conn_contacts` cont ON cont.id=rsvp.contact_id 
                            JOIN `conn_contacts_systeminfo` sys ON sys.contact_id=rsvp.contact_id 
                            JOIN `conn_setting_lead_source` leads ON leads.id=cont.lead_source
                            WHERE rsvp.development_id=".$development_id." 
                            ".$searchQuery."
                            GROUP by rsvp.id
                            ORDER by rsvp.id ".$short_order."";

        $command = $connection->createCommand($rsvp_query_count);
        $records =$command->queryAll();
        $totalRecordwithFilter =count($records);

        $rsvp_query="select rsvp.id,rsvp.contact_id,rsvp.rsvp_date,rsvp.rsvp_status,cont.first_name,cont.last_name,cont.email,cont.phone,sys.contact_type,leads.lead_source_name
                    from `conn_contacts_rsvp` rsvp 
                    JOIN `conn_contacts` cont ON cont.id=rsvp.contact_id 
                    JOIN `conn_contacts_systeminfo` sys ON sys.contact_id=rsvp.contact_id 
                    JOIN `conn_setting_lead_source` leads ON leads.id=cont.lead_source
                    WHERE rsvp.development_id=".$development_id." 
                    ".$searchQuery."
                    GROUP by rsvp.id
                    ORDER by rsvp.id ".$short_order."
                    LIMIT ".$row.",".$rowperpage."
                    ";
        $command_records =$connection->createCommand($rsvp_query);
        $rsvp_record =$command_records->queryAll();

        $data=[];
        foreach ($rsvp_record as $rsvpItem){
            $rsvp_id=$rsvpItem['id'];
            $contact_id=$rsvpItem['contact_id'];
            $first_name=!empty($rsvpItem['first_name'])?$rsvpItem['first_name']:'';
            $last_name=!empty($rsvpItem['last_name'])?$rsvpItem['last_name']:'';
            $name=$first_name.' '.$last_name;
            $phone=!empty($rsvpItem['phone'])?$rsvpItem['phone']:'';
            $contact_type=!empty($rsvpItem['contact_type'])?$rsvpItem['contact_type']:'Prospect';
            $media=!empty($rsvpItem['lead_source_name'])?$rsvpItem['lead_source_name']:'';
            $registration_date=CommonHelper::changeDateFormat($rsvpItem['rsvp_date'], 'd M Y H g:i a');
            $id='<a class="" href="'.$baseurl.'/backend/web/contacts/contacts-rsvp/update?id='.$rsvp_id.'">'.$rsvp_id.'</a>';
            $full_name='<a class="" href="'.$baseurl.'/backend/web/contacts/contacts/view?id='.$contact_id.'">'.$name.'</a>';
            $urlInterac = $baseurl.'/backend/web/developments/developments/change-rsvp-status?id='.$rsvpItem['id'];
            $rsvp_status=Html::dropDownList('contact_type-dropdown',$rsvpItem['rsvp_status'], CommonHelper::rsvpStatusTypeDropDownList(), [
                'class' => 'contact_type-dropdown form-control',
                'onchange' => "
                        var statusid = $(this).val();
                        console.log(statusid);
                        $.ajax('$urlInterac&status='+statusid+'', {
                            type: 'POST'
                        }).done(function(data) {
                            //$.pjax.reload({container: '#pjax-container'});
                        });                 
                        ",
            ]);
            $data[]=[
                'id'=>$rsvp_id,
                'contact_id'=>$id,
                'contact_name'=>$full_name,
                'phone'=>$phone,
                'contact_type'=>$contact_type,
                'media'=>$media,
                'submission_date'=>$registration_date,
                'rsvp_status'=>$rsvp_status,
            ];
        }
        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecordwithFilter,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return json_encode($response);
    }

    //Count registraion rsvp and appointment
    public function actionDevelopmentsRegistrationCount($tab_section,$development_id){
        $connection = Yii::$app->getDb();
        if ($tab_section=='Registrations') {
            $query_reg = "select reg.id,cont.email,sys.contact_type
                from `conn_contacts_registration` reg 
                JOIN `conn_contacts` cont ON cont.id=reg.contact_id 
                JOIN `conn_contacts_systeminfo` sys ON sys.contact_id=reg.contact_id 
                WHERE reg.development_id=" . $development_id . "
                GROUP by reg.id";
        }elseif ($tab_section=='RSVP'){
            $query_reg = "select reg.id,cont.email,sys.contact_type
                from `conn_contacts_rsvp` reg 
                JOIN `conn_contacts` cont ON cont.id=reg.contact_id 
                JOIN `conn_contacts_systeminfo` sys ON sys.contact_id=reg.contact_id 
                WHERE reg.development_id=".$development_id."
                GROUP by reg.id";
        }elseif ($tab_section=='Appointments'){
            $query_reg="SELECT * FROM `conn_contacts_systeminfo` WHERE `development_id`=".$development_id." AND `event`='true' and `form_type`='appointment'";
        }

        $command = $connection->createCommand($query_reg);
        $registration_record =$command->queryAll();
        $total_record =count($registration_record);
        $prospects=$agent=0;
        foreach ($registration_record as $regItem){
            if ($regItem['contact_type']=='Prospect'){
                $prospects++;
            }
            elseif ($regItem['contact_type']==''){
                $prospects++;
            }
            elseif ($regItem['contact_type']=='Agent'){
                $agent++;
            }
        }
        $data=[
            'tab_section_result'=>$tab_section,
            'total_reg'=>$total_record,
            'agent'=>$agent,
            'prospect'=>$prospects,
        ];

        return json_encode($data);
    }

}

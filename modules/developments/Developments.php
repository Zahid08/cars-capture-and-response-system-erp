<?php

namespace unlock\modules\developments;

/**
 * Class Developments
 */
class Developments extends \yii\base\Module
{
    /**
     * @var string the namespace that controller classes are in
     */
    public $controllerNamespace = 'unlock\modules\developments\controllers';

    /**
    * @var string the namespace that view file are in
    */
    public $viewNamespace = '@unlock/modules/developments/views/';
}

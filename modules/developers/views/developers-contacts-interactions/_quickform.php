<?php

use yii\helpers\Url;
use yii\helpers\Html;

use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtons;
use unlock\modules\core\buttons\BackButton;
use yii\helpers\ArrayHelper;
use kartik\datetime\DateTimePicker;
use unlock\modules\developers\models\DevelopersContacts;

/* @var $this yii\web\View */
/* @var $model unlock\modules\developers\models\DevelopersContacts */
/* @var $form yii\bootstrap\ActiveForm */

$this->title= ($model->isNewRecord) ? Yii::t('app', 'Create a New Contact') : Yii::t('app', 'Update Developer Contact');
?>

<div class="main-container main-container-form" role="main">
    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
        /*'options' => ['enctype' => 'multipart/form-data'],*/
        'id' => 'add-contact-form-1',
        'validateOnBlur' =>true,
        'enableAjaxValidation'=>false,
        'errorCssClass'=>'has-error',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
            'options' => ['class' => 'form-group'],
            'horizontalCssClasses' => [
                'label' => 'col-sm-2',
                'offset' => '',
                'wrapper' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'developer_contact_id')->hiddenInput(['value'=>$developer_id])->label(false) ?>

    <?= $form->field($model, 'interaction_type')->dropDownList(CommonHelper::interactionTypeDropDownList(), ['prompt'=>'Select interaction type']) ?>


    <?= $form->field($model, 'descriptions')->textarea(['rows' => 6]) ?>


    <div style="margin-bottom: 10px;" class="form-group field-interactions-datetitme required">
        <label class="control-label col-sm-2" for="developerscontacts-interactions_datetitme">Date</label>
        <div class="col-sm-10">
            <?php
            $todaydatetime = date("Y-m-d H:i:s");
            echo DateTimePicker::widget([
                'name' => 'DevelopersContactsInteractions[interactions_datetitme]',
                'value' => $todaydatetime,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd hh:ii:ss',
                ]
            ]);
            ?>
        </div>
        <p class="help-block help-block-error "></p>
    </div>


    <?php
        $users = ArrayHelper::map(\unlock\modules\usermanager\user\models\User::find()->all(), 'id',function($model) {
            return $model['first_name'].' '.$model['last_name'];
        });
    ?>

    <?php /*
    <?=
    $form->field($model, 'user_id')->widget(Select2::classname(), [
        'data' => $users,
        'language' => 'de',
        'options' => ['placeholder' => 'Select a user ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>



    <?= $form->field($model, 'status')->dropDownList(CommonHelper::statusDropDownList(), ['prompt'=>'Select status']) ?>

    */ ?>

    <div class="admin-form-button">
        <div class="form-group">
            <div class="col-sm-10 col-sm-offset-2">
                <?= FormButtons::widget(['item' => ['save']]) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<?php

use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\widgets\DetailView;
use unlock\modules\core\buttons\BackButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $model unlock\modules\developers\models\DevelopersContactsInteractions */

$this->title = Yii::t('app', 'View Developers Contacts Interactions');
?>
<div class="main-container main-container-view" role="main">
    <div class="page-header">
        <div class="page-header-title">
            <h1 class="page-title"><?= Yii::t('app', 'Developers Contacts Interactions') ?></h1>
        </div>
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li><?= Html::a(Yii::t('app', 'Developers Contacts Interactions'), Url::to('index')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-view-container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fa fa-eye"></i> <?= Yii::t('app', 'View') ?> - <?= Html::encode($model->id) ?></h3>
                </div>
                <div class="panel-heading-button">
                    <?= BackButton::widget() ?>
                </div>
            </div>

            <div class="panel-body">
                <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                	'id',
					'developer_contact_id',
					'interaction_type',
					'descriptions:ntext',
					'interactions_datetitme',
					'user_id',
                ],
                ]) ?>
            </div>

        </div>
    </div>
</div>

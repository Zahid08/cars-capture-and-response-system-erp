<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtons;
use unlock\modules\core\buttons\BackButton;
use yii\helpers\ArrayHelper;
use unlock\modules\developers\models\DevelopersContacts;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model unlock\modules\developers\models\DevelopersContactsInteractions */
/* @var $form yii\bootstrap\ActiveForm */

$this->title= ($model->isNewRecord) ? Yii::t('app', 'Create DevelopersContactsInteractions') : Yii::t('app', 'Update DevelopersContactsInteractions');
?>

<div class="main-container main-container-form" role="main">
    <div class="page-header">

        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a(Yii::t('app', 'Home'), Yii::$app->homeUrl) ?></li>
                <li><?= Html::a(Yii::t('app', 'Developers Contacts Interactions'), Url::to('index')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-form-container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fa fa-pencil"></i> <?= Html::encode($this->title) ?></h3>
                </div>
                <div class="panel-heading-button">
                    <?= BackButton::widget() ?>
                </div>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
                    /*'options' => ['enctype' => 'multipart/form-data'],*/
                    'validateOnBlur' =>true,
                    'enableAjaxValidation'=>true,
                    'errorCssClass'=>'has-error',
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
                        'options' => ['class' => 'form-group'],
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-2',
                            'offset' => '',
                            'wrapper' => '',
                            'hint' => '',
                        ],
                    ],
                ]); ?>

                <?php
                $DevelopersContacts = ArrayHelper::map(DevelopersContacts::find()->all(), 'id',function($model) {
                    return $model['first_name'].' '.$model['last_name'];
                });
                ?>
                <?=
                $form->field($model, 'developer_contact_id')->widget(Select2::classname(), [
                    'data' => $DevelopersContacts,
                    'language' => 'de',
                    'options' => ['placeholder' => 'Select a Developer Contact ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

                <?= $form->field($model, 'interaction_type')->dropDownList(CommonHelper::interactionTypeDropDownList(), ['prompt'=>'Select interaction type']) ?>


				<?= $form->field($model, 'descriptions')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'interactions_datetitme')->widget(
                    DateTimePicker::className(), [
                    'name' => 'interactions_datetitme',
                    'value' => '12-31-2010 05:10:20',
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd hh:ii:ss'
                    ]
                ]);?>

                <?php
                $users = ArrayHelper::map(\unlock\modules\usermanager\user\models\User::find()->all(), 'id',function($model) {
                    return $model['first_name'].' '.$model['last_name'];
                });
                ?>

                <?=
                $form->field($model, 'user_id')->widget(Select2::classname(), [
                    'data' => $users,
                    'language' => 'de',
                    'options' => ['placeholder' => 'Select a user ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>


                <div class="admin-form-button">
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <?= FormButtons::widget() ?>
                        </div>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

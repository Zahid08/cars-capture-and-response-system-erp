<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\buttons\SearchFilterButton;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model unlock\modules\developers\models\DevelopersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="developers-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
        'validateOnBlur' => false,
        'enableAjaxValidation' => false,
        'errorCssClass'=>'has-error',
        'fieldConfig' => [
            'template' => "{label}\n{input}\n",
            'options' => ['class' => 'col-sm-4'],
            'horizontalCssClasses' => [
                'label' => '',
                'offset' => '',
                'wrapper' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?php //$form->field($model, 'id') ?>
    <div class="admin-search-form-fields">
        <div class="row">
            <div class="col-sm-12 col-md-5 col-lg-6">
                <?= $form->field($model, 'developer_name') ?>
            </div>
            <div class="col-sm-12 col-md-3 col-lg-3">
                <?= $form->field($model, 'developer_grade')->dropDownList(\unlock\modules\core\helpers\CommonHelper::developerGradeDropDownList(),['prompt'=>'Select Grade']) ?>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-3">
                <div class="filter-custom-search-btn">
                    <?= SearchFilterButton::widget() ?>
                    <?php //ResetFilterButton::widget(['url' => Url::toRoute(['index'])]) ?>
                    <a class="btn btn-default" href="<?php echo Url::toRoute(['index']);?>"> Reset Filter</a>
                </div>
            </div>
        </div>
    </div>



    <?php //$form->field($model, 'phone') ?>

    <?php //$form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'city') ?>

    <?php // echo $form->field($model, 'suite') ?>

    <?php // echo $form->field($model, 'postal') ?>

    <?php // echo $form->field($model, 'province') ?>

    <?php // echo $form->field($model, 'website_url') ?>

    <?php // echo $form->field($model, 'logo_image') ?>

    <?php // echo $form->field($model, 'lifestyle_image') ?>

    <?php // echo $form->field($model, 'excerpt_text') ?>

    <?php // echo $form->field($model, 'overview_text') ?>

    <?php // echo $form->field($model, 'description_text') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'status') ?>



    <?php ActiveForm::end(); ?>

</div>

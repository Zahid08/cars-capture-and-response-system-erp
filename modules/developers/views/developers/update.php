<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model unlock\modules\developers\models\Developers */

$this->title = 'Update Developer: ' . $model->developer_name;
$this->params['breadcrumbs'][] = ['label' => 'Developers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->developer_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="developers-update">
    
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

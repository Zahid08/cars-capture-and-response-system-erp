<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model unlock\modules\developers\models\Developers */

$this->title = 'Create a New Developer';
$this->params['breadcrumbs'][] = ['label' => 'Developers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="developers-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>

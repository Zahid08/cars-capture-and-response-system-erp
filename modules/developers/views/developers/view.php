<?php

use yii\helpers\Html;

use yii\bootstrap\ActiveForm;
use yii\widgets\DetailView;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\usermanager\user\models\User;
use yii\helpers\ArrayHelper;
use kartik\datetime\DateTimePicker;
use unlock\modules\core\buttons\FormButtons;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use unlock\modules\generalsettings\models\City;

/* @var $this yii\web\View */
/* @var $model unlock\modules\developers\models\Developers */

$this->title = $model->developer_name;
$this->params['breadcrumbs'][] = ['label' => 'Developers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="developers-view">

    <div class="panel panel-primary">
        <div class="panel-heading">Developer Details</div>
        <div class="panel-body">
            <div class="developer-heading">
                <div class="row">
                    <div class="col-md-4 col-12 developer-name">
                        <h3><?php echo $model->developer_name;?></h3>
                        <p style="margin: 0"><a style="text-decoration: underline;" target="_blank" href="<?php echo $model->website_url; ?>"><?php echo $model->website_url; ?></a></p>
                    </div>
                    <div class="col-md-4 col-12 developer-grade">
                        <h3 style="margin: 10px 0px !important;">Grade: <span class="grade-value"><?php echo CommonHelper::developerGradeDropDownList($model->developer_grade); ?></span></h3>
                    </div>
                    <div class="col-md-4 col-12 developer-id">
                        <h3 style="margin: 10px 0px !important;">ID: <span class="grade-value"><?php echo $model->id;?></span></h3>
                    </div>
                </div>
            </div>
            <div class="divider"></div>

            <div class="developer-status-details">
                <div class="row">
                    <div class="col-md-4 col-12 developer-name">
                        <p><span class="grade-value">Created at: </span><?php echo CommonHelper::changeDateFormat($model->created_at, 'd M Y') ; ?></p>
                    </div>
                    <div class="col-md-4 col-12 developer-grade">
                        <p><span class="grade-value">Last Update: </span><?php echo CommonHelper::changeDateFormat($model->updated_at, 'd M Y') ; ?></p>
                    </div>
                    <div class="col-md-4 col-12 developer-id">
                        <p><span class="grade-value">Status: </span><?php echo CommonHelper::developerStatusDropDownList($model->status); ?></p>
                    </div>
                </div>
            </div>
            <div class="divider"></div>
            <div id="app" class="tab_section">

                <tabs>
                    <?php
                          Modal::begin([
                              'header' => '<h4>Add Developer Contact</h4>',
                              'id'     => 'model-contact',
                              'size'   => 'model-lg',
                          ]);

                          echo "<div id='add-contact-modelContent'></div>";

                          Modal::end();
                      ?>

                    <?php
                      Modal::begin([
                          'header' => '<h4>Add Contact Interaction</h4>',
                          'id'     => 'model-interaction',
                          'size'   => 'model-lg',
                      ]);

                      echo "<div id='add-contactInteraction-modelContent'></div>";

                      Modal::end();
                    ?>
                    <?php
                    Modal::begin([
                        'header' => '<h4>Update Contact Interaction</h4>',
                        'id'     => 'update-model-interaction',
                        'size'   => 'model-lg',
                    ]);

                    echo "<div id='update-contactInteraction-modelContent'></div>";

                    Modal::end();
                    ?>
                    <?php
                        Modal::begin([
                            'header' => '<h4>Update Developer Contact</h4>',
                            'id'     => 'update-model-contact',
                            'size'   => 'model-lg',
                        ]);

                        echo "<div id='update-contact-modelContent'></div>";

                        Modal::end();
                    ?>
                    <?php
                        Modal::begin([
                            'header' => '<h4>View Developer Contact</h4>',
                            'id'     => 'view-model-contact',
                            'size'   => 'model-lg',
                        ]);

                        echo "<div id='view-contact-modelContent'></div>";

                        Modal::end();
                    ?>
                    <?php
                    Modal::begin([
                        'header' => '<h4>Create Recognition</h4>',
                        'id'     => 'create-model-recognition',
                        'size'   => 'model-lg',
                    ]);

                    echo "<div id='create-recognition-modelContent'></div>";

                    Modal::end();
                    ?>
                    <?php
                    Modal::begin([
                        'header' => '<h4>Update Recognition</h4>',
                        'id'     => 'update-model-recognition',
                        'size'   => 'model-lg',
                    ]);

                    echo "<div id='update-recognition-modelContent'></div>";

                    Modal::end();
                    ?>
                  <?php
                    $developercontacts = CommonHelper::developerByDeveloperContact($model->id);
                  ?>
                  <tab name="Developer Contacts" :selected="true">
                      
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Title</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Last Contact</th>
                                <th>Next Contact</th>
                                <th>Primary</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            foreach ($developercontacts as $developercontact){
                            ?>
                            <tr>
                                <td><?php echo $developercontact->id; ?></td>
                                <td>
                                    <?= Html::a($developercontact->first_name.' '.$developercontact->last_name, ['developers-contacts/view', 'id' => $developercontact->id]) ?>
                                </td>
                            
                                <td><?php echo $developercontact->job_title; ?></td>
                           
                                <td><?php echo $developercontact->phone; ?></td>
                            
                                <td><a href="#"><?php echo $developercontact->email; ?></a></td>
                           
                                <td><?php echo CommonHelper::changeDateFormat($developercontact->last_contact, 'd M Y g:i a'); ?></td>
                            
                                <td><?php echo CommonHelper::changeDateFormat($developercontact->next_contact, 'd M Y g:i a'); ?></td>

                                <!--<td>
                                    <?/*= Html::button('<span class="glyphicon glyphicon-pencil"></span>', ['value' => \yii\helpers\Url::to(['developers-contacts/quick-update?id='.$developercontact->id.'']), 'class' => 'update-contact-modelButton btn btn-success']) */?>
                                    <?php
/*                                        $urldevcontact = yii\helpers\Url::to(['developers-contacts/delete?id='.$developercontact->id.'&developerid='.$model->id]);
                                    */?>
                                    <?/*= Html::a(Yii::t('yii', '<span class="glyphicon glyphicon-trash"></span>'), '#', [
                                        'title' => Yii::t('yii', 'Delete'),
                                        'aria-label' => Yii::t('yii', 'Delete'),
                                        'onclick' => "
                                            if (confirm('Are you sure you want to delete this item?')) {
                                                $.ajax('$urldevcontact', {
                                                    type: 'POST'
                                                }).done(function(data) {
                                                    $.pjax.reload({container: '#pjax-container'});
                                                });
                                            }
                                            return false;
                                        ",
                                    ]); */?>
                                </td>-->
                                <td>
                                    <?php
                                    $checked = '';
                                    if($model->primary_contact_id == $developercontact->id){
                                        $checked = 'checked';
                                    }
                                    ?>

                                    <label class="custom-radio">
                                        <input <?php echo $checked;?> data-type="<?php echo $model->id;?>" class="primary_contact_check" name="primary-check" type="radio" value="<?php echo $developercontact->id;?>">
                                        <span class="checkmark"></span>
                                    </label>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                  </tab>
                  <tab name="Interactions">

                  <?php
                        $developercontactsInteraction = CommonHelper::developerByDeveloperContactInteraction($model->id);
                  ?>
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Interaction With</th>
                                <th>Description</th>
                                <th>Type</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>User</th>
                                <th width="90px">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach ($developercontactsInteraction as $interaction){
                            ?>
                            <tr>
                                <td><?php echo $interaction->id; ?></td>

                                <td>
                                    <?php $developerContactInfo = CommonHelper::developerContactById($interaction->developer_contact_id); ?>
                                    <?= Html::a($developerContactInfo->first_name.' '.$developerContactInfo->last_name, ['developers-contacts/view', 'id' => $developerContactInfo->id]) ?>
                                </td>
                            
                                <td><?php echo $interaction->descriptions; ?></td>
                           
                                <td><?php echo CommonHelper::interactionTypeListById($interaction->interaction_type)?></td>
                            
                                <td><?php echo CommonHelper::changeDateFormat($interaction->interactions_datetitme, 'd M Y'); ?></td>
                           
                                <td><?php echo CommonHelper::changeDateFormat($interaction->interactions_datetitme, 'g:i a'); ?></td>
                            
                                <td>
                                    <?php $user = CommonHelper::getUserById($interaction->user_id)?>
                                    <?php
                                        echo $user->first_name.' '.$user->last_name;
                                    ?>
                                </td>
                                <td>
                                    <?= Html::button('<span class="glyphicon glyphicon-pencil"></span>', ['value' => \yii\helpers\Url::to(['developers-contacts-interactions/quick-update?id='.$interaction->id.'']), 'class' => 'update-contactInteraction-modelButton btn btn-success']) ?>
                                    <?php
                                        $urlInterac = yii\helpers\Url::to(['developers-contacts-interactions/delete?id='.$interaction->id.'&developerid='.$model->id]);
                                    ?>
                                    <?= Html::a(Yii::t('yii', '<span class="glyphicon glyphicon-trash"></span>'), '#', [
                                        'title' => Yii::t('yii', 'Delete'),
                                        'aria-label' => Yii::t('yii', 'Delete'),
                                        'onclick' => "
                                            if (confirm('Are you sure you want to delete this item?')) {
                                                $.ajax('$urlInterac', {
                                                    type: 'POST'
                                                }).done(function(data) {
                                                    $.pjax.reload({container: '#pjax-container'});
                                                });
                                            }
                                            return false;
                                        ",
                                    ]); ?>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                  </tab>
                  <tab name="Developments">
                       <table class="table table-striped table-bordered">
                            <?php
                            $developmentsList = CommonHelper::developerIdByDevelopmentList($model->developer_id);

                            ?>
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Development Name</th>
                                    <th>Grade</th>
                                    <th>Sales Status</th>
                                    <th>Type</th>
                                    <th>Launch Date</th>
                                    <th>Launch State</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(!empty($developmentsList)){ foreach ($developmentsList as $development){
                                    ?>
                                <tr>
                                    <td><?php echo $development['development_id']; ?></td>

                                    <td>
                                        <?php
                                            echo Html::a($development['development_name'], ['/developments/developments/view', 'id' => $development['id']]);
                                        ?>
                                    </td>

                                    <td>
                                        <?php
                                            echo CommonHelper::developerGradeDropDownList($development['grade']);
                                        ?>
                                    </td>

                                    <td>
                                        <?php
                                            echo CommonHelper::developmentSalesStatusDropDownList($development['sales_status']);
                                        ?>
                                    </td>

                                    <td>NULL</td>

                                    <td>NULL</td>

                                    <td>NULL</td>
                                </tr>
                                <?php }} ?>
                            </tbody>
                        </table>
                  </tab>
                  <tab name="Address">
                      <?php $form = ActiveForm::begin(
                          [
                              'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
                              /*'options' => ['enctype' => 'multipart/form-data'],*/
                              'validateOnBlur' =>true,
                              'enableAjaxValidation'=>false,
                              'errorCssClass'=>'has-error',
                              'id' => 'update-form-2',
                              'action' => Url::to(['developers/update', 'id' => $model->id]),
                              'fieldConfig' => [
                                  'template' => "{label}\n<div class=\"col-sm-9\">\n{input}\n{hint}\n{error}\n</div>",
                                  'options' => ['class' => 'form-group'],
                                  'horizontalCssClasses' => [
                                      'label' => 'col-sm-3',
                                      'offset' => '',
                                      'wrapper' => '',
                                      'hint' => '',
                                  ],
                              ],
                          ]
                      ); ?>
                      <div class="row">
                          <div class="col-md-6">
                              <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

                              <?= $form->field($model, 'suite')->textInput(['maxlength' => true]) ?>

                              <?= $form->field($model, 'postal')->textInput(['maxlength' => true]) ?>
                          </div>
                          <div class="col-md-6">

                              <?= $form->field($model, 'province')->dropDownList(CommonHelper::provinceDropDownList(),
                                  [
                                      'prompt'=> 'Select a Province..',
                                      'onChange' => '
                                    $.post("city?id='.'"+$(this).val(), function(data){
                                        $("select#developers-city").html(data);
                                    });
                                ',
                                      'class'=>'form-control select2'
                                  ]
                              ); ?>
                              <?php
                              if(!empty($model->city)){
                                  $city = ArrayHelper::map(City::find()->where(['province_id' => $model->province])->all(), 'id', 'city_name');
                                  ?>
                                  <?= $form->field($model, 'city')->dropDownList(
                                      $city
                                  //[''=> 'Select a City']
                                  );?>
                                  <?php
                              }else{
                                  ?>
                                  <?= $form->field($model, 'city')->dropDownList(['' => 'Select a City']) ?>
                                  <?php
                              }
                              ?>


                              <?= $form->field($model, 'website_url')->textInput(['maxlength' => true]) ?>
                          </div>
                      </div>
                      <?php ActiveForm::end(); ?>

                  </tab>
                  <tab name="Website">
                      <?php $form = ActiveForm::begin(
                              [
                              'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
                              /*'options' => ['enctype' => 'multipart/form-data'],*/
                              'validateOnBlur' =>true,
                              'enableAjaxValidation'=>false,
                              'errorCssClass'=>'has-error',
                              'id' => 'update-form-1',
                              'action' => Url::to(['developers/update', 'id' => $model->id]),
                              'fieldConfig' => [
                                  'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
                                  'options' => ['class' => 'form-group'],
                                  'horizontalCssClasses' => [
                                      'label' => 'col-sm-2',
                                      'offset' => '',
                                      'wrapper' => '',
                                      'hint' => '',
                                  ],
                              ],
                          ]
                      ); ?>
                      <div class="divider"></div>
                      <div class="row vertical-align">
                          <div class="col-md-8">
                              <?= $form->field($model, 'connect_url')->textInput() ?>
                          </div>
                          <div class="col-md-4 text-center">
                              <?= Html::button('Load From Connect', ['class' => 'btn btn-success', 'id' => 'load-form-connect']) ?>
                          </div>
                      </div>
                      <br>
                      <div class="row">
                          <div class="col-md-6 ">
                              <?= $form->field($model, 'logo_image')->fileInput(); ?>
                              <?= $form->field($model, 'logo_img_url')->hiddenInput()->label(false); ?>
                              <?php
                              if(!empty($model->logo_image)){
                                  echo '<img class="logo-imgprev" style="margin-left: 75px;width: 190px;border: 3px solid #ddd;border-radius: 2px;background: #ddd;padding: 5px;margin-bottom: 20px;" src="'.Yii::$app->request->BaseUrl.'/uploads/developers/'.$model->logo_image.'" alt="'.$model->developer_name.'" />';
                              }else{
                                  echo '<img class="logo-imgprev" style="margin-left: 75px;width: 190px;border: 3px solid #ddd;border-radius: 2px;background: #ddd;padding: 5px;margin-bottom: 20px;" src="'.Yii::$app->request->BaseUrl.'/uploads/not-found-img.png" alt="#" />';
                              }
                              ?>
                          </div>
                          <div class="col-md-6">
                              <?php /* $form->field($model, 'lifestyle_image')->widget(FileInput::classname(),['dirName' => 'developers', 'template' => '{preview}{input}', 'thumbSize' => ['120px', '120px']]); */ ?>
                              <?= $form->field($model, 'lifestyle_image')->fileInput(); ?>
                              <?= $form->field($model, 'lifestyle_img_url')->hiddenInput()->label(false); ?>
                              <?php
                              if(!empty($model->lifestyle_image)){
                                  echo '<img  class="lifestyle-imgprev" style="margin-left: 75px;width: 190px;border: 3px solid #ddd;border-radius: 2px;background: #ddd;padding: 5px;margin-bottom: 20px;" src="'.Yii::$app->request->BaseUrl.'/uploads/developers/'.$model->lifestyle_image.'" alt="'.$model->developer_name.'" />';
                              }else{
                                  echo '<img class="lifestyle-imgprev" style="margin-left: 75px;width: 190px;border: 3px solid #ddd;border-radius: 2px;background: #ddd;padding: 5px;margin-bottom: 20px;" src="'.Yii::$app->request->BaseUrl.'/uploads/not-found-img.png" alt="#" />';
                              }
                              ?>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-md-12">
                              <?= $form->field($model, 'excerpt_text')->textarea(['rows' => 6]) ?>

                              <?= $form->field($model, 'overview_text')->textarea(['rows' => 6]) ?>

                              <?= $form->field($model, 'description_text')->textarea(['rows' => 6]) ?>
                          </div>
                      </div>
                      <?php ActiveForm::end(); ?>
                  </tab>
                  <tab name="Recognition">
                      <?php
                            $developerRecognition = CommonHelper::developerByDeveloperRecognition($model->id);
                      ?>
                      <table class="table table-striped table-bordered">
                          <thead>
                          <tr>
                              <th>ID</th>
                              <th>Recognition Name</th>
                              <th>Recognition Type</th>
                              <th>Year</th>
                              <th style="width: 85px;">Action</th>
                          </tr>
                          </thead>
                          <tbody>
                          <?php  foreach ($developerRecognition as $recognition){  ?>
                          <tr>
                              <td><?php echo $recognition->id; ?></td>

                              <td><?php echo $recognition->recognition_name; ?></td>

                              <td><?php echo CommonHelper::developersRecognitionTypeDropDownList($recognition->type); ?></td>

                              <td><?php echo $recognition->year; ?></td>
                              <td>
                                  <?= Html::button('<span class="glyphicon glyphicon-pencil"></span>', ['value' => \yii\helpers\Url::to(['developers-recognitions/quick-update?id='.$recognition->id.'']), 'class' => 'update-recognition-modelButton btn btn-success']) ?>
                                  <?php
                                    $urlrec = yii\helpers\Url::to(['developers-recognitions/delete?id='.$recognition->id.'&developerid='.$model->id]);
                                  ?>
                                  <?= Html::a(Yii::t('yii', '<span class="glyphicon glyphicon-trash"></span>'), '#', [
                                        'title' => Yii::t('yii', 'Delete'),
                                        'aria-label' => Yii::t('yii', 'Delete'),
                                        'onclick' => "
                                            if (confirm('Are you sure you want to delete this item?')) {
                                                $.ajax('$urlrec', {
                                                    type: 'POST'
                                                }).done(function(data) {
                                                    $.pjax.reload({container: '#pjax-container'});
                                                });
                                            }
                                            return false;
                                        ",
                                    ]); ?>
                              </td>
                          </tr>
                          <?php } ?>
                          </tbody>
                      </table>
                  </tab>
                </tabs>
            </div>
            <div class="divider"></div>
            <div class="button_group">
                <div class="row">
                    <div class="pull-left form_action_btn_2">

                        <?= Html::button('Add Contact', ['id' => 'add-contact-modelButton', 'value' => \yii\helpers\Url::to(['developers-contacts/quick-create?devloperid='.$model->id.'']), 'class' => 'btn btn-success']) ?>

                        <!-- <button type="button" id="add-contact" class="btn btn-success">Add Contact</button> -->
                        <?= Html::button('Add Interaction', ['id' => 'add-contactInteraction-modelButton', 'value' => \yii\helpers\Url::to(['developers-contacts-interactions/quick-create?devloperid='.$model->id.'']), 'class' => 'btn btn-success']) ?>

                        <button type="button" class="btn btn-success">Add Development</button>

                        <?= Html::button('Add Recognition', ['id' => 'add-recognition-modelButton', 'value' => \yii\helpers\Url::to(['developers-recognitions/quick-create?devloperid='.$model->id.'']), 'class' => 'btn btn-success']) ?>

                    </div>
                    <div class="pull-right form_action_btn_2">
                        <?= Html::a('Edit Developer', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    </div>
                </div>
                <br>
                <div class="update_btn">
                    <div class="row">
                        <?= FormButtons::widget(['item' => ['save','cancel']]) ?>
                        <?php
                        $contactUrl = yii\helpers\Url::to(['developers/delete?id='.$model->id]);
                        ?>
                        <?= Html::a(Yii::t('yii', '<span class="glyphicon glyphicon-trash"></span> Delete'), '#', [
                            'class' => 'btn btn-danger',
                            'title' => Yii::t('yii', 'Delete'),
                            'aria-label' => Yii::t('yii', 'Delete'),
                            'onclick' => "
                                if (confirm('Are you sure you want to delete this item?')) {
                                    $.ajax('$contactUrl', {
                                        type: 'POST'
                                    }).done(function(data) {
                                        $.pjax.reload({container: '#pjax-container'});
                                    });
                                }
                                return false;
                            ",
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?php
$webliveUrl = Yii::getAlias('@webliveUrl');
$baseUrl = Yii::getAlias('@baseUrl');
$js = <<< JS
    $(function(){
        $('#add-contact-modelButton').click(function(){
            $('#model-contact').modal('show')
                .find('#add-contact-modelContent')
                .load($(this).attr('value'));
        });
        $('.update-contact-modelButton').click(function(){
            $('#update-model-contact').modal('show')
                .find('#update-contact-modelContent')
                .load($(this).attr('value'));
        });
        $('.view-contact-modelButton').click(function(){
            $('#view-model-contact').modal('show')
                .find('#view-contact-modelContent')
                .load($(this).attr('value'));
        });   
        
        $('#add-recognition-modelButton').click(function(){
            $('#create-model-recognition').modal('show')
                .find('#create-recognition-modelContent')
                .load($(this).attr('value'));
        });
        $('#add-contactInteraction-modelButton').click(function(){
            $('#model-interaction').modal('show')
                .find('#add-contactInteraction-modelContent')
                .load($(this).attr('value'));
        });
        $('.update-contactInteraction-modelButton').click(function(){
            $('#update-model-interaction').modal('show')
                .find('#update-contactInteraction-modelContent')
                .load($(this).attr('value'));
        });
        
        $('.update-recognition-modelButton').click(function(){
            $('#update-model-recognition').modal('show')
                .find('#update-recognition-modelContent')
                .load($(this).attr('value'));
        });
        
        
        
        
        
        $('.update_btn button').click(function(){
            var form = $("#update-form-1");
            var formData = form.serialize();
            console.log(formData);
            $.ajax({
  
                url: form.attr("action"),
        
                type: form.attr("method"),
        
                data: formData,
            
                success: function (data) {
                    var obj = jQuery.parseJSON( JSON.stringify(data) );
                    console.log(obj);
                    $('button.btn.btn-primary').text("Save");
                },
                error: function () {
                    //$('button.btn.btn-primary').text("Save");
                    //alert("Something went wrong");
                    //$('button.btn.btn-primary').text("Save");
                },
                headers: {'Authorization': 'Basic bWFkaHNvbWUxMjM='},
                beforeSend: function (xhr) {
                    $('button.btn.btn-primary').text("Sending....");
                },
        
            });
        });
        $('.update_btn button').click(function(){
            var form = $("#update-form-2");
            var formData = form.serialize();
            console.log(formData);
            $.ajax({
  
                url: form.attr("action"),
        
                type: form.attr("method"),
        
                data: formData,
            
                success: function (data) {
                    var obj = jQuery.parseJSON( JSON.stringify(data) );
                    console.log(obj);
                    $('button.btn.btn-primary').text("Save");
                },
                error: function () {
                    //$('button.btn.btn-primary').text("Save");
                    //alert("Something went wrong");
                    //$('button.btn.btn-primary').text("Save");
                },
                headers: {'Authorization': 'Basic bWFkaHNvbWUxMjM='},
                beforeSend: function (xhr) {
                    $('button.btn.btn-primary').text("Sending....");
                },
        
            });
        });
        
        $('.primary_contact_check').click(function(){
            //alert($("input[name=primary-check]:checked").val());
            var primaryContactId = $("input[name=primary-check]:checked").val();
            var devId = $("input[name=primary-check]:checked").attr("data-type");
           
            $.ajax({
            
                url: 'primary-contact?devId='+devId+'&primary-contact-id='+primaryContactId,
        
                type: 'post',
        
                //data: formData,
            
                success: function (data) {
                    //var obj = jQuery.parseJSON( JSON.stringify(data) );
                    console.log(obj);
                    //$('button.btn.btn-primary').text("Save");
                },
                error: function () {
                    //$('button.btn.btn-primary').text("Save");
                    //alert("Something went wrong");
                    //$('button.btn.btn-primary').text("Save");
                },
                headers: {'Authorization': 'Basic bWFkaHNvbWUxMjM='},
        
            });
        });
    });
    
    
     $(document).on("beforeSubmit", "#add-contact-form-1", function () {
        var form = $(this);
        var formData = form.serialize();
        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: formData,
            success: function (data) {
                var obj = jQuery.parseJSON( JSON.stringify(data) );
                console.log(obj);
                $('button.btn.btn-primary').text("Save");
            },
            error: function () {
                //alert("Something went wrong");
            },
            headers: {'Authorization': 'Basic bWFkaHNvbWUxMjM='},
            beforeSend: function (xhr) {
                $('button.btn.btn-primary').text("Sending....");
            },
        });
     });
     
      $(document).on("beforeSubmit", "#add-contact-form-2", function () {
        var form = $(this);
        var formData = form.serialize();
        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: formData,
            success: function (data) {
                var obj = jQuery.parseJSON( JSON.stringify(data) );
                console.log(obj);
                $('button.btn.btn-primary').text("Save");
            },
            error: function () {
                //alert("Something went wrong");
            },
            headers: {'Authorization': 'Basic bWFkaHNvbWUxMjM='},
            beforeSend: function (xhr) {
                $('button.btn.btn-primary').text("Sending....");
            },
        });
     }).on('submit', function(e){
        e.preventDefault();
     });
      
     $(document).on("beforeSubmit", "#add-contact-form-3", function () {
        var form = $(this);
        var formData = form.serialize();
        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: formData,
            success: function (data) {
                var obj = jQuery.parseJSON( JSON.stringify(data) );
                console.log(obj);
                $('button.btn.btn-primary').text("Save");
            },
            error: function () {
                //alert("Something went wrong");
            },
            headers: {'Authorization': 'Basic bWFkaHNvbWUxMjM='},
            beforeSend: function (xhr) {
                $('button.btn.btn-primary').text("Sending....");
            },
        });
     }).on('submit', function(e){
        e.preventDefault();
     });
      
     
      jQuery(document).ready(function($) {
       $('#load-form-connect').click(function (index, value) {
           var loadurl = $('#developers-connect_url').val();
           
           var slugname = loadurl.split('/');
          
           var siteUrl = '$webliveUrl/wp-json/wp-connect/api?access_token=Y29uZG8tZGV2ZWxvcG1lbnRzVW5sb&post_type='+slugname[3]+'&slug='+slugname[4]+'';
           
           $.ajax({ 
                type: 'GET', 
                url: siteUrl, 
                //data: { login: login }, 
                dataType: 'json',
                success: function (data) {
                    //var jsonss = $.parseJSON(data);
                    var obj = jQuery.parseJSON( JSON.stringify(data) );
                    console.log(obj);
                    $.each(data, function(index, element) {
                        if(element.LogoImage == null){
                            $('img.logo-imgprev').attr('src', '$baseUrl/backend/web/uploads/not-found-img.png');
                            $('#developers-logo_img_url').val('false');
                        }else {
                            $('img.logo-imgprev').attr('src', element.LogoImage);
                            $('#developers-logo_img_url').val(element.LogoImage);
                        }
                        
                        if(element.LifestyleImage == null){
                            $('img.lifestyle-imgprev').attr('src', '$baseUrl/backend/web/uploads/not-found-img.png');
                             $('#developers-lifestyle_img_url').val('false');
                        }else{
                            $('img.lifestyle-imgprev').attr('src', element.LifestyleImage);
                            $('#developers-lifestyle_img_url').val(element.LifestyleImage);
                        }
                        
                        
                        $('#developers-excerpt_text').val(element.excerpt);
                        $('#developers-overview_text').val(element.overview);
                        $('#developers-description_text').val(element.description);
                        
                        
                        
                        
                    });
                    $('#load-form-connect').text("Load From Connect");
                },
                error: function (data) {
                    console.log('Loading Error.');
                    alert('URL Data not found. please check again developers url.');
                    $('#load-form-connect').text("Load From Connect");
                },
                headers: {'Authorization': 'Basic bWFkaHNvbWUxMjM='},
                beforeSend: function (xhr) {
                    $('#load-form-connect').text("Loading....");
                },
                
           });
       });
      // var rowCount = $('.fees-type-list tbody tr').length;
       //alert(rowCount);
       
        
    });
     
JS;
$this->registerJs($js);
?>

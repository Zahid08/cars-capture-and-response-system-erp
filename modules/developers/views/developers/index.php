<?php

use yii\helpers\Html;

use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\grid\GridView;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\buttons\NewButton;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel unlock\modules\developers\models\DevelopersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Developers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="developers-index">
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="admin-grid-content">
        <div class="admin-grid-toolbar">
            <div class="admin-grid-toolbar-left">
                <?php //  NewButton::widget(['url' => Url::toRoute(['create'])]) ?>
            </div>
            <div class="admin-grid-toolbar-right">
                <div class="col-md-8">
                    <?= Summary::widget(['dataProvider' => $dataProvider]) ?>
                </div>
                <div class="col-md-4">
                    <?= Html::a('Load', ['/developers/developers/load-developers'], ['class'=>'btn btn-primary pull-right']) ?>
                </div>
            </div>
        </div>
        <div class="admin-grid-view">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    /*[
                        'class' => 'yii\grid\SerialColumn',
                        'header' => 'ID',
                        'headerOptions' => ['style' => 'color:#337ab7;width:50px'],
                    ],*/
                    [
                        'attribute' => 'developer_id',
                        //'headerOptions' => ['style' => 'width:100px'],
                    ],
                    [
                            'attribute' => 'developer_name',
                            'format'    => 'html',
                            'value' => function($model){
                                $name = Html::a($model->developer_name, ['view', 'id' => $model->id]);
                                return $name;
                            }
                    ],
                    [
                        'attribute' => 'developer_grade',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'format'    => 'html',
                        'value' => function($model) {
                            return '<p style="text-align: center;margin: 0;">'.CommonHelper::developerGradeDropDownList($model->developer_grade).'</p>';
                        },
                        'filter' => Html::activeDropDownList($searchModel, 'developer_grade', CommonHelper::developerGradeDropDownList(), ['class' => 'form-control', 'prompt' => CommonHelper::GRID_FILTER_PROMPT]),
                    ],
                    [
                        'attribute' => 'Primary Contact',
                        'headerOptions' => ['style' => 'color:#337ab7'],
                        'format'    => 'html',
                        'value' => function($model) {
                            $developerContact = CommonHelper::developerContactByPrimaryContactId($model->primary_contact_id);
                            $contactName = '<a href="'.Url::toRoute('/developers/developers-contacts/view?id='.$developerContact['id'].'').'">'.$developerContact['first_name'].' '.$developerContact['last_name'].'</a>';
                            return $contactName;
                        },
                    ],
                    [
                        'attribute' => 'Last Contact',
                        'headerOptions' => ['style' => 'width:200px;color:#337ab7'],
                        'format'    => 'html',
                        'value' => function($model) {
                            $developerContact = CommonHelper::developerContactByPrimaryContactId($model->primary_contact_id);
                            return CommonHelper::changeDateFormat($developerContact['last_contact'], 'd M Y g:i a');
                        },
                    ],
                    [
                        'attribute' => 'Next Contact',
                        'headerOptions' => ['style' => 'width:200px;color:#337ab7'],
                        'format'    => 'html',
                        'value' => function($model) {
                            $developerContact = CommonHelper::developerContactByPrimaryContactId($model->primary_contact_id);
                            return CommonHelper::changeDateFormat($developerContact['next_contact'], 'd M Y g:i a');
                        },
                    ],
                    //'phone',
                    //'address',
                    //'city',
                    //'suite',
                    //'postal',
                    //'province',
                    //'website_url:url',
                    //'logo_image',
                    //'lifestyle_image',
                    //'excerpt_text:ntext',
                    //'overview_text:ntext',
                    //'description_text:ntext',
                    //'created_at',
                    //'created_by',
                    //'updated_at',
                    //'updated_by',
                    /*[
                        'attribute' => 'status',
                        'value' => function($model) {
                            return CommonHelper::developerStatusDropDownList($model->status);
                        },
                        'filter' => Html::activeDropDownList($searchModel, 'status', CommonHelper::developerStatusDropDownList(), ['class' => 'form-control', 'prompt' => CommonHelper::GRID_FILTER_PROMPT]),
                    ],*/
                    /*[
                        'class' => 'yii\grid\ActionColumn',
                        'header' => 'Actions',
                        'headerOptions' => ['style' => 'color:#337ab7;width:115px'],
                    ],*/
                ],
            ]); ?>
        </div>
</div>

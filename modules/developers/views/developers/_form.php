<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\components\FileInput;
use unlock\modules\core\buttons\FormButtons;
use yii\helpers\ArrayHelper;
use unlock\modules\generalsettings\models\City;


/* @var $this yii\web\View */
/* @var $model unlock\modules\developers\models\Developers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="developers-form">



    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
        /*'options' => ['enctype' => 'multipart/form-data'],*/
        'validateOnBlur' =>true,
        'enableAjaxValidation'=>false,
        'errorCssClass'=>'has-error',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-sm-9\">\n{input}\n{hint}\n{error}\n</div>",
            'options' => ['class' => 'form-group'],
            'horizontalCssClasses' => [
                'label' => 'col-sm-3',
                'offset' => '',
                'wrapper' => '',
                'hint' => '',
            ],
        ],
    ]); ?>
    <div class="panel panel-primary">
        <div class="panel-heading"><?= Html::encode($this->title) ?> </div>
        <div class="panel-body">
            <div class="row ">
                <div class="col-md-4">
                    <?= $form->field($model, 'developer_name')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-4 text-center">
                    <?= $form->field($model, 'developer_grade')->dropDownList(CommonHelper::developerGradeDropDownList()) ?>
                    <?php /* Html::button('Quick Add', ['class' => 'btn btn-success', 'id' => 'quick-add-developer']) */ ?>
                    <?= Html::submitButton('Quick Add', ['class' => 'btn btn-success', 'id' => 'quick-add-developer']) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'website_url')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'status')->dropDownList(CommonHelper::developerStatusDropDownList()) ?>
                </div>
            </div>
            <div class="divider"></div>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'suite')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'postal')->textInput(['maxlength' => true]) ?>

                </div>
                <div class="col-md-6">

                    <?= $form->field($model, 'province')->dropDownList(CommonHelper::provinceDropDownList(),
                        [
                            'prompt'=> 'Select a Province..',
                            'onChange' => '
                                    $.post("city?id='.'"+$(this).val(), function(data){
                                        $("select#developers-city").html(data);
                                    });
                                ',
                            'class'=>'form-control select2'
                        ]
                    ); ?>
                    <?php
                    if(!empty($model->city)){
                        $city = ArrayHelper::map(City::find()->where(['province_id' => $model->province])->all(), 'id', 'city_name');
                        ?>
                        <?= $form->field($model, 'city')->dropDownList(
                            $city
                        //[''=> 'Select a City']
                        );?>
                        <?php
                    }else{
                        ?>
                        <?= $form->field($model, 'city')->dropDownList(['' => 'Select a City']) ?>
                        <?php
                    }
                    ?>

                    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

                </div>
            </div>
            <div class="divider"></div>
            <div class="row vertical-align">
                <div class="col-md-8">
                    <?= $form->field($model, 'connect_url')->textInput() ?>
                </div>
                <div class="col-md-4 text-center">
                    <?= Html::button('Load From Connect', ['class' => 'btn btn-success', 'id' => 'load-form-connect']) ?>
                </div>
            </div>
            <div class="divider"></div>
            <div class="row">
                <div class="col-md-6 ">
                    <?= $form->field($model, 'logo_image')->fileInput(); ?>
                    <?= $form->field($model, 'logo_img_url')->hiddenInput()->label(false); ?>
                    <?php
                    if(!empty($model->logo_image)){
                        echo '<img class="logo-imgprev" style="margin-left: 75px; width: 190px;border: 3px solid #ddd;border-radius: 2px;background: #ddd;padding: 5px;margin-bottom: 20px;" src="'.Yii::$app->request->BaseUrl.'/uploads/developers/'.$model->logo_image.'" alt="'.$model->developer_name.'" />';
                    }else{
                        echo '<img class="logo-imgprev" style="margin-left: 75px; width: 190px;border: 3px solid #ddd;border-radius: 2px;background: #ddd;padding: 5px;margin-bottom: 20px;" src="'.Yii::$app->request->BaseUrl.'/uploads/not-found-img.png" alt="#" />';
                    }
                    ?>
                </div>
                <div class="col-md-6">
                    <?php /* $form->field($model, 'lifestyle_image')->widget(FileInput::classname(),['dirName' => 'developers', 'template' => '{preview}{input}', 'thumbSize' => ['120px', '120px']]); */ ?>
                    <?= $form->field($model, 'lifestyle_image')->fileInput(); ?>
                    <?= $form->field($model, 'lifestyle_img_url')->hiddenInput()->label(false); ?>
                    <?php
                    if(!empty($model->lifestyle_image)){
                        echo '<img  class="lifestyle-imgprev" style="margin-left: 75px; width: 190px;border: 3px solid #ddd;border-radius: 2px;background: #ddd;padding: 5px;margin-bottom: 20px;" src="'.Yii::$app->request->BaseUrl.'/uploads/developers/'.$model->lifestyle_image.'" alt="'.$model->developer_name.'" />';
                    }else{
                        echo '<img class="lifestyle-imgprev" style="margin-left: 75px; width: 190px;border: 3px solid #ddd;border-radius: 2px;background: #ddd;padding: 5px;margin-bottom: 20px;" src="'.Yii::$app->request->BaseUrl.'/uploads/not-found-img.png" alt="#" />';
                    }
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'excerpt_text')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'overview_text')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'description_text')->textarea(['rows' => 6]) ?>
                </div>
            </div>



            <div class="row">
                <div class="col-md-4 text-left">
                    <?= Html::button('Add/Edit Recognition', ['class' => 'btn btn-success', 'id' => 'push-to-connect']) ?>
                </div>
                <div class="col-md-4 text-center">
                    <div class="form-group">
                        <?= FormButtons::widget(); ?>
                    </div>
                </div>
                <div class="col-md-4 text-right">
                    <?= Html::button('Push To Connect', ['class' => 'btn btn-success', 'id' => 'push-to-connect']) ?>
                </div>
            </div>


        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php
$baseUrl = Yii::getAlias('@baseUrl');
$webliveUrl = Yii::getAlias('@webliveUrl');
$script = <<< JS
    jQuery(document).ready(function($) {
       $('#load-form-connect').click(function (index, value) {
           var loadurl = $('#developers-connect_url').val();
           
           var slugname = loadurl.split('/');
          
           var siteUrl = '$webliveUrl/wp-json/wp-connect/api?access_token=Y29uZG8tZGV2ZWxvcG1lbnRzVW5sb&post_type='+slugname[3]+'&slug='+slugname[4]+'';
           //var siteurl = Yii::getAlias('@basePath'); 
           $.ajax({ 
                type: 'GET', 
                url: siteUrl, 
                //data: { login: login }, 
                dataType: 'json',
                success: function (data) {
                    //var jsonss = $.parseJSON(data);
                    var obj = jQuery.parseJSON( JSON.stringify(data) );
                    console.log(obj);
                    $.each(data, function(index, element) {
                        if(element.LogoImage == null){
                            $('img.logo-imgprev').attr('src', '$baseUrl/backend/web/uploads/not-found-img.png');
                            $('#developers-logo_img_url').val('false');
                        }else {
                            $('img.logo-imgprev').attr('src', element.LogoImage);
                            $('#developers-logo_img_url').val(element.LogoImage);
                        }
                        
                        if(element.LifestyleImage == null){
                            $('img.lifestyle-imgprev').attr('src', '$baseUrl/backend/web/uploads/not-found-img.png');
                             $('#developers-lifestyle_img_url').val('false');
                        }else{
                            $('img.lifestyle-imgprev').attr('src', element.LifestyleImage);
                            $('#developers-lifestyle_img_url').val(element.LifestyleImage);
                        }
                        
                        
                        $('#developers-excerpt_text').val(element.excerpt);
                        $('#developers-overview_text').val(element.overview);
                        $('#developers-description_text').val(element.description);
                        
                        
                        
                        
                    });
                    $('#load-form-connect').text("Load From Connect");
                },
                error: function (data) {
                    console.log('Loading Error.');
                    alert('URL Data not found. please check again developers url.');
                    $('#load-form-connect').text("Load From Connect");
                },
                headers: {'Authorization': 'Basic bWFkaHNvbWUxMjM='},
                beforeSend: function (xhr) {
                    $('#load-form-connect').text("Loading....");
                },
                
           });
       });
      // var rowCount = $('.fees-type-list tbody tr').length;
       //alert(rowCount);
       
        
    });
JS;
$this->registerJs($script);
<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtons;
use unlock\modules\core\buttons\BackButton;
use yii\helpers\ArrayHelper;
use kartik\datetime\DateTimePicker;
use yii\jui\JuiAsset;
JuiAsset::register($this);
/* @var $this yii\web\View */
/* @var $model unlock\modules\developers\models\DevelopersContacts */
/* @var $form yii\bootstrap\ActiveForm */

$this->title = ($model->isNewRecord) ? Yii::t('app', 'Create Developers Contacts') : Yii::t('app', 'Update Developers Contacts');
?>

<div class="main-container main-container-form" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a(Yii::t('app', 'Home'), Yii::$app->homeUrl) ?></li>
                <li><?= Html::a(Yii::t('app', 'Developers Contacts'), Url::to('index')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-form-container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fas fa-pencil-alt"></i> <?= Html::encode($this->title) ?></h3>
                </div>
                <div class="panel-heading-button">
                    <?= BackButton::widget() ?>
                </div>
            </div>
            <div class="panel-body">
                <?php
                $form = ActiveForm::begin([
                            'layout' => 'horizontal', //  'default', 'horizontal' or 'inline'
                            /* 'options' => ['enctype' => 'multipart/form-data'], */
                            'validateOnBlur' => true,
                            'enableAjaxValidation' => true,
                            'errorCssClass' => 'has-error',
                            'fieldConfig' => [
                                'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
                                'options' => ['class' => 'form-group'],
                                'horizontalCssClasses' => [
                                    'label' => 'col-sm-2',
                                    'offset' => '',
                                    'wrapper' => '',
                                    'hint' => '',
                                ],
                            ],
                ]);
                ?>

                <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

                <?php
                if ($model->isNewRecord){
                    echo $form->field($model, 'developer_name', ['options' => ['class' => 'form-group form-group-search-auto-complete'],'inputTemplate' => '{input}<i class="fa fa-search"></i>'])->textInput(['class' => 'form-control field-developer-id field-search-auto-complete', 'placeholder' => 'Search','data-input-type' => 'select','data-hidden-field' => '#auto-complete-developer-id','data-ajax-url' => 'ajax/ajax/developer-name-list', 'maxlength' => true]);
                    echo '<input type="hidden" name="DevelopersContacts[developer_id]" id="auto-complete-developer-id">';
                }
                else{
                    $contact_id=$model->developer_id;
                    $contact_name=CommonHelper::developerIdByName($contact_id);
                    echo $form->field($model, 'developer_name', ['options' => ['class' => 'form-group form-group-search-auto-complete'],'inputTemplate' => '{input}<i class="fa fa-search"></i>'])->textInput([ 'value'=>$contact_name, 'class' => 'form-control field-developer-id field-search-auto-complete', 'placeholder' => 'Search','data-input-type' => 'select','data-hidden-field' => '#auto-complete-developer-id','data-ajax-url' => 'ajax/ajax/developer-name-list', 'maxlength' => true]);
                    echo "<input type='hidden' name='DevelopersContacts[developer_id]' id='auto-complete-developer-id' value='$contact_id'>";
                }
                ?>

                <?= $form->field($model, 'job_title')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                <?=
                $form->field($model, 'last_contact')->widget(
                        DateTimePicker::className(), [
                    'name' => 'last_contact',
                    'value' => '12-31-2010 05:10:20',
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd hh:ii:ss'
                    ]
                ]);
                ?>
                <?=
                $form->field($model, 'next_contact')->widget(
                        DateTimePicker::className(), [
                    'name' => 'next_contact',
                    'value' => '12-31-2010 05:10:20',
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd hh:ii:ss'
                    ]
                ]);
                ?>


                <div class="admin-form-button">
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <?= FormButtons::widget() ?>
                        </div>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

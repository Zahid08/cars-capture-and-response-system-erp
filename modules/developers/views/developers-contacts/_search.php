<?php

use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\buttons\ExportCsvButton;
use unlock\modules\core\buttons\ExportPdfButton;
use unlock\modules\core\buttons\ResetFilterButton;
use unlock\modules\core\buttons\SearchFilterButton;
use unlock\modules\core\helpers\CommonHelper;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model unlock\modules\developers\models\DevelopersContactsSearch */
/* @var $form yii\bootstrap\ActiveForm */
?>
<style>
    select#developerscontactssearch-developer_id{
        width: 380px;
    }
</style>
<div class="admin-search-form-container developers-contacts-search">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
        'validateOnBlur' => false,
        'enableAjaxValidation' => false,
        'errorCssClass'=>'has-error',
        'fieldConfig' => [
            ///'template' => "{label}\n{input}\n",
             'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
            'options' => ['class' => 'col-sm-4'],
            'horizontalCssClasses' => [
               // 'label' => '',
                  'label' => 'col-sm-2',
                'offset' => '',
                'wrapper' => '',
                'hint' => '',
            ],
        ],
    ]); ?>
    <div class="admin-search-form-fields">
        <div class="row">
            <div class="col-sm-12 col-md-4 col-lg-4">
                <?= $form->field($model, 'first_name')->textInput(['style'=>'width:100%'])->label('Name') ?>
            </div>       
            <div class="col-sm-12 col-md-5 col-lg-5">
                <?php // $form->field($model, 'developer_id')->dropDownList(\unlock\modules\core\helpers\CommonHelper::developersDropdownListById(), ['prompt' => 'Select Developer'])->label('Developer') ?>
                <?php 
                echo $form->field($model, 'developer_id')->widget(Select2::classname(), [
    'data' => CommonHelper::developersDropdownListById(),
    'language' => 'en',
    'options' => ['placeholder' => 'Select Developer'],
    'pluginOptions' => [
        'allowClear' => true
    ],
])->label('Developer');
                
                ?>
            </div>
            <div class="col-sm-12 col-md-3 col-lg-3">
                <div class="filter-custom-search-btn">
                    <?= SearchFilterButton::widget() ?>
                    <?php //ResetFilterButton::widget(['url' => Url::toRoute(['index'])]) ?>
                    <a class="btn btn-default" href="<?php echo Url::toRoute(['index']);?>"> Reset Filter</a>
                </div>
            </div>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>

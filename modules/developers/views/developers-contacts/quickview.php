<?php

use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\widgets\DetailView;
use unlock\modules\core\buttons\BackButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $model unlock\modules\developers\models\DevelopersContacts */

$this->title = Yii::t('app', 'View Developers Contacts');
?>
<div class="main-container main-container-view" role="main">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'first_name',
            'last_name',
            [
                'attribute' => 'developer_id',
                'value' => function($model){
                    $developer = CommonHelper::developerById($model->developer_id);
                    return $developer->developer_name;
                },
            ],
            'job_title',
            'phone',
            'email:email',
            [
                'attribute' => 'last_contact',
                'value' => CommonHelper::changeDateFormat($model->last_contact, 'd M Y g:i a')
            ],
            [
                'attribute' => 'next_contact',
                'value' => CommonHelper::changeDateFormat($model->next_contact, 'd M Y g:i a')
            ],
            'created_at:datetime',
            'created_by:user',
            'updated_at:datetime',
            'updated_by:user',
        ],
    ]) ?>
</div>

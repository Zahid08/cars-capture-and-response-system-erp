<?php

use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\widgets\DetailView;
use unlock\modules\core\buttons\BackButton;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtons;
use yii\bootstrap\Modal;
$this->title = Yii::t('app', 'View Developers Contacts');

?>

<div class="main-container main-container-view" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li><?= Html::a(Yii::t('app', 'Developers Contacts'), Url::to('index')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>
    <div class="developers-view">
        <div class="panel panel-primary">
            <div class="panel-heading">Developer Contact Details</div>
            <div class="panel-body">
                <div class="developer-heading">
                    <div class="row">
                        <div class="col-md-4 col-12 developer-name">
                            <h3><?php echo $model->first_name.' '.$model->last_name;?></h3>
                        </div>
                        <div class="col-md-4 col-12 developer-grade">
                            <h3 style="margin: 10px 0px !important;">Grade: <span class="grade-value"><?php
                                    $developer_list=CommonHelper::developerListById($model->developer_id);
                                    if (!empty($developer_list['developer_grade'])) {
                                        $grade = CommonHelper::developerGradeDropDownList($developer_list['developer_grade']);
                                    }else{
                                        $grade='';
                                    }
                                    echo $grade; ?></span></h3>
                        </div>
                        <div class="col-md-4 col-12 developer-id">
                            <h3 style="margin: 10px 0px !important;">ID: <span class="grade-value"><?php echo $model->id;?></span></h3>
                        </div>
                    </div>
                </div>
                <div class="divider"></div>
                <div class="developer-status-details">
                    <div class="row">
                        <div class="col-md-4 col-12 developer-name">
                            <p><span class="grade-value">Created at: </span><?php echo CommonHelper::changeDateFormat($model->created_at, 'd M Y') ; ?></p>
                        </div>
                        <div class="col-md-4 col-12 developer-grade">
                            <p><span class="grade-value">Last Update: </span><?php echo CommonHelper::changeDateFormat($model->updated_at, 'd M Y') ; ?></p>
                        </div>
                        <div class="col-md-4 col-12 developer-id">
                            <p><span class="grade-value">Status: </span><?php
                                $developer_list=CommonHelper::developerListById($model->developer_id);
                                $status=CommonHelper::developerStatusDropDownList($developer_list['status']);
                                echo $status; ?></p>
                        </div>
                    </div>
                </div>
                <div class="divider"></div>
                <?php
                $contactInteractions = CommonHelper::developerByDeveloperContactInteraction($model->id);
                ?>
                <?php
                Modal::begin([
                    'header' => '<h4>Add Contact Interaction</h4>',
                    'id'     => 'model-interaction',
                    'size'   => 'model-lg',
                ]);

                echo "<div id='add-contactInteraction-modelContent'></div>";

                Modal::end();
                ?>
                <?php
                Modal::begin([
                    'header' => '<h4>Update Developer Interaction</h4>',
                    'id'     => 'model-developer-update-interaction',
                    'size'   => 'model-lg',
                ]);

                echo "<div id='developer-update-contactInteraction-modelContent'></div>";

                Modal::end();
                ?>

                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Note</th>
                        <th>Type</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>User</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($contactInteractions as $contactInteraction){
                        ?>
                        <tr>
                            <td><?php echo $contactInteraction->id;?></td>
                            <td><?php echo $contactInteraction->descriptions;?></td>
                            <td><?php echo CommonHelper::interactionTypeListById($contactInteraction->interaction_type);?></td>
                            <td><?php echo CommonHelper::changeDateFormat($contactInteraction->interactions_datetitme, 'd M Y'); ?></td>
                            <td><?php echo CommonHelper::changeDateFormat($contactInteraction->interactions_datetitme, 'g:i a'); ?></td>
                            <td>
                                <?php $user = CommonHelper::getUserById($contactInteraction->user_id)?>
                                <?php
                                if(!empty($user)){
                                    echo $user->first_name.' '.$user->last_name;
                                }
                                ?>
                            </td>
                            <td>
                                <?= Html::button('<span class="glyphicon glyphicon-pencil"></span>', ['value' => \yii\helpers\Url::to(['developers-contacts-interactions/quick-update?id='.$contactInteraction->id.'']), 'class' => 'developer-update-contactInteraction-modelButton btn btn-success']) ?>
                                <?php
                                $urlInterac = yii\helpers\Url::to(['developers-contacts-interactions/delete?id='.$contactInteraction->id.'&developerid='.$model->id]);
                                ?>
                                <?= Html::a(Yii::t('yii', '<span class="glyphicon glyphicon-trash"></span>'), '#', [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'aria-label' => Yii::t('yii', 'Delete'),
                                    'onclick' => "
                                            if (confirm('Are you sure you want to delete this item?')) {
                                                $.ajax('$urlInterac', {
                                                    type: 'POST'
                                                }).done(function(data) {
                                                    $.pjax.reload({container: '#pjax-container'});
                                                });
                                            }
                                            return false;
                                        ",
                                ]); ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>

                <div class="divider"></div>
                <div class="button_group">
                    <div class="row">
                        <div class="pull-left form_action_btn_2">
                            <?= Html::button('Add Interaction', ['id' => 'add-developer-contactInteraction-modelButton', 'value' => \yii\helpers\Url::to(['developers-contacts-interactions/quick-create?devloperid='.$model->id.'']), 'class' => 'btn btn-success']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$js = <<< JS
$(function(){
     $('#add-developer-contactInteraction-modelButton').click(function(){
            $('#model-interaction').modal('show')
                .find('#add-contactInteraction-modelContent')
                .load($(this).attr('value'));
        });
     
     $('.developer-update-contactInteraction-modelButton').click(function(){
            $('#model-developer-update-interaction').modal('show')
                .find('#developer-update-contactInteraction-modelContent')
                .load($(this).attr('value'));
        });
     
     $(document).on("beforeSubmit", "#add-contact-form-interaction", function () {
        var form = $(this);
        var formData = form.serialize();
        console.log(formData);
        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: formData,
            success: function (data) {
                var obj = jQuery.parseJSON( JSON.stringify(data) );
                console.log(obj);
                $('button.btn.btn-primary').text("Save");
            },
            error: function () {
              
            },
            headers: {'Authorization': 'Basic bWFkaHNvbWUxMjM='},
            beforeSend: function (xhr) {
                $('button.btn.btn-primary').text("Sending....");
            },
        });
     });
     
});
JS;
$this->registerJs($js);
?>
<?php

use yii\helpers\Url;
use yii\helpers\Html;

use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtons;
use unlock\modules\core\buttons\BackButton;
use yii\helpers\ArrayHelper;
use kartik\datetime\DateTimePicker;


/* @var $this yii\web\View */
/* @var $model unlock\modules\developers\models\DevelopersContacts */
/* @var $form yii\bootstrap\ActiveForm */

$this->title= ($model->isNewRecord) ? Yii::t('app', 'Create Developers Contacts') : Yii::t('app', 'Update Developers Contacts');
?>

<div class="main-container main-container-form" role="main">
    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
        /*'options' => ['enctype' => 'multipart/form-data'],*/
        'id' => 'add-contact-form-3',
        'validateOnBlur' =>true,
        'enableAjaxValidation'=>false,
        'errorCssClass'=>'has-error',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
            'options' => ['class' => 'form-group'],
            'horizontalCssClasses' => [
                'label' => 'col-sm-2',
                'offset' => '',
                'wrapper' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>


    <?php
    $developersList = \unlock\modules\developers\models\Developers::find()->where(['status' => '1'])->all();
    ?>


    <?= $form->field($model, 'job_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    <?php
        $todaydatetime = date("Y-m-d H:i:s");
    ?>
    <div style="margin-bottom: 10px;" class="form-group field-last-contact required">
        <label class="control-label col-sm-2" for="developerscontacts-email">Last Contact</label>
        <div class="col-sm-10">
            <?php
            $todaydatetime = date("Y-m-d H:i:s");
            echo DateTimePicker::widget([
                'name' => 'DevelopersContacts[last_contact]',
                'value' => $todaydatetime,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd hh:ii:ss',
                ]
            ]);
            ?>
        </div>
        <p class="help-block help-block-error "></p>
    </div>

    <div style="margin-bottom: 10px;" class="form-group field-next-contact required">
        <label class="control-label col-sm-2" for="developerscontacts-email">Next Contact</label>
        <div class="col-sm-10">
            <?php
            $next7datetime = date("Y-m-d H:i:s", strtotime('+7 days'));
            echo DateTimePicker::widget([
                'name' => 'DevelopersContacts[next_contact]',
                'value' => $next7datetime,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd hh:ii:ss',
                ]
            ]);
            ?>
        </div>
        <p class="help-block help-block-error "></p>
    </div>

    <?= $form->field($model, 'developer_id')->hiddenInput(['value' => $_REQUEST['devloperid']])->label('') ?>

    <div class="admin-form-button">
        <div class="form-group">
            <div class="col-sm-10 col-sm-offset-2">
                <?= FormButtons::widget(['item' => ['save']]) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

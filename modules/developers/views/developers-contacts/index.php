<?php

use yii\helpers\Url;
use yii\helpers\Html;
use unlock\modules\core\grid\GridView;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\buttons\NewButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $searchModel unlock\modules\developers\models\DevelopersContactsSearch */
/* @var $dataProvider unlock\modules\core\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Developers Contacts');
?>
<div class="main-container" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-grid-filter">
        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <div class="admin-grid-content">
        <div class="admin-grid-toolbar">
            <div class="admin-grid-toolbar-left">
                <?= NewButton::widget(['url' => Url::toRoute(['create']),'title'=>'New']) ?>
            </div>
            <div class="admin-grid-toolbar-right">
                <?= Summary::widget(['dataProvider' => $dataProvider]) ?>
            </div>
        </div>
        <div class="admin-grid-view">
            <?php
                echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
                'pager' => ['class' => LinkPager::className()],
                'columns' => [
                    //['class' => 'unlock\modules\core\grid\SerialColumn'],
                        [
                            'attribute' => 'id',
                            'format' => 'html',
                            'headerOptions' => ['style' => 'color:#337ab7;'],
                            'value' => function($model){
                                $id = Html::a($model->id, ['view', 'id' => $model->id]);
                                return $id;
                            }
                        ],
                        [
                            'attribute' => 'Name',
                            'format' => 'html',
                            'headerOptions' => ['style' => 'color:#337ab7;'],
                            'value' => function($model){
                                //$name = '<a href="'.Url::to(['view'], '').'">'.$model->first_name.' '.$model->last_name.'</a>';
                                $name = Html::a($model->first_name." ".$model->last_name, ['view', 'id' => $model->id]);
                                return $name;
                            }
                        ],
                        [
                            'attribute' => 'Company',
                            'value' => function($model){
                                $developer = CommonHelper::developerById($model->developer_id);
                                if(!empty($developer)){
                                    return $developer->developer_name;
                                }
                            },
                        ],
                        [
                            'attribute' => 'Title',
                            'value' => 'job_title',
                        ],
                        //'job_title',
                         'phone',
                        [

                            'attribute' => 'last_contact',
                            'headerOptions' => ['style' => 'width:190px'],
                            'value' => function($model) {
                                return CommonHelper::changeDateFormat($model->last_contact, 'd M Y g:i a');
                            },
                        ],
                       /* [
                            'attribute' => 'next_contact',
                            'headerOptions' => ['style' => 'width:190px'],
                            'value' => function($model) {
                                return CommonHelper::changeDateFormat($model->next_contact, 'd M Y g:i a');
                            },
                        ],*/

                        // 'created_at',
                        // 'created_by',
                        // 'updated_at',
                        // 'updated_by',

                   /* [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => 'Actions',
                        'headerOptions' => ['style' => 'color:#337ab7;width:115px'],
                    ],*/
                ],
            ]);   ?>

        </div>
    </div>

</div>

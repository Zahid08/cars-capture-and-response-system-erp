<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\jui\DatePicker;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtons;
use unlock\modules\core\buttons\BackButton;

/* @var $this yii\web\View */
/* @var $model unlock\modules\developers\models\DevelopersRecognitions */
/* @var $form yii\bootstrap\ActiveForm */

$this->title= ($model->isNewRecord) ? Yii::t('app', 'Create Recognitions') : Yii::t('app', 'Update Recognitions');
?>

<div class="main-container main-container-form" role="main">
    <div class="page-header">
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a(Yii::t('app', 'Home'), Yii::$app->homeUrl) ?></li>
                <li><?= Html::a(Yii::t('app', 'Developers Recognitions'), Url::to('index')) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-form-container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fa fa-pencil"></i> <?= Html::encode($this->title) ?></h3>
                </div>
                <div class="panel-heading-button">
                    <?= BackButton::widget() ?>
                </div>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
                    /*'options' => ['enctype' => 'multipart/form-data'],*/
                    'validateOnBlur' =>true,
                    'enableAjaxValidation'=>true,
                    'errorCssClass'=>'has-error',
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
                        'options' => ['class' => 'form-group'],
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-2',
                            'offset' => '',
                            'wrapper' => '',
                            'hint' => '',
                        ],
                    ],
                ]); ?>

                <?= $form->field($model, 'developer_id')->textInput() ?>

				<?= $form->field($model, 'recognition_name')->textInput(['maxlength' => true]) ?>

				<?= $form->field($model, 'type')->textInput() ?>

				<?= $form->field($model, 'year')->textInput(['maxlength' => true]) ?>

				<?= $form->field($model, 'created_at')->textInput() ?>

				<?= $form->field($model, 'created_by')->textInput() ?>

				<?= $form->field($model, 'updated_at')->textInput() ?>

				<?= $form->field($model, 'updated_by')->textInput() ?>

                <div class="admin-form-button">
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <?= FormButtons::widget() ?>
                        </div>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

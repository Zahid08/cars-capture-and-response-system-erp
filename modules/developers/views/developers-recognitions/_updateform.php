<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\FormButtons;
use unlock\modules\core\buttons\BackButton;

/* @var $this yii\web\View */
/* @var $model unlock\modules\developers\models\DevelopersRecognitions */
/* @var $form yii\bootstrap\ActiveForm */

$this->title= ($model->isNewRecord) ? Yii::t('app', 'Create Recognitions') : Yii::t('app', 'Update Recognitions');
?>

<div class="main-container main-container-form" role="main">
    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
        /*'options' => ['enctype' => 'multipart/form-data'],*/
        'id' => 'add-contact-form',
        'validateOnBlur' =>true,
        'enableAjaxValidation'=>false,
        'errorCssClass'=>'has-error',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
            'options' => ['class' => 'form-group'],
            'horizontalCssClasses' => [
                'label' => 'col-sm-2',
                'offset' => '',
                'wrapper' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'recognition_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList(CommonHelper::developersRecognitionTypeDropDownList(), ['prompt'=>'Select recognition type'])?>

    <?= $form->field($model, 'year')->textInput(['maxlength' => true]) ?>


    <div class="admin-form-button">
        <div class="form-group">
            <div class="col-sm-10 col-sm-offset-2">
                <?= FormButtons::widget(['item' => ['save']]) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

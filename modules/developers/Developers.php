<?php

namespace unlock\modules\developers;

/**
 * developers module definition class
 */
class Developers extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'unlock\modules\developers\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}

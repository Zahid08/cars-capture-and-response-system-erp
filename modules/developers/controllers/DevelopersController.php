<?php

namespace unlock\modules\developers\controllers;

use unlock\modules\developers\models\DevelopersContacts;
use unlock\modules\developers\models\DevelopersContactsInteractions;
use unlock\modules\developers\models\DevelopersRecognitions;
use Yii;
use unlock\modules\developers\models\Developers;
use unlock\modules\developers\models\DevelopersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use unlock\modules\core\helpers\CommonHelper;
use yii\web\UploadedFile;
use unlock\modules\generalsettings\models\City;

/**
 * DevelopersController implements the CRUD actions for Developers model.
 */
class DevelopersController extends Controller
{
    /**
     * {@inheritdoc}
     */


    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => Yii::$app->DynamicAccessRules->customAccess('developers/developers'),
        ];
    }

    /**
     * Lists all Developers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DevelopersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Developers model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $developercontacts = new DevelopersContacts();

        return $this->render('view', [
            'model' => $this->findModel($id),
            'developercontactsmodel' => $developercontacts,
        ]);
    }

    /**
     * Creates a new Developers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Developers();

        if ($model->load(Yii::$app->request->post())) {
            $developerName = $model->developer_name;

            // Logo Images
            if(!empty($model->logo_img_url)){
                $allowed_extension = array("jpg", "png", "jpeg", "gif");
                $url_array = explode("/", $model->logo_img_url);
                $image_name = end($url_array);
                $image_array = explode(".", $image_name);
                $extension = end($image_array);
                if(in_array($extension, $allowed_extension))
                {
                    $image_data = file_get_contents($model->logo_img_url);
                    $new_image_path = Yii::getAlias('@backend').'/web/uploads/developers/'.$developerName.date("Ymdhis").'.'. $extension;
                    file_put_contents($new_image_path, $image_data);

                    $model->logo_image = $developerName.date("Ymdhis").'.'. $extension;
                }
            }else{
                if ($model->logo_image = UploadedFile::getInstance($model, 'logo_image')) {
                    $model->logo_image->saveAs(Yii::getAlias('@backend').'/web/uploads/developers/'.$developerName.date("Ymdhis").'.'.$model->logo_image->extension);
                    $model->logo_image = $developerName.date("Ymdhis").'.'.$model->logo_image->extension;
                }
            }

            // Life style Images
            if(!empty($model->lifestyle_img_url)){
                $allowed_extension = array("jpg", "png", "jpeg", "gif");
                $url_array = explode("/", $model->lifestyle_img_url);
                $image_name = end($url_array);
                $image_array = explode(".", $image_name);
                $extension = end($image_array);

                if(in_array($extension, $allowed_extension))
                {
                    $image_data = file_get_contents($model->lifestyle_img_url);
                    $new_image_path = Yii::getAlias('@backend').'/web/uploads/developers/'.$developerName.date("Ymdhis").'.'. $extension;
                    file_put_contents($new_image_path, $image_data);

                    $model->lifestyle_image = $developerName.date("Ymdhis").'.'. $extension;
                }

            }else{
                if ($model->lifestyle_image = UploadedFile::getInstance($model, 'lifestyle_image')) {
                    $model->lifestyle_image->saveAs(Yii::getAlias('@backend').'/web/uploads/developers/'.$developerName.date("Ymdhis").'.'.$model->lifestyle_image->extension);
                    $model->lifestyle_image = $developerName.date("Ymdhis").'.'.$model->lifestyle_image->extension;
                }
            }



            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Developers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $logoImage = $model->logo_image;
        $lifestyleImage = $model->lifestyle_image;
        if ($model->load(Yii::$app->request->post())) {

            $developerName = $model->developer_name;

            // Logo Images
            if($model->logo_img_url != 'false'){
                if(!empty($model->logo_img_url)){
                    $allowed_extension = array("jpg", "png", "jpeg", "gif");
                    $url_array = explode("/", $model->logo_img_url);
                    $image_name = end($url_array);
                    $image_array = explode(".", $image_name);
                    $extension = end($image_array);
                    if(in_array($extension, $allowed_extension))
                    {
                        $image_data = file_get_contents($model->logo_img_url);
                        $new_image_path = Yii::getAlias('@backend').'/web/uploads/developers/'.$developerName.date("Ymdhis").'.'. $extension;
                        file_put_contents($new_image_path, $image_data);
                        $model->logo_image = $developerName.date("Ymdhis").'.'. $extension;
                    }
                }elseif ($model->logo_image = UploadedFile::getInstance($model, 'logo_image')){
                    $model->logo_image->saveAs(Yii::getAlias('@backend').'/web/uploads/developers/'.$developerName.date("Ymdhis").'.'.$model->logo_image->extension);
                    $model->logo_image = $developerName.date("Ymdhis").'.'.$model->logo_image->extension;
                }else{
                    $model->logo_image = $logoImage;
                }
            }else{
                $model->logo_image = '';
            }



            // Life style Images
            if($model->lifestyle_img_url != 'false'){
                if(!empty($model->lifestyle_img_url)){
                    $allowed_extension = array("jpg", "png", "jpeg", "gif");
                    $url_array = explode("/", $model->lifestyle_img_url);
                    $image_name = end($url_array);
                    $image_array = explode(".", $image_name);
                    $extension = end($image_array);

                    if(in_array($extension, $allowed_extension))
                    {
                        $image_data = file_get_contents($model->lifestyle_img_url);
                        $new_image_path = Yii::getAlias('@backend').'/web/uploads/developers/'.$developerName.date("Ymdhis").'.'. $extension;
                        file_put_contents($new_image_path, $image_data);

                        $model->lifestyle_image = $developerName.date("Ymdhis").'.'. $extension;
                    }

                }else{
                    if ($model->lifestyle_image = UploadedFile::getInstance($model, 'lifestyle_image')) {
                        $model->lifestyle_image->saveAs(Yii::getAlias('@backend').'/web/uploads/developers/'.$developerName.date("Ymdhis").'.'.$model->lifestyle_image->extension);
                        $model->lifestyle_image = $developerName.date("Ymdhis").'.'.$model->lifestyle_image->extension;
                    }else{
                        $model->lifestyle_image = $lifestyleImage;
                    }
                }
            }else{
                $model->logo_image = '';
            }



            $model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Developers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {

        $model = $this->findModel($id);

        try {
            DevelopersRecognitions::deleteAll(['developer_id' => $id]);
            DevelopersContacts::deleteAll(['developer_id' => $id]);
            DevelopersContactsInteractions::deleteAll(['developer_id' => $id]);
            if (!$model->delete()) {
                throw new Exception(Yii::t('app', 'The record cannot be deleted.'));
            }

            Yii::$app->session->setFlash('success', Yii::t('app', 'The record have been successfully deleted.'));
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            if($e->getName() == CommonHelper::INTEGRITY_CONSTRAINT_VIOLATION){
                $message = "This record has relational data. You must delete the following data first";
                $message .= "<ul>";
                $message .= "<li>Example => Example List</li>";
                $message .= "</ul>";
            }
            Yii::$app->session->setFlash('error', $message);
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Developers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Developers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Developers::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }



    /*** Pull all developer via rest API  ***/
    public function actionPullDevelopers(){

        return $this->redirect(['/site/developers']);
    }

    /*** Pull all developer via rest API  ***/
    public function actionPushDevelopers(){

        return $this->redirect(['/site/developers']);
    }


    // City List
    public function actionCity($id){

        $sectionCount = City::find()->where(['province_id' => $id])->count();
        $sectionAll = City::find()->where(['province_id' => $id])->all();

        if($sectionCount > 0){
            echo '<option>Select a City...</option>';
            foreach ($sectionAll as $value){
                echo '<option value="'.$value->id.'">'.$value->city_name.'</option>';
            }
        }else{
            echo '<option>Select a City...</option>';
        }

    }

    public function actionPrimaryContact(){
        $devId = $_REQUEST['devId'];
        $primaryContactId = $_REQUEST['primary-contact-id'];
        if(!empty($devId) && $primaryContactId){
            $model = Developers::find()->where(['id' => $devId])->one();
            $model->primary_contact_id = $primaryContactId;
            if($model->save(false)){
                echo 'Successfully Data Added';
            }else{
                echo 'Data Insert Fail';
            }
        }
    }


    public function actionLoadDevelopers(){


        $webliveUrl = Yii::getAlias('@webliveUrl');
        $developerurl = $webliveUrl.'/wp-json/wp-connect/api?access_token=Y29uZG8tZGV2ZWxvcG1lbnRzVW5sb&post_type=developers&time='.time();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $developerurl);
        $result = curl_exec($ch);
        curl_close($ch);

        $developerData = json_decode($result);


        if(!empty($developerData)){
            foreach ($developerData as $value){
                $checkId = CommonHelper::developerIdByList($value->developerId);
                if(!empty($checkId)){
                    $model = Developers::findOne($checkId->id);
                    $model->developer_name = str_replace("&amp;","&",$value->title);



                    /*$model->logo_image  = $value->LogoImage;
                    $model->lifestyle_image  = $value->LifestyleImage;*/
                    // Logo Images
                    if(!empty($value->LogoImage)){
                        $allowed_extension = array("jpg", "png", "jpeg", "gif");
                        $url_array = explode("/", $value->LogoImage);
                        $image_name = end($url_array);
                        $image_array = explode(".", $image_name);
                        $extension = end($image_array);
                        if(in_array($extension, $allowed_extension))
                        {
                            if(file_exists($value->LogoImage)){
                                $image_data = file_get_contents($value->LogoImage);
                                $new_image_path = Yii::getAlias('@backend').'/web/uploads/developers/'.$model->developer_name .date("Ymdhis").'.'. $extension;
                                file_put_contents($new_image_path, $image_data);
                                $model->logo_image = $model->developer_name .date("Ymdhis").'.'. $extension;
                            }

                        }
                    }else{
                        if ($model->logo_image = UploadedFile::getInstance($model, 'logo_image')) {
                            $model->logo_image->saveAs(Yii::getAlias('@backend').'/web/uploads/developers/'.$model->developer_name .date("Ymdhis").'.'.$model->logo_image->extension);
                            $model->logo_image = $model->developer_name .date("Ymdhis").'.'.$model->logo_image->extension;
                        }
                    }

                    // Life style Images
                    if(!empty($value->LifestyleImage)){
                        $allowed_extension = array("jpg", "png", "jpeg", "gif");
                        $url_array = explode("/", $value->LifestyleImage);
                        $image_name = end($url_array);
                        $image_array = explode(".", $image_name);
                        $extension = end($image_array);

                        if(in_array($extension, $allowed_extension))
                        {
                            $image_data = file_get_contents($value->LifestyleImage);
                            $new_image_path = Yii::getAlias('@backend').'/web/uploads/developers/'.$model->developer_name .date("Ymdhis").'.'. $extension;
                            file_put_contents($new_image_path, $image_data);

                            $model->lifestyle_image = $model->developer_name .date("Ymdhis").'.'. $extension;
                        }

                    }else{
                        if ($model->lifestyle_image = UploadedFile::getInstance($model, 'lifestyle_image')) {
                            $model->lifestyle_image->saveAs(Yii::getAlias('@backend').'/web/uploads/developers/'.$model->developer_name .date("Ymdhis").'.'.$model->lifestyle_image->extension);
                            $model->lifestyle_image = $model->developer_name .date("Ymdhis").'.'.$model->lifestyle_image->extension;
                        }
                    }


                    $model->excerpt_text  = $value->excerpt;
                    $model->overview_text  = $value->overview;
                    $model->description_text  = $value->description;
                    $model->connect_url  = $value->pageLink;
                    if($model->save(false)){
                        //echo '<h2 style="color: #1c7430">Successfully Update Developer Data. '.$value->developerId.'</h2>';
                        Yii::$app->session->setFlash('success', Yii::t('app', 'Successfully Update Developer Data.'));

                    }else{
                        //echo '<h2 style="color: red">Developer Data Import Error!!!!.</h2>';
                        Yii::$app->session->setFlash('error', Yii::t('app', 'Developer Data Import Error!!!!.'));
                    }

                }else{
                    $model = new Developers();
                    $model->developer_id = $value->developerId;
                    $model->developer_name = str_replace("&amp;","&",$value->title);


                    // Logo Images
                    if(!empty($value->LogoImage)){
                        $allowed_extension = array("jpg", "png", "jpeg", "gif");
                        $url_array = explode("/", $value->LogoImage);
                        $image_name = end($url_array);
                        $image_array = explode(".", $image_name);
                        $extension = end($image_array);
                        if(in_array($extension, $allowed_extension))
                        {
                            $image_data = file_get_contents($value->LogoImage);
                            $new_image_path = Yii::getAlias('@backend').'/web/uploads/developers/'.$model->developer_name .date("Ymdhis").'.'. $extension;
                            file_put_contents($new_image_path, $image_data);

                            $model->logo_image = $model->developer_name .date("Ymdhis").'.'. $extension;
                        }
                    }else{
                        if ($model->logo_image = UploadedFile::getInstance($model, 'logo_image')) {
                            $model->logo_image->saveAs(Yii::getAlias('@backend').'/web/uploads/developers/'.$model->developer_name .date("Ymdhis").'.'.$model->logo_image->extension);
                            $model->logo_image = $model->developer_name .date("Ymdhis").'.'.$model->logo_image->extension;
                        }
                    }

                    // Life style Images
                    if(!empty($value->LifestyleImage)){
                        $allowed_extension = array("jpg", "png", "jpeg", "gif");
                        $url_array = explode("/", $value->LifestyleImage);
                        $image_name = end($url_array);
                        $image_array = explode(".", $image_name);
                        $extension = end($image_array);

                        if(in_array($extension, $allowed_extension))
                        {
                            $image_data = file_get_contents($value->LifestyleImage);
                            $new_image_path = Yii::getAlias('@backend').'/web/uploads/developers/'.$model->developer_name .date("Ymdhis").'.'. $extension;
                            file_put_contents($new_image_path, $image_data);

                            $model->lifestyle_image = $model->developer_name .date("Ymdhis").'.'. $extension;
                        }

                    }else{
                        if ($model->lifestyle_image = UploadedFile::getInstance($model, 'lifestyle_image')) {
                            $model->lifestyle_image->saveAs(Yii::getAlias('@backend').'/web/uploads/developers/'.$model->developer_name .date("Ymdhis").'.'.$model->lifestyle_image->extension);
                            $model->lifestyle_image = $model->developer_name .date("Ymdhis").'.'.$model->lifestyle_image->extension;
                        }
                    }


                    $model->excerpt_text  = $value->excerpt;
                    $model->overview_text  = $value->overview;
                    $model->description_text  = $value->description;
                    $model->connect_url  = $value->pageLink;
                    if($model->save(false)){
                        //echo '<h2 style="color: #1c7430">Successfully Import All Developer Data. '.$value->developerId.'</h2>';
                        Yii::$app->session->setFlash('success', Yii::t('app', 'Successfully Import All Developer Data.'));

                    }else{
                        //echo '<h2 style="color: red">Developer Data Import Error!!!!.</h2>';
                        Yii::$app->session->setFlash('error', Yii::t('app', 'Developer Data Import Error!!!!.'));
                    }
                }
            }

        }else{
            echo '<h2 style="color: red">Please check development JSON URL.</h2>';
        }
        return $this->redirect(['index']);
        exit();
    }

}

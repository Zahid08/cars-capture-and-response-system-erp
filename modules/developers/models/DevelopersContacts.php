<?php

namespace unlock\modules\developers\models;

use Yii;
use yii\helpers\ArrayHelper;
use unlock\modules\core\helpers\CommonHelper;

/**
 * This is the model class for table "{{%developers_contacts}}".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property integer $developer_id
 * @property string $job_title
 * @property string $phone
 * @property string $email
 * @property string $last_contact
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property integer $status
 *
 * @property Developers $developer
 */
class DevelopersContacts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $developer_name;
    public static function tableName()
    {
        return '{{%developers_contacts}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['developer_id', 'created_by', 'updated_by'], 'integer'],
            [['last_contact', 'next_contact', 'created_at', 'updated_at'], 'safe'],
            [['first_name', 'last_name', 'job_title', 'phone', 'email'], 'string', 'max' => 100],
         //   [['first_name', 'email'], 'required'],
            [['first_name'], 'required'],
            [['developer_name'], 'required'],
            [['developer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Developers::className(), 'targetAttribute' => ['developer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'DCID'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'next_contact' => Yii::t('app', 'Next Contact'),
            'developer_id' => Yii::t('app', ''),
            'developer_name' => Yii::t('app', 'Developer Name'),
            'job_title' => Yii::t('app', 'Job Title'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'last_contact' => Yii::t('app', 'Last Contact'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeveloper()
    {
        return $this->hasOne(Developers::className(), ['id' => 'developer_id']);
    }

    /** Get All Active DevelopersContacts Drop Down List*/
    public static function developersContactsDropDownList()
    {
        $model = self::find()
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }
}

<?php

namespace unlock\modules\developers\models;

use Yii;
use yii\helpers\ArrayHelper;
use unlock\modules\core\helpers\CommonHelper;

/**
 * This is the model class for table "{{%developers_recognitions}}".
 *
 * @property integer $id
 * @property integer $developer_id
 * @property string $recognition_name
 * @property integer $type
 * @property string $year
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property integer $status
 *
 * @property Developers $developer
 */
class DevelopersRecognitions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%developers_recognitions}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['developer_id', 'type', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['type', 'recognition_name', 'year'], 'required'],
            [['recognition_name'], 'string', 'max' => 255],
            [['year'], 'string', 'max' => 100],
            [['developer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Developers::className(), 'targetAttribute' => ['developer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'developer_id' => Yii::t('app', 'Developer ID'),
            'recognition_name' => Yii::t('app', 'Recognition Name'),
            'type' => Yii::t('app', 'Type'),
            'year' => Yii::t('app', 'Year'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeveloper()
    {
        return $this->hasOne(Developers::className(), ['id' => 'developer_id']);
    }

    /** Get All Active DevelopersRecognitions Drop Down List*/
    public static function developersRecognitionsDropDownList()
    {
        $model = self::find()
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }
}

<?php

namespace unlock\modules\developers\models;

use Yii;
use yii\base\Model;
use unlock\modules\core\data\ActiveDataProvider;
use unlock\modules\developers\models\DevelopersRecognitions;
use unlock\modules\core\helpers\CommonHelper;

/**
 * DevelopersRecognitionsSearch represents the model behind the search form about `unlock\modules\developers\models\DevelopersRecognitions`.
 */
class DevelopersRecognitionsSearch extends DevelopersRecognitions
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'developer_id', 'type', 'created_by', 'updated_by'], 'integer'],
            [['recognition_name', 'year', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DevelopersRecognitions::find();

        // add conditions that should always apply here
        $pageSize = isset($params['per-page']) ? intval($params['per-page']) : CommonHelper::GRID_PER_PAGE;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' =>  ['pageSize' => $pageSize],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'developer_id' => $this->developer_id,
            'type' => $this->type,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'recognition_name', $this->recognition_name])
            ->andFilterWhere(['like', 'year', $this->year]);

        return $dataProvider;
    }
}

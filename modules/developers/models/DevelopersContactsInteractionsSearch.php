<?php

namespace unlock\modules\developers\models;

use Yii;
use yii\base\Model;
use unlock\modules\core\data\ActiveDataProvider;
use unlock\modules\developers\models\DevelopersContactsInteractions;
use unlock\modules\core\helpers\CommonHelper;

/**
 * DevelopersContactsInteractionsSearch represents the model behind the search form about `unlock\modules\developers\models\DevelopersContactsInteractions`.
 */
class DevelopersContactsInteractionsSearch extends DevelopersContactsInteractions
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'developer_contact_id', 'interaction_type', 'user_id'], 'integer'],
            [['descriptions', 'interactions_datetitme'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DevelopersContactsInteractions::find();

        // add conditions that should always apply here
        $pageSize = isset($params['per-page']) ? intval($params['per-page']) : CommonHelper::GRID_PER_PAGE;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' =>  ['pageSize' => $pageSize],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'developer_contact_id' => $this->developer_contact_id,
            'interaction_type' => $this->interaction_type,
            'interactions_datetitme' => $this->interactions_datetitme,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'descriptions', $this->descriptions]);

        return $dataProvider;
    }
}

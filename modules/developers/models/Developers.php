<?php

namespace unlock\modules\developers\models;

use Yii;
use unlock\modules\usermanager\user\models\User;
use unlock\modules\core\helpers\CommonHelper;

/**
 * This is the model class for table "conn_developers".
 *
 * @property int $id
 * @property string $developer_name
 * @property int $developer_grade 1 = A+, 2 = A, 3 = B+,4 = B,5 = C, 6 = D
 * @property string $phone
 * @property string $address
 * @property string $city
 * @property string $suite
 * @property string $postal
 * @property int $province 1 = Ontario, 2 = Quebec, 3 = Manitoba
 * @property string $website_url
 * @property string $logo_image
 * @property string $lifestyle_image
 * @property string $excerpt_text
 * @property string $overview_text
 * @property string $description_text
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property int $status 1 = Active, 0 = In-Active, 2 = Archive, 3 = Delete
 *
 * @property User $createdBy
 * @property User $updatedBy
 * @property DevelopersContacts[] $developersContacts
 */
class Developers extends \yii\db\ActiveRecord
{
    //public $connect_url;
    public $logo_img_url;
    public $lifestyle_img_url;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%developers}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['primary_contact_id', 'developer_id', 'developer_grade', 'province', 'created_by', 'updated_by', 'status'], 'integer'],
            [['excerpt_text', 'overview_text', 'description_text'], 'string'],
            [['developer_name', 'developer_grade', 'status'], 'required'],

            [['connect_url','created_at', 'updated_at', 'logo_img_url', 'lifestyle_img_url'], 'safe'],
            [['logo_image', 'lifestyle_image'], 'file'],
            [['developer_name', 'phone', 'website_url', 'logo_image', 'lifestyle_image'], 'string', 'max' => 100],
            [['address'], 'string', 'max' => 255],
            [['city', 'suite'], 'string', 'max' => 50],
            [['postal'], 'string', 'max' => 30],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'connect_url' => 'Connect URL',
            'developer_id' => 'Developer ID',
            'developer_name' => 'Developer Name',
            'developer_grade' => 'Grade',
            'phone' => 'Phone',
            'address' => 'Address',
            'city' => 'City',
            'suite' => 'Suite',
            'postal' => 'Postal',
            'province' => 'Province',
            'website_url' => 'Website Url',
            'logo_image' => 'Logo Image',
            'lifestyle_image' => 'Lifestyle Image',
            'excerpt_text' => 'Excerpt Text',
            'overview_text' => 'Overview Text',
            'description_text' => 'Description Text',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevelopersContacts()
    {
        return $this->hasMany(DevelopersContacts::className(), ['developer_id' => 'id']);
    }

    /**
     * before save data
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }
}

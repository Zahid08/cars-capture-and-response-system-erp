<?php

namespace unlock\modules\developers\models;

use Yii;
use yii\helpers\ArrayHelper;
use unlock\modules\core\helpers\CommonHelper;

/**
 * This is the model class for table "{{%developers_contacts_interactions}}".
 *
 * @property integer $id
 * @property integer $developer_contact_id
 * @property integer $interaction_type
 * @property string $descriptions
 * @property string $interactions_datetitme
 * @property integer $user_id
 * @property integer $status
 */
class DevelopersContactsInteractions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%developers_contacts_interactions}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['developer_id', 'developer_contact_id', 'interaction_type', 'user_id'], 'integer'],
            [['developer_contact_id', 'interaction_type', 'descriptions', 'interactions_datetitme'], 'required'],
            [['descriptions'], 'string'],
            //[['interactions_datetitme'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'developer_contact_id' => Yii::t('app', 'Contact Name'),
            'interaction_type' => Yii::t('app', 'Type'),
            'descriptions' => Yii::t('app', 'Notes'),
            'interactions_datetitme' => Yii::t('app', 'Date'),
            'user_id' => Yii::t('app', 'User Name'),
        ];
    }

    /** Get All Active DevelopersContactsInteractions Drop Down List*/
    public static function developersContactsInteractionsDropDownList()
    {
        $model = self::find()
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->user_id = CommonHelper::getLoggedInUserId();
                //$this->created_at = date('Y-m-d H:i:s');
            }
           /* else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }*/
            return true;
        }
        else return false;
    }
}

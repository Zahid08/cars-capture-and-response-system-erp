<?php

namespace unlock\modules\developers\models;

use Yii;
use yii\base\Model;
use unlock\modules\core\data\ActiveDataProvider;
use unlock\modules\developers\models\DevelopersContacts;
use unlock\modules\core\helpers\CommonHelper;

/**
 * DevelopersContactsSearch represents the model behind the search form about `unlock\modules\developers\models\DevelopersContacts`.
 */
class DevelopersContactsSearch extends DevelopersContacts
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'developer_id', 'created_by', 'updated_by'], 'integer'],
            [['first_name', 'last_name', 'next_contact', 'job_title', 'phone', 'email', 'last_contact', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DevelopersContacts::find();

        // add conditions that should always apply here
        $pageSize = isset($params['per-page']) ? intval($params['per-page']) : CommonHelper::GRID_PER_PAGE;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created_at' => SORT_DESC]],
            'pagination' =>  ['pageSize' => $pageSize],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'developer_id' => $this->developer_id,
            'last_contact' => $this->last_contact,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        /*$query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])*/
           $query->andFilterWhere(['like', 'next_contact', $this->next_contact])
            ->andFilterWhere(['like', 'job_title', $this->job_title])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email]);
        
        $query->andFilterWhere(['like',"concat(first_name, ' ', last_name)", $this->first_name]);
        /// echo $query->createCommand()->rawSql;
        return $dataProvider;
    }
}

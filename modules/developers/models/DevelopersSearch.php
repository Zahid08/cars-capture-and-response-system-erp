<?php

namespace unlock\modules\developers\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use unlock\modules\developers\models\Developers;
use unlock\modules\core\helpers\CommonHelper;

/**
 * DevelopersSearch represents the model behind the search form of `unlock\modules\developers\models\Developers`.
 */
class DevelopersSearch extends Developers
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'primary_contact_id', 'developer_grade', 'province', 'created_by', 'updated_by', 'status'], 'integer'],
            [['developer_name', 'phone', 'address', 'city', 'suite', 'postal', 'website_url', 'logo_image', 'lifestyle_image', 'excerpt_text', 'overview_text', 'description_text', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Developers::find();

        // add conditions that should always apply here
        $pageSize = isset($params['per-page']) ? intval($params['per-page']) : CommonHelper::GRID_PER_PAGE;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created_at' => SORT_DESC]],
            'pagination' =>  ['pageSize' => $pageSize],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'developer_grade' => $this->developer_grade,
            'province' => $this->province,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'developer_name', $this->developer_name])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'suite', $this->suite])
            ->andFilterWhere(['like', 'postal', $this->postal])
            ->andFilterWhere(['like', 'website_url', $this->website_url])
            ->andFilterWhere(['like', 'logo_image', $this->logo_image])
            ->andFilterWhere(['like', 'lifestyle_image', $this->lifestyle_image])
            ->andFilterWhere(['like', 'excerpt_text', $this->excerpt_text])
            ->andFilterWhere(['like', 'overview_text', $this->overview_text])
            ->andFilterWhere(['like', 'description_text', $this->description_text]);

        return $dataProvider;
    }
}

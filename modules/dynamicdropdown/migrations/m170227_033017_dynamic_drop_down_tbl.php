<?php

use yii\db\Migration;

/**
 * Class m170227_033017_dynamic_drop_down_tbl
 */
class m170227_033017_dynamic_drop_down_tbl extends Migration
{
    /**
     * This method contains the logic to be executed when applying this migration.
     */
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%dynamic_drop_down}}', [
            'id'          => $this->primaryKey(),
            'parent_id'   => $this->integer()->defaultValue(0)->comment('Parent Item'),
            'key'         => $this->string(255)->notNull(),
            'value'       => $this->string(255)->notNull(),
            'slug'        => $this->string(255)->notNull(),
            'created_by'  => $this->integer(),
            'created_at'  => $this->dateTime(),
            'updated_by'  => $this->integer(),
            'updated_at'  => $this->dateTime(),
            'sort_order'  => $this->integer()->comment('Sort Order'),
            'status'      => $this->smallInteger(2)->defaultValue(1)->comment('1=>Active, 2=>Inactive'),
        ], $tableOptions);

        // Foreign Keys
        $this->addForeignKey('fk_dynamicDropDown_user_createdBy', '{{%dynamic_drop_down}}', 'created_by', '{{%user}}', 'id');
        $this->addForeignKey('fk_dynamicDropDown_user_updatedBy', '{{%dynamic_drop_down}}', 'updated_by', '{{%user}}', 'id');

    }

    /**
     * This method contains the logic to be executed when removing this migration.
     */
    public function down()
    {
        // Foreign Keys
        $this->dropForeignKey('fk_dynamicDropDown_user_createdBy', '{{%dynamic_drop_down}}');
        $this->dropForeignKey('fk_dynamicDropDown_user_updatedBy', '{{%dynamic_drop_down}}');

        $this->dropTable('{{%dynamic_drop_down}}');
    }

}
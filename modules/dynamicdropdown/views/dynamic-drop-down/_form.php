<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\jui\DatePicker;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use unlock\modules\core\components\FileInput;
use unlock\modules\core\helpers\CommonHelper;
use unlock\modules\core\buttons\BackButton;
use unlock\modules\core\buttons\FormButtons;

/* @var $this yii\web\View */
/* @var $model unlock\modules\dynamicdropdown\models\DynamicDropDown */
/* @var $form yii\bootstrap\ActiveForm */

$this->title = $title;
$this->title= ($model->isNewRecord) ? Yii::t('app', 'Create ' . $title) : Yii::t('app', 'Update ' . $title);
$panelHeading= ($model->isNewRecord) ? Yii::t('app', 'Create ' . str_replace('Manage ', '', $title)) : Yii::t('app', 'Update ' . $model->value);
?>

<div class="main-container main-container-form" role="main">
    <div class="page-header">
        <div class="page-header-title">
            <h1 class="page-title"><?= Html::encode($title) ?></h1>
        </div>
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a(Yii::t('app', 'Home'), Yii::$app->homeUrl) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-form-container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-heading-title">
                    <h3 class="panel-title"><i class="fa fa-pencil"></i> <?= Html::encode($panelHeading) ?></h3>
                </div>
                <div class="panel-heading-button">
                    <?= BackButton::widget() ?>
                </div>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
                    /*'options' => ['enctype' => 'multipart/form-data'],*/
                    'validateOnBlur' =>true,
                    'enableAjaxValidation'=>true,
                    'errorCssClass'=>'has-error',
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
                        'options' => ['class' => 'form-group'],
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-2',
                            'offset' => '',
                            'wrapper' => '',
                            'hint' => '',
                        ],
                    ],
                ]); ?>


                <?php if($parent_id) { ?>
                    <?= $form->field($model, 'parent_id')->hiddenInput(['value'=> $parent_id])->label(false) ?>
                <?php } else { ?>
                    <?= $form->field($model, 'parent_id')->hiddenInput(['value'=> 0])->label(false) ?>
                <?php } ?>

                <?php if($model->isNewRecord) { ?>
                <?= $form->field($model, 'key')->textInput(['maxlength' => true]) ?>
                <?php } else { ?>
                <?= $form->field($model, 'key', [
                        'inputTemplate' => '<label>' . $model->key . '</label>',
                    ]); ?>
                <?php } ?>

                <?= $form->field($model, 'value')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'sort_order')->textInput() ?>

                <?= $form->field($model, 'status')->dropDownList(CommonHelper::statusDropDownList()) ?>

                <div class="admin-form-button">
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <?= FormButtons::widget(['item' => ['save']]) ?>
                        </div>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use unlock\modules\core\widgets\Summary;
use unlock\modules\core\widgets\LinkPager;
use unlock\modules\core\buttons\NewButton;
use unlock\modules\core\buttons\ExportCsvButton;
use unlock\modules\core\buttons\ExportPdfButton;
use unlock\modules\core\buttons\ResetFilterButton;
use unlock\modules\core\helpers\CommonHelper;

/* @var $this yii\web\View */
/* @var $searchModel unlock\modules\dynamicdropdown\models\DynamicDropDown */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $title;
?>
<div class="main-container" role="main">
    <div class="page-header">
        <div class="page-header-title">
            <h1 class="page-title"><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="page-header-breadcrumb">
            <ul class="breadcrumb">
                <li><?= Html::a('Home', Yii::$app->homeUrl, ['class' => '']) ?></li>
                <li class="active"><span><?= Html::encode($this->title) ?></span></li>
            </ul>
        </div>
    </div>

    <div class="admin-grid-filter">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <div class="admin-grid-content">
        <div class="admin-grid-toolbar">
            <div class="admin-grid-toolbar-left">
                <?= Summary::widget(['dataProvider' => $dataProvider]) ?>
            </div>
            <div class="admin-grid-toolbar-right">
                <?= NewButton::widget(['route' => 'dynamic-drop-down/dynamic-drop-down/index', 'url' => Url::toRoute([$createUrl])]) ?>
            </div>
        </div>

        <div class="admin-grid-view">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => "select[name='".$dataProvider->getPagination()->pageSizeParam."'],input[name='".$dataProvider->getPagination()->pageParam."']",
                'pager' => ['class' => LinkPager::className()],
                'columns' => [
                    [
                        'class' => 'unlock\modules\core\grid\SerialColumn',
                        'headerOptions' => ['class' => 'serial-column column-auto-fit'],
                    ],
                    [
                        'attribute' => 'key',
                        'label' => $labelFieldValue,
                    ],
                    [
                        'attribute' => 'value',
                        'label' => $labelFieldText,
                    ],
                    [
                        'attribute' => 'id',
                        'format' => 'html',
                        'label' => 'Manage',
                        'visible' => $showManageButton,
                        'value' => function ($model) {
                            $url =  Url::toRoute(['/dynamic-drop-down/dynamic-drop-down/'. $model->slug .'/index']);
                            return Html::a('<span class="fa fa-tasks"></span> Manage', $url, [
                                'title' => Yii::t('app', 'Manage'),
                            ]);
                        }
                    ],
                    [
                        'attribute' => 'status',
                        'format' => 'status',
                        'headerOptions' => ['class' => 'status-column column-auto-fit'],
                        'filter' => Html::activeDropDownList($searchModel, 'status', CommonHelper::statusDropDownList(), ['prompt' => CommonHelper::GRID_FILTER_PROMPT]),
                    ],
                    [
                        'class' => 'unlock\modules\core\grid\ActionColumn',
                        'template' => '{update} {delete}',
                        'header' => 'Action',
                        'buttons' => [
                            'update' => function ($url, $model, $index) {
                                $updateUrl = $model->getUrl('update', $model->id);
                                return Html::a('<span class="fa fa-pencil" style="margin-right: 10px"></span>', $updateUrl, [
                                    'title' => Yii::t('app', 'Edit'),
                                ]);
                            },
                            'delete' => function ($url, $model, $deleteUrl) {
                                $deleteUrl = $model->getUrl('delete', $model->id);
                                return Html::a('<span class="fa fa-trash"></span> ', $deleteUrl, [
                                    'title' => Yii::t('app', 'Delete'),
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]);
                            }
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>

</div>

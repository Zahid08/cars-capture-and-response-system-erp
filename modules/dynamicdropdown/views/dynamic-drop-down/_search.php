<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model unlock\modules\dynamicdropdown\models\DynamicDropDown */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="admin-search-form-container dynamic-drop-down-search">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
        'validateOnBlur' => false,
        'enableAjaxValidation' => false,
        'errorCssClass'=>'has-error',
        'fieldConfig' => [
            'template' => "{label}\n{input}\n",
            'options' => ['class' => 'col-sm-4'],
            'horizontalCssClasses' => [
                'label' => '',
                'offset' => '',
                'wrapper' => '',
                'hint' => '',
            ],
        ],
    ]); ?>


    <div class="admin-search-form-fields">

        <div class="row">
            <?= $form->field($model, 'parent_id') ?>

            <?= $form->field($model, 'title') ?>

            <?= $form->field($model, 'slug') ?>
        </div>

        <div class="row">
            <div class="col-sm-6 filter-custom-search-btn">
                <?= SearchFilterButton::widget() ?>
                <?= ResetFilterButton::widget(['url' => Url::toRoute(['index'])]) ?>
            </div>
            <div class="col-sm-6 filter-custom-export-btn">
                <?= ExportCsvButton::widget(['url' => Url::toRoute(['export-csv'])]) ?>
                <?= ExportPdfButton::widget(['url' => Url::toRoute(['export-pdf'])]) ?>
            </div>
        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>

Dynamic Drop Down
=======================================================================================================================

Dynamic Drop Down for every Site

Migration
-----------------------------------------------------------------------------------------------------------------------

**Create Database**

php yii migrate/up --migrationPath=@unlock/modules/dynamicdropdown/migrations

**Delete Database**

php yii migrate/down --migrationPath=@unlock/modules/dynamicdropdown/migrations

Configuration
-----------------------------------------------------------------------------------------------------------------------

**Module Setup**

common/config/main.php

'modules' => [
    'dynamic-drop-down' =>  [
        'class' => 'unlock\modules\dynamicdropdown\DynamicDropDownModule',
    ],
],


Left Menu
-----------------------------------------------------------------------------------------------------------------------
**Left Admin Menu**
// Dynamic Drop Down
[
    'label' => '<i class="fa fa-cog"></i> <span>Settings</span>',
    'url' => ['#'],
    'visible'=> Yii::$app->user->checkAccess('setting/backendsetting', 'general') ||
                Yii::$app->user->checkAccess('dynamicdropdown/dynamicdropdown', 'index'),
    'template' => '<a class="dropdown-toggle" href="{url}" >{label}</a>',
    'items' => [
        [
            'label' => '<span>Configuration</span>',
            'url' => ['/setting/backend-setting/general'],
            'visible'=> Yii::$app->user->checkAccess('setting/backendsetting', 'general'),
            'active' => LeftMenu::isActive(['setting/backend-setting/general']),
        ],
        [
            'label' => '<span>Manage Drop Down</span>',
            'url' => ['/dynamic-drop-down/dynamic-drop-down/index'],
            'visible'=> Yii::$app->user->checkAccess('dynamicdropdown/dynamicdropdown', 'index'),
            'active' => LeftMenu::isActive(['dynamic-drop-down/dynamic-drop-down/']),
        ],
    ],
],

Usage
-----------------------------------------------------------------------------------------------------------------------


URLs
-----------------------------------------------------------------------------------------------------------------------
Backend:
/dynamic-drop-down/dynamic-drop-down/index

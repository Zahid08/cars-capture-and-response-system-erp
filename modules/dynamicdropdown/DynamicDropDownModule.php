<?php

namespace unlock\modules\dynamicdropdown;

/**
 * Class DynamicDropDownModule
 */
class DynamicDropDownModule extends \yii\base\Module
{
    /**
     * @var string the namespace that controller classes are in
     */
    public $controllerNamespace = 'unlock\modules\dynamicdropdown\controllers';

    /**
     * @var string the namespace that view file are in
     */
    public $viewNamespace = '@unlock/modules/dynamicdropdown/views/';
}

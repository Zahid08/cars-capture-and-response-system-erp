<?php

namespace unlock\modules\dynamicdropdown\models;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\behaviors\SluggableBehavior;
use unlock\modules\core\helpers\CommonHelper;

/**
 * This is the model class for table "{{%dynamic_drop_down}}".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $key
 * @property string $value
 * @property string $slug
 * @property integer $created_by
 * @property string $created_at
 * @property integer $updated_by
 * @property string $updated_at
 * @property integer $sort_order
 * @property integer $status
 */
class DynamicDropDown extends \yii\db\ActiveRecord
{

    // Slug Behavior for url
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'value',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%dynamic_drop_down}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key', 'value'], 'required'],
            [['parent_id', 'created_by', 'updated_by', 'sort_order', 'status'], 'integer'],
            [['value'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['slug', 'key', 'value'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'key' => Yii::t('app', 'Drop Down Key'),
            'value' => Yii::t('app', 'Drop Down Value'),
            'slug' => Yii::t('app', 'Slug'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'sort_order' => Yii::t('app', 'Sort Order'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
                $this->parent_id = !empty($this->parent_id) ? $this->parent_id : 0;
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }

    public function beforeDelete(){
        self::deleteAll(['parent_id' => $this->id]);

        return parent::beforeDelete();
    }

    public static function parentDropDownList(){
        $list = self::find()
                    ->select('id, value')
                    ->where('status=1')
                    ->all();

        return ArrayHelper::map($list, 'id', 'value');
    }

    public static function getIdBySlug($slug){
        $data = self::find()
            ->select('id')
            ->where(['slug' => $slug])
            ->one();

        return isset($data->id) ? $data->id : null;
    }

    public static function getIdByKey($key){
        $data = self::find()
            ->select('id')
            ->where(['key' => $key])
            ->one();

        return isset($data->id) ? $data->id : null;
    }

    public static function dropDownListByParentSlug($slug){
        $parentId = self::getIdBySlug('user-type-registered');

        $list = self::find()
            ->select('key, value')
            ->where('status=1')
            ->andWhere(['=', 'parent_id', $parentId])
            ->all();

        return ArrayHelper::map($list, 'key', 'value');
    }

    public function getUrl($action, $id)
    {
        $info = self::findOne($id);
        $parentInfo = self::findOne($info->parent_id);

        if($action == 'update' && !empty($parentInfo->slug)){
            return Url::toRoute(['/dynamic-drop-down/dynamic-drop-down/'. $parentInfo->slug .'/update', 'id' => $id]);
        }
        else if($action == 'update' && empty($parentInfo->slug)){
            return Url::toRoute(['/dynamic-drop-down/dynamic-drop-down/update', 'id' => $id]);
        }
        else if($action == 'delete' && !empty($parentInfo->slug)){
            return Url::toRoute(['/dynamic-drop-down/dynamic-drop-down/'. $parentInfo->slug .'/delete', 'id' => $id]);
        }
        else if($action == 'delete' && empty($parentInfo->slug)){
            return Url::toRoute(['/dynamic-drop-down/dynamic-drop-down/delete', 'id' => $id]);
        }

    }

    public static function getDynamicDropDownInfoByKey($key, $slug){
        if(empty($key) || empty($slug)){
            return null;
        }

        $parentId = self::getIdBySlug($slug);
        return self::find()
            ->where(['key' => $key])
            ->andWhere(['parent_id' => $parentId])
            ->one();
    }
}

<?php

namespace unlock\modules\dynamicdropdown\controllers;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\base\Exception;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use unlock\modules\core\helpers\Collection;
use unlock\modules\core\helpers\FileHelper;
use unlock\modules\core\actions\ExportCsvAction;
use unlock\modules\core\actions\ExportPdfAction;
use unlock\modules\core\actions\MassDeleteAction;
use unlock\modules\core\actions\MassActiveAction;
use unlock\modules\core\actions\MassInactiveAction;
use unlock\modules\core\actions\ToggleUpdateAction;
use unlock\modules\dynamicdropdown\models\DynamicDropDown;
use unlock\modules\dynamicdropdown\models\DynamicDropDownSearch;

/**
 * DynamicDropDownController implements the CRUD actions for DynamicDropDown model.
 */
class DynamicDropDownController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => Yii::$app->DynamicAccessRules->customAccess('dynamicdropdown/dynamicdropdown'),
        ];
    }

    /**
     * Lists all DynamicDropDown models.
     * @return mixed
     */
    public function actionIndex($slug = null)
    {
        $searchModel = new DynamicDropDownSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $title = 'Manage Drop Down';
        $labelFieldValue = 'Drop Down Code';
        $labelFieldText = 'Drop Down List';
        $createUrl = 'create';
        $updateUrl = 'update';
        if(!empty($slug)) {
            $info = DynamicDropDown::find()->select('id, value')->where(['slug' => $slug])->one();
            $dataProvider->query->andFilterWhere(['parent_id' => $info->id]);
            $title = $info->value;
            $labelFieldValue = 'Drop Down Key';
            $labelFieldText = 'Drop Down Value';

            $createUrl = '/dynamic-drop-down/dynamic-drop-down/'. $slug .'/create';
            $updateUrl = '/dynamic-drop-down/dynamic-drop-down/'. $slug .'/update';
            $showManageButton = false;
        }
        else {
            $dataProvider->query->andFilterWhere(['parent_id' => 0]);
            $showManageButton = true;
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => $title,
            'createUrl' => $createUrl,
            'updateUrl' => $updateUrl,
            'showManageButton' => $showManageButton,
            'labelFieldValue' => $labelFieldValue,
            'labelFieldText' => $labelFieldText,
        ]);
    }

    /**
     * Creates a new DynamicDropDown model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($slug = null)
    {
        $model = new DynamicDropDown();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            if(empty($slug)){
                $model->key = 'dynamic-drop-down-' . $model->key;
            }
            $this->saveData($model, $slug);
        }

        $parent_id = '';
        $title = 'Manage Drop Down';
        if(!empty($slug)){
            $info = DynamicDropDown::find()->where(['slug' => $slug])->one();
            $title = $info->value;
            $parent_id = $info->id;
        }
        return $this->render('_form', [
            'model' => $model,
            'title' => $title,
            'slug' => $slug,
            'parent_id' => $parent_id,
        ]);
    }

    /**
     * Updates an existing DynamicDropDown model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $slug = null)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $this->saveData($model, $slug);
        }

        $title = 'Manage Drop Down';
        if(!empty($slug)) {
            $info = DynamicDropDown::find()->where(['slug' => $slug])->one();
            $title = $info->value;
        }

        return $this->render('_form', [
            'model' => $model,
            'title' => $title,
            'slug' => $slug,
            'parent_id' => $model->parent_id,
        ]);
    }

    /**
     * Deletes an existing DynamicDropDown model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $slug = null)
    {
        $model = $this->findModel($id);

        try {
            if (!$model->delete()) {
                throw new Exception(Yii::t('app', 'The record cannot be deleted.'));
            }

            // Delete Image:
            // FileHelper::removeFile($model->image, 'sanction');
            Yii::$app->session->setFlash('success', Yii::t('app', 'The record have been successfully deleted.'));
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            if($e->getName() == CommonHelper::INTEGRITY_CONSTRAINT_VIOLATION){
                $message = "This record has relational data. You must delete the following data first";
                $message .= "<ul>";
                $message .= "<li>Example => Example List</li>";
                $message .= "</ul>";
            }
            Yii::$app->session->setFlash('error', $message);
        }

        if($slug){
            return $this->redirect(['/dynamic-drop-down/dynamic-drop-down/'. $slug .'/index']);
        }
        else{
            return $this->redirect(['/dynamic-drop-down/dynamic-drop-down/index']);
        }

    }

    /**
    * Save Data.
    * @param object $model
    * @return mixed
    */
    private function saveData($model, $slug = null){
        if (isset($model)) {
            //$transaction = Yii::$app->db->beginTransaction();
            try {
                $post = Yii::$app->request->post();

                if (!$model->save()) {
                    throw new Exception(Yii::t('app', Html::errorSummary($model)));
                }

                //$transaction->commit();
                Yii::$app->session->setFlash('success', Yii::t('app', 'Record successfully saved.'));
                if($slug){
                    return $this->redirect(['/dynamic-drop-down/dynamic-drop-down/'. $slug .'/index']);
                }
                else {
                    return $this->redirect(['/dynamic-drop-down/dynamic-drop-down/index']);
                }

            }
            catch (Exception $e) {
                //$transaction->rollBack();
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
    }

    /**
     * Finds the DynamicDropDown model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DynamicDropDown the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DynamicDropDown::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    //ajax-dynamic-drop-down
    public function actionAjaxDynamicDropDown()
    {
        $model = new DynamicDropDown();
        $group = $post = Yii::$app->request->post('group');
        $data = $model->dropDownListByParentSlug($group);

        $html = '';
        if(count($data) > 0 ){
            foreach($data as $key => $value){
                $html .= "<option value='". $key ."'>".$value."</option>";
            }
        }
        else{
            $html .= "<option>-</option>";
        }

        return $html;
    }
}

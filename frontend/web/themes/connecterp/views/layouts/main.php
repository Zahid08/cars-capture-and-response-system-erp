<?php

/* @var $this \yii\web\View */
/* @var $content string */

use frontend\assets\ConnecterpAsset;

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

ConnecterpAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>CAPTURE AND RESPONSE SYSTEM</title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="content-wrapper">
    <div class="container">
        <?= $content ?>
    </div>
</div>
<div class="footer_area">
    <div class="container">
        <div class="row text-center">
            <div class="col-12">
                <p> &copy; 2018 CONNECT asset management. All Rights Reserved. Website designed by <a class="footer_link" href="http://www.unlocklive.com">UnlockLive IT Limited</a>.</p>
            </div>
        </div>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

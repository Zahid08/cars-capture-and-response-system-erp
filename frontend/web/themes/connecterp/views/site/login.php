<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
?>
<section id="login_wrapper">
        <div class="container">
            <div class="login_content text-center">
                <div class="site_logo">
                    <a href="#" class="form_logo"><img class="img-fluid" src="<?= Yii::getAlias('@web/themes/connecterp') ?>/images/logo.png"></a>
                </div>
                <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                <div class="form_inputs">
                    <div class="form-group input-group input-group-sm cus_input" id="login_username">
                        <div class="input-group-prepend">
                            <span class="input-group-text input_icon" id="basic-addon1"><i class="fa fa-user"></i></span>
                        </div>
                        <?= $form->field($model, 'username')->textInput(['autofocus' => false, 'class' => 'cus_input_box', 'placeholder' => 'Username'])->label(false) ?>
                        <span class="login_style"></span>
                        <div class="input-group-append">
                                <span class="input-group-text input_icon" id="basic-addon2" v-show="message=='' && !init"><i class="fa fa-times"></i></span>
                                
                                <span class="input-group-text input_icon" id="basic-addon2" v-show="!check && message!=''"><i class="fa fa-check"></i></span>
                                <span class="input-group-text input_icon" id="basic-addon2" v-show="init"></span>
                        </div>
                        </div>
                </div>
                <div class="form_inputs">
                    <div class="form-group input-group input-group-sm cus_input" id="login_password">
                        <div class="input-group-prepend">
                            <span class="input-group-text input_icon" id="basic-addon1"><i class="fa fa-key"></i></span>
                        </div>
                        <?= $form->field($model, 'password')->passwordInput(['autofocus' => false, 'class' => 'cus_input_box', 'placeholder' => 'Password'])->label(false) ?>
                        <span class="login_style"></span>
                        <div class="input-group-append">
                                <span class="input-group-text input_icon" id="basic-addon2" v-show="message1=='' && !init1"><i class="fa fa-times"></i></span>
                                
                                <span class="input-group-text input_icon" id="basic-addon2" v-show="!check1 && message1!=''"><i class="fa fa-check"></i></span>
                                <span class="input-group-text input_icon" id="basic-addon2" v-show="init1"></span>

                                
                        </div>
                    </div>
                </div>

                

                <?= $form->field($model, 'rememberMe')->checkbox() ?>

                <div class="form_inputs">
                        <div class="form-group input-group input-group-sm mt-4 mb-0">

                            <?= Html::submitButton('Login', ['class' => 'btn btn-primary cus_input_btn', 'name' => 'login-button']) ?>
                        </div>

                </div>
                

                <?php ActiveForm::end(); ?>
                
                <a href="#" class="form_links">Forgot Password</a>
            </div>
            <div class="footer_wrapper text-center">
                <p> &copy; 2018 CONNECT asset management. All Rights Reserved. Software Development By <a class="footer_link" href="http://www.unlocklive.com">UnlockLive IT Limited</a>.</p>
            </div>
        </div>
    </section>


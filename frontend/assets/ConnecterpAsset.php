<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class ConnecterpAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'themes/connecterp/css/bootstrap.min.css',
        'themes/connecterp/css/fontawesome.min.css',
        'themes/connecterp/css/owl.carousel.css',
        'themes/connecterp/css/style.css',
        
    ];
    public $js = [
        'themes/connecterp/js/fontawesome.min.js',
        'themes/connecterp/js/bootstrap.min.js',
        'themes/connecterp/js/owl.carousel.min.js',
        'themes/connecterp/js/owl.carousel.min.js',
        'themes/connecterp/js/popper.min.js',
        'themes/connecterp/js/vue.js',
        'themes/connecterp/js/vue-script.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}

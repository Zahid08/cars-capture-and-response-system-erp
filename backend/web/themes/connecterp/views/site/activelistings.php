<?php

/* @var $this yii\web\View */
use yii\helpers\Url;

$this->title = 'Active Listings';
?>
<div class="site-index">
        <div class="dashboard_items">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo Url::to(['developers/developers/index']);?>" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-user-cog"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Developers</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo Url::to(['developments/developments/index']);?>" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-building"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Developments</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>


                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo Url::to(['allocation/allocation/index']);?>" class="dashboard_links change_color">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-chart-pie"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Allocations</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo Url::to(['assignments/assignments/index']);?>" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-tasks"></i>
                                </div>
                                <div class="item_text">
                                    <h5>ASSIGNMENTS</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo Url::to(['developers/developers-contacts/index']);?>" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-file-contract"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Developer Contacts</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links change_color">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="far fa-clipboard"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Storyboard</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo Url::to(['inventory/inventory/index']);?>" class="dashboard_links change_color">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-warehouse"></i>
                                </div>
                                <div class="item_text">
                                    <h5>INVENTORY</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links change_color">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fab fa-elementor"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Future Dashboard Item</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>

        </div>
</div>

<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'Developers';
?>
<div class="site-index">
        <div class="dashboard_items">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo Url::to(['developers/developers/create']);?>" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-user-plus"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Create Developer </h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo Url::to(['developers/developers/index']);?>" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-list-ul"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Developers List</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo Url::to(['developers/developers/push-developers']);?>" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-cloud-upload-alt"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Push All Developers</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo Url::to(['developers/developers/pull-developers']);?>" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-cloud-download-alt"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Pull All Developers</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

        </div>
</div>

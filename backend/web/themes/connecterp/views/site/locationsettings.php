<?php

/* @var $this yii\web\View */

$this->title = 'Location Settings';

?>
<div class="site-index">
        <div class="dashboard_items">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-city"></i>
                                </div>
                                <div class="item_text">
                                    <h5>City</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fab fa-fort-awesome"></i>
                                </div>
                                <div class="item_text">
                                    <h5>District</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-school"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Neighborhood</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fab fa-elementor"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Future Dashboard Item</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fab fa-elementor"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Future Dashboard Item</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>


                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fab fa-elementor"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Future Dashboard Item</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fab fa-elementor"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Future Dashboard Item</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fab fa-elementor"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Future Dashboard Item</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>

        </div>
</div>
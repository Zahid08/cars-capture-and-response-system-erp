<?php
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'Users';

?>
<div class="site-index">

        <div class="dashboard_items">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo Url::to(['user/admin/create']);?>" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-user-plus"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Add new user</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo Url::to(['user/admin/index']);?>" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-list-ul"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Users List</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo Url::to(['role/role/index']);?>" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-user-shield"></i>
                                </div>
                                <div class="item_text">
                                    <h5>User Role</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo Url::to(['permission/access-control/assign-permission']);?>" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-user-lock"></i>
                                </div>
                                <div class="item_text">
                                    <h5>User Permission</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo Url::to(['permission/access-control/reload-permission']);?>" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-sync-alt"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Reload Permission</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>


                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo Url::to(['permission/access-control/reload-permission-session']);?>" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-retweet"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Reload Session</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fab fa-elementor"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Future Dashboard Item</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fab fa-elementor"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Future Dashboard Item</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>

            </div>
</div>
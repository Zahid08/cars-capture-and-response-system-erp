<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'Heroes';
?>
<div class="site-index">
    <div class="dashboard_items">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="text-center for_items">
                    <a href="" class="dashboard_links change_color">
                        <div class="single_item">
                            <div class="item_logo">
                                <i class="fa fa-project-diagram"></i>
                            </div>
                            <div class="item_text">
                                <h5>Project Manager</h5>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="text-center for_items">
                    <a href="#" class="dashboard_links change_color">
                        <div class="single_item">
                            <div class="item_logo">
                                <i class="fa fa-business-time"></i>
                            </div>
                            <div class="item_text">
                                <h5>Timesheets</h5>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="text-center for_items">
                    <a href="#" class="dashboard_links change_color">
                        <div class="single_item">
                            <div class="item_logo">
                                <i class="fas fa-sort-amount-down"></i>
                            </div>
                            <div class="item_text">
                                <h5>Future Dashboard Item</h5>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="text-center for_items">
                    <a href="#" class="dashboard_links change_color">
                        <div class="single_item">
                            <div class="item_logo">
                                <i class="fas fa-product-hunt"></i>
                            </div>
                            <div class="item_text">
                                <h5>Future Dashboard Item</h5>

                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="text-center for_items">
                    <a href="#" class="dashboard_links change_color">
                        <div class="single_item">
                            <div class="item_logo">
                                <i class="fas fa-search-minus"></i>
                            </div>
                            <div class="item_text">
                                <h5>Future Dashboard Item</h5>

                            </div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="text-center for_items">
                    <a href="#" class="dashboard_links change_color">
                        <div class="single_item">
                            <div class="item_logo">
                                <i class="fab fa-elementor "></i>
                            </div>
                            <div class="item_text">
                                <h5>Future Dashboard Item</h5>

                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="text-center for_items">
                    <a href="#" class="dashboard_links change_color">
                        <div class="single_item">
                            <div class="item_logo">
                                <i class="fab fa-elementor"></i>
                            </div>
                            <div class="item_text">
                                <h5>Future Dashboard Item</h5>

                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="text-center for_items">
                    <a href="#" class="dashboard_links change_color">
                        <div class="single_item">
                            <div class="item_logo">
                                <i class="fab fa-elementor"></i>
                            </div>
                            <div class="item_text">
                                <h5>Future Dashboard Item</h5>

                            </div>
                        </div>
                    </a>
                </div>
            </div>

        </div>

    </div>
</div>
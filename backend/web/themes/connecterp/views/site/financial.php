<?php

/* @var $this yii\web\View */

$this->title = 'Financial';
?>
<div class="site-index">
        <div class="dashboard_items">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links change_color">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-file-invoice-dollar"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Budget</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links change_color">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-briefcase"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Resources</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links change_color">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-sort-amount-up"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Scaling Up</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links change_color">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-project-diagram"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Projects</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links change_color">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-brain"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Strategy Meetings</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>


                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links change_color">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fab fa-elementor "></i>
                                </div>
                                <div class="item_text">
                                    <h5>Future Dashboard Item</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links change_color">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fab fa-elementor"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Future Dashboard Item</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links change_color">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fab fa-elementor"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Future Dashboard Item</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>

        </div>
</div>
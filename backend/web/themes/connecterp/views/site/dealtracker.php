<?php

/* @var $this yii\web\View */

$this->title = 'Deal Tracker';
?>
<div class="site-index">
        <div class="dashboard_items">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links change_color">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-search-location"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Pre Con Tracker</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links change_color">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-hand-holding-usd"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Disbursements</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links change_color">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Schedule</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links change_color">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-redo"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Resale Tracker</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links change_color">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fab fa-elementor"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Future Dashboard Item</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>


                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links change_color">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fab fa-elementor"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Future Dashboard Item</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links change_color">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fab fa-elementor"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Future Dashboard Item</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links change_color">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fab fa-elementor"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Future Dashboard Item</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>

        </div>
</div>
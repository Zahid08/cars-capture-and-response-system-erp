<?php

/* @var $this yii\web\View */

$this->title = 'Active Listings Setting';

?>
<div class="site-index">
        <div class="dashboard_items">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-user-edit"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Architects</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-user-shield"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Listing Brokers</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-ad"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Ad Agencies</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-pencil-ruler"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Designers</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-sort-alpha-up"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Grading</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>


                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fab fa-elementor"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Future Dashboard Item</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fab fa-elementor"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Future Dashboard Item</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fab fa-elementor"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Future Dashboard Item</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>

        </div>
</div>
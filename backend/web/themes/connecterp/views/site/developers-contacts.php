<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'Developers Contacts';
?>
<div class="site-index">
        <div class="dashboard_items">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo Url::to(['developers/developers-contacts/create']);?>" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-user-plus"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Create Developer Contact</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo Url::to(['developers/developers-contacts/index']);?>" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-list-ul"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Developers Contacts List</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>

        </div>
</div>

<?php
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'Marketing';
?>
<div class="site-index">
        <div class="dashboard_items">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links change_color">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-money-bill-alt change_color"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Media Sources &amp; Contacts</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links change_color">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-bullhorn"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Campaigns &amp; Deployment</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links change_color">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-chart-line"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Media Performance</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links change_color">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-wine-glass-alt"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Event Management</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo Url::to(['newsletters/newsletters/index']);?>" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-envelope-square"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Newsletter</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>


                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links change_color">
                            <div class="single_item">
                                <div class="item_logo">
                                    <svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.57 53.57">
                                        <defs>
                                            <style>.cls-1{fill:#4ac6e9;}</style>
                                        </defs>
                                        <title>adwords</title>
                                        <path class="cls-1" d="M37.23,1.41a1.68,1.68,0,0,0-1.36-.7H19.13a1.69,1.69,0,0,0-1.59,1.15L.8,52.08A1.69,1.69,0,0,0,1,53.59a1.66,1.66,0,0,0,1.36.7h10.7a10,10,0,0,0,9.53-6.87L37.46,2.92A1.69,1.69,0,0,0,37.23,1.41Z" transform="translate(-0.71 -0.71)"/>
                                        <path class="cls-1" d="M54.2,52.08,39.4,7.68,29.27,38.09l3.11,9.33a10,10,0,0,0,9.52,6.87H52.61a1.68,1.68,0,0,0,1.59-2.21Z" transform="translate(-0.71 -0.71)"/>
                                    </svg>
                                </div>
                                <div class="item_text">
                                    <h5>Adwords tool</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links change_color">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fab fa-facebook"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Facebook tool</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links change_color">
                            <div class="single_item">
                                <div class="item_logo">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 55 55" enable-background="new 0 0 55 55" xml:space="preserve">
                                        <g id="Clip-32"></g>
                                        <g>
                                            <path fill="#4AC6E9" d="M9.6,22.5c-0.5,0-0.7,0-1,0c-0.3,0-0.6,0.1-0.6-0.4c0-0.5,0.2-0.5,0.6-0.6c1.6-0.2,3.1-0.4,4.6-0.6
                                                c0.2,0,0.6-0.3,0.7,0.3c0,0.4,0.1,0.7-0.5,0.8c-1.2,0.2-2.1,0.9-2.8,1.8c-0.7,0.9-1.5,1.9-2.3,2.8c-0.2,0.2-0.2,0.3,0,0.5
                                                c1.5,2.2,3.1,4.3,4.7,6.4c0.5,0.6,2.4,0.7,3,0.3c0.2-0.1,0.2-0.3,0.2-0.5c0.2-2.7,0.1-5.4,0.1-8.2c0-0.6-0.3-0.8-0.8-0.8
                                                c-0.2,0-0.5,0-0.7,0c-0.2,0-0.6,0.2-0.6-0.3c0-0.4,0-0.7,0.6-0.7c2.1-0.1,4.1-0.4,6.2-0.9c0.2-0.1,0.5-0.2,0.5,0.2
                                                c0,0.3,0.3,0.6-0.2,0.8c-0.5,0.1-1.1,0.3-1.6,0.4c-0.3,0.1-0.5,0.3-0.5,0.7c-0.1,2.8-0.1,5.5,0,8.3c0,1.1,0.1,1.1,1.2,1.2
                                                c0.6,0,0.6,0.3,0.5,0.7c0,0.3-0.2,0.4-0.5,0.4c-3.1-0.2-6.1-0.1-9.1,0.4c-0.4,0.1-0.5-0.2-0.6-0.4c-1.3-2-2.7-4.1-4-6.1
                                                c-0.2-0.3-0.4-0.6-0.8-0.7C5.3,28.1,5,28,5,28.6c0,1.4,0,2.9,0,4.3c0,0.9,0.1,0.9,1,1.1c0.3,0,0.8-0.2,0.8,0.4
                                                c0,0.5-0.1,0.7-0.7,0.7c-1.8-0.1-3.5-0.1-5.3,0c-0.3,0-0.5,0.1-0.5-0.3c0-0.3-0.2-0.7,0.4-0.7c1.5-0.1,1.4-0.1,1.5-1.6
                                                c0.1-3.4,0-6.9,0.1-10.3c0-1.4,0-2.9,0-4.3c0-1.2,0-1.2-1.1-1.2c-0.6,0-0.6-0.2-0.6-0.7c0-0.3,0.1-0.4,0.4-0.4
                                                c1.2,0,2.4-0.1,3.7-0.3c0.4-0.1,0.5,0.1,0.5,0.5C5,19.2,5,22.9,5,26.5c0,0.2-0.1,0.6,0.1,0.7c0.2,0.1,0.5-0.1,0.7-0.3
                                                c1-0.7,1.8-1.6,2.5-2.6C8.7,23.7,9.1,23.2,9.6,22.5z"/>
                                            <path fill="#4AC6E9" d="M39.7,29.5c0-2.2,0-4.3,0-6.5c0-0.5-0.2-0.8-0.7-0.8c-0.6-0.1-1.2-0.2-1.7-0.2c-0.5,0-0.4-0.3-0.4-0.6
                                                c0-0.4,0.2-0.5,0.5-0.4c2.3,0.3,4.5,0.4,6.8,0.5c0.2,0,0.4,0.1,0.3,0.3c-0.1,0.3,0.1,0.7-0.4,0.7c-0.3,0-0.7,0-1,0
                                                c-0.5,0-0.7,0.2-0.7,0.7c0,1.8,0,3.7,0,5.5c0,2.3,0.1,4.6-0.3,6.9c-0.4,2.5-1.7,4.2-4.1,5.1c-0.3,0.1-0.7,0.2-1.1,0.2
                                                c-0.5,0-0.9-0.3-1-0.7c-0.1-0.4,0-0.9,0.4-1.2c0.4-0.3,0.9-0.4,1.3,0c0.5,0.5,0.8,0.2,1.1-0.2c0.7-0.8,0.8-1.8,0.9-2.7
                                                C39.8,33.9,39.7,31.7,39.7,29.5z"/>
                                            <path fill="#4AC6E9" d="M24.3,27.8c0-1.9,0-3.9,0-5.8c0-0.6-0.2-0.9-0.8-0.8c-0.5,0.1-1.1,0-1.6,0.1c-0.6,0.1-0.5-0.3-0.6-0.7
                                                c-0.1-0.5,0.3-0.4,0.5-0.4c2.4-0.1,4.7-0.4,7.1-0.7c0.3,0,0.6-0.2,0.7,0.3c0,0.4,0,0.6-0.5,0.7c-2,0.3-2,0.3-2,2.3
                                                c0,2.9-0.1,5.8-0.1,8.7c0,1-0.1,2-0.4,3c-0.7,2.2-2.2,3.6-4.5,4.2c-0.6,0.2-1.1,0-1.4-0.6c-0.3-0.5-0.1-0.9,0.3-1.3
                                                c0.5-0.4,1-0.3,1.5,0.1c0.3,0.3,0.4,0.1,0.6-0.1c0.7-0.6,1-1.5,1.1-2.4C24.3,32.4,24.4,30.1,24.3,27.8z"/>
                                            <path fill="#4AC6E9" d="M32,30.3c0-1.5,0-2.9,0-4.4c0-0.5-0.2-0.7-0.6-0.7c-0.6,0-1.2,0-1.8,0c-0.2,0-0.5,0.1-0.5-0.3
                                                c0-0.3-0.1-0.7,0.5-0.7c2.2-0.1,4.3-0.3,6.4-0.8c0.2,0,0.4-0.1,0.4,0.2c0,0.3,0.3,0.6-0.2,0.8c-0.5,0.2-1.2,0.1-1.4,0.6
                                                c-0.2,0.5-0.1,1.1-0.1,1.6c0,2.6,0,5.2,0,7.8c0,1,0.1,1.1,1.1,1.1c0.6,0,0.7,0.3,0.6,0.8c0,0.3-0.1,0.4-0.4,0.3
                                                c-1.9-0.1-3.8-0.1-5.8,0c-0.3,0-0.4-0.1-0.4-0.4c0-0.3-0.2-0.6,0.3-0.7c0,0,0,0,0.1,0c1.6-0.3,1.6-0.2,1.6-1.9
                                                C32,32.6,32,31.4,32,30.3z"/>
                                            <path fill="#4AC6E9" d="M47.7,28.5c0-1.3,0-2.7,0-4c0-0.6-0.2-0.8-0.8-0.8c-0.2,0-0.4,0-0.6,0c-0.3,0-0.6,0.1-0.5-0.4
                                                c0-0.4,0-0.7,0.5-0.7c1.8,0,3.6-0.4,5.4-0.7c0.2,0,0.4-0.2,0.4,0.2c0,0.3,0.3,0.7-0.2,0.8c-1.5,0.1-1.5,1-1.5,2.2c0,2.5,0,5,0,7.5
                                                c0,1.2,0.1,1.2,1.2,1.3c0.5,0,0.6,0.2,0.6,0.6c0,0.4-0.2,0.5-0.6,0.5c-1.8-0.1-3.7-0.2-5.5,0c-0.3,0-0.5,0-0.5-0.4
                                                c0-0.3-0.1-0.6,0.5-0.7c1.5-0.1,1.5-0.1,1.6-1.6C47.7,31,47.7,29.8,47.7,28.5C47.7,28.5,47.7,28.5,47.7,28.5z"/>
                                            <path fill="#4AC6E9" d="M49.2,16.6c-1.4,0-2.5,1-2.5,2.4c0,1.3,1.1,2.4,2.4,2.4c1.3,0,2.4-1,2.4-2.4C51.5,17.7,50.5,16.6,49.2,16.6
                                                z M49.1,20.1c-0.7,0-1.1-0.5-1.1-1.2c0-0.6,0.5-1.1,1.1-1.1c0.7,0,1.1,0.5,1.1,1.2C50.2,19.7,49.7,20.1,49.1,20.1z"/>
                                            <path fill="#4AC6E9" d="M25.4,14.1c-1.3,0-2.4,1.1-2.4,2.4c0,1.4,1.1,2.5,2.4,2.5c1.3,0,2.4-1.1,2.4-2.4
                                                C27.8,15.2,26.8,14.1,25.4,14.1z M25.5,17.7c-0.6,0-1.2-0.5-1.2-1.1c0-0.6,0.5-1.2,1.2-1.2c0.6,0,1.1,0.5,1.1,1.1
                                                C26.6,17.1,26.1,17.6,25.5,17.7z"/>
                                            <path fill="#4AC6E9" d="M41.3,15.4c-1.4,0-2.5,1-2.5,2.4c0,1.3,1.1,2.4,2.4,2.4c1.3,0,2.4-1.1,2.4-2.4
                                                C43.6,16.4,42.6,15.4,41.3,15.4z M41.2,18.9c-0.6,0-1.2-0.5-1.2-1.1c0-0.6,0.6-1.1,1.1-1.2c0.6,0,1.2,0.5,1.2,1.2
                                                C42.3,18.4,41.8,18.8,41.2,18.9z"/>
                                            <path fill="#4AC6E9" d="M33.3,18.1c-1.4,0-2.4,1-2.4,2.4c0,1.3,1.1,2.4,2.4,2.4c1.3,0,2.4-1,2.4-2.4C35.7,19.1,34.7,18.1,33.3,18.1
                                                z M33.3,21.7c-0.7,0-1.1-0.5-1.1-1.2c0-0.6,0.5-1.1,1.1-1.1c0.7,0,1.1,0.5,1.1,1.2C34.4,21.2,33.9,21.7,33.3,21.7z"/>
                                            <path fill="#4AC6E9" d="M17.5,17.2c-1.4,0-2.4,1-2.4,2.4c0,1.4,1,2.4,2.4,2.4c1.3,0,2.4-1,2.4-2.3C19.9,18.2,18.9,17.2,17.5,17.2z
                                                 M17.5,20.7c-0.6,0-1.2-0.5-1.2-1.2c0-0.6,0.5-1.1,1.1-1.1c0.6,0,1.2,0.5,1.2,1.1C18.6,20.2,18.1,20.7,17.5,20.7z"/>
                                            <g>
                                                <path fill="#4AC6E9" d="M52.9,21.7h0.7v0.2h-0.2v0.6h-0.2v-0.6h-0.2V21.7z"/>
                                                <path fill="#4AC6E9" d="M53.7,21.7h0.2l0.2,0.5l0.2-0.5h0.2v0.8h-0.2l0-0.5l-0.2,0.4h-0.1L53.9,22v0.5h-0.2V21.7z"/>
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                                <div class="item_text">
                                    <h5>Kijiji Tool</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links change_color">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-chart-bar"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Website Analytics</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo Url::to(['formtools/formtools/index']);?>" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-chart-bar"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Form Tool</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>

        </div>
</div>

<?php
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'CAPTURE AND RESPONSE SYSTEM';
?>
<div class="site-index">
        <div class="dashboard_items">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo Url::to(['site/active-listings']);?>" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-home"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Active listings</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo Url::to(['site/marketing']);?>" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-poll"></i>

                                </div>
                                <div class="item_text">
                                    <h5>Marketing</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo Url::to(['site/contacts']);?>" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-address-book"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Contacts</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo Url::to(['site/deal-tracker']);?>" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-search-dollar"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Deal Trackers</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo Url::to(['site/leadership']);?>" class="dashboard_links change_color">
                            <div class="single_item ">
                                <div class="item_logo">
                                    <i class="fas fa-dollar-sign"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Leadership</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fas fa-users"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Reggie</h5>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo Url::to(['site/users']);?>" class="dashboard_links">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fab fa-elementor"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Heroes</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links change_color">
                            <div class="single_item">
                                <div class="item_logo">
                                    <i class="fab fa-elementor"></i>
                                </div>
                                <div class="item_text">
                                    <h5>Yadda Yadda</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>

        </div>
</div>

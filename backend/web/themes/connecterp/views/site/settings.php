<?php
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'Settings';

?>
<div class="site-index custom_bg">
  
       <div class="dashboard_items">
           <div class="row">
          
               <div class="col-md-3">
                   <div class="single-item panel panel-primary">
                        <div class="item_heading  panel-heading">
                            <h3>General Environment</h3>
                       </div>
                       <div class="panel-body padding_zero">
                         <div class="item_list">
                           <ul>
                            <li><a href="<?php echo Url::to(['generalsettings/city/index']);?>">Cities</a></li>
                            <li><a href="<?php echo Url::to(['generalsettings/province/index']);?>">Provinces</a></li>
                            <li><a href="<?php echo Url::to(['generalsettings/country/index']);?>">Country</a></li>
                            <li><a href="<?php echo Url::to(['setting/backend-setting/general']);?>">Backend Settings</a></li>
                            <li><a href="<?php echo Url::to(['generalsettings/quote/index']);?>">Quote</a></li>
                            <li><a href="#">Future</a></li>
                             
                           </ul>
                       </div>
                   </div>
                   </div>
               </div>
               <div class="col-md-3">
                   <div class="single-item panel panel-primary">
                        <div class="item_heading  panel-heading">
                            <h3>Active Listings</h3>
                       </div>
                       <div class="panel-body padding_zero">
                         <div class="item_list">
                           <ul>
                            <li><a href="#">Developer Grades</a></li>
                            <li><a href="#">Development Grades</a></li>
                            <li><a href="<?php echo Url::to(['setting/sales-status/index']);?>">Sales Status</a></li>
                            <li><a href="#">Recognition Types</a></li>
                               <li><a href="<?php echo Url::to(['setting/suite-type/index']);?>">Suite Type</a></li>
                            <li><a href="#">Future</a></li>
                           </ul>
                       </div>
                   </div>
                   </div>
               </div>
               <div class="col-md-3">
                   <div class="single-item panel panel-primary">
                        <div class="item_heading  panel-heading">
                            <h3>Marketing</h3>
                       </div>
                       <div class="panel-body padding_zero">
                         <div class="item_list">
                           <ul>
                               <li><a href="<?php echo Url::to(['setting/media-type/index']);?>"> Media Source Type </a></li>
                            <li><a href="<?php echo Url::to(['setting/newsletter-status/index']);?>">Newsletter Status</a></li>
                            <li><a href="#">Future</a></li>
                            <li><a href="#">Future</a></li>
                            <li><a href="#">Future</a></li>
                            <li><a href="#">Future</a></li>

                           </ul>
                       </div>
                   </div>
                   </div>
               </div>
               <div class="col-md-3">
                   <?php
                        $roleName = \unlock\modules\core\helpers\CommonHelper::getLoggedInUserRoleName();
                   ?>
                   <div class="single-item panel panel-primary">
                        <div class="item_heading  panel-heading">
                            <h3>users</h3>
                       </div>
                       <div class="panel-body padding_zero">
                         <div class="item_list">
                           <ul>
                               <?php
                                    if($roleName == 'admin'){
                                        ?>
                                        <li><a href="<?php echo Url::to(['user/profile']);?>">My Profile</a></li>
                                        <li><a href="<?php echo Url::to(['user/account']);?>">Change My Password</a></li>
                                        <?php
                                    }else{
                                        ?>
                                        <li><a href="<?php echo Url::to(['user/admin/create']);?>">Add New User</a></li>
                                        <li><a href="<?php echo Url::to(['user/admin/index']);?>">Users List</a></li>
                                        <li><a href="<?php echo Url::to(['role/role/index']);?>">User Role</a></li>
                                        <li><a href="<?php echo Url::to(['permission/access-control/assign-permission']);?>">User Permission</a></li>
                                        <?php
                                    }
                               ?>


                            <li><a href="<?php echo Url::to(['permission/access-control/reload-permission']);?>">Reload Permission</a></li>
                            <li><a href="<?php echo Url::to(['permission/access-control/reload-permission-session']);?>">Reload Session</a></li>
                           </ul>
                       </div>
                   </div>
                   </div>
               </div>
           </div>
           <div class="row">
               <div class="col-md-3">
                   <div class="single-item panel panel-primary">
                        <div class="item_heading  panel-heading">
                            <h3>Contacts</h3>
                       </div>
                       <div class="panel-body padding_zero">
                         <div class="item_list">
                           <ul>
                            <li><a href="<?php echo Url::to(['setting/agent-type/index']);?>">Agent Types</a></li>
                            <li><a href="<?php echo Url::to(['setting/brands/index']);?>">Brands</a></li>
                            <li><a href="<?php echo Url::to(['setting/interaction-type/index']);?>">Interaction Type</a></li>
                            <li><a href="<?php echo Url::to(['setting/registration-status/index']);?>">Registration Status</a></li>
                            <li><a href="<?php echo Url::to(['setting/rsvp-status/index']);?>">Rsvp Status</a></li>
                            <li><a href="<?php echo Url::to(['setting/reservation-status']);?>">Reservation Status</a></li>
                            <li><a href="<?php echo Url::to(['contacts/contacts-tags-categories/index']);?>">Contacts Tag Category</a></li>
                            <li><a href="#">Email Lists</a></li>
                           </ul>
                       </div>
                   </div>
                   </div>
               </div>
               <div class="col-md-3">
                   <div class="single-item panel panel-primary">
                        <div class="item_heading  panel-heading">
                            <h3>Deal Tracker</h3>
                       </div>
                       <div class="panel-body padding_zero">
                         <div class="item_list">
                           <ul>
                            <li><a href="#">Tracker Statuses</a></li>
                            <li><a href="#">Future</a></li>
                            <li><a href="#">Future</a></li>
                            <li><a href="#">Future</a></li>
                            <li><a href="#">Future</a></li>
                            <li><a href="#">Future</a></li>
                            <li><a href="#">Future</a></li>
                            <li><a href="#">Future</a></li>
                           </ul>
                       </div>
                   </div>
                   </div>
               </div>
               <div class="col-md-3">
                   <div class="single-item panel panel-primary">
                        <div class="item_heading  panel-heading">
                            <h3>System</h3>
                       </div>
                       <div class="panel-body padding_zero">
                         <div class="item_list">
                           <ul>
                               <li><a href="<?php echo Url::to(['contacts/contacts-systeminfo/index']);?>">System Log</a></li>
                            <li><a href="<?php echo Url::to(['contacts/contacts-systeminfo/api-info']);?>">API Reference Page</a></li>
                            <li><a href="#">Future</a></li>
                            <li><a href="#">Future</a></li>
                            <li><a href="#">Future</a></li>
                            <li><a href="#">Future</a></li>
                            <li><a href="#">Future</a></li>
                            <li><a href="#">Future</a></li>
                           </ul>
                       </div>
                   </div>
                   </div>
               </div>
               <div class="col-md-3">
                   <div class="single-item panel panel-primary">
                        <div class="item_heading  panel-heading">
                            <h3>Financial</h3>
                       </div>
                       <div class="panel-body padding_zero">
                         <div class="item_list">
                           <ul>
                            <li><a href="#">Future</a></li>
                            <li><a href="#">Future</a></li>
                            <li><a href="#">Future</a></li>
                            <li><a href="#">Future</a></li>
                            <li><a href="#">Future</a></li>
                            <li><a href="#">Future</a></li>
                            <li><a href="#">Future</a></li>
                            <li><a href="#">Future</a></li>

                           </ul>
                       </div>
                   </div>
                   </div>
               </div>
           </div>
           <div class="row">
              
               <div class="col-md-3">
                   <div class="single-item panel panel-primary">
                        <div class="item_heading  panel-heading">
                            <h3>Reggie</h3>
                       </div>
                       <div class="panel-body padding_zero">
                         <div class="item_list">
                           <ul>
                            <li><a href="#" class="dashboard_links">Future</a></li>
                            <li><a href="#">Future</a></li>
                            <li><a href="#">Future</a></li>
                            <li><a href="#">Future</a></li>
                            <li><a href="#">Future</a></li>
                            <li><a href="#">Future</a></li>
                           
                           </ul>
                       </div>
                   </div>
                   </div>
               </div>
               
           </div>
       </div>

</div>
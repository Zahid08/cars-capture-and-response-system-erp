<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\ConnecterpAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\helpers\Url;
use yii\widgets\Menu;
use unlock\modules\core\helpers\CommonHelper;

ConnecterpAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="32x32" href="<?= Yii::getAlias('@web/themes/connecterp') ?>/images/favicon-32x32.png">
    <?= Html::csrfMetaTags() ?>
    <title>CARS</title>
    <?php $this->head() ?>
</head>
<body class="<?=strtolower($this->title)?>">
<?php $this->beginBody() ?>
<header class="header_area">
    <div class="container">
        <div class="upper">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12 text-left order-lg-1 order-1">
                    <div class="logo">
                        <a href="<?php echo Url::home(true);?>"><img class="img-fluid" src="<?= Yii::getAlias('@web/themes/connecterp') ?>/images/logo.png"></a>
                    </div>
                </div>
                
                <div class="col-md-4 col-sm-5 col-xs-12 order-lg-3 order-md-2 header_links">
                    <ul>
                        <?php
                            $activeclass = '';
                            if( $this->context->route == 'site/settings'){
                                $activeclass = 'active';
                            }elseif( $this->context->route == 'site/active-listings-setting'){
                                $activeclass = 'active';
                            }elseif( $this->context->route == 'site/contact-settings'){
                                $activeclass = 'active';
                            }elseif( $this->context->route == 'site/location-settings'){
                                $activeclass = 'active';
                            }elseif( $this->context->route == 'site/users'){
                                $activeclass = 'active';
                            }
                        ?>
                        <li class="<?php echo $activeclass; ?>">
                            <a href="<?php echo Url::to(['/site/settings']); ?>">Settings <i class="fas fa-cog"></i></a>
                        </li>
                        <li>
                            <?= Html::a('<i class="fa fa-power-off"></i> Logout',['/user/logout'],['data-method' => 'post', 'data-pjax' => '0'])?>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 col-sm-3 col-xs-12 text-center order-lg-2 order-md-3">
                    <h3 class="menu_header">CAPTURE AND RESPONSE SYSTEM<?php //$this->title; ?></h3>
                </div>
            </div>
        </div>
    </div>
        <div class="lower">
            <div class="container">
                <div class="row">
                    <div class="mobile-menu">
                        <button id="header_icon"><i class="fas fa-bars"></i></button>
                    </div>
                    <?php

                    echo Menu::widget([
                        'options'=> ['class'=>'main-menu'],
                        'items' => [
                            // Important: you need to specify url as 'controller/action',
                            // not just as 'controller' even if default action is used.
                            // 'Products' menu item will be selected as long as the route is 'product/index'
                            [
                                'label' => 'Active Listings',
                                'url' => ['/site/active-listings'],
                                'items' => [
                                    [
                                        'label' => 'Developers',
                                        'url' => Url::to(['/developers/developers/index']),
                                        'items' => [
                                            [
                                                'label' => 'Create Developer',
                                                'url' => Url::to(['/developers/developers/create']),
                                            ],
                                        ],

                                    ],
                                    [
                                        'label' => 'Developer Contacts',
                                        'url' => Url::to(['/developers/developers-contacts/index']),
                                        'items' => [
                                            [
                                                'label' => 'Add Developer Contacts',
                                                'url' => Url::to(['/developers/developers-contacts/create']),
                                            ],
                                        ],

                                    ],
                                    [
                                        'label' => 'Developments',
                                        'url' => Url::to(['/developments/developments/index']),
                                        'items' => [
                                            [
                                                'label' => 'Add Developments',
                                                'url' => Url::to(['/developments/developments/create']),
                                            ],
                                        ],
                                    ],
                                    [
                                        'label' => 'Allocations',
                                        'url' => Url::to(['/allocation/allocation/index'])
                                    ],
                                    [
                                        'label' => 'Storyboard',
                                        'url' => '#'
                                    ],
                                    [
                                        'label' => 'Assignments',
                                        'url' => Url::to(['/assignments/assignments/index']),
                                        'items' => [
                                            [
                                                'label' => 'Add Assignments',
                                                'url' => Url::to(['/assignments/assignments/create']),
                                            ],
                                        ],
                                    ],
                                ],

                            ],
                            [
                                'label' => 'Marketing',
                                'url' => ['/site/marketing'],
                                'items'=>[
                                    [
                                        'label' => 'Media Sources',
                                        'url' => ['/setting/lead-source'],
                                        'items' => [
                                            [
                                                'label' => 'Add Media Sources',
                                                'url' => ['/setting/lead-source/create'],
                                            ],
                                            [
                                                'label' => 'Media Contacts',
                                                'url' => Url::to(['#']),
                                            ],
                                        ],

                                    ],

                                    [
                                        'label' => 'Campaigns & Deployment',
                                        'url' => Url::to(['#']),
                                        'items' => [
                                            [
                                                'label' => 'Add Campaign',
                                                'url' => Url::to(['#']),
                                            ],
                                        ],
                                    ],
                                    [
                                        'label' => 'Media Performance',
                                        'url' => Url::to(['#']),
                                    ],
                                    [
                                        'label' => 'Event Management',
                                        'url' => Url::to(['#']),
                                    ],

                                    [
                                        'label' => 'Marketing Tools',
                                        'url' => Url::to(['#']),
                                        'items' => [
                                            [
                                                'label' => 'Newsletter Tool',
                                                'url' => Url::to(['#']),
                                            ],
                                            [
                                                'label' => 'Form Tool',
                                                'url' => ['/formtools/formtools/index'],
                                            ],

                                            [
                                                'label' => 'AdWords Tool',
                                                'url' => Url::to(['#']),
                                            ],
                                            [
                                                'label' => 'Facebook Tool',
                                                'url' => Url::to(['#']),
                                            ],
                                            [
                                                'label' => ' Kijiji Tool',
                                                'url' => Url::to(['#']),
                                            ],
                                        ],

                                    ],
                                    [
                                        'label' => 'Website Analytics',
                                        'url' => Url::to(['#']),
                                    ],
                                ],
                            ],
                            [
                                'label' => 'Contacts',
                                'url' => ['/site/contacts'],
                                'items' => [
                                    [
                                        'label' => 'Prospects',
                                        'url' => Url::to(['/contacts/contacts/index']),
                                        'items' => [
                                            [
                                                'label' => 'Add Prospect',
                                                'url' => Url::to(['/contacts/contacts/create']),
                                            ],
                                        ],

                                    ],
                                    [
                                        'label' => 'Registrations',
                                        'url' => Url::to(['/contacts/contacts-registration/index']),
                                        'items' => [
                                            [
                                                'label' => 'Add Registration',
                                                'url' => Url::to(['/contacts/contacts-registration/create']),
                                            ],
                                        ],

                                    ],
                                    [
                                        'label' => 'RSVP',
                                        'url' => Url::to(['/contacts/contacts-rsvp/index']),
                                        'items' => [
                                            [
                                                'label' => 'Add RSVP',
                                                'url' => Url::to(['/contacts/contacts-rsvp/create']),
                                            ],
                                        ],

                                    ],
                                    [
                                        'label' => 'Reservation',
                                        'url' => Url::to(['/contacts/contacts-reservations/index']),
                                        'items' => [
                                            [
                                                'label' => 'Add Reservation',
                                                'url' => Url::to(['/contacts/contacts-reservations/create']),
                                            ],
                                        ],

                                    ],
                                    [
                                        'label' => 'Purchasers',
                                        'url' => Url::to(['/contacts/contacts-purchases/index']),
                                        'items' => [
                                            [
                                                'label' => 'Add Purchaser',
                                                'url' => Url::to(['/contacts/contacts-purchases/create']),
                                            ],
                                        ],

                                    ],
                                    [
                                        'label' => 'Agents',
                                        'url' => Url::to(['/contacts/agent/index']),
                                        'items' => [
                                            [
                                                'label' => 'Add Agents',
                                                'url' => Url::to(['/contacts/agent/create']),
                                            ],
                                        ],

                                    ],
                                    [
                                        'label' => 'Contact Tags',
                                        'url' => Url::to(['/contacts/contacts-tags-new/index']),
                                        'items' => [
                                            [
                                                'label' => 'Add Contact Tags',
                                                'url' => Url::to(['/contacts/contacts-tags-new/create']),
                                            ],
                                        ],

                                    ],
                                    [
                                        'label' => 'Contact Import',
                                        'url' => Url::to(['/contacts/contacts/import-contacts']),
                                    ],

                                ],
                            ],
                            [
                                'label' => 'Deal Tracker',
                                'url' => ['/site/deal-tracker'],
                            ],
                            [
                                'label' => 'Leadership',
                                'url' => ['/site/leadership'],
                                'visible'=> Yii::$app->user->checkAccess('site', 'financial'),
                            ],
                            [
                                'label' => 'Heroes',
                                'url' => ['/site/heroes'],
                                'items' => [
                                    [
                                        'label' => 'Project Manager',
                                        'url' => Url::to(['#']),
                                    ],
                                    [
                                        'label' => 'Timesheets',
                                        'url' => Url::to(['#']),
                                    ],
                                ],
                            ],
                            [
                                'label' => 'Reggie',
                                'url' => ['/site/reggie'],
                                'items' => [
                                    [
                                        'label' => 'Email Log',
                                        'url' => Url::to(['/contacts/contacts-email-log/index']),
                                    ],
                                    [
                                        'label' => 'Import Log',
                                        'url' => Url::to(['/contacts/contacts-import-log/index']),
                                    ],
                                    [
                                        'label' => 'Registration Report',
                                        'url' => Url::to(['/contacts/contacts-registration/registration-report']),
                                    ],
                                ],
                            ],

                        ],
                    ]);
                    ?>

                    <?php
                        NavBar::begin([
                         'brandLabel' => Yii::$app->name,
                            'brandUrl' => Yii::$app->homeUrl,
                            'options' => [
                                'class' => 'navbar navbar-expand-md cus_navbar',
                            ],
                        ]);
                        NavBar::end();
                    ?>

                </div>
            </div>
        </div>
   </header>
<div class="content-wrapper">
    <div class="container">
         <div id="app3">
            <!-- <fullscreen :fullscreen.sync="fullscreen" ref="fullscreen" class="container" background="#EEE">
            <p class="fullscreen-instruct"><span><button title="Enter/Exit Fullscreen Mode" class="fullscreen-toggle" @click="toggle"></button></span></p> -->      
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        
        <?= $content ?>
            <!-- </fullscreen> -->
        </div>
    </div>
</div>
<?php $quoteList=CommonHelper::quoteList();?>
<section id="quotes">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <div class="quote_items owl-carousel owl-loaded owl-drag">
                    <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-3330px, 0px, 0px); transition: all 0.25s ease 0s; width: 7770px;">
                            <?php foreach ($quoteList as $quote): ?>
                                <div class="owl-item" style="width: 1110px;">
                                    <div class="quote_item text-center">
                                        <div class="quote_content">
                                            <span><?php echo $quote['quote'];?></span><br>
                                            <p class="author_name"><?php echo $quote['author'];?></p>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="owl-nav disabled">
                        <button type="button" role="presentation" class="owl-prev">
                            <svg class="svg-inline--fa fa-angle-left fa-w-8" aria-hidden="true" data-prefix="fa" data-icon="angle-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" data-fa-i2svg="">
                                <path fill="currentColor" d="M31.7 239l136-136c9.4-9.4 24.6-9.4 33.9 0l22.6 22.6c9.4 9.4 9.4 24.6 0 33.9L127.9 256l96.4 96.4c9.4 9.4 9.4 24.6 0 33.9L201.7 409c-9.4 9.4-24.6 9.4-33.9 0l-136-136c-9.5-9.4-9.5-24.6-.1-34z"></path>
                            </svg><!-- <i class="fa fa-angle-left"></i> -->
                        </button>
                        <button type="button" role="presentation" class="owl-next">
                            <svg class="svg-inline--fa fa-angle-right fa-w-8" aria-hidden="true" data-prefix="fa" data-icon="angle-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" data-fa-i2svg="">
                                <path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z"></path>
                            </svg><!-- <i class="fa fa-angle-right"></i> -->
                        </button>
                    </div>
                    <div class="owl-dots disabled">

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<footer class="footer">
    <div class="container">

        <p class="col-md-6 col-12">&copy; <?= date('Y') ?> <?= Html::encode(Yii::$app->name) ?>.</p>

        <p class="col-md-6 col-12">Software Development By <a style="color:#fff" href="http://www.unlocklive.com">Unlocklive IT Limited</a></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

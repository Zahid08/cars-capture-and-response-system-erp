var baseurl_registration    = 'https://connectassetmgmt.connectcondos.com';
var version                 = 1.5;
var current_date            = new Date();
var event_date_html         = '';
var event_time_html         = '';
var last_key_of_json;
var first_key_of_json;

/*var highest = eventsinfo[ Object.keys(eventsinfo).sort().pop() ];*/
if (form_type_registration=="RSVP" || form_type_registration=="Appointment") {
    //Embeded Event Date
    for (var key in eventsinfo) {
        //embeded date
        var get_event_date= new Date(key);
        var event_date_wrapper = formatDateV1(get_event_date);
        event_date_html +="<option value='"+key+"'>"+event_date_wrapper+"</option>";

        //findout last event json key
        if(eventsinfo.hasOwnProperty(key)){
            last_key_of_json = key;
        }
    }
    //Embeded Event Time
    for(var key in eventsinfo) {
        console.log(key);
        if(eventsinfo.hasOwnProperty(key)) {
            first_key_of_json = eventsinfo[key];
            break;
        }
    }
    for (var i=0;i<first_key_of_json.length;i++) {
        event_time_html +="<option>"+first_key_of_json[i]+"</option>";
    }
    //Reddeclare to Registration
    var event_date=new Date(last_key_of_json);
    if (event_date < current_date) {
        form_type_registration = "Registration";
    }
}



//Embeded Registration Html
var embeded_registration_template_registration ='<link rel="stylesheet" type="text/css" href="'+baseurl_registration+'/backend/web/themes/connecterp/css/cars-portableform-embed.css?v='+version+'">\n' +
    '<div class="portable-hmtl-container">\n' +
    '   <div class="portable-hmtl-row">\n' +
    '           <div style="text-align: center;"><label id="message'+form_id_registration+'"></label></div><form id="cars-portable-form'+form_id_registration+'">\n' +
    '           <div class="portable-hmtl-form-group">\n' +
    '               <label class="portable-hmtl-label" for="first_name">First Name :<span style="color: red;">*</span></label>\n' +
    '               <input type="text" class="portable-hmtl-form-control" id="first_name'+form_id_registration+'" value="" name="first_name" placeholder="First Name*"><span id="first_name_error'+form_id_registration+'"></span>\n' +
    '           </div>\n' +
    '           <div class="portable-hmtl-form-group">\n' +
    '               <label class="portable-hmtl-label" for="last_name">Last Name :</label>\n' +
    '               <input type="text" class="portable-hmtl-form-control" id="last_name'+form_id_registration+'" value="" name="last_name" placeholder="Last Name">\n' +
    '           </div>\n' +
    '           <div class="portable-hmtl-form-group">\n' +
    '               <label class="portable-hmtl-label" for="email">Email :<span style="color: red;">*</span></label>\n' +
    '               <input  type="email" class="portable-hmtl-form-control" id="email'+form_id_registration+'" value="" placeholder="Email*" name="email" ><span id="email_error'+form_id_registration+'"></span>\n' +
    '           </div>\n' +
    '           <div class="portable-hmtl-form-group">\n' +
    '               <label class="portable-hmtl-label" for="phone">Phone :<span id="phone_star'+form_id_registration+'"></span></label>\n' +
    '               <input type="number" class="portable-hmtl-form-control" id="phone'+form_id_registration+'" value="" name="phone" placeholder="Phone" onkeydown="javascript: return event.keyCode == 69 ? false : true"><span id="phone_error'+form_id_registration+'"></span>\n' +
    '           </div>\n' +
    '           <div class="radio" id="event-show-status'+form_id_registration+'">\n' +
    '            <label class="portable-hmtl-label check-box-label" for="event"><input type="checkbox" id="event'+form_id_registration+'" name="event" value="true"> I am attending the Event</label>\n' +
    '           </div>\n' +
    '           <div id="event_section">\n' +
    '              <div class="portable-hmtl-form-group">\n' +
    '                <label class="portable-hmtl-label portable-hmtl-event-on" for="on">On :</label>\n' +
    '                <select class="portable-hmtl-form-control event_on" name="on" id="event_on'+form_id_registration+'" onchange="loadtimefunctionV1(this.value)">'+event_date_html+'</select>\n' +
    '              </div>\n' +
    '              <div class="portable-hmtl-form-group portable-hmtl-event-at">\n' +
    '                <label class="portable-hmtl-label" for="at">At :</label>\n' +
    '                <select class="portable-hmtl-form-control event_at" name="at" id="event_at'+form_id_registration+'">'+event_time_html+'</select>\n' +
    '              </div>\n' +
    '            </div>\n' +
    '           <div class="portable-hmtl-form-group">\n' +
    '               <label class="portable-hmtl-label check-box-label"><input type="checkbox" id="realtor'+form_id_registration+'" name="realtor" value="2"> I am a Realtor</label>\n' +
    '           </div>\n' +
    '           <div class="portable-hmtl-button-group"><p id="privacy_policy_link_show'+form_id_registration+'"></p>\n' +
    '              <input type="hidden" class="portable-hmtl-form-control" id="system_log_id'+form_id_registration+'" value="" name="system_log_id"><button id="Save'+form_id_registration+'" type="button" class="portable-hmtl-btn">'+form_button_label_registration+'<span id="submit_loader'+form_id_registration+'"></span></button>\n' +
    '            </div>\n' +
    '           </div>\n' +
    '           <div class="portable-hmtl-button-group-label">\n' +
    '              <label for="disclaimer" class="terms-label"><p>By providing your email address, CONNECT asset management will communicate with you periodically. You can unsubscribe at any time.</p><p>We never sell or share personal information with any other party.</p>View Our <a href="/privacy-policy/">Privacy Policy</a>*</label>\n' +
    '            </div>\n' +
    '       </form>\n' +
    '   </div>\n' +
    '</div>\n';

//Embeded Subscription Html
var embeded_subscription_template_registration ='<link rel="stylesheet" type="text/css" href="'+baseurl_registration+'/backend/web/themes/connecterp/css/cars-portableform-embed.css?v='+version+'">\n' +
    '     <div class="portable-hmtl-container">\n' +
    '       <div class="portable-hmtl-row">\n' +
    '               <div style="text-align: center;"><label id="message'+form_id_registration+'"></label></div><form id="cars-portable-form'+form_id_registration+'">\n' +
    '               <div class="portable-hmtl-form-group">\n' +
    '                   <label class="portable-hmtl-label" for="email">Email :<span style="color: red;">*</span></label>\n' +
    '                   <input  type="email" class="portable-hmtl-form-control" id="email'+form_id_registration+'" value="" placeholder="Enter your email" name="email"><span id="email_error'+form_id_registration+'"></span>\n' +
    '               </div>         \n' +
    '               <div class="portable-hmtl-button-group">\n' +
    '                 <input type="hidden" class="portable-hmtl-form-control" id="system_log_id'+form_id_registration+'" value="" name="system_log_id"><button id="Save'+form_id_registration+'" type="button" class="portable-hmtl-btn">+form_button_label_registration+<span id="submit_loader'+form_id_registration+'"></span></button>\n' +
    '              </div>\n' +
    '          </form>\n' +
    '      </div>\n' +
    '    </div>';

//Embeded Subscription Html
var  embeded_contact_template_registration ='<link rel="stylesheet" type="text/css" href="'+baseurl_registration+'/backend/web/themes/connecterp/css/cars-portableform-embed.css?v='+version+'">\n' +
    '     <div class="portable-hmtl-container">\n' +
    '       <div class="portable-hmtl-row">\n' +
    '               <div style="text-align: center;"><label id="message'+form_id_registration+'"></label></div><form id="cars-portable-form'+form_id_registration+'">      \n' +
    '\t\t\t   <div class="portable-hmtl-form-group">\n' +
    '                   <label class="portable-hmtl-label" for="first_name">FULL NAME :<span style="color: red;">*</span></label>\n' +
    '                   <input type="text" class="portable-hmtl-form-control" id="first_name'+form_id_registration+'" value="" name="first_name" placeholder="Enter your full name"><span id="first_name_error'+form_id_registration+'"></span>\n' +
    '               </div>  \t \n' +
    '\t\t\t   <div class="portable-hmtl-form-group">\n' +
    '                   <label class="portable-hmtl-label" for="email">EMAIL :<span style="color: red;">*</span></label>\n' +
    '                   <input  type="email" class="portable-hmtl-form-control" id="email'+form_id_registration+'" value="" name="email" placeholder="Enter your email"><span id="email_error'+form_id_registration+'"></span>\n' +
    '               </div>           \n' +
    '           <div class="portable-hmtl-form-group">\n' +
    '               <label class="portable-hmtl-label" for="phone">Phone :<span id="phone_star'+form_id_registration+'"></span></label>\n' +
    '               <input type="number" class="portable-hmtl-form-control" id="phone'+form_id_registration+'" value="" name="phone" placeholder="Enter your phone"><span id="phone_error'+form_id_registration+'"></span>\n' +
    '           </div>\n' +
    '               <div class="portable-hmtl-form-group">\n' +
    '                   <label class="portable-hmtl-label" for="phone">MESSAGE :</label>\n' +
    '                   <textarea name="message" rows="10" cols="30" class="portable-hmtl-form-control-message" id="message'+form_id_registration+'"></textarea>\n' +
    '               </div>   \n' +
    '               <div class="portable-hmtl-button-group"><p id="privacy_policy_link_show'+form_id_registration+'"></p>\n' +
    '                 <input type="hidden" class="portable-hmtl-form-control" id="system_log_id'+form_id_registration+'" value="" name="system_log_id"><button id="Save'+form_id_registration+'" type="button" class="portable-hmtl-btn">+form_button_label_registration+<span id="submit_loader'+form_id_registration+'"></span></button>\n' +
    '              </div>\n' +
    '          </form>\n' +
    '      </div>\n' +
    '     </div>';

//Success_registration Message Shwoing Html
var success_registration='<link rel="stylesheet" type="text/css" href="'+baseurl_registration+'/backend/web/themes/connecterp/css/cars-portableform-embed.css?v='+version+'">' +
    '<div class="portable-hmtl-success-container">\n' +
    '   <div class="portable-hmtl-row">   \n' +
    '           <div class="portable-hmtl-success-message">\n' +
    '               <label class="" for="submission">'+success_message_registration+'</label>  \n' +
    '           </div>\n' +
    '       </form>\n' +
    '   </div>\n' +
    '</div>';

/*Dynamic load time onchange*/
var dynamic_load_time;
function loadtimefunctionV1(value){
    if(eventsinfo.hasOwnProperty(''+value+'')) {
        dynamic_load_time = eventsinfo[''+value+''];
    }
    var embeded_new_time='';
    for (var i=0;i<dynamic_load_time.length;i++) {
        embeded_new_time +="<option>"+dynamic_load_time[i]+"</option>";
    }
    document.getElementById("event_at"+form_id_registration).innerHTML = ""+embeded_new_time+"";
}

/*Form type wise embeded html*/
if (form_type_registration=='Subscribe'){
    document.getElementById("portable-form"+form_id_registration).innerHTML = embeded_subscription_template_registration;
}
else if (form_type_registration=='Contact'){
    document.getElementById("portable-form"+form_id_registration).innerHTML = embeded_contact_template_registration;
    if (link_to_privacy_policy != ''){
         var link_privacy_contact=make_privacy_policy_linkV1(link_to_privacy_policy);
        document.getElementById("privacy_policy_link_show"+form_id_registration).innerHTML= '<a href="'+link_privacy_contact+'" target="_blank">Privacy Policy</a>';
    }
}
else {
    document.getElementById("portable-form"+form_id_registration).innerHTML = embeded_registration_template_registration;
    if (link_to_privacy_policy != ''){
        var link_privacy_registration=make_privacy_policy_linkV1(link_to_privacy_policy);
        document.getElementById("privacy_policy_link_show"+form_id_registration).innerHTML= '<a href="'+link_privacy_registration+'" target="_blank">Privacy Policy</a>';
    }
}

if (validatephone==1){
    document.getElementById("phone_star"+form_id_registration).innerHTML = "*";
    document.getElementById("phone_star"+form_id_registration).style.color = "red";
}



/*Cookie setup and save action event*/
document.onpageshow= new function() {

    if (form_type_registration =='Subscribe'){
        document.forms["cars-portable-form"+form_id_registration]["email"+form_id_registration].value      = getCookieV1('email'+form_id_registration);
    }
    else if (form_type_registration =='Contact'){
        document.forms["cars-portable-form"+form_id_registration]["email"+form_id_registration].value      = getCookieV1('email'+form_id_registration);
        document.forms["cars-portable-form"+form_id_registration]["first_name"+form_id_registration].value = getCookieV1('full_name'+form_id_registration);
        document.forms["cars-portable-form"+form_id_registration]["phone"+form_id_registration].value      = getCookieV1('phone'+form_id_registration);
    }
    else {
        const event_checkbox = document.getElementById('event'+form_id_registration); //event checkbox
        const event_section = document.getElementById('event_section'+form_id_registration); //event full section

        /*get cookie value from browser*/
        document.forms["cars-portable-form"+form_id_registration]["email"+form_id_registration].value      = getCookieV1('email'+form_id_registration);
        document.forms["cars-portable-form"+form_id_registration]["first_name"+form_id_registration].value = getCookieV1('first_name'+form_id_registration);
        document.forms["cars-portable-form"+form_id_registration]["last_name"+form_id_registration].value  = getCookieV1('last_name'+form_id_registration);
        document.forms["cars-portable-form"+form_id_registration]["phone"+form_id_registration].value      = getCookieV1('phone'+form_id_registration);
        if (getCookieV1('realtor') == 'true') {
            document.getElementById("realtor"+form_id_registration).checked = true;
        }
        if (form_type_registration =='Registration'){
            document.getElementById("event-show-status"+form_id_registration).style.display="none";
        }
        else if (form_type_registration =='RSVP') {
            event_section.style.display     = "none";
        }
        else if (form_type_registration =='Appointment') {
            event_checkbox.addEventListener('change', (event) => {
                if (event.target.checked) {
                    event_section.style.display     = "block";
                } else {
                    event_section.style.display     = "none";
                }
            });
        }
    }



    /*If Register Then Cehck Validation And Call Api*/
    document.getElementById("Save"+form_id_registration).onclick = function fun() {

        var realtor;var  event;var at;var on;var api_url;
        if (form_type_registration =='Subscribe'){
            var email           = document.forms["cars-portable-form"+form_id_registration]["email"+form_id_registration].value;
            var get_title       = document.title;
            var page_url        = window.location;
            setCookieV1('email'+form_id_registration, email, 1);
        }
        else if (form_type_registration =='Contact'){
            var email           = document.forms["cars-portable-form"+form_id_registration]["email"+form_id_registration].value;
            var full_name      = document.forms["cars-portable-form"+form_id_registration]["first_name"+form_id_registration].value;
            var phone           = document.forms["cars-portable-form"+form_id_registration]["phone"+form_id_registration].value;
            var interaction     = document.forms["cars-portable-form"+form_id_registration]["message"+form_id_registration].value;
            var get_title       = document.title;
            var page_url        = window.location;
            setCookieV1('email'+form_id_registration, email, 1);
            setCookieV1('full_name'+form_id_registration, full_name, 1);
        }
        else {
            var event_at        = document.getElementById("event_at"+form_id_registration);
            var event_on        = document.getElementById("event_on"+form_id_registration);
            var email           = document.forms["cars-portable-form"+form_id_registration]["email"+form_id_registration].value;
            var first_name      = document.forms["cars-portable-form"+form_id_registration]["first_name"+form_id_registration].value;
            var last_name       = document.forms["cars-portable-form"+form_id_registration]["last_name"+form_id_registration].value;
            var phone           = document.forms["cars-portable-form"+form_id_registration]["phone"+form_id_registration].value;
            var system_log_id   = document.forms["cars-portable-form"+form_id_registration]["system_log_id"+form_id_registration].value;
            var check_realtor   = document.getElementById("realtor"+form_id_registration).checked;
            var event_check     = document.getElementById('event'+form_id_registration).checked;
            var get_title       = document.title;
            var page_url        = window.location;

            if (check_realtor == true) {realtor = '1';} else {realtor = '';}
            if (event_check == true) {
                event = 'true';
                at = event_at.options[event_at.selectedIndex].value;
                on = event_on.options[event_on.selectedIndex].value;
            } else {event = '';at = '';on = '';}

            //set cookie
            setCookieV1('email'+form_id_registration, email, 1);
            setCookieV1('first_name'+form_id_registration, first_name, 1);
            setCookieV1('last_name'+form_id_registration, last_name, 1);
            setCookieV1('phone'+form_id_registration, phone, 1);
            setCookieV1('realtor'+form_id_registration, realtor, 1);
        }

        /*validation check Form Type and utm_source_registration exist or not*/
        if (utm_source_registration=='' || form_type_registration==''){
            document.getElementById("message"+form_id_registration).style.color      = "red";
            document.getElementById("message"+form_id_registration).innerText        =fail_message_registration+' - '+300;
            return false;
        }
        else {
            if (form_type_registration =='Subscribe' || form_type_registration =='Contact' || form_type_registration =='Registration' || form_type_registration =='RSVP' || form_type_registration =='Appointment' || form_type_registration =='Reservation' || form_type_registration =='Survey') {
                //return true;
            }
            else{
                document.getElementById("message"+form_id_registration).style.color   = "red";
                document.getElementById("message"+form_id_registration).innerText     =fail_message_registration+' - '+300;
                return false;
            }
        }

        if (form_type_registration =='Subscribe'){
            validateEmailV1(email);
            if (emailCheckV1(email) == true){
                submitLoaderV1();
                var allow_email = email.replace(/\+/g, "%2B");
                api_url = ''+baseurl_registration+'/api/web/v1/portable-form/register-contact?access-token=C6VlLzAnLJp-lM8O3v_7DBHH6I6mXql5&email=' + allow_email + '&first_name=' + '' + '&last_name=' + '' + '&phone=' + '' + '&event=' + '' + '&url=' + page_url + '&media=' + utm_source_registration + '&development_id=' + '' + '&brand=' + brand_id_registration + '&realtor=' + '' + '&development_name=' + '' + '&affiliate_tag=' + affiliate_tag_registration + '&form_type=' + form_type_registration + '&on=' + '' + '&at=' + '' + '&system_log_id=' + '' + '&page_name=' + get_title + '&comments=' + comments + '';
                apiCallV1(api_url);
            }
        }
        else if (form_type_registration =='Contact'){
            validateEmailV1(email);
            validateFullNameV1(first_name);
            if (emailCheckV1(email) == true && nameCheckV1(first_name) == true) {
                submitLoaderV1();
                var allow_email = email.replace(/\+/g, "%2B");

                var last_name=first_name='';

                if (full_name.indexOf(' ') <= 0){
                    first_name=full_name;
                }else {
                    if (full_name.substr(0,full_name.indexOf(' '))){
                        first_name=full_name.substr(0,full_name.indexOf(' '));
                    }
                    if (full_name.substr(full_name.indexOf(' ')+1)){
                        last_name=full_name.substr(full_name.indexOf(' ')+1);
                    }
                }
                console.log(first_name);
                console.log(last_name);

                api_url = ''+baseurl_registration+'/api/web/v1/portable-form/register-contact?access-token=C6VlLzAnLJp-lM8O3v_7DBHH6I6mXql5&email=' + allow_email + '&first_name=' + first_name + '&last_name=' + last_name + '&phone=' + phone + '&event=' + '' + '&url=' + page_url + '&media=' + utm_source_registration + '&development_id=' + development_id_registration + '&brand=' + brand_id_registration + '&realtor=' + '' + '&development_name=' + development_name_registration + '&affiliate_tag=' + affiliate_tag_registration + '&form_type=' + form_type_registration + '&on=' + '' + '&at=' + '' + '&system_log_id=' + '' + '&page_name=' + get_title + '&interaction=' + interaction + '&comments=' + comments + '';
                console.log(api_url);
                apiCallV1(api_url);
            }
        }
        else if (form_type_registration =='Registration'){

            validateFirstNameV1(first_name);
            validateEmailV1(email);

            if (validatephone==1){
                validatePhoneNumberV1(phone);
                if (emailCheckV1(email) == true && nameCheckV1(first_name) == true && phoneCheckV1(phone) == true) {
                    submitLoaderV1();
                    var allow_email = email.replace(/\+/g, "%2B");
                    var api_url = ''+baseurl_registration+'/api/web/v1/portable-form/register-contact?access-token=C6VlLzAnLJp-lM8O3v_7DBHH6I6mXql5&email=' + allow_email + '&first_name=' + first_name + '&last_name=' + last_name + '&phone=' + phone + '&event=' + '' + '&url=' + page_url + '&media=' + utm_source_registration + '&development_id=' + development_id_registration + '&brand=' + brand_id_registration + '&realtor=' + realtor + '&development_name=' + development_name_registration + '&affiliate_tag=' + affiliate_tag_registration + '&form_type=' + form_type_registration + '&on=' + '' + '&at=' + '' + '&system_log_id=' + '' + '&page_name=' + get_title + '&comments=' + comments + '';
                    apiCallV1(api_url);
                }
            }
            else {
                if (emailCheckV1(email) == true && nameCheckV1(first_name) == true) {
                    submitLoaderV1();
                    var allow_email = email.replace(/\+/g, "%2B");
                    var api_url = ''+baseurl_registration+'/api/web/v1/portable-form/register-contact?access-token=C6VlLzAnLJp-lM8O3v_7DBHH6I6mXql5&email=' + allow_email + '&first_name=' + first_name + '&last_name=' + last_name + '&phone=' + phone + '&event=' + '' + '&url=' + page_url + '&media=' + utm_source_registration + '&development_id=' + development_id_registration + '&brand=' + brand_id_registration + '&realtor=' + realtor + '&development_name=' + development_name_registration + '&affiliate_tag=' + affiliate_tag_registration + '&form_type=' + form_type_registration + '&on=' + '' + '&at=' + '' + '&system_log_id=' + '' + '&page_name=' + get_title + '&comments=' + comments + '';
                    apiCallV1(api_url);
                }
            }
        }
        else if (form_type_registration =='RSVP'){
            validateEmailV1(email);
            validateFirstNameV1(first_name);
            if (validatephone==1){
                validatePhoneNumberV1(phone);
                if (emailCheckV1(email) == true && nameCheckV1(first_name) == true && phoneCheckV1(phone) == true) {
                    submitLoaderV1();
                    var allow_email = email.replace(/\+/g, "%2B");
                    var api_url = ''+baseurl_registration+'/api/web/v1/portable-form/register-contact?access-token=C6VlLzAnLJp-lM8O3v_7DBHH6I6mXql5&email=' + allow_email + '&first_name=' + first_name + '&last_name=' + last_name + '&phone=' + phone + '&event=' + event + '&url=' + page_url + '&media=' + utm_source_registration + '&development_id=' + development_id_registration + '&brand=' + brand_id_registration + '&realtor=' + realtor + '&development_name=' + development_name_registration + '&affiliate_tag=' + affiliate_tag_registration + '&form_type=' + form_type_registration + '&on=' + '' + '&at=' + '' + '&system_log_id=' + '' + '&page_name=' + get_title + '&comments=' + comments + '';
                    apiCallV1(api_url);
                }
            }
            else {
                if (emailCheckV1(email) == true && nameCheckV1(first_name) == true) {
                    submitLoaderV1();
                    var allow_email = email.replace(/\+/g, "%2B");
                    var api_url = ''+baseurl_registration+'/api/web/v1/portable-form/register-contact?access-token=C6VlLzAnLJp-lM8O3v_7DBHH6I6mXql5&email=' + allow_email + '&first_name=' + first_name + '&last_name=' + last_name + '&phone=' + phone + '&event=' + event + '&url=' + page_url + '&media=' + utm_source_registration + '&development_id=' + development_id_registration + '&brand=' + brand_id_registration + '&realtor=' + realtor + '&development_name=' + development_name_registration + '&affiliate_tag=' + affiliate_tag_registration + '&form_type=' + form_type_registration + '&on=' + '' + '&at=' + '' + '&system_log_id=' + '' + '&page_name=' + get_title + '&comments=' + comments + '';
                    apiCallV1(api_url);
                }
            }
        }
        else {
            validateEmailV1(email);
            validateFirstNameV1(first_name);
            if (validatephone==1){
                validatePhoneNumberV1(phone);
                if (emailCheckV1(email) == true && nameCheckV1(first_name) == true && phoneCheckV1(phone) == true) {
                    submitLoaderV1();
                    var allow_email = email.replace(/\+/g, "%2B");
                    var api_url = ''+baseurl_registration+'/api/web/v1/portable-form/register-contact?access-token=C6VlLzAnLJp-lM8O3v_7DBHH6I6mXql5&email=' + allow_email + '&first_name=' + first_name + '&last_name=' + last_name + '&phone=' + phone + '&event=' + event + '&url=' + page_url + '&media=' + utm_source_registration + '&development_id=' + development_id_registration + '&brand=' + brand_id_registration + '&realtor=' + realtor + '&development_name=' + development_name_registration + '&affiliate_tag=' + affiliate_tag_registration + '&form_type=' + form_type_registration + '&on=' + on + '&at=' + at + '&system_log_id=' + system_log_id + '&page_name=' + get_title + '&comments=' + comments + '';
                    apiCallV1(api_url);
                }
            }
            else {
                if (emailCheckV1(email) == true && nameCheckV1(first_name) == true) {
                    submitLoaderV1();
                    var allow_email = email.replace(/\+/g, "%2B");
                    var api_url = ''+baseurl_registration+'/api/web/v1/portable-form/register-contact?access-token=C6VlLzAnLJp-lM8O3v_7DBHH6I6mXql5&email=' + allow_email + '&first_name=' + first_name + '&last_name=' + last_name + '&phone=' + phone + '&event=' + event + '&url=' + page_url + '&media=' + utm_source_registration + '&development_id=' + development_id_registration + '&brand=' + brand_id_registration + '&realtor=' + realtor + '&development_name=' + development_name_registration + '&affiliate_tag=' + affiliate_tag_registration + '&form_type=' + form_type_registration + '&on=' + on + '&at=' + at + '&system_log_id=' + system_log_id + '&page_name=' + get_title + '&comments=' + comments + '';
                    apiCallV1(api_url);
                }
            }

        }
    }



};




/*Name-check function*/
    function nameCheckV1(firstname) {
        if(firstname=='') {return false;}
        else {return true;}
    }

    function phoneCheckV1(phone) {
        if(phone=='') {return false;}
        else {return true;}
    }

/*Email-check function*/
    function emailCheckV1(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

/*Set-cokkie function*/
    function setCookieV1(name, value, days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "") + expires + "; path=/";
    }

    function getCookieV1(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    }

/*Validaton-email and firstname*/
    function validateEmailV1(email) {
        //Validate email address
        if (emailCheckV1(email) == false) {
            document.getElementById("email" + form_id_registration).style.border = "1px solid red";
            if (email == '') {
                document.getElementById("email_error" + form_id_registration).innerText = "Email is required.";
            }
            else {
                document.getElementById("email_error" + form_id_registration).innerText = "Email is not valid.";
            }
        }
        else {
            document.getElementById("email" + form_id_registration).style.border = "1px solid green";
            document.getElementById("email_error" + form_id_registration).innerText = "";
        }
    }

    function validateFirstNameV1(first_name) {
        if (nameCheckV1(first_name) == false) {
            //first_name_error
            document.getElementById("first_name" + form_id_registration).style.border = "1px solid red";
            document.getElementById("first_name_error" + form_id_registration).innerText = "First name is required.";

        }
        else {
            document.getElementById("first_name" + form_id_registration).style.border = "1px solid green";
            document.getElementById("first_name_error" + form_id_registration).innerText = "";
        }
    }

    function validateFullNameV1(full_name) {
        if (nameCheckV1(full_name) == false) {
            //first_name_error
            document.getElementById("first_name" + form_id_registration).style.border = "1px solid red";
            document.getElementById("first_name_error" + form_id_registration).innerText = "Full name is required.";

        }
        else {
            document.getElementById("first_name" + form_id_registration).style.border = "1px solid green";
            document.getElementById("first_name_error" + form_id_registration).innerText = "";
        }
    }

    function validatePhoneNumberV1(phone) {
        if (phoneCheckV1(phone) == false) {
            //first_name_error
            document.getElementById("phone" + form_id_registration).style.border = "1px solid red";
            document.getElementById("phone_error" + form_id_registration).innerText = "Phone number is required.";
            document.getElementById("phone_error" + form_id_registration).style.color = "red";
        }
        else {
            document.getElementById("phone" + form_id_registration).style.border = "1px solid green";
            document.getElementById("phone_error" + form_id_registration).innerText = "";
        }
    }


    function apiCallV1(api_url) {
        const Http = new XMLHttpRequest();
        Http.open("GET", api_url);
        Http.send();
        Http.onreadystatechange = (e) =>
        {
            //document.getElementById("portable-form").innerHTML = '<div> <center><img style="height: 85px;padding: 170px 70px 1px 2px;" src="https://connectassetmgmt.connectcondos.com/backend/web/themes/connecterp/images/loader.gif"></center> </div>';
            if (redirect_registration == 0) { //0 for success message
                setTimeout(function () {
                    document.getElementById("portable-form" + form_id_registration).innerHTML = success_registration;
                }, 1000);
            } else {  //1 for success redirect_registration
                setTimeout(function () {
                    window.location.href = redirect_url_registration;
                }, 1000);
            }
        }
        ;
    }

    function submitLoaderV1() {
        document.getElementById('submit_loader' + form_id_registration).innerHTML = '<img style="margin: -5px auto;height: 22px;padding: 0px 0px 0px 3px;" src="https://connectassetmgmt.connectcondos.com/backend/web/themes/connecterp/images/loader.gif">';
        document.getElementById('Save' + form_id_registration).style.cursor = ' not-allowed';
    }

//Extra Function
    function formatDateV1(date) {
        var monthNames = [
            "January", "February", "March",
            "April", "May", "June", "July",
            "August", "September", "October",
            "November", "December"
        ];
        var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        var dayName = days[date.getDay()];
        var day = date.getDate();
        var monthIndex = date.getMonth();
        return dayName + ' ' + monthNames[monthIndex] + ' ' + day;
    }

    function make_privacy_policy_linkV1(privacy_link) {
        var link = '';
        if (privacy_link.indexOf('https://') > -1) {
            link = privacy_link;
        }
        else if (privacy_link.indexOf('http://') > -1) {
            link = privacy_link;
        } else {
            link = 'https://' + privacy_link;
        }
        return link;
    }

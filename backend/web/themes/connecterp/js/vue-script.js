Vue.use(VueFullscreen.default)
var Main = {
  methods: {
    toggle() {
      this.$refs['fullscreen'].toggle()
    }
  },
  
  data() {
    return {
      fullscreen: false,
      showModalContact: false,
      showModalInteraction: false,
      showModalDevelopment: false,
      showModalRecognition: false
    }
  }
}
Vue.component('tabs',{
  name:'tabs',
  template: `
  <div>
    <div class="tabs">
      <ul>
        <li v-for="tab in tabs" :class="{'isActive':tab.isActive}">
        <a :href="tab.href" @click="selectTab(tab)">{{ tab.name }}</a>
        </li>
      </ul>
    </div>

    <div class="tab-details">
      <slot></slot>
    </div>
  </div>
  `,
  data(){
    return {tabs: []};
  },
  created(){
    this.tabs=this.$children;
  },
  methods: {
    selectTab(selectedTab){
      
      this.tabs.forEach(tab=>{
        tab.isActive=(tab.name==selectedTab.name);
        tab.selected=false;
      });
      selectedTab.selected=true;
    }
  }
});
Vue.component('tab',{
  name:'tab',
  template:`
  <div v-show="isActive"><slot></slot></div>
  `,
  props: {
    name: {required:true},
    selected: {default: false}
  },
  data(){
    return{
      isActive:false
    };
  },
  computed: {
    href() {
      return '#' + this.name.toLowerCase().replace(/ /g,'_');
    }
  },
  mounted(){
    this.isActive=this.selected;
  }
});
var App = Vue.extend(Main)
new App().$mount('#app3')


// var app = new Vue({
//   el: '#app',
//     data:{
        
//     }
// });
// register modal component
Vue.component('contact-modal', {
  template: `
  <transition name="modal">
    <div class="modal-mask">
      <div class="modal-wrapper">
        <div class="modal-container">
          
          <div class="modal-header">
            <slot name="header">
              
            </slot>
          </div>

          <div class="modal-body">
            <slot name="body">
              default body
            </slot>
            
            <button type="button" class="btn btn-danger modal-default-button" href="#" @click="$emit('close')"><i class="fa fa-times"></i> Cancel</button> 
          </div>

          <div class="modal-footer">
            <slot name="footer">
              
              
            </slot>
          </div>
        </div>
      </div>
    </div>
  </transition>
  `
});
Vue.component('interaction-modal', {
  template: `
  <transition name="modal">
    <div class="modal-mask">
      <div class="modal-wrapper">
        <div class="modal-container">
          
          <div class="modal-header">
            <slot name="header">
              
            </slot>
          </div>

          <div class="modal-body">
            <slot name="body">
              default body
            </slot>
            
            <button type="button" class="btn btn-danger modal-default-button" href="#" @click="$emit('close')"><i class="fa fa-times"></i> Cancel</button> 
          </div>

          <div class="modal-footer">
            <slot name="footer">
              
              
            </slot>
          </div>
        </div>
      </div>
    </div>
  </transition>
  `
});
Vue.component('development-modal', {
  template: `
  <transition name="modal">
    <div class="modal-mask">
      <div class="modal-wrapper">
        <div class="modal-container">
          
          <div class="modal-header">
            <slot name="header">
              
            </slot>
          </div>

          <div class="modal-body">
            <slot name="body">
              default body
            </slot>
            
            <button type="button" class="btn btn-danger modal-default-button" href="#" @click="$emit('close')"><i class="fa fa-times"></i> Cancel</button> 
          </div>

          <div class="modal-footer">
            <slot name="footer">
              
              
            </slot>
          </div>
        </div>
      </div>
    </div>
  </transition>
  `
});
Vue.component('recognition-modal', {
  template: `
  <transition name="modal">
    <div class="modal-mask">
      <div class="modal-wrapper">
        <div class="modal-container">
          
          <div class="modal-header">
            <slot name="header">
              
            </slot>
          </div>

          <div class="modal-body">
            <slot name="body">
              default body
            </slot>
            
            <button type="button" class="btn btn-danger modal-default-button" href="#" @click="$emit('close')"><i class="fa fa-times"></i> Cancel</button> 
          </div>

          <div class="modal-footer">
            <slot name="footer">
              
              
            </slot>
          </div>
        </div>
      </div>
    </div>
  </transition>
  `
});

// start app
// new Vue({
//   el: '#app1',
//   data: {
//     showModalContact: false,
//     showModalInteraction: false,
//     showModalDevelopment: false,
//     showModalRecognition: false
//   }
// });


var baseurl_subscribe    = 'https://connectassetmgmt.connectcondos.com';
var version                 = 1.5;
var current_date            = new Date();
var event_date_html         = '';
var event_time_html         = '';
var last_key_of_json;
var first_key_of_json;

/*var highest = eventsinfo[ Object.keys(eventsinfo).sort().pop() ];*/
if (form_type_subscribe =="RSVP" || form_type_subscribe=="Appointment") {
    //Embeded Event Date
    for (var key in eventsinfo) {
        //embeded date
        var get_event_date= new Date(key);
        var event_date_wrapper = formatDateV2(get_event_date);
        event_date_html +="<option value='"+key+"'>"+event_date_wrapper+"</option>";

        //findout last event json key
        if(eventsinfo.hasOwnProperty(key)){
            last_key_of_json = key;
        }
    }
    //Embeded Event Time
    for(var key in eventsinfo) {
        console.log(key);
        if(eventsinfo.hasOwnProperty(key)) {
            first_key_of_json = eventsinfo[key];
            break;
        }
    }
    for (var i=0;i<first_key_of_json.length;i++) {
        event_time_html +="<option>"+first_key_of_json[i]+"</option>";
    }
    //Reddeclare to Registration
    var event_date=new Date(last_key_of_json);
    if (event_date < current_date) {
        form_type_subscribe = "Registration";
    }
}

//Embeded Registration Html
var embeded_subscribe_template_subscribe ='<link rel="stylesheet" type="text/css" href="'+baseurl_subscribe+'/backend/web/themes/connecterp/css/cars-portableform-embed.css?v='+version+'">\n' +
    '<div class="portable-hmtl-container">\n' +
    '   <div class="portable-hmtl-row">\n' +
    '           <div style="text-align: center;"><label id="message'+form_id_subscribe+'"></label></div><form id="cars-portable-form'+form_id_subscribe+'">\n' +
    '           <div class="portable-hmtl-form-group">\n' +
    '               <label class="portable-hmtl-label" for="first_name">First Name :<span style="color: red;">*</span></label>\n' +
    '               <input type="text" class="portable-hmtl-form-control" id="first_name'+form_id_subscribe+'" value="'+first_name_subscribe+'" name="first_name" placeholder="First Name*"><span id="first_name_error'+form_id_subscribe+'"></span>\n' +
    '           </div>\n' +
    '           <div class="portable-hmtl-form-group">\n' +
    '               <label class="portable-hmtl-label" for="last_name">Last Name :</label>\n' +
    '               <input type="text" class="portable-hmtl-form-control" id="last_name'+form_id_subscribe+'" value="'+last_name_subscribe+'" name="last_name" placeholder="Last Name">\n' +
    '           </div>\n' +
    '           <div class="portable-hmtl-form-group">\n' +
    '               <label class="portable-hmtl-label" for="email">Email :<span style="color: red;">*</span></label>\n' +
    '               <input  type="email" class="portable-hmtl-form-control" id="email'+form_id_subscribe+'" value="'+email_subscribe+'" placeholder="Email*" name="email" ><span id="email_error'+form_id_subscribe+'"></span>\n' +
    '           </div>\n' +
    '           <div class="portable-hmtl-form-group">\n' +
    '               <label class="portable-hmtl-label" for="phone">Phone :<span id="phone_star'+form_id_subscribe+'"></span></label>\n' +
    '               <input type="number" class="portable-hmtl-form-control" id="phone'+form_id_subscribe+'" value="'+phone_subscribe+'" name="phone" placeholder="Phone" onkeydown="javascript: return event.keyCode == 69 ? false : true"><span id="phone_error'+form_id_subscribe+'"></span>\n' +
    '           </div>\n' +
    '           <div class="radio" id="event-show-status'+form_id_subscribe+'">\n' +
    '            <label class="portable-hmtl-label check-box-label" for="event"><input type="checkbox" id="event'+form_id_subscribe+'" name="event" value="true"> I am attending the Event</label>\n' +
    '           </div>\n' +
    '           <div id="event_section">\n' +
    '              <div class="portable-hmtl-form-group">\n' +
    '                <label class="portable-hmtl-label portable-hmtl-event-on" for="on">On :</label>\n' +
    '                <select class="portable-hmtl-form-control event_on" name="on" id="event_on'+form_id_subscribe+'" onchange="loadtimefunctionV1(this.value)">'+event_date_html+'</select>\n' +
    '              </div>\n' +
    '              <div class="portable-hmtl-form-group portable-hmtl-event-at">\n' +
    '                <label class="portable-hmtl-label" for="at">At :</label>\n' +
    '                <select class="portable-hmtl-form-control event_at" name="at" id="event_at'+form_id_subscribe+'">'+event_time_html+'</select>\n' +
    '              </div>\n' +
    '            </div>\n' +
    '           <div class="portable-hmtl-form-group">\n' +
    '               <label class="portable-hmtl-label check-box-label"><input type="checkbox" id="realtor'+form_id_subscribe+'" name="realtor" value="2"> I am a Realtor</label>\n' +
    '           </div>\n' +
    '           <div class="portable-hmtl-button-group"><p id="privacy_policy_link_show'+form_id_subscribe+'"></p>\n' +
    '              <input type="hidden" class="portable-hmtl-form-control" id="system_log_id'+form_id_subscribe+'" value="" name="system_log_id"><button id="Save'+form_id_subscribe+'" type="button" class="portable-hmtl-btn">'+form_button_label_subscribe+'<span id="submit_loader'+form_id_subscribe+'"></span></button>\n' +
    '            </div>\n' +
    '           <div class="portable-hmtl-button-group-label">\n' +
    '              <label for="disclaimer" class="terms-label"><p style="margin-top: 15px;">I consent to having CONNECT asset management collect my name, email, and phone number if applicable, to communicate with me. View our</p><a href="/privacy-policy/">Privacy Policy</a></label>\n' +
    '            </div>\n' +
    '       </form>\n' +
    '   </div>\n' +
    '</div>\n';

//Embeded Subscription Html
var embeded_subscription_template_subscribe ='<link rel="stylesheet" type="text/css" href="'+baseurl_subscribe+'/backend/web/themes/connecterp/css/cars-portableform-embed.css?v='+version+'">\n' +
    '     <div class="portable-hmtl-container">\n' +
    '       <div class="portable-hmtl-row">\n' +
    '               <div style="text-align: center;"><label id="message'+form_id_subscribe+'"></label></div><form id="cars-portable-form'+form_id_subscribe+'">\n' +
    '               <div class="portable-hmtl-form-group">\n' +
    '                   <label class="portable-hmtl-label" for="email">Email :<span style="color: red;">*</span></label>\n' +
    '                   <input  type="email" class="portable-hmtl-form-control" id="email'+form_id_subscribe+'" value="" placeholder="Enter your email" name="email"><span id="email_error'+form_id_subscribe+'"></span>\n' +
    '               </div>         \n' +
    '               <div class="portable-hmtl-button-group">\n' +
    '                 <input type="hidden" class="portable-hmtl-form-control" id="system_log_id'+form_id_subscribe+'" value="" name="system_log_id"><button id="Save'+form_id_subscribe+'" type="button" class="portable-hmtl-btn">+form_button_label_subscribe+<span id="submit_loader'+form_id_subscribe+'"></span></button>\n' +
    '              </div>\n' +
    '          </form>\n' +
    '      </div>\n' +
    '    </div>';

//Embeded Subscription Html
var  embeded_contact_template_subscribe ='<link rel="stylesheet" type="text/css" href="'+baseurl_subscribe+'/backend/web/themes/connecterp/css/cars-portableform-embed.css?v='+version+'">\n' +
    '     <div class="portable-hmtl-container">\n' +
    '       <div class="portable-hmtl-row">\n' +
    '               <div style="text-align: center;"><label id="message'+form_id_subscribe+'"></label></div><form id="cars-portable-form'+form_id_subscribe+'">      \n' +
    '\t\t\t   <div class="portable-hmtl-form-group">\n' +
    '                   <label class="portable-hmtl-label" for="first_name">FULL NAME :<span style="color: red;">*</span></label>\n' +
    '                   <input type="text" class="portable-hmtl-form-control" id="first_name'+form_id_subscribe+'" value="" name="first_name" placeholder="Enter your full name"><span id="first_name_error'+form_id_subscribe+'"></span>\n' +
    '               </div>  \t \n' +
    '\t\t\t   <div class="portable-hmtl-form-group">\n' +
    '                   <label class="portable-hmtl-label" for="email">EMAIL :<span style="color: red;">*</span></label>\n' +
    '                   <input  type="email" class="portable-hmtl-form-control" id="email'+form_id_subscribe+'" value="" name="email" placeholder="Enter your email"><span id="email_error'+form_id_subscribe+'"></span>\n' +
    '               </div>           \n' +
    '           <div class="portable-hmtl-form-group">\n' +
    '               <label class="portable-hmtl-label" for="phone">Phone :<span id="phone_star'+form_id_subscribe+'"></span></label>\n' +
    '               <input type="number" class="portable-hmtl-form-control" id="phone'+form_id_subscribe+'" value="" name="phone" placeholder="Enter your phone"><span id="phone_error'+form_id_subscribe+'"></span>\n' +
    '           </div>\n' +
    '               <div class="portable-hmtl-form-group">\n' +
    '                   <label class="portable-hmtl-label" for="phone">MESSAGE :</label>\n' +
    '                   <textarea name="message" rows="10" cols="30" class="portable-hmtl-form-control-message" id="message'+form_id_subscribe+'"></textarea>\n' +
    '               </div>   \n' +
    '               <div class="portable-hmtl-button-group"><p id="privacy_policy_link_show'+form_id_subscribe+'"></p>\n' +
    '                 <input type="hidden" class="portable-hmtl-form-control" id="system_log_id'+form_id_subscribe+'" value="" name="system_log_id"><button id="Save'+form_id_subscribe+'" type="button" class="portable-hmtl-btn">+form_button_label_subscribe+<span id="submit_loader'+form_id_subscribe+'"></span></button>\n' +
    '              </div>\n' +
    '          </form>\n' +
    '      </div>\n' +
    '     </div>';

//Success_subscribe Message Shwoing Html
var success_subscribe='<link rel="stylesheet" type="text/css" href="'+baseurl_subscribe+'/backend/web/themes/connecterp/css/cars-portableform-embed.css?v='+version+'">' +
    '<div class="portable-hmtl-success-container">\n' +
    '   <div class="portable-hmtl-row">   \n' +
    '           <div class="portable-hmtl-success-message">\n' +
    '               <label class="" for="submission">'+success_message_subscribe+'</label>  \n' +
    '           </div>\n' +
    '       </form>\n' +
    '   </div>\n' +
    '</div>';

/*Dynamic load time onchange*/
var dynamic_load_time;
function loadtimefunctionV1(value){
    if(eventsinfo.hasOwnProperty(''+value+'')) {
        dynamic_load_time = eventsinfo[''+value+''];
    }
    var embeded_new_time='';
    for (var i=0;i<dynamic_load_time.length;i++) {
        embeded_new_time +="<option>"+dynamic_load_time[i]+"</option>";
    }
    document.getElementById("event_at"+form_id_subscribe).innerHTML = ""+embeded_new_time+"";
}

/*Form type wise embeded html*/
if (form_type_subscribe=='Subscribe'){
    document.getElementById("portable-form"+form_id_subscribe).innerHTML = embeded_subscription_template_subscribe;
}
else if (form_type_subscribe=='Contact'){
    document.getElementById("portable-form"+form_id_subscribe).innerHTML = embeded_contact_template_subscribe;
    if (link_to_privacy_policy != ''){
        var link_privacy_contact=make_privacy_policy_linkV2(link_to_privacy_policy);
        document.getElementById("privacy_policy_link_show"+form_id_subscribe).innerHTML= '<a href="'+link_privacy_contact+'" target="_blank">Privacy Policy</a>';
    }
}
else {
    document.getElementById("portable-form"+form_id_subscribe).innerHTML = embeded_subscribe_template_subscribe;
    if (link_to_privacy_policy != ''){
        var link_privacy_subscribe=make_privacy_policy_linkV2(link_to_privacy_policy);
        document.getElementById("privacy_policy_link_show"+form_id_subscribe).innerHTML= '<a href="'+link_privacy_subscribe+'" target="_blank">Privacy Policy</a>';
    }
}

if (validatephone==1){
    document.getElementById("phone_star"+form_id_subscribe).innerHTML = "*";
    document.getElementById("phone_star"+form_id_subscribe).style.color = "red";
}



/*Cookie setup and save action event*/
document.onpageshow= new function() {

    if (form_type_subscribe =='Subscribe'){
        document.forms["cars-portable-form"+form_id_subscribe]["email"+form_id_subscribe].value      = getCookieV2('email'+form_id_subscribe);
    }
    else if (form_type_subscribe =='Contact'){
        document.forms["cars-portable-form"+form_id_subscribe]["email"+form_id_subscribe].value      = getCookieV2('email'+form_id_subscribe);
        document.forms["cars-portable-form"+form_id_subscribe]["first_name"+form_id_subscribe].value = getCookieV2('full_name'+form_id_subscribe);
        document.forms["cars-portable-form"+form_id_subscribe]["phone"+form_id_subscribe].value      = getCookieV2('phone'+form_id_subscribe);
    }
    else {
        const event_checkbox = document.getElementById('event'+form_id_subscribe); //event checkbox
        const event_section = document.getElementById('event_section'+form_id_subscribe); //event full section

        /*get cookie value from browser*/


        if(!email_subscribe){ document.forms["cars-portable-form"+form_id_subscribe]["email"+form_id_subscribe].value      = getCookieV2('email'+form_id_subscribe); }
        if(!first_name_subscribe){ document.forms["cars-portable-form"+form_id_subscribe]["first_name"+form_id_subscribe].value = getCookieV2('first_name'+form_id_subscribe); }
        if(!last_name_subscribe){ document.forms["cars-portable-form"+form_id_subscribe]["last_name"+form_id_subscribe].value  = getCookieV2('last_name'+form_id_subscribe); }
        if(!phone_subscribe){ document.forms["cars-portable-form"+form_id_subscribe]["phone"+form_id_subscribe].value      = getCookieV2('phone'+form_id_subscribe); }

        if(realtor_subscribe){
            document.getElementById("realtor"+form_id_subscribe).checked = true;
        }else{
            if (getCookieV2('realtor') == 'true') {
                document.getElementById("realtor"+form_id_subscribe).checked = true;
            }
        }

        if (form_type_subscribe =='Registration'){
            document.getElementById("event-show-status"+form_id_subscribe).style.display="none";
        }
        else if (form_type_subscribe =='RSVP') {
            event_section.style.display     = "none";
        }
        else if (form_type_subscribe =='Appointment') {
            event_checkbox.addEventListener('change', (event) => {
                if (event.target.checked) {
                event_section.style.display     = "block";
            } else {
                event_section.style.display     = "none";
            }
        });
        }
    }



    /*If Register Then Cehck Validation And Call Api*/
    document.getElementById("Save"+form_id_subscribe).onclick = function fun() {

        var realtor;var  event;var at;var on;var api_url;
        if (form_type_subscribe =='Subscribe'){
            var email           = document.forms["cars-portable-form"+form_id_subscribe]["email"+form_id_subscribe].value;
            var get_title       = document.title;
            var page_url        = window.location;
            setCookieV2('email'+form_id_subscribe, email, 1);
        }
        else if (form_type_subscribe =='Contact'){
            var email           = document.forms["cars-portable-form"+form_id_subscribe]["email"+form_id_subscribe].value;
            var full_name      = document.forms["cars-portable-form"+form_id_subscribe]["first_name"+form_id_subscribe].value;
            var phone           = document.forms["cars-portable-form"+form_id_subscribe]["phone"+form_id_subscribe].value;
            var interaction     = document.forms["cars-portable-form"+form_id_subscribe]["message"+form_id_subscribe].value;
            var get_title       = document.title;
            var page_url        = window.location;
            setCookieV2('email'+form_id_subscribe, email, 1);
            setCookieV2('full_name'+form_id_subscribe, full_name, 1);
        }
        else {
            var event_at        = document.getElementById("event_at"+form_id_subscribe);
            var event_on        = document.getElementById("event_on"+form_id_subscribe);
            var email           = document.forms["cars-portable-form"+form_id_subscribe]["email"+form_id_subscribe].value;
            var first_name      = document.forms["cars-portable-form"+form_id_subscribe]["first_name"+form_id_subscribe].value;
            var last_name       = document.forms["cars-portable-form"+form_id_subscribe]["last_name"+form_id_subscribe].value;
            var phone           = document.forms["cars-portable-form"+form_id_subscribe]["phone"+form_id_subscribe].value;
            var system_log_id   = document.forms["cars-portable-form"+form_id_subscribe]["system_log_id"+form_id_subscribe].value;
            var check_realtor   = document.getElementById("realtor"+form_id_subscribe).checked;
            var event_check     = document.getElementById('event'+form_id_subscribe).checked;
            var get_title       = document.title;
            var page_url        = window.location;

            if (check_realtor == true) {realtor = '1';} else {realtor = '';}
            if (event_check == true) {
                event = 'true';
                at = event_at.options[event_at.selectedIndex].value;
                on = event_on.options[event_on.selectedIndex].value;
            } else {event = '';at = '';on = '';}

            //set cookie
            setCookieV2('email'+form_id_subscribe, email, 1);
            setCookieV2('first_name'+form_id_subscribe, first_name, 1);
            setCookieV2('last_name'+form_id_subscribe, last_name, 1);
            setCookieV2('phone'+form_id_subscribe, phone, 1);
            setCookieV2('realtor'+form_id_subscribe, realtor, 1);
        }

        /*validation check Form Type and utm_source_subscribe exist or not*/
        if (utm_source_subscribe=='' || form_type_subscribe==''){
            document.getElementById("message"+form_id_subscribe).style.color      = "red";
            document.getElementById("message"+form_id_subscribe).innerText        =fail_message_subscribe+' - '+300;
            return false;
        }
        else {
            if (form_type_subscribe =='Subscribe' || form_type_subscribe =='Contact' || form_type_subscribe =='Registration' || form_type_subscribe =='RSVP' || form_type_subscribe =='Appointment' || form_type_subscribe =='Reservation' || form_type_subscribe =='Survey') {
                //return true;
            }
            else{
                document.getElementById("message"+form_id_subscribe).style.color   = "red";
                document.getElementById("message"+form_id_subscribe).innerText     =fail_message_subscribe+' - '+300;
                return false;
            }
        }

        if (form_type_subscribe =='Subscribe'){
            validateEmailV2(email);
            if (emailCheckV2(email) == true){
                submitLoaderV2();
                var allow_email = email.replace(/\+/g, "%2B");
                api_url = ''+baseurl_subscribe+'/api/web/v1/portable-form/register-contact?access-token=C6VlLzAnLJp-lM8O3v_7DBHH6I6mXql5&email=' + allow_email + '&first_name=' + '' + '&last_name=' + '' + '&phone=' + '' + '&event=' + '' + '&url=' + page_url + '&media=' + utm_source_subscribe + '&development_id=' + '' + '&brand=' + brand_id_subscribe + '&realtor=' + '' + '&development_name=' + '' + '&affiliate_tag=' + affiliate_tag_subscribe + '&form_type=' + form_type_subscribe + '&on=' + '' + '&at=' + '' + '&system_log_id=' + '' + '&page_name=' + get_title + '&comments=' + comments + '';
                apiCallV2(api_url);
            }
        }
        else if (form_type_subscribe =='Contact'){
            validateEmailV2(email);
            validateFullNameV2(first_name);
            if (emailCheckV2(email) == true && nameCheckV2(first_name) == true) {
                submitLoaderV2();
                var allow_email = email.replace(/\+/g, "%2B");

                var last_name=first_name='';

                if (full_name.indexOf(' ') <= 0){
                    first_name=full_name;
                }else {
                    if (full_name.substr(0,full_name.indexOf(' '))){
                        first_name=full_name.substr(0,full_name.indexOf(' '));
                    }
                    if (full_name.substr(full_name.indexOf(' ')+1)){
                        last_name=full_name.substr(full_name.indexOf(' ')+1);
                    }
                }
                console.log(first_name);
                console.log(last_name);

                api_url = ''+baseurl_subscribe+'/api/web/v1/portable-form/register-contact?access-token=C6VlLzAnLJp-lM8O3v_7DBHH6I6mXql5&email=' + allow_email + '&first_name=' + first_name + '&last_name=' + last_name + '&phone=' + phone + '&event=' + '' + '&url=' + page_url + '&media=' + utm_source_subscribe + '&development_id=' + development_id_subscribe + '&brand=' + brand_id_subscribe + '&realtor=' + '' + '&development_name=' + development_name_subscribe + '&affiliate_tag=' + affiliate_tag_subscribe + '&form_type=' + form_type_subscribe + '&on=' + '' + '&at=' + '' + '&system_log_id=' + '' + '&page_name=' + get_title + '&interaction=' + interaction + '&comments=' + comments + '';
                console.log(api_url);
                apiCallV2(api_url);
            }
        }
        else if (form_type_subscribe =='Registration'){

            validateFirstNameV2(first_name);
            validateEmailV2(email);

            if (validatephone==1){
                validatePhoneNumberV2(phone);
                if (emailCheckV2(email) == true && nameCheckV2(first_name) == true && phoneCheckV2(phone) == true) {
                    submitLoaderV2();
                    var allow_email = email.replace(/\+/g, "%2B");
                    var api_url = ''+baseurl_subscribe+'/api/web/v1/portable-form/register-contact?access-token=C6VlLzAnLJp-lM8O3v_7DBHH6I6mXql5&email=' + allow_email + '&first_name=' + first_name + '&last_name=' + last_name + '&phone=' + phone + '&event=' + '' + '&url=' + page_url + '&media=' + utm_source_subscribe + '&development_id=' + development_id_subscribe + '&brand=' + brand_id_subscribe + '&realtor=' + realtor + '&development_name=' + development_name_subscribe + '&affiliate_tag=' + affiliate_tag_subscribe + '&form_type=' + form_type_subscribe + '&on=' + '' + '&at=' + '' + '&system_log_id=' + '' + '&page_name=' + get_title + '&comments=' + comments + '';
                    apiCallV2(api_url);
                }
            }
            else {
                if (emailCheckV2(email) == true && nameCheckV2(first_name) == true) {
                    submitLoaderV2();
                    var allow_email = email.replace(/\+/g, "%2B");
                    var api_url = ''+baseurl_subscribe+'/api/web/v1/portable-form/register-contact?access-token=C6VlLzAnLJp-lM8O3v_7DBHH6I6mXql5&email=' + allow_email + '&first_name=' + first_name + '&last_name=' + last_name + '&phone=' + phone + '&event=' + '' + '&url=' + page_url + '&media=' + utm_source_subscribe + '&development_id=' + development_id_subscribe + '&brand=' + brand_id_subscribe + '&realtor=' + realtor + '&development_name=' + development_name_subscribe + '&affiliate_tag=' + affiliate_tag_subscribe + '&form_type=' + form_type_subscribe + '&on=' + '' + '&at=' + '' + '&system_log_id=' + '' + '&page_name=' + get_title + '&comments=' + comments + '';
                    apiCallV2(api_url);
                }
            }
        }
        else if (form_type_subscribe =='RSVP'){
            validateEmailV2(email);
            validateFirstNameV2(first_name);
            if (validatephone==1){
                validatePhoneNumberV2(phone);
                if (emailCheckV2(email) == true && nameCheckV2(first_name) == true && phoneCheckV2(phone) == true) {
                    submitLoaderV2();
                    var allow_email = email.replace(/\+/g, "%2B");
                    var api_url = ''+baseurl_subscribe+'/api/web/v1/portable-form/register-contact?access-token=C6VlLzAnLJp-lM8O3v_7DBHH6I6mXql5&email=' + allow_email + '&first_name=' + first_name + '&last_name=' + last_name + '&phone=' + phone + '&event=' + event + '&url=' + page_url + '&media=' + utm_source_subscribe + '&development_id=' + development_id_subscribe + '&brand=' + brand_id_subscribe + '&realtor=' + realtor + '&development_name=' + development_name_subscribe + '&affiliate_tag=' + affiliate_tag_subscribe + '&form_type=' + form_type_subscribe + '&on=' + '' + '&at=' + '' + '&system_log_id=' + '' + '&page_name=' + get_title + '&comments=' + comments + '';
                    apiCallV2(api_url);
                }
            }
            else {
                if (emailCheckV2(email) == true && nameCheckV2(first_name) == true) {
                    submitLoaderV2();
                    var allow_email = email.replace(/\+/g, "%2B");
                    var api_url = ''+baseurl_subscribe+'/api/web/v1/portable-form/register-contact?access-token=C6VlLzAnLJp-lM8O3v_7DBHH6I6mXql5&email=' + allow_email + '&first_name=' + first_name + '&last_name=' + last_name + '&phone=' + phone + '&event=' + event + '&url=' + page_url + '&media=' + utm_source_subscribe + '&development_id=' + development_id_subscribe + '&brand=' + brand_id_subscribe + '&realtor=' + realtor + '&development_name=' + development_name_subscribe + '&affiliate_tag=' + affiliate_tag_subscribe + '&form_type=' + form_type_subscribe + '&on=' + '' + '&at=' + '' + '&system_log_id=' + '' + '&page_name=' + get_title + '&comments=' + comments + '';
                    apiCallV2(api_url);
                }
            }
        }
        else {
            validateEmailV2(email);
            validateFirstNameV2(first_name);
            if (validatephone==1){
                validatePhoneNumberV2(phone);
                if (emailCheckV2(email) == true && nameCheckV2(first_name) == true && phoneCheckV2(phone) == true) {
                    submitLoaderV2();
                    var allow_email = email.replace(/\+/g, "%2B");
                    var api_url = ''+baseurl_subscribe+'/api/web/v1/portable-form/register-contact?access-token=C6VlLzAnLJp-lM8O3v_7DBHH6I6mXql5&email=' + allow_email + '&first_name=' + first_name + '&last_name=' + last_name + '&phone=' + phone + '&event=' + event + '&url=' + page_url + '&media=' + utm_source_subscribe + '&development_id=' + development_id_subscribe + '&brand=' + brand_id_subscribe + '&realtor=' + realtor + '&development_name=' + development_name_subscribe + '&affiliate_tag=' + affiliate_tag_subscribe + '&form_type=' + form_type_subscribe + '&on=' + on + '&at=' + at + '&system_log_id=' + system_log_id + '&page_name=' + get_title + '&comments=' + comments + '';
                    apiCallV2(api_url);
                }
            }
            else {
                if (emailCheckV2(email) == true && nameCheckV2(first_name) == true) {
                    submitLoaderV2();
                    var allow_email = email.replace(/\+/g, "%2B");
                    var api_url = ''+baseurl_subscribe+'/api/web/v1/portable-form/register-contact?access-token=C6VlLzAnLJp-lM8O3v_7DBHH6I6mXql5&email=' + allow_email + '&first_name=' + first_name + '&last_name=' + last_name + '&phone=' + phone + '&event=' + event + '&url=' + page_url + '&media=' + utm_source_subscribe + '&development_id=' + development_id_subscribe + '&brand=' + brand_id_subscribe + '&realtor=' + realtor + '&development_name=' + development_name_subscribe + '&affiliate_tag=' + affiliate_tag_subscribe + '&form_type=' + form_type_subscribe + '&on=' + on + '&at=' + at + '&system_log_id=' + system_log_id + '&page_name=' + get_title + '&comments=' + comments + '';
                    apiCallV2(api_url);
                }
            }

        }
    }



};




/*Name-check function*/
    function nameCheckV2(firstname) {
        if(firstname=='') {return false;}
        else {return true;}
    }

    function phoneCheckV2(phone) {
        if(phone=='') {return false;}
        else {return true;}
    }

/*Email-check function*/
    function emailCheckV2(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

/*Set-cokkie function*/
    function setCookieV2(name, value, days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "") + expires + "; path=/";
    }

    function getCookieV2(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    }

/*Validaton-email and firstname*/
    function validateEmailV2(email) {
        //Validate email address
        if (emailCheckV2(email) == false) {
            document.getElementById("email" + form_id_subscribe).style.border = "1px solid red";
            if (email == '') {
                document.getElementById("email_error" + form_id_subscribe).innerText = "Email is required.";
            }
            else {
                document.getElementById("email_error" + form_id_subscribe).innerText = "Email is not valid.";
            }
        }
        else {
            document.getElementById("email" + form_id_subscribe).style.border = "1px solid green";
            document.getElementById("email_error" + form_id_subscribe).innerText = "";
        }
    }

    function validateFirstNameV2(first_name) {
        if (nameCheckV2(first_name) == false) {
            //first_name_error
            document.getElementById("first_name" + form_id_subscribe).style.border = "1px solid red";
            document.getElementById("first_name_error" + form_id_subscribe).innerText = "First name is required.";

        }
        else {
            document.getElementById("first_name" + form_id_subscribe).style.border = "1px solid green";
            document.getElementById("first_name_error" + form_id_subscribe).innerText = "";
        }
    }

    function validateFullNameV2(full_name) {
        if (nameCheckV2(full_name) == false) {
            //first_name_error
            document.getElementById("first_name" + form_id_subscribe).style.border = "1px solid red";
            document.getElementById("first_name_error" + form_id_subscribe).innerText = "Full name is required.";

        }
        else {
            document.getElementById("first_name" + form_id_subscribe).style.border = "1px solid green";
            document.getElementById("first_name_error" + form_id_subscribe).innerText = "";
        }
    }

    function validatePhoneNumberV2(phone) {
        if (phoneCheckV2(phone) == false) {
            //first_name_error
            document.getElementById("phone" + form_id_subscribe).style.border = "1px solid red";
            document.getElementById("phone_error" + form_id_subscribe).innerText = "Phone number is required.";
            document.getElementById("phone_error" + form_id_subscribe).style.color = "red";
        }
        else {
            document.getElementById("phone" + form_id_subscribe).style.border = "1px solid green";
            document.getElementById("phone_error" + form_id_subscribe).innerText = "";
        }
    }


    function apiCallV2(api_url) {
        const Http = new XMLHttpRequest();
        Http.open("GET", api_url);
        Http.send();
        Http.onreadystatechange = (e) =>
        {
            //document.getElementById("portable-form").innerHTML = '<div> <center><img style="height: 85px;padding: 170px 70px 1px 2px;" src="https://connectassetmgmt.connectcondos.com/backend/web/themes/connecterp/images/loader.gif"></center> </div>';
            if (redirect_subscribe == 0) { //0 for success message
                setTimeout(function () {
                    document.getElementById("portable-form" + form_id_subscribe).innerHTML = success_subscribe;
                }, 1000);
            } else {  //1 for success redirect_subscribe
                setTimeout(function () {
                    window.location.href = redirect_url_subscribe;
                }, 1000);
            }
        }
        ;
    }

    function submitLoaderV2() {
        document.getElementById('submit_loader' + form_id_subscribe).innerHTML = '<img style="margin: -5px auto;height: 22px;padding: 0px 0px 0px 3px;" src="https://connectassetmgmt.connectcondos.com/backend/web/themes/connecterp/images/loader.gif">';
        document.getElementById('Save' + form_id_subscribe).style.cursor = ' not-allowed';
    }

//Extra Function
    function formatDateV2(date) {
        var monthNames = [
            "January", "February", "March",
            "April", "May", "June", "July",
            "August", "September", "October",
            "November", "December"
        ];
        var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        var dayName = days[date.getDay()];
        var day = date.getDate();
        var monthIndex = date.getMonth();
        return dayName + ' ' + monthNames[monthIndex] + ' ' + day;
    }

    function make_privacy_policy_linkV2(privacy_link) {
        var link = '';
        if (privacy_link.indexOf('https://') > -1) {
            link = privacy_link;
        }
        else if (privacy_link.indexOf('http://') > -1) {
            link = privacy_link;
        } else {
            link = 'https://' + privacy_link;
        }
        return link;
    }
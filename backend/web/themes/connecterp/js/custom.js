var autoCompleteParentThis = '';
var autoCompleteAjaxUrl = '';
var autoCompleteHiddenField = '';
var autoCompleteInputType = '';
var cache = {};
var baseUrl='https://connectassetmgmt.com';

$(document).ready(function() {
    searchAutoComplete();
});
function searchAutoComplete() {
    $('.field-search-auto-complete').autocomplete({
        minLength: 3,
        open: function() {
            $('.ui-menu').width($('.ui-menu').width() + 30);
        },
        select: function( event, ui ) {
            if(typeof(autoCompleteHiddenField) != "undefined" && autoCompleteHiddenField !== null) {
                if($(autoCompleteHiddenField).length){
                    $(autoCompleteHiddenField).val(ui.item ? ui.item.id : "");
                }
            }

            if (autoCompleteInputType == 'select' && ui.item == null) {
                autoCompleteParentThis.val((ui.item ? ui.item.id : ""));
            }
        },
        change: function( event, ui ) {
            if(typeof(autoCompleteHiddenField) != "undefined" && autoCompleteHiddenField !== null) {
                if($(autoCompleteHiddenField).length){
                    $(autoCompleteHiddenField).val(ui.item ? ui.item.id : "");
                }
            }

            if (autoCompleteInputType == 'select' && ui.item == null) {
                autoCompleteParentThis.val((ui.item ? ui.item.id : ""));
            }
        },
        source: function( request, response ) {
            var term = request.term;
            if ( term in cache ) {
                response( cache[ term ] );
                return;
            }

            $.ajax({
                url: baseUrl + '/backend/web/' + autoCompleteAjaxUrl,
                type: 'POST',
                dataType: 'json',
                data:{
                    search: request.term,
                    _csrf : yii.getCsrfToken()
                },
                success: function (data) {
                    cache[ term ] = data;
                    response( data );
                }
            });
        },
        search: function () {
            autoCompleteParentThis = $(this);
            autoCompleteAjaxUrl = $(this).data('ajax-url');
            autoCompleteHiddenField = $(this).data('hidden-field');
            autoCompleteInputType = $(this).data('input-type');

            autoCompleteParentThis.parent().find('.fa').removeClass('fa-search');
            autoCompleteParentThis.parent().find('.fa').addClass('fa-spinner fa-spin');
        },
        response: function () {

            autoCompleteParentThis.parent().find('.fa').removeClass('fa-spinner');
            autoCompleteParentThis.parent().find('.fa').removeClass('fa-spin');
            autoCompleteParentThis.parent().find('.fa').addClass('fa-search');
        }
    }).data("ui-autocomplete")._renderItem = function(ul, item) {
        var newText = String(item.label).replace(new RegExp(this.term, "gi"), "<span class='highlight'>$&</span>");

        return $("<li></li>")
            .data("item.autocomplete", item)
            .append("<a>" + newText + "</a>")
            .appendTo(ul);
    };
}
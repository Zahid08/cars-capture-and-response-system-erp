<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'name' => 'CONNECT asset management',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'users' => [
            'class' => 'backend\modules\users\Users',
        ],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],

        'user' => [
            'identityClass' => 'unlock\modules\usermanager\user\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'connect-erp',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        //Starts : For New theme - abcrealestate - hooking
        'view' => [
            'theme' => [
            'pathMap' => [ 
                '@app/views' => [ 
                    '@webroot/themes/connecterp/views',
                ]
            ],
            ],
        ],

       /* 'assetManager' => [
            'bundles' => [
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [],
                    'js'=>[]
                ],
            ],
        ],*/

        //Ends : For New theme - abcrealestate - hooking
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */

        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/dynamic-drop-down/dynamic-drop-down/<slug>/index/' => '/dynamic-drop-down/dynamic-drop-down/index', // Listing
                '/dynamic-drop-down/dynamic-drop-down/<slug>/create' => '/dynamic-drop-down/dynamic-drop-down/create', // Create
                '/dynamic-drop-down/dynamic-drop-down/<slug>/update' => '/dynamic-drop-down/dynamic-drop-down/update', // Update
                '/dynamic-drop-down/dynamic-drop-down/<slug>/delete' => '/dynamic-drop-down/dynamic-drop-down/delete', // Delete
            ],
        ],
    ],
    'params' => $params,
    'defaultRoute' => '/user/default/login',
];

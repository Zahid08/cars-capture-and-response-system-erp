<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class ConnecterpAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'themes/connecterp/css/bootstrap.min.css',
        'themes/connecterp/css/fontawesome.min.css',
        'themes/connecterp/css/owl.carousel.css',
        'themes/connecterp/css/style.css?v=1.15',
        'themes/connecterp/css/datatables.min.css',
        'themes/connecterp/css/bootstrap-clockpicker.min.css',

    ];
    public $js = [
        'themes/connecterp/js/fontawesome.min.js',
        //'themes/connecterp/js/bootstrap.min.js',
        'themes/connecterp/js/owl.carousel.min.js',
        'themes/connecterp/js/owl.carousel.min.js',
        'themes/connecterp/js/popper.min.js',
        
        'themes/connecterp/js/vue.js',
        'themes/connecterp/js/vue-fullscreen.js',
        'themes/connecterp/js/vue-script.js',
        'themes/connecterp/js/main.js?tp=1.0',
        'themes/connecterp/js/custom.js',
        'themes/connecterp/js/datatables.min.js',
        'themes/connecterp/js/bootstrap-clockpicker.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}

<?php

namespace backend\modules\users\controllers;

use Yii;
use backend\modules\users\models\Users;
use backend\modules\users\models\UsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'view', 'create', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['logout', 'view', 'update', 'profile', 'changepassword'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Users();

        if ($model->load(Yii::$app->request->post())) {
            $model->setPassword($model->password);
            $model->generateAuthKey();
            $model->created_at = Yii::$app->user->id;
            $model->save(false);

            return $this->redirect(['view', 'id' => $model->id]);
        }

        try{
            $model->scenario = 'create';
            if ($model->load(Yii::$app->request->post())) {

                $model->setPassword($model->password);
                $model->generateAuthKey();
                $model->created_at = Yii::$app->user->id;
                //$model->save(false);

                /* if($model->role == 10){
                     $rolename = 'admin';
                     $admin = Yii::$app->authManager;

                     $getRoleName = $admin->getRolesByUser($model->id);
                     if(empty($getRoleName[$rolename])){
                         $adminRole = $admin->getRole($rolename);
                         $admin->assign($adminRole, $model->id);
                     }
                 }elseif ($model->role == 20){
                     $rolename = 'sales-manager';
                     $salesManager = Yii::$app->authManager;

                     $getRoleName = $salesManager->getRolesByUser($model->id);
                     if(empty($getRoleName[$rolename])) {
                         $salesManagerRole = $salesManager->getRole($rolename);
                         $salesManager->assign($salesManagerRole, $model->id);
                     }
                 }*/

                if($model->save(false)){
                    $emailcheck = Users::find()->where(['email' => $model->email])->one();
                    $usernamecheck = Users::find()->where(['username' => $model->username])->one();

                    if(!empty($emailcheck) && !empty($usernamecheck)){
                        Yii::$app->session->setFlash('failed', 'Username and Email Already Exit!!!');
                    }elseif (!empty($emailcheck)){
                        Yii::$app->session->setFlash('failed', 'Email Already Exit!!!');
                    }elseif(!empty($usernamecheck)){
                        Yii::$app->session->setFlash('failed', 'Username Already Exit!!!');
                    }else{
                        Yii::$app->session->setFlash('success', 'Successfully Create User!');

                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                }



            }
        }catch (Exception $e){

            $emailcheck = User::find()->where(['email' => $model->email])->one();
            $usernamecheck = User::find()->where(['username' => $model->username])->one();

            if(!empty($emailcheck) && !empty($usernamecheck)){
                Yii::$app->session->setFlash('failed', 'Username and Email Already Exit!!!');
            }elseif (!empty($emailcheck)){
                Yii::$app->session->setFlash('failed', 'Email Already Exit!!!');
            }elseif(!empty($usernamecheck)){
                Yii::$app->session->setFlash('failed', 'Username Already Exit!!!');
            }

            return $this->render('create', ['model' => $model]);
        }


        return $this->render('create', [
            'model' => $model,
        ]);
    }
    /**** Password Change ****/
    public function actionChangepassword($id){

        $model = $this->findModel($id);
        $model->scenario = 'changepassword';
        if ($model->load(Yii::$app->request->post())) {

            $model->password_hash = Yii::$app->security->generatePasswordHash($model->password);

            $model->updated_at = Yii::$app->user->id;

            $model->save(false);

            Yii::$app->session->setFlash('success', 'Password Changed!');

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('changepassword', [
            'model' => $model,
        ]);
    }
    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->updated_at = Yii::$app->user->id;
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

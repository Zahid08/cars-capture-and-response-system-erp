<?php

namespace backend\modules\users\models;

use Yii;

/**
 * This is the model class for table "conn_user".
 *
 * @property int $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class Users extends \yii\db\ActiveRecord
{
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';
    const SCENARIO_CHANGEPASSWORD = 'changepassword';

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $password;
    public $confirm_password;


    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = ['first_name', 'last_name', 'phone', 'username', 'email', 'status', 'role', 'password', 'confirm_password'];
        $scenarios[self::SCENARIO_UPDATE] = ['first_name', 'last_name', 'phone', 'username', 'email', 'status', 'role'];
        $scenarios[self::SCENARIO_CHANGEPASSWORD] = ['password', 'confirm_password'];
        return $scenarios;
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'conn_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            // Required Field Create
            [['first_name', 'last_name', 'phone', 'username', 'email', 'mobile', 'department', 'start_date', 'create_date', 'end_date', 'status', 'role', 'password', 'confirm_password'], 'required', 'on' => self::SCENARIO_CREATE],

            // Required Field Update
            [['first_name', 'last_name', 'phone', 'username', 'email', 'mobile', 'department', 'start_date', 'create_date', 'end_date', 'status', 'role'], 'required', 'on' => self::SCENARIO_UPDATE],

            // Required Field Change Password
            [['password', 'confirm_password'], 'required', 'on' => self::SCENARIO_CHANGEPASSWORD],


            ['username', 'string', 'min' => 2, 'max' => 20],

            ['password', 'string', 'min' => 6],

            ['confirm_password', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ],

            [['status', 'created_at', 'updated_at'], 'integer'],
            [['first_name', 'last_name'], 'required'],
            [['first_name', 'last_name', 'mobile', 'start_date', 'create_date', 'end_date', 'role'], 'safe'],
            [['password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'title' => 'Title',
            'department' => 'Department',
            'status' => 'Status',
            'start_date' => 'Start Date',
            'create_date' => 'Create Date',
            'end_date' => 'End Date',
            'role' => 'Role',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
}

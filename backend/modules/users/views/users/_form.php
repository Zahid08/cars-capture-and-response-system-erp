<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\modules\users\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'first_name')->textInput() ?>

    <?= $form->field($model, 'last_name')->textInput() ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'confirm_password')->passwordInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mobile')->textInput() ?>

    <?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'department')->dropDownList(['Admin' => 'Admin', 'Sales' => 'Sales', 'Marketing' => 'Marketing'],['prompt'=>'Select Department']); ?>

    <?= $form->field($model, 'start_date')->widget(
        DatePicker::className(), [
        'name' => 'start_date',
        'value' => date('yyyy-m-d', strtotime('+2 days')),
        'options' => ['placeholder' => 'Select start date ...'],
        'pluginOptions' => [
            'format' => 'yyyy-m-d',
            'todayHighlight' => true
        ]
    ]);?>

    <?= $form->field($model, 'create_date')->widget(
        DatePicker::className(), [
        'name' => 'create_date',
        'value' => date('yyyy-m-d', strtotime('+2 days')),
        'options' => ['placeholder' => 'Select start date ...'],
        'pluginOptions' => [
            'format' => 'yyyy-m-d',
            'todayHighlight' => true
        ]
    ]);?>

    <?= $form->field($model, 'end_date')->widget(
        DatePicker::className(), [
        'name' => 'end_date',
        'value' => date('yyyy-m-d', strtotime('+2 days')),
        'options' => ['placeholder' => 'Select start date ...'],
        'pluginOptions' => [
            'format' => 'yyyy-m-d',
            'todayHighlight' => true
        ]
    ]);?>


    <?= $form->field($model, 'role')->dropDownList(['1' => 'Leader', '2' => 'Manager', '3' => 'Technician'],['prompt'=>'Select Role']); ?>

    <?= $form->field($model, 'status')->dropDownList(['10' => 'Active', '20' => 'Inactive'],['prompt'=>'Select Status']); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

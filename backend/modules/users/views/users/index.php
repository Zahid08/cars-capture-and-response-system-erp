<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\users\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users List';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <h2 style="text-align: center"><?= Html::encode($this->title) ?></h2>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'first_name',
            'last_name',
            'username',
            'email:email',
            'mobile',
            //'title',
            'department',
            [
                'attribute' => 'role',
                'value' => function($model, $key, $index, $column){
                    if($model->role == '1'){
                        return 'Leader';
                    }elseif($model->role == '2'){
                        return 'Manager';
                    }elseif($model->role == '3'){
                        return 'Technician';
                    }
                },
                'filter'=> '',
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:7%'],
            ],
            [
                'attribute' => 'status',
                'value' => function($model, $key, $index, $column){
                    if($model->status == '10'){
                        return 'Active';
                    }else{
                        return 'Inactive';
                    }
                },
                'filter'=> '',
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:7%'],
            ],
            //'created_at',
            //'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'headerOptions' => ['style' => 'color:#337ab7;width:120px'],
            ],
        ],
    ]); ?>
</div>

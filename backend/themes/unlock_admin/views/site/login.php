<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
if(!empty($errMsg)) echo '<div class="alert alert-danger">'.$errMsg.'</div>';
?>
 <?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'layout' => 'horizontal',  //  'default', 'horizontal' or 'inline'
    'validateOnBlur' =>true,
    //'errorCssClass'=>'has-error',    
    'fieldConfig' => [
        //http://www.yiiframework.com/doc-2.0/yii-bootstrap-activefield.html
        //'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-7\">{error}</div>",
        'template' => "<div class=\"input-group\"><span class=\"input-group-addon\"> </span> \n{input} \n </div><div class=\"err\"> {error}</div>",
        'options' => ['class' => 'input-group-'],        
        'horizontalCssClasses' => [
            'label' => '',
            'offset' => '',
            'wrapper' => '',            
            'hint' => '',
            ],
        ],
    ]);  ?>
    
    <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
    <?= $form->field($model, 'password')->passwordInput() ?>
    <?= $form->field($model, 'rememberMe')->checkbox() ?>
    <div class="row">
        <div class="col-xs-12">                       
            <?= Html::submitButton('Login', ['class' => 'btn btn-success col-xs-12', 'name' => 'login-button']) ?>
        </div>
    </div>         
 <?php ActiveForm::end(); ?>   


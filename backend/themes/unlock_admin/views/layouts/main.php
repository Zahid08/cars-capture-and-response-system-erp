<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\web\Session;
use backend\themes\unlock_admin\components\HeaderPart;
use backend\themes\unlock_admin\components\MainNavigation;
use backend\themes\unlock_admin\assets\AdminThemeAsset;

AdminThemeAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo Yii::getAlias('@baseUrl').'/media/setting/' . Yii::$app->setting->favicon_icon; ?>" type="image/x-icon">
    <link rel="icon" href="<?php echo Yii::getAlias('@baseUrl').'/media/setting/' . Yii::$app->setting->favicon_icon; ?>" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
    <script type="text/javascript"> var baseUrl = '<?php echo Yii::getAlias('@baseUrl'); ?>'; </script>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="fixed-header fixed-leftmenu">
<?php $this->beginBody() ?>

<div id="theme-wrapper">
    <header class="header" role="header">
        <div class="header-top">
            <?= HeaderPart::widget() ?>
        </div>
        <nav class="main-menu main-menu-horizontal" role="navigation">
            <?= MainNavigation::widget() ?>
        </nav>
        <div style="clear: both"></div>
        <nav class="sub-menu-ribbon-tab" role="ribbon-tab">&nbsp;</nav>
        <div style="clear: both"></div>
        <div class="header-bottom-border"></div>
    </header>

    <main role="main" class="main clearfix">
        <div id="page-wrapper" class="container">
            <div class="row">
                <div id="content-wrapper">
                    <?= Alert::widget() ?>
                    <?= $content;?>
                </div>
            </div>
        </div>
    </main>

    <footer id="footer" role="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p id="footer-copyright" class="col-xs-12">
                        <?= Yii::$app->setting->copyright; ?><br>
                        Developed by : <a href='http://www.unlocklive.com' target="_blank"> Unlocklive. </a>
                    </p>
                </div>
            </div>
        </div>
    </footer>

</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

<script type="text/javascript">
    $(document).ready(function() {
        if($('.main-menu-horizontal ul > li.active > ul.submenu').length){
            var subMenu = $('.main-menu-horizontal ul > li.active > ul.submenu').html();

            var subMenuHtml = '<ul class="submenu">';
            subMenuHtml += subMenu;
            subMenuHtml += '</ul>';

            $('.sub-menu-ribbon-tab').html(subMenuHtml);
        }

        $('.main-menu-horizontal ul > li > a.dropdown-toggle').click(function (event) {

            var url = $(this).attr('href');

            $('.main-menu-horizontal ul > li').removeClass('active');
            $(this).parent().addClass('active');

            if (url.indexOf("#") != -1)
            {
                event.preventDefault();

                var subMenu = $(this).next('ul.submenu').html();

                var subMenuHtml = '<ul class="submenu">';
                subMenuHtml += subMenu;
                subMenuHtml += '</ul>';

                $('.sub-menu-ribbon-tab').html(subMenuHtml);
            }

        });

    });
</script>

<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\web\Session;
use backend\themes\unlock_admin\components\HeaderPart;
use backend\themes\unlock_admin\components\LeftMenu;
use backend\themes\unlock_admin\assets\AdminLoginThemeAsset;

AdminLoginThemeAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo Yii::getAlias('@baseUrl').'/media/setting/' . Yii::$app->setting->favicon_icon; ?>" type="image/x-icon">
    <link rel="icon" href="<?php echo Yii::getAlias('@baseUrl').'/media/setting/' . Yii::$app->setting->favicon_icon; ?>" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="login-page admin-login-page">
<?php $this->beginBody() ?>

<div id="theme-wrapper">
    <div id="content-wrapper">
        <?= $content;?>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

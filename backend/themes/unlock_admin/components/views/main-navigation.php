<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Menu;
use backend\themes\unlock_admin\components\MainNavigation;
?>

<?php
echo  Menu::widget([
        'items' => [
            [
                'label' => '<span>'.Yii::t('app', 'Home').'</span>',
                'url' => ['/site/index'],
                'visible'=> Yii::$app->user->checkAccess('backend/site', 'index'),
                'options'=>['class'=>''],
            ],
            [
                'label' => '<span>' . Yii::t('app', 'Organization') . '</span>',
                'url' => ['/organization/organization/index'],
                'visible'=> Yii::$app->user->checkAccess('organization/organization', 'index'),
                'template' => '<a class="dropdown-toggle" href="{url}" >{label}</a>',
                'items' => [
                    [
                        'label' => '<i class="fa fa-check-circle"></i> <span>'.Yii::t('app', 'Organization').'</span>',
                        'url' => ['/organization/organization/index'],
                        'visible'=> Yii::$app->user->checkAccess('organization/organization', 'index'),
                        'active' => MainNavigation::isActive(['organization/organization/']),
                    ],
                ],
            ],
            [
                'label' => '<span>'.Yii::t('app', 'Users').'</span>',
                'url' => ['/user/admin/index'],
                'visible'=> Yii::$app->user->checkAccess('user/admin', 'index') ||
                    Yii::$app->user->checkAccess('role/role', 'index') ||
                    Yii::$app->user->checkAccess('permission/accesscontrol', 'assign-permission'),
                'template' => '<a class="dropdown-toggle" href="{url}" >{label}</a>',
                'items' => [
                    [
                        'label' => '<i class="fa fa-check-circle"></i> <span>'.Yii::t('app', 'Users').'</span>',
                        'url' => ['/user/admin/index'],
                        'visible'=> Yii::$app->user->checkAccess('user/admin', 'index'),
                        'active' => MainNavigation::isActive(['user/admin/']),
                    ],
                    [
                        'label' => '<i class="fa fa-check-circle"></i> <span>'.Yii::t('app', 'Roles').'</span>',
                        'url' => ['/role/role/index'],
                        'visible'=> Yii::$app->user->checkAccess('role/role', 'index'),
                        'active' => MainNavigation::isActive(['role/role/']),
                    ],
                    [
                        'label' => '<i class="fa fa-check-circle"></i> <span>'.Yii::t('app', 'Permissions').'</span>',
                        'url' => ['/permission/access-control/assign-permission'],
                        'visible'=> Yii::$app->user->checkAccess('permission/accesscontrol', 'assign-permission'),
                        'active' => MainNavigation::isActive(['permission/access-control/']),
                    ],
                ],
            ],
            [
                'label' => '<span>'.Yii::t('app', 'Settings').'</span>',
                'url' => ['/setting/backend-setting/general'],
                'visible'=> Yii::$app->user->checkAccess('setting/backendsetting', 'general') ||
                    Yii::$app->user->checkAccess('dynamicdropdown/dynamicdropdown', 'index') ||
                    Yii::$app->user->checkAccess('location/upazila', 'index') ||
                    Yii::$app->user->checkAccess('translationmanager/default', 'index'),
                'template' => '<a class="dropdown-toggle" href="{url}" >{label}</a>',
                'items' => [
                    [
                        'label' => '<i class="fa fa-check-circle"></i> <span>'.Yii::t('app', 'Configuration').'</span>',
                        'url' => ['/setting/backend-setting/general'],
                        'visible'=> Yii::$app->user->checkAccess('setting/backendsetting', 'general'),
                        'active' => MainNavigation::isActive(['setting/backend-setting/general']),
                    ],
                    [
                        'label' => '<i class="fa fa-check-circle"></i> <span>'.Yii::t('app', 'Translations').'</span>',
                        'url' => ['/translate-manager/default/index'],
                        'visible'=> Yii::$app->user->checkAccess('translationmanager/default', 'index'),
                        'active' => MainNavigation::isActive(['translate-manager/default/']),
                    ],
                    [
                        'label' => '<i class="fa fa-check-circle"></i> <span>'.Yii::t('app', 'Upazila / Thana').'</span>',
                        'url' => ['/location/upazila/index'],
                        'visible'=> Yii::$app->user->checkAccess('location/upazila', 'index'),
                        'active' => MainNavigation::isActive(['location/upazila/']),
                    ],
                    [
                        'label' => '<i class="fa fa-check-circle"></i> <span>'.Yii::t('app', 'Manage Drop Down').'</span>',
                        'url' => ['/dynamic-drop-down/dynamic-drop-down/index'],
                        'visible'=> Yii::$app->user->checkAccess('dynamicdropdown/dynamicdropdown', 'index'),
                        'active' => MainNavigation::isActive(['dynamic-drop-down/dynamic-drop-down/']),
                    ],
                ],
            ],
        ],
        'submenuTemplate' => "\n<ul class='submenu'>\n{items}\n</ul>\n",
        'encodeLabels' => false,
        'activateParents' => true,
        'options' => ['class'=>'nav nav-menu'],
        'firstItemCssClass' => 'menu-first-item',
        'lastItemCssClass' => 'menu-last-item',
    ]
);

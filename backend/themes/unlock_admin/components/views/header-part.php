<?php
use yii\helpers\Html;
use yii\helpers\Url;
use unlock\modules\core\helpers\CommonHelper;

$url = Url::to(['/site/index']);

if(Yii::$app->controller->action->id != 'account' && (isset(Yii::$app->user->identity->force_password_change) && Yii::$app->user->identity->force_password_change == 1)){
    Yii::$app->session->setFlash('danger', Yii::t('app', 'You need to update your password because this is the first time you are signing in, or because your password has expired.'));
    Yii::$app->response->redirect(Url::to(['/user/default/account']));
}

?>

<style type="text/css">
    .header-top .navbar-nav > li > a.badge-language{
        margin: 6px 5px 0 0;
        padding: 5px 10px 3px 10px;
        line-height: 14px;
        background: #c8c8c8;
        color: #444444;
    }
</style>

<div class="company-logo-block">
    <?= Html::img(Yii::getAlias('@baseUrl') . '/media/setting/' . Yii::$app->setting->company_logo, ['class' => 'company-logo', 'alt' => Yii::$app->setting->company_logo_alt]);?>
    <label><?= Yii::$app->setting->company_logo_text ?></label>
</div>

<div class="nav-no-collapse pull-right" id="header-nav">
    <ul class="nav navbar-nav pull-right" style="margin-left: 0">
        <?php if(Yii::$app->user->checkUrlPermission(Url::toRoute(['/translate-manager/default/change-language']))): ?>
            <li class="switch-language-english">
                <?= Html::a(Yii::t('app', 'Language English'), ['/translate-manager/default/change-language'],['class' => 'badge badge-language'])?>
            </li>
        <?php endif;?>
        <?php if(Yii::$app->user->checkAccess('permission/accesscontrol', 'reload-permission')): ?>
            <li class="dropdown permission-dropdown">
                <a href="<?= Url::to(['/user/view-profile'])?>" class="dropdown-toggle" data-toggle="dropdown">
                    <span>Permission</span> <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <?= Html::a('Reload Permission',['/permission/access-control/reload-permission'],['title'=>'Reload Permission'])?>
                    </li>
                    <li>
                        <?= Html::a('Reload Session',['/permission/access-control/reload-permission-session'], ['title'=> 'Reload Session'])?>
                    </li>
                </ul>
            </li>
        <?php endif;?>
        <li class="dropdown profile-dropdown">
            <a href="<?= Url::to(['/user/view-profile'])?>" class="dropdown-toggle" data-toggle="dropdown">
                <span class="hidden-xs"><?php echo !empty(CommonHelper::getLoggedInUserName()) ? Yii::$app->user->identity->name . ' ['. CommonHelper::getLoggedInUserName() . ']' : 'Guest'?></span> <b class="caret"></b>
            </a>
            <ul class="dropdown-menu dropdown-menu-right">
                <li><?= Html::a('<i class="fa fa-cog"></i>Profile',['/user/view-profile'],['data-method' => 'post', 'data-pjax' => '0'])?></li>
                <?php if(Yii::$app->user->checkUrlPermission(Url::toRoute(['/signature/signature/my-signature']))) { ?>
                <li><?= Html::a('<i class="fa fa-cog"></i>My Signature',['/signature/signature/my-signature'],['data-method' => 'post', 'data-pjax' => '0'])?></li>
                <?php } ?>
                <li><?= Html::a('<i class="fa fa-cog"></i>Change Password',['/user/account'],['data-method' => 'post', 'data-pjax' => '0'])?></li>
            </ul>
        </li>
        <li class="top-log-out-btn"><?= Html::a('<i class="fa fa-power-off"></i> Logout',['/user/logout'],['data-method' => 'post', 'data-pjax' => '0'])?></li>
    </ul>
</div>
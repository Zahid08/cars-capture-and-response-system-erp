<?php
namespace backend\themes\unlock_admin\components;

use Yii;
use yii\base\Widget;

class HeaderPart extends Widget
{
    public $messageCount = 0;

    public $messages = [];

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('header-part');
    }
}
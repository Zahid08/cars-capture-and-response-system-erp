<?php
namespace backend\themes\unlock_admin\components;

use Yii;
use yii\base\Widget;
use yii\helpers\Url;

class MainNavigation extends Widget
{
	public $message;

	public function init()
	{
		// your logic here
		parent::init();		
	}

	public function run()
	{
	    // you can load & return the view or you can return the output variable
		return $this->render('main-navigation',[]);
	}

    public static function isActive($routes = [])
    {
        if (Yii::$app->requestedRoute == "" && count($routes) == 0){
            return false;
        }

        $currentUrl = Url::current();
        $routeCurrent = preg_split('@web/@', $currentUrl);
        $routeCurrent = isset($routeCurrent[1]) ? $routeCurrent[1] : '';

        foreach ($routes as $route) {
            $pattern = sprintf('~%s~', preg_quote($route));
            if (preg_match($pattern, $routeCurrent)) {
                return true;
            }
        }

        return false;
    }
}

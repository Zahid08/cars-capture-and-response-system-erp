<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\themes\unlock_admin\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AdminLoginThemeAsset extends AssetBundle
{
    public $basePath = '@webroot';

    public $baseUrl = '@web';

    public $css = [
        'unlock_admin/css/font-awesome.min.css',
        'unlock_admin/css/theme_reset_styles.css',
        'unlock_admin/css/login_styles.css',
    ];

    public $js = [
        'unlock_admin/js/jquery.nanoscroller.min.js',
        'unlock_admin/js/scripts.js',
        'unlock_admin/js/custom.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

}

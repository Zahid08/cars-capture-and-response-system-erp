<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\web\Session;
use backend\themes\unlock_admin\components\HeaderPart;
use backend\themes\unlock_admin\components\MainNavigation;
use backend\themes\unlock_admin_vnav\assets\AdminVnavThemeAsset;

AdminVnavThemeAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo Yii::getAlias('@baseUrl').'/media/setting/' . Yii::$app->setting->favicon_icon; ?>" type="image/x-icon">
    <link rel="icon" href="<?php echo Yii::getAlias('@baseUrl').'/media/setting/' . Yii::$app->setting->favicon_icon; ?>" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
    <script type="text/javascript"> var baseUrl = '<?php echo Yii::getAlias('@baseUrl'); ?>'; </script>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="fixed-header fixed-leftmenu">
<?php $this->beginBody() ?>

<div id="theme-wrapper">
    <header class="navbar" id="header-navbar">
        <?= HeaderPart::widget() ?>
    </header>
    <div id="page-wrapper" class="container">
        <div class="row">
            <?= MainNavigation::widget() ?>
            <div id="content-wrapper">
                <?= $content;?>
                <footer id="footer-bar" class="row">
                    <p id="footer-copyright" class="col-xs-12">
                        Version : <?=Yii::getVersion();?>, Powered by : <a href='http://www.unlocklive.com' target="_blank"> UnLockLive </a>
                    </p>
                </footer>
            </div>
        </div>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

<script type="text/javascript">
    $(document).ready(function() {
        if($('.main-menu-horizontal ul > li.active > ul.submenu').length){
            var subMenu = $('.main-menu-horizontal ul > li.active > ul.submenu').html();

            var subMenuHtml = '<ul class="submenu">';
            subMenuHtml += subMenu;
            subMenuHtml += '</ul>';

            $('.sub-menu-ribbon-tab').html(subMenuHtml);
        }

        $('.main-menu-horizontal ul > li > a.dropdown-toggle').click(function (event) {

            var url = $(this).attr('href');

            $('.main-menu-horizontal ul > li').removeClass('active');
            $(this).parent().addClass('active');

            if (url.indexOf("#") != -1)
            {
                event.preventDefault();

                var subMenu = $(this).next('ul.submenu').html();

                var subMenuHtml = '<ul class="submenu">';
                subMenuHtml += subMenu;
                subMenuHtml += '</ul>';

                $('.sub-menu-ribbon-tab').html(subMenuHtml);
            }

        });

    });
</script>

<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\themes\unlock_admin_vnav\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AdminVnavThemeAsset extends AssetBundle
{
    public $basePath = '@webroot';

    public $baseUrl = '@web';

    public $css = [
        'unlock_admin/css/font-awesome.min.css',
        'unlock_admin/css/nanoscroller.css',
        'unlock_admin/css/theme_reset_styles.css',
        'unlock_admin/css/theme_styles.css',
        'unlock_admin/css/vertical_navigation.css',
        'unlock_admin/css/theme_responsive_styles.css',
    ];

    public $js = [
        'unlock_admin/js/jquery.nanoscroller.min.js',
        'unlock_admin/js/scripts.js',
        'unlock_admin/js/custom.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

}

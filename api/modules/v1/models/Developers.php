<?php

namespace api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;
use api\models\User;

/**
 * This is the model class for table "{{%sample}}".
 *
 * @property string $id
 * @property string $packageId
 * @property string $orgIdPrefix
 * @property string $name
 * @property string $domain
 * @property string $email
 * @property string $favicon
 * @property string $logo
 * @property string $theme
 * @property string $skin
 * @property string $detailsinfo
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class Developers extends \yii\db\ActiveRecord
{
    public $connect_url;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%developers}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['developer_grade', 'province', 'created_by', 'updated_by', 'status'], 'integer'],
            [['excerpt_text', 'overview_text', 'description_text'], 'string'],
            [['developer_name', 'developer_grade', 'phone', 'status'], 'required'],

            [['created_at', 'updated_at'], 'safe'],
            [['developer_name', 'phone', 'website_url', 'logo_image', 'lifestyle_image'], 'string', 'max' => 100],
            [['address'], 'string', 'max' => 255],
            [['city', 'suite'], 'string', 'max' => 50],
            [['postal'], 'string', 'max' => 30],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'developer_name' => 'Developer Name',
            'developer_grade' => 'Developer Grade',
            'phone' => 'Phone',
            'address' => 'Address',
            'city' => 'City',
            'suite' => 'Suite',
            'postal' => 'Postal',
            'province' => 'Province',
            'website_url' => 'Website Url',
            'logo_image' => 'Logo Image',
            'lifestyle_image' => 'Lifestyle Image',
            'excerpt_text' => 'Excerpt Text',
            'overview_text' => 'Overview Text',
            'description_text' => 'Description Text',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'status' => 'Status',
        ];
    }


    public function fields()
    {
        $fields = parent::fields();

        $additionalFields = [
            /*'userInfo' => function ($model) {
                return ArrayHelper::getValue($model, 'crBy0');
            },*/
        ];

        $result = ArrayHelper::merge($fields, $additionalFields);

        return $result;
    }

    public function extraFields()
    {
        return [
            /*'userInfo' => function ($model) {
                return ArrayHelper::getValue($model, 'crBy0');
            },*/
        ];
    }
}

<?php

namespace api\modules\v1\models;

use api\models\User;
use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%assignments_units}}".
 */
class AssignmentsUnits extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%assignments_units}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['assignment_id', 'suite_number', 'beds_id', 'baths', 'balcony', 'balcony_size', 'parking', 'parking_total', 'locker', 'created_by', 'updated_by'], 'safe'],
            [['created_at', 'updated_at'], 'safe'],
            [['sqft', 'exposure'], 'safe'],
            [['notes'], 'safe'],
            [['beds_id','baths','sqft'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'assignment_id' => Yii::t('app', 'Assignment ID'),
            'suite_number' => Yii::t('app', 'Suite Number'),
            'beds_id' => Yii::t('app', 'Beds'),
            'baths' => Yii::t('app', 'Baths'),
            'balcony' => Yii::t('app', 'Balcony'),
            'balcony_size' => Yii::t('app', 'Balcony Size'),
            'parking' => Yii::t('app', 'Parking'),
            'parking_total' => Yii::t('app', 'Parking Total'),
            'locker' => Yii::t('app', 'Locker'),
            'sqft' => Yii::t('app', 'Sqft'),
            'exposure' => Yii::t('app', 'Exposure'),
            'notes' => Yii::t('app', 'Notes'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }

    public function fields()
    {
        $fields = parent::fields();

        $additionalFields = [
            /*'userInfo' => function ($model) {
                return ArrayHelper::getValue($model, 'crBy0');
            },*/
        ];

        $result = ArrayHelper::merge($fields, $additionalFields);

        return $result;
    }

    public function extraFields()
    {
        return [
            /*'userInfo' => function ($model) {
                return ArrayHelper::getValue($model, 'crBy0');
            },*/
        ];
    }
}

<?php

namespace api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%sample}}".
 *
 * @property string $id
 * @property string $packageId
 * @property string $orgIdPrefix
 * @property string $name
 * @property string $domain
 * @property string $email
 * @property string $favicon
 * @property string $logo
 * @property string $theme
 * @property string $skin
 * @property string $detailsinfo
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class Contacts extends \yii\db\ActiveRecord
{
    // Contact Type
    /*public $contact_type;*/
    // Registration Contact
    public $development_id;
    public $reg_source;
    public $reg_date;

    // Tags
    public $tag_name;
    public $applied_date;

    // System Information
    public $page_name;
    public $page_url;
    public $ip_address;
    public $browser;
    public $operating;
    public $visit_date;

    public $realtor;
    public  $brand_name;
    public  $comments;
    public  $learn_cnt;

    public  $interaction_note;
    public  $interaction_type;
    public  $intended_nature_purchase,$do_require_locker,$ffp_choice_no_1,$ffp_choice_no_2,$ffp_choice_no_3,$desired_floor_range,$do_require_parking;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%contacts}}';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contact_type','learn_cnt','comments','intended_nature_purchase','do_require_locker','do_require_parking','desired_floor_range','ffp_choice_no_3','ffp_choice_no_2','ffp_choice_no_1','interaction_type','interaction_note','brand_name','development_id', 'reg_date', 'reg_source', 'tag_name', 'applied_date', 'page_name', 'page_url', 'ip_address', 'browser', 'operating', 'visit_date', 'realtor', 'suite','date_of_birth', 'SIN', 'created_at', 'updated_at'], 'safe'],
            [['lead_source', 'status', 'created_by', 'updated_by'], 'integer'],
            [['email'], 'required'],
            //[['email'], 'unique'],
            [['first_name', 'last_name', 'phone'], 'string', 'max' => 80],
            [['land_line', 'profession', 'email', 'social_insurance_number', 'drivers_license'], 'string', 'max' => 100],
            [['address'], 'string', 'max' => 255],
            [['city', 'province', 'country', 'agent'], 'string', 'max' => 50],
            [['postal'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'address' => Yii::t('app', 'Address'),
            'city' => Yii::t('app', 'City'),
            'province' => Yii::t('app', 'Province'),
            'postal' => Yii::t('app', 'Postal'),
            'country' => Yii::t('app', 'Country'),
            'agent' => Yii::t('app', 'Agent'),
            'social_insurance_number' => Yii::t('app', 'Social Insurance Number'),
            'drivers_license' => Yii::t('app', 'Drivers License'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'date_of_birth' => Yii::t('app', 'Date of Birth'),
            'SIN' => Yii::t('app', 'S.I.N'),
            'lead_source' => Yii::t('app', 'Media Source'),
            'lead_score' => Yii::t('app', 'Lead Score'),
            'status' => Yii::t('app', 'Status'),
            'land_line' => Yii::t('app', 'Land Line'),
            'profession' => Yii::t('app', 'Profession'),
            'contact_type' => Yii::t('app', 'Contact Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = 39;
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }

    public function fields()
    {
        $fields = parent::fields();

        $additionalFields = [
            /*'userInfo' => function ($model) {
                return ArrayHelper::getValue($model, 'crBy0');
            },*/
        ];

        $result = ArrayHelper::merge($fields, $additionalFields);

        return $result;
    }

    public function extraFields()
    {
        return [
            /*'userInfo' => function ($model) {
                return ArrayHelper::getValue($model, 'crBy0');
            },*/
        ];
    }
}

<?php

namespace api\modules\v1\models;

use api\models\User;
use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%sample}}".
 *
 * @property string $id
 * @property string $packageId
 * @property string $orgIdPrefix
 * @property string $name
 * @property string $domain
 * @property string $email
 * @property string $favicon
 * @property string $logo
 * @property string $theme
 * @property string $skin
 * @property string $detailsinfo
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class ContactsEmailLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%contacts_email_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contact_id', 'user_id', 'status'], 'safe'],
            [['created_at', 'updated_at'],['sent_date_time'], 'safe'],
            [['subject', 'mail_header'], 'safe'],
            [['mail_body'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Log ID'),
            'contact_id' => Yii::t('app', 'Contact ID'),
            'user_id' => Yii::t('app', 'Recipient '),
            'subject' => Yii::t('app', 'Subject'),
            'mail_header' => Yii::t('app', 'Mail Header'),
            'mail_body' => Yii::t('app', 'Mail Body'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'sent_date_time' => Yii::t('app', 'Sent Date'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        //return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
       // return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                //$this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
               // $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }

    public function fields()
    {
        $fields = parent::fields();

        $additionalFields = [
            /*'userInfo' => function ($model) {
                return ArrayHelper::getValue($model, 'crBy0');
            },*/
        ];

        $result = ArrayHelper::merge($fields, $additionalFields);

        return $result;
    }

    public function extraFields()
    {
        return [
            /*'userInfo' => function ($model) {
                return ArrayHelper::getValue($model, 'crBy0');
            },*/
        ];
    }
}

<?php

namespace api\modules\v1\models;

use api\models\User;
use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%assignments_assets}}".
 */
class AssignmentsAssets extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    /**
     * {@inheritdoc}
     */
    public $option;
    public static function tableName()
    {
        return '{{%assignments_assets}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['assignment_id', 'created_by', 'updated_by'], 'safe'],
            [['option','assignment_assets'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['assignment_assets', 'upload_url', 'upload_file'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'assignment_id' => Yii::t('app', 'Assignment ID'),
            'assignment_assets' => Yii::t('app', 'Assets-1'),
            'upload_url' => Yii::t('app', 'Upload Url'),
            'upload_file' => Yii::t('app', 'Upload File'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'option' => Yii::t('app', 'Upload'),
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }

    public function fields()
    {
        $fields = parent::fields();

        $additionalFields = [
            /*'userInfo' => function ($model) {
                return ArrayHelper::getValue($model, 'crBy0');
            },*/
        ];

        $result = ArrayHelper::merge($fields, $additionalFields);

        return $result;
    }

    public function extraFields()
    {
        return [
            /*'userInfo' => function ($model) {
                return ArrayHelper::getValue($model, 'crBy0');
            },*/
        ];
    }
}

<?php

namespace api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;
use unlock\modules\core\helpers\CommonHelper;

/**
 * This is the model class for table "{{%contacts_systeminfo}}".
 *
 * @property integer $id
 * @property integer $contact_id
 * @property string $page_name
 * @property string $page_url
 * @property string $log_note
 * @property string $ip_address
 * @property string $browser
 * @property string $operator
 * @property string $visit_date
 *
 * @property Contacts $contact
 */
class ContactsSysteminfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%contacts_systeminfo}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contact_id', 'brand_id', 'development_id'], 'safe'],
            [['log_date', 'visit_date'], 'safe'],
            [['log_note'], 'string'],
            [['first_name', 'phone','realtor'],'safe'],
            [['last_name'], 'safe'],
            [['email', 'media_source', 'affiliate_tag'], 'safe'],
            [['development_name', 'page_name', 'page_url','comment'], 'safe'],
            [['event_on', 'event_at', 'event'], 'safe'],
            [['contact_type', 'form_type', 'ip_address','device'], 'safe'],
            [['browser', 'operating'], 'safe'],
            [['log_data','log_create_status'], 'safe'],
            [['contact_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contacts::className(), 'targetAttribute' => ['contact_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'contact_id' => Yii::t('app', 'Contact ID'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'brand_id' => Yii::t('app', 'Brand ID'),
            'development_id' => Yii::t('app', 'Development ID'),
            'development_name' => Yii::t('app', 'Development Name'),
            'media_source' => Yii::t('app', 'Media Source'),
            'event_on' => Yii::t('app', 'Event On'),
            'event_at' => Yii::t('app', 'Event At'),
            'event' => Yii::t('app', 'Event'),
            'affiliate_tag' => Yii::t('app', 'Affiliate Tag'),
            'log_date' => Yii::t('app', 'Log Date'),
            'contact_type' => Yii::t('app', 'Contact Type'),
            'form_type' => Yii::t('app', 'Form Type'),
            'page_name' => Yii::t('app', 'Page Name'),
            'page_url' => Yii::t('app', 'Page Url'),
            'log_note' => Yii::t('app', 'Log Note'),
            'ip_address' => Yii::t('app', 'Ip Address'),
            'browser' => Yii::t('app', 'Browser'),
            'operating' => Yii::t('app', 'Operating'),
            'device ' => Yii::t('app', 'Device'),
            'visit_date' => Yii::t('app', 'Visit Date'),
            'realtor' => Yii::t('app', 'Realtor'),
            'log_create_status' => Yii::t('app', 'Status'),
            'log_data' => Yii::t('app', 'Log Data'),
            'comment' => Yii::t('app', 'Comments'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(Contacts::className(), ['id' => 'contact_id']);
    }

    /** Get All Active ContactsSysteminfo Drop Down List*/
    public static function contactsSysteminfoDropDownList($status = CommonHelper::STATUS_ACTIVE)
    {
        $model = self::find()
            ->where('status =:status', [':status' => $status])
            ->all();

        return  ArrayHelper::map($model, 'id', 'title');
    }

    /**
    * before save data
    */
   /* public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }*/
}

<?php

namespace api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;
use api\models\User;

/**
 * This is the model class for table "{{%developments}}".
 *
 * @property string $id
 * @property string $packageId
 * @property string $orgIdPrefix
 * @property string $name
 * @property string $domain
 * @property string $email
 * @property string $favicon
 * @property string $logo
 * @property string $theme
 * @property string $skin
 * @property string $detailsinfo
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class Developments extends \yii\db\ActiveRecord
{

    public $developer_id;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%developments}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['development_id', 'created_by', 'updated_by','assignment_id'], 'integer'],
            [['launch_date','development_address'],'safe'],
            ['assignment_id','required','on'=>'assignment_required'],
            [['development_name'], 'required'],
            [['image_url', 'excerpt', 'sales_status', 'grade', 'development_url', 'created_at', 'updated_at','assignment_id'], 'safe'],
            [['development_name'], 'string', 'max' => 90],
            //[['developer_id'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'development_id' => Yii::t('app', 'ID'),
            'development_name' => Yii::t('app', 'Development Name'),
            'development_address' => Yii::t('app', 'Development Address'),
            'assignment_id' => Yii::t('app', 'Assigned To'),
            'developer_id' => Yii::t('app', 'Developer Name'),
            'development_url' => Yii::t('app', 'Development URL'),
            'sales_status' => Yii::t('app', 'Sales Status'),
            'launch_date' => Yii::t('app', 'Launch Date'),
            'performance' => Yii::t('app', 'Performance'),
            'grade' => Yii::t('app', 'Grade'),
            'excerpt' => Yii::t('app', 'Excerpt'),
            'image_url' => Yii::t('app', 'Image URL'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }

    public function fields()
    {
        $fields = parent::fields();

        $additionalFields = [
            /*'userInfo' => function ($model) {
                return ArrayHelper::getValue($model, 'crBy0');
            },*/
        ];

        $result = ArrayHelper::merge($fields, $additionalFields);

        return $result;
    }

    public function extraFields()
    {
        return [
            /*'userInfo' => function ($model) {
                return ArrayHelper::getValue($model, 'crBy0');
            },*/
        ];
    }
}

<?php

namespace api\modules\v1\models;

use api\models\User;
use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%sample}}".
 *
 * @property string $id
 * @property string $packageId
 * @property string $orgIdPrefix
 * @property string $name
 * @property string $domain
 * @property string $email
 * @property string $favicon
 * @property string $logo
 * @property string $theme
 * @property string $skin
 * @property string $detailsinfo
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class ContactsReservations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%contacts_reservations}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contact_id', 'development_id', 'created_by', 'updated_by', 'status'], 'integer'],
            [['floor_range', 'require_parking', 'require_locker'], 'string'],
            [['development_id', 'floor_range', 'require_parking', 'require_locker'], 'required'],
            [['created_at', 'updated_at','nature_of_purchase'], 'safe'],
            [['floor_plans_1st_choice', 'floor_plans_2nd_choice', 'floor_plans_3rd_choice'], 'string', 'max' => 100],
            //[['nature_of_purchase'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'contact_id' => Yii::t('app', 'Contact Name'),
            'development_id' => Yii::t('app', 'Development Name'),
            'floor_plans_1st_choice' => Yii::t('app', 'Floor Plans 1st Choice'),
            'floor_plans_2nd_choice' => Yii::t('app', 'Floor Plans 2nd Choice'),
            'floor_plans_3rd_choice' => Yii::t('app', 'Floor Plans 3rd Choice'),
            'floor_range' => Yii::t('app', 'Floor Range'),
            'require_parking' => Yii::t('app', 'Require Parking'),
            'require_locker' => Yii::t('app', 'Require Locker'),
            'nature_of_purchase' => Yii::t('app', 'Nature Of Purchase'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'status' => Yii::t('app', 'Reservation Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * before save data
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = 39;
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }

    public function fields()
    {
        $fields = parent::fields();

        $additionalFields = [
            /*'userInfo' => function ($model) {
                return ArrayHelper::getValue($model, 'crBy0');
            },*/
        ];

        $result = ArrayHelper::merge($fields, $additionalFields);

        return $result;
    }

    public function extraFields()
    {
        return [
            /*'userInfo' => function ($model) {
                return ArrayHelper::getValue($model, 'crBy0');
            },*/
        ];
    }
}

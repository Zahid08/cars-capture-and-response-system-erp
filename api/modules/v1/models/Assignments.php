<?php

namespace api\modules\v1\models;

use api\models\User;
use Yii;
use yii\helpers\ArrayHelper;
use api\helpers\CommonHelper;

/**
 * This is the model class for table "{{%assignments}}".

 */
class Assignments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $beds_id;
    public $address;
    public $sendemail;
    public $subject;
    public $message_body;
    public $suite_number;

    public static function tableName()
    {
        return '{{%assignments}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['development_id','listing_price', 'original_price', 'deposit_price', 'remaining_deposit', 'commission', 'service', 'psf', 'status', 'mail_status', 'created_by', 'updated_by'], 'safe'],
            [['remaining_due_date', 'occupancy', 'expire_date', 'created_at', 'updated_at'], 'safe'],
            [['purchaser_name'], 'string', 'max' => 100],
            [['development_id','listing_price','original_price','deposit_price','remaining_deposit','purchaser_name','commission'], 'required'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'development_id' => Yii::t('app', 'Building Name'),
            'sendemail' => Yii::t('app', 'To : '),
            'subject' => Yii::t('app', 'Subject:'),
            'message_body' => Yii::t('app', 'Top Message:'),
            'address' => Yii::t('app', 'Address'),
            'beds_id' => Yii::t('app', 'Suite Type'),
            'suite_number' => Yii::t('app', 'Suite Number'),
            'assignment_assets_id' => Yii::t('app', 'Assignment Assets ID'),
            'listing_price' => Yii::t('app', 'Listing Price'),
            'original_price' => Yii::t('app', 'Original Price'),
            'deposit_price' => Yii::t('app', 'Deposit Price'),
            'remaining_deposit' => Yii::t('app', 'Remaining Deposit'),
            'remaining_due_date' => Yii::t('app', 'Remaining Due Date'),
            'occupancy' => Yii::t('app', 'Occupancy'),
            'purchaser_name' => Yii::t('app', 'Purchaser Name'),
            'commission' => Yii::t('app', 'Commission'),
            'expire_date' => Yii::t('app', 'Expire Date'),
            'service' => Yii::t('app', 'MLS/Exclusive'),
            'psf' => Yii::t('app', 'PSF'),
            'status' => Yii::t('app', 'Status'),
            'mail_status' => Yii::t('app', 'Mail Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
    * before save data
    */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord)
            {
                $this->created_by = CommonHelper::getLoggedInUserId();
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_by = CommonHelper::getLoggedInUserId();
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else return false;
    }

    public function fields()
    {
        $fields = parent::fields();

        $additionalFields = [
            /*'userInfo' => function ($model) {
                return ArrayHelper::getValue($model, 'crBy0');
            },*/
        ];

        $result = ArrayHelper::merge($fields, $additionalFields);

        return $result;
    }

    public function extraFields()
    {
        return [
            /*'userInfo' => function ($model) {
                return ArrayHelper::getValue($model, 'crBy0');
            },*/
        ];
    }
}

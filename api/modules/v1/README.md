Core
=======================================================================================================================

Core for every Site

Usage
-----------------------------------------------------------------------------------------------------------------------
Token: 
?access-token=7Cg9EAJh7RalxFzOc5IrC4py5H9aZ0oV
Authorization: Bearer 7Cg9EAJh7RalxFzOc5IrC4py5H9aZ0oV

CDC Search:

Search: cdc_number
http://localhost/gso/api/web/v1/service-cdc/search?cdc_number=C/O/5267

Date of Issue
http://localhost/gso/api/web/v1/service-cdc/search?issue_date=2017-09-13

Date of Expire
http://localhost/gso/api/web/v1/service-cdc/search?expire_date=C/O/5267

Place of Birth
http://localhost/gso/api/web/v1/service-cdc/search?birth_place=Bangladesh

Religion
http://localhost/gso/api/web/v1/service-cdc/search?religion=Islam


CDC Details:
http://localhost/gso/api/web/v1/service-cdc/view?id=3

Voyage Listing:
http://localhost/gso/api/web/v1/service-voyage/search?cdc_id=4
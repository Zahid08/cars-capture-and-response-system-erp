<?php

namespace api\modules\v1\controllers;

use api\common\maropost\maropost;
use api\common\maropost\tags;
use api\modules\v1\models\Agent;
use api\modules\v1\models\City;
use api\modules\v1\models\Contacts;
use api\modules\v1\models\ContactsBrands;
use api\modules\v1\models\ContactsSysteminfo;
use api\modules\v1\models\ContactsTags;
use api\modules\v1\models\ContactsTagsApplied;
use api\modules\v1\models\ContactsTagsNew;
use api\modules\v1\models\ContactsTagsRelation;
use api\modules\v1\models\Developments;
use api\modules\v1\models\Province;
use api\modules\v1\models\Reservations;
use unlock\modules\contacts\models\ContactsReservations;
use unlock\modules\events\models\Events;
use Yii;
use yii\base\Exception;
use yii\web\HttpException;
use yii\web\Response;
use yii\rest\ActiveController;
use yii\filters\Cors;
use yii\filters\RateLimiter;
use yii\filters\ContentNegotiator;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use api\modules\v1\models\Sample;
use api\modules\v1\models\search\SampleSearch;
use yii\web\ServerErrorHttpException;

class ReservationController extends ActiveController
{
    public $modelClass = Contacts::class;
    public $modelAgentClass = Agent::class;

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];

        $behaviors['rateLimiter'] = [
            'class' => RateLimiter::className(),
            'enableRateLimitHeaders' => false,
        ];

        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' =>  ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page', 'X-Pagination-Page-Count', 'X-Pagination-Per-Page', 'X-Pagination-Total-Count', 'Link'],
            ],
        ];

        return $behaviors;
    }

    /**
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();

        unset($actions['create'], $actions['update'], $actions['delete'], $actions['options']);

        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    /**
     * List all records page by page
     *
     * HTTP Verb: GET
     * URL: /companies
     *
     * @return \yii\data\ActiveDataProvider
     * @throws HttpException
     */
    public function prepareDataProvider()
    {
        $searchModel = new SampleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if($dataProvider->getCount() <= 0) {
            throw new HttpException(404, 'No results found.');
        }
        else {
            return $dataProvider;
        }
    }


    /**
     * Finds the record based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $modelClass = $this->modelClass;
        if (($model = $modelClass::findOne($id)) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException("No records found");
        }
    }

    protected function findAgentModel($id)
    {
        $modelClass = $this->modelAgentClass;
        if (($model = $modelClass::findOne($id)) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException("No records found");
        }
    }

    /*
    * @Function Process :
    * API : @Method   : POST/GET
    *       @API URL :https://connectassetmgmt.com/api/web/v1/reservation/reservation-register?access-token=sample
    */
    public function actionReservationRegister(){
        /*Only Json data Get this function and process*/
        $data               = json_decode(file_get_contents('php://input'));

        $definition_data	=	$data->form_response->definition;
        $fields_data		=	$data->form_response->definition->fields;
        $answers_data		=	$data->form_response->answers;

        // Make real fields values
        $development_name	= $definition_data->title;
        $type_form_id       =$definition_data->id;

        $dataList			= array();
        $dataList['development_name']			= $development_name;
        $dataList['type_id']			= $type_form_id;

        foreach ($fields_data as $fields_key => $fields_value) {

                $title = str_replace(' - ', ' ', $fields_value->title);

                $make_string = str_replace(' ', '-', $title);
                $make_title = preg_replace('/[^A-Za-z0-9\-]/', '', $make_string);
                $make_title = str_replace('-', '_', strtolower($make_title));

                if ($answers_data[$fields_key]->type == 'text') {
                    $dataList[$make_title] = $answers_data[$fields_key]->text;
                }else if($answers_data[$fields_key]->type == 'number') {
                    $dataList[$make_title] = $answers_data[$fields_key]->number;
                }else if($answers_data[$fields_key]->type == 'email') {
                    $dataList[$make_title] = $answers_data[$fields_key]->email;
                }else if($answers_data[$fields_key]->type == 'boolean') {
                    $dataList[$make_title] = $answers_data[$fields_key]->boolean;
                }
                else if($answers_data[$fields_key]->type == 'choice') {
                    $dataList[$make_title] = $answers_data[$fields_key]->choice->label;
                }
        }

        if (!empty($dataList)){
           $this->saveData($dataList);

        }
        else{
            throw new HttpException(404, 'No results found.');
        }
    }

    public function saveData($registraion_data){
        if (isset($registraion_data)) {
            try {
                //prepare data for saving
                $development_name					=   !empty($registraion_data['development_name']) ? $registraion_data['development_name'] : '';
                $development_id                     =   $this->getDevelopmentId($development_name);
                $type_id                            =   !empty($registraion_data['type_id']) ? $registraion_data['type_id'] : '';
                $page_url                           =   'https://connectasset.typeform.com/to/'.$type_id;
                $first_name                         =   !empty($registraion_data['first_name']) ? $registraion_data['first_name'] : '';
                $last_name                          = !empty($registraion_data['last_name']) ? $registraion_data['last_name'] : '';
                $phone_number                       = !empty($registraion_data['phone_number']) ? $registraion_data['phone_number'] : '';
                $email                              = !empty($registraion_data['email']) ? $registraion_data['email'] : '';
                $address                            = !empty($registraion_data['address']) ? $registraion_data['address'] : '';
                $city                               = !empty($registraion_data['city']) ? $registraion_data['city'] : '';

                $province                           = !empty($registraion_data['province']) ? $registraion_data['province'] : '';
                $nature                             = !empty($registraion_data['intended_nature_of_purchase_choose_one']) ? $registraion_data['intended_nature_of_purchase_choose_one'] : '';
                $are_you_a_realtor                  = !empty($registraion_data['are_you_a_realtor']) ? $registraion_data['are_you_a_realtor'] : '';
                $comments_section                   = !empty($registraion_data['comments_section']) ? $registraion_data['comments_section'] : '';


                //do_require_parking_20000
                if ( !empty($registraion_data['do_you_require_parkingpurchase_price_79000']) ||!empty($registraion_data['do_you_require_parkingbremincl_in_price_with_1den__larger_45000_valueem'])  ||!empty($registraion_data['do_you_require_a_parking_spot']) || !empty($registraion_data['do_you_require_parking_65000_avail_on_2br__up'])  ||!empty($registraion_data['do_you_require_parking']) ||!empty($registraion_data['do_require_parking']) ||!empty($registraion_data['do_require_parking_20000']) || !empty($registraion_data['do_you_require_parkingbr']) || !empty($registraion_data['parking_only_available_for_2_beds_or_larger'])){
                    if (isset($registraion_data['do_require_parking_20000']) ||isset($registraion_data['do_require_parking']) || isset($registraion_data['parking_only_available_for_2_beds_or_larger'])){
                        $require_parking='Yes';
                    }
                    elseif (isset($registraion_data['do_you_require_parkingbr'])){
                        if (strtolower($registraion_data['do_you_require_parkingbr'])=='yes'){$require_parking='Yes';}
                        elseif (strtolower($registraion_data['do_you_require_parkingbr'])=='no'){$require_parking='No';}
                        else{$require_parking='';}
                    }
                    elseif (isset($registraion_data['do_you_require_parking'])){
                        if (strtolower($registraion_data['do_you_require_parking'])=='yes'){$require_parking='Yes';}
                        elseif (strtolower($registraion_data['do_you_require_parking'])=='no'){$require_parking='No';}
                        else{$require_parking='';}
                    }
                    elseif (isset($registraion_data['do_you_require_a_parking_spot'])){
                        if (strtolower($registraion_data['do_you_require_a_parking_spot'])=='yes'){$require_parking='Yes';}
                        elseif (strtolower($registraion_data['do_you_require_a_parking_spot'])=='no'){$require_parking='No';}
                        else{$require_parking='';}
                    }
                    elseif (isset($registraion_data['do_you_require_parking_65000_avail_on_2br__up'])){
                        if (strtolower($registraion_data['do_you_require_parking_65000_avail_on_2br__up'])=='yes'){$require_parking='Yes';}
                        elseif (strtolower($registraion_data['do_you_require_parking_65000_avail_on_2br__up'])=='no'){$require_parking='No';}
                        else{$require_parking='';}
                    }
                    elseif (isset($registraion_data['do_you_require_parkingbremincl_in_price_with_1den__larger_45000_valueem'])){
                        if (strtolower($registraion_data['do_you_require_parkingbremincl_in_price_with_1den__larger_45000_valueem'])=='yes'){$require_parking='Yes';}
                        elseif (strtolower($registraion_data['do_you_require_parkingbremincl_in_price_with_1den__larger_45000_valueem'])=='no'){$require_parking='No';}
                        else{$require_parking='';}
                    }
                    elseif (isset($registraion_data['do_you_require_parkingpurchase_price_79000'])){
                        if (strtolower($registraion_data['do_you_require_parkingpurchase_price_79000'])=='yes'){$require_parking='Yes';}
                        elseif (strtolower($registraion_data['do_you_require_parkingpurchase_price_79000'])=='no'){$require_parking='No';}
                        else{$require_parking='';}
                    }
                }
                else{
                    $require_parking='No';
                }

                //do_require_a_locker_35000
                if (!empty($registraion_data['do_you_require_a_lockerpurchase_price_7950_']) ||!empty($registraion_data['do_you_require_a_locker_']) ||!empty($registraion_data['do_you_require_a_lockerbremincl_in_price_7000_valueem'])  ||!empty($registraion_data['do_you_require_a_locker_7500']) ||!empty($registraion_data['do_you_require_a_locker']) ||!empty($registraion_data['do_require_a_locker']) ||!empty($registraion_data['do_require_a_locker_35000']) || !empty($registraion_data['do_you_require_a_locker_br'])){
                    if (isset($registraion_data['do_require_a_locker_35000']) || isset($registraion_data['do_require_a_locker'])){
                        $require_a_locker='Yes';
                    }
                    elseif (isset($registraion_data['do_you_require_a_locker_br'])){
                        if (strtolower($registraion_data['do_you_require_a_locker_br'])=='yes'){$require_a_locker='Yes';}
                        elseif (strtolower($registraion_data['do_you_require_a_locker_br'])=='no'){$require_a_locker='No';}
                        else{$require_a_locker='';}
                    }
                    elseif (isset($registraion_data['do_you_require_a_locker'])){
                        if (strtolower($registraion_data['do_you_require_a_locker'])=='yes'){$require_a_locker='Yes';}
                        elseif (strtolower($registraion_data['do_you_require_a_locker'])=='no'){$require_a_locker='No';}
                        else{$require_a_locker='';}
                    }
                    elseif (isset($registraion_data['do_you_require_a_locker_7500'])){
                        if (strtolower($registraion_data['do_you_require_a_locker_7500'])=='yes'){$require_a_locker='Yes';}
                        elseif (strtolower($registraion_data['do_you_require_a_locker_7500'])=='no'){$require_a_locker='No';}
                        else{$require_a_locker='';}
                    }
                    elseif (isset($registraion_data['do_you_require_a_lockerbremincl_in_price_7000_valueem'])){
                        if (strtolower($registraion_data['do_you_require_a_lockerbremincl_in_price_7000_valueem'])=='yes'){$require_a_locker='Yes';}
                        elseif (strtolower($registraion_data['do_you_require_a_lockerbremincl_in_price_7000_valueem'])=='no'){$require_a_locker='No';}
                        else{$require_a_locker='';}
                    }
                    elseif (isset($registraion_data['do_you_require_a_locker_'])){
                        if (strtolower($registraion_data['do_you_require_a_locker_'])=='yes'){$require_a_locker='Yes';}
                        elseif (strtolower($registraion_data['do_you_require_a_locker_'])=='no'){$require_a_locker='No';}
                        else{$require_a_locker='';}
                    }
                    elseif (isset($registraion_data['do_you_require_a_lockerpurchase_price_7950_'])){
                        if (strtolower($registraion_data['do_you_require_a_lockerpurchase_price_7950_'])=='yes'){$require_a_locker='Yes';}
                        elseif (strtolower($registraion_data['do_you_require_a_lockerpurchase_price_7950_'])=='no'){$require_a_locker='No';}
                        else{$require_a_locker='';}
                    }
                }
                else{
                    $require_a_locker='No';
                }

                //drivers_license_number
                if (!empty($registraion_data['drivers_license_number_not_mandatory']) || !empty($registraion_data['drivers_license_number']) || !empty($registraion_data['drivers_license_number__not_mandatory'])){
                    if (isset($registraion_data['drivers_license_number'])) {
                        $drivers_license_number =$registraion_data['drivers_license_number'];
                    } elseif (isset($registraion_data['drivers_license_number__not_mandatory'])){
                        $drivers_license_number = $registraion_data['drivers_license_number__not_mandatory'];}
                    elseif (isset($registraion_data['drivers_license_number_not_mandatory'])){
                        $drivers_license_number = $registraion_data['drivers_license_number_not_mandatory'];}
                }else{$drivers_license_number='';}

                //postal code
                if (!empty($registraion_data['postal_code_']) || !empty($registraion_data['postal_code'])){
                   if (isset($registraion_data['postal_code_'])) {
                       $postal_code =$registraion_data['postal_code_'];
                   } elseif (isset($registraion_data['postal_code'])){
                       $postal_code = $registraion_data['postal_code'];}
                }else{$postal_code='';}

                //date_of_birth_daymonth_year
                if ( !empty($registraion_data['date_of_birth_year_month_day']) ||!empty($registraion_data['date_of_birth_ddmmyy']) ||!empty($registraion_data['date_of_birth_daymonth_year']) || !empty($registraion_data['date_of_birth'])){
                    if (isset($registraion_data['date_of_birth_daymonth_year'])) {
                        $date_of_birth_daymonth_year =$registraion_data['date_of_birth_daymonth_year'];
                    } elseif (isset($registraion_data['date_of_birth'])){
                        $date_of_birth_daymonth_year =$registraion_data['date_of_birth'];}
                    elseif (isset($registraion_data['date_of_birth_ddmmyy'])){
                        $date_of_birth_daymonth_year =$registraion_data['date_of_birth_ddmmyy'];}
                    elseif (isset($registraion_data['date_of_birth_year_month_day'])){
                        $date_of_birth_daymonth_year =$registraion_data['date_of_birth_year_month_day'];}
                }else{$date_of_birth_daymonth_year='';}

                //sin_not_mandatory
                if (!empty($registraion_data['sin__not_mandatory']) ||!empty($registraion_data['sin_not_mandatory']) || !empty($registraion_data['sin'])){
                    if (isset($registraion_data['sin_not_mandatory'])) {
                        $sin_not_mandatory =$registraion_data['sin_not_mandatory'];
                    }
                    elseif (isset($registraion_data['sin'])){
                        $sin_not_mandatory =$registraion_data['sin'];
                    }
                    elseif (isset($registraion_data['sin__not_mandatory'])){
                        $sin_not_mandatory =$registraion_data['sin__not_mandatory'];
                    }
                }else{$sin_not_mandatory='';}


                //profession
                if (!empty($registraion_data['profession_what_do_you_do_and_where_do_you_do_it']) || !empty($registraion_data['profession'])){
                    if (isset($registraion_data['profession_what_do_you_do_and_where_do_you_do_it'])) {
                        $professions =$registraion_data['profession_what_do_you_do_and_where_do_you_do_it'];
                    } elseif (isset($registraion_data['profession'])){
                        $professions =$registraion_data['profession'];}
                }else{$professions='';}

                //choice_1_model
                if (!empty($registraion_data['choice_1model']) ||!empty($registraion_data['choice_1_model']) || !empty($registraion_data['choice_1']) || !empty($registraion_data['choice_1model']) ){
                    if (isset($registraion_data['choice_1_model'])) {
                        $choice_1_model =$registraion_data['choice_1_model'];
                    } elseif (isset($registraion_data['choice_1'])){
                        $choice_1_model =$registraion_data['choice_1'];}
                    elseif (isset($registraion_data['choice_1model'])){
                        $choice_1_model =$registraion_data['choice_1model'];}
                    elseif (isset($registraion_data['choice_1model'])){
                        $choice_1_model =$registraion_data['choice_1model'];}
                }else{$choice_1_model='';}

                //choice_2_model
                if (!empty($registraion_data['choice_2_model']) || !empty($registraion_data['choice_2']) || !empty($registraion_data['choice_2model'])){
                    if (isset($registraion_data['choice_2_model'])) {
                        $choice_2_model =$registraion_data['choice_2_model'];
                    } elseif (isset($registraion_data['choice_2'])){
                        $choice_2_model =$registraion_data['choice_2'];}
                    elseif (isset($registraion_data['choice_2model'])){
                        $choice_2_model =$registraion_data['choice_2model'];}
                }else{$choice_2_model='';}

                //choice_3_model
                if (!empty($registraion_data['choice_3model']) ||!empty($registraion_data['choice_3_model']) || !empty($registraion_data['choice_3'])){
                    if (isset($registraion_data['choice_3_model'])) {
                        $choice_3_model =$registraion_data['choice_3_model'];
                    } elseif (isset($registraion_data['choice_3'])){
                        $choice_3_model =$registraion_data['choice_3'];}
                    elseif (isset($registraion_data['choice_3model'])){
                        $choice_3_model =$registraion_data['choice_3model'];}
                }else{$choice_3_model='';}


                //floor_range
                if (!empty($registraion_data['enter_your_desired_floor_range_please_note_floor_premiums_do_apply']) || !empty($registraion_data['please_enter_the_range_of_floors_you_are_most_interested_in'])){
                    if (isset($registraion_data['enter_your_desired_floor_range_please_note_floor_premiums_do_apply'])) {
                        $floor_range =$registraion_data['enter_your_desired_floor_range_please_note_floor_premiums_do_apply'];
                    } elseif (isset($registraion_data['please_enter_the_range_of_floors_you_are_most_interested_in'])){
                        $floor_range =$registraion_data['please_enter_the_range_of_floors_you_are_most_interested_in'];}
                }else{$floor_range='';}

                if ($nature=='End user' || $nature=='End User'){
                    $nature_of_purchase='End User';
                }else{
                    $nature_of_purchase='Investor';
                };

                $cityInfo = City::find()->where(['city_name' => $city])->one();
                if (!empty($cityInfo)) {$cityId = $cityInfo->id;} else {$cityId = '';}
                $proviInfo = Province::find()->where(['province_name' => $province])->one();
                if (!empty($proviInfo)) {
                    $provId = $proviInfo->id;
                } else {
                    $provId = '';
                }
                if ($are_you_a_realtor==1){
                    $agentModel = new Agent();
                    $checkEmail = Agent::find()->where(['email' => $email])->one();

                    if (!empty($checkEmail)) {
                        //agent data save
                        $agentModel = $this->findAgentModel($checkEmail->id);
                        $agentModel->first_name = $first_name;
                        $agentModel->last_name = $last_name;
                        $agentModel->email = $email;
                        $agentModel->mobile =(string)$phone_number;
                        $agentModel->agent_type = '1';
                        $agentModel->brand = '1';
                        $agentModel->address =$address;
                        $agentModel->province =(string)$provId;
                        $agentModel->city =(string)$cityId;
                        $agentModel->postal_code =$postal_code;
                        if ($agentModel->save()){
                            echo "Successfully save";
                        }

                    }else{
                        $agentModel->first_name = $first_name;
                        $agentModel->last_name = $last_name;
                        $agentModel->email = $email;
                        $agentModel->mobile =(string)$phone_number;
                        $agentModel->agent_type = '1';
                        $agentModel->brand = '1';
                        $agentModel->address =$address;
                        $agentModel->province =(string)$provId;
                        $agentModel->city =(string)$cityId;
                        $agentModel->postal_code =$postal_code;
                        if ($agentModel->save()){
                            echo "Successfully save";
                        }
                    }
                    if ($agentModel->save()) {
                        $baseUrl = Yii::getAlias('@baseUrl');
                        $dataListSendMaropost = array(
                            'development_id' => $development_id,
                            'media_source' => 'Website',
                            'pageUrl' => $page_url,
                            'email' => $email,
                            'firstname' => $first_name,
                            'lastname' => $last_name,
                            'phone' => $phone_number,
                            'realtor' => '1',
                            'cars_url' => '' . $baseUrl . '/backend/web/contacts/contacts-reservations/update?id=' . $agentModel->id
                        );
                        $this->reservation_type_form($registraion_data,$dataListSendMaropost);
                        $this->sendEmailTypeForm($registraion_data, $dataListSendMaropost['media_source'], $dataListSendMaropost['cars_url'], $dataListSendMaropost['pageUrl']);
                    }
                }
                else {
                    $contactModel = new Contacts();
                    $checkEmail = Contacts::find()->where(['email' => $email])->one();
                    if (!empty($checkEmail)) {
                        $contactModel = $this->findModel($checkEmail->id);
                        $contactModel->first_name = $first_name;
                        $contactModel->last_name = $last_name;
                        $contactModel->email = $email;
                        $contactModel->phone = (string)$phone_number;
                        $contactModel->address = $address;
                        $contactModel->city =(string)$cityId;
                        $contactModel->province =(string)$provId;
                        $contactModel->postal = $postal_code;
                        $contactModel->date_of_birth = $date_of_birth_daymonth_year;
                        $contactModel->drivers_license = $drivers_license_number;
                        $contactModel->profession = $professions;
                        $contactModel->SIN = $sin_not_mandatory;
                        $contactModel->created_at = date('Y-m-d H:i:s');
                        $contactModel->lead_source ='1';
                        if ($contactModel->save()) {

                            //tag information
                            $contactsTag = new ContactsTagsNew();
                            $contactsTag->tag_name =strtolower($development_name).' - '.'reservation';
                            $contactsTag->tag_category_id=4;
                            $checkTags = ContactsTagsNew::find()->where(['tag_name' => $contactsTag->tag_name])->one();
                           if (empty($checkTags)){
                               if ($contactsTag->save(false)) {
                                   //save data relation table
                                   $tagRelation=new ContactsTagsRelation();
                                   $tagRelation->contact_id=$checkEmail->id;
                                   $tagRelation->tag_id=$contactsTag->id;
                                   $tagRelation->date_applied= date('Y-m-d H:i:s');
                                   if (!$tagRelation->save(false)) {
                                       throw new Exception(json_encode($tagRelation->errors));
                                   }
                                   //save data to applied tag
                                   $tagApplied=new ContactsTagsApplied();
                                   $tagApplied->tag_id=$contactsTag->id;
                                   if (!$tagApplied->save(false)) {
                                       throw new Exception(json_encode($tagApplied->errors));
                                   }

                               }
                           }
                           else{
                               $tagRelation=new ContactsTagsRelation();
                               $tagRelation->contact_id=$checkEmail->id;
                               $tagRelation->tag_id=$checkTags->id;
                               $tagRelation->date_applied= date('Y-m-d H:i:s');
                               if (!$tagRelation->save(false)) {
                                   throw new Exception(json_encode($tagRelation->errors));
                               }
                               $tagApplied=new ContactsTagsApplied();
                               $tagApplied->tag_id=$checkTags->id;
                               if (!$tagApplied->save(false)) {
                                   throw new Exception(json_encode($tagApplied->errors));
                               }
                           }

                            //Contact Reservation Save
                            $contactReservation = new ContactsReservations();
                            $contactReservation->contact_id = $contactModel->id;
                            $contactReservation->development_id = $development_id;
                            $contactReservation->floor_plans_1st_choice = $choice_1_model;
                            $contactReservation->floor_plans_2nd_choice = $choice_2_model;
                            $contactReservation->floor_plans_3rd_choice = $choice_3_model;
                            $contactReservation->floor_range = $floor_range;
                            $contactReservation->require_parking = $require_parking;
                            $contactReservation->require_locker =$require_a_locker;
                            $contactReservation->nature_of_purchase = $nature_of_purchase;
                            if (!$contactReservation->save(false)) {
                                throw new Exception(json_encode($contactReservation->errors));
                            }

                            // System information
                            $contactsSystemInfo = new ContactsSysteminfo();
                            $contactsSystemInfo->contact_id = $contactModel->id;
                            $contactsSystemInfo->page_name = $development_name;
                            $contactsSystemInfo->page_url = $page_url;
                            $contactsSystemInfo->ip_address ='103.15.141.78';
                            $contactsSystemInfo->browser = 'Chrome';
                            $contactsSystemInfo->operating ='Windows';
                            $contactsSystemInfo->visit_date = date('Y-m-d H:i:s');
                            if (!$contactsSystemInfo->save(false)) {
                                throw new Exception(json_encode($contactsSystemInfo->errors));
                            }

                        }
                    } else {
                        $contactReservation = new ContactsReservations();

                        $contactModel->first_name = $first_name;
                        $contactModel->last_name = $last_name;
                        $contactModel->email = $email;
                        $contactModel->phone = (string)$phone_number;
                        $contactModel->address = $address;
                        $contactModel->city =(string)$cityId;
                        $contactModel->province =(string)$provId;
                        $contactModel->postal = $postal_code;
                        $contactModel->date_of_birth = $date_of_birth_daymonth_year;
                        $contactModel->drivers_license = $drivers_license_number;
                        $contactModel->profession = $professions;
                        $contactModel->SIN = $sin_not_mandatory;
                        $contactModel->created_at = date('Y-m-d H:i:s');
                        $contactModel->lead_source ='1';
                        if ($contactModel->save()) {

                            //tag information
                            $contactsTag = new ContactsTagsNew();
                            $contactsTag->tag_name =strtolower($development_name).' - '.'reservation';
                            $contactsTag->tag_category_id=4;
                            $checkTags = ContactsTagsNew::find()->where(['tag_name' => $contactsTag->tag_name])->one();
                            if (empty($checkTags)){
                                if ($contactsTag->save(false)) {
                                    //save data relation table
                                    $tagRelation=new ContactsTagsRelation();
                                    $tagRelation->contact_id=$contactModel->id;
                                    $tagRelation->tag_id=$contactsTag->id;
                                    $tagRelation->date_applied= date('Y-m-d H:i:s');
                                    if (!$tagRelation->save(false)) {
                                        throw new Exception(json_encode($tagRelation->errors));
                                    }
                                    //save data to applied tag
                                    $tagApplied=new ContactsTagsApplied();
                                    $tagApplied->tag_id=$contactsTag->id;
                                    if (!$tagApplied->save(false)) {
                                        throw new Exception(json_encode($tagApplied->errors));
                                    }

                                }
                            }
                            else{
                                $tagRelation=new ContactsTagsRelation();
                                $tagRelation->contact_id=$contactModel->id;
                                $tagRelation->tag_id=$checkTags->id;
                                $tagRelation->date_applied= date('Y-m-d H:i:s');
                                if (!$tagRelation->save(false)) {
                                    throw new Exception(json_encode($tagRelation->errors));
                                }
                                //save data to applied tag
                                $tagApplied=new ContactsTagsApplied();
                                $tagApplied->tag_id=$checkTags->id;
                                if (!$tagApplied->save(false)) {
                                    throw new Exception(json_encode($tagApplied->errors));
                                }
                            }

                            //brand information
                            $brandInfo=new ContactsBrands();
                            $brandInfo->contact_id = $contactModel->id;
                            $brandInfo->brand_id =1;
                            $brandInfo->applied_date =date('Y-m-d H:i:s');
                            if (!$brandInfo->save(false)) {
                                throw new Exception(json_encode($brandInfo->errors));
                            }

                            //Contact Reservation Save
                            $contactReservation = new ContactsReservations();
                            $contactReservation->contact_id = $contactModel->id;
                            $contactReservation->development_id =$development_id;
                            $contactReservation->floor_plans_1st_choice = $choice_1_model;
                            $contactReservation->floor_plans_2nd_choice = $choice_2_model;
                            $contactReservation->floor_plans_3rd_choice = $choice_3_model;
                            $contactReservation->floor_range = $floor_range;
                            $contactReservation->require_parking =$require_parking;
                            $contactReservation->require_locker =$require_a_locker;
                            $contactReservation->nature_of_purchase = $nature_of_purchase;
                            if (!$contactReservation->save(false)) {
                                throw new Exception(json_encode($contactReservation->errors));
                            }

                            // System information
                            $contactsSystemInfo = new ContactsSysteminfo();
                            $contactsSystemInfo->contact_id = $contactModel->id;
                            $contactsSystemInfo->page_name = $development_name;
                            $contactsSystemInfo->page_url = $page_url;
                            $contactsSystemInfo->ip_address ='103.15.141.78';
                            $contactsSystemInfo->browser = 'Chrome';
                            $contactsSystemInfo->operating ='Windows';
                            $contactsSystemInfo->visit_date = date('Y-m-d H:i:s');
                            if (!$contactsSystemInfo->save(false)) {
                                throw new Exception(json_encode($contactsSystemInfo->errors));
                            }
                        }
                    }
                    if ($contactModel->save()) {
                        $baseUrl = Yii::getAlias('@baseUrl');
                        $dataListSendMaropost = array(
                            'development_id' => $development_id,
                            'media_source' => 'Website',
                            'pageUrl' => $page_url,
                            'email' => $email,
                            'firstname' => $first_name,
                            'lastname' => $last_name,
                            'phone' => $phone_number,
                            'realtor' => '',
                            'cars_url' => '' . $baseUrl . '/backend/web/contacts/contacts-reservations/update?id=' . $contactReservation->id
                        );
                        $this->reservation_type_form($registraion_data,$dataListSendMaropost);
                        $this->sendEmailTypeForm($registraion_data, $dataListSendMaropost['media_source'], $dataListSendMaropost['cars_url'], $dataListSendMaropost['pageUrl']);
                    }
                    }
            }
            catch (Exception $e) {
                throw new ServerErrorHttpException($e->getMessage());
            }
        }
    }

    public  function getDevelopmentId($data){
        $development_id='';
        $development_name=strtolower($data);
        if (strpos(strtolower($data), '&') !== false) {
            $development_name = str_replace('&', 'and', strtolower($data));
        }
        $development=Developments::find()->where(['like', 'development_name', $development_name])->one();
        if (!empty($development)){
            $development_id=$development->development_id;
        }
        else{
            $development_id='';
        }

        return $development_id;
    }

    /*Reservation Type Form data send to maropost*/
    public  function  reservation_type_form($model,$registraion_data){
            if (!empty($registraion_data['development_id'])) {
                $develomentList = Developments::find()->where(['development_id' => $registraion_data['development_id']])->one();
                $projectTitle = $projectName = $projectUrl = $project_sales_status = '';
                if (!empty($develomentList)) {
                    $projectTitle = $develomentList->development_name;
                    $projectName = $develomentList->development_name;
                    $project_sales_status = $develomentList->sales_status;
                }
            }
            else{
                $projectTitle = $model['development_name'];
                $projectName = $model['development_name'];
                $project_sales_status ='comingsoon';
            }
            $projectUrl = $registraion_data['pageUrl'];

            $realtor = '';
            $prospect = '';
            $realtor_tag = '';

            if (!empty($registraion_data['realtor'])) {
                $realtor_ = 'true';
                $prospect = 'False';
                $realtor = 1;
                $realtor_tag = 'Realtor';
                $list_id = 17;
            } else {
                $realtor_ = 'False';
                $prospect = 'true';
                $realtor_tag = 'Prospect';
                $realtor = 0;
                $list_id = 1;
            }

            $registraion_data['infusionsoftTag']=$model['development_name'].'-'.'reservation';
            $registraion_data['media_source']='Original Media Source -'.$registraion_data['media_source'];

            $tags=new tags();
            //tag created if tag not exist in maropost
            $array = array(
                "tags" => array(
                    "name" =>$registraion_data['infusionsoftTag']
                )
            );
            $tags->post_tags($array);


            $array1 = array(
                "tags" => array(
                    "name" =>$registraion_data['media_source']
                )
            );
            $tags->post_tags($array1);

            /*
             * data prepare for maropost
             */
            $contactsNew=new maropost();
            $contactsNew = $contactsNew->contacts();

            $arrayNew = array(
                'list_id' => $list_id,
                'contact' =>
                    array(
                        'first_name' => $registraion_data['firstname'],
                        'last_name' => $registraion_data['lastname'],
                        'email' => $registraion_data['email'],
                        'subscribe' => 'true',
                        'custom_field' =>
                            array(
                                'phone_1' => $registraion_data['phone'],
                                'company' => 'CONNECT asset management',
                                'realtor' => $realtor_,
                                'prospect' => $prospect,
                                'comment_question' => $projectName,
                                'development_name' => $projectTitle,
                                'reference_link' => $projectUrl,
                                'reservation_register_flag' => time(),
                            ),
                        'add_tags' =>
                            array(
                                $registraion_data['infusionsoftTag'],
                                $registraion_data['media_source'],
                                $realtor_tag
                            )
                    ));

                    $arrayNewsletter = $arrayNew;
                    if($arrayNewsletter['list_id'] == 1){
                        $arrayNewsletter['list_id'] = 3;
                        $contactsNew->post_new_contact_into_list($arrayNewsletter);
                    }

                    $contactsNew->post_new_contact_into_list($arrayNew);
    }

    /*Send Email from Type Form*/
    private function sendEmailTypeForm($registraion_data,$media_source,$cars_url,$pageUrl) {

        $development_name					=   !empty($registraion_data['development_name']) ? $registraion_data['development_name'] : '';
        $development_id                     =   $this->getDevelopmentId($development_name);
        $first_name                         =   !empty($registraion_data['first_name']) ? $registraion_data['first_name'] : '';
        $last_name                          = !empty($registraion_data['last_name']) ? $registraion_data['last_name'] : '';
        $phone_number                       = !empty($registraion_data['phone_number']) ? $registraion_data['phone_number'] : '';
        $email_client                       = !empty($registraion_data['email']) ? $registraion_data['email'] : '';
        $address                            = !empty($registraion_data['address']) ? $registraion_data['address'] : '';
        $city                               = !empty($registraion_data['city']) ? $registraion_data['city'] : '';
        $province                           = !empty($registraion_data['province']) ? $registraion_data['province'] : '';
        $nature_of_purchase                 = !empty($registraion_data['intended_nature_of_purchase_choose_one']) ? $registraion_data['intended_nature_of_purchase_choose_one'] : '';
        $are_you_a_realtor                  = !empty($registraion_data['are_you_a_realtor']) ? $registraion_data['are_you_a_realtor'] : '';
        $comments_section                   = !empty($registraion_data['comments_section']) ? $registraion_data['comments_section'] : '';
        $ip                                 ='103.15.141.78';
        $browser                            ='Chrome';

        //postal code
        if (!empty($registraion_data['postal_code_']) || !empty($registraion_data['postal_code'])){
            if (isset($registraion_data['postal_code_'])) {
                $postal_code =$registraion_data['postal_code_'];
            } elseif (isset($registraion_data['postal_code'])){
                $postal_code = $registraion_data['postal_code'];}
        }else{$postal_code='';}

        //profession
        if (!empty($registraion_data['profession_what_do_you_do_and_where_do_you_do_it']) || !empty($registraion_data['profession'])){
            if (isset($registraion_data['profession_what_do_you_do_and_where_do_you_do_it'])) {
                $professions =$registraion_data['profession_what_do_you_do_and_where_do_you_do_it'];
            } elseif (isset($registraion_data['profession'])){
                $professions =$registraion_data['profession'];}
        }else{$professions='';}

        //choice_1_model
        if (!empty($registraion_data['choice_1model']) ||!empty($registraion_data['choice_1_model']) || !empty($registraion_data['choice_1']) || !empty($registraion_data['choice_1model']) ){
            if (isset($registraion_data['choice_1_model'])) {
                $choice_1_model =$registraion_data['choice_1_model'];
            } elseif (isset($registraion_data['choice_1'])){
                $choice_1_model =$registraion_data['choice_1'];}
            elseif (isset($registraion_data['choice_1model'])){
                $choice_1_model =$registraion_data['choice_1model'];}
            elseif (isset($registraion_data['choice_1model'])){
                $choice_1_model =$registraion_data['choice_1model'];}
        }else{$choice_1_model='';}

        //choice_2_model
        if (!empty($registraion_data['choice_2_model']) || !empty($registraion_data['choice_2']) || !empty($registraion_data['choice_2model'])){
            if (isset($registraion_data['choice_2_model'])) {
                $choice_2_model =$registraion_data['choice_2_model'];
            } elseif (isset($registraion_data['choice_2'])){
                $choice_2_model =$registraion_data['choice_2'];}
            elseif (isset($registraion_data['choice_2model'])){
                $choice_2_model =$registraion_data['choice_2model'];}
        }else{$choice_2_model='';}

        //choice_3_model
        if (!empty($registraion_data['choice_3model']) ||!empty($registraion_data['choice_3_model']) || !empty($registraion_data['choice_3'])){
            if (isset($registraion_data['choice_3_model'])) {
                $choice_3_model =$registraion_data['choice_3_model'];
            } elseif (isset($registraion_data['choice_3'])){
                $choice_3_model =$registraion_data['choice_3'];}
            elseif (isset($registraion_data['choice_3model'])){
                $choice_3_model =$registraion_data['choice_3model'];}
        }else{$choice_3_model='';}

        //do_require_parking_20000
        if ( !empty($registraion_data['do_you_require_parkingpurchase_price_79000']) ||!empty($registraion_data['do_you_require_parkingbremincl_in_price_with_1den__larger_45000_valueem'])  ||!empty($registraion_data['do_you_require_a_parking_spot']) || !empty($registraion_data['do_you_require_parking_65000_avail_on_2br__up'])  ||!empty($registraion_data['do_you_require_parking']) ||!empty($registraion_data['do_require_parking']) ||!empty($registraion_data['do_require_parking_20000']) || !empty($registraion_data['do_you_require_parkingbr']) || !empty($registraion_data['parking_only_available_for_2_beds_or_larger'])){
            if (isset($registraion_data['do_require_parking_20000']) ||isset($registraion_data['do_require_parking']) || isset($registraion_data['parking_only_available_for_2_beds_or_larger'])){
                $require_parking='Yes';
            }
            elseif (isset($registraion_data['do_you_require_parkingbr'])){
                if (strtolower($registraion_data['do_you_require_parkingbr'])=='yes'){$require_parking='Yes';}
                elseif (strtolower($registraion_data['do_you_require_parkingbr'])=='no'){$require_parking='No';}
                else{$require_parking='';}
            }
            elseif (isset($registraion_data['do_you_require_parking'])){
                if (strtolower($registraion_data['do_you_require_parking'])=='yes'){$require_parking='Yes';}
                elseif (strtolower($registraion_data['do_you_require_parking'])=='no'){$require_parking='No';}
                else{$require_parking='';}
            }
            elseif (isset($registraion_data['do_you_require_a_parking_spot'])){
                if (strtolower($registraion_data['do_you_require_a_parking_spot'])=='yes'){$require_parking='Yes';}
                elseif (strtolower($registraion_data['do_you_require_a_parking_spot'])=='no'){$require_parking='No';}
                else{$require_parking='';}
            }
            elseif (isset($registraion_data['do_you_require_parking_65000_avail_on_2br__up'])){
                if (strtolower($registraion_data['do_you_require_parking_65000_avail_on_2br__up'])=='yes'){$require_parking='Yes';}
                elseif (strtolower($registraion_data['do_you_require_parking_65000_avail_on_2br__up'])=='no'){$require_parking='No';}
                else{$require_parking='';}
            }
            elseif (isset($registraion_data['do_you_require_parkingbremincl_in_price_with_1den__larger_45000_valueem'])){
                if (strtolower($registraion_data['do_you_require_parkingbremincl_in_price_with_1den__larger_45000_valueem'])=='yes'){$require_parking='Yes';}
                elseif (strtolower($registraion_data['do_you_require_parkingbremincl_in_price_with_1den__larger_45000_valueem'])=='no'){$require_parking='No';}
                else{$require_parking='';}
            }
            elseif (isset($registraion_data['do_you_require_parkingpurchase_price_79000'])){
                if (strtolower($registraion_data['do_you_require_parkingpurchase_price_79000'])=='yes'){$require_parking='Yes';}
                elseif (strtolower($registraion_data['do_you_require_parkingpurchase_price_79000'])=='no'){$require_parking='No';}
                else{$require_parking='';}
            }
        }
        else{
            $require_parking='No';
        }

        //do_require_a_locker_35000
        if (!empty($registraion_data['do_you_require_a_lockerpurchase_price_7950_']) ||!empty($registraion_data['do_you_require_a_locker_']) ||!empty($registraion_data['do_you_require_a_lockerbremincl_in_price_7000_valueem'])  ||!empty($registraion_data['do_you_require_a_locker_7500']) ||!empty($registraion_data['do_you_require_a_locker']) ||!empty($registraion_data['do_require_a_locker']) ||!empty($registraion_data['do_require_a_locker_35000']) || !empty($registraion_data['do_you_require_a_locker_br'])){
            if (isset($registraion_data['do_require_a_locker_35000']) || isset($registraion_data['do_require_a_locker'])){
                $require_a_locker='Yes';
            }
            elseif (isset($registraion_data['do_you_require_a_locker_br'])){
                if (strtolower($registraion_data['do_you_require_a_locker_br'])=='yes'){$require_a_locker='Yes';}
                elseif (strtolower($registraion_data['do_you_require_a_locker_br'])=='no'){$require_a_locker='No';}
                else{$require_a_locker='';}
            }
            elseif (isset($registraion_data['do_you_require_a_locker'])){
                if (strtolower($registraion_data['do_you_require_a_locker'])=='yes'){$require_a_locker='Yes';}
                elseif (strtolower($registraion_data['do_you_require_a_locker'])=='no'){$require_a_locker='No';}
                else{$require_a_locker='';}
            }
            elseif (isset($registraion_data['do_you_require_a_locker_7500'])){
                if (strtolower($registraion_data['do_you_require_a_locker_7500'])=='yes'){$require_a_locker='Yes';}
                elseif (strtolower($registraion_data['do_you_require_a_locker_7500'])=='no'){$require_a_locker='No';}
                else{$require_a_locker='';}
            }
            elseif (isset($registraion_data['do_you_require_a_lockerbremincl_in_price_7000_valueem'])){
                if (strtolower($registraion_data['do_you_require_a_lockerbremincl_in_price_7000_valueem'])=='yes'){$require_a_locker='Yes';}
                elseif (strtolower($registraion_data['do_you_require_a_lockerbremincl_in_price_7000_valueem'])=='no'){$require_a_locker='No';}
                else{$require_a_locker='';}
            }
            elseif (isset($registraion_data['do_you_require_a_locker_'])){
                if (strtolower($registraion_data['do_you_require_a_locker_'])=='yes'){$require_a_locker='Yes';}
                elseif (strtolower($registraion_data['do_you_require_a_locker_'])=='no'){$require_a_locker='No';}
                else{$require_a_locker='';}
            }
            elseif (isset($registraion_data['do_you_require_a_lockerpurchase_price_7950_'])){
                if (strtolower($registraion_data['do_you_require_a_lockerpurchase_price_7950_'])=='yes'){$require_a_locker='Yes';}
                elseif (strtolower($registraion_data['do_you_require_a_lockerpurchase_price_7950_'])=='no'){$require_a_locker='No';}
                else{$require_a_locker='';}
            }
        }
        else{
            $require_a_locker='No';
        }

        //floor_range
        if (!empty($registraion_data['enter_your_desired_floor_range_please_note_floor_premiums_do_apply']) || !empty($registraion_data['please_enter_the_range_of_floors_you_are_most_interested_in'])){
            if (isset($registraion_data['enter_your_desired_floor_range_please_note_floor_premiums_do_apply'])) {
                $floor_range =$registraion_data['enter_your_desired_floor_range_please_note_floor_premiums_do_apply'];
            } elseif (isset($registraion_data['please_enter_the_range_of_floors_you_are_most_interested_in'])){
                $floor_range =$registraion_data['please_enter_the_range_of_floors_you_are_most_interested_in'];}
        }else{$floor_range='';}

        $comments_section                 = !empty($registraion_data['comments_section']) ? $registraion_data['comments_section'] : '';

        if ($are_you_a_realtor == 1){
            $realtor='Yes';
        }
        else{
            $realtor='No';
        }


        $email='admin@connectassetmanagement.com';
        //$email3='aiubzahid@gmail.com';

        $subject =$subject ='New Reservation!'.' '.$first_name.' '.$last_name.' for '.$development_name;
        $message='<table width="100%" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
                    <tr>
                        <td>YEEHAW! We\'ve got a live one for '.$development_name.' !</td>
                    </tr>
                    <tr>
                         <td height="3"></td>
                    </tr>
                    <tr> 
                        <td style="margin: 0;padding: 0; font-weight: bold;">CONTACT INFORMATION</td>
                    </tr>
                    <tr>
                        <td height="5" style="border-bottom: 1px dashed;width: 150px;"></td>
                    </tr>
                    <tr>
                         <td height="4"></td>
                    </tr>
                    <tr>
                         <td style="line-height: 20px;">
                            First Name:<span>'.' '.$first_name.'</span><br/>
                            Last Name:<span>'.' '.$last_name.'</span><br/>
                            Phone:<span>'.' '.$phone_number.'</span><br/>
                            Email:<span>'.' '.$email_client.'</span><br/>
                            Address:<span>'.' '.$address.'</span><br/>
                            City:<span>'.' '.$city.'</span><br/>
                            Province:<span>'.' '.$province.'</span><br/>
                            Postal Code:<span>'.' '.$postal_code.'</span><br/>
                            Profession:<span>'.' '.$professions.'</span>
                         </td>
                    </tr>
                    <tr>
                         <td height="10"></td>
                    </tr>
                    <tr>
                         <td>
                            <a href="'.$cars_url.'" style="text-decoration: none;background: #0067b3; font-size: 13px; padding: 5px 10px; border-radius: 3px; color: #fff;text-transform: uppercase;">More Here...</a>
                         </td>
                    </tr>
                    <tr>
                         <td height="20"></td>
                    </tr>
                    <tr> 
                        <td style="font-weight: bold;">DEVELOPMENT INFORMATION</td>
                    </tr>
                    <tr>
                        <td height="5" style="border-bottom: 1px dashed;width: 150px;"></td>
                    </tr>
                    <tr>
                         <td height="4"></td>
                    </tr>
                    <tr>
                         <td style="line-height: 20px;">
                            Development Name:<span>' .' '.$development_name.'</span><br/>
                            Choice #1:<span>' .' '.$choice_1_model.'</span><br/>
                            Choice #2:<span>' .' '.$choice_2_model.'</span><br/>
                            Choice #3:<span>' .' '.$choice_3_model.'</span><br/>
                            Floor Plan Range:<span>' .' '.$floor_range.'</span><br/>
                            Parking:<span>' .' '.$require_parking.'</span><br/>
                            Locker:<span>' .' '.$require_a_locker.'</span><br/>
                            Intended Nature of Purchase:<span>' .' '.$nature_of_purchase.'</span><br/>
                            Realtor:<span>' .' '.$realtor.'</span><br/>
                            Comments:<span>' .' '.$comments_section.'</span>
                         </td>
                    </tr>
                    <tr>
                         <td height="10"></td>
                    </tr>
                    <tr> 
                        <td style="font-weight: bold;">ADDITIONAL & SYSTEM INFORMATION</td>
                    </tr>
                    <tr>
                        <td height="5" style="border-bottom: 1px dashed;width: 150px;"></td>
                    </tr>
                    <tr>
                         <td height="4"></td>
                    </tr>
                    <tr>
                         <td style="line-height: 20px;">
                            Date/Time:<span>' .' '.date('Y-m-d H:i:s').'</span><br/>
                            URL Source:<span>' .' '.$pageUrl.'</span><br/>
                            Media Source:<span>' .' '.$media_source.'</span><br/>
                            IP Address:<span>' .' '.$ip.'</span><br/>
                            Browser:<span>' .' '.$browser.'</span>         
                         </td>
                    </tr>
                </table><br/>
                ';
        $message .='Thanks,<br/><br/>';
        $message .='Reggie<br/><br/>';
        $message .='Confidential Information - Obtain Release Permission <br/><br/>               
                            www.connectassetmanagement.com';

        $emailSend = Yii::$app->mailer->compose()
            ->setFrom('reggie@connectassetmanagement.com')
            ->setTo($email)
            ->setSubject($subject)
            ->setHtmlBody($message);
        $emailSend->send();

        /*$emailSend3 = Yii::$app->mailer->compose()
            ->setFrom('reggie@connectassetmanagement.com')
            ->setTo($email3)
            ->setSubject($subject)
            ->setHtmlBody($message);
        $emailSend3->send();*/

    }

}

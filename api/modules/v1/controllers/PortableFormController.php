<?php

namespace api\modules\v1\controllers;

use api\common\maropost\appointments;
use api\common\maropost\maropost;
use api\common\maropost\tags;
use api\helpers\CommonHelper;
use api\models\User;
use api\modules\v1\models\Agent;
use api\modules\v1\models\Contacts;
use api\modules\v1\models\ContactsBrands;
use api\modules\v1\models\ContactsEmailLog;
use api\modules\v1\models\ContactsInteractions;
use api\modules\v1\models\ContactsRegistration;
use api\modules\v1\models\ContactsRsvp;
use api\modules\v1\models\ContactsSysteminfo;
use api\modules\v1\models\ContactsTagsApplied;
use api\modules\v1\models\ContactsTagsNew;
use api\modules\v1\models\ContactsTagsRelation;
use api\modules\v1\models\Developments;
use api\modules\v1\models\MediaLeads;
use Symfony\Component\Console\Helper\ProgressIndicator;
use Yii;
use yii\web\ServerErrorHttpException;
use yii\base\Exception;
use yii\web\HttpException;
use yii\web\Response;
use yii\rest\ActiveController;
use yii\filters\Cors;
use yii\filters\RateLimiter;
use yii\filters\ContentNegotiator;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use api\modules\v1\models\Sample;
use api\modules\v1\models\search\SampleSearch;
use yii\web\NotFoundHttpException;

class PortableFormController extends ActiveController
{
    public $modelClass = Contacts::class;
    public $modelAgentClass = Agent::class;
    public $developmentId;
    private $contactId;
    private $mediaLeadId;
    private $agentId;
    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];

        $behaviors['rateLimiter'] = [
            'class' => RateLimiter::className(),
            'enableRateLimitHeaders' => false,
        ];

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' =>  ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => false,
                'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page', 'X-Pagination-Page-Count', 'X-Pagination-Per-Page', 'X-Pagination-Total-Count', 'Link'],
            ],
        ];

        return $behaviors;
    }

    /**
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();

        unset($actions['create'], $actions['update'], $actions['delete'], $actions['options']);

        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    /**
     * List all records page by page
     *
     * HTTP Verb: GET
     * URL: /companies
     *
     * @return \yii\data\ActiveDataProvider
     * @throws HttpException
     */
    public function prepareDataProvider()
    {

    }

    protected function findAgentModel($id)
    {
        $modelClass = $this->modelAgentClass;
        if (($model = $modelClass::findOne($id)) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException("No records found");
        }
    }
    protected function findModel($id)
    {
        $modelClass = $this->modelClass;
        if (($model = $modelClass::findOne($id)) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException("No records found");
        }
    }
    /*
         * @Function Process : Global Portable html action event.Any web page can be applied and call this api funciotn.
         * API : @Method   : GET
         *       @API URL :https://connectassetmgmt.connectcondos.com/api/web/v1/portable-form/register-contact?access-token=C6VlLzAnLJp-lM8O3v_7DBHH6I6mXql5
    */
    public function actionRegisterContact()
        {
            $first_name         =  !empty($_REQUEST['first_name']) ? $_REQUEST['first_name'] : '';
            $last_name          =  !empty($_REQUEST['last_name']) ? $_REQUEST['last_name'] : '';
            $email              =  !empty($_REQUEST['email']) ? $_REQUEST['email'] : '';
            $phone              =  !empty($_REQUEST['phone']) ? $_REQUEST['phone'] : '';
            $media              =  !empty($_REQUEST['media']) ? $_REQUEST['media'] : '';
            $development_name   =  !empty($_REQUEST['development_name']) ? $_REQUEST['development_name'] : '';
            $url                =  !empty($_REQUEST['url']) ? $_REQUEST['url'] : '';
            $brand              =  !empty($_REQUEST['brand']) ? $_REQUEST['brand'] : '1';
            $event              =  !empty($_REQUEST['event']) ? $_REQUEST['event'] : '';
            $realtor            =  !empty($_REQUEST['realtor']) ? $_REQUEST['realtor'] : '';
            $affiliate_tag      =  !empty($_REQUEST['affiliate_tag']) ? $_REQUEST['affiliate_tag'] : '';
            $form_type          =  !empty($_REQUEST['form_type']) ? $_REQUEST['form_type'] : '';
            $event_on           =  !empty($_REQUEST['on']) ? $_REQUEST['on'] : '';
            $event_at           =  !empty($_REQUEST['at']) ? $_REQUEST['at'] : '';
            $page_name          =  !empty($_REQUEST['page_name']) ? $_REQUEST['page_name'] : '';
            $interaction        =  isset($_REQUEST['interaction']) ? $_REQUEST['interaction'] : '';
            $comments           =  isset($_REQUEST['comments']) ? $_REQUEST['comments'] : '';

            /*Utm source handel.Utm source come :1.From page url or set static*/
            $utm_from_page_url = parse_url($url);
            if (isset($utm_from_page_url['query'])) {
                parse_str($utm_from_page_url['query'], $utm_from_page_url_get);
            }
            if (isset($utm_from_page_url_get['utm_source'])):
                $media_source=$utm_from_page_url_get['utm_source'];
            else:
                $media_source=$media;
            endif;

            /*Evaluation function call and recieved return value*/
            $evaluate_array  =$this->evaluateContactInfo($_REQUEST);
            if (empty($_REQUEST['development_id']) || $_REQUEST['development_id'] == '') :
                $this->developmentId =CommonHelper::getDevelopmentsByDevelopmentName($development_name);
            else:
                $this->developmentId=$_REQUEST['development_id'];
            endif;

            /*Prepare Datalist*/
            $dataList =[
            'first_name'        => ucfirst(strtolower($first_name)),
            'last_name'         => ucfirst(strtolower($last_name)),
            'email'             => strtolower($email),
            'phone'             => preg_replace("/[^0-9]/", '', $phone),
            'media_source'      => $media_source,
            'development_id'    => $this->developmentId,
            'development_name'  => $development_name,
            'page_url'          => preg_replace('#^https?://www.#', '', rtrim($url,'/')),
            'brand_id'          => $brand,
            'event'             => $event,
            'form_type'         => strtolower($form_type),
            'at'                => $event_at,
            'on'                => $event_on,
            'affiliate_tag'     => $affiliate_tag,
            'realtor'           => $realtor,
            'page_name'         => $page_name,
            'interaction_note'  => $interaction,
            'interaction_type'  => '2',
            'comments'          => $comments,
            ];

            if (!empty($dataList)) :
                $contact_system_model=$this->saveSystemLog($dataList,$evaluate_array);
                if ($evaluate_array['email']['vcode'] !='204' && $media !='' && $evaluate_array['form_type']['vcode'] !='315'):
                    $this->saveData($dataList,$contact_system_model,$evaluate_array);
                else:
                    $this->sendFailNoticeEmail($dataList,$contact_system_model);
                endif;
             else :
                throw new HttpException(404, 'No results found.');
            endif;
        }

   //@@@ Contact Information Save To Cars (Event Action All)
    private function saveData($model,$contact_system_model,$evalute_array){
        if (isset($model)) {
            try {
                if(!empty($model)) {
                          $contact_model = $this->saveProspectInfo($contact_system_model, $model);

                          if (!empty($contact_model)) {
                              if ($model['form_type'] == 'registration') {
                                  $this->saveRegistrationInfo($model, $contact_model, $contact_system_model);
                              } elseif ($model['form_type'] == 'rsvp') {
                                  $this->saveRegistrationInfo($model, $contact_model, $contact_system_model);
                                  $this->saveRsvpInfo($contact_model, $contact_system_model);
                              } elseif ($model['form_type'] == 'appointment') {
                                  $this->saveRegistrationInfo($model, $contact_model, $contact_system_model);
                              } elseif ($model['form_type'] == 'reservation') {
                                  $this->saveRegistrationInfo($model, $contact_model, $contact_system_model);
                              } elseif ($model['form_type'] == 'survey') {
                                  $this->saveInteractionInfo($model, $contact_system_model, $contact_model->id);
                              } elseif ($model['form_type'] == 'contest') {
                                  $this->saveInteractionInfo($model, $contact_system_model, $contact_model->id);
                              } elseif ($model['form_type'] == 'contact') {
                                  $this->saveInteractionInfo($model, $contact_system_model, $contact_model->id);
                              }
                              $this->saveBrandInfo($model, $contact_model->id);
                              $this->saveTagsInfo($model, $contact_model->id);
                              if (!empty($contact_system_model->affiliate_tag)){
                                  $this->saveAfflicatTagsInfo($model, $contact_model->id);
                              }
                              $this->updateSystemInfo($contact_model->id, $contact_system_model->id);
                          }
                }
            } catch (Exception $e) {
                throw new ServerErrorHttpException($e->getMessage());
            }
        }
    }

    /*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  "Six Event Function" @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/

    /*Prospect Information Save To Cars (Event Prospect)*/
    private function saveProspectInfo($contact_system_model,$model){

        $contact_type=$update_system_contact_type='';
        if (!empty($contact_system_model->realtor)):
            $contact_type=2;
            $update_system_contact_type='Agent';
        else:
            $contact_type=1;
            $update_system_contact_type='Prospect';
        endif;

        /*Save Contacts Information*/
        $contactModel=new Contacts();
        $checkEmail = Contacts::find()->where(['email' =>$contact_system_model->email])->one();
        if (!empty($checkEmail)) {
            $contactModel = $this->findModel($checkEmail->id);

            if (empty($checkEmail->first_name)) {
                $contactModel->first_name = $contact_system_model->first_name;
            }
            if (empty($checkEmail->last_name)) {
                $contactModel->last_name =$contact_system_model->last_name;
            }
            if (empty($checkEmail->phone)) {
                $contactModel->phone = $contact_system_model->phone;
            }

            if ($checkEmail->contact_type !='2'){
                $contactModel->contact_type =$contact_type;
            }

            $contactModel->agent = 'connect';
            if ($contactModel->save()) {
                $this->contactId=$contactModel->id;
            }
        }
        else{
            $mediaLeadSourceID=$this->saveMediaInfo($contact_system_model);
            $contactModel->first_name =$contact_system_model->first_name;
            $contactModel->last_name =$contact_system_model->last_name;
            $contactModel->email = $contact_system_model->email;
            $contactModel->phone = $contact_system_model->phone;
            $contactModel->lead_source = $mediaLeadSourceID;
            $contactModel->contact_type = $contact_type;
            $contactModel->agent = 'connect';
            if($contactModel->save()){
              $this->contactId=$contactModel->id;
            }
        }
        /*Update System Log Contact Type*/
        $this->updateSystemContactType($update_system_contact_type,$contact_system_model->id);

        /*subscribe data send to maropost*/
        if ($contact_system_model->form_type=='subscribe'){
            $this->subscribe_form_to_maropost($model);
        }

        return $contactModel;
    }

    /*Registration Information Save To Cars (Event Registration)*/
    private function saveRegistrationInfo($model,$contact_model,$contact_system_model){
        $baseUrl = Yii::getAlias('@baseUrl');
        $mediaLeadSourceID=$this->saveMediaInfo($contact_system_model);
        $contactsregistration = new ContactsRegistration();
        $contactsregistration->contact_id =$contact_model->id;
        $contactsregistration->development_id =$contact_system_model->development_id;
        $contactsregistration->reg_source = $mediaLeadSourceID;
        $contactsregistration->reg_datetime = date('Y-m-d H:i:s');
        $contactsregistration->status_date = date('Y-m-d H:i:s');
        $contactsregistration->system_log_id =$contact_system_model->id;
        if (!$contactsregistration->save(false)) {
            throw new Exception(json_encode($contactsregistration->errors));
        }
        /*Registration data save to maropost and new lead email fire to assigned user*/
        if ($contact_system_model->form_type=='registration' || $contact_system_model->form_type=='appointment' || $contact_system_model->form_type=='rsvp'){
            $this->customerMail($model,$contact_system_model->form_type);
            $cars_contact_url=$baseUrl . '/backend/web/contacts/contacts-registration/update?id=' . $contactsregistration->id;
            $this->sendNewLeadEmail($contact_system_model,$cars_contact_url);
        }
        if ($contact_system_model->form_type=='appointment'){
            $this->send_data_to_appointment_list($contact_system_model);
        }
    }

    /*Interaction Information (Event Interaction)*/
    private function saveInteractionInfo($model,$contact_system_model,$contact_id){
        /*interaction record*/
        $baseUrl = Yii::getAlias('@baseUrl');

        $contactInteractionInfo = new ContactsInteractions();
        $contactInteractionInfo->contact_id =$contact_id;
        $contactInteractionInfo->interaction_note =$model['interaction_note'];
        $contactInteractionInfo->interaction_type = $model['interaction_type'];
        $contactInteractionInfo->interaction_datetime =date('Y-m-d H:i:s');
        $contactInteractionInfo->created_at =date('Y-m-d H:i:s');
        $contactInteractionInfo->created_by =39;
        if (!$contactInteractionInfo->save(false)) {
            throw new Exception(json_encode($contactInteractionInfo->errors));
        }
        /*Contact data sent to maropost and reggie email fire from maropost*/
        if ($model['form_type']=='contact'){
            $this->contact_form_to_maropost($model);
            $cars_contact_url=$baseUrl . '/backend/web/contacts/contacts/view?id=' . $contact_id;
            $this->send_reggie_email_contact_type($contact_system_model,$cars_contact_url,$model['interaction_note']);
        }
    }

    /*Rsvp Save To Cars (Event Rsvp)*/
    private function saveRsvpInfo($contact_model,$contact_system_model){
        $rsvp_status='';
        if ($contact_system_model->event=='true'){$rsvp_status=2;}
        else{$rsvp_status=1;}

        $contactsrsvp = new ContactsRsvp();
        $mediaLeadSourceID=$this->saveMediaInfo($contact_system_model);
        $contactsrsvp->contact_id = $contact_model->id;
        $contactsrsvp->development_id =$contact_system_model->development_id;
        $contactsrsvp->event_id =1;
        $contactsrsvp->rsvp_source =$mediaLeadSourceID;
        $contactsrsvp->rsvp_date = date('Y-m-d H:i:s');
        $contactsrsvp->rsvp_status =$rsvp_status;
        $contactsrsvp->rsvp_event =$contact_system_model->event;
        if (!$contactsrsvp->save(false)) {
            throw new Exception(json_encode($contactsrsvp->errors));
        }
    }

    /*Media Information Save To Cars*/
    private function saveMediaInfo($contact_system_model){
        $mediaLeadSource=new MediaLeads();
        $checkLeadName=MediaLeads::find()->where(['lead_source_name' => $contact_system_model->media_source])->one();
        if (empty($checkLeadName)) {
            $mediaLeadSource->lead_source_name = $contact_system_model->media_source;
            $mediaLeadSource->created_at = date('Y-m-d H:i:s');
            $mediaLeadSource->status = 1;
            if (!$mediaLeadSource->save(false)) {
                throw new Exception(json_encode($mediaLeadSource->errors));
            }
            $this->mediaLeadId = $mediaLeadSource->id;
        } else {
            $this->mediaLeadId  = $checkLeadName->id;
        }
        return $this->mediaLeadId ;
    }

    /*Brand Information Save To Cars*/
    private function saveBrandInfo($model,$contact_id){
        $brandName = ContactsBrands::find()->where(['contact_id' => $contact_id])->andWhere(['brand_id' => $model['brand_id']])->one();
        if (empty($brandName)) {
            $brandInfo = new ContactsBrands();
            $brandInfo->contact_id = $contact_id;
            $brandInfo->brand_id =$model['brand_id'];
            $brandInfo->applied_date = date('Y-m-d H:i:s');
            if (!$brandInfo->save(false)) {
                throw new Exception(json_encode($brandInfo->errors));
            }
        }
    }

    /*Tags Information Save To Cars*/
    private function saveTagsInfo($model,$contact_id){

        $tag_name=$develomentList=$development_name='';
        if (!empty($model['development_id'])) {
            $develomentList = Developments::find()->where(['development_id' => $model['development_id']])->one();
            if (!empty($develomentList)){
                $development_name='-'.$develomentList->development_name;
            }
        }
        //Realtor Check
        if (!empty($model['realtor'])) {
            $contact_type='realtor';
        }
        else{
            $contact_type='prospect';
        }
         //Tag Name Configure
        if ($model['form_type']=='rsvp' || $model['form_type']=='appointment'):
            if ($model['event']=='true'):
                $tag_name=$model['form_type'].'-'.'yes'.'-'.$contact_type.strtolower($development_name);
            else:
                $tag_name=$model['form_type'].'-'.'no'.'-'.$contact_type.strtolower($development_name);
            endif;
        else:
           $tag_name=$model['form_type'].'-'.$contact_type.strtolower($development_name);
        endif;

        //Save contact tag
        $contactsTag = new ContactsTagsNew();
        $contactsTag->tag_name =$tag_name;
        $contactsTag->tag_category_id=1;
        $checkTags = ContactsTagsNew::find()->where(['tag_name' => $contactsTag->tag_name])->one();

        if (empty($checkTags)){
            if ($contactsTag->save(false)) {
                //save data relation table
                $tagRelation=new ContactsTagsRelation();
                $tagRelation->contact_id=$contact_id;
                $tagRelation->tag_id=$contactsTag->id;
                $tagRelation->date_applied= date('Y-m-d H:i:s');
                if (!$tagRelation->save(false)) {
                    throw new Exception(json_encode($tagRelation->errors));
                }
                //save data to applied tag
                $tagApplied=new ContactsTagsApplied();
                $tagApplied->tag_id=$contactsTag->id;
                if (!$tagApplied->save(false)) {
                    throw new Exception(json_encode($tagApplied->errors));
                }

            }
        }
        else{
            $tagRelation=new ContactsTagsRelation();
            $tagRelation->contact_id=$contact_id;
            $tagRelation->tag_id=$checkTags->id;
            $tagRelation->date_applied= date('Y-m-d H:i:s');
            if (!$tagRelation->save(false)) {
                throw new Exception(json_encode($tagRelation->errors));
            }
            $tagApplied=new ContactsTagsApplied();
            $tagApplied->tag_id=$checkTags->id;
            if (!$tagApplied->save(false)) {
                throw new Exception(json_encode($tagApplied->errors));
            }
        }
    }

    /*Tags Information Save To Cars*/
    private function saveAfflicatTagsInfo($model,$contact_id){
        if (!empty($model['affiliate_tag'])) {
            //Save contact tag
            $tag_name=strtolower($model['affiliate_tag']);
            $contactsTag = new ContactsTagsNew();
            $contactsTag->tag_name = $tag_name;
            $contactsTag->tag_category_id = 1;
            $checkTags = ContactsTagsNew::find()->where(['tag_name' => $contactsTag->tag_name])->one();

            if (empty($checkTags)) {
                if ($contactsTag->save(false)) {
                    //save data relation table
                    $tagRelation = new ContactsTagsRelation();
                    $tagRelation->contact_id = $contact_id;
                    $tagRelation->tag_id = $contactsTag->id;
                    $tagRelation->date_applied = date('Y-m-d H:i:s');
                    if (!$tagRelation->save(false)) {
                        throw new Exception(json_encode($tagRelation->errors));
                    }
                    //save data to applied tag
                    $tagApplied = new ContactsTagsApplied();
                    $tagApplied->tag_id = $contactsTag->id;
                    if (!$tagApplied->save(false)) {
                        throw new Exception(json_encode($tagApplied->errors));
                    }

                }
            } else {
                $tagRelation = new ContactsTagsRelation();
                $tagRelation->contact_id = $contact_id;
                $tagRelation->tag_id = $checkTags->id;
                $tagRelation->date_applied = date('Y-m-d H:i:s');
                if (!$tagRelation->save(false)) {
                    throw new Exception(json_encode($tagRelation->errors));
                }
                $tagApplied = new ContactsTagsApplied();
                $tagApplied->tag_id = $checkTags->id;
                if (!$tagApplied->save(false)) {
                    throw new Exception(json_encode($tagApplied->errors));
                }
            }
        }
    }

    /*Update System Information*/
    private function updateSystemInfo($contact_id,$system_log_id){
        // System information
        $checkSystemInfo = ContactsSysteminfo::find()->where(['id' =>$system_log_id])->one();
        if (!empty($checkSystemInfo)) {
            $checkSystemInfo->contact_id = $contact_id;
            if (!$checkSystemInfo->save(false)) {
                throw new Exception(json_encode($checkSystemInfo->errors));
            }
        }
    }

    /*Update System Contact Type Information*/
    private function updateSystemContactType($contact_type,$system_log_id){
        $checkSystemInfo = ContactsSysteminfo::find()->where(['id' =>$system_log_id])->one();
        if (!empty($checkSystemInfo)) {
            $checkSystemInfo->contact_type =$contact_type;
            if (!$checkSystemInfo->save(false)) {
                throw new Exception(json_encode($checkSystemInfo->errors));
            }
        }
    }

    /*System Information Save To Cars*/
    public function saveSystemLog($dataList,$evaluate_array){
            /*if event on & event at not null then from type updated to 'appointment'*/
            if ($evaluate_array['event_on']['value']!='' && $evaluate_array['event_at']['value'] !=''):
                $form_type='appointment';
            else:
                $form_type=$dataList['form_type'];
            endif;

            /*Development Name*/
            $development_name='';
            if (!empty($dataList['development_id'])) {
                $develomentList = Developments::find()
                    ->where(['development_id' => $dataList['development_id']])
                    ->andWhere(['not', ['development_id' => null]])
                    ->one();
                if (!empty($develomentList)) {
                    $development_name = $develomentList->development_name;
                } else {
                    $development_name =$dataList['development_name'];
                }
            }
            else{
                $development_name =$dataList['development_name'];
            }

            /*Save System Information*/
             $json_data=json_encode($evaluate_array);
             $browser=$this->getBrowserInfo(); //Browser Informaiton Get Dynamically
             $contactsSystemInfo = new ContactsSysteminfo(); //Contact System Information Model Get
             $contactsSystemInfo->first_name        = $dataList['first_name'];
             $contactsSystemInfo->last_name         = $dataList['last_name'];
             $contactsSystemInfo->email             = $dataList['email'];
             $contactsSystemInfo->phone             = $dataList['phone'];
             $contactsSystemInfo->media_source      = $dataList['media_source'];
             $contactsSystemInfo->development_id    = $dataList['development_id'];
             $contactsSystemInfo->development_name  = $development_name;
             $contactsSystemInfo->page_name         = $dataList['page_name'];
             $contactsSystemInfo->page_url          =$dataList['page_url'];
             $contactsSystemInfo->brand_id          =$dataList['brand_id'];
             $contactsSystemInfo->event             =$dataList['event'];
             $contactsSystemInfo->form_type         =$form_type;
             $contactsSystemInfo->event_at          =$dataList['at'];
             $contactsSystemInfo->event_on          =$dataList['on'];
             $contactsSystemInfo->affiliate_tag     =$dataList['affiliate_tag'];
             $contactsSystemInfo->realtor           =$dataList['realtor'];
             $contactsSystemInfo->ip_address        = $browser['ip_address'];
             $contactsSystemInfo->browser           = $browser['name'];
             $contactsSystemInfo->operating         = $browser['platform'];
             $contactsSystemInfo->device            = $browser['device'];
             $contactsSystemInfo->visit_date        = date('Y-m-d H:i:s');
             $contactsSystemInfo->log_create_status =0;
             $contactsSystemInfo->log_data          = $json_data;
             $contactsSystemInfo->comment           = $dataList['comments'];
             if (!$contactsSystemInfo->save(false)) {
                 throw new Exception(json_encode($contactsSystemInfo->errors));
             }
             return $contactsSystemInfo;
    }

    /*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  "Maropost Action" @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
    /* Contact information send to maropost*/
    private function contact_form_to_maropost($registraion_data){
        //@@Tag Configuration
        $development_name='';
        if (!empty($registraion_data['development_id'])) {
            $develomentList = Developments::find()->where(['development_id' => $registraion_data['development_id']])->andWhere(['not', ['development_id' => null]])->one();
            if (!empty($develomentList)){
                $development_name='-'.$develomentList->development_name;
            }
        }
        $tag_name=$registraion_data['form_type'].'-'.'prospect'.$development_name;
        $tags=new tags();
        $array = array(
            "tags" => array(
                "name" =>$tag_name
            )
        );
        $tags->post_tags($array);

        $registraion_data['affiliate_tag_maropost']='';
        if (!empty($registraion_data['affiliate_tag'])){
            $array2 = array(
                "tags" => array(
                    "name" =>strtolower($registraion_data['affiliate_tag'])
                )
            );
            $tags->post_tags($array2);
            $registraion_data['affiliate_tag_maropost']=strtolower($registraion_data['affiliate_tag']);
        }
        //@@End Tag Configuration

        $contactsNew=new maropost();
        $contactsNew = $contactsNew->contacts();

        $get_contact_by_email   = $contactsNew->get_contact_by_contact_email($registraion_data['email']);

        $user_existing_status   = 1;
        $subscriptions_status   = 1;
        if($get_contact_by_email['http_status'] == 404){
            $user_existing_status = 0;
        }else{
            $list_subscriptions = $get_contact_by_email['list_subscriptions'];
            foreach ($list_subscriptions as $subscriptions){
                //
            }
            if($subscriptions['status'] == 'Unsubscribed'){
                $subscriptions_status = 0;
            }
        }

        if($user_existing_status == 1){

            if($subscriptions_status == 1){
                $arrayNew = array(
                    'list_id' => 1,
                    'contact' =>
                        array(
                            'first_name' 	=> $registraion_data['first_name'],
                            'last_name' 	=> $registraion_data['last_name'],
                            'email' 		=> $registraion_data['email'],
                            'subscribe' 	=> 'true',
                            'custom_field'  =>
                                array(
                                    'message_1' 	    => $registraion_data['interaction_note'],
                                    'company' 		    => 'CONNECT asset management',
                                    'development_name'  => 'CONNECT asset management',
                                    'development_reg_existing' 	=> time(),
                                ),
                            'add_tags'  =>
                                array(
                                    $tag_name,
                                    $registraion_data['affiliate_tag_maropost']
                                )

                        ));
            }

        }else if($user_existing_status == 0){

            $arrayNew = array(
                'list_id' => 1,
                'contact' =>
                    array(
                        'first_name' 	=> $registraion_data['first_name'],
                        'last_name' 	=> $registraion_data['last_name'],
                        'email' 		=> $registraion_data['email'],
                        'subscribe' 	=> 'true',
                        'custom_field'  =>
                            array(
                                'message_1' 	=> $registraion_data['interaction_note'],
                                'company' 		=> 'CONNECT asset management',
                                'development_name'  => 'CONNECT asset management',
                                'development_reg_new_user' 	=> time(),
                            ),
                        'add_tags'  =>
                            array(
                                $tag_name,
                                $registraion_data['affiliate_tag_maropost']
                            )

                    ));

        }
        if( empty($arrayNew) ){

            $arrayNew = array(
                'list_id' => 1,
                'contact' =>
                    array(
                        'first_name' 	=> $registraion_data['first_name'],
                        'last_name' 	=> $registraion_data['last_name'],
                        'email' 		=> $registraion_data['email'],
                        'subscribe' 	=> 'true',
                        'custom_field'  =>
                            array(
                                'message_1' 	=> $registraion_data['interaction_note'],
                                'company' 		=> 'CONNECT asset management',
                            ),
                        'add_tags'  =>
                            array(
                                $tag_name,
                                $registraion_data['affiliate_tag_maropost']
                            )

                    ));
        }

        $arrayNewsletter = $arrayNew;
        if($arrayNewsletter['list_id'] == 1){
            $arrayNewsletter['list_id'] = 3;
            $contactsNew->post_new_contact_into_list($arrayNewsletter);
        }
        $response=$contactsNew->post_new_contact_into_list($arrayNew);
    }

    /*Subscribe form to maropost*/
    public function subscribe_form_to_maropost($registraion_data){
        $baseAuthhorId = Yii::getAlias('@baseAuthhor');
        $checkEmailAgent =CommonHelper::getAgentByEmail($registraion_data['email']);
        if (!empty($checkEmailAgent)){
            $registraion_data['realtor']='1';
        }

        $tag_name=$develomentList=$development_name=$assign_user_name='';
        $assign_id=$baseAuthhorId;
        if (!empty($registraion_data['development_id'])) {
            $develomentList = Developments::find()->where(['development_id' => $registraion_data['development_id']])->andWhere(['not', ['development_id' => null]])->one();
            if (!empty($develomentList)){
                $development_name='-'.$develomentList->development_name;
                $assigned_id=$develomentList->assignment_id;
                if ($assigned_id==0){
                    $assign_id=$baseAuthhorId;
                }else{
                    $assign_id=$assigned_id;
                }
            }
        }

        $assign_user=User::find()->where(['id'=>$assign_id])->one();
        $assign_user_name=$assign_user->first_name.' '.$assign_user->last_name;

        if (!empty($registraion_data['realtor'])) {
            $contact_type='realtor';
        }
        else{
            $contact_type='prospect';
        }

        $tag_name=$registraion_data['form_type'].'-'.$contact_type. strtolower($development_name);

        $realtor_='';
        if (!empty($registraion_data['realtor'])){
            $realtor_       = 'true';
            $prospect       = 'False';
            $realtor_tag    = 'Realtor';
            $realtor        = 1;
            $list_id        = 17;
        }else{
            $realtor_       = 'False';
            $prospect       = 'true';
            $realtor_tag    = 'Prospect';
            $realtor        = 0;
            $list_id        = 1;
        }

        $tags=new tags();
        //tag created if tag not exist in maropost
        $registraion_data['affiliate_tag_maropost']='';
        if (!empty($registraion_data['affiliate_tag'])){
            $array2 = array(
                "tags" => array(
                    "name" =>strtolower($registraion_data['affiliate_tag'])
                )
            );
            $tags->post_tags($array2);
            $registraion_data['affiliate_tag_maropost']=strtolower($registraion_data['affiliate_tag']);
        }
        //tag2
        $array1 = array(
            "tags" => array(
                "name"=>$tag_name
            )
        );
        $tags->post_tags($array1);


        $contactsNew=new maropost();
        $contactsNew = $contactsNew->contacts();

        $get_contact_by_email   = $contactsNew->get_contact_by_contact_email($registraion_data['email']);

        $user_existing_status   = 1;
        $subscriptions_status   = 1;
        if($get_contact_by_email['http_status'] == 404){
            $user_existing_status = 0;
        }else{
            $list_subscriptions = $get_contact_by_email['list_subscriptions'];
            foreach ($list_subscriptions as $subscriptions){
                //
            }
            if($subscriptions['status'] == 'Unsubscribed'){
                $subscriptions_status = 0;
            }
        }

        if($realtor == 1){

            $arrayNew = array(
                'list_id' => $list_id,
                'contact' =>
                    array(
                        'first_name' 	=> $registraion_data['first_name'],
                        'last_name' 	=> $registraion_data['last_name'],
                        'email' 		=> $registraion_data['email'],
                        'subscribe' 	=> 'true',
                        'custom_field'  =>
                            array(
                                'phone_1' 		        => $registraion_data['phone'],
                                'company' 		        => 'CONNECT asset management',
                                'realtor' 		        => $realtor_,
                                'prospect' 		        => $prospect,
                                'assigned_to' 		    => $assign_user_name,
                                'development_name' 	    => 'CONNECT asset management',
                                'reference_link' 	    => $registraion_data['page_url'],
                                'development_reg_realtor' 	=> time(),
                            ),
                        'add_tags'  =>
                            array(
                                $realtor_tag,
                                $tag_name,
                                $registraion_data['affiliate_tag_maropost']
                            )
                    ));

        }else{

            if($user_existing_status == 1){

                if($subscriptions_status == 1){
                    $arrayNew = array(
                        'list_id' => $list_id,
                        'contact' =>
                            array(
                                'first_name' 	=> $registraion_data['first_name'],
                                'last_name' 	=> $registraion_data['last_name'],
                                'email' 		=> $registraion_data['email'],
                                'subscribe' 	=> 'true',
                                'custom_field'  =>
                                    array(
                                        'phone_1' 		        => $registraion_data['phone'],
                                        'company' 		        => 'CONNECT asset management',
                                        'realtor' 		        => $realtor_,
                                        'prospect' 		        => $prospect,
                                        'assigned_to' 		    => $assign_user_name,
                                        'development_name' 	    => 'CONNECT asset management',
                                        'reference_link' 	    => $registraion_data['page_url'],
                                        'development_reg_existing' 	=> time(),
                                    ),
                                'add_tags'  =>
                                    array(
                                        $realtor_tag,
                                        $tag_name,
                                        $registraion_data['affiliate_tag_maropost']
                                    )
                            ));
                }

            }else if($user_existing_status == 0){

                $arrayNew = array(
                    'list_id' => $list_id,
                    'contact' =>
                        array(
                            'first_name' 	=> $registraion_data['first_name'],
                            'last_name' 	=> $registraion_data['last_name'],
                            'email' 		=> $registraion_data['email'],
                            'subscribe' 	=> 'true',
                            'custom_field'  =>
                                array(
                                    'phone_1' 		        => $registraion_data['phone'],
                                    'company' 		        => 'CONNECT asset management',
                                    'realtor' 		        => $realtor_,
                                    'assigned_to' 		    => $assign_user_name,
                                    'prospect' 		        => $prospect,
                                    'development_name' 	    => 'CONNECT asset management',
                                    'reference_link' 	    => $registraion_data['page_url'],
                                    'development_reg_new_user' 	=> time(),
                                ),
                            'add_tags'  =>
                                array(
                                    $realtor_tag,
                                    $tag_name,
                                    $registraion_data['affiliate_tag_maropost']
                                )
                        ));

            }

        }

        if( empty($arrayNew) ){

            $arrayNew = array(
                'list_id' => $list_id,
                'contact' =>
                    array(
                        'first_name' 	=> $registraion_data['first_name'],
                        'last_name' 	=> $registraion_data['last_name'],
                        'email' 		=> $registraion_data['email'],
                        'subscribe' 	=> 'true',
                        'custom_field'  =>
                            array(
                                'phone_1' 		        => $registraion_data['phone'],
                                'company' 		        => 'CONNECT asset management',
                                'realtor' 		        => $realtor_,
                                'prospect' 		        => $prospect,
                                'assigned_to' 		    => $assign_user_name,
                                'reference_link' 	    => $registraion_data['page_url'],
                            ),
                        'add_tags'  =>
                            array(
                                $realtor_tag,
                                $tag_name,
                                $registraion_data['affiliate_tag_maropost']
                            )
                    ));

        }

        $arrayNewsletter = $arrayNew;
        if($arrayNewsletter['list_id'] == 1){
            $arrayNewsletter['list_id'] = 3;
            $contactsNew->post_new_contact_into_list($arrayNewsletter);
        }
        $response=$contactsNew->post_new_contact_into_list($arrayNew);
    }

    /* Mail Functionality for Customer*/
    public  function  customerMail($registraion_data,$form_type){
        $baseAuthhorId = Yii::getAlias('@baseAuthhor');
        $contact_type=$infusionsoftTag='';
        if (!empty($registraion_data['realtor'])) {
            $contact_type='realtor';
        }
        else{
            $contact_type='prospect';
        }

        //Development Id check
        if (!empty($registraion_data['development_id'])) {
            $develomentList = Developments::find()->where(['development_id' => $registraion_data['development_id']])->andWhere(['not', ['development_id' => null]])->one();
            if (!empty($develomentList)) {
                $projectTitle = $develomentList->development_name;
                $project_sales_status = $develomentList->sales_status;
                $projectName = $develomentList->development_name;
                $projectUrl = $registraion_data['page_url'];
                //Tag Configure
                if ($form_type=='rsvp' || $form_type=='appointment'):
                    if ($registraion_data['event']=='true'):
                        $registraion_data['infusionsoftTag']=$form_type.'-'.'yes'.'-'.$contact_type.'-'. strtolower($develomentList->development_name);
                    else:
                        $registraion_data['infusionsoftTag']=$form_type.'-'.'no'.'-'.$contact_type.'-'. strtolower($develomentList->development_name);
                    endif;
                else:
                    $registraion_data['infusionsoftTag']=$form_type.'-'.$contact_type.'-'. strtolower($develomentList->development_name);
                endif;

            } else {
                $projectTitle =$registraion_data['development_name'];
                $project_sales_status ='comingsoon';
                $projectName =$registraion_data['development_name'];
                $projectUrl = $registraion_data['page_url'];
                $tag_development_name=!empty($registraion_data['development_name'])?'-'.$registraion_data['development_name']:'';
                //Tag Configure
                if ($form_type=='rsvp' || $form_type=='appointment'):
                    if ($registraion_data['event']=='true'):
                        $registraion_data['infusionsoftTag']=$form_type.'-'.'yes'.'-'.$contact_type.strtolower($tag_development_name);
                    else:
                        $registraion_data['infusionsoftTag']=$form_type.'-'.'no'.'-'.$contact_type.strtolower($tag_development_name);
                    endif;
                else:
                    $registraion_data['infusionsoftTag']=$form_type.'-'.$contact_type.strtolower($tag_development_name);
                endif;
            }
        }
        else{
            $projectTitle =$registraion_data['development_name'];
            $project_sales_status ='comingsoon';
            $projectName =$registraion_data['development_name'];
            $projectUrl = $registraion_data['page_url'];
            $tag_development_name=!empty($registraion_data['development_name'])?'-'.$registraion_data['development_name']:'';
            //Tag Configure
            if ($form_type=='rsvp' || $form_type=='appointment'):
                if ($registraion_data['event']=='true'):
                    $registraion_data['infusionsoftTag']=$form_type.'-'.'yes'.'-'.$contact_type.strtolower($tag_development_name);
                else:
                    $registraion_data['infusionsoftTag']=$form_type.'-'.'no'.'-'.$contact_type.strtolower($tag_development_name);
                endif;
            else:
                $registraion_data['infusionsoftTag']=$form_type.'-'.$contact_type.strtolower($tag_development_name);
            endif;
        }

        //assigned user info
        $assign_user_name='';
        if (!empty($registraion_data['development_id'])) {
            $develomentList = Developments::find()->where(['development_id' => $registraion_data['development_id']])->andWhere(['not', ['development_id' => null]])->one();
            if (!empty($develomentList)){
                $assigned_id=$develomentList->assignment_id;
                if ($assigned_id==0){
                    $assign_id=$baseAuthhorId;
                }else{
                    $assign_id=$assigned_id;
                }
            }else{
                $assign_id=$baseAuthhorId;
            }
            $assign_user=User::find()->where(['id'=>$assign_id])->one();
            $assign_user_name=$assign_user->first_name.' '.$assign_user->last_name;
        }

        $realtor = '';
        $prospect = '';
        $realtor_tag = '';
        $checkEmailAgent =CommonHelper::getAgentByEmail($registraion_data['email']);
        if (!empty($registraion_data['realtor'])) {
            $realtor_ = 'true';
            $prospect = 'False';
            $realtor = 1;
            $realtor_tag = 'Realtor';
            $list_id = 17;
        } else {
            if (!empty($checkEmailAgent)){
                $realtor_ = 'true';
                $prospect = 'False';
                $realtor = 1;
                $realtor_tag = 'Realtor';
                $list_id = 17;
            }else{
            $realtor_ = 'False';
            $prospect = 'true';
            $realtor_tag = 'Prospect';
            $realtor = 0;
            $list_id = 1;
            }
        }

        $checkEmailAgent =CommonHelper::getAgentByEmail($registraion_data['email']);

        //get sales_status
        $sales_status = '';

        if ($project_sales_status == 'comingsoon') {
            $sales_status = 'Registration';
        } else if ($project_sales_status == 'active') {
            $sales_status = 'Available';
        } else if ($project_sales_status == 'archived') {
            $sales_status = 'Sold Out';
        }
        else if ($project_sales_status == 'vip') {
            $sales_status = 'Vip';
        }

        $projectDownloadsEnabled =0;
        $registraion_data['media_source']='Original Media Source -'.$registraion_data['media_source'];


        $tags=new tags();
        //tag created if tag not exist in maropost
        $array = array(
            "tags" => array(
                "name" =>$registraion_data['infusionsoftTag']
            )
        );
        $tags->post_tags($array);


        $array1 = array(
            "tags" => array(
                "name" =>$registraion_data['media_source']
            )
        );
        $tags->post_tags($array1);

        $registraion_data['affiliate_tag_maropost']='';
        if (!empty($registraion_data['affiliate_tag'])){
            $array2 = array(
                "tags" => array(
                    "name" =>strtolower($registraion_data['affiliate_tag'])
                )
            );
            $tags->post_tags($array2);
            $registraion_data['affiliate_tag_maropost']=strtolower($registraion_data['affiliate_tag']);
        }

        /*
         * data prepare for maropost
         */
        $contactsNew=new maropost();
        $contactsNew = $contactsNew->contacts();

        $get_contact_by_email = $contactsNew->get_contact_by_contact_email($registraion_data['email']);


        //echo '<pre>'; print_r($get_contact_by_email); exit();

        $user_existing_status = 1;
        $subscriptions_status = 1;


        if ($get_contact_by_email['http_status'] == 404) {
            $user_existing_status = 0;
        } else {

            $list_subscriptions = $get_contact_by_email['list_subscriptions'];

            foreach ($list_subscriptions as $subscriptions) {
                //
            }
            if ($subscriptions['status'] == 'Unsubscribed') {
                $subscriptions_status = 0;
            }
        }


        if ($sales_status == 'Registration') {

            if ($realtor == 1) {

                $arrayNew = array(
                    'list_id' => $list_id,
                    'contact' =>
                        array(
                            'first_name' => $registraion_data['first_name'],
                            'last_name' => $registraion_data['last_name'],
                            'email' => $registraion_data['email'],
                            'subscribe' => 'true',
                            'custom_field' =>
                                array(
                                    'phone_1' => $registraion_data['phone'],
                                    'company' => 'CONNECT asset management',
                                    'realtor' => $realtor_,
                                    'prospect' => $prospect,
                                    'assigned_to' => $assign_user_name,
                                    'comment_question' => $projectName,
                                    'development_name' => $projectTitle,
                                    'reference_link' => $projectUrl,
                                    'development_reg_realtor' => time(),
                                ),
                            'add_tags' =>
                                array(
                                    $registraion_data['infusionsoftTag'],
                                    $registraion_data['media_source'],
                                    $registraion_data['affiliate_tag_maropost'],
                                    $realtor_tag
                                )
                        ));


            } else {

                if ($user_existing_status == 1) {

                    if ($subscriptions_status == 1) {
                        $arrayNew = array(
                            'list_id' => $list_id,
                            'contact' =>
                                array(
                                    'first_name' => $registraion_data['first_name'],
                                    'last_name' => $registraion_data['last_name'],
                                    'email' => $registraion_data['email'],
                                    'subscribe' => 'true',
                                    'custom_field' =>
                                        array(
                                            'phone_1' => $registraion_data['phone'],
                                            'company' => 'CONNECT asset management',
                                            'realtor' => $realtor_,
                                            'prospect' => $prospect,
                                            'assigned_to' => $assign_user_name,
                                            'comment_question' => $projectName,
                                            'development_name' => $projectTitle,
                                            'reference_link' => $projectUrl,
                                            'development_reg_existing' => time(),
                                        ),
                                    'add_tags' =>
                                        array(
                                            $registraion_data['infusionsoftTag'],
                                            $registraion_data['media_source'],
                                            $registraion_data['affiliate_tag_maropost'],
                                            $realtor_tag
                                        )
                                ));
                    }

                } else if ($user_existing_status == 0) {
                    $arrayNew = array(
                        'list_id' => $list_id,
                        'contact' =>
                            array(
                                'first_name' => $registraion_data['first_name'],
                                'last_name' => $registraion_data['last_name'],
                                'email' => $registraion_data['email'],
                                'subscribe' => 'true',
                                'custom_field' =>
                                    array(
                                        'phone_1' => $registraion_data['phone'],
                                        'company' => 'CONNECT asset management',
                                        'realtor' => $realtor_,
                                        'prospect' => $prospect,
                                        'assigned_to' => $assign_user_name,
                                        'comment_question' => $projectName,
                                        'development_name' => $projectTitle,
                                        'reference_link' => $projectUrl,
                                        'development_reg_new_user' => time(),
                                    ),
                                'add_tags' =>
                                    array(
                                        $registraion_data['infusionsoftTag'],
                                        $registraion_data['media_source'],
                                        $registraion_data['affiliate_tag_maropost'],
                                        $realtor_tag
                                    )
                            ));
                }


            }

        } else if ($sales_status == 'Available') {

            if ($projectDownloadsEnabled != 1) {

                if ($realtor == 1) {

                    $arrayNew = array(
                        'list_id' => $list_id,
                        'contact' =>
                            array(
                                'first_name' => $registraion_data['first_name'],
                                'last_name' => $registraion_data['last_name'],
                                'email' => $registraion_data['email'],
                                'subscribe' => 'true',
                                'custom_field' =>
                                    array(
                                        'phone_1' => $registraion_data['phone'],
                                        'company' => 'CONNECT asset management',
                                        'realtor' => $realtor_,
                                        'assigned_to' => $assign_user_name,
                                        'prospect' => $prospect,
                                        'comment_question' => $projectName,
                                        'development_name' => $projectTitle,
                                        'reference_link' => $projectUrl,
                                        'development_reg_realtor' => time(),
                                    ),
                                'add_tags' =>
                                    array(
                                        $registraion_data['infusionsoftTag'],
                                        $registraion_data['media_source'],
                                        $registraion_data['affiliate_tag_maropost'],
                                        $realtor_tag
                                    )
                            ));


                } else {

                    if ($user_existing_status == 1) {

                        if ($subscriptions_status == 1) {
                            $arrayNew = array(
                                'list_id' => $list_id,
                                'contact' =>
                                    array(
                                        'first_name' => $registraion_data['first_name'],
                                        'last_name' => $registraion_data['last_name'],
                                        'email' => $registraion_data['email'],
                                        'subscribe' => 'true',
                                        'custom_field' =>
                                            array(
                                                'phone_1' => $registraion_data['phone'],
                                                'company' => 'CONNECT asset management',
                                                'realtor' => $realtor_,
                                                'prospect' => $prospect,
                                                'assigned_to' => $assign_user_name,
                                                'comment_question' => $projectName,
                                                'development_name' => $projectTitle,
                                                'reference_link' => $projectUrl,
                                                'development_reg_existing' => time(),
                                            ),
                                        'add_tags' =>
                                            array(
                                                $registraion_data['infusionsoftTag'],
                                                $registraion_data['media_source'],
                                                $registraion_data['affiliate_tag_maropost'],
                                                $realtor_tag
                                            )
                                    ));
                        }

                    } else if ($user_existing_status == 0) {
                        $arrayNew = array(
                            'list_id' => $list_id,
                            'contact' =>
                                array(
                                    'first_name' => $registraion_data['first_name'],
                                    'last_name' => $registraion_data['last_name'],
                                    'email' => $registraion_data['email'],
                                    'subscribe' => 'true',
                                    'custom_field' =>
                                        array(
                                            'phone_1' => $registraion_data['phone'],
                                            'company' => 'CONNECT asset management',
                                            'realtor' => $realtor_,
                                            'prospect' => $prospect,
                                            'assigned_to' => $assign_user_name,
                                            'comment_question' => $projectName,
                                            'development_name' => $projectTitle,
                                            'reference_link' => $projectUrl,
                                            'development_reg_new_user' => time(),
                                        ),
                                    'add_tags' =>
                                        array(
                                            $registraion_data['infusionsoftTag'],
                                            $registraion_data['media_source'],
                                            $registraion_data['affiliate_tag_maropost'],
                                            $realtor_tag
                                        )
                                ));
                    }


                }

            }

        } else if ($sales_status == 'Sold Out') {

            if ($realtor == 1) {

                $arrayNew = array(
                    'list_id' => $list_id,
                    'contact' =>
                        array(
                            'first_name' => $registraion_data['first_name'],
                            'last_name' => $registraion_data['last_name'],
                            'email' => $registraion_data['email'],
                            'subscribe' => 'true',
                            'custom_field' =>
                                array(
                                    'phone_1' => $registraion_data['phone'],
                                    'company' => 'CONNECT asset management',
                                    'realtor' => $realtor_,
                                    'prospect' => $prospect,
                                    'assigned_to' => $assign_user_name,
                                    'comment_question' => $projectName,
                                    'development_name' => $projectTitle,
                                    'reference_link' => $projectUrl,
                                    'development_reg_soldout_realtor' => time(),
                                ),
                            'add_tags' =>
                                array(
                                    $registraion_data['infusionsoftTag'],
                                    $registraion_data['media_source'],
                                    $registraion_data['affiliate_tag_maropost'],
                                    $realtor_tag
                                )
                        ));

            } else {

                if ($user_existing_status == 1) {

                    if ($subscriptions_status == 1) {

                        $arrayNew = array(
                            'list_id' => $list_id,
                            'contact' =>
                                array(
                                    'first_name' => $registraion_data['first_name'],
                                    'last_name' => $registraion_data['last_name'],
                                    'email' => $registraion_data['email'],
                                    'subscribe' => 'true',
                                    'custom_field' =>
                                        array(
                                            'phone_1' => $registraion_data['phone'],
                                            'company' => 'CONNECT asset management',
                                            'realtor' => $realtor_,
                                            'prospect' => $prospect,
                                            'assigned_to' => $assign_user_name,
                                            'comment_question' => $projectName,
                                            'development_name' => $projectTitle,
                                            'reference_link' => $projectUrl,
                                            'development_reg_soldout_existing' => time(),
                                        ),
                                    'add_tags' =>
                                        array(
                                            $registraion_data['infusionsoftTag'],
                                            $registraion_data['media_source'],
                                            $registraion_data['affiliate_tag_maropost'],
                                            $realtor_tag
                                        )
                                ));
                    }

                } else if ($user_existing_status == 0) {

                    $arrayNew = array(
                        'list_id' => $list_id,
                        'contact' =>
                            array(
                                'first_name' => $registraion_data['first_name'],
                                'last_name' => $registraion_data['last_name'],
                                'email' => $registraion_data['email'],
                                'subscribe' => 'true',
                                'custom_field' =>
                                    array(
                                        'phone_1' => $registraion_data['phone'],
                                        'company' => 'CONNECT asset management',
                                        'realtor' => $realtor_,
                                        'prospect' => $prospect,
                                        'assigned_to' => $assign_user_name,
                                        'comment_question' => $projectName,
                                        'development_name' => $projectTitle,
                                        'reference_link' => $projectUrl,
                                        'development_reg_soldout_new_user' => time(),
                                    ),
                                'add_tags' =>
                                    array(
                                        $registraion_data['infusionsoftTag'],
                                        $registraion_data['media_source'],
                                        $registraion_data['affiliate_tag_maropost'],
                                        $realtor_tag
                                    )
                            ));
                }

            }
        }
        else if ($sales_status == 'Vip') {

            if ($realtor == 1) {

                $arrayNew = array(
                    'list_id' => $list_id,
                    'contact' =>
                        array(
                            'first_name' => $registraion_data['first_name'],
                            'last_name' => $registraion_data['last_name'],
                            'email' => $registraion_data['email'],
                            'subscribe' => 'true',
                            'custom_field' =>
                                array(
                                    'phone_1' => $registraion_data['phone'],
                                    'company' => 'CONNECT asset management',
                                    'realtor' => $realtor_,
                                    'prospect' => $prospect,
                                    'assigned_to' => $assign_user_name,
                                    'comment_question' => $projectName,
                                    'development_name' => $projectTitle,
                                    'reference_link' => $projectUrl,
                                    'development_reg_vip_realtor' => time(),
                                ),
                            'add_tags' =>
                                array(
                                    $registraion_data['infusionsoftTag'],
                                    $registraion_data['media_source'],
                                    $registraion_data['affiliate_tag_maropost'],
                                    $realtor_tag
                                )
                        ));

            } else {

                $arrayNew = array(
                'list_id' => $list_id,
                'contact' =>
                    array(
                        'first_name' => $registraion_data['first_name'],
                        'last_name' => $registraion_data['last_name'],
                        'email' => $registraion_data['email'],
                        'subscribe' => 'true',
                        'custom_field' =>
                            array(
                                'phone_1' => $registraion_data['phone'],
                                'company' => 'CONNECT asset management',
                                'realtor' => $realtor_,
                                'prospect' => $prospect,
                                'assigned_to' => $assign_user_name,
                                'comment_question' => $projectName,
                                'development_name' => $projectTitle,
                                'reference_link' => $projectUrl,
                                'development_reg_vip_new_user' => time(),
                            ),
                        'add_tags' =>
                            array(
                                $registraion_data['infusionsoftTag'],
                                $registraion_data['media_source'],
                                $registraion_data['affiliate_tag_maropost'],
                                $realtor_tag
                            )
                    ));

            }
        }


        if (empty($arrayNew)) {
            $arrayNew = array(
                'list_id' => $list_id,
                'contact' =>
                    array(
                        'first_name' => $registraion_data['first_name'],
                        'last_name' => $registraion_data['last_name'],
                        'email' => $registraion_data['email'],
                        'subscribe' => 'true',
                        'custom_field' =>
                            array(
                                'phone_1' => $registraion_data['phone'],
                                'company' => 'CONNECT asset management',
                                'realtor' => $realtor_,
                                'prospect' => $prospect,
                                'assigned_to' => $assign_user_name,
                                'comment_question' => $projectName,
                                'development_name' => $projectTitle,
                                'reference_link' => $projectUrl,
                            ),
                        'add_tags' =>
                            array(
                                $registraion_data['infusionsoftTag'],
                                $registraion_data['media_source'],
                                $registraion_data['affiliate_tag_maropost'],
                                $realtor_tag
                            )
                    ));
        }

        $arrayNewsletter = $arrayNew;
        if($arrayNewsletter['list_id'] == 1){
            $arrayNewsletter['list_id'] = 3;
            $contactsNew->post_new_contact_into_list($arrayNewsletter);
        }

        $contactsNew->post_new_contact_into_list($arrayNew);
    }

    //@@Fail Notification Alert Function Calling
    public function sendFailNoticeEmail($raw_data,$contact_system_model){
        $browser_information=$this->getBrowserInfo();
        $baseAuthhorId = Yii::getAlias('@baseAuthhor');
        $baseUrl = Yii::getAlias('@baseUrl');

        $system_log_id=!empty($contact_system_model->id)?$contact_system_model->id:'';
        $system_log_url=''.$baseUrl.'/backend/web/contacts/contacts-systeminfo/view?id='.$system_log_id.'';
        $page_url='<a href="'.$raw_data['page_url'].'" target="_blank">'.$raw_data['page_url'].'</a>';

        if (!empty($raw_data['realtor'])){
            $contact_type='Agent';
        }
        else{
            $contact_type='Prospect';
        }
        $subject="Registation Fail Notification Alert";
        $message='<table width="100%" cellspacing="0" cellpadding="0" style="table-layout:fixed;" >
                <tr>
                    <td>Fail Notification Alert For System Log ID <a href="'.$system_log_url.'" target="_blank">'.$system_log_id.'</a> !</td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                <tr>
                     <td style="line-height: 20px;">
                        First Name:<span>'.' '.$raw_data['first_name'] .'</span><br/>
                        Last Name:<span>'.' '.$raw_data['last_name'] .'</span><br/>
                        Phone:<span>'.' '.$raw_data['phone'] .'</span><br/>
                        Email:<span>'.' '.$raw_data['email'] .'</span><br/>    
                        Realtor:<span>'.' '.$raw_data['realtor'] .'</span>
                     </td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                <tr>
                     <td style="line-height: 20px;">
                        Form Type:<span>'.' '.$raw_data['form_type'].'</span><br/>      
                        Development Name:<span>'.' '.$raw_data['development_name'].'</span><br/>      
                        Development ID:<span>'.' '.$raw_data['development_id'].'</span><br/>
                        Brand ID: <span>'.$raw_data['brand_id'].'</span><br/> 
                        Media Source:<span>'.' '.$raw_data['media_source'].'</span><br/>        
                        Page URL:<span>'.' '.$page_url.'</span><br/>
                        Page Name:<span>'.' '.$raw_data['page_name'].'</span><br/>
                        Tag Applied:<span>'.' '.$raw_data['affiliate_tag'].'</span><br/>                
                        Event At: <span>'.$raw_data['at'].'</span><br/> 
                        Event On: <span>'.$raw_data['on'].'</span><br/>   
                        Event: <span>'.$raw_data['event'].'</span><br/>   
                        Log Date: <span>'.date('Y-m-d H:i:s').'</span><br/>   
                        Contact Type: <span>'.$contact_type .'</span><br/>   
                     </td>
                </tr>
               <tr>
                     <td height="20"></td>
                </tr>
                <tr>
                     <td style="line-height: 20px;">
                        Device:<span>'.' '.$browser_information['device'] .'</span><br/>
                        Ip Address:<span>'.' '.$browser_information['ip_address'].'</span><br/>
                        Operating:<span>'.' '.$browser_information['platform'].'</span><br/>
                        Browser:<span>'.' '.$browser_information['name'].'</span><br/>                        
                     </td>
                </tr>          
                 <tr>
                     <td height="20"></td>
                </tr>   
                
                <tr>
                     <td height="20"></td>
                </tr>
                <tr> 
                    <td>Thanks,</td>
                </tr>
                 <tr>
                     <td height="30"></td>
                </tr>
                 <tr> 
                    <td>Reggie</td>
                </tr>
                <tr>
                     <td height="30"></td>
                </tr>
                <tr> 
                    <td>Confidential Information - Obtain Release Permission</td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                <tr> 
                    <td>www.connectassetmanagement.com </td>
                </tr>
            </table>';

        /*print_r($message);exit();*/

        $maropost=new maropost();
        $arrayNew = array(
            "content" => array(
                "name" =>'Admin System Global Mail Content [Fail]',
                //"text_part" => "text part of the content",
                "html_part" => $message,
                "full_email" => "false",
                "footer_type" => "default_footer",
                "footer_id" => ""
            ),
            "id" =>240
        );
        $maropost->put_template_content($arrayNew);
        $assign_user=User::find()->where(['id'=>$baseAuthhorId])->one(); //User assigned id
        $response_register=$this->register_user_for_fail_email_notice($assign_user,$subject);
    }

    //@@Send Automation Mail For Each New Lead
    private function sendNewLeadEmail($contact_system_model,$cars_contact_url) {

        $baseAuthhorId = Yii::getAlias('@baseAuthhor');
        $assign_id=$developement_sale_status=$interaction_note=$download_url=$contact_type=$tag_name='';

        //Realtor Check
        if (!empty($contact_system_model->realtor)){
            $realtor='Yes';
            $contact_type='realtor';
        }
        else{
            $realtor='No';
            $contact_type='prospect';
        }

        $checkAssignedUser=Developments::find()->where(['development_id' => $contact_system_model->development_id])->andWhere(['not', ['development_id' => null]])->one();
        if (!empty($checkAssignedUser)):
            if ($checkAssignedUser->sales_status == 'comingsoon') {
                $sales_status = 'Registration';
            } else if ($checkAssignedUser->sales_status == 'active') {
                $sales_status = 'Available';
            } else if ($checkAssignedUser->sales_status == 'archived') {
                $sales_status = 'Sold Out';
            }
            else if ($checkAssignedUser->sales_status == 'vip') {
                $sales_status = 'Vip';
                $download_url='Download Page URL: '.'<a href="'.$checkAssignedUser->development_url.'download/'.'" style="text-decoration: none;border-radius: 3px;" target="_blank">Go to Downloads....</a><br/>';
            }
            $developement_sale_status=$sales_status;

            //tag configure
            if ($contact_system_model->form_type=='rsvp' || $contact_system_model->form_type=='appointment'):
                if ($contact_system_model->event=='true'):
                    $tag_name=$contact_system_model->form_type.'-'.'yes'.'-'.$contact_type.strtolower('-'.$checkAssignedUser->development_name);
                else:
                    $tag_name=$contact_system_model->form_type.'-'.'no'.'-'.$contact_type.strtolower('-'.$checkAssignedUser->development_name);
                endif;
            else:
                $tag_name=$contact_system_model->form_type.'-'.$contact_type. strtolower('-'.$checkAssignedUser->development_name);
            endif;
        else:
            $developement_sale_status='';
            //tag configure
            if ($contact_system_model->form_type=='rsvp' || $contact_system_model->form_type=='appointment'):
                if ($contact_system_model->event=='true'):
                    $tag_name=$contact_system_model->form_type.'-'.'yes'.'-'.$contact_type.strtolower(!empty($checkAssignedUser->development_name)?'-'.$checkAssignedUser->development_name:'');
                else:
                    $tag_name=$contact_system_model->form_type.'-'.'no'.'-'.$contact_type.strtolower(!empty($checkAssignedUser->development_name)?'-'.$checkAssignedUser->development_name:'');
                endif;
            else:
                $tag_name=$contact_system_model->form_type.'-'.$contact_type. strtolower(!empty($checkAssignedUser->development_name)?'-'.$checkAssignedUser->development_name:'');
            endif;
        endif;

        if (!empty($checkAssignedUser->assignment_id)){
            $assign_id=$checkAssignedUser->assignment_id;
        }
        else{
            $assign_id=$baseAuthhorId;
        }

        $assign_user=User::find()->where(['id'=>$assign_id])->one();
        $assign_user_name=$assign_user->first_name.' '.$assign_user->last_name;
        $developement='for'.' '.$contact_system_model->development_name;
        $developmentname=$contact_system_model->development_name;

        $subject =' '.$contact_system_model->first_name.' '.$contact_system_model->last_name.' for '.$developmentname;
        $message='<table width="100%" cellspacing="0" cellpadding="0" style="table-layout:fixed;" >
                <tr>
                    <td>Howdy Partner,</td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                <tr>
                    <td>We have a new lead '.$developement.'!</td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                <tr>
                     <td style="line-height: 20px;">
                        First Name:<span>'.' '.$contact_system_model->first_name .'</span><br/>
                        Last Name:<span>'.' '.$contact_system_model->last_name .'</span><br/>
                        Phone:<span>'.' '.$contact_system_model->phone .'</span><br/>
                        Email:<span>'.' '.$contact_system_model->email .'</span><br/>
                        Is a Realtor?:<span>'.' '.$realtor .'</span>
                     </td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                <tr>
                     <td style="line-height: 20px;">
                        Development Name:<span>'.' '.$developmentname.'</span><br/>
                        Development Sales Status:<span>'.' '.$developement_sale_status.'</span><br/>
                        Assigned User:<span>'.' '.$assign_user_name.'</span><br/>
                        Media Source:<span>'.' '.$contact_system_model->media_source.'</span><br/>
                        Page URL:<span>'.' '.$contact_system_model->page_url.'</span><br/>
                        '.$download_url.'
                        Tag Applied:<span>'.' '.$tag_name.'</span><br/>
                        Brand: <span>CONNECT asset Management</span><br/>      
                     </td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                 <tr>
                     <td>
                        <a href="'.$cars_contact_url.'" style="text-decoration: none;font-size: 16px;border-radius: 3px;">Learn More....</a>
                     </td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                <tr> 
                    <td>Thanks,</td>
                </tr>
                 <tr>
                     <td height="30"></td>
                </tr>
                 <tr> 
                    <td>Reggie</td>
                </tr>
                <tr>
                     <td height="30"></td>
                </tr>
                <tr> 
                    <td>Confidential Information - Obtain Release Permission</td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                <tr> 
                    <td>www.connectassetmanagement.com </td>
                </tr>
            </table>';


        $maropost=new maropost();
        $arrayNew = array(
            "content" => array(
                "name" =>'Admin System Global Mail Content [Daily]',
                //"text_part" => "text part of the content",
                "html_part" => $message,
                "full_email" => "false",
                "footer_type" => "default_footer",
                "footer_id" => ""
            ),
            "id" =>191
        );
        $maropost->put_template_content($arrayNew);
        $response_register=$this->register_user_for_new_lead_email($assign_user,$subject);
        
        if ($response_register['http_status']==201){
            $email_log=new ContactsEmailLog();
            $email_log->contact_id=$contact_system_model->id;
            $email_log->user_id=$assign_user->id;
            $email_log->subject=$subject;
            $email_log->mail_header=$subject;
            $email_log->mail_body=$message;
            $email_log->sent_date_time=date('Y-m-d H:i:s');
            $email_log->status=1;
            if (!$email_log->save(false)) {
                throw new Exception(json_encode($email_log->errors));
            }
        }
    }

    private function send_reggie_email_contact_type($contact_system_model,$cars_contact_url,$interaction){
        $baseAuthhorId = Yii::getAlias('@baseAuthhor');
        $assign_id=$developement_sale_status=$download_url=$contact_type=$tag_name='';
        //Realtor Check
        if (!empty($contact_system_model->realtor)){
            $realtor='Yes';
            $contact_type='realtor';
        }
        else{
            $realtor='No';
            $contact_type='prospect';
        }

        $checkAssignedUser=Developments::find()->where(['development_id' => $contact_system_model->development_id])->andWhere(['not', ['development_id' => null]])->one();

        if (!empty($checkAssignedUser)):
            if ($checkAssignedUser->sales_status == 'comingsoon') {
                $sales_status = 'Registration';
            } else if ($checkAssignedUser->sales_status == 'active') {
                $sales_status = 'Available';
            } else if ($checkAssignedUser->sales_status == 'archived') {
                $sales_status = 'Sold Out';
            }
            else if ($checkAssignedUser->sales_status == 'vip') {
                $sales_status = 'Vip';
                $download_url='Download Page URL: '.'<a href="'.$checkAssignedUser->development_url.'download/'.'" style="text-decoration: none;border-radius: 3px;" target="_blank">Go to Downloads....</a><br/>';
            }
            $developement_sale_status=$sales_status;
            //tag configure
            if ($contact_system_model->form_type=='rsvp' || $contact_system_model->form_type=='appointment'):
                if ($contact_system_model->event=='true'):
                    $tag_name=$contact_system_model->form_type.'-'.'yes'.'-'.$contact_type.strtolower('-'.$checkAssignedUser->development_name);
                else:
                    $tag_name=$contact_system_model->form_type.'-'.'no'.'-'.$contact_type.'-'.strtolower('-'.$checkAssignedUser->development_name);
                endif;
            else:
                $tag_name=$contact_system_model->form_type.'-'.$contact_type.'-'.strtolower('-'.$checkAssignedUser->development_name);
            endif;

        else:
            $developement_sale_status='';
            //tag configure
            if ($contact_system_model->form_type=='rsvp' || $contact_system_model->form_type=='appointment'):
                if ($contact_system_model->event=='true'):
                    $tag_name=$contact_system_model->form_type.'-'.'yes'.'-'.$contact_type.strtolower(!empty($checkAssignedUser->development_name)?'-'.$checkAssignedUser->development_name:'');
                else:
                    $tag_name=$contact_system_model->form_type.'-'.'no'.'-'.$contact_type.strtolower(!empty($checkAssignedUser->development_name)?'-'.$checkAssignedUser->development_name:'');
                endif;
            else:
                $tag_name=$contact_system_model->form_type.'-'.$contact_type.strtolower(!empty($checkAssignedUser->development_name)?'-'.$checkAssignedUser->development_name:'');
            endif;
        endif;

        $assign_id=$baseAuthhorId;

        $assign_user=User::find()->where(['id'=>$assign_id])->one();
        $assign_user_name=$assign_user->first_name.' '.$assign_user->last_name;
        $developement='for'.' '.$contact_system_model->development_name;
        $developmentname=$contact_system_model->development_name;

        if (!empty($contact_system_model->realtor)){
            $realtor='Yes';
        }
        else{
            $realtor='No';
        }

        $subject =' '.$contact_system_model->first_name.' '.$contact_system_model->last_name.' for '.$developmentname;
        $message='<table width="100%" cellspacing="0" cellpadding="0" style="table-layout:fixed;" >
                <tr>
                    <td>Howdy Partner,</td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                <tr>
                    <td>We have a new lead '.$developement.'!</td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                <tr>
                     <td style="line-height: 20px;">
                        Message:<span>'.' '.$interaction.'</span><br/>    
                     </td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                <tr>
                     <td style="line-height: 20px;">
                        First Name:<span>'.' '.$contact_system_model->first_name .'</span><br/>
                        Last Name:<span>'.' '.$contact_system_model->last_name .'</span><br/>
                        Phone:<span>'.' '.$contact_system_model->phone .'</span><br/>
                        Email:<span>'.' '.$contact_system_model->email .'</span><br/>
                        Is a Realtor?:<span>'.' '.$realtor .'</span>
                     </td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                <tr>
                     <td style="line-height: 20px;">
                        Development Name:<span>'.' '.$developmentname.'</span><br/>
                        Development Sales Status:<span>'.' '.$developement_sale_status.'</span><br/>
                        Assigned User:<span>'.' '.$assign_user_name.'</span><br/>
                        Media Source:<span>'.' '.$contact_system_model->media_source.'</span><br/>
                        Page URL:<span>'.' '.$contact_system_model->page_url.'</span><br/>
                        '.$download_url.'
                        Tag Applied:<span>'.' '.$tag_name.'</span><br/>
                        Brand: <span>CONNECT asset Management</span><br/>      
                     </td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                 <tr>
                     <td>
                        <a href="'.$cars_contact_url.'" style="text-decoration: none;font-size: 16px;border-radius: 3px;">Learn More....</a>
                     </td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                <tr> 
                    <td>Thanks,</td>
                </tr>
                 <tr>
                     <td height="30"></td>
                </tr>
                 <tr> 
                    <td>Reggie</td>
                </tr>
                <tr>
                     <td height="30"></td>
                </tr>
                <tr> 
                    <td>Confidential Information - Obtain Release Permission</td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                <tr> 
                    <td>www.connectassetmanagement.com </td>
                </tr>
            </table>';

        $maropost=new maropost();
        $arrayNew = array(
            "content" => array(
                "name" =>'Admin System Global Mail Content [Daily]',
                //"text_part" => "text part of the content",
                "html_part" => $message,
                "full_email" => "false",
                "footer_type" => "default_footer",
                "footer_id" => ""
            ),
            "id" =>191
        );
        $maropost->put_template_content($arrayNew);
        $response_register=$this->register_user_for_new_lead_email($assign_user,$subject);

        if ($response_register['http_status']==201){
            $email_log=new ContactsEmailLog();
            $email_log->contact_id=$contact_system_model->id;
            $email_log->user_id=$assign_user->id;
            $email_log->subject=$subject;
            $email_log->mail_header=$subject;
            $email_log->mail_body=$message;
            $email_log->sent_date_time=date('Y-m-d H:i:s');
            $email_log->status=1;
            if (!$email_log->save(false)) {
                throw new Exception(json_encode($email_log->errors));
            }
        }
    }

    //@@Fail Notification Maropost Function Calling
    private function register_user_for_fail_email_notice($assign_user,$subject){
        $first_name =   !empty($assign_user->first_name) ? $assign_user->first_name : '';
        $last_name =   !empty($assign_user->last_name) ? $assign_user->last_name : '';
        $mobile =   !empty($assign_user->mobile) ? $assign_user->mobile : '';
        $email =   !empty($assign_user->email) ? $assign_user->email : '';
        $list_id=50;
        $contactsNew=new maropost();
        $contactsNew = $contactsNew->contacts();
        $arrayNew = array(
            'list_id' => $list_id,
            'contact' =>
                array(
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'email' => $email,
                    'subscribe' => 'true',
                    'custom_field' =>
                        array(
                            'phone_1' =>$mobile,
                            'company' => 'CONNECT asset management',
                            'realtor' => 'False',
                            'prospect' => 'true',
                            'system_mail_subject' => $subject,
                            'fail_notice_status' => time()
                        )
                ));
        $response=$contactsNew->post_new_contact_into_list($arrayNew);
        return $response;
    }

    //@@New Lead Register Automation Mail Maropost Function Calling
    private function register_user_for_new_lead_email($assign_user,$subject){
        $first_name =   !empty($assign_user->first_name) ? $assign_user->first_name : '';
        $last_name =   !empty($assign_user->last_name) ? $assign_user->last_name : '';
        $mobile =   !empty($assign_user->mobile) ? $assign_user->mobile : '';
        $email =   !empty($assign_user->email) ? $assign_user->email : '';
        $list_id=50;
        $contactsNew=new maropost();
        $contactsNew = $contactsNew->contacts();
        $arrayNew = array(
            'list_id' => $list_id,
            'contact' =>
                array(
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'email' => $email,
                    'subscribe' => 'true',
                    'custom_field' =>
                        array(
                            'phone_1' =>$mobile,
                            'company' => 'CONNECT asset management',
                            'realtor' => 'False',
                            'prospect' => 'true',
                            'system_mail_subject' => $subject,
                            'system_email_status' => time()
                        )
                ));
        $response=$contactsNew->post_new_contact_into_list($arrayNew);
        return $response;
    }

    public function send_data_to_appointment_list($contact_system_model){
        $baseAuthhorId = Yii::getAlias('@baseAuthhor');

        $appointment_date=$appointment_time=$dev_name=$development_id=$triger_date=$hero_image=$assigne_user_footer_content=$appointment_content_agent='';
        $appointment_content='default';

        if (!empty($contact_system_model->event_on)){
            $appointment_date=date('l',strtotime($contact_system_model->event_on)).', '.date('F',strtotime($contact_system_model->event_on)).' '.date('d',strtotime($contact_system_model->event_on)).','.' '.date('Y',strtotime($contact_system_model->event_on));
            /*$triger_date=date('Y-m-dThh:mm:ssTZD', strtotime($contact_system_model->event_on.'20:00:00.000+03:00'));*/
            $triger_date=date('Y-m-dh:i A', strtotime($contact_system_model->event_on));
        }
       /*echo "<pre>";print_r($triger_date);exit();*/

        if (!empty($contact_system_model->event_at)){
            $appointment_time=$contact_system_model->event_at;
        }

        $develomentOne= Developments::find()->where(['development_id' => $contact_system_model->development_id])->andWhere(['not', ['development_id' => null]])->one();
        if (!empty($develomentOne)){
            $dev_name=$develomentOne->development_name;
            if (!empty($develomentOne->development_image_url)){
                $hero_image=$develomentOne->development_image_url;
            }else{
                $hero_image=$develomentOne->image_url;
            }
            if (!empty($develomentOne->appointment_content)){
            $appointment_content=$develomentOne->appointment_content;
            }
            if (!empty($contact_system_model->realtor)){
                $appointment_content_agent=$develomentOne->appointment_content_agent;
            }
        }

        if (!empty($contact_system_model->development_id)){
            $development_id=$contact_system_model->development_id;
        }

        if (!empty($develomentOne)){
            $assigned_id=$develomentOne->assignment_id;
            if ($assigned_id==0){
                $assign_id=$baseAuthhorId;
            }else{
                $assign_id=$assigned_id;
            }
            $assign_user=User::find()->where(['id'=>$assign_id])->one();
            $assigne_user_first_name=$assign_user->first_name;
            $assigne_user_email=$assign_user->email;
            $assigne_user_footer_content=$assign_user->footer_content;
        }

        $appointment=new appointments();
        if (!empty($contact_system_model)) {
            if ($contact_system_model->event=='true'){
                if (!empty($contact_system_model->realtor)){
                    $arraynew_agent_appointment = [
                        "record" => [
                            "contact_email"         => $contact_system_model->email,
                            "development_id"        => $development_id,
                            "development_name"      =>$dev_name,
                            "appointment_date"      =>$appointment_date,
                            "appointment_time"      => $appointment_time,
                            "appointment_content"   => $appointment_content_agent,
                            "development_image_url" =>$hero_image,
                            "first_name"             => $contact_system_model->first_name,
                            "agent_name"            => $assigne_user_first_name,
                            "agent_email"           => $assigne_user_email,
                            "agent_footer"          => $assigne_user_footer_content,
                            "trigger_date"          =>$triger_date
                        ]
                    ];
                    $response=$appointment->post_agent_apointments($arraynew_agent_appointment);
                }
                else{
                    $arraynew_appointment = [
                        "record" => [
                            "email"                 => $contact_system_model->email,
                            "development_id"        => $development_id,
                            "development_name"      =>$dev_name,
                            "appointment_date"      =>$appointment_date,
                            "appointment_time"      => $appointment_time,
                            "appointment_content"   => $appointment_content,
                            "development_image_url" =>$hero_image,
                            "FirstName"             => $contact_system_model->first_name,
                            "agent_name"            => $assigne_user_first_name,
                            "agent_email"           => $assigne_user_email,
                            "agent_footer"          => $assigne_user_footer_content,
                            "trigger_date"          =>$triger_date
                        ]
                    ];
                    $response=$appointment->post_apointments($arraynew_appointment);
                }

            }
        }
    }


    /*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  "Evaluation Action & Browser Get Information" @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/

    /* Evaluation Functionality */
    public function evaluateContactInfo($Request){
        $first_name      =  !empty($_REQUEST['first_name']) ? $_REQUEST['first_name'] : '';
        $last_name       =  !empty($_REQUEST['last_name']) ? $_REQUEST['last_name'] : '';
        $email           =  !empty($_REQUEST['email']) ? $_REQUEST['email'] : '';
        $phone           =  !empty($_REQUEST['phone']) ? $_REQUEST['phone'] : '';
        $media           =  !empty($_REQUEST['media']) ? $_REQUEST['media'] : '';
        $development_id  =  !empty($_REQUEST['development_id']) ? $_REQUEST['development_id'] : '';
        $development_name=  !empty($_REQUEST['development_name']) ? $_REQUEST['development_name'] : '';
        $brand           =  !empty($_REQUEST['brand']) ? $_REQUEST['brand'] : '';
        $event           =  !empty($_REQUEST['event']) ? $_REQUEST['event'] : '';
        $affiliate_tag   =  !empty($_REQUEST['affiliate_tag']) ? $_REQUEST['affiliate_tag'] : '';
        $form_type       =  !empty($_REQUEST['form_type']) ? $_REQUEST['form_type'] : '';
        $event_on        =  !empty($_REQUEST['on']) ? $_REQUEST['on'] : '';
        $event_at        =  !empty($_REQUEST['at']) ? $_REQUEST['at'] : '';
        $realtor         =  !empty($_REQUEST['realtor']) ? $_REQUEST['realtor'] : '';

        $evaluate_array = [];

        //email validate
        if (empty($email) || $email == '') {
            $evaluate_array['email'] =['value'=>strtolower($email),'vcode'=>'204'];
        }
        if (!empty($email)) {
            $evaluate_array['email'] =['value'=>strtolower($email),'vcode'=>'200'];
        }
        //first name validate
        if (empty($first_name) || $first_name == '') {
            $evaluate_array['first_name'] =['value'=>ucfirst($first_name),'vcode'=>'201'];
        }
        if (!empty($first_name)) {
            if (ctype_alpha(str_replace(' ', '', $first_name)) === false) {
                $evaluate_array['first_name'] =['value'=>ucfirst($first_name),'vcode'=>'201'];
            }
            elseif (strlen($first_name) > 25){
                $evaluate_array['first_name'] =['value'=>ucfirst($first_name),'vcode'=>'201'];
            }
            else{
                $evaluate_array['first_name'] =['value'=>ucfirst($first_name),'vcode'=>'200'];
            }
        }

        //last name validate
        if (empty($last_name) || $last_name == '') {
            $evaluate_array['last_name'] =['value'=>$last_name,'vcode'=>'202'];
        }
        if (!empty($last_name)) {
            if (ctype_alpha(str_replace(' ', '', $last_name)) === false) {
                $evaluate_array['last_name'] =['value'=>$last_name,'vcode'=>'202'];
            }
            elseif (strlen($last_name) > 40){
                $evaluate_array['last_name'] =['value'=>$last_name,'vcode'=>'202'];
            }
            else{
                $evaluate_array['last_name'] =['value'=>ucfirst($last_name),'vcode'=>'200'];
            }
        }
        //phone validate
        if (empty($phone) || $phone == '') {
            $evaluate_array['phone'] =['value'=>$phone,'vcode'=>'203'];
        }
        if (!empty($phone)) {
            $filtered_phone_number = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);
            $phone_to_check = str_replace("-", "", $filtered_phone_number);
            if (strlen($phone) > 25) {
                $evaluate_array['phone'] =['value'=>$phone,'vcode'=>'203'];
            }
            elseif (strlen($phone_to_check) < 10 || strlen($phone_to_check) > 14){
                $evaluate_array['phone'] =['value'=>$phone,'vcode'=>'203'];
            }
            else{
                $evaluate_array['phone'] =['value'=>preg_replace("/[^0-9]/", '', $phone),'vcode'=>'200'];
            }
        }
        //brand id validate
        if (empty($brand) || $brand == '') {
            $evaluate_array['brand'] =['value'=>$brand,'vcode'=>'205'];
        }
        if (!empty($brand)){
            if ((is_int($brand) || ctype_digit($brand)) && (int)$brand > 0 ){
                $evaluate_array['brand'] =['value'=>$brand,'vcode'=>'200'];
            }
            else{
                $evaluate_array['brand'] =['value'=>$brand,'vcode'=>'205'];
            }
        }

        //development Id validate
        if (empty($development_id) || $development_id == '') {
            $development=CommonHelper::getDevelopmentsByDevelopmentName($development_name);
            if (!empty($development)){
                $evaluate_array['development_id'] =['value'=>$development->development_id,'vcode'=>'200'];
            }
            else{
                $evaluate_array['development_id'] =['value'=>$development_id,'vcode'=>'206'];
            }
        }
        if (!empty($development_id)){
            if ((is_int($development_id) || ctype_digit($development_id)) && (int)$development_id > 0 ){
                $evaluate_array['development_id'] =['value'=>$development_id,'vcode'=>'200'];
            }
            else{
                $evaluate_array['development_id'] =['value'=>$development_id,'vcode'=>'206'];
            }
        }
        //Developement Name validate
        if (empty($development_name) || $development_name == '') {
            $evaluate_array['development_name'] =['value'=>$development_name,'vcode'=>'207'];
        }
        if (!empty($development_name)){
            $evaluate_array['development_name'] =['value'=>$development_name,'vcode'=>'200'];
        }
        //form type validate
        if (empty($form_type) || $form_type == '') {
            $evaluate_array['form_type'] =['value'=>strtolower($form_type),'vcode'=>'315'];
        }
        if (!empty($form_type)) {
            //Accepted Subscribe/Contact/Registration/RSVP/Appointment/Reservation/Survey
            if ($form_type =='Subscribe' || $form_type =='Contact' || $form_type =='Registration' || $form_type =='RSVP' || $form_type =='Appointment' || $form_type =='Reservation' || $form_type =='Survey') {
                $evaluate_array['form_type'] =['value'=>strtolower($form_type),'vcode'=>'200'];
            }
            else{
                $evaluate_array['form_type'] =['value'=>strtolower($form_type),'vcode'=>'315'];
            }
        }
        //event  validate
        if (empty($event) || $event == '') {
            $evaluate_array['event'] =['value'=>'false','vcode'=>'200'];
        }
        if (!empty($event)) {
            if (strtolower($form_type)  =='appointment' || strtolower($form_type)=='rsvp') {
                $evaluate_array['event'] = ['value' => 'true', 'vcode' => '200'];
            }
            else{
                $evaluate_array['event'] =['value'=>'false','vcode'=>'200'];
            }
        }
        //Event on  validate
        if (empty($event_on) || $event_on == '') {
            $evaluate_array['event_on'] =['value'=>$event_on,'vcode'=>'209'];
        }
        if (!empty($event_on)) {
            if (strtolower($form_type)  =='appointment') {
                $evaluate_array['event_on'] =['value'=>$event_on,'vcode'=>'200'];
            }
            else{
                $evaluate_array['event_on'] =['value'=>$event_on,'vcode'=>'209'];
            }
        }
        //Event at validate
        if (empty($event_at) || $event_at == '') {
            $evaluate_array['event_at'] =['value'=>$event_at,'vcode'=>'210'];
        }
        if (!empty($event_at)) {
            if (strtolower($form_type)  =='appointment') {
                $evaluate_array['event_at'] =['value'=>$event_at,'vcode'=>'200'];
            }
            else{
                $evaluate_array['event_at'] =['value'=>$event_at,'vcode'=>'210'];
            }
        }
        //Affiliate_tag  validate
        if (empty($affiliate_tag) || $affiliate_tag == '') {
            $evaluate_array['affiliate_tag'] =['value'=>$affiliate_tag,'vcode'=>'200'];
        }
        else {
            $checkTags = ContactsTagsNew::find()->where(['tag_name' => $affiliate_tag])->one();
            if (!empty($checkTags)){
                $evaluate_array['affiliate_tag'] =['value'=>$affiliate_tag,'vcode'=>'200'];
            }
            else{
                $evaluate_array['affiliate_tag'] =['value'=>$affiliate_tag,'vcode'=>'200'];
            }
        }
        //Media  validate
        if (empty($media) || $media == '') {
            $evaluate_array['media'] =['value'=>$media,'vcode'=>'308'];
        }
        if (!empty($media)) {
            $checkMediaSource=CommonHelper::getMediaByMediaName($media);
            if (!empty($checkMediaSource)){
                $evaluate_array['media'] =['value'=>ucfirst($media),'vcode'=>'200'];
            }else{
                $evaluate_array['media'] =['value'=>ucfirst($media),'vcode'=>'308'];
            }
        }

        //Contact Type validate
        if (empty($realtor) || $realtor == '') {
            $evaluate_array['realtor'] =['value'=>'false','vcode'=>'200'];
        }
        if (!empty($realtor)) {
            $evaluate_array['realtor'] =['value'=>'true','vcode'=>'200'];
        }
        return $evaluate_array;

    }

    /*Browser Information get*/
    public function getBrowserInfo() {
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version= "";
        $deveice='';
        if (stristr($_SERVER['HTTP_USER_AGENT'],'mobi')!==FALSE) {
            $deveice='Mobile';
        }
        else{
            $deveice='Desktop';
        }

        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'linux';
        } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'mac';
        } elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'windows';
        }
        if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) {
            $bname = 'Internet Explorer';
            $ub = "MSIE";
        } elseif(preg_match('/Firefox/i',$u_agent)) {
            $bname = 'Mozilla Firefox';
            $ub = "Firefox";
        } elseif(preg_match('/Chrome/i',$u_agent)) {
            $bname = 'Google Chrome';
            $ub = "Chrome";
        } elseif(preg_match('/Safari/i',$u_agent)) {
            $bname = 'Apple Safari';
            $ub = "Safari";
        } elseif(preg_match('/Opera/i',$u_agent)) {
            $bname = 'Opera';
            $ub = "Opera";
        } elseif(preg_match('/Netscape/i',$u_agent)) {
            $bname = 'Netscape';
            $ub = "Netscape";
        }
        $known = array('Version', $ub, 'other');
        $pattern = '#(?<browser>' . join('|', $known) . ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
        }
        $i = count($matches['browser']);
        if ($i != 1) {
            if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
                $version= $matches['version'][0];
            } else {
                $version= $matches['version'][1];
            }
        } else {
            $version= $matches['version'][0];
        }
        if ($version==null || $version=="") {$version="?";}

        if (!empty($_SERVER['HTTP_CLIENT_IP']))
        {
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            $ip=$_SERVER['REMOTE_ADDR'];
        }

        return array(
            'userAgent' => $u_agent,
            'name'      => $bname,
            'version'   => $version,
            'platform'  => $platform,
            'pattern'    => $pattern,
            'ip_address'=>$ip,
            'device'=>$deveice,
        );
    }


}



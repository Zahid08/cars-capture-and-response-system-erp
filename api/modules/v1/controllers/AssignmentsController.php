<?php

namespace api\modules\v1\controllers;

use api\helpers\CommonHelper;
use api\modules\v1\models\Assignments;
use api\modules\v1\models\AssignmentsUnits;
use api\modules\v1\models\search\AssignmentsSearch;
use Yii;
use yii\web\HttpException;
use yii\web\Response;
use yii\rest\ActiveController;
use yii\filters\Cors;
use yii\filters\RateLimiter;
use yii\filters\ContentNegotiator;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use api\modules\v1\models\Sample;
use api\modules\v1\models\search\SampleSearch;

class AssignmentsController extends ActiveController
{
    public $modelClass = Assignments::class;

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];

        $behaviors['rateLimiter'] = [
            'class' => RateLimiter::className(),
            'enableRateLimitHeaders' => false,
        ];

        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' =>  ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page', 'X-Pagination-Page-Count', 'X-Pagination-Per-Page', 'X-Pagination-Total-Count', 'Link'],
            ],
        ];

        return $behaviors;
    }

    /**
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();

        unset($actions['create'], $actions['update'], $actions['delete'], $actions['options']);

        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    /**
     * List all records page by page
     *
     * HTTP Verb: GET
     * URL: /api/web/v1/assignments?access-token=C6VlLzAnLJp-lM8O3v_7DBHH6I6mXql5
     *
     * @return \yii\data\ActiveDataProvider
     * @throws HttpException
     */
    public function prepareDataProvider()
    {
        if (isset($_GET['id'])){
            $assignment =Assignments::find()->where(['id'=>$_GET['id']])->all();
        }
        else{
            $assignment =Assignments::find()->all();
        }
        
        $dataProvider=[];
        foreach ($assignment as $key=>$assignmentItem){
            $developmentData        =CommonHelper::developmentListById($assignmentItem->development_id);
            $building_name          =$developmentData->development_name;
            $building_image_url     =$developmentData->image_url;
            $assignmentUnits        = AssignmentsUnits::find()->where(['assignment_id' => $assignmentItem->id])->one();
            $bedNameModel           =CommonHelper::getBedName($assignmentUnits->beds_id);
            $sqft                   =   !empty($assignmentUnits->sqft) ? $assignmentUnits->sqft : '';
            $listing_price          =   !empty($assignmentItem->listing_price) ? $assignmentItem->listing_price : '';
            $original_price         =   !empty($assignmentItem->original_price) ? $assignmentItem->original_price : '';
            $deposit_price          =   !empty($assignmentItem->deposit_price) ? $assignmentItem->deposit_price : '';
            $occupancy              =   !empty($assignmentItem->occupancy) ? $assignmentItem->occupancy : '';
            $psf                    =   !empty($assignmentItem->psf) ? $assignmentItem->psf : '';
            $bed                    =   !empty($bedNameModel->suite_type_name) ? $bedNameModel->suite_type_name : '';
            $baths                  =   !empty($assignmentUnits->baths) ? $assignmentUnits->baths : '';
            $balcony_size           =   !empty($assignmentUnits->balcony_size) ? $assignmentUnits->balcony_size : '';
            $parking_total          =   !empty($assignmentUnits->parking_total) ? $assignmentUnits->parking_total : '';
            $exposure               =   !empty($assignmentUnits->exposure) ? $assignmentUnits->exposure : '';
            $notes                  =   !empty($assignmentUnits->notes) ? $assignmentUnits->notes : '';
            $slug                   =$this->sanitizeStringForUrl($building_name);

            $parking =$locker=  '';
            if ($assignmentUnits->parking==0){
                $parking='No';
            }
            else{
                $parking='Yes';
            }
            if ($assignmentUnits->parking==0){
                $locker='No';
            }
            else{
                $locker='Yes';
            }
            $dataProvider[]=[
                'id'=>$assignmentItem->id,
                'building_name'=>$building_name,
                'image_url'=>$building_image_url,
                'listing_price'=>$listing_price,
                'original_price'=>$original_price,
                'deposit_price'=>$deposit_price,
                'occupancy'=>$occupancy,
                'beds'=>$bed,
                'sqft'=>$sqft,
                'baths'=>$baths,
                'parking'=>$parking,
                'locker'=>$locker,
                'psf'=>$psf,
                'balcony_size'=>$balcony_size,
                'parking_total'=>$parking_total,
                'exposure'=>$exposure,
                'slug'=>$slug,
                'notes'=>$notes,
            ];
        }

        return $dataProvider;
    }

    public function sanitizeStringForUrl($string){
        $string = strtolower($string);
        $string = html_entity_decode($string);
        $string = str_replace(array('ä','ü','ö','ß'),array('ae','ue','oe','ss'),$string);
        $string = preg_replace('#[^\w\säüöß]#',null,$string);
        $string = preg_replace('#[\s]{2,}#',' ',$string);
        $string = str_replace(array(' '),array('-'),$string);
        return $string;
    }
}

<?php

namespace api\modules\v1\controllers;

use api\modules\v1\models\Contacts;
use Yii;
use yii\web\HttpException;
use yii\web\Response;
use yii\rest\ActiveController;
use yii\filters\Cors;
use yii\filters\RateLimiter;
use yii\filters\ContentNegotiator;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use api\modules\v1\models\Sample;
use api\modules\v1\models\search\SampleSearch;

class FacebookLeadController extends ActiveController
{
    public $modelClass = Sample::class;

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];

        $behaviors['rateLimiter'] = [
            'class' => RateLimiter::className(),
            'enableRateLimitHeaders' => false,
        ];

        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' =>  ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page', 'X-Pagination-Page-Count', 'X-Pagination-Per-Page', 'X-Pagination-Total-Count', 'Link'],
            ],
        ];

        return $behaviors;
    }

    /**
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();

        unset($actions['create'], $actions['update'], $actions['delete'], $actions['options']);

        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    /**
     * List all records page by page
     *
     * HTTP Verb: GET
     * URL: /companies
     *
     * @return \yii\data\ActiveDataProvider
     * @throws HttpException
     */
    public function prepareDataProvider()
    {
        $searchModel = new SampleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if($dataProvider->getCount() <= 0) {
            throw new HttpException(404, 'No results found.');
        }
        else {
            return $dataProvider;
        }
    }
    //https://connectassetmgmt.connectcondos.com/api/web/v1/facebook-lead/register-contact?access-token=C6VlLzAnLJp-lM8O3v_7DBHH6I6mXql5
    public function actionRegisterContact(){
        $data               = json_decode(file_get_contents('php://input'));
        $final_data='';
        $count=0;
        foreach ($data->state->output as $fields_key => $fields_value) {
            if ($count==1) {
                $final_data = $fields_value;
            }
            $count++;
        }
        $datalist=array();
        foreach ($final_data as $data){
            $datalist[] =$data->raw__email;
        }
        if (!empty($datalist)){
            foreach ($datalist as $final_data){
                $contactModel=new Contacts();
                $contactModel->email =$final_data;
                if($contactModel->save()){
                }
            }
        }
        else{
            throw new HttpException(404, 'No results found.');
        }
    }

}

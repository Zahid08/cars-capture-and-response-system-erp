<?php
/**
 * Created by PhpStorm.
 * User: Shopon_PC
 * Date: 1/18/2019
 * Time: 3:20 PM
 */

namespace api\modules\v1\controllers;

use api\common\maropost\tags;
use api\models\User;
use api\modules\v1\models\Agent;
use api\modules\v1\models\ContactsBrands;
use api\modules\v1\models\ContactsSysteminfo;
use api\modules\v1\models\ContactsTagsApplied;
use api\modules\v1\models\ContactsTagsNew;
use api\modules\v1\models\ContactsTagsRelation;
use api\modules\v1\models\Developments;
use api\modules\v1\models\MediaLeads;
use unlock\modules\contacts\models\ContactsEmailLog;
use Yii;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use yii\web\ServerErrorHttpException;
use yii\web\Response;
use yii\rest\ActiveController;
use yii\filters\RateLimiter;
use yii\filters\ContentNegotiator;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use api\helpers\CommonHelper;
use api\modules\v1\models\Contacts;
use api\modules\v1\models\search\ContactsSearch;
use api\common\maropost\maropost;
use api\modules\v1\models\ContactsRegistration;
use api\modules\v1\models\ContactsTags;

class UnbounceController extends ActiveController
{
    public $modelClass = Contacts::class;
    public $modelAgentClass = Agent::class;

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                //'application/json' => Response::FORMAT_JSON,
                'text/html' => Response::FORMAT_HTML,
            ],
        ];

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];

        $behaviors['rateLimiter'] = [
            'class' => RateLimiter::className(),
            'enableRateLimitHeaders' => false,
        ];

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' =>  ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page', 'X-Pagination-Page-Count', 'X-Pagination-Per-Page', 'X-Pagination-Total-Count', 'Link'],
            ],
        ];

        return $behaviors;
    }

    /**
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();

        unset($actions['view'], $actions['create'], $actions['update'], $actions['delete'], $actions['options']);

        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    /**
     * List all records page by page
     *
     * HTTP Verb: GET
     * URL: /unbounce
     *
     * @return \yii\data\ActiveDataProvider
     * @throws HttpException
     */
    public function prepareDataProvider()
    {

    }

    /**
     * Return the details of the record {id}
     *
     * HTTP Verb: GET
     * URL: /companies/{id}
     *
     * @param $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->findModel($id);
    }

    /**
     * Create a new record
     *
     * HTTP Verb: POST
     * URL: /companies
     * 'Content-Type': 'multipart/form-data'
     *
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionCreate()
    {

    }

    /**
     * Updates an existing record
     *
     * HTTP Verb: PATCH, PUT
     * URL: /companies/{id}
     * 'Content-Type': 'application/x-www-form-urlencoded'
     *
     * @param $id
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->getRequest()->getBodyParams()))
        {
            return $this->saveData($model);
        }
        else{
            throw new BadRequestHttpException('Unable to process data due to invalid data type. Expecting: array. Example data: ModelName[fieldName]=Field Value');
        }
    }


    /**
     * Deletes an existing record
     *
     * HTTP Verb: DELETE
     * URL: /companies/{id}
     *
     * @param $id
     * @return array
     * @throws ServerErrorHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        try {
            if (!$model->delete()) {
                throw new Exception('The record cannot be deleted.');
            }

            Yii::$app->getResponse()->setStatusCode(200);
            return ['status' => 200, 'message' => 'The record have been successfully deleted.'];
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            if($e->getName() == CommonHelper::INTEGRITY_CONSTRAINT_VIOLATION){
                $message = "This record has relational data. You must delete the following data first";
                $message .= "<ul>";
                $message .= "<li>Example => Example List</li>";
                $message .= "</ul>";
            }
            throw new ServerErrorHttpException($message);
        }

    }

    /*
     * @Function Process : Unbounce landing page data comes from api then receive this function  and save to CARS and data send to maropost and sent to custom email.
     * API : @Method   : GET
     *       @API URL :https://connectassetmgmt.com/api/web/v1/unbounce/unbounce-register?access-token=sample
     */
    public function actionUnbounceRegister()
    {

        $dataList=array();
        if (isset($_GET['first_name'])){
            $dataList['firstname']=$_GET['first_name'];
        }
        if (isset($_GET['last_name'])){
            $dataList['lastname']=$_GET['last_name'];
        }
        else{
            $dataList['lastname']='';
        }
        if (isset($_GET['email'])){
            $dataList['email']=$_GET['email'];
        }

        if (isset($_GET['phone_']) || isset($_GET['phone_number'])){
            if (isset($_GET['phone_number'])){
                $dataList['phone']=$_GET['phone_number'];
            }
            else{
                $dataList['phone']=$_GET['phone_'];
            }
        }
        else{
            $dataList['phone']='';
        }

        if (isset($_GET['are_you_a_realtor']) || isset($_GET['i_am_a_realtor_']) || isset($_GET['i_am_a_realtor'])){
            if (isset($_GET['are_you_a_realtor'])){
                if ($_GET['are_you_a_realtor']=='Investor') {
                    $dataList['realtor'] = 0;
                }
                else {
                    $dataList['realtor'] = 1;
                }
            }
            else {
                $dataList['realtor'] = 1;
            }
        }
        else{
            $dataList['realtor']=0;
        }

        if (isset($_GET['media_source'])){
            $dataList['media_source']=$_GET['media_source'];
        }
        else{
            $dataList['media_source']='';
        }

        if (isset($_GET['development_name'])){
            $dataList['development_name']=$_GET['development_name'];
        }
        else{
            $dataList['development_name']='';
        }

        if (isset($_GET['tag_name'])){
            $dataList['tag_name']=$_GET['tag_name'];
        }
        else{
            $dataList['tag_name']='';
        }
        if (isset($_GET['pageUrl'])){
            $dataList['pageUrl']=$_GET['pageUrl'];
        }
        else{
            $dataList['pageUrl']='';
        }
        if (isset($_GET['Brand'])){
            $dataList['brand_id']=$_GET['Brand'];
        }
        else{
            $dataList['brand_id']='';
        }

        if (isset($_GET['development_id'])){
            $dataList['development_id']=$_GET['development_id'];
        }
        else{
            $dataList['development_id']='';
        }


        $dataList['contact_type']='global_registration';

        $have_data_list = 0;
        foreach ($dataList as $dataL){
            if( !empty($dataL) ){
                $have_data_list++;
            }
        }

        if (!empty($dataList) && $have_data_list>1){
            $this->saveData($dataList);
        }
        else{
            throw new HttpException(404, 'No results found.');
        }
    }

    /**
     * Save Data.
     *
     * @param $model
     * @return mixed
     * @throws ServerErrorHttpException
     */
    private function saveData($model){

        if (isset($model)) {
            try {
                if(!empty($model)) {

                    if ($model['realtor']== 1) {

                        $agentModel = new Agent();

                        $checkEmail = Agent::find()->where(['email' => $model['email']])->one();

                        if (!empty($checkEmail)) {
                            //agent data save
                            $agentModel = $this->findAgentModel($checkEmail->id);
                            $agentModel->first_name = $model['firstname'];
                            $agentModel->last_name = $model['lastname'];
                            $agentModel->email = $model['email'];
                            $agentModel->mobile = $model['phone'];
                            $agentModel->agent_type = '1';
                            $agentModel->brand = '1';
                            if($agentModel->save()){
                                echo "";
                            }
                        }else{
                            $agentModel->first_name =$model['firstname'];
                            $agentModel->last_name =$model['lastname'];
                            $agentModel->email = $model['email'];
                            $agentModel->mobile = $model['phone'];
                            $agentModel->agent_type = '1';
                            $agentModel->brand = '1';
                            if($agentModel->save()){
                                echo "";
                            }
                        }
                        if ($agentModel->save()){
                            $baseUrl = Yii::getAlias('@baseUrl');
                            $carsContactUrl=$baseUrl . '/backend/web/contacts/agent/update?id=' . $agentModel->id;
                            $this->registration_form($model);
                            $this->sendEmail($model,$carsContactUrl,$agentModel->id);

                            //Pop Up Info
                            $result=$this->popupHtml();
                            echo  $result;
                        }
                    }
                    else{
                        if ($model['contact_type'] == 'global_registration'){

                            $mediaLeadSource=new MediaLeads();
                            $contactModel=new Contacts();

                            $checkLeadName=MediaLeads::find()->where(['lead_source_name' => $model['media_source']])->one();

                            $checkEmail = Contacts::find()->where(['email' => $model['email']])->one();

                            if (!empty($checkEmail)) {
                                //get browser information
                                $browser=$this->getBrowser();
                                $brandName = ContactsBrands::find()->where(['contact_id' => $checkEmail->id])->andWhere(['brand_id' => $model['brand_id']])->one();

                                //contact information save
                                $contactModel = $this->findModel($checkEmail->id);

                                //lead source
                                if (empty($checkLeadName)){
                                    $mediaLeadSource->lead_source_name=$model['media_source'];
                                    $mediaLeadSource->created_at=date('Y-m-d H:i:s');
                                    $mediaLeadSource->status=1;
                                    if (!$mediaLeadSource->save(false)) {
                                        throw new Exception(json_encode($mediaLeadSource->errors));
                                    }
                                    $mediaLeadSourceID = $mediaLeadSource->id;
                                }else{
                                    $mediaLeadSourceID = $checkLeadName->id;
                                }

                                $contactModel->first_name =$model['firstname'];
                                $contactModel->last_name =$model['lastname'];
                                $contactModel->email = $model['email'];
                                $contactModel->lead_source = $mediaLeadSourceID;
                                $contactModel->phone = $model['phone'];
                                $contactModel->agent = 'connect';
                                if($contactModel->save()){

                                    //contact registration information
                                    $contactsregistration = new ContactsRegistration();
                                    $contactsregistration->contact_id = $contactModel->id;
                                    $contactsregistration->development_id =$model['development_id'];
                                    $contactsregistration->reg_source = $mediaLeadSourceID;
                                    $contactsregistration->reg_datetime = date('Y-m-d H:i:s');
                                    $contactsregistration->status_date = date('Y-m-d H:i:s');
                                    if (!$contactsregistration->save(false)) {
                                        throw new Exception(json_encode($contactsregistration->errors));
                                    }

                                    //brand information
                                    if (empty($brandName)) {
                                        $brandInfo = new ContactsBrands();
                                        $brandInfo->contact_id = $contactModel->id;
                                        $brandInfo->brand_id =$model['brand_id'];
                                        $brandInfo->applied_date = date('Y-m-d H:i:s');
                                        if (!$brandInfo->save(false)) {
                                            throw new Exception(json_encode($brandInfo->errors));
                                        }
                                    }
                                    //tag information
                                    $contactsTag = new ContactsTagsNew();
                                    $contactsTag->tag_name =strtolower($model['development_name']).' - '.'Registration Journey';
                                    $contactsTag->tag_category_id=1;
                                    $checkTags = ContactsTagsNew::find()->where(['tag_name' => $contactsTag->tag_name])->one();
                                    if (empty($checkTags)){
                                        if ($contactsTag->save(false)) {
                                            //save data relation table
                                            $tagRelation=new ContactsTagsRelation();
                                            $tagRelation->contact_id=$checkEmail->id;
                                            $tagRelation->tag_id=$contactsTag->id;
                                            $tagRelation->date_applied= date('Y-m-d H:i:s');
                                            if (!$tagRelation->save(false)) {
                                                throw new Exception(json_encode($tagRelation->errors));
                                            }
                                            //save data to applied tag
                                            $tagApplied=new ContactsTagsApplied();
                                            $tagApplied->tag_id=$contactsTag->id;
                                            if (!$tagApplied->save(false)) {
                                                throw new Exception(json_encode($tagApplied->errors));
                                            }

                                        }
                                    }
                                    else{
                                        $tagRelation=new ContactsTagsRelation();
                                        $tagRelation->contact_id=$checkEmail->id;
                                        $tagRelation->tag_id=$checkTags->id;
                                        $tagRelation->date_applied= date('Y-m-d H:i:s');
                                        if (!$tagRelation->save(false)) {
                                            throw new Exception(json_encode($tagRelation->errors));
                                        }
                                        $tagApplied=new ContactsTagsApplied();
                                        $tagApplied->tag_id=$checkTags->id;
                                        if (!$tagApplied->save(false)) {
                                            throw new Exception(json_encode($tagApplied->errors));
                                        }
                                    }

                                    // System information
                                    $contactsSystemInfo = new ContactsSysteminfo();
                                    $contactsSystemInfo->contact_id = $contactModel->id;
                                    $contactsSystemInfo->page_name = $model['development_name'];
                                    $contactsSystemInfo->page_url = $model['pageUrl'];
                                    $contactsSystemInfo->ip_address = $browser['ip_address'];
                                    $contactsSystemInfo->browser = $browser['name'];
                                    $contactsSystemInfo->operating = $browser['platform'];
                                    $contactsSystemInfo->visit_date = date('Y-m-d H:i:s');
                                    if (!$contactsSystemInfo->save(false)) {
                                        throw new Exception(json_encode($contactsSystemInfo->errors));
                                    }
                                    /* //Pop Up Info
                                     $result=$this->popupHtml();
                                     echo  $result;*/
                                }
                            }
                            else {

                                //lead source
                                if (empty($checkLeadName)){
                                    $mediaLeadSource->lead_source_name=$model['media_source'];
                                    $mediaLeadSource->created_at=date('Y-m-d H:i:s');
                                    $mediaLeadSource->status=1;
                                    if (!$mediaLeadSource->save(false)) {
                                        throw new Exception(json_encode($mediaLeadSource->errors));
                                    }
                                    $mediaLeadSourceID = $mediaLeadSource->id;
                                }else{
                                    $mediaLeadSourceID = $checkLeadName->id;
                                }

                                $browser=$this->getBrowser();
                                $contactModel->first_name =$model['firstname'];
                                $contactModel->last_name =$model['lastname'];
                                $contactModel->email = $model['email'];
                                $contactModel->phone = $model['phone'];
                                $contactModel->lead_source = $mediaLeadSourceID;
                                $contactModel->agent = 'connect';
                                if($contactModel->save()){

                                    //contact information
                                    $contactsregistration = new ContactsRegistration();
                                    $contactsregistration->contact_id = $contactModel->id;
                                    $contactsregistration->development_id =$model['development_id'];
                                    $contactsregistration->reg_source = $mediaLeadSourceID;
                                    $contactsregistration->reg_datetime = date('Y-m-d H:i:s');
                                    $contactsregistration->status_date = date('Y-m-d H:i:s');
                                    if (!$contactsregistration->save(false)) {
                                        throw new Exception(json_encode($contactsregistration->errors));
                                    }
                                    //tag information
                                    $contactsTag = new ContactsTagsNew();
                                    $contactsTag->tag_name =strtolower($model['development_name']).' - '.'Registration Journey';
                                    $contactsTag->tag_category_id=1;
                                    $checkTags = ContactsTagsNew::find()->where(['tag_name' => $contactsTag->tag_name])->one();
                                    if (empty($checkTags)){
                                        if ($contactsTag->save(false)) {
                                            //save data relation table
                                            $tagRelation=new ContactsTagsRelation();
                                            $tagRelation->contact_id=$contactModel->id;
                                            $tagRelation->tag_id=$contactsTag->id;
                                            $tagRelation->date_applied= date('Y-m-d H:i:s');
                                            if (!$tagRelation->save(false)) {
                                                throw new Exception(json_encode($tagRelation->errors));
                                            }
                                            //save data to applied tag
                                            $tagApplied=new ContactsTagsApplied();
                                            $tagApplied->tag_id=$contactsTag->id;
                                            if (!$tagApplied->save(false)) {
                                                throw new Exception(json_encode($tagApplied->errors));
                                            }

                                        }
                                    }
                                    else{
                                        $tagRelation=new ContactsTagsRelation();
                                        $tagRelation->contact_id=$contactModel->id;
                                        $tagRelation->tag_id=$checkTags->id;
                                        $tagRelation->date_applied= date('Y-m-d H:i:s');
                                        if (!$tagRelation->save(false)) {
                                            throw new Exception(json_encode($tagRelation->errors));
                                        }
                                        //save data to applied tag
                                        $tagApplied=new ContactsTagsApplied();
                                        $tagApplied->tag_id=$checkTags->id;
                                        if (!$tagApplied->save(false)) {
                                            throw new Exception(json_encode($tagApplied->errors));
                                        }
                                    }

                                    //brand information
                                    $brandInfo=new ContactsBrands();
                                    $brandInfo->contact_id = $contactModel->id;
                                    $brandInfo->brand_id = $model['brand_id'];
                                    $brandInfo->applied_date =date('Y-m-d H:i:s');
                                    if (!$brandInfo->save(false)) {
                                        throw new Exception(json_encode($brandInfo->errors));
                                    }

                                    // System information
                                    $contactsSystemInfo = new ContactsSysteminfo();
                                    $contactsSystemInfo->contact_id = $contactModel->id;
                                    $contactsSystemInfo->page_name = $model['development_name'];
                                    $contactsSystemInfo->page_url = $model['pageUrl'];
                                    $contactsSystemInfo->ip_address = $browser['ip_address'];
                                    $contactsSystemInfo->browser = $browser['name'];
                                    $contactsSystemInfo->operating = $browser['platform'];
                                    $contactsSystemInfo->visit_date = date('Y-m-d H:i:s');
                                    if (!$contactsSystemInfo->save(false)) {
                                        throw new Exception(json_encode($contactsSystemInfo->errors));
                                    }
                                }
                            }

                            if ($contactModel->save()){
                                $baseUrl = Yii::getAlias('@baseUrl');
                                $carsContactUrl=$baseUrl . '/backend/web/contacts/contacts-registration/update?id=' . $contactsregistration->id;
                                $this->registration_form($model);
                                $this->sendEmail($model,$carsContactUrl,$contactModel->id);
                                //Pop Up Info
                                $result=$this->popupHtml();
                                echo  $result;
                            }
                        }
                    }


                }
            }
            catch (Exception $e) {
                throw new ServerErrorHttpException($e->getMessage());
            }
        }
    }


    //@PopUp Information
    function popupHtml(){
        $html='
            <html>
            <body style="
            background-image: url(\'https://connectassetmanagement.com/wp-content/uploads/2019/01/popup_thankyou.jpg\');
            text-align: center;
            width: 385px;
            height: 225px;">
            <h1 style="text-align:center;color:#000000;line-height: 13px;font-family: oswald;font-size:28px;padding-top: 35px;">Thank You!</h1>
            <p style="text-align:center;color:#111;">Your form has been submitted.</p>
            <div style="width:150px;margin:35px auto;">
                <table width="100%">
                    <tr>
                        <td width="33%">
                            <iframe id="twitter-widget-1" scrolling="no" frameborder="0" allowtransparency="true" class="twitter-share-button twitter-share-button-rendered twitter-tweet-button" style="position: static; visibility: visible; width: 60px; height: 20px;" title="Twitter Tweet Button" src="https://platform.twitter.com/widgets/tweet_button.a600a62a1c92aa33bb89e73fa1e8b3b3.en.html#dnt=false&amp;id=twitter-widget-1&amp;lang=en&amp;original_referer=https%3A%2F%2Fapp.unbounce.com%2F791802%2Fvariants%2F172700421%2Fedit&amp;size=m&amp;text=Get%20insider%20access%20to%20today%27s%20hottest%20%23Toronto%20developments!%20Register%20now%20for%20prices%2C%20plans%2C%20%26%20more%20%23danilesCorp%20%23Artworks&amp;time=1548677214872&amp;type=share&amp;url=http%3A%2F%2Fconnectassetmanagement.com%2Fform-confirmation-page%2F" data-url="http://connectassetmanagement.com/form-confirmation-page/"></iframe>
                        </td>
                        <td width="33%">
                            <iframe ng-non-bindable="" frameborder="0" hspace="0" marginheight="0" marginwidth="0" scrolling="no" style="position: static; top: 0px; width: 32px; margin: 0px; border-style: none; left: 0px; visibility: visible; height: 20px;" tabindex="0" vspace="0" width="100%" id="I1_1548677214977" name="I1_1548677214977" src="https://apis.google.com/u/0/se/0/_/+1/fastbutton?usegapi=1&amp;annotation=none&amp;size=medium&amp;origin=https%3A%2F%2Fapp.unbounce.com&amp;url=http%3A%2F%2Fplus.google.com%2F%2BCONNECTassetmanagementToronto&amp;gsrc=3p&amp;jsh=m%3B%2F_%2Fscs%2Fapps-static%2F_%2Fjs%2Fk%3Doz.gapi.en.eUmlrZ-GjRk.O%2Fam%3DwQ%2Frt%3Dj%2Fd%3D1%2Frs%3DAGLTcCPbVaczsDu16R3q9EnCF7wlxlmADg%2Fm%3D__features__#_methods=onPlusOne%2C_ready%2C_close%2C_open%2C_resizeMe%2C_renderstart%2Concircled%2Cdrefresh%2Cerefresh%2Conload&amp;id=I1_1548677214977&amp;_gfid=I1_1548677214977&amp;parent=https%3A%2F%2Fapp.unbounce.com&amp;pfname=&amp;rpctoken=15188678" data-gapiattached="true" title="G+"></iframe>
                        </td>
                        <td width="33%">
                            <iframe src="https://www.facebook.com/plugins/like.php?href=http://www.facebook.com/connectasset&amp;send=false&amp;layout=button&amp;width=49&amp;height=&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;;appId=131761285788" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:49px; height:21px;" allowtransparency="true"></iframe>
                        </td>
                    </tr>
                </table>
            </div>
            </body>
        </html>
        ';
        return $html;
    }

    //@@Send Email from unbounce form to admin
    private function sendEmail($registerd_datalist,$carsUrl,$contactId) {
        $checkAssignedUser=Developments::find()->where(['development_id' => $registerd_datalist['development_id']])->one();
        $tagname =strtolower($registerd_datalist['development_name']).' - '.'Registration Journey';
        $develomentList = Developments::find()->where(['development_id' => $registerd_datalist['development_id']])->one();

        if (!empty($develomentList)){
            $developement='for'.' '.$develomentList->development_name;
            $developmentname=$develomentList->development_name;
        }
        if ($registerd_datalist['realtor']==1){
            $realtor='Yes';
        }
        else{
            $realtor='No';
        }
        $subject =' '.$registerd_datalist['firstname'].' '.$registerd_datalist['lastname'].' for '.$registerd_datalist['development_name'];
        $message='<table width="100%" cellspacing="0" cellpadding="0" style="table-layout:fixed;" >
                <tr>
                    <td>Howdy Partner,</td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                <tr>
                    <td>We have a new lead '.$developement.'!</td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                <tr>
                     <td style="line-height: 20px;">
                        First Name:<span>'.' '.$registerd_datalist['firstname'] .'</span><br/>
                        Last Name:<span>'.' '.$registerd_datalist['lastname'] .'</span><br/>
                        Phone:<span>'.' '.$registerd_datalist['phone'] .'</span><br/>
                        Email:<span>'.' '.$registerd_datalist['email'] .'</span><br/>
                        Is a Realtor?:<span>'.' '.$realtor .'</span>
                     </td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                <tr>
                     <td style="line-height: 20px;">
                        Development Name:<span>'.' '.$developmentname.'</span><br/>
                        Media Source:<span>'.' '.$registerd_datalist['media_source'] .'</span><br/>
                        Page URL:<span>'.' '.$registerd_datalist['pageUrl'] .'</span><br/>
                        Tag Applied:<span>'.' '.$tagname.'</span><br/>
                        Brand: <span>CONNECT asset Management</span>
                     </td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                 <tr>
                     <td>
                        <a href="'.$carsUrl.'" style="text-decoration: none;font-size: 16px;border-radius: 3px;">Learn More....</a>
                     </td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                <tr> 
                    <td>Thanks,</td>
                </tr>
                 <tr>
                     <td height="30"></td>
                </tr>
                 <tr> 
                    <td>Reggie</td>
                </tr>
                <tr>
                     <td height="30"></td>
                </tr>
                <tr> 
                    <td>Confidential Information - Obtain Release Permission</td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                <tr> 
                    <td>www.connectassetmanagement.com </td>
                </tr>
            </table>';

        $maropost=new maropost();
       /* Template Add First*/
        $arrayNew = array(
            "content" => array(
                "name" =>'Admin System Global Mail Content [Daily]',
                "html_part" => $message,
                "full_email" => "false",
                "footer_type" => "default_footer",
                "footer_id" => ""
            ),
            "id" =>191
        );
        $maropost->put_template_content($arrayNew);

        /* Assgoned User Register*/
        if (!empty($checkAssignedUser->assignment_id)){
            $assign_id=$checkAssignedUser->assignment_id;}
            else{$assign_id=37;}

        $assign_user=User::find()->where(['id'=>$assign_id])->one();
        $response_register=$this->register_assigned_user_maropost($assign_user,$subject);
        if ($response_register['http_status']==201) {
            $email_log = new ContactsEmailLog();
            $email_log->contact_id = $contactId;
            $email_log->user_id = $assign_user->id;
            $email_log->subject = $subject;
            $email_log->mail_header = $subject;
            $email_log->mail_body = $message;
            $email_log->sent_date_time = date('Y-m-d H:i:s');
            $email_log->status = 1;
            if (!$email_log->save(false)) {
                throw new Exception(json_encode($email_log->errors));
            }
        }
    }
    //@@Assigned User Register to mropost
    private function register_assigned_user_maropost($assign_user,$subject){
        $first_name =   !empty($assign_user->first_name) ? $assign_user->first_name : '';
        $last_name =   !empty($assign_user->last_name) ? $assign_user->last_name : '';
        $mobile =   !empty($assign_user->mobile) ? $assign_user->mobile : '';
        $email =   !empty($assign_user->email) ? $assign_user->email : '';
        $list_id=50;
        $contactsNew=new maropost();
        $contactsNew = $contactsNew->contacts();
        $arrayNew = array(
            'list_id' => $list_id,
            'contact' =>
                array(
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'email' => $email,
                    'subscribe' => 'true',
                    'custom_field' =>
                        array(
                            'phone_1' =>$mobile,
                            'company' => 'CONNECT asset management',
                            'realtor' => 'False',
                            'prospect' => 'true',
                            'system_mail_subject' => $subject,
                            'system_email_status' => time()
                        )
                ));
        $response=$contactsNew->post_new_contact_into_list($arrayNew);
        return $response;
    }
    /**
     * Finds the record based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $modelClass = $this->modelClass;
        if (($model = $modelClass::findOne($id)) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException("No records found");
        }
    }
    protected function findAgentModel($id)
    {
        $modelClass = $this->modelAgentClass;
        if (($model = $modelClass::findOne($id)) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException("No records found");
        }
    }

    //@@@Browser information get
    public function getBrowser() {
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version= "";
        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'linux';
        } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'mac';
        } elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'windows';
        }
        if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) {
            $bname = 'Internet Explorer';
            $ub = "MSIE";
        } elseif(preg_match('/Firefox/i',$u_agent)) {
            $bname = 'Mozilla Firefox';
            $ub = "Firefox";
        } elseif(preg_match('/Chrome/i',$u_agent)) {
            $bname = 'Google Chrome';
            $ub = "Chrome";
        } elseif(preg_match('/Safari/i',$u_agent)) {
            $bname = 'Apple Safari';
            $ub = "Safari";
        } elseif(preg_match('/Opera/i',$u_agent)) {
            $bname = 'Opera';
            $ub = "Opera";
        } elseif(preg_match('/Netscape/i',$u_agent)) {
            $bname = 'Netscape';
            $ub = "Netscape";
        }
        $known = array('Version', $ub, 'other');
        $pattern = '#(?<browser>' . join('|', $known) . ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
        }
        $i = count($matches['browser']);
        if ($i != 1) {
            if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
                $version= $matches['version'][0];
            } else {
                $version= $matches['version'][1];
            }
        } else {
            $version= $matches['version'][0];
        }
        if ($version==null || $version=="") {$version="?";}

        if (!empty($_SERVER['HTTP_CLIENT_IP']))
        {
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            $ip=$_SERVER['REMOTE_ADDR'];
        }

        return array(
            'userAgent' => $u_agent,
            'name'      => $bname,
            'version'   => $version,
            'platform'  => $platform,
            'pattern'    => $pattern,
            'ip_address'=>$ip
        );
    }


    //@@Unbounce Registration Form data to maropost
    public  function  registration_form($registraion_data){
        if (!empty($registraion_data['development_id'])) {
            $develomentList = Developments::find()->where(['development_id' => $registraion_data['development_id']])->one();
            if (!empty($develomentList)) {
                $projectTitle = $develomentList->development_name;
                $project_sales_status = $develomentList->sales_status;
                $projectName = $develomentList->development_name;
                $projectUrl = $registraion_data['pageUrl'];
                $registraion_data['infusionsoftTag'] = $develomentList->development_name . ' ' . 'Registration Journey';
            } else {
                $projectTitle =$registraion_data['development_name'];
                $project_sales_status ='comingsoon';
                $projectName =$registraion_data['development_name'];
                $projectUrl = $registraion_data['pageUrl'];
                $registraion_data['infusionsoftTag'] =$registraion_data['development_name'].' ' . 'Registration Journey';
            }
        }
        else{
            $projectTitle =$registraion_data['development_name'];
            $project_sales_status ='comingsoon';
            $projectName =$registraion_data['development_name'];
            $projectUrl = $registraion_data['pageUrl'];
            $registraion_data['infusionsoftTag'] =$registraion_data['development_name'].' ' . 'Registration Journey';
        }

        $realtor = '';
        $prospect = '';
        $realtor_tag = '';

        if (!empty($registraion_data['realtor'])) {
            $realtor_ = 'true';
            $prospect = 'False';
            $realtor = 1;
            $realtor_tag = 'Realtor';
            $list_id = 17;
        } else {
            $realtor_ = 'False';
            $prospect = 'true';
            $realtor_tag = 'Prospect';
            $realtor = 0;
            $list_id = 1;
        }

        //get sales_status
        $sales_status = '';

        if ($project_sales_status == 'comingsoon') {
            $sales_status = 'Registration';
        } else if ($project_sales_status == 'active') {
            $sales_status = 'Available';
        } else if ($project_sales_status == 'archived') {
            $sales_status = 'Sold Out';
        }

        $projectDownloadsEnabled =0;
        $registraion_data['media_source']='Original Media Source -'.$registraion_data['media_source'];


        $tags=new tags();
        //tag created if tag not exist in maropost
        $array = array(
            "tags" => array(
                "name" =>$registraion_data['infusionsoftTag']
            )
        );
        $tags->post_tags($array);


        $array1 = array(
            "tags" => array(
                "name" =>$registraion_data['media_source']
            )
        );
        $tags->post_tags($array1);

        /*
         * data prepare for maropost
         */
        $contactsNew=new maropost();
        $contactsNew = $contactsNew->contacts();

        $get_contact_by_email = $contactsNew->get_contact_by_contact_email($registraion_data['email']);


        //echo '<pre>'; print_r($get_contact_by_email); exit();

        $user_existing_status = 1;
        $subscriptions_status = 1;


        if ($get_contact_by_email['http_status'] == 404) {
            $user_existing_status = 0;
        } else {

            $list_subscriptions = $get_contact_by_email['list_subscriptions'];

            foreach ($list_subscriptions as $subscriptions) {
                //
            }
            if ($subscriptions['status'] == 'Unsubscribed') {
                $subscriptions_status = 0;
            }
        }


        if ($sales_status == 'Registration') {

            if ($realtor == 1) {

                $arrayNew = array(
                    'list_id' => $list_id,
                    'contact' =>
                        array(
                            'first_name' => $registraion_data['firstname'],
                            'last_name' => $registraion_data['lastname'],
                            'email' => $registraion_data['email'],
                            'subscribe' => 'true',
                            'custom_field' =>
                                array(
                                    'phone_1' => $registraion_data['phone'],
                                    'company' => 'CONNECT asset management',
                                    'realtor' => $realtor_,
                                    'prospect' => $prospect,
                                    'comment_question' => $projectName,
                                    'development_name' => $projectTitle,
                                    'reference_link' => $projectUrl,
                                    'development_reg_realtor' => time(),
                                ),
                            'add_tags' =>
                                array(
                                    $registraion_data['infusionsoftTag'],
                                    $registraion_data['media_source'],
                                    $realtor_tag
                                )
                        ));


            } else {

                if ($user_existing_status == 1) {

                    if ($subscriptions_status == 1) {
                        $arrayNew = array(
                            'list_id' => $list_id,
                            'contact' =>
                                array(
                                    'first_name' => $registraion_data['firstname'],
                                    'last_name' => $registraion_data['lastname'],
                                    'email' => $registraion_data['email'],
                                    'subscribe' => 'true',
                                    'custom_field' =>
                                        array(
                                            'phone_1' => $registraion_data['phone'],
                                            'company' => 'CONNECT asset management',
                                            'realtor' => $realtor_,
                                            'prospect' => $prospect,
                                            'comment_question' => $projectName,
                                            'development_name' => $projectTitle,
                                            'reference_link' => $projectUrl,
                                            'development_reg_existing' => time(),
                                        ),
                                    'add_tags' =>
                                        array(
                                            $registraion_data['infusionsoftTag'],
                                            $registraion_data['media_source'],
                                            $realtor_tag
                                        )
                                ));
                    }

                } else if ($user_existing_status == 0) {
                    $arrayNew = array(
                        'list_id' => $list_id,
                        'contact' =>
                            array(
                                'first_name' => $registraion_data['firstname'],
                                'last_name' => $registraion_data['lastname'],
                                'email' => $registraion_data['email'],
                                'subscribe' => 'true',
                                'custom_field' =>
                                    array(
                                        'phone_1' => $registraion_data['phone'],
                                        'company' => 'CONNECT asset management',
                                        'realtor' => $realtor_,
                                        'prospect' => $prospect,
                                        'comment_question' => $projectName,
                                        'development_name' => $projectTitle,
                                        'reference_link' => $projectUrl,
                                        'development_reg_new_user' => time(),
                                    ),
                                'add_tags' =>
                                    array(
                                        $registraion_data['infusionsoftTag'],
                                        $registraion_data['media_source'],
                                        $realtor_tag
                                    )
                            ));
                }


            }

        } else if ($sales_status == 'Available') {

            if ($projectDownloadsEnabled != 1) {

                if ($realtor == 1) {

                    $arrayNew = array(
                        'list_id' => $list_id,
                        'contact' =>
                            array(
                                'first_name' => $registraion_data['firstname'],
                                'last_name' => $registraion_data['lastname'],
                                'email' => $registraion_data['email'],
                                'subscribe' => 'true',
                                'custom_field' =>
                                    array(
                                        'phone_1' => $registraion_data['phone'],
                                        'company' => 'CONNECT asset management',
                                        'realtor' => $realtor_,
                                        'prospect' => $prospect,
                                        'comment_question' => $projectName,
                                        'development_name' => $projectTitle,
                                        'reference_link' => $projectUrl,
                                        'development_reg_realtor' => time(),
                                    ),
                                'add_tags' =>
                                    array(
                                        $registraion_data['infusionsoftTag'],
                                        $registraion_data['media_source'],
                                        $realtor_tag
                                    )
                            ));


                } else {

                    if ($user_existing_status == 1) {

                        if ($subscriptions_status == 1) {
                            $arrayNew = array(
                                'list_id' => $list_id,
                                'contact' =>
                                    array(
                                        'first_name' => $registraion_data['firstname'],
                                        'last_name' => $registraion_data['lastname'],
                                        'email' => $registraion_data['email'],
                                        'subscribe' => 'true',
                                        'custom_field' =>
                                            array(
                                                'phone_1' => $registraion_data['phone'],
                                                'company' => 'CONNECT asset management',
                                                'realtor' => $realtor_,
                                                'prospect' => $prospect,
                                                'comment_question' => $projectName,
                                                'development_name' => $projectTitle,
                                                'reference_link' => $projectUrl,
                                                'development_reg_existing' => time(),
                                            ),
                                        'add_tags' =>
                                            array(
                                                $registraion_data['infusionsoftTag'],
                                                $registraion_data['media_source'],
                                                $realtor_tag
                                            )
                                    ));
                        }

                    } else if ($user_existing_status == 0) {
                        $arrayNew = array(
                            'list_id' => $list_id,
                            'contact' =>
                                array(
                                    'first_name' => $registraion_data['firstname'],
                                    'last_name' => $registraion_data['lastname'],
                                    'email' => $registraion_data['email'],
                                    'subscribe' => 'true',
                                    'custom_field' =>
                                        array(
                                            'phone_1' => $registraion_data['phone'],
                                            'company' => 'CONNECT asset management',
                                            'realtor' => $realtor_,
                                            'prospect' => $prospect,
                                            'comment_question' => $projectName,
                                            'development_name' => $projectTitle,
                                            'reference_link' => $projectUrl,
                                            'development_reg_new_user' => time(),
                                        ),
                                    'add_tags' =>
                                        array(
                                            $registraion_data['infusionsoftTag'],
                                            $registraion_data['media_source'],
                                            $realtor_tag
                                        )
                                ));
                    }


                }

            }

        } else if ($sales_status == 'Sold Out') {

            if ($realtor == 1) {

                $arrayNew = array(
                    'list_id' => $list_id,
                    'contact' =>
                        array(
                            'first_name' => $registraion_data['firstname'],
                            'last_name' => $registraion_data['lastname'],
                            'email' => $registraion_data['email'],
                            'subscribe' => 'true',
                            'custom_field' =>
                                array(
                                    'phone_1' => $registraion_data['phone'],
                                    'company' => 'CONNECT asset management',
                                    'realtor' => $realtor_,
                                    'prospect' => $prospect,
                                    'comment_question' => $projectName,
                                    'development_name' => $projectTitle,
                                    'reference_link' => $projectUrl,
                                    'development_reg_soldout_realtor' => time(),
                                ),
                            'add_tags' =>
                                array(
                                    $registraion_data['infusionsoftTag'],
                                    $registraion_data['media_source'],
                                    $realtor_tag
                                )
                        ));

            } else {

                if ($user_existing_status == 1) {

                    if ($subscriptions_status == 1) {

                        $arrayNew = array(
                            'list_id' => $list_id,
                            'contact' =>
                                array(
                                    'first_name' => $registraion_data['firstname'],
                                    'last_name' => $registraion_data['lastname'],
                                    'email' => $registraion_data['email'],
                                    'subscribe' => 'true',
                                    'custom_field' =>
                                        array(
                                            'phone_1' => $registraion_data['phone'],
                                            'company' => 'CONNECT asset management',
                                            'realtor' => $realtor_,
                                            'prospect' => $prospect,
                                            'comment_question' => $projectName,
                                            'development_name' => $projectTitle,
                                            'reference_link' => $projectUrl,
                                            'development_reg_soldout_existing' => time(),
                                        ),
                                    'add_tags' =>
                                        array(
                                            $registraion_data['infusionsoftTag'],
                                            $registraion_data['media_source'],
                                            $realtor_tag
                                        )
                                ));
                    }

                } else if ($user_existing_status == 0) {

                    $arrayNew = array(
                        'list_id' => $list_id,
                        'contact' =>
                            array(
                                'first_name' => $registraion_data['firstname'],
                                'last_name' => $registraion_data['lastname'],
                                'email' => $registraion_data['email'],
                                'subscribe' => 'true',
                                'custom_field' =>
                                    array(
                                        'phone_1' => $registraion_data['phone'],
                                        'company' => 'CONNECT asset management',
                                        'realtor' => $realtor_,
                                        'prospect' => $prospect,
                                        'comment_question' => $projectName,
                                        'development_name' => $projectTitle,
                                        'reference_link' => $projectUrl,
                                        'development_reg_soldout_new_user' => time(),
                                    ),
                                'add_tags' =>
                                    array(
                                        $registraion_data['infusionsoftTag'],
                                        $registraion_data['media_source'],
                                        $realtor_tag
                                    )
                            ));
                }

            }
        }

        if (empty($arrayNew)) {
            $arrayNew = array(
                'list_id' => $list_id,
                'contact' =>
                    array(
                        'first_name' => $registraion_data['firstname'],
                        'last_name' => $registraion_data['lastname'],
                        'email' => $registraion_data['email'],
                        'subscribe' => 'true',
                        'custom_field' =>
                            array(
                                'phone_1' => $registraion_data['phone'],
                                'company' => 'CONNECT asset management',
                                'realtor' => $realtor_,
                                'prospect' => $prospect,
                                'comment_question' => $projectName,
                                'development_name' => $projectTitle,
                                'reference_link' => $projectUrl,
                            ),
                        'add_tags' =>
                            array(
                                $registraion_data['infusionsoftTag'],
                                $registraion_data['media_source'],
                                $realtor_tag
                            )
                    ));
        }

        $arrayNewsletter = $arrayNew;
        if($arrayNewsletter['list_id'] == 1){
            $arrayNewsletter['list_id'] = 3;
            $contactsNew->post_new_contact_into_list($arrayNewsletter);
        }

        $contactsNew->post_new_contact_into_list($arrayNew);

    }



}


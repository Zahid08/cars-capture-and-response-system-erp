<?php

namespace api\modules\v1\controllers;

use api\common\maropost\tags;
use api\models\User;
use api\modules\v1\models\Agent;
use api\modules\v1\models\City;
use api\modules\v1\models\ContactsBrands;
use api\modules\v1\models\ContactsEmailLog;
use api\modules\v1\models\ContactsInteractions;
use api\modules\v1\models\ContactsReservations;
use api\modules\v1\models\ContactsSysteminfo;
use api\modules\v1\models\ContactsTagsApplied;
use api\modules\v1\models\ContactsTagsNew;
use api\modules\v1\models\ContactsTagsRelation;
use api\modules\v1\models\Developments;
use api\modules\v1\models\MediaLeads;
use api\modules\v1\models\Province;
use Yii;
use yii\base\Exception;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use yii\web\ServerErrorHttpException;
use yii\web\Response;
use yii\rest\ActiveController;
use yii\filters\RateLimiter;
use yii\filters\ContentNegotiator;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use api\helpers\CommonHelper;
use api\modules\v1\models\Contacts;
use api\modules\v1\models\search\ContactsSearch;
use api\common\maropost\maropost;
use api\modules\v1\models\ContactsRegistration;
use api\modules\v1\models\ContactsTags;

class ContactsController extends ActiveController
{
    public $modelClass = Contacts::class;
    public $modelAgentClass = Agent::class;

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];

        $behaviors['rateLimiter'] = [
            'class' => RateLimiter::className(),
            'enableRateLimitHeaders' => false,
        ];

        $behaviors['corsFilter'] = [
        'class' => \yii\filters\Cors::className(),
        'cors' => [
            'Origin' => ['*'],
            'Access-Control-Request-Method' =>  ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
            'Access-Control-Request-Headers' => ['*'],
            'Access-Control-Allow-Credentials' => true,
            'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page', 'X-Pagination-Page-Count', 'X-Pagination-Per-Page', 'X-Pagination-Total-Count', 'Link'],
        ],
    ];

        return $behaviors;
    }

    /**
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();

        unset($actions['view'], $actions['create'], $actions['update'], $actions['delete'], $actions['options']);

        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    /**
     * List all records page by page
     *
     * HTTP Verb: GET
     * URL: /companies
     *
     * @return \yii\data\ActiveDataProvider
     * @throws HttpException
     */
    public function prepareDataProvider()
    {
        $searchModel = new ContactsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if($dataProvider->getCount() <= 0) {
            throw new HttpException(404, 'No results found.');
        }
        else {
            return $dataProvider;
        }
    }

    /**
     * Return the details of the record {id}
     *
     * HTTP Verb: GET
     * URL: /companies/{id}
     *
     * @param $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->findModel($id);
    }

    /**
     * Create a new record
     *
     * HTTP Verb: POST
     * URL: /companies
     * 'Content-Type': 'multipart/form-data'
     *
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionCreate()
    {

        $modelClass = $this->modelClass;
        $model = new $modelClass();
        //$contactsregistration = new ContactsRegistration();

        if ($model->load(Yii::$app->getRequest()->getBodyParams())) {

            return $this->saveData($model);

        }
        else{
            throw new BadRequestHttpException('Unable to process data due to invalid data type. Expecting: array. Example data: ModelName[fieldName]=Field Value');
        }
    }

    /**
     * Updates an existing record
     *
     * HTTP Verb: PATCH, PUT
     * URL: /companies/{id}
     * 'Content-Type': 'application/x-www-form-urlencoded'
     *
     * @param $id
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->getRequest()->getBodyParams()))
        {
            return $this->saveData($model);
        }
        else{
            throw new BadRequestHttpException('Unable to process data due to invalid data type. Expecting: array. Example data: ModelName[fieldName]=Field Value');
        }
    }


    /**
     * Deletes an existing record
     *
     * HTTP Verb: DELETE
     * URL: /companies/{id}
     *
     * @param $id
     * @return array
     * @throws ServerErrorHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        try {
            if (!$model->delete()) {
                throw new Exception('The record cannot be deleted.');
            }

            // Delete Image:
            // FileHelper::removeFile($model->image, 'sanction');

            Yii::$app->getResponse()->setStatusCode(200);
            return ['status' => 200, 'message' => 'The record have been successfully deleted.'];
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            if($e->getName() == CommonHelper::INTEGRITY_CONSTRAINT_VIOLATION){
                $message = "This record has relational data. You must delete the following data first";
                $message .= "<ul>";
                $message .= "<li>Example => Example List</li>";
                $message .= "</ul>";
            }
            throw new ServerErrorHttpException($message);
        }

    }

    /**
     * Save Data.
     *
     * @param $model
     * @return mixed
     * @throws ServerErrorHttpException
     */
    private function saveData($model){

        if (isset($model)) {

            //$transaction = Yii::$app->db->beginTransaction();
            try {
                $post = Yii::$app->request->post();

                // Realtor or Not (If yes then data store agent table)
                // Development Registration (Prospect and Registration Table)
                // Contact Form (Only Prospect Table)
                // Global Registration Form (Only Prospect Table)


                if(!empty($model)) {

                    if ($model->realtor == 1) {

                        $agentModel = new Agent();

                        $checkEmail = Agent::find()->where(['email' => $model->email])->one();

                        if (!empty($checkEmail)) {
                            $agentModel = $this->findAgentModel($checkEmail->id);
                            $agentModel->first_name = $model->first_name;
                            $agentModel->last_name = $model->last_name;
                            $agentModel->email = $model->email;
                            $agentModel->mobile = $model->phone;
                            $agentModel->agent_type = '1';
                            $agentModel->brand = '1';
                            if($agentModel->save()){
                                echo "successfully save";
                            }
                        }else{
                            $agentModel->first_name = $model->first_name;
                            $agentModel->last_name = $model->last_name;
                            $agentModel->email = $model->email;
                            $agentModel->mobile = $model->phone;
                            $agentModel->agent_type = '1';
                            $agentModel->brand = '1';
                            if($agentModel->save()){
                               echo "successfully save";
                            }
                        }

                        if ($model->contact_type == 'development_registration') {
                            $baseUrl = Yii::getAlias('@baseUrl');
                            $carsContactUrl=$baseUrl . '/backend/web/contacts/agent/update?id=' . $agentModel->id;
                            $this->sendEmailFromConnectWp($model,$carsContactUrl,'');
                        }

                        if ($model->contact_type == 'global_registration') {
                            if (isset($model->learn_cnt)){
                                if ($agentModel->save()){
                                    $mediaLeads = MediaLeads::find()->where(['id' => $model->lead_source])->one();
                                    if (!empty($mediaLeads)) {
                                        $leadName = $mediaLeads->lead_source_name;
                                    }
                                    $baseUrl = Yii::getAlias('@baseUrl');
                                    $dataList = array(
                                        'media_source' => $leadName,
                                        'pageUrl' => $model->page_url,
                                        'email' => $model->email,
                                        'firstname' => $model->first_name,
                                        'lastname' => $model->last_name,
                                        'phone' => $model->phone,
                                        'realtor' => '1',
                                        'cars_url' => '' . $baseUrl . '/backend/web/contacts/contacts/view?id=' . $agentModel->id
                                    );
                                    $this->sendToMaropostForLearnConnect($dataList);
                                }
                            }
                        }

                    }else {
                        if ($model->contact_type == 'development_registration') {

                            $checkEmail = Contacts::find()->where(['email' => $model->email])->one();

                            if (!empty($checkEmail)) {
                                $brandName = ContactsBrands::find()->where(['contact_id' => $checkEmail->id])->andWhere(['brand_id' => $model->brand_name])->one();
                                $model = $this->findModel($checkEmail->id);
                                if ($model->load(Yii::$app->getRequest()->getBodyParams())) {

                                    $model->save();

                                    // 1 = Registration
                                    // Contacts Registration Table

                                    $contactsregistration = new ContactsRegistration();
                                    $contactsregistration->contact_id = $checkEmail->id;
                                    $contactsregistration->development_id = $model->development_id;
                                    $contactsregistration->reg_source = $model->lead_source;
                                    $contactsregistration->reg_datetime = $model->reg_date;
                                    $contactsregistration->status_date =$model->visit_date;

                                    if (!$contactsregistration->save(false)) {
                                        throw new Exception(json_encode($contactsregistration->errors));
                                    }

                                    //tag information
                                    $contactsTag = new ContactsTagsNew();
                                    $contactsTag->tag_name =$model->tag_name;
                                    $contactsTag->tag_category_id=1;
                                    $checkTags = ContactsTagsNew::find()->where(['tag_name' => $contactsTag->tag_name])->one();
                                    if (empty($checkTags)){
                                        if ($contactsTag->save(false)) {
                                            //save data relation table
                                            $tagRelation=new ContactsTagsRelation();
                                            $tagRelation->contact_id=$checkEmail->id;
                                            $tagRelation->tag_id=$contactsTag->id;
                                            $tagRelation->date_applied=date('Y-m-d H:i:s');
                                            if (!$tagRelation->save(false)) {
                                                throw new Exception(json_encode($tagRelation->errors));
                                            }
                                            //save data to applied tag
                                            $tagApplied=new ContactsTagsApplied();
                                            $tagApplied->tag_id=$contactsTag->id;
                                            if (!$tagApplied->save(false)) {
                                                throw new Exception(json_encode($tagApplied->errors));
                                            }

                                        }
                                    }
                                    else{
                                        $tagRelation=new ContactsTagsRelation();
                                        $tagRelation->contact_id=$checkEmail->id;
                                        $tagRelation->tag_id=$checkTags->id;
                                        $tagRelation->date_applied=date('Y-m-d H:i:s');
                                        if (!$tagRelation->save(false)) {
                                            throw new Exception(json_encode($tagRelation->errors));
                                        }
                                        $tagApplied=new ContactsTagsApplied();
                                        $tagApplied->tag_id=$checkTags->id;
                                        if (!$tagApplied->save(false)) {
                                            throw new Exception(json_encode($tagApplied->errors));
                                        }
                                    }

                                    //brand add
                                    if (empty($brandName)) {
                                        $brandInfo = new ContactsBrands();
                                        $brandInfo->contact_id = $checkEmail->id;
                                        $brandInfo->brand_id = $model->brand_name;
                                        $brandInfo->applied_date =$model->visit_date;
                                        if (!$brandInfo->save(false)) {
                                            throw new Exception(json_encode($brandInfo->errors));
                                        }
                                    }


                                    // System Info
                                    $contactsSystemInfo = new ContactsSysteminfo();
                                    $contactsSystemInfo->contact_id = $model->id;
                                    $contactsSystemInfo->page_name = $model->page_name;
                                    $contactsSystemInfo->page_url = $model->page_url;
                                    $contactsSystemInfo->ip_address = $model->ip_address;
                                    $contactsSystemInfo->browser = $model->browser;
                                    $contactsSystemInfo->operating = $model->operating;
                                    $contactsSystemInfo->visit_date = $model->visit_date;

                                    if (!$contactsSystemInfo->save(false)) {
                                        throw new Exception(json_encode($contactsSystemInfo->errors));
                                    }

                                    if ($model->save()){
                                        $baseUrl = Yii::getAlias('@baseUrl');
                                        $carsContactUrl=$baseUrl . '/backend/web/contacts/contacts-registration/update?id=' . $contactsregistration->id;
                                        $this->sendEmailFromConnectWp($model,$carsContactUrl,$checkEmail->id);
                                    }
                                }
                            } else {

                                if ($model->load(Yii::$app->getRequest()->getBodyParams())) {

                                    $model->save();

                                    // 1 = Registration
                                    // Contacts Registration Table
                                    $contactsregistration = new ContactsRegistration();
                                    $contactsregistration->contact_id = $model->id;
                                    $contactsregistration->development_id = $model->development_id;
                                    $contactsregistration->reg_source = $model->lead_source;
                                    $contactsregistration->reg_datetime = $model->reg_date;
                                    $contactsregistration->status_date =$model->visit_date;
                                    if (!$contactsregistration->save(false)) {
                                        throw new Exception(json_encode($contactsregistration->errors));
                                    }

                                    //tag information
                                    $contactsTag = new ContactsTagsNew();
                                    $contactsTag->tag_name =$model->tag_name;
                                    $contactsTag->tag_category_id=1;
                                    $checkTags = ContactsTagsNew::find()->where(['tag_name' => $contactsTag->tag_name])->one();
                                    if (empty($checkTags)){
                                        if ($contactsTag->save(false)) {
                                            //save data relation table
                                            $tagRelation=new ContactsTagsRelation();
                                            $tagRelation->contact_id=$model->id;
                                            $tagRelation->tag_id=$contactsTag->id;
                                            $tagRelation->date_applied=date('Y-m-d H:i:s');
                                            if (!$tagRelation->save(false)) {
                                                throw new Exception(json_encode($tagRelation->errors));
                                            }
                                            //save data to applied tag
                                            $tagApplied=new ContactsTagsApplied();
                                            $tagApplied->tag_id=$contactsTag->id;
                                            if (!$tagApplied->save(false)) {
                                                throw new Exception(json_encode($tagApplied->errors));
                                            }

                                        }
                                    }
                                    else{
                                        $tagRelation=new ContactsTagsRelation();
                                        $tagRelation->contact_id=$model->id;
                                        $tagRelation->tag_id=$checkTags->id;
                                        $tagRelation->date_applied=date('Y-m-d H:i:s');
                                        if (!$tagRelation->save(false)) {
                                            throw new Exception(json_encode($tagRelation->errors));
                                        }
                                        //save data to applied tag
                                        $tagApplied=new ContactsTagsApplied();
                                        $tagApplied->tag_id=$checkTags->id;
                                        if (!$tagApplied->save(false)) {
                                            throw new Exception(json_encode($tagApplied->errors));
                                        }
                                    }

                                    //brand add
                                    //$brandName = ContactsBrands::find()->where(['contact_id' => $checkEmail->id])->andWhere(['brand_id' => $model->brand_name])->one();

                                    $brandInfo = new ContactsBrands();
                                    $brandInfo->contact_id = $model->id;
                                    $brandInfo->brand_id = $model->brand_name;
                                    $brandInfo->applied_date =$model->visit_date;

                                    if (!$brandInfo->save(false)) {
                                        throw new Exception(json_encode($brandInfo->errors));
                                    }



                                    // System Info
                                    $contactsSystemInfo = new ContactsSysteminfo();
                                    $contactsSystemInfo->contact_id = $model->id;
                                    $contactsSystemInfo->page_name = $model->page_name;
                                    $contactsSystemInfo->page_url = $model->page_url;
                                    $contactsSystemInfo->ip_address = $model->ip_address;
                                    $contactsSystemInfo->browser = $model->browser;
                                    $contactsSystemInfo->operating = $model->operating;
                                    $contactsSystemInfo->visit_date = $model->visit_date;

                                    if (!$contactsSystemInfo->save(false)) {
                                        throw new Exception(json_encode($contactsSystemInfo->errors));
                                    }
                                    //Email Log Save

                                    if ($model->save()){
                                        $baseUrl = Yii::getAlias('@baseUrl');
                                        $carsContactUrl=$baseUrl . '/backend/web/contacts/contacts-registration/update?id=' . $contactsregistration->id;
                                        $this->sendEmailFromConnectWp($model,$carsContactUrl,$model->id);
                                    }
                                }
                            }
                        } elseif ($model->contact_type == 'global_registration') {
                            // print_r(date('Y-m-d H:i:s'));exit();
                            $checkEmail = Contacts::find()->where(['email' => $model->email])->one();

                            if (!empty($checkEmail)) {
                                $brandName = ContactsBrands::find()->where(['contact_id' => $checkEmail->id])->andWhere(['brand_id' => $model->brand_name])->one();

                                $model = $this->findModel($checkEmail->id);
                                if ($model->load(Yii::$app->getRequest()->getBodyParams())) {

                                    $model->save();

                                    //tag information
                                    $contactsTag = new ContactsTagsNew();
                                    $contactsTag->tag_name =$model->tag_name;
                                    $contactsTag->tag_category_id=5;
                                    $checkTags = ContactsTagsNew::find()->where(['tag_name' => $contactsTag->tag_name])->one();
                                    if (empty($checkTags)){
                                        if ($contactsTag->save(false)) {
                                            //save data relation table
                                            $tagRelation=new ContactsTagsRelation();
                                            $tagRelation->contact_id=$checkEmail->id;
                                            $tagRelation->tag_id=$contactsTag->id;
                                            $tagRelation->date_applied=date('Y-m-d H:i:s');
                                            if (!$tagRelation->save(false)) {
                                                throw new Exception(json_encode($tagRelation->errors));
                                            }
                                            //save data to applied tag
                                            $tagApplied=new ContactsTagsApplied();
                                            $tagApplied->tag_id=$contactsTag->id;
                                            if (!$tagApplied->save(false)) {
                                                throw new Exception(json_encode($tagApplied->errors));
                                            }

                                        }
                                    }
                                    else{
                                        $tagRelation=new ContactsTagsRelation();
                                        $tagRelation->contact_id=$checkEmail->id;
                                        $tagRelation->tag_id=$checkTags->id;
                                        $tagRelation->date_applied=date('Y-m-d H:i:s');
                                        if (!$tagRelation->save(false)) {
                                            throw new Exception(json_encode($tagRelation->errors));
                                        }
                                        $tagApplied=new ContactsTagsApplied();
                                        $tagApplied->tag_id=$checkTags->id;
                                        if (!$tagApplied->save(false)) {
                                            throw new Exception(json_encode($tagApplied->errors));
                                        }
                                    }


                                    if (empty($brandName)) {
                                        //brand add
                                        $brandInfo = new ContactsBrands();
                                        $brandInfo->contact_id = $checkEmail->id;
                                        $brandInfo->brand_id = $model->brand_name;
                                        $brandInfo->applied_date =$model->visit_date;
                                        if (!$brandInfo->save(false)) {
                                            throw new Exception(json_encode($brandInfo->errors));
                                        }
                                    }

                                    // System Info
                                    $contactsSystemInfo = new ContactsSysteminfo();
                                    $contactsSystemInfo->contact_id = $checkEmail->id;
                                    $contactsSystemInfo->page_name = $model->page_name;
                                    $contactsSystemInfo->page_url = $model->page_url;
                                    $contactsSystemInfo->ip_address = $model->ip_address;
                                    $contactsSystemInfo->browser = $model->browser;
                                    $contactsSystemInfo->operating = $model->operating;
                                    $contactsSystemInfo->visit_date = $model->visit_date;

                                    if (!$contactsSystemInfo->save(false)) {
                                        throw new Exception(json_encode($contactsSystemInfo->errors));
                                    }

                                    if (isset($model->learn_cnt)){
                                        if ($model->save()){
                                            $mediaLeads = MediaLeads::find()->where(['id' => $model->lead_source])->one();
                                            if (!empty($mediaLeads)) {
                                                $leadName = $mediaLeads->lead_source_name;
                                            }
                                            $baseUrl = Yii::getAlias('@baseUrl');
                                            $dataList = array(
                                                'media_source' => $leadName,
                                                'pageUrl' => $model->page_url,
                                                'email' => $model->email,
                                                'firstname' => $model->first_name,
                                                'lastname' => $model->last_name,
                                                'phone' => $model->phone,
                                                'realtor' => '',
                                                'cars_url' => '' . $baseUrl . '/backend/web/contacts/contacts/view?id=' . $checkEmail->id
                                            );
                                          $this->sendToMaropostForLearnConnect($dataList);
                                        }
                                    }

                                }
                            } else {
                                $contactModel = new Contacts();
                                $contactModel->first_name = $model->first_name;
                                $contactModel->last_name = $model->last_name;
                                $contactModel->email = $model->email;
                                $contactModel->phone = $model->phone;
                                $contactModel->lead_source =$model->lead_source;
                                if ($contactModel->save()) {

                                    //tag information
                                    $contactsTag = new ContactsTagsNew();
                                    $contactsTag->tag_name =$model->tag_name;
                                    $contactsTag->tag_category_id=5;
                                    $checkTags = ContactsTagsNew::find()->where(['tag_name' => $contactsTag->tag_name])->one();
                                    if (empty($checkTags)){
                                        if ($contactsTag->save(false)) {
                                            //save data relation table
                                            $tagRelation=new ContactsTagsRelation();
                                            $tagRelation->contact_id=$contactModel->id;
                                            $tagRelation->tag_id=$contactsTag->id;
                                            $tagRelation->date_applied=date('Y-m-d H:i:s');
                                            if (!$tagRelation->save(false)) {
                                                throw new Exception(json_encode($tagRelation->errors));
                                            }
                                            //save data to applied tag
                                            $tagApplied=new ContactsTagsApplied();
                                            $tagApplied->tag_id=$contactsTag->id;
                                            if (!$tagApplied->save(false)) {
                                                throw new Exception(json_encode($tagApplied->errors));
                                            }

                                        }
                                    }
                                    else{
                                        $tagRelation=new ContactsTagsRelation();
                                        $tagRelation->contact_id=$contactModel->id;
                                        $tagRelation->tag_id=$checkTags->id;
                                        $tagRelation->date_applied=date('Y-m-d H:i:s');
                                        if (!$tagRelation->save(false)) {
                                            throw new Exception(json_encode($tagRelation->errors));
                                        }
                                        //save data to applied tag
                                        $tagApplied=new ContactsTagsApplied();
                                        $tagApplied->tag_id=$checkTags->id;
                                        if (!$tagApplied->save(false)) {
                                            throw new Exception(json_encode($tagApplied->errors));
                                        }
                                    }

                                    //brand add
                                    $brandInfo = new ContactsBrands();
                                    $brandInfo->contact_id = $contactModel->id;
                                    $brandInfo->brand_id = $model->brand_name;
                                    $brandInfo->applied_date =$model->visit_date;
                                    if (!$brandInfo->save(false)) {
                                        throw new Exception(json_encode($brandInfo->errors));
                                    }

                                    // System Info
                                    $contactsSystemInfo = new ContactsSysteminfo();
                                    $contactsSystemInfo->contact_id = $contactModel->id;
                                    $contactsSystemInfo->page_name = $model->page_name;
                                    $contactsSystemInfo->page_url = $model->page_url;
                                    $contactsSystemInfo->ip_address = $model->ip_address;
                                    $contactsSystemInfo->browser = $model->browser;
                                    $contactsSystemInfo->operating = $model->operating;
                                    $contactsSystemInfo->visit_date = $model->visit_date;

                                    if (!$contactsSystemInfo->save(false)) {
                                        throw new Exception(json_encode($contactsSystemInfo->errors));
                                    }

                                    if (isset($model->learn_cnt)){
                                        if ($contactModel->save()){
                                            $mediaLeads = MediaLeads::find()->where(['id' => $model->lead_source])->one();
                                            if (!empty($mediaLeads)) {
                                                $leadName = $mediaLeads->lead_source_name;
                                            }
                                            $baseUrl = Yii::getAlias('@baseUrl');
                                            $dataList = array(
                                                'media_source' => $leadName,
                                                'pageUrl' => $model->page_url,
                                                'email' => $model->email,
                                                'firstname' => $model->first_name,
                                                'lastname' => $model->last_name,
                                                'phone' => $model->phone,
                                                'realtor' => '',
                                                'cars_url' => '' . $baseUrl . '/backend/web/contacts/contacts/view?id=' . $contactModel->id
                                            );
                                            $this->sendToMaropostForLearnConnect($dataList);
                                        }
                                    }
                                }
                            }

                        } elseif ($model->contact_type == 'contact') {

                            $checkEmail = Contacts::find()->where(['email' => $model->email])->one();

                            if (!empty($checkEmail)) {
                                $brandName = ContactsBrands::find()->where(['contact_id' => $checkEmail->id])->andWhere(['brand_id' => $model->brand_name])->one();

                                $model = $this->findModel($checkEmail->id);

                                if ($model->load(Yii::$app->getRequest()->getBodyParams())) {
                                    $model->save();

                                    //tag information
                                    $contactsTag = new ContactsTagsNew();
                                    $contactsTag->tag_name =$model->tag_name;
                                    $contactsTag->tag_category_id=5;
                                    $checkTags = ContactsTagsNew::find()->where(['tag_name' => $contactsTag->tag_name])->one();
                                    if (empty($checkTags)){
                                        if ($contactsTag->save(false)) {
                                            //save data relation table
                                            $tagRelation=new ContactsTagsRelation();
                                            $tagRelation->contact_id=$checkEmail->id;
                                            $tagRelation->tag_id=$contactsTag->id;
                                            $tagRelation->date_applied=date('Y-m-d H:i:s');
                                            if (!$tagRelation->save(false)) {
                                                throw new Exception(json_encode($tagRelation->errors));
                                            }
                                            //save data to applied tag
                                            $tagApplied=new ContactsTagsApplied();
                                            $tagApplied->tag_id=$contactsTag->id;
                                            if (!$tagApplied->save(false)) {
                                                throw new Exception(json_encode($tagApplied->errors));
                                            }

                                        }
                                    }
                                    else{
                                        $tagRelation=new ContactsTagsRelation();
                                        $tagRelation->contact_id=$checkEmail->id;
                                        $tagRelation->tag_id=$checkTags->id;
                                        $tagRelation->date_applied=date('Y-m-d H:i:s');
                                        if (!$tagRelation->save(false)) {
                                            throw new Exception(json_encode($tagRelation->errors));
                                        }
                                        $tagApplied=new ContactsTagsApplied();
                                        $tagApplied->tag_id=$checkTags->id;
                                        if (!$tagApplied->save(false)) {
                                            throw new Exception(json_encode($tagApplied->errors));
                                        }
                                    }

                                    if (empty($brandName)) {
                                        //brand add
                                        $brandInfo = new ContactsBrands();
                                        $brandInfo->contact_id = $checkEmail->id;
                                        $brandInfo->brand_id = $model->brand_name;
                                        $brandInfo->applied_date =$model->visit_date;
                                        if (!$brandInfo->save(false)) {
                                            throw new Exception(json_encode($brandInfo->errors));
                                        }
                                    }

                                    //interaction record
                                    $contactInteractionInfo = new ContactsInteractions();
                                    $contactInteractionInfo->contact_id = $checkEmail->id;
                                    $contactInteractionInfo->interaction_note = $model->interaction_note;
                                    $contactInteractionInfo->interaction_type = $model->interaction_type;
                                    $contactInteractionInfo->interaction_datetime =$model->visit_date;
                                    $contactInteractionInfo->created_at =$model->visit_date;
                                    $contactInteractionInfo->created_by = $model->created_by;
                                    if (!$contactInteractionInfo->save(false)) {
                                        throw new Exception(json_encode($contactInteractionInfo->errors));
                                    }
                                    // System Info
                                    $contactsSystemInfo = new ContactsSysteminfo();
                                    $contactsSystemInfo->contact_id = $checkEmail->id;
                                    $contactsSystemInfo->page_name = $model->page_name;
                                    $contactsSystemInfo->page_url = $model->page_url;
                                    $contactsSystemInfo->ip_address = $model->ip_address;
                                    $contactsSystemInfo->browser = $model->browser;
                                    $contactsSystemInfo->operating = $model->operating;
                                    $contactsSystemInfo->visit_date = $model->visit_date;

                                    if (!$contactsSystemInfo->save(false)) {
                                        throw new Exception(json_encode($contactsSystemInfo->errors));
                                    }

                                }
                            } else {
                                if ($model->load(Yii::$app->getRequest()->getBodyParams())) {

                                    $model->save();

                                    //tag information
                                    $contactsTag = new ContactsTagsNew();
                                    $contactsTag->tag_name =$model->tag_name;
                                    $contactsTag->tag_category_id=5;
                                    $checkTags = ContactsTagsNew::find()->where(['tag_name' => $contactsTag->tag_name])->one();
                                    if (empty($checkTags)){
                                        if ($contactsTag->save(false)) {
                                            //save data relation table
                                            $tagRelation=new ContactsTagsRelation();
                                            $tagRelation->contact_id=$model->id;
                                            $tagRelation->tag_id=$contactsTag->id;
                                            $tagRelation->date_applied=date('Y-m-d H:i:s');
                                            if (!$tagRelation->save(false)) {
                                                throw new Exception(json_encode($tagRelation->errors));
                                            }
                                            //save data to applied tag
                                            $tagApplied=new ContactsTagsApplied();
                                            $tagApplied->tag_id=$contactsTag->id;
                                            if (!$tagApplied->save(false)) {
                                                throw new Exception(json_encode($tagApplied->errors));
                                            }

                                        }
                                    }
                                    else{
                                        $tagRelation=new ContactsTagsRelation();
                                        $tagRelation->contact_id=$model->id;
                                        $tagRelation->tag_id=$checkTags->id;
                                        $tagRelation->date_applied=date('Y-m-d H:i:s');
                                        if (!$tagRelation->save(false)) {
                                            throw new Exception(json_encode($tagRelation->errors));
                                        }
                                        //save data to applied tag
                                        $tagApplied=new ContactsTagsApplied();
                                        $tagApplied->tag_id=$checkTags->id;
                                        if (!$tagApplied->save(false)) {
                                            throw new Exception(json_encode($tagApplied->errors));
                                        }
                                    }

                                    //brand add
                                    $brandInfo = new ContactsBrands();
                                    $brandInfo->contact_id = $model->id;
                                    $brandInfo->brand_id = $model->brand_name;
                                    $brandInfo->applied_date =$model->visit_date;
                                    if (!$brandInfo->save(false)) {
                                        throw new Exception(json_encode($brandInfo->errors));
                                    }

                                    //interaction record
                                    $contactInteractionInfo = new ContactsInteractions();
                                    $contactInteractionInfo->contact_id = $model->id;
                                    $contactInteractionInfo->interaction_note = $model->interaction_note;
                                    $contactInteractionInfo->interaction_type = $model->interaction_type;
                                    $contactInteractionInfo->interaction_datetime =$model->visit_date;
                                    $contactInteractionInfo->created_at =$model->visit_date;
                                    $contactInteractionInfo->created_by = $model->created_by;

                                    if (!$contactInteractionInfo->save(false)) {
                                        throw new Exception(json_encode($contactInteractionInfo->errors));
                                    }

                                    // System Info
                                    $contactsSystemInfo = new ContactsSysteminfo();
                                    $contactsSystemInfo->contact_id = $model->id;
                                    $contactsSystemInfo->page_name = $model->page_name;
                                    $contactsSystemInfo->page_url = $model->page_url;
                                    $contactsSystemInfo->ip_address = $model->ip_address;
                                    $contactsSystemInfo->browser = $model->browser;
                                    $contactsSystemInfo->operating = $model->operating;
                                    $contactsSystemInfo->visit_date = $model->visit_date;

                                    if (!$contactsSystemInfo->save(false)) {
                                        throw new Exception(json_encode($contactsSystemInfo->errors));
                                    }
                                }
                            }
                        } elseif ($model->contact_type == 'development_reservation') {
                            if ($model->realtor == 'Yes') {
                                $agentModel = new Agent();
                                $contactModel = new Contacts();
                                $cityInfo = City::find()->where(['city_name' => $model->city])->one();
                                if (!empty($cityInfo)) {
                                    $cityId = $cityInfo->id;
                                } else {
                                    $cityId = '';
                                }
                                $proviInfo = Province::find()->where(['province_name' => $model->province])->one();
                                if (!empty($proviInfo)) {
                                    $provId = $proviInfo->id;
                                } else {
                                    $provId = '';
                                }
                                $checkEmail = Agent::find()->where(['email' => $model->email])->one();
                                if (!empty($checkEmail)) {
                                    $agentModel = $this->findAgentModel($checkEmail->id);
                                    $agentModel->first_name = $model->first_name;
                                    $agentModel->last_name = $model->last_name;
                                    $agentModel->email = $model->email;
                                    $agentModel->mobile =(string)$model->phone;
                                    $agentModel->agent_type = '1';
                                    $agentModel->brand = '1';
                                    $agentModel->address =$model->address;
                                    $agentModel->province =(string)$provId;
                                    $agentModel->city =(string)$cityId;
                                    $agentModel->postal_code =$model->postal;
                                    if ($agentModel->save()){
                                     echo "Successfully save";
                                    }
                                }else{
                                    $agentModel->first_name = $model->first_name;
                                    $agentModel->last_name = $model->last_name;
                                    $agentModel->email = $model->email;
                                    $agentModel->mobile =(string)$model->phone;
                                    $agentModel->agent_type = '1';
                                    $agentModel->brand = '1';
                                    $agentModel->address =$model->address;
                                    $agentModel->province =(string)$provId;
                                    $agentModel->city =(string)$cityId;
                                    $agentModel->postal_code =$model->postal;
                                    if ($agentModel->save()){
                                        echo "Successfully save";
                                    }
                                }
                                /*@@reservation post to maropost and email fire (Connect website reservation form)*/
                                if ($agentModel->save()) {
                                    $mediaLeads = MediaLeads::find()->where(['id' => $model->lead_source])->one();
                                    if (!empty($mediaLeads)) {
                                        $leadName = $mediaLeads->lead_source_name;
                                    }
                                    $baseUrl = Yii::getAlias('@baseUrl');
                                    $dataList = array(
                                        'development_id' => $model->development_id,
                                        'media_source' => $leadName,
                                        'pageUrl' => $model->page_url,
                                        'email' => $model->email,
                                        'firstname' => $model->first_name,
                                        'lastname' => $model->last_name,
                                        'phone' => $model->phone,
                                        'realtor' => '1',
                                        'cars_url' => '' . $baseUrl . '/backend/web/contacts/agent/update?id=' . $agentModel->id
                                    );

                                    $this->reservation_form($dataList);
                                    $this->sendEmail($model, $dataList['media_source'], $dataList['cars_url'], $dataList['pageUrl']);
                                }
                                } else {
                                 //without realtor
                                $contactModel = new Contacts();
                                $cityInfo = City::find()->where(['city_name' => $model->city])->one();
                                if (!empty($cityInfo)) {
                                    $cityId = $cityInfo->id;
                                } else {
                                    $cityId = '';
                                }
                                $proviInfo = Province::find()->where(['province_name' => $model->province])->one();
                                if (!empty($proviInfo)) {
                                    $provId = $proviInfo->id;
                                } else {
                                    $provId = '';
                                }
                                $contactReservation = new ContactsReservations();

                                $checkEmail = Contacts::find()->where(['email' => $model->email])->one();
                                if (!empty($checkEmail)) {
                                    $brandName = ContactsBrands::find()->where(['contact_id' => $checkEmail->id])->andWhere(['brand_id' => $model->brand_name])->one();
                                    $contactModel = $this->findModel($checkEmail->id);
                                    $contactModel->first_name = $model->first_name;
                                    $contactModel->last_name = $model->last_name;
                                    $contactModel->email = $model->email;
                                    $contactModel->phone = (string)$model->phone;
                                    $contactModel->address = $model->address;
                                    $contactModel->city = (string)$cityId;
                                    $contactModel->province = (string)$provId;
                                    $contactModel->postal = $model->postal;
                                    $contactModel->date_of_birth = $model->date_of_birth;
                                    $contactModel->drivers_license = $model->drivers_license;
                                    $contactModel->profession = $model->profession;
                                    $contactModel->SIN = $model->SIN;
                                    $contactModel->created_at =$model->visit_date;
                                    $contactModel->lead_source = $model->lead_source;
                                    if ($contactModel->save()) {

                                        if (empty($brandName)) {
                                            //brand add
                                            $brandInfo = new ContactsBrands();
                                            $brandInfo->contact_id = $checkEmail->id;
                                            $brandInfo->brand_id = $model->brand_name;
                                            $brandInfo->applied_date =$model->visit_date;
                                            if (!$brandInfo->save(false)) {
                                                throw new Exception(json_encode($brandInfo->errors));
                                            }
                                        }
                                        //tag information
                                        $contactsTag = new ContactsTagsNew();
                                        $contactsTag->tag_name = $model->tag_name;
                                        $contactsTag->tag_category_id=4;
                                        $checkTags = ContactsTagsNew::find()->where(['tag_name' => $contactsTag->tag_name])->one();
                                        if (empty($checkTags)){
                                            if ($contactsTag->save(false)) {
                                                //save data relation table
                                                $tagRelation=new ContactsTagsRelation();
                                                $tagRelation->contact_id=$checkEmail->id;
                                                $tagRelation->tag_id=$contactsTag->id;
                                                if (!$tagRelation->save(false)) {
                                                    throw new Exception(json_encode($tagRelation->errors));
                                                }
                                                //save data to applied tag
                                                $tagApplied=new ContactsTagsApplied();
                                                $tagApplied->tag_id=$contactsTag->id;
                                                if (!$tagApplied->save(false)) {
                                                    throw new Exception(json_encode($tagApplied->errors));
                                                }

                                            }
                                        }
                                        else{
                                            $tagRelation=new ContactsTagsRelation();
                                            $tagRelation->contact_id=$checkEmail->id;
                                            $tagRelation->tag_id=$checkTags->id;
                                            if (!$tagRelation->save(false)) {
                                                throw new Exception(json_encode($tagRelation->errors));
                                            }
                                            $tagApplied=new ContactsTagsApplied();
                                            $tagApplied->tag_id=$checkTags->id;
                                            if (!$tagApplied->save(false)) {
                                                throw new Exception(json_encode($tagApplied->errors));
                                            }
                                        }

                                        //Contact Reservation Save
                                        $contactReservation = new ContactsReservations();
                                        $contactReservation->contact_id = $checkEmail->id;
                                        $contactReservation->development_id = $model->development_id;
                                        $contactReservation->floor_plans_1st_choice = $model->ffp_choice_no_1;
                                        $contactReservation->floor_plans_2nd_choice = $model->ffp_choice_no_2;
                                        $contactReservation->floor_plans_3rd_choice = $model->ffp_choice_no_3;
                                        $contactReservation->floor_range = $model->desired_floor_range;
                                        $contactReservation->require_parking = $model->do_require_parking;
                                        $contactReservation->require_locker = $model->do_require_locker;
                                        $contactReservation->nature_of_purchase = $model->intended_nature_purchase;
                                        if (!$contactReservation->save(false)) {
                                            throw new Exception(json_encode($contactReservation->errors));
                                        }

                                        // System Info
                                        $contactsSystemInfo = new ContactsSysteminfo();
                                        $contactsSystemInfo->contact_id = $checkEmail->id;
                                        $contactsSystemInfo->page_name = $model->page_name;
                                        $contactsSystemInfo->page_url = $model->page_url;

                                        $contactsSystemInfo->ip_address = $model->ip_address;
                                        $contactsSystemInfo->browser = $model->browser;
                                        $contactsSystemInfo->operating = $model->operating;
                                        $contactsSystemInfo->visit_date = $model->visit_date;

                                        if (!$contactsSystemInfo->save(false)) {
                                            throw new Exception(json_encode($contactsSystemInfo->errors));
                                        }
                                    }
                                } else {
                                    $contactModel->first_name = $model->first_name;
                                    $contactModel->last_name = $model->last_name;
                                    $contactModel->email = $model->email;
                                    $contactModel->phone = (string)$model->phone;
                                    $contactModel->address = $model->address;
                                    $contactModel->city = (string)$cityId;
                                    $contactModel->province = (string)$provId;
                                    $contactModel->postal = $model->postal;
                                    $contactModel->date_of_birth = $model->date_of_birth;
                                    $contactModel->drivers_license = $model->drivers_license;
                                    $contactModel->profession = $model->profession;
                                    $contactModel->SIN = $model->SIN;
                                    $contactModel->created_at =$model->visit_date;
                                    $contactModel->lead_source = $model->lead_source;

                                    if ($contactModel->save()) {

                                        //tag information
                                        $contactsTag = new ContactsTagsNew();
                                        $contactsTag->tag_name =$model->tag_name;
                                        $contactsTag->tag_category_id=4;
                                        $checkTags = ContactsTagsNew::find()->where(['tag_name' => $contactsTag->tag_name])->one();
                                        if (empty($checkTags)){
                                            if ($contactsTag->save(false)) {
                                                //save data relation table
                                                $tagRelation=new ContactsTagsRelation();
                                                $tagRelation->contact_id=$contactModel->id;
                                                $tagRelation->tag_id=$contactsTag->id;
                                                if (!$tagRelation->save(false)) {
                                                    throw new Exception(json_encode($tagRelation->errors));
                                                }
                                                //save data to applied tag
                                                $tagApplied=new ContactsTagsApplied();
                                                $tagApplied->tag_id=$contactsTag->id;
                                                if (!$tagApplied->save(false)) {
                                                    throw new Exception(json_encode($tagApplied->errors));
                                                }

                                            }
                                        }
                                        else{
                                            $tagRelation=new ContactsTagsRelation();
                                            $tagRelation->contact_id=$contactModel->id;
                                            $tagRelation->tag_id=$checkTags->id;
                                            if (!$tagRelation->save(false)) {
                                                throw new Exception(json_encode($tagRelation->errors));
                                            }
                                            //save data to applied tag
                                            $tagApplied=new ContactsTagsApplied();
                                            $tagApplied->tag_id=$checkTags->id;
                                            if (!$tagApplied->save(false)) {
                                                throw new Exception(json_encode($tagApplied->errors));
                                            }
                                        }


                                        //Contact Reservation Save
                                        $contactReservation = new ContactsReservations();
                                        $contactReservation->contact_id = $contactModel->id;
                                        $contactReservation->development_id = $model->development_id;
                                        $contactReservation->floor_plans_1st_choice = $model->ffp_choice_no_1;
                                        $contactReservation->floor_plans_2nd_choice = $model->ffp_choice_no_2;
                                        $contactReservation->floor_plans_3rd_choice = $model->ffp_choice_no_3;
                                        $contactReservation->floor_range = $model->desired_floor_range;
                                        $contactReservation->require_parking = $model->do_require_parking;
                                        $contactReservation->require_locker = $model->do_require_locker;
                                        $contactReservation->nature_of_purchase = $model->intended_nature_purchase;
                                        if (!$contactReservation->save(false)) {
                                            throw new Exception(json_encode($contactReservation->errors));
                                        }

                                        //brand add
                                        $brandInfo = new ContactsBrands();
                                        $brandInfo->contact_id = $contactModel->id;
                                        $brandInfo->brand_id = $model->brand_name;
                                        $brandInfo->applied_date =$model->visit_date;
                                        if (!$brandInfo->save(false)) {
                                            throw new Exception(json_encode($brandInfo->errors));
                                        }

                                        // System Info
                                        $contactsSystemInfo = new ContactsSysteminfo();
                                        $contactsSystemInfo->contact_id = $contactModel->id;
                                        $contactsSystemInfo->page_name = $model->page_name;
                                        $contactsSystemInfo->page_url = $model->page_url;
                                        $contactsSystemInfo->ip_address = $model->ip_address;
                                        $contactsSystemInfo->browser = $model->browser;
                                        $contactsSystemInfo->operating = $model->operating;
                                        $contactsSystemInfo->visit_date = $model->visit_date;

                                        if (!$contactsSystemInfo->save(false)) {
                                            throw new Exception(json_encode($contactsSystemInfo->errors));
                                        }

                                    }
                                }
                                /*@@reservation post to maropost and email fire (Connect Website Reservation form)*/
                                if ($contactModel->save()) {
                                    $mediaLeads = MediaLeads::find()->where(['id' => $model->lead_source])->one();
                                    if (!empty($mediaLeads)) {
                                        $leadName = $mediaLeads->lead_source_name;
                                    }
                                    $baseUrl = Yii::getAlias('@baseUrl');
                                    $dataList = array(
                                        'development_id' => $model->development_id,
                                        'media_source' => $leadName,
                                        'pageUrl' => $model->page_url,
                                        'email' => $model->email,
                                        'firstname' => $model->first_name,
                                        'lastname' => $model->last_name,
                                        'phone' => $model->phone,
                                        'realtor' => '',
                                        'cars_url' => '' . $baseUrl . '/backend/web/contacts/contacts-reservations/update?id=' . $contactReservation->id
                                    );
                                    $this->reservation_form($dataList);
                                    $this->sendEmail($model, $dataList['media_source'], $dataList['cars_url'], $dataList['pageUrl']);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception $e) {
                //$transaction->rollBack();
                throw new ServerErrorHttpException($e->getMessage());
            }
        }
    }

    /**
     * Finds the record based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $modelClass = $this->modelClass;
        if (($model = $modelClass::findOne($id)) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException("No records found");
        }
    }

    protected function findAgentModel($id)
    {
        $modelClass = $this->modelAgentClass;
        if (($model = $modelClass::findOne($id)) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException("No records found");
        }
    }
    //@@Reservation Form data to maropost "Connect website"
    public  function  reservation_form($registraion_data){
        if (!empty($registraion_data['development_id'])) {
            $develomentList = Developments::find()->where(['development_id' => $registraion_data['development_id']])->one();
           $projectTitle=$projectName=$projectUrl=$project_sales_status='';
            if (!empty($develomentList)) {
                $projectTitle = $develomentList->development_name;
                $projectName = $develomentList->development_name;
                $project_sales_status =$develomentList->sales_status;
            }
            $projectUrl = $registraion_data['pageUrl'];

            $realtor = '';
            $prospect = '';
            $realtor_tag = '';

            if (!empty($registraion_data['realtor'])) {
                $realtor_ = 'true';
                $prospect = 'False';
                $realtor = 1;
                $realtor_tag = 'Realtor';
                $list_id = 17;
            } else {
                $realtor_ = 'False';
                $prospect = 'true';
                $realtor_tag = 'Prospect';
                $realtor = 0;
                $list_id = 1;
            }

            $registraion_data['infusionsoftTag']=$develomentList->development_name.' '.'reservation';
            $registraion_data['media_source']='Original Media Source -'.$registraion_data['media_source'];


            $tags=new tags();
            //tag created if tag not exist in maropost
            $array = array(
                "tags" => array(
                    "name" =>$registraion_data['infusionsoftTag']
                )
            );
            $tags->post_tags($array);


            $array1 = array(
                "tags" => array(
                    "name" =>$registraion_data['media_source']
                )
            );
            $tags->post_tags($array1);

            /*
             * data prepare for maropost
             */
            $contactsNew=new maropost();
            $contactsNew = $contactsNew->contacts();

            $arrayNew = array(
                'list_id' => $list_id,
                'contact' =>
                    array(
                        'first_name' => $registraion_data['firstname'],
                        'last_name' => $registraion_data['lastname'],
                        'email' => $registraion_data['email'],
                        'subscribe' => 'true',
                        'custom_field' =>
                            array(
                                'phone_1' => $registraion_data['phone'],
                                'company' => 'CONNECT asset management',
                                'realtor' => $realtor_,
                                'prospect' => $prospect,
                                'comment_question' => $projectName,
                                'development_name' => $projectTitle,
                                'reference_link' => $projectUrl,
                                'reservation_register_flag' => time(),
                            ),
                        'add_tags' =>
                            array(
                                $registraion_data['infusionsoftTag'],
                                $registraion_data['media_source'],
                                $realtor_tag
                            )
                    ));

                    $arrayNewsletter = $arrayNew;
                    if($arrayNewsletter['list_id'] == 1){
                        $arrayNewsletter['list_id'] = 3;
                        $contactsNew->post_new_contact_into_list($arrayNewsletter);
                    }

                    $contactsNew->post_new_contact_into_list($arrayNew);
        }
    }

    //@@Send Email Reservation form "Connect website reservation form"
    private function sendEmail($model,$media_source,$cars_url,$pageUrl) {
        $develomentList = Developments::find()->where(['development_id' => $model->development_id])->one();
        if (!empty($develomentList)){
            $developmentname='for'.' '.$develomentList->development_name;
        }
        if ($model->realtor == 1){
            $realtor='Yes';
        }
        else{
            $realtor='No';
        }
        $comments_section                 = !empty($model->comments) ? $model->comments : '';

        //$email='admin@connectassetmanagement.com';
        $email='philippe.audet@connectassetmgmt.com';
       // $email2='aiubzahid@gmail.com';

        $subject =$subject ='New Reservation!'.' '.$model->first_name.' '.$model->last_name.' for '.$develomentList->development_name;
        $message='<table width="100%" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
                    <tr>
                        <td>YEEHAW! We\'ve got a live one for '.$develomentList->development_name.' !</td>
                    </tr>
                    <tr>
                         <td height="3"></td>
                    </tr>
                    <tr> 
                        <td style="margin: 0;padding: 0; font-weight: bold;">CONTACT INFORMATION</td>
                    </tr>
                    <tr>
                        <td height="5" style="border-bottom: 1px dashed;width: 150px;"></td>
                    </tr>
                    <tr>
                         <td height="4"></td>
                    </tr>
                    <tr>
                         <td style="line-height: 20px;">
                            First Name:<span>'.' '.$model->first_name.'</span><br/>
                            Last Name:<span>'.' '.$model->last_name.'</span><br/>
                            Phone:<span>'.' '.$model->phone.'</span><br/>
                            Email:<span>'.' '.$model->email.'</span><br/>
                            Address:<span>'.' '.$model->address.'</span><br/>
                            City:<span>'.' '.$model->city.'</span><br/>
                            Province:<span>'.' '.$model->province.'</span><br/>
                            Postal Code:<span>'.' '.$model->postal.'</span><br/>
                            Profession:<span>'.' '.$model->profession.'</span>
                         </td>
                    </tr>
                    <tr>
                         <td height="10"></td>
                    </tr>
                    <tr>
                         <td>
                            <a href="'.$cars_url.'" style="text-decoration: none;background: #0067b3; font-size: 13px; padding: 5px 10px; border-radius: 3px; color: #fff;text-transform: uppercase;">More Here...</a>
                         </td>
                    </tr>
                    <tr>
                         <td height="20"></td>
                    </tr>
                    <tr> 
                        <td style="font-weight: bold;">DEVELOPMENT INFORMATION</td>
                    </tr>
                    <tr>
                        <td height="5" style="border-bottom: 1px dashed;width: 150px;"></td>
                    </tr>
                    <tr>
                         <td height="4"></td>
                    </tr>
                    <tr>
                         <td style="line-height: 20px;">
                            Development Name:<span>' .' '.$develomentList->development_name.'</span><br/>
                            Choice #1:<span>' .' '.$model->ffp_choice_no_1.'</span><br/>
                            Choice #2:<span>' .' '.$model->ffp_choice_no_2.'</span><br/>
                            Choice #3:<span>' .' '.$model->ffp_choice_no_3.'</span><br/>
                            Floor Plan Range:<span>' .' '.$model->desired_floor_range.'</span><br/>
                            Parking:<span>' .' '.$model->do_require_parking.'</span><br/>
                            Locker:<span>' .' '.$model->do_require_locker.'</span><br/>
                            Intended Nature of Purchase:<span>' .' '.$model->intended_nature_purchase.'</span><br/>
                            Realtor:<span>' .' '.$realtor.'</span><br/>
                            Comments:<span>' .' '.$comments_section.'</span>
                         </td>
                    </tr>
                    <tr>
                         <td height="10"></td>
                    </tr>
                    <tr> 
                        <td style="font-weight: bold;">ADDITIONAL & SYSTEM INFORMATION</td>
                    </tr>
                    <tr>
                        <td height="5" style="border-bottom: 1px dashed;width: 150px;"></td>
                    </tr>
                    <tr>
                         <td height="4"></td>
                    </tr>
                    <tr>
                         <td style="line-height: 20px;">
                            Date/Time:<span>' .' '.date('Y-m-d H:i:s').'</span><br/>
                            URL Source:<span>' .' '.$pageUrl.'</span><br/>
                            Media Source:<span>' .' '.$media_source.'</span><br/>
                            IP Address:<span>' .' '.$model->ip_address.'</span><br/>
                            Browser:<span>' .' '.$model->browser.'</span>         
                         </td>
                    </tr>
                </table><br/>
                ';
        $message .='Thanks,<br/><br/>';
        $message .='Reggie<br/><br/>';
        $message .='Confidential Information - Obtain Release Permission <br/><br/>               
                            www.connectassetmanagement.com';

        $emailSend = Yii::$app->mailer->compose()
            ->setFrom('reggie@connectassetmanagement.com')
            ->setTo($email)
            ->setSubject($subject)
            ->setHtmlBody($message);
        $emailSend->send();

        /*$emailSend2 = Yii::$app->mailer->compose()
            ->setFrom('reggie@connectassetmanagement.com')
            ->setTo($email2)
            ->setSubject($subject)
            ->setHtmlBody($message);
        $emailSend2->send();*/

    }

    //@@Send Email Development Registration form "Connect website registration form"
    private function sendEmailFromConnectWp($registerd_datalist,$carsUrl,$contactId) {
        //Assinged User list Get
        $checkAssignedUser=Developments::find()->where(['development_id' => $registerd_datalist->development_id])->one();
        //Developement List
        $develomentList = Developments::find()->where(['development_id' => $registerd_datalist->development_id])->one();
        if (!empty($develomentList)){
            $developement='for'.' '.$develomentList->development_name;
            $developmentname=$develomentList->development_name;
            $tagname =strtolower($developmentname).' - '.'Registration Journey';
        }
        if ($registerd_datalist->realtor==1){
            $realtor='Yes';
        }
        else{
            $realtor='No';
        }

        $subject =' '.$registerd_datalist->first_name.' '.$registerd_datalist->last_name.' for '.$developmentname;
        $message='<table width="100%" cellspacing="0" cellpadding="0" style="table-layout:fixed;" >
                <tr>
                    <td>Howdy Partner,</td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                <tr>
                    <td>We have a new lead '.$developement.'!</td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                <tr>
                     <td style="line-height: 20px;">
                        First Name:<span>'.' '.$registerd_datalist->first_name .'</span><br/>
                        Last Name:<span>'.' '.$registerd_datalist->last_name .'</span><br/>
                        Phone:<span>'.' '.$registerd_datalist->phone .'</span><br/>
                        Email:<span>'.' '.$registerd_datalist->email .'</span><br/>
                        Is a Realtor?:<span>'.' '.$realtor .'</span>
                     </td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                <tr>
                     <td style="line-height: 20px;">
                        Development Name:<span>'.' '.$developmentname.'</span><br/>
                        Media Source:<span>'.' '.'Website'.'</span><br/>
                        Page URL:<span>'.' '.$registerd_datalist->page_url.'</span><br/>
                        Tag Applied:<span>'.' '.$tagname.'</span><br/>
                        Brand: <span>CONNECT asset Management</span>
                     </td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                 <tr>
                     <td>
                        <a href="'.$carsUrl.'" style="text-decoration: none;font-size: 16px;border-radius: 3px;">Learn More....</a>
                     </td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                <tr> 
                    <td>Thanks,</td>
                </tr>
                 <tr>
                     <td height="30"></td>
                </tr>
                 <tr> 
                    <td>Reggie</td>
                </tr>
                <tr>
                     <td height="30"></td>
                </tr>
                <tr> 
                    <td>Confidential Information - Obtain Release Permission</td>
                </tr>
                <tr>
                     <td height="20"></td>
                </tr>
                <tr> 
                    <td>www.connectassetmanagement.com </td>
                </tr>
            </table>';

        $maropost=new maropost();
        $arrayNew = array(
            "content" => array(
                "name" =>'Admin System Global Mail Content [Daily]',
                //"text_part" => "text part of the content",
                "html_part" => $message,
                "full_email" => "false",
                "footer_type" => "default_footer",
                "footer_id" => ""
            ),
            "id" =>191
        );
        $maropost->put_template_content($arrayNew);

        $assign_id='';
        if (!empty($checkAssignedUser->assignment_id)){
            $assign_id=$checkAssignedUser->assignment_id;
        }
        else{
            $assign_id=37;
        }
        $assign_user=User::find()->where(['id'=>$assign_id])->one();
        $response_register=$this->register_assigned_user_maropost($assign_user,$subject);
        if ($response_register['http_status']==201){
            $email_log=new ContactsEmailLog();
            $email_log->contact_id=$contactId;
            $email_log->user_id=$assign_user->id;
            $email_log->subject=$subject;
            $email_log->mail_header=$subject;
            $email_log->mail_body=$message;
            $email_log->sent_date_time=date('Y-m-d H:i:s');
            $email_log->status=1;
            if (!$email_log->save(false)) {
                throw new Exception(json_encode($email_log->errors));
            }
        }
    }

    //@@Assigned User Register to mropost
    private function register_assigned_user_maropost($assign_user,$subject){
        $first_name =   !empty($assign_user->first_name) ? $assign_user->first_name : '';
        $last_name =   !empty($assign_user->last_name) ? $assign_user->last_name : '';
        $mobile =   !empty($assign_user->mobile) ? $assign_user->mobile : '';
        $email =   !empty($assign_user->email) ? $assign_user->email : '';
        $list_id=50;
        $contactsNew=new maropost();
        $contactsNew = $contactsNew->contacts();
        $arrayNew = array(
            'list_id' => $list_id,
            'contact' =>
                array(
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'email' => $email,
                    'subscribe' => 'true',
                    'custom_field' =>
                        array(
                            'phone_1' =>$mobile,
                            'company' => 'CONNECT asset management',
                            'realtor' => 'False',
                            'prospect' => 'true',
                            'system_mail_subject' => $subject,
                            'system_email_status' => time()
                        )
                ));
        $response=$contactsNew->post_new_contact_into_list($arrayNew);
        return $response;
    }

    //@@Learn Website register data to maropost
    private function sendToMaropostForLearnConnect($registraion_data){
        $projectUrl = $registraion_data['pageUrl'];

        $realtor = '';
        $prospect = '';
        $realtor_tag = '';

        if (!empty($registraion_data['realtor'])) {
            $realtor_ = 'true';
            $prospect = 'False';
            $realtor = 1;
            $realtor_tag = 'Realtor';
            $list_id = 17;
        } else {
            $realtor_ = 'False';
            $prospect = 'true';
            $realtor_tag = 'Prospect';
            $realtor = 0;
            $list_id = 1;
        }

        $registraion_data['infusionsoftTag']='Learn Erational - Step 1';
        $registraion_data['media_source']=$registraion_data['media_source'];

        $tags=new tags();
        //tag created if tag not exist in maropost
        $array = array(
            "tags" => array(
                "name" =>$registraion_data['infusionsoftTag']
            )
        );
        $tags->post_tags($array);


        $array1 = array(
            "tags" => array(
                "name" =>$registraion_data['media_source']
            )
        );
        $tags->post_tags($array1);

        $contactsNew=new maropost();
        $contactsNew = $contactsNew->contacts();

        $arrayNew = array(
            'list_id' => $list_id,
            'contact' =>
                array(
                    'first_name' => $registraion_data['firstname'],
                    'last_name' => $registraion_data['lastname'],
                    'email' => $registraion_data['email'],
                    'subscribe' => 'true',
                    'custom_field' =>
                        array(
                            'phone_1' => $registraion_data['phone'],
                            'company' => 'CONNECT asset management',
                            'realtor' => $realtor_,
                            'prospect' => $prospect,
                            'reference_link' => $projectUrl,
                            'development_name' 	    => 'CONNECT asset management',
                            'development_reg_existing' 	=> time(),
                        ),
                    'add_tags' =>
                        array(
                            $registraion_data['infusionsoftTag'],
                            $registraion_data['media_source'],
                            $realtor_tag
                        )
                ));
        $arrayNewsletter = $arrayNew;
        if($arrayNewsletter['list_id'] == 1){
            $arrayNewsletter['list_id'] = 3;
            $contactsNew->post_new_contact_into_list($arrayNewsletter);
        }
        $response=$contactsNew->post_new_contact_into_list($arrayNew);
    }
}

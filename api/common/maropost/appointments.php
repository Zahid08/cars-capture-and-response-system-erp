<?php


	/**
	 * http://api.maropost.com/api
	 * Wrapper for communication to the Maropost system via JSON api
	 * You may need to refer to the above page to figure out what post/put body fields are accepted/required.
	 * Sub classes will check for required fields and return false rather then throwing a system error if validation fails.
	 * An error log entry is added for these validation fails describing what the error is, and in which method.
	 * i.e. 428 - Required Field "foo" missing in method bar.
	 * Note: HTTP status code 428 is 'Precondition Required' and is used for validation errors.
	 */

namespace api\common\maropost;
use api\common\maropost\maropost;
use Yii;
use api\common\maropost\contacts;
	class appointments {

		public $page = 1;

		//Connect info
		public $auth_token 	= "M7lNxhfdmqvn8gWcdaig7vM1b84lI5VFRlQOQHT18wNX5e0lksDAWw";
		public $url_api 	= "http://rdb.maropost.com/1384/";

		public function __construct( $auth_token = FALSE, $api_url = FALSE ) {
			$this->auth_token = $auth_token ?: $this->auth_token;
			$this->url_api    = $api_url ?: $this->url_api;
		}

        public function post_apointments( array $record ) {
            return $this->request( "POST", 'appointments/create', array("record" =>$record["record"]) );
        }
        public function post_agent_apointments( array $record ) {
            return $this->request( "POST", 'agent_appointments/create', array("record" =>$record["record"]) );
        }

		public function request( $action, $endpoint, $dataArray, $options = FALSE ) {
			if ( $this->page > 1 || $options['page'] > 1 ) {
				$dataArray['page'] = $this->page > 1 ? $this->page : $options['page'];
			}

			$auth_token = ! empty( $options['auth_token'] ) ? $options['auth_token'] : $this->auth_token;
			$url_api    = ! empty( $options['url_api'] ) ? $options['url_api'] : $this->url_api;

			$url = $url_api . $endpoint . ".json";
			$ch  = curl_init();

			if ( $action == "GET" ) {
				$newURL = json_encode( $dataArray );
				$newURL = str_replace( "{", "", $newURL );
				$newURL = str_replace( "}", "", $newURL );
				$newURL = str_replace( ":", "=", $newURL );
				$newURL = str_replace( ",", "&", $newURL );
				$newURL = str_replace( '"', "", $newURL );
				$url    = $newURL !== 'null' && ! empty( $newURL ) ? $url . "?" . $newURL : $url;
				//echo $url . "<br/>";
				$json = json_encode( array( 'auth_token' => $auth_token ) );
			} else {
				$dataArray['auth_token'] = $auth_token;
				$json                    = json_encode( $dataArray );
			}

			//curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			//curl_setopt( $ch, CURLOPT_MAXREDIRS, 10 );
			curl_setopt( $ch, CURLOPT_URL, $url );
			curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "$action" );
			if ( $action = "POST" ) {
				curl_setopt( $ch, CURLOPT_POSTFIELDS, $json );
			}
			curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
				'Content-type: application/json',
				'Accept: application/json'
			) );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, TRUE );
			curl_setopt( $ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
			$output         = curl_exec( $ch );
			$curlinfo       = curl_getinfo( $ch );
			$content_length = $curlinfo["download_content_length"];
			$http_code      = $curlinfo["http_code"];

			error_log($json);
			error_log($url);
			error_log($http_code);
			error_log($output);
			if ( $output === FALSE ) {
				printf( "cUrl error (#%d): %s<br>\n", curl_errno( $ch ),
					htmlspecialchars( curl_error( $ch ) ) );
			}
			$decoded                = json_decode( $output, TRUE );
			$decoded['http_status'] = $http_code;
			$decoded['http_request_url'] = $http_code;
			$decoded['http_request_json'] = $json;
			curl_close( $ch );

			return $decoded;
		}
	}

<?php

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),    
    'bootstrap' => ['log'],
    'modules' => [
        'v1' => [
            'basePath' => '@app/modules/v1',
            'class' => 'api\modules\v1\Module'
        ]
    ],
    'timeZone' => 'America/Toronto',
    'components' => [
        'request' => [
            'enableCookieValidation' => false,
            'enableCsrfValidation'   => false,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'formatter' => [
            'class' => 'unlock\modules\core\i18n\Formatter',
            'timeZone' => 'America/Toronto',
            'dateFormat' => 'php:m/d/Y',
            'datetimeFormat' => 'php:M d Y \a\t g:i a',
            'timeFormat' => 'php:g:i a',
            //'defaultTimeZone' => 'Asia/Dhaka',
        ],
        'response' => [
            'class' => 'yii\web\Response',
            'format' => \yii\web\Response::FORMAT_JSON,
            'charset' => 'utf-8',
            'on beforeSend' => function ($event) {
                $response = $event->sender;
                if ($response->data !== null) {
                    $response->data = [
                        'success' => $response->isSuccessful,
                        'message' => $response->statusText,
                        'count' => count($response->data),
                        'status' => $response->getStatusCode(),
                        'timestamp' => time(),
                        'path' => Yii::$app->request->getPathInfo(),
                        'data' => $response->data,
                    ];
                    $response->statusCode = 200;
                }
            },
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'api\models\User',
            'enableAutoLogin' => false,
            'enableSession' => false,
            'loginUrl' => null,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => [
                        'v1/developers',
                        'v1/contacts',
                        'v1/developments',
                    ],
                    'pluralize' => true,
                    'tokens' => [
                        '{id}' => '<id:\\w+>'
                    ],
                    'extraPatterns' => [
                        'POST create-multiple' => 'create-multiple',
                    ]
                ],
            ],
        ]
    ],
    'params' => $params,
];
